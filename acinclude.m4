# a macro to get the libs/cflags for libxml (adapted from test for libxml)
# serial 1

dnl AM_PATH_LIBXML([ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND [, MODULES]]])
dnl Test to see if libxml is installed, and define LIBXML_CFLAGS, LIBS
dnl
AC_DEFUN(AM_PATH_LIBXML,
[dnl
dnl Get the cflags and libraries from the libxml-config script
dnl
AC_ARG_WITH(xml-config,
[  --with-xml-config=XML_CONFIG  Location of xml-config],
LIBXML_CONFIG="$withval")

AC_PATH_PROG(LIBXML_CONFIG, xml-config, no)
AC_MSG_CHECKING(for libxml)
if test "$LIBXML_CONFIG" = "no"; then
  AC_MSG_RESULT(no)
  ifelse([$2], , :, [$2])
else
  LIBXML_CFLAGS=`$LIBXML_CONFIG --cflags`
  LIBXML_LIBS=`$LIBXML_CONFIG --libs`
  AC_MSG_RESULT(yes)
  ifelse([$1], , :, [$1])
fi
AC_SUBST(LIBXML_CFLAGS)
AC_SUBST(LIBXML_LIBS)
])

