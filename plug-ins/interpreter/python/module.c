
/* the code is in python.c */

#include <stdio.h>
#include <glib.h>
#include <gmodule.h>
#include <config.h>
#include "struct.h"
#include "gabyerror.h"
#include "errors.h"

GList *list_windows = NULL;
void *app = NULL;
gchar *appname;
char language[3];

GList *list_tables;
GList *list_subtables;
GList *list_views;
GList *list_actions;

void gaby_perror_in_a_box()
{
	if ( gaby_errno == CUSTOM_MESSAGE || gaby_errno == CUSTOM_ERROR ) {
		fputs(gaby_message, stdout);
		fputs("\n", stdout);
	} else {
		fputs(gaby_perror(), stderr);
		fputs("\n", stderr);
	}
	if ( gaby_message ) g_free(gaby_message);
}

#ifdef FOLLOW_MIGUEL
#  ifdef NO_GUI
#    define GtkWidget int
#  endif
#  include "../../../src/py_fake.c"
#endif

