#! /usr/bin/python

import string

f = open ( '../../plug-ins/interpreter/python/python.c' )
lines = f.readlines()
f.close()

object = 0
section = 0
para = 0

para_about_gabywindow = '''<para>
This object is used to hold a window as shown in Gaby. It has methods for
common window actions (move, resize, destroy) as well as for actions specific
to Gaby (bind, unbind, ...). It also has attributes to describe it closer
(type, name, subtable, ...).
</para>'''

para_about_gabysubtable = '''<para>
This object is used to hold a window as shown in Gaby. It has methods to access
(both get and set) particular records, to move within the records, ...
It also has attributes to describe it closer (name, fields, ...).
</para>'''

for line in lines:
	if ( line == '/* GabyWindow Object */\n' ):
		print '<sect2><title>GabyWindow object</title>\n'
		print para_about_gabywindow
		object = 1
		section = 0
		continue
	if ( line == '/* GabySubTable Object */\n' ):
		print '<sect2><title>GabySubTable object</title>\n'
		print para_about_gabysubtable
		object = 2
		section = 0
		continue
	if ( line == '/* misc functions */\n' ):
		print '<sect2><title>Standalone functions</title>\n'
		object = -1
		section = 1
		continue
	if ( object != 0 and line[:11] == 'static char' ):
		print '</sect3></sect2>\n'
		object = 0
		section = 0
	if ( object == 0 ):
		continue
	if ( line == '\t/* Methods */\n' ):
		print '<sect3><title>Methods</title>'
		section = 1
		continue
	if ( line == '\t/* Items */\n' ):
		print '</sect3>\n'
		print '<sect3><title>Items</title>'
		section = 2
		continue
	if ( line[-4:] == '/**\n' ):
		print '\n<simplesect>'
		para = 1
		continue
	if ( line[-4:] == '**/\n') :
		print '</para></simplesect>'
		para = 0
	if ( para == 0 ):
		continue
	if ( line[0] == '\t' ):
		realline = line[4:-1]
	else:
		realline = line[3:-1]
	if ( para == 1 and section == 1 ):
		method_name = realline [ string.find(realline, '.')+1 : 
						string.find(realline, '(') ]
		args = realline [ string.find(realline, '(')+1 : \
						string.rfind(realline, ')') ]
		print '<title>' + method_name + '</title>'
		print '<para><function>' + method_name + '</function>' + \
					' (' + args + ')</para>'
		print '<para>'
		para = 2
		continue
	if ( para == 1 and section == 2 ):
		attribute_name = realline [ string.find(realline, '.')+1 : ]
		print '<title>' + attribute_name + '</title>'
		print '<para>'
		para = 2
		continue
	if ( len(realline) < 2 or realline == 'Description:' ):
		continue
	print realline

print '</sect2>'

