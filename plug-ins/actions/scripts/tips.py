
from gtk import *
import whrandom
import os

if ( len(os.environ['LANGUAGE']) > 0 ):
	filename = params[1] + '_' + os.environ['LANGUAGE'] + '.txt'

try:
	tips = open(filename)
except IOError:
	tips = open(params[1] + '.txt')

line = tips.readline()

tip = ''
list_tips = []

while ( len(line) > 0 ):
	if ( line[0] == '#' ):
		line = tips.readline(); continue
	if ( len(tip) == 0 and line[0] == '\n'):
		line = tips.readline(); continue
	if ( line[0] == '\n' ):
		tip = tip[0:-1]
		list_tips.append(tip)
		tip = ''
		line = tips.readline()
		continue
	tip = tip + line
	line = tips.readline();

tips.close()

todaytip = whrandom.choice(list_tips)

def ok_clicked(*args):
        win.destroy()

win = GtkWindow(type=WINDOW_DIALOG)
win.set_title(params[0])

box = GtkVBox()
box.set_border_width(5)
box.set_spacing(5)
win.add(box)
box.show()

label = GtkLabel(todaytip)
label.show()
box.pack_start(label, expand=FALSE)

separ = GtkHSeparator()
separ.show()
box.pack_start(separ, expand=FALSE)

button = GtkButton('OK')
button.show()
button.connect('clicked', ok_clicked)
box.pack_end(button, expand=FALSE, fill=FALSE)

win.show()

