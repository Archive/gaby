# /usr/bin/gabyscript --as select --interpreter python
# Interpreter: Python
# Descfiles: *
# Description: show the number of records in a (selectable) subtable

subtable = gaby.select_subtable()
if subtable == None: # we take the current window
	subtable = gaby.get_current_window().subtable

list = subtable.fast_records_list()
length = len(list)
print 'There are %d records in subtable %s.' % ( length, subtable.i18n_name )
	# I used to do 'print 'there are', length, 'records'... but it bugged
	# that way: it gave 'There are45records' when redirecting sys.output to
	# a window
# print len(get_current_window().subtable.fast_records_list())

