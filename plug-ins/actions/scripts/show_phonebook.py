# /usr/bin/gabyscript --as gaby --interpreter python
# Interpreter: Python
# Descfiles: gaby
# Description: dumps a list of people and their phone numbers

subtable = gaby.get_subtable_by_name('Phone Book')
list = subtable.fast_records_list()

print '%-20s%-20s%-20s' % ( subtable.i18n_fields[0],
			    subtable.i18n_fields[1],
			    subtable.i18n_fields[2] )
print '-'*52

dict = {}

for id in list:
	r = subtable.get_record_no(id)
	dict[ r[ subtable.dict_fields['Last Name'] ] ] = r

keys = dict.keys()
keys.sort()

for k in keys:
	r = dict[ k ]
	print '%-20s%-20s%-20s' % ( r[0], r[1], r[2] )

