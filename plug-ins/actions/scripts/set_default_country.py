# /usr/bin/gabyscript --as gaby --interpreter python
# Interpreter: python
# Descfiles: gaby
# Description: Sets the country field of every records

# This script sets the country fields of every records to 'Belgium' if no
# country was previously given.

subtable = gbay.get_subtable_by_name('Address Book')
field_number = subtable.dict_fields['Country']

for i in subtable.fast_records_list():
	record = subtable.get_record_no(i)
	if ( len(record[field_number]) == 0 ):
		record[field_number] = 'Belgium'
	subtable.set_record_no(i, record)

