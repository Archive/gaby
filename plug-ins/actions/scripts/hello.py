
from gtk import *


# simple test function
gaby.hello_world ();

def ok_clicked(*args):
	win.destroy()

	# retrieving a record
win = gaby.get_current_window()
record = win.subtable.get_record_no(win.id)

mainw = GtkWindow(_obj = win.gtkwindow)
mainw.set_title('My title was cracked by evil Python script kiddies!')

win = GtkWindow()
win.set_title('Hello from Python/GTK+')	
box = GtkVBox()
win.add(box)
box.show()

for i in range(0, len(record) ):
	label = GtkLabel(record[i])
	label.show()
	box.pack_start(label, expand=FALSE)

separ = GtkHSeparator()
separ.show()
box.pack_start(separ, expand=FALSE)

button = GtkButton('OK')
button.show()
button.connect('clicked', ok_clicked)
box.pack_start(button, expand=FALSE)

win.show()

