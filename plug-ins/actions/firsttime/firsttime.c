/*  Gaby
 *  Copyright (C) 1998-2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* 
 * I wish to thank Pan authors; I started this file using their druid.c as a
 * basis (only real usage of gnome-druid I saw)
 */

#include <gaby.h>
#include <tables.h>
#include <f_config.h>
#include "gnome-druid-page-text.h"

/* this logo is just a temporary pic waiting to be replaced */
#include "logo.xpm"

#include "gaby-icon.xpm"

/* action: druid */

mstatic void firsttime_druid (struct action_param *table, int *dec);

#ifndef FOLLOW_MIGUEL
void get_function_by_name(gchar *name, action *a)
{
	a->function = NULL;
	if ( strcmp(name, "druid") == 0 ) {
		a->function = firsttime_druid;
	}
}
#endif /* ! FOLLOW_MIGUEL */


#ifdef USE_GNOME
static void druid_next (GnomeDruidPage *page, GnomeDruid *druid,
			GnomeDruidPage *next)
{
	gtk_object_set_data (GTK_OBJECT (next), "back", page);
	gnome_druid_set_page (druid, next);
}

static void druid_finish (GnomeDruidPage *page, GnomeDruid *druid,
			  gpointer window)
{
	int *dec = gtk_object_get_data(GTK_OBJECT(druid), "dec");
	(*dec)--;
	gtk_widget_destroy (GTK_WIDGET (window));
}

static GtkWidget* create_druid_text_page(char *title, char *text,
					 GdkImlibImage *logo, GdkImlibImage *wm)
{
	/* I might use a canvas item if I want common pages to look like the
	 * start/finish pages */
	GtkWidget *page;
	GtkWidget *label;
	GtkWidget *vbox, *hbox;
	
	/* I wonder how imlib manage memory: do I have to free it myself once
	 * I'm done ?
	 * and when switching to gdk-pixbuf, how will it work ? */
	logo = gdk_imlib_create_image_from_xpm_data (gaby_icon_xpm);
	page = gnome_druid_page_standard_new_with_vals (title, logo);
	vbox = GNOME_DRUID_PAGE_STANDARD(page)->vbox;
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start (GTK_BOX(vbox), hbox, FALSE, FALSE, 40);
	gtk_widget_show(hbox);
	
	label = gtk_label_new(text);
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
	gtk_widget_show(label);
	gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 30);
	return page;
}

mstatic void firsttime_druid (struct action_param *table, int *dec)
{
	GtkWidget *window;
	GtkWidget *druid;
	gchar *fname;
	GdkImlibImage *logo = NULL;
	GdkImlibImage *watermark = NULL;
	GtkWidget *page_start, *page_design1, *page_design2, *page_select;
	GtkWidget *page_builder, *page_finish;
	
	window = gtk_window_new (gnome_preferences_get_dialog_type());
	gtk_window_set_position (GTK_WINDOW (window), GTK_WIN_POS_CENTER_ALWAYS);

	druid = gnome_druid_new ();
	gtk_object_set_data(GTK_OBJECT(druid), "dec", dec);

	fname = gnome_pixmap_file ("gnome-logo-icon.png");
	if (fname) {
		logo = gdk_imlib_load_image (fname);
		g_free (fname);
	} else {
		logo = gdk_imlib_create_image_from_xpm_data (gaby_icon_xpm);
	}

	watermark = gdk_imlib_create_image_from_xpm_data (logo_xpm);
	
	page_start = gnome_druid_page_start_new_with_vals (
_("First minute in Gaby"),
_("Welcome to Gaby!\n"
  "Since this is your first time running Gaby,\n"
  "I'll talk you a bit about its philosophy and\n"
  "ask you a few questions.\n"),
		logo, watermark);

	/* I should probably let GTK+ handle new lines... */
	page_design1 = create_druid_text_page (
_("Gaby design"),
_("As you might already know, Gaby is a small database manager;\n"
  "that means it is designed to handle things like addresses,\n"
  "CDs, tape records, ... (NOT your critical business database).\n\n"
  "The reason for such a choice is simply making your life easier\n"
  "(you shouldn't have to know SQL to handle your friends' phone numbers!)"),
		logo, watermark);
	
	page_design2 = create_druid_text_page (
_("Gaby design (II)"), 
_("This means you can launch Gaby and (voila!) you're ready to\n"
  "enter some addresses -- you don't need things like File/Open, ...\n"
  "But being able to enter addresses is not really interesting\n"
  "when what you need is to look for the songs on a CD.\n"
  "And the question is now: where to say you want CDs ?"),
		logo, watermark);

	/* This will be moved to a special case when there will be possible for
	 * the user to choose a default descfile */
	page_select = create_druid_text_page (
_("So Where ?"),
_("The traditional (and still current) answer is to use command line\n"
  "args (or UNIX tricks like symbolic links). The idea is to execute:\n"
  "'gaby --as gcd' or (with the symlink trick) 'gaby-gcd').\n\n"
  "There is also a \"databases\" menu item in the File menu useful to\n"
  "start a new Gaby session with another database\n\n"
  "[it will be possible here to configure a default startup. Please wait.]"),
		logo, watermark);
	
	page_builder = create_druid_text_page (
_("The Gaby Database Builder"),
_("And if you do not find the kind of database you need in the provided\n"
  "ones, there is the Gaby Database Builder that allows you to define\n"
  "tables, fields, ... [try 'gabybuilder' on the command line]"),
		logo, watermark);

	page_finish = gnome_druid_page_finish_new_with_vals (
_("First minute over!"),
_("Voila.\n"
  "Gaby should now be ready to please you.\n"
  "If it does (or doesn't) don't hesitate to send\n"
  "any comments, bug reports or suggestions\n"
  "to fpeters@theridion.com."),
		logo, watermark);
	
	gtk_signal_connect (GTK_OBJECT (page_design1), "next",
			    GTK_SIGNAL_FUNC (druid_next),
			    (gpointer) page_design2);
	gtk_signal_connect (GTK_OBJECT (page_design2), "next",
			    GTK_SIGNAL_FUNC (druid_next),
			    (gpointer) page_select);
	gtk_signal_connect (GTK_OBJECT (page_select), "next",
			    GTK_SIGNAL_FUNC (druid_next),
			    (gpointer) page_builder);
	gtk_signal_connect (GTK_OBJECT (page_builder), "next",
			    GTK_SIGNAL_FUNC (druid_next),
			    (gpointer) page_finish);
	gtk_signal_connect (GTK_OBJECT (page_finish), "finish",
			    GTK_SIGNAL_FUNC (druid_finish),
			    window);

	gtk_signal_connect_object (GTK_OBJECT (druid), "cancel",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy),
				   GTK_OBJECT (window));

	/* tie it all together */
	gtk_container_add (GTK_CONTAINER (window), druid);		
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (page_start));
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (page_design1));
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (page_design2));
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (page_select));
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (page_builder));
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (page_finish));

	/* set the first page */
	gnome_druid_set_page (GNOME_DRUID (druid),
			      GNOME_DRUID_PAGE (page_start));

	gtk_widget_show_all (window);

}

#else /* ! USE_GNOME */

mstatic void firsttime_druid (struct action_param *table, int *dec)
{
	/* TODO (post-2.0?): gtk-only fisrt minute
	 * delayed since beginners probably uses Gnome
	 */
}

#endif

