/* gnome-druid-page-text.h
 * Copyright (C) 2000 Frederic Peters
 * based on gnome-druid-page-start.h, copyright (C) 1999  Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GNOME_DRUID_PAGE_TEXT_H__
#define __GNOME_DRUID_PAGE_TEXT_H__

#include <gtk/gtk.h>
#include <gdk_imlib.h>
#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-druid-page.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_DRUID_PAGE_TEXT			(gnome_druid_page_text_get_type ())
#define GNOME_DRUID_PAGE_TEXT(obj)			(GTK_CHECK_CAST ((obj), GNOME_TYPE_DRUID_PAGE_TEXT, GnomeDruidPageText))
#define GNOME_DRUID_PAGE_TEXT_CLASS(klass)		(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_DRUID_PAGE_TEXT, GnomeDruidPageTextClass))
#define GNOME_IS_DRUID_PAGE_TEXT(obj)			(GTK_CHECK_TYPE ((obj), GNOME_TYPE_DRUID_PAGE_TEXT))
#define GNOME_IS_DRUID_PAGE_TEXT_CLASS(klass)		(GTK_CHECK_CLASS_TYPE ((obj), GNOME_TYPE_DRUID_PAGE_TEXT))


typedef struct _GnomeDruidPageText       GnomeDruidPageText;
typedef struct _GnomeDruidPageTextClass  GnomeDruidPageTextClass;

struct _GnomeDruidPageText
{
	GnomeDruidPage parent;

	GdkColor background_color;
	GdkColor textbox_color;
	GdkColor logo_background_color;
	GdkColor title_color;
	GdkColor text_color;

	gchar *title;
	gchar *text;
	GdkImlibImage *logo_image;
	GdkImlibImage *watermark_image;

	/*< private >*/
	GtkWidget *canvas;
	GnomeCanvasItem *background_item;
	GnomeCanvasItem *textbox_item;
	GnomeCanvasItem *text_item;
	GnomeCanvasItem *logo_item;
	GnomeCanvasItem *logoframe_item;
	GnomeCanvasItem *watermark_item;
	GnomeCanvasItem *title_item;
};
struct _GnomeDruidPageTextClass
{
  GnomeDruidPageClass parent_class;
};


GtkType    gnome_druid_page_text_get_type    (void);
GtkWidget *gnome_druid_page_text_new         (void);
GtkWidget *gnome_druid_page_text_new_with_vals(const gchar *title,
					       const gchar* text,
					       GdkImlibImage *logo,
					       GdkImlibImage *watermark);
void gnome_druid_page_text_set_bg_color      (GnomeDruidPageText *druid_page_text,
					       GdkColor *color);
void gnome_druid_page_text_set_textbox_color (GnomeDruidPageText *druid_page_text,
					       GdkColor *color);
void gnome_druid_page_text_set_logo_bg_color (GnomeDruidPageText *druid_page_text,
					       GdkColor *color);
void gnome_druid_page_text_set_title_color   (GnomeDruidPageText *druid_page_text,
					       GdkColor *color);
void gnome_druid_page_text_set_text_color    (GnomeDruidPageText *druid_page_text,
					       GdkColor *color);
void gnome_druid_page_text_set_text          (GnomeDruidPageText *druid_page_text,
					       const gchar *text);
void gnome_druid_page_text_set_title         (GnomeDruidPageText *druid_page_text,
					       const gchar *title);
void gnome_druid_page_text_set_logo          (GnomeDruidPageText *druid_page_text,
					       GdkImlibImage *logo_image);
void gnome_druid_page_text_set_watermark     (GnomeDruidPageText *druid_page_text,
					       GdkImlibImage *watermark);

END_GNOME_DECLS

#endif /* __GNOME_DRUID_PAGE_TEXT_H__ */
