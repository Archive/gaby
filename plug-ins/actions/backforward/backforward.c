/*  Gaby
 *  Copyright (C) 1998-2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <tables.h>
#include <f_config.h>
#include <gtk_config_dlg.h>

/* action: toolbar */

static GList *positions = NULL;
static GList *cur_pos = NULL;

mstatic void backforward_toolbar (struct action_param *table, int *dec);

#ifndef FOLLOW_MIGUEL
static gboolean already_there = FALSE;

void get_function_by_name(gchar *name, action *a)
{
	a->function = NULL;
	if ( ! already_there && strcmp(name, "toolbar") == 0 ) {
		a->function = backforward_toolbar;
		already_there = TRUE;
	}
}
#endif /* ! FOLLOW_MIGUEL */

static void backforward_back(GtkWidget *but, gpointer data);
static void backforward_go(GtkWidget *but, gpointer data);
static void backforward_forward(GtkWidget *but, gpointer data);
static void backforward_attach(GtkWidget *but, gpointer data);

static GnomeUIInfo bf_toolbar[] = {
/* 0 */	{GNOME_APP_UI_ITEM, N_("Back"), NULL,
		backforward_back, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_UNDO, 0, 0, NULL},

/* 1 */	{GNOME_APP_UI_SEPARATOR},
	
/* 2 */	{GNOME_APP_UI_ITEM, N_("Memorize"), NULL,
		backforward_attach, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_ATTACH, 0, 0, NULL},
/* x */ /* built manually */
/* 3 */	{GNOME_APP_UI_SEPARATOR},

/* 4 */	{GNOME_APP_UI_ITEM, N_("Forward"), NULL,
		backforward_forward, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_REDO, 0, 0, NULL},

/* 5 */	{GNOME_APP_UI_ENDOFINFO}
};

#define disable_back()	   gtk_widget_set_sensitive(bf_toolbar[0].widget, FALSE)
#define enable_back()	   gtk_widget_set_sensitive(bf_toolbar[0].widget, TRUE)
#define disable_forward()  gtk_widget_set_sensitive(bf_toolbar[4].widget, FALSE)
#define enable_forward()   gtk_widget_set_sensitive(bf_toolbar[4].widget, TRUE)

static void set_buttons_sensitiveness()
{
	if ( cur_pos == NULL ) disable_back();
	else enable_back();

	if ( cur_pos != NULL && cur_pos->next != NULL ) {
		enable_forward();
	} else {
		if ( cur_pos == NULL && positions != NULL ) enable_forward();
		else disable_forward();
	}
	
}

mstatic void backforward_toolbar (struct action_param *table, int *dec)
{
	GtkWidget *toolbar;
	GtkWidget *vbox, *entry, *button;

	toolbar = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_BOTH);
	gtk_widget_show(toolbar);
	gnome_app_fill_toolbar(GTK_TOOLBAR(toolbar), bf_toolbar, NULL);

#if 0
	vbox = gtk_vbox_new(FALSE, 0);
	entry = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(vbox), entry, TRUE, TRUE, 1);
	gtk_widget_show(entry);
	button = gtk_button_new_with_label("Go!");
	gtk_signal_connect(GTK_OBJECT(button), "clicked", backforward_go, NULL);
	gtk_box_pack_start(GTK_BOX(vbox), button, TRUE, TRUE, 1);
	gtk_widget_show(button);
	gtk_widget_show(vbox);
	gtk_toolbar_insert_widget(GTK_TOOLBAR(toolbar), vbox, NULL, NULL, 3);
#endif

	set_buttons_sensitiveness();

	gnome_app_add_toolbar(app, GTK_TOOLBAR(toolbar), "BackForward Toolbar", 
		/*GNOME_DOCK_ITEM_BEH_EXCLUSIVE | */GNOME_DOCK_ITEM_BEH_NEVER_VERTICAL,
		GNOME_DOCK_TOP, 2, 0, 0);
}

static void backforward_back(GtkWidget *but, gpointer data)
{
	gabywindow *win = list_windows->data;
	int id;
	
	if ( cur_pos == NULL ) return;
	
	id = GPOINTER_TO_INT(cur_pos->data);
	cur_pos = g_list_previous(cur_pos);
	
	set_buttons_sensitiveness();

	win->id = id;
	win->view->type->view_fill(win);

}

static void backforward_forward(GtkWidget *but, gpointer data)
{
	gabywindow *win = list_windows->data;
	int id;
	
	if ( positions == NULL ) return;
	if ( cur_pos == NULL ) {
		cur_pos = g_list_first(positions);
	} else {
		if ( cur_pos->next == NULL ) return;
		cur_pos = g_list_next(cur_pos);
	}
	id = GPOINTER_TO_INT(cur_pos->data);
	
	set_buttons_sensitiveness();

	win->id = id;
	win->view->type->view_fill(win);
	
}

static void backforward_go(GtkWidget *but, gpointer data)
{
	gabywindow *win = list_windows->data;

#ifdef DEBUG_GABY
	debug_print("[backforward_go] win: %s\n", win->name);
#endif

}

static void backforward_attach(GtkWidget *but, gpointer data)
{
	gabywindow *win = list_windows->data;
	GList *tmp = NULL;
	
	if ( cur_pos != NULL && cur_pos->next == NULL ) {
	} else {
		tmp = positions;
		if ( cur_pos == NULL ) {
			if ( positions != NULL ) {
				g_list_free(positions);
				positions = NULL;
			}
		} else {
			while ( tmp != cur_pos )
				tmp = g_list_next(tmp);
			while ( tmp->next != NULL ) 
				tmp = g_list_remove_link(tmp, tmp->next);
			if ( tmp == NULL ) positions = NULL;
		}
	}
	positions = g_list_append(positions, GINT_TO_POINTER(win->id));
	cur_pos = g_list_last(positions);

	set_buttons_sensitiveness();

}

