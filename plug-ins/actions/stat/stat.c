/*  Gaby
 *  Copyright (C) 1998-2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <tables.h>
#include <f_config.h>
/*#include <configure.h>*/

/* actions: count sum average maximum minimum */

mstatic void stat_count   (struct action_param fields[1], int *dec);
mstatic void stat_sum     (struct action_param params[2], int *dec);
mstatic void stat_average (struct action_param params[2], int *dec);
mstatic void stat_maximum (struct action_param params[2], int *dec);
mstatic void stat_minimum (struct action_param params[2], int *dec);

#ifndef FOLLOW_MIGUEL
void get_function_by_name(gchar *name, action *a)
{
	a->function = NULL;
	if ( strcmp(name, "count") == 0 )
		a->function = stat_count;
	if ( strcmp(name, "sum") == 0 )
		a->function = stat_sum;
	if ( strcmp(name, "average") == 0 )
		a->function = stat_average;
	if ( strcmp(name, "maximum") == 0 )
		a->function = stat_maximum;
	if ( strcmp(name, "minimum") == 0 )
		a->function = stat_minimum;
}
#endif /* ! FOLLOW_MIGUEL */

static int count_real (table *t)
{
	int i;
	int count=0;
	
	for ( i=0; i < t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 ) count++;
	}

	return count;
}

/**
 * count
 * @param1: table
 * 
 * Description:
 * count the number of records in a table
 */
mstatic void stat_count (struct action_param param[1], int *dec)
{
	char message[1000];
	int count;

	if ( param[0].type != P_TABLE ) {
#ifdef DEBUG_GABY
		debug_print("count called with an invalid parameter.\n");
#endif
		(*dec)--;
		return;
	}

	count = count_real(param[0].table);

	gaby_errno = 1;
	
	sprintf(message, _("There are %d records."), count );
	gaby_message = g_strdup(message);
	(*dec)--;
}

static long sum_int_real(table *t, int field_no)
{
	int i;
	long sum=0;
	
	for ( i=0; i < t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 )
			sum += t->records[i]->cont[field_no].i;
	}

	return sum;
}

static float sum_double_real(table *t, int field_no)
{
	int i;
	float sum=0;
	
	for ( i=0; i < t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 )
			sum += t->records[i]->cont[field_no].d;
	}

	return sum;
}

/**
 * sum
 * @param1: table
 * @param2: field number
 *
 * Description:
 * Sum the number of the given field number from the given table. It handles
 * integers and reals.
 */
mstatic void stat_sum (struct action_param params[2], int *dec)
{
	char message[1000];
	long result_int;
	float result_dbl;
	field *f;

	f = &params[1].table->fields[params[1].field_no];
	
	switch ( f->type ) {
		case T_INTEGER:
		{
			result_int = sum_int_real(params[0].table, 
					params[1].field_no);
			sprintf(message, _("The sum is %ld."), result_int);
		} break;
		case T_DECIMAL:
		{
			result_int = sum_int_real(params[0].table, 
					params[1].field_no);
			sprintf(message, _("The sum is %f."), result_int/100.0);
		} break;
		case T_REAL:
		{
			result_dbl = sum_double_real(params[0].table, 
					params[1].field_no);
			sprintf(message, _("The sum is %f."), result_dbl);
		} break;
		default:
		{
			(*dec)--;
			return;
		} break;
	}
	
	gaby_message = g_strdup(message);
	gaby_errno = 1;
	(*dec)--;
}

/**
 * average
 * @param1: table
 * @param2: field number
 *
 * Description:
 * Calculate the average of the given field number from the given table.
 * It handles integers and reals.
 */
mstatic void stat_average (struct action_param params[2], int *dec)
{
	char message[1000];
	long result_int;
	float result_dbl;
	field *f;
	double result;
	int count;

	f = &params[1].table->fields[params[1].field_no];
	
	count = count_real(params[0].table);
	
	switch ( f->type ) {
		case T_INTEGER:
		{
			result_int = sum_int_real(params[0].table, 
					params[1].field_no);
			result = (double)result_int / (double)count;
		} break;
		case T_DECIMAL:
		{
			result_int = sum_int_real(params[0].table, 
					params[1].field_no);
			result = (double)result_int / (double)count;
			result /= 100.0;
		} break;
		case T_REAL:
		{
			result_dbl = sum_double_real(params[0].table, 
					params[1].field_no);
			result = (double)result_dbl / (double)count;
		} break;
		default:
		{
			(*dec)--;
			return;
		} break;
	}
	
	sprintf(message, _("The average is %f."), result);
	
	gaby_message = g_strdup(message);
	gaby_errno = 1;

	(*dec)--;
}

static float max_double_real(table *t, int field_no)
{
	int i;
	float max=0;

	for ( i=0; i < t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 ) {
			max = t->records[i]->cont[field_no].d;
			break;
		}
	}
		
	for ( i=i+1; i < t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 )
			if ( t->records[i]->cont[field_no].d > max )
				max = t->records[i]->cont[field_no].d;
	}

	return max;
}

static long max_int_real(table *t, int field_no)
{
	int i;
	long max=0;

	for ( i=0; i < t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 ) {
			max = t->records[i]->cont[field_no].i;
			break;
		}
	}
		
	for ( i=i+1; i < t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 )
			if ( t->records[i]->cont[field_no].i > max )
				max = t->records[i]->cont[field_no].i;
	}

	return max;
}

/**
 * maximum
 * @param1: table
 * @param2: field number
 *
 * Description:
 * Search for the maximum value in the given field number from the given table.
 * It handles integers and reals.
 */
mstatic void stat_maximum (struct action_param params[2], int *dec)
{
	char message[1000];
	long result_int;
	float result_dbl;
	field *f;
	int count;

	f = &params[1].table->fields[params[1].field_no];
	
	count = count_real(params[0].table);
	
	switch ( f->type ) {
		case T_INTEGER:
		{
			result_int = max_int_real(params[0].table, 
					params[1].field_no);
			sprintf(message, _("The maximum is %ld."), result_int);
		} break;
		case T_DECIMAL:
		{
			result_int = max_int_real(params[0].table, 
					params[1].field_no);
			sprintf(message, _("The maximum is %f."),
							result_int / 100.0 );
		} break;
		case T_REAL:
		{
			result_dbl = max_double_real(params[0].table, 
					params[1].field_no);
			sprintf(message, _("The maximum is %f."), result_dbl);
		} break;
		default:
		{
			(*dec)--;
			return;
		} break;
	}
	
	gaby_message = g_strdup(message);
	gaby_errno = 1;
	(*dec)--;
}

static float min_double_real(table *t, int field_no)
{
	int i;
	float min=0;

	for ( i=0; i < t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 ) {
			min = t->records[i]->cont[field_no].d;
			break;
		}
	}
		
	for ( i=i+1; i < t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 )
			if ( t->records[i]->cont[field_no].d < min )
				min = t->records[i]->cont[field_no].d;
	}

	return min;
}

static long min_int_real(table *t, int field_no)
{
	int i;
	long min=0;

	for ( i=0; i < t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 ) {
			min = t->records[i]->cont[field_no].i;
			break;
		}
	}
		
	for ( i=i+1; i < t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 )
			if ( t->records[i]->cont[field_no].i < min )
				min = t->records[i]->cont[field_no].i;
	}

	return min;
}

/**
 * minimum
 * @param1: table
 * @param2: field number
 *
 * Description:
 * Search for the minimum value in the given field number from the given table.
 * It handles integers and reals.
 */
mstatic void stat_minimum (struct action_param params[2], int *dec)
{
	char message[1000];
	long result_int;
	float result_dbl;
	field *f;
	int count;

	f = &params[1].table->fields[params[1].field_no];
	
	count = count_real(params[0].table);
	
	switch ( f->type ) {
		case T_INTEGER:
		{
			result_int = min_int_real(params[0].table, 
					params[1].field_no);
			sprintf(message, _("The minimum is %ld."), result_int);
		} break;
		case T_DECIMAL:
		{
			result_int = min_int_real(params[0].table, 
					params[1].field_no);
			sprintf(message, _("The minimum is %f."),
						result_int / 100.0 );
		} break;
		case T_REAL:
		{
			result_dbl = min_double_real(params[0].table, 
					params[1].field_no);
			sprintf(message, _("The minimum is %f."), result_dbl);
		} break;
		default:
		{
			(*dec)--;
			return;
		} break;
	}
	
	gaby_message = g_strdup(message);
	gaby_errno = 1;
	(*dec)--;
}

