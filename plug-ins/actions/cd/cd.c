/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <tables.h>
#include <f_config.h>
#include <gtk_config_dlg.h>

/* action: import_cddb */

mstatic void cd_import_cddb (struct action_param table[2], int *dec);

#ifndef FOLLOW_MIGUEL
void get_function_by_name(gchar *name, action *a)
{
	a->function = NULL;
	if ( strcmp(name, "import_cddb") == 0 )
		a->function = cd_import_cddb;
}
#endif /* ! FOLLOW_MIGUEL */

static void import_cddb_real(char *filename, table *cds, table *tracks)
{
	FILE *f;
	record *r_cd;
	record *r_track;
	char title[100];
	record *r[100];
	int i=0, nbt;
	int frame1, frame2, len;
	int duration;

#ifdef DEBUG_GABY
	debug_print("[import_cddb_real] filename: %s - cds: %p, tracks: %p\n", 
			filename, cds, tracks);
#endif
	
	f = fopen(filename, "r");
	if ( f == NULL ) {
		gaby_message = g_strdup(filename);
		gaby_errno = FILE_READ_ERROR;
		gaby_perror_in_a_box();
		return;
	}

	r_cd = g_new0(record, 1);
	r_cd->id = 0;
	r_cd->cont = g_new0(union data, cds->nb_fields);
	
	fgets(title, 99, f);
	while ( ! feof(f) ) {
		if ( strncmp(title, "DTITLE=", 7) == 0 ) break;
		fgets(title, 99, f);
		title[strlen(title)-1] = 0;
	}
	r_cd->cont[0].str = g_string_new(title+7);	/* title*/
	r_cd->cont[1].str = 0;				/* author */
	r_cd->cont[2].str = g_string_new("");		/* genre */
	r_cd->cont[3].str = g_string_new("");		/* company */
	r_cd->cont[4].str = g_string_new("");		/* remarks */
	record_add(cds, r_cd, FALSE, TRUE);
	
	fgets(title, 99, f);
	while ( ! feof(f) ) {
		if ( strncmp(title, "TTITLE", 6) == 0 ) {
			if ( strchr(title, '\n') ) strchr(title, '\n')[0] = 0;
#ifdef DEBUG_GABY
			debug_print("track : %s\n", title);
#endif
			r_track = g_new0(record, 1);
			r_track->id = 0;
			r_track->cont = g_new0(union data, tracks->nb_fields);
			r_track->cont[0].i = atoi(title+6);
			r_track->cont[1].str = g_string_new( 
							strchr(title, '=')+1);
			r_track->cont[2].str = g_string_new("");
			r_track->cont[3].i = r_cd->id;
			record_add(tracks, r_track, FALSE, TRUE);
			r[i++] = r_track;
		}
		fgets(title, 99, f);
	}
	rewind(f);
	nbt = i;
	do {
		fgets(title, 99, f);
	} while ( strcmp(title, "# Track frame offsets:\n") != 0 );
	
	/* track durations
	 * the trick is :
	 *  begin : value/75 seconds + value%75 frames
	 * and you do this till you arrive on an empty line
	 * that is followed with 'Disc length'. Since you have
	 * the disc length and the length of every tracks minus
	 * the last one you get this one too. Easy. :)
	 */
	len = 0;
	frame2 = 0;
	for (i=0; i<nbt; i++) {
		fgets(title, 99, f);
		frame1 = frame2;
		frame2 = atoi(title+1);
		if ( i == 0 ) continue;
		duration = frame2 - frame1;
		duration /= 75;
#ifdef DEBUG_GABY
		debug_print("[import_cddb_real] track%d length: %d\n", 
				i, duration );
#endif
		g_string_sprintf(r[i-1]->cont[2].str, "%d:%02d", 
						duration/60, duration%60 );
	}
	fgets(title, 99, f);
	fgets(title, 99, f);
	duration = (atoi(strchr(title, ':')+1)*75 - frame2)/75;
#ifdef DEBUG_GABY
	debug_print("[import_cddb_real] track%d length: %d\n", i, duration );
#endif
	g_string_sprintf(r[i-1]->cont[2].str, "%d:%02d", 
						duration/60, duration%60 );
	fclose(f);
}

static void close_dialog(GtkWidget *button, GtkWidget *dialog)
{
	int *dec = gtk_object_get_data(GTK_OBJECT(dialog), "dec");

	gtk_widget_destroy(dialog);
	(*dec)--;
}

static void import_ok(GtkWidget *button, GtkWidget *dialog)
{
	table *cds, *tracks;
	GtkCList *clist = gtk_object_get_data(GTK_OBJECT(dialog), "clist");
	int row;
	char *id, *title, *dir = NULL;
	char filename[PATH_MAX];

#ifdef DEBUG_GABY
	debug_print("[cddb:import_ok] --\n");
#endif
	if ( clist->selection == NULL ) return;

	cds = gtk_object_get_data(GTK_OBJECT(dialog), "cds");
	tracks = gtk_object_get_data(GTK_OBJECT(dialog), "tracks");

	row = GPOINTER_TO_INT(clist->selection->data);
#ifdef DEBUG_GABY
	debug_print("[cddb:import_ok] row : %d\n", row);
#endif
	gtk_clist_get_text(clist, row, 0, &id);
	gtk_clist_get_text(clist, row, 1, &title);
	gtk_clist_get_text(clist, row, 2, &dir);
	
#ifdef DEBUG_GABY
	debug_print("[cddb:import_ok] id : %p, title %p, dir : %p\n", 
			id, title, dir);
	debug_print("[cddb:import_ok] id : %s, title %s, dir : %s\n", 
			id, title, dir);
#endif
	sprintf(filename, "%s/%s", dir, id);
	import_cddb_real(filename, cds, tracks);
	tracks->updated = TRUE;
	close_dialog(button, dialog);
}

static void clist_fill_with_cddb_files(GtkCList *clist, gchar *dirname)
{
	char filename[PATH_MAX];
	DIR *d;
	FILE *f;
	struct dirent *ent;
	gchar *row[3];
	gchar id[9];
	gchar title[100];

	row[0] = id;
	row[2] = dirname;

#ifdef DEBUG_GABY
	debug_print("[cfwcf] scanning cddb records from %s\n", dirname);
#endif
	d = opendir(dirname);
	if ( d == NULL ) return;
	while ( 1 ) {
		ent = readdir(d);
		if ( ent == NULL ) break;
		if ( strlen(ent->d_name) != 8 ) continue;
		strcpy(id, ent->d_name);
		sprintf(filename, "%s/%s", dirname, ent->d_name );
		f = fopen(filename, "r");
		if ( f == NULL ) continue;
		fgets(title, 99, f);
		while ( ! feof(f) ) {
			if ( strncmp(title, "DTITLE=", 7) == 0 ) break;
			fgets(title, 99, f);
			title[strlen(title)-1] = 0;
		}
		if ( ! feof(f) ) {
			row[1] = title+7;
#ifdef DEBUG_GABY
			debug_print("[cd:cfwcf] adding %s %s\n", row[0], row[1]);
#endif
			gtk_clist_append(clist, row);
		}
		fclose(f);
	}

	closedir(d);
	
	
}

/**
 * import_cddb
 * @param1: table 'CDs'
 * @param2: table 'Tracks'
 *
 * Description:
 * Loads the information provided by a cddb file (local only)
 */
mstatic void cd_import_cddb (struct action_param table[2], int *dec)
{
	GtkWidget *dialog;
	GtkWidget *vbox, *label, *sclwin, *clist;
	char dirname[PATH_MAX];
	DIR *d;
	struct dirent *ent;
	
	if ( table[0].table == NULL || table[1].table == NULL ||
	 		strcmp(table[0].table->name, "CDs") != 0 || 
			strcmp(table[1].table->name, "Tracks") != 0 ) {
#ifdef DEBUG_GABY
		debug_print("[load_cddb] wrong tables provided\n");
#endif
		(*dec)--;
		return;
	}
	
#ifndef USE_GNOME
	gaby_errno = ONLY_IN_GNOME;
	gaby_perror_in_a_box();
	(*dec)--;
	return;
#endif
	
#ifdef USE_GNOME
	dialog = gnome_dialog_new(_("cddb importing"), 
				GNOME_STOCK_BUTTON_OK, 
				GNOME_STOCK_BUTTON_CANCEL, 
				NULL );
	vbox = GNOME_DIALOG(dialog)->vbox;
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, 
				GTK_SIGNAL_FUNC(import_ok), dialog );
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, 
				GTK_SIGNAL_FUNC(close_dialog), dialog );
#endif
	label = gtk_label_new("Available cddb entries :");
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0 );
	gtk_widget_show(label);

	sclwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sclwin), 
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_widget_show(sclwin);
	
	clist = gtk_clist_new(3);
	gtk_clist_set_column_title(GTK_CLIST(clist), 0, _("ID") );
	gtk_clist_set_column_auto_resize(GTK_CLIST(clist), 0, TRUE);
	gtk_clist_set_column_title(GTK_CLIST(clist), 1, _("Title") );
	gtk_clist_set_column_title(GTK_CLIST(clist), 2, "dir" );
	gtk_clist_set_column_visibility(GTK_CLIST(clist), 2, FALSE);
	gtk_clist_set_column_auto_resize(GTK_CLIST(clist), 1, TRUE);
	gtk_clist_column_titles_show (GTK_CLIST(clist));
	
	/* TODO (post-2.0): let the user set the cddb directory
	 *  this should be a configuration option or I need a 'set
	 *  cddb directory' button in this dialog.
	 *
	 *  delayed since not critical and using sensible default value
	 */
	sprintf(dirname, "%s/.cddb", g_get_home_dir());
	clist_fill_with_cddb_files(GTK_CLIST(clist), dirname);
	d = opendir("/var/lib/cddb");
	if ( d != NULL ) {
		while ( 1 ) {
			ent = readdir(d);
			if ( ent == NULL ) break;
			if ( ent->d_name[0] == '.' ) continue;
			sprintf(dirname, "/var/lib/cddb/%s", ent->d_name);
			clist_fill_with_cddb_files(GTK_CLIST(clist), dirname);
		}
	}
	closedir(d);
		
	gtk_clist_set_sort_column(GTK_CLIST(clist), 1);
	gtk_clist_sort(GTK_CLIST(clist));
	gtk_widget_show(clist);
	
	gtk_container_add(GTK_CONTAINER(sclwin), clist);
	gtk_widget_set_usize(sclwin, -1, 250);
	gtk_box_pack_start(GTK_BOX(vbox), sclwin, TRUE, TRUE, 0 );

	gtk_object_set_data(GTK_OBJECT(dialog), "clist", clist);
	gtk_object_set_data(GTK_OBJECT(dialog), "dec", dec );
	gtk_object_set_data(GTK_OBJECT(dialog), "cds", table[0].table);
	gtk_object_set_data(GTK_OBJECT(dialog), "tracks", table[1].table);
	gtk_widget_show(dialog);
}


