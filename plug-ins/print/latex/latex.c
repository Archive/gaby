/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <files.h>
#include <records.h>
#include <gtk_config_dlg.h>

static void latex_print(subtable *s, char *filename, FILE *file, int *dec);
static void latex_print_fast(subtable *s, char *filename, FILE *file, int *dec);

gboolean latex_init_print_plugin (PrintPluginData *ppd)
{
	ppd->print = latex_print;
	ppd->print_fast = latex_print_fast;
/*	
	ppd->cleanup = cleanup;
	ppd->name = g_string_new("latex");
	ppd->i18n_name = g_string_new(_("LaTeX"));
*/
#ifdef DEBUG_GABY
	debug_print("Initialization of print plugin '%s' done succesfully.\n",
			"LaTeX" );
#endif
	
	return TRUE;
}

static void latex_print_real(FILE *f, subtable *st, int *fields_no, int len);

static void close_dialog(GtkWidget *but, GtkWidget *dialog)
{
	int *dec = gtk_object_get_data(GTK_OBJECT(dialog), "dec");
#ifdef USE_GNOME
	gnome_dialog_close(GNOME_DIALOG(dialog));
#else
	gtk_widget_destroy(dialog);
#endif
	(*dec)--;
}

static gint cmp_int(int *a, int *b)
{
	return ( *a > *b );
}

static void latex_print_ok_callback(GtkWidget *but, GtkWidget *dialog)
{
	FILE *f;
	GtkWidget *clist = gtk_object_get_data(GTK_OBJECT(dialog), "list");
	GtkWidget *entry = gtk_object_get_data(GTK_OBJECT(dialog), "filename");
	subtable *st = gtk_object_get_data(GTK_OBJECT(dialog), "subtable");
	gchar *filename;
	GList *selection;
	int length;
	int *fields_no;
	char *text;
	int i;
	int action;
	char cmd[500];
	char tmp[PATH_MAX];
	char dir[PATH_MAX], old_dir[PATH_MAX];

	action = GPOINTER_TO_INT(gtk_object_get_data(
					GTK_OBJECT(dialog), "value" ));
	
#ifdef DEBUG_GABY
	debug_print("[latex_print_ok_callback] action : %d\n", action);
#endif

	filename = gtk_entry_get_text(GTK_ENTRY(entry));
	if ( strlen(filename) == 0 && action != 5 ) {
		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("You didn't select a file."));
		gaby_perror_in_a_box();
		return;
	}
	
	if ( action == 5 ) {
#ifdef HAVE_TMPNAM
		filename = tmpnam(tmp);
#else
		sprintf(tmp, "%s/gabyprint_XXXXXX", g_get_tmp_dir());
		filename = mktemp(tmp);
#endif
		strcpy(dir, g_get_tmp_dir());
	} else {
		strcpy(dir, filename);
		if ( strrchr(dir, '/') != NULL ) {
			strrchr(dir, '/')[0] = 0;
		}
	}

	if ( strrchr(filename, '.') != NULL ) {
		strrchr(filename, '.')[0] = 0;
	}

	selection = GTK_CLIST(clist)->selection;
	length = g_list_length(selection);
	
	if ( length == 0 ) {
		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("You didn't select fields you want."));
		gaby_perror_in_a_box();
		return;
	}

	fields_no = g_malloc(sizeof(int)*length);

	for ( i=0; i<length; i++ ) {
		fields_no[i] = GPOINTER_TO_INT(selection->data);
		selection = g_list_next(selection);
	}
	qsort(fields_no, length, sizeof(int), (int (*)())cmp_int);
	
	for ( i=0; i<length; i++ ) {
		gtk_clist_get_text(GTK_CLIST(clist), fields_no[i], 0, &text );
		fields_no[i] = subtable_get_field_no(st, text);
	}
	
	sprintf(cmd, "%s.tex", filename);
	
	f = fopen(cmd, "w");
	if ( f == NULL ) {
#ifdef DEBUG_GABY
		debug_print("Unable to create %s\n", cmd);
#endif
		gaby_errno = FILE_WRITE_ERROR;
		gaby_message = g_strdup(cmd);
		gaby_perror_in_a_box();
		close_dialog(but, dialog);
		return;
	}
	latex_print_real(f, st, fields_no, i);
	fclose(f);

	if ( action == 0 ) {
		gaby_errno = CUSTOM_MESSAGE;
		gaby_message = g_strdup(_("File created succesfully."));
		gaby_perror_in_a_box();
		close_dialog(but, dialog);
		return;
	}

	getcwd(old_dir, PATH_MAX);
	chdir(dir);
#ifdef DEBUG_GABY
	sprintf(cmd, "latex %s.tex --interaction batchmode", filename);
#else
	sprintf(cmd, "latex %s.tex --interaction batchmode 2&> /dev/null", filename);
#endif
	if ( system(cmd) != 0 ) {
		/* error */
		;
	}
	chdir(old_dir);
	
	sprintf(cmd, "%s.log", filename);
	remove(cmd);

	if ( action == 1 ) {
		gaby_errno = CUSTOM_MESSAGE;
		gaby_message = g_strdup(_("File created succesfully."));
		gaby_perror_in_a_box();
		close_dialog(but, dialog);
		return;
	}
	
	if ( action == 2 ) {
		close_dialog(but, dialog);		
		/* xdvi */
		sprintf(cmd, "xdvi %s.dvi &", filename);
#if 1
		if ( system(cmd) != 0 ) {
			/* error */
		}
#else
		if (fork()) return;
		setsid();
		if(execl("/bin/sh","sh","-c", cmd, NULL)==-1){exit(100);}
		exit(0);
#endif
		return;
	}

#ifdef DEBUG_GABY
	sprintf(cmd, "dvips -o %s.ps %s.dvi", filename, filename);
#else
	sprintf(cmd, "dvips -q -o %s.ps %s.dvi", filename, filename);
#endif
	if ( system(cmd) ) {
		/* error */
		;
	}
	
	if ( action == 3 ) {
		gaby_errno = CUSTOM_MESSAGE;
		gaby_message = g_strdup(_("File created succesfully."));
		gaby_perror_in_a_box();
		close_dialog(but, dialog);
		return;
	}
	
	if ( action == 4 ) {
		/* GhostView */
		sprintf(cmd, "gv %s.ps", filename);
		close_dialog(but, dialog);
#if 1
		if ( system(cmd) != 0 ) {
			/* error */
		}
#else
		if (fork()) return;
		setsid();
		if(execl("/bin/sh","sh","-c", cmd, NULL)==-1){exit(100);}
		exit(0);
#endif
		return;
	}

	/* lpr */
	sprintf(cmd, "lpr %s.ps", filename);
	if ( system(cmd) ) {
		/* error */
		;
	}
	sprintf(cmd, "%s.tex", filename);
	remove(cmd);
	sprintf(cmd, "%s.dvi", filename);
	remove(cmd);
	sprintf(cmd, "%s.ps", filename);
	remove(cmd);
	
	gaby_errno = CUSTOM_MESSAGE;
	gaby_message = g_strdup(_("File sent to the printer succesfully."));
	gaby_perror_in_a_box();
	close_dialog(but, dialog);
}

static void action_selected(GtkWidget *mi, GtkWidget *dialog)
{
	GtkWidget *filewid;
	
	gtk_object_set_data(GTK_OBJECT(dialog), "value",
			gtk_object_get_data(GTK_OBJECT(mi), "value"));
	
	filewid = gtk_object_get_data(GTK_OBJECT(dialog), "file_widget");
	
	if ( GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(mi), "value"))==5) {
		gtk_widget_set_sensitive(filewid, FALSE);
	} else {
		gtk_widget_set_sensitive(filewid, TRUE);
	}
}

static void item_up(GtkWidget *button, GtkWidget *clist)
{
	if ( GTK_CLIST(clist)->focus_row == 0 )
		return;

	gtk_clist_swap_rows(GTK_CLIST(clist), GTK_CLIST(clist)->focus_row,
			GTK_CLIST(clist)->focus_row-1);

	/* the focused row is set correctly in this case */
}

static void item_down(GtkWidget *button, GtkWidget *clist)
{
	if ( GTK_CLIST(clist)->focus_row == GTK_CLIST(clist)->rows-1 )
		return;

	gtk_clist_swap_rows(GTK_CLIST(clist), GTK_CLIST(clist)->focus_row,
			GTK_CLIST(clist)->focus_row+1);

	/* the focused row is not set correctly in this case */
	GTK_CLIST(clist)->focus_row = GTK_CLIST(clist)->focus_row+1;
}

static void latex_print(subtable *st, char *filename, FILE *file, int *dec)
{
	/* TODO: move fields selection to main
	 *  the fields selection should be moved in a common place since this
	 *  will be used by most of the print plug-ins
	 */
	
	GtkWidget *dialog, *vbox, *hbox, *entry, *label, *clist, *buttonbox;
	GtkWidget *om, *ommenu, *omitem;
	GtkWidget *button;
	int i;

#ifdef USE_GNOME
	dialog = gnome_dialog_new(_("LaTeX output"),
			GNOME_STOCK_BUTTON_OK,
			GNOME_STOCK_BUTTON_CANCEL,
			NULL );
	vbox = GNOME_DIALOG(dialog)->vbox;

	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0,
			GTK_SIGNAL_FUNC(latex_print_ok_callback), dialog);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1,
			GTK_SIGNAL_FUNC(close_dialog), dialog);
#else /* ! USE_GNOME */
	dialog = gtk_dialog_new();
	gtk_window_set_title(GTK_WINDOW(dialog), _("LaTeX output"));
	vbox = GTK_DIALOG(dialog)->vbox;
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 5 );

	button = gtk_button_new_with_label(_("OK"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
				button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
			GTK_SIGNAL_FUNC(latex_print_ok_callback), dialog);

	button = gtk_button_new_with_label(_("Cancel"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
				button, TRUE, TRUE, 0);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
			GTK_SIGNAL_FUNC(close_dialog), dialog);
		
#endif
	
	ommenu = gtk_menu_new();
	gtk_widget_show(ommenu);

	omitem = gtk_menu_item_new_with_label(_("Create LaTeX"));
	gtk_object_set_data(GTK_OBJECT(omitem), "value", GINT_TO_POINTER(0));
	gtk_signal_connect(GTK_OBJECT(omitem), "activate",
				GTK_SIGNAL_FUNC(action_selected), dialog );
	gtk_widget_show(omitem);
	gtk_menu_append(GTK_MENU(ommenu), omitem);
	omitem = gtk_menu_item_new_with_label(_("Create DVI"));
	gtk_object_set_data(GTK_OBJECT(omitem), "value", GINT_TO_POINTER(1));
	gtk_signal_connect(GTK_OBJECT(omitem), "activate",
				GTK_SIGNAL_FUNC(action_selected), dialog );
	gtk_widget_show(omitem);
	gtk_menu_append(GTK_MENU(ommenu), omitem);
	omitem = gtk_menu_item_new_with_label(_("Create DVI and run xdvi"));
	gtk_object_set_data(GTK_OBJECT(omitem), "value", GINT_TO_POINTER(2));
	gtk_signal_connect(GTK_OBJECT(omitem), "activate",
				GTK_SIGNAL_FUNC(action_selected), dialog );
	gtk_widget_show(omitem);
	gtk_menu_append(GTK_MENU(ommenu), omitem);
	omitem = gtk_menu_item_new_with_label(_("Create PostScript"));
	gtk_object_set_data(GTK_OBJECT(omitem), "value", GINT_TO_POINTER(3));
	gtk_signal_connect(GTK_OBJECT(omitem), "activate",
				GTK_SIGNAL_FUNC(action_selected), dialog );
	gtk_widget_show(omitem);
	gtk_menu_append(GTK_MENU(ommenu), omitem);
	omitem = gtk_menu_item_new_with_label(_("Create PostScript and run GhostView"));
	gtk_object_set_data(GTK_OBJECT(omitem), "value", GINT_TO_POINTER(4));
	gtk_signal_connect(GTK_OBJECT(omitem), "activate",
				GTK_SIGNAL_FUNC(action_selected), dialog );
	gtk_widget_show(omitem);
	gtk_menu_append(GTK_MENU(ommenu), omitem);
	omitem = gtk_menu_item_new_with_label(_("Send to printer"));
	gtk_object_set_data(GTK_OBJECT(omitem), "value", GINT_TO_POINTER(5));
	gtk_signal_connect(GTK_OBJECT(omitem), "activate",
				GTK_SIGNAL_FUNC(action_selected), dialog );
	gtk_widget_show(omitem);
	gtk_menu_append(GTK_MENU(ommenu), omitem);
	
	om = gtk_option_menu_new();
	gtk_object_set_data(GTK_OBJECT(dialog), "option_menu", om);
	gtk_option_menu_set_menu(GTK_OPTION_MENU(om), ommenu);
	gtk_widget_show(om);
	gtk_box_pack_start(GTK_BOX(vbox), om, TRUE, TRUE, 0);
	
	hbox = gtk_hbox_new(FALSE, 0);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
	label = gtk_label_new(_("Filename"));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);

#ifdef USE_GNOME
	entry = gnome_file_entry_new(NULL, _("Filename"));
	gtk_object_set_data(GTK_OBJECT(dialog), "filename",
			gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(entry)));
#else
	entry = gtk_entry_new();
	gtk_object_set_data(GTK_OBJECT(dialog), "filename", entry );
#endif
	gtk_object_set_data(GTK_OBJECT(dialog), "file_widget", entry);
	gtk_widget_show(entry);
	gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);

	clist = gtk_clist_new(1);
	gtk_object_set_data(GTK_OBJECT(dialog), "list", clist);
	gtk_widget_show(clist);
	gtk_clist_set_selection_mode(GTK_CLIST(clist), GTK_SELECTION_MULTIPLE);
	for ( i=0; i<st->nb_fields; i++) {
		if ( st->fields[i].type == T_RECORDS ) continue;
		if ( st->fields[i].type == T_MULTIMEDIA ) continue;
		gtk_clist_append(GTK_CLIST(clist), &st->fields[i].i18n_name);
	}

	gtk_box_pack_start(GTK_BOX(hbox), clist, TRUE, TRUE, 0);

	buttonbox = gtk_vbutton_box_new();
	gtk_widget_show(buttonbox);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(buttonbox),
			GTK_BUTTONBOX_SPREAD );
	gtk_box_pack_start(GTK_BOX(hbox), buttonbox, FALSE, FALSE, 5 );
	
#ifdef USE_GNOME
	button = gnome_stock_button(GNOME_STOCK_BUTTON_UP);
#else
	button = gtk_button_new_with_label(_("Up"));
#endif
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
				GTK_SIGNAL_FUNC(item_up), clist );
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(buttonbox), button, FALSE, FALSE, 0);
	
#ifdef USE_GNOME
	button = gnome_stock_button(GNOME_STOCK_BUTTON_DOWN);
#else
	button = gtk_button_new_with_label(_("Down"));
#endif
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
				GTK_SIGNAL_FUNC(item_down), clist );
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(buttonbox), button, FALSE, FALSE, 0);
	
	gtk_object_set_data(GTK_OBJECT(dialog), "subtable", st);
	gtk_object_set_data(GTK_OBJECT(dialog), "dec", dec);
	gtk_widget_show(dialog);
			
}

static void latex_print_fast(subtable *s, char *filename, FILE *file, int *dec)
{
	FILE *f;
	gboolean file_to_close = FALSE;
	int fields_no[3] = { 0, 1, 2 };
	char r_filename[PATH_MAX];

	if ( filename != NULL ) {
		if ( strlen(filename) < 5 ||
				strcmp(filename+strlen(filename)-4, ".tex")!=0){
			sprintf(r_filename, "%s.tex", filename);
		} else {
			strcpy(r_filename, filename);
		}
		strcpy(filename, r_filename);
		f = fopen(filename, "w");
		if ( f == NULL ) {
			f = stdout;
		} else {
			file_to_close = TRUE;
		}
	} else {
		f = file;
	}

	latex_print_real(f, s, fields_no,
			(s->nb_fields > 3 ) ? 3 : s->nb_fields);

	(*dec)--;
}

static void latex_print_real(FILE *f, subtable *st, int *fields_no, int len)
{
	GString *str;
	int i, j;
	record *r;

#ifdef DEBUG_GABY
	debug_print("[latex_print_real] -- (file : %p)\n", f);
#endif

	fputs("\\documentclass{article}\n\n", f);

	fputs("\\usepackage[T1]{fontenc}\n", f);
	fputs("\\usepackage{fullpage}\n", f);
	fputs("\\usepackage{longtable}\n", f);
	
	fputs("\n\\begin{document}\n\n", f);
	fprintf(f, "\\title{%s}\n", st->i18n_name);
	fputs("\\author{}\n", f);
	fputs("\\date{\\today}\n", f);
	
	fputs("\n\\maketitle\n", f);
	
	fprintf(f, "\n\\begin{longtable}[c]{|*{%d}{p{0.3\\linewidth}|}}\n", len);
	fputs("\\hline\n", f);
	for ( i=0; i < len-1; i++ ) {
		fprintf(f, "\\textbf{%s}", st->fields[fields_no[i]].i18n_name);
		fputs(" & ", f);
	}
	fprintf(f, "\\textbf{%s}", st->fields[fields_no[i]].i18n_name);
	fputs(" \\endfirsthead\n", f);
	
	fputs("\\hline\n", f);
	for ( i=0; i < len-1; i++ ) {
		fprintf(f, "\\textbf{%s}", st->fields[fields_no[i]].i18n_name);
		fputs(" & ", f);
	}
	fprintf(f, "\\textbf{%s}", st->fields[fields_no[i]].i18n_name);
	fputs(" \\\\\n", f);
	for ( i=0; i < len-1; i++ ) {
		fprintf(f, "\\textit{%s} & ", _("More"));
	}
	fprintf(f, "\\textit{%s} \\endhead\n", _("More"));
	fprintf(f, "\\multicolumn{%d}{|c|}{\\textit{%s$\\ldots$}} "\
			"\\\\ \\hline \\endfoot\n", len, _("To be continued"));
	fputs("\\endlastfoot\n", f);
	fputs("\\hline\n", f);
	
	for ( i=0; i < st->table->max_records; i++) {
		r = st->table->records[i];
		if ( r == NULL || r->id == 0 )
			continue;
		for (j=0; j<len-1; j++) {
			str = get_subtable_stringed_field(st, r, fields_no[j]);
			fprintf(f, str->str);
			fputs(" & ", f);
			g_string_free(str, 1);
		}
		str = get_subtable_stringed_field(st, r, fields_no[j]);
		fprintf(f, str->str);
		fputs(" \\\\ \\hline\n", f);
	}

	fputs("\\end{longtable}\n", f);
	fputs("\n\\end{document}\n", f);

}

