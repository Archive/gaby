/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <files.h>
#include <records.h>
#include <gtk_config_dlg.h>

static void html_print( subtable *s, char *filename, FILE *file, int *dec);
static void html_print_fast( subtable *s, char *filename, FILE *file, int *dec);

gboolean html_init_print_plugin (PrintPluginData *ppd)
{
	ppd->print = html_print;
	ppd->print_fast = html_print_fast;
/*	
	ppd->cleanup = cleanup;
	ppd->name = g_string_new("html");
	ppd->i18n_name = g_string_new(_("HTML"));
*/
#ifdef DEBUG_GABY
	debug_print("Initialization of print plugin '%s' done succesfully.\n",
			"HTML" );
#endif
	
	return TRUE;
}

static void html_print_ok_callback(GtkWidget *but, GtkWidget *dialog);

static gboolean html_print_top(FILE *f, subtable *st);
static gboolean html_print_list(FILE *f, gchar *filename, gboolean has_forms, 
				subtable *st, GtkCList *clist,gchar *fileforms);
static gboolean html_print_forms(FILE *f, gchar *filename, gboolean has_list, 
				 subtable *st, GtkCList *clist,gchar *filelist);
static gboolean html_print_bottom(FILE *f);

static void check_clicked(GtkToggleButton *check, GtkWidget *frame)
{
	gtk_widget_set_sensitive(frame, gtk_toggle_button_get_active(check));
}

static void item_up(GtkWidget *button, GtkWidget *clist)
{
	if ( GTK_CLIST(clist)->focus_row == 0 )
		return;

	gtk_clist_swap_rows(GTK_CLIST(clist), GTK_CLIST(clist)->focus_row, 
			GTK_CLIST(clist)->focus_row-1);
	
	/* the focused row is set correctly in this case */
}

static void item_down(GtkWidget *button, GtkWidget *clist)
{
	if ( GTK_CLIST(clist)->focus_row == GTK_CLIST(clist)->rows-1 )
		return;
	
	gtk_clist_swap_rows(GTK_CLIST(clist), GTK_CLIST(clist)->focus_row, 
			GTK_CLIST(clist)->focus_row+1);
	
	/* the focused row is not set correctly in this case */
	GTK_CLIST(clist)->focus_row = GTK_CLIST(clist)->focus_row+1;
}

static void create_one_side( gchar *type, GtkWidget *dialog, GtkWidget *table,
			     gint col, subtable *st, gboolean different_files,
			     char *filename)
{
	GtkWidget *check, *frame, *list, *vbox2, *hbox2, *label, *entry;
	GtkWidget *button;
	char str[30];
	int i;
	
	if ( strcmp(type, "list")==0) {
		sprintf(str, _("Create a list"));
	} else {
		sprintf(str, _("Create forms"));
	}
	check = gtk_check_button_new_with_label(str);
	sprintf(str, "check_%s", type);
	gtk_object_set_data(GTK_OBJECT(dialog), str, check);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), TRUE);
	gtk_widget_show(check);
	gtk_table_attach_defaults(GTK_TABLE(table), check, 0+col, 1+col, 0, 1);
	
	if ( strcmp(type, "list")==0) {
		sprintf(str, _("List"));
	} else {
		sprintf(str, _("Form"));
	}
	frame = gtk_frame_new(str);
	gtk_container_set_border_width(GTK_CONTAINER(frame), 5);
	gtk_widget_show(frame);
	gtk_table_attach_defaults(GTK_TABLE(table), frame, 0+col, 1+col, 1, 2);
	gtk_signal_connect(GTK_OBJECT(check), "clicked", 
			GTK_SIGNAL_FUNC(check_clicked), frame);
	vbox2 = gtk_vbox_new(FALSE, 5);
	gtk_container_set_border_width(GTK_CONTAINER(vbox2), 5);
	gtk_widget_show(vbox2);
	gtk_container_add(GTK_CONTAINER(frame), vbox2);
	label = gtk_label_new(_("Select the fields you want to print"));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox2), label, TRUE, TRUE, 0);
	
	if ( different_files ) {
		hbox2 = gtk_hbox_new(FALSE, 3);
		gtk_widget_show(hbox2);
		gtk_box_pack_start(GTK_BOX(vbox2), hbox2, TRUE, TRUE, 0);
		label = gtk_label_new(_("Filename :"));
		gtk_widget_show(label);
		gtk_box_pack_start(GTK_BOX(hbox2), label, TRUE, TRUE, 0);
		sprintf(str, "filename_%s", type);
#ifdef USE_GNOME
		entry = gnome_file_entry_new(NULL, _("Filename"));
		gtk_object_set_data(GTK_OBJECT(dialog), str, 
			gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(entry)));
#else
		entry = gtk_entry_new();
		gtk_object_set_data(GTK_OBJECT(dialog), str, entry);
#endif
		if ( filename != NULL && strcmp(type, "list") == 0 ) {
			gtk_entry_set_text(GTK_ENTRY(entry), filename );
		}
		gtk_widget_show(entry);
		gtk_box_pack_start(GTK_BOX(hbox2), entry, TRUE, TRUE, 0);
	}
	
	hbox2 = gtk_hbox_new(FALSE, 0);
	gtk_widget_show(hbox2);
	gtk_box_pack_start(GTK_BOX(vbox2), hbox2, TRUE, TRUE, 0);
	
	list = gtk_clist_new(1);
	sprintf(str, "list_%s", type);
	gtk_object_set_data(GTK_OBJECT(dialog), str, list);
	gtk_widget_show(list);
	gtk_clist_set_selection_mode(GTK_CLIST(list), GTK_SELECTION_MULTIPLE);
	gtk_box_pack_start(GTK_BOX(hbox2), list, TRUE, TRUE, 0);
	for ( i=0; i < st->nb_fields; i++ ) {
		if ( st->fields[i].type == T_RECORDS ) continue;
		if ( st->fields[i].type == T_MULTIMEDIA ) continue;
		gtk_clist_append(GTK_CLIST(list), &st->fields[i].i18n_name);
	}

	vbox2 = gtk_vbutton_box_new();
	gtk_widget_show(vbox2);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(vbox2), GTK_BUTTONBOX_SPREAD);
	gtk_box_pack_start(GTK_BOX(hbox2), vbox2, FALSE, FALSE, 5);
#ifdef USE_GNOME
	button = gnome_stock_button(GNOME_STOCK_BUTTON_UP);
#else
	button = gtk_button_new_with_label(_("Up"));
#endif
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
				GTK_SIGNAL_FUNC(item_up), list);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(vbox2), button, FALSE, FALSE, 0);
#ifdef USE_GNOME
	button = gnome_stock_button(GNOME_STOCK_BUTTON_DOWN);
#else
	button = gtk_button_new_with_label(_("Down"));
#endif
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
				GTK_SIGNAL_FUNC(item_down), list);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(vbox2), button, FALSE, FALSE, 0);
	
	if ( strcmp(type, "list")==0 && st->nb_fields > 3 ) {
		for (i=0; i<3; i++) {
			gtk_clist_select_row(GTK_CLIST(list), i, 0);
		}
	} else {
		gtk_clist_select_all(GTK_CLIST(list));
	}

}

static void close_dialog(GtkWidget *but, GtkWidget *dialog)
{
	int *dec = gtk_object_get_data(GTK_OBJECT(dialog), "dec");
#ifdef USE_GNOME
	gnome_dialog_close(GNOME_DIALOG(dialog));
#else
	gtk_widget_destroy(dialog);
#endif
	(*dec)--;
}

static void html_print( subtable *st, char *filename, FILE *file, int *dec)
{
	GtkWidget *dialog;
	GtkWidget *vbox, *hbox2, *table, *label, *entry;
#ifndef USE_GNOME
	GtkWidget *button;
#endif
	gboolean different_files;

	different_files = get_config_bool("print", "html", 
						"different_files", FALSE );

#ifdef USE_GNOME
	dialog = gnome_dialog_new(_("HTML output"), 
			GNOME_STOCK_BUTTON_OK, 
			GNOME_STOCK_BUTTON_CANCEL, 
			NULL );
	vbox = GNOME_DIALOG(dialog)->vbox;
	
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, 
			GTK_SIGNAL_FUNC(html_print_ok_callback), dialog);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, 
				GTK_SIGNAL_FUNC(close_dialog), dialog);
				
#else /* ! USE_GNOME */
	dialog = gtk_dialog_new();
	gtk_window_set_title(GTK_WINDOW(dialog), _("HTML output"));
	vbox = GTK_DIALOG(dialog)->vbox;

	gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);

	button = gtk_button_new_with_label(_("OK"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), 
					button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
			GTK_SIGNAL_FUNC(html_print_ok_callback), dialog);

	button = gtk_button_new_with_label(_("Cancel"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), 
					button, TRUE, TRUE, 0);
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
			GTK_SIGNAL_FUNC(close_dialog), dialog);
	
#endif
	gtk_object_set_data(GTK_OBJECT(dialog), "dec", dec);
	
	/* file != NULL happens when the caller opened a file for us, this
	 * means that we _can't_ write a file in different files
	 */
	if ( different_files == FALSE || file != NULL ) {
		hbox2 = gtk_hbox_new(FALSE, 0);
		gtk_widget_show(hbox2);
		gtk_box_pack_start(GTK_BOX(vbox), hbox2, TRUE, TRUE, 0);
		label = gtk_label_new(_("Filename"));
		gtk_widget_show(label);
		gtk_box_pack_start(GTK_BOX(hbox2), label, TRUE, TRUE, 0);
	
#ifdef USE_GNOME
		entry = gnome_file_entry_new(NULL, _("Filename"));
		gtk_object_set_data(GTK_OBJECT(dialog), "filename", 
			gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(entry)));
#else
		entry = gtk_entry_new();
		gtk_object_set_data(GTK_OBJECT(dialog), "filename", entry);
#endif
		if ( file != NULL ) {
			gtk_entry_set_text(GTK_ENTRY(entry), filename);
			gtk_entry_set_editable(GTK_ENTRY(entry), FALSE );
		} else {
			if ( filename != NULL ) {
				gtk_entry_set_text(GTK_ENTRY(entry), filename);
			}
		}
		gtk_widget_show(entry);
		gtk_box_pack_start(GTK_BOX(hbox2), entry, TRUE, TRUE, 0);
	}
	
	table = gtk_table_new(2, 2, FALSE);
	gtk_widget_show(table);
	gtk_box_pack_start(GTK_BOX(vbox), table, TRUE, TRUE, 0);

	create_one_side("list", dialog, table, 0, st, different_files,filename);
	create_one_side("form", dialog, table, 1, st, different_files,NULL);
	
	gtk_object_set_data(GTK_OBJECT(dialog), "subtable", st);
	gtk_object_set_data(GTK_OBJECT(dialog), "FILE", file);
	if ( file != NULL ) {
		gtk_object_set_data(GTK_OBJECT(dialog), "file", filename);
	}

	gtk_widget_show(dialog);
}

static void html_print_ok_callback(GtkWidget *but, GtkWidget *dialog)
{
	subtable *st;
	GtkWidget *wid;
	gchar *filename, *filename2;
	char real_filename[PATH_MAX], real_filename2[PATH_MAX];
	gboolean create_list;
	gboolean create_forms;
	FILE *file=NULL;
	FILE *file2=NULL;
	gboolean same = FALSE;

	file = gtk_object_get_data(GTK_OBJECT(dialog), "FILE");
	
	wid = gtk_object_get_data(GTK_OBJECT(dialog), "check_list");
	create_list = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(wid));
	wid = gtk_object_get_data(GTK_OBJECT(dialog), "check_form");
	create_forms = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(wid));
	
	if ( create_list == FALSE && create_forms == FALSE ) {
		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("You didn't choose what to print "
					  "(list and/or forms)."));
		gaby_perror_in_a_box();
		return;
	}

	st = gtk_object_get_data(GTK_OBJECT(dialog), "subtable");
	
	if ( file == NULL ) {
		wid = gtk_object_get_data(GTK_OBJECT(dialog), "filename");
		if ( wid == NULL ) {
			/* we have different files for list and forms */
			wid = gtk_object_get_data(GTK_OBJECT(dialog), 
							"filename_list");
			filename = gtk_entry_get_text(GTK_ENTRY(wid));
			wid = gtk_object_get_data(GTK_OBJECT(dialog), 
							"filename_form");
			filename2 = gtk_entry_get_text(GTK_ENTRY(wid));
			if ( strcmp(filename, filename2) == 0 ) {
				same = TRUE;
				filename2 = filename;
			}
		} else {
			same = TRUE;
			filename = gtk_entry_get_text(GTK_ENTRY(wid));
			filename2 = filename;
		}

		if ( same == TRUE && (strlen(filename) == 0 ||
					filename[strlen(filename)-1] == '/') ) {
			gaby_errno = CUSTOM_WARNING;
			gaby_message = g_strdup(_("You didn't select a file."));
		}
	
		if ( create_list && same == FALSE && 
				(strlen(filename) == 0 ||
					filename[strlen(filename)-1] == '/') ) {
			gaby_errno = CUSTOM_WARNING;
			gaby_message = g_strdup( 
				_("You didn't select a file for the list."));
		}

		if ( create_forms && same == FALSE && 
			(strlen(filename2)==0 || 
				filename[strlen(filename2)-1] == '/')){
			if ( gaby_errno == CUSTOM_WARNING ) {
				/* we don't have a filename for the list*/
				g_free(gaby_message);
				gaby_message = g_strdup( 
						_("You didn't select files "
						"for the list and the forms."));
			} else {
				gaby_errno = CUSTOM_WARNING;
				gaby_message = g_strdup( 
						_("You didn't select a file "
						  "for the forms."));
			}
		}

		if ( gaby_errno == CUSTOM_WARNING ) {
			gaby_perror_in_a_box();
			return;
		}
	} else {
		same = TRUE;
		filename = gtk_object_get_data(GTK_OBJECT(dialog), "file");
		filename2 = filename;
		file2 = file;
	}
	
	wid = gtk_object_get_data(GTK_OBJECT(dialog), "list_list");
	if ( create_list && g_list_length(GTK_CLIST(wid)->selection) == 0 ) {
		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("You want to print a list but you "
					  "didn't select any field for it."));
		gaby_perror_in_a_box();
		return;
	}
	
	wid = gtk_object_get_data(GTK_OBJECT(dialog), "list_form");
	if ( create_forms && g_list_length(GTK_CLIST(wid)->selection) == 0 ) {
		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("You want to print forms but you "
					  "didn't select any field for them."));
		gaby_perror_in_a_box();
		return;
	}
	
	
	if ( 	( strlen(filename) > 4 && 
		  strcmp(filename+strlen(filename)-4, ".htm")  == 0 ) || 
		( strlen(filename) > 5 &&
		  strcmp(filename+strlen(filename)-5, ".html") == 0 ) ) {
		strcpy(real_filename, filename);
	} else {
		sprintf(real_filename, "%s.html", filename);
	}
	
	if ( 	( strlen(filename2) > 4 && 
		  strcmp(filename2+strlen(filename2)-4, ".htm")  == 0 ) || 
		( strlen(filename2) > 5 &&
		  strcmp(filename2+strlen(filename2)-5, ".html") == 0 ) ) {
		strcpy(real_filename2, filename2);
	} else {
		sprintf(real_filename2, "%s.html", filename2);
	}

	if ( file == NULL ) {
#ifdef DEBUG_GABY
		debug_print("[print:html:hpoc] creating %s\n", real_filename);
#endif
		file = fopen(real_filename, "w");
		if ( file == NULL ) {
			gaby_errno = FILE_WRITE_ERROR;
			gaby_message = g_strdup(real_filename);
			gaby_perror_in_a_box();
			close_dialog(but, dialog);
			return;
		}
		if ( same == FALSE ) {
#ifdef DEBUG_GABY
			debug_print("[print:html:hpoc] creating %s\n", 
					real_filename2);
#endif
			file2 = fopen(real_filename2, "w");
			if ( file2 == NULL ) {
				gaby_errno = FILE_WRITE_ERROR;
				gaby_message = g_strdup(real_filename2);
				gaby_perror_in_a_box();
				close_dialog(but, dialog);
				return;
			}
		} else {
			file2 = file;
		}
	}
	
	if ( same == TRUE || create_list ) {
		html_print_top(file, st);
	}

	if ( same == FALSE && create_forms ) {
		html_print_top(file2, st);
	}

	if ( create_list ) {
		wid = gtk_object_get_data(GTK_OBJECT(dialog), "list_list");
		html_print_list(file, real_filename, create_forms, 
					st, GTK_CLIST(wid), real_filename2);
	}
	
	if ( create_forms ) {
		wid = gtk_object_get_data(GTK_OBJECT(dialog), "list_form");
		html_print_forms(file2, real_filename2, create_list, 
					st, GTK_CLIST(wid), real_filename);
	}
	
	if ( same == TRUE || create_list ) {
		html_print_bottom(file);
	}

	if ( same == FALSE && create_forms ) {
		html_print_bottom(file2);
	}

	if ( gtk_object_get_data(GTK_OBJECT(dialog), "FILE") == NULL ) {
		fclose(file);
		if ( same == FALSE ) fclose(file2);
	}

	gaby_errno = CUSTOM_MESSAGE;
	gaby_message = g_strdup(_("File correctly created."));
	gaby_perror_in_a_box();

	close_dialog(but, dialog);
}

static void html_print_fast( subtable *st, char *filename, FILE *file, int *dec)
{
	FILE *f = file;
	char *s = filename;
	gboolean file_to_close = FALSE;
	char r_filename[PATH_MAX];

	/* TODO: user customizable default way to print
	 *  this should use preferences to provide what the user want without
	 *  asking him/her (this could also be used for default in normal use)
	 */

	if ( filename != NULL ) {
		if ( strlen(filename) < 6 || 
				strcmp(filename+strlen(filename)-5,".html")!=0){
			sprintf(r_filename, "%s.html", filename);
		} else {
			strcpy(r_filename, filename);
		}
		strcpy(filename, r_filename);
		f = fopen(filename, "w");
		if ( f == NULL ) {
			f = stdout;
		} else {
			file_to_close = TRUE;
		}
	} else {
		f = file;
	}
	
	if ( ! html_print_top(f, st) ) {
		fputs(_("An error occured\n"), stderr);
		return;
	}
	html_print_list(f, s, TRUE, st, NULL, s);
	html_print_forms(f, s, TRUE, st, NULL, s);
	html_print_bottom(f);

	if ( file_to_close == TRUE ) {
		fclose(f);
	}

	(*dec)--;
}

#if 0
static gint cmp_int(gconstpointer a, gconstpointer b)
{
#ifdef DEBUG_GABY
	int ia, ib;
	ia = GPOINTER_TO_INT(a);
	ib = GPOINTER_TO_INT(b);

	debug_print("[cmp_int] %d <-> %d\n", ia, ib);
#endif
	return ( GPOINTER_TO_INT(a) > GPOINTER_TO_INT(b) );
}
#else
static int cmp_int(int *a, int *b)
{
#ifdef DEBUG_GABY
	debug_print("[cmp_int] %d <-> %d\n", *a, *b);
#endif
	return ( *a > *b );
}
#endif

static gboolean html_print_top(FILE *f, subtable *st)
{
	char str[200];
	
	fputs("<html>\n  <head>\n", f);
	sprintf(str, "    <title>%s</title>\n", st->i18n_name);
	fputs(str, f);
	fputs("  </head>\n  <body>\n",f);
	
	fputs("\n<h1>", f);
	fputs(st->i18n_name, f);
	fputs("</h1>\n", f);
	
	fputs("<!-- generated by gaby : start -->\n", f);
	
	return TRUE;
}

#ifdef DEBUG_GABY
static void show_this_one(gpointer data, gpointer user)
{
	debug_print("[show_this_one] row : %d\n", GPOINTER_TO_INT(data));
}
#endif

static gboolean html_print_list(FILE *f, gchar *filename, gboolean has_forms,
				subtable *st, GtkCList *clist, gchar *fileforms)
{
	GList *tmp;
	int i, j;
	record *r;
	gchar *text;
	GString *str;
	int *fields_no;
	int nb_fields;
	char ahref[PATH_MAX];
	property *p;
	int *formats;
	char vide[1];
	vide[0] = 0;

	if ( clist != NULL ) {
		tmp = clist->selection;
		/* we want the fields in the order the user wants them */
#ifdef DEBUG_GABY
		debug_print("[html_print_list] sorting the selection\n");
#endif

/*
		tmp = g_list_sort(tmp, cmp_int);
		tmp = g_list_first(tmp);
#ifdef DEBUG_GABY
		g_list_foreach(tmp, show_this_one, NULL);
#endif
*/

		nb_fields = g_list_length(tmp);
		fields_no = g_malloc(sizeof(int)*nb_fields);

		for (i=0; i<nb_fields; i++) {
			fields_no[i] = GPOINTER_TO_INT(tmp->data);
			tmp = g_list_next(tmp);
		}
		qsort(fields_no, nb_fields, sizeof(int), (int (*)())cmp_int);
		
#if 0
		/* this is bubble sort :)
		 *  totally inefficient but I was really tired ...
		 */ 
		fl = 0;
		do {
			t = 0;
			for ( j=0; j<nb_fields-1; j++ ) {
				if ( fields_no[j] > fields_no[j+1] ) {
#ifdef DEBUG_GABY
					debug_print("swap(%d, %d)\n", 
							fields_no[j], 
							fields_no[j+1]);
#endif
					t = fields_no[j];
					fields_no[j] = fields_no[j+1];
					fields_no[j+1] = t;
					fl = 1;
				}
			}
		} while ( fl );
#endif
		
#ifdef DEBUG_GABY
		for (i=0; i<nb_fields; i++) {
			debug_print("row : %d\n", fields_no[i]);
		}
#endif
		
		for (i=0; i<nb_fields; i++) {
			gtk_clist_get_text(clist, fields_no[i], 0, &text);
#ifdef DEBUG_GABY
			debug_print("[html_print_list] row : %d, text : %s\n", 
					fields_no[i], text);
#endif
			fields_no[i] = subtable_get_field_no(st, text);
#ifdef DEBUG_GABY
			debug_print("[html_print_list] fields_no[%d] = %d\n", 
								i,fields_no[i]);
#endif
		}
	} else {
		fields_no = g_malloc(sizeof(int)*3);
		for ( i=0; i<3; i++) {
			fields_no[i] = i;
		}
		i = ( st->nb_fields > 3 ) ? 3 : st->nb_fields;
		nb_fields = i;
	}
	
	formats = g_malloc(sizeof(int) * nb_fields );

	fputs("<table border=1>\n  <tr>", f);
	if ( has_forms ) {
		fputs("<th>", f);
		fputs(_("Form"), f);
	}
		
	for ( i=0; i < nb_fields; i++ ) {
		fputs("<th>", f);
		fputs(st->fields[fields_no[i]].i18n_name, f);
		p = field_get_property( 
			&st->table->fields[st->fields[fields_no[i]].no], 
				"format" );
		if ( p != NULL ) {
			text = p->val;
			if        ( strcmp(text, "email") == 0 ) {
				formats[i] = 1;
			} else if ( strcmp(text, "website") == 0 ) {
				formats[i] = 2;
			} else {
				formats[i] = 0;
			}
		} else {
			formats[i] = 0;
		}
	}
	fputs("\n", f);
	
	for ( i=0; i < st->table->max_records; i++) {
		r = st->table->records[i];
		if ( r == NULL || r->id == 0 )
			continue;
		fputs("  <tr>", f);
		if ( has_forms ) {
			if ( fileforms == NULL ) {
				text=vide;
			} else {
				if ( strchr(fileforms, '/') ) {
					text = strrchr(fileforms, '/')+1;
				} else {
					text = fileforms;
				}
			}
			sprintf(ahref, "<td><a href=\"%s#%d\">%s</a>", 
					text, r->id, _("click"));
			fputs(ahref, f);
		}
		for ( j=0; j < nb_fields; j++ ) {
			str = get_subtable_stringed_field(st, r, fields_no[j]);
			fputs("<td>", f);
			switch ( formats[j] ) {
				case 1: /* email */
				{
					fputs("<a href=\"mailto:", f);
					fputs(str->str, f);
					fputs("\">", f);
					fputs(str->str, f);
					fputs("</a>", f);
				} break;
				case 2: /* website */
				{
					fputs("<a href=\"", f);
					fputs(str->str, f);
					fputs("\">", f);
					fputs(str->str, f);
					fputs("</a>", f);
				} break;
				default:
				{
					fputs(str->str, f);
				} break;
			}
			g_string_free(str, 1);
		}
		fputs("\n", f);
	}

	fputs("</table>\n", f);
	
	g_free(formats);
	g_free(fields_no);

	return TRUE;
}

static gboolean html_print_forms(FILE *f, gchar *filename, gboolean has_list, 
				 subtable *st, GtkCList *clist, gchar *filelist)
{
	GList *tmp;
	int i, j;
	int *fields_no;
	int nb_fields;
	gchar *text;
	char ahref[PATH_MAX];
	GString *str;
	record *r;
	int *formats;
	property *p;
	
	if ( clist != NULL ) {
		tmp = clist->selection;
		
		nb_fields = g_list_length(tmp);
		fields_no = g_malloc(sizeof(int)*nb_fields);

		for (i=0; i<nb_fields; i++) {
			fields_no[i] = GPOINTER_TO_INT(tmp->data);
			tmp = g_list_next(tmp);
		}
		qsort(fields_no, nb_fields, sizeof(int), (int (*)())cmp_int);

		for ( i=0; i<nb_fields; i++ ) {
			gtk_clist_get_text(clist, fields_no[i], 0, &text);
			fields_no[i] = subtable_get_field_no(st, text);
			tmp = g_list_next(tmp);
		}

	} else {
		fields_no = g_malloc(sizeof(int)*st->nb_fields);
		for ( i=0; i<st->nb_fields; i++) {
			fields_no[i] = i;
		}
		i = st->nb_fields;
	}
	nb_fields = i;
	formats = g_malloc(sizeof(int) * nb_fields );
	
	for ( i=0; i < nb_fields; i++ ) {
		p = field_get_property( 
			&st->table->fields[st->fields[fields_no[i]].no], 
				"format" );
		if ( p != NULL ) {
			text = p->val;
			if        ( strcmp(text, "email") == 0 ) {
				formats[i] = 1;
			} else if ( strcmp(text, "website") == 0 ) {
				formats[i] = 2;
			} else {
				formats[i] = 0;
			}
		} else {
			formats[i] = 0;
		}
	}

	if ( has_list )
		fputs("\n<hr>\n", f);


	for ( i=0; i < st->table->max_records; i++) {
		r = st->table->records[i];
		if ( r == NULL || r->id == 0 )
			continue;
		
		fputs("\n<p>\n", f);
		if ( has_list ) {
			sprintf(ahref, "<a name=\"%d\"></a>\n", r->id );
			fputs(ahref, f);
		}
		
		for ( j=0; j < nb_fields; j++ ) {
			str = get_subtable_stringed_field(st, r, fields_no[j]);
			if ( str->len != 0 ) {
				fputs(st->fields[fields_no[j]].i18n_name, f);
				fputs(" : ", f);
				switch ( formats[j] ) {
					case 1: /* email */
					{
						fputs("<a href=\"mailto:", f);
						fputs(str->str, f);
						fputs("\">", f);
						fputs(str->str, f);
						fputs("</a>", f);
					} break;
					case 2: /* website */
					{
						fputs("<a href=\"", f);
						fputs(str->str, f);
						fputs("\">", f);
						fputs(str->str, f);
						fputs("</a>", f);
					} break;
					default:
					{
						fputs(str->str, f);
					} break;
				}
				fputs("<br>\n", f);
			}
			g_string_free(str, 1);
		}
		fputs("</p><hr>\n", f);
	}
	
	g_free(fields_no);
	g_free(formats);
	
	return TRUE;
}

static gboolean html_print_bottom(FILE *f)
{

	fputs("<!-- generated by gaby : end -->\n", f);
	fputs("  </body>\n</html>\n", f);
	
	return TRUE;
}



/* configuration functions */

static GtkWidget* configure_widget = NULL;
static void configure_save();

#ifdef FOLLOW_MIGUEL
GtkWidget* html_configure()
#else
GtkWidget* configure()
#endif
{
	GtkWidget *vbox;
	GtkWidget *check;

	vbox = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox);

	check = gtk_check_button_new_with_label( 
				_("List and forms in different files"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 
				get_config_bool("print", "html",
					"different_files", FALSE ) );
	gtk_signal_connect_object(GTK_OBJECT(check), "clicked", 
				GTK_SIGNAL_FUNC(gaby_property_box_changed), 
				GTK_OBJECT(vbox) );
	gtk_widget_show(check);
	gtk_box_pack_start(GTK_BOX(vbox), check, FALSE, FALSE, 0);
	gtk_object_set_data(GTK_OBJECT(vbox), "different_files", check);
	
	gtk_object_set_data(GTK_OBJECT(vbox), "cfg_save", configure_save);
	
	configure_widget = vbox;
	return vbox;
}

static void configure_save()
{
	GtkWidget *check;

	check = gtk_object_get_data(GTK_OBJECT(configure_widget), 
			"different_files");

	write_config_bool("print", "html", "different_files", 
			gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check)));

}

