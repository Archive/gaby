/*  Gaby
 *  Copyright (C) 2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <files.h>
#include <records.h>
#include <gtk_config_dlg.h>
#include <gtk_print_dlg.h>
#include <windows.h>

static void html2_print( subtable *s, char *filename, FILE *file, int *dec);
static void html2_print_fast( subtable *s, char *filename, FILE *file, int *dec);

struct html2_part {
	gboolean todo;
	char *filename;
	FILE *f;
	int *fields;
};
	
gboolean html2_init_print_plugin (PrintPluginData *ppd)
{
	ppd->print = html2_print;
	ppd->print_fast = html2_print_fast;
#ifdef DEBUG_GABY
	debug_print("Initialization of print plugin '%s' done succesfully.\n",
			"HTML (new)" );
#endif
	
	return TRUE;
}

static void html2_print_ok_callback(GtkWidget *but, GtkWidget *dialog);
static gboolean html2_real_print( subtable *st, GList *records,
				  struct html2_part *form,
				  struct html2_part *list);
static gboolean html2_dump_list ( subtable *st, GList *records,
				  struct html2_part *forms,
				  struct html2_part *list);
static gboolean html2_dump_forms( subtable *st, GList *records,
				  struct html2_part *forms,
				  struct html2_part *list);

static void close_dialog(GtkWidget *but, GtkWidget *dialog)
{
	int *dec = gtk_object_get_data(GTK_OBJECT(dialog), "dec");

#ifdef USE_GNOME
	gnome_dialog_close(GNOME_DIALOG(dialog));
#else
	gtk_widget_destroy(dialog);
#endif
	(*dec)--;
}

static GtkWidget* page_select_output()
{
	GtkWidget *table;
	GtkWidget *entry;
	GtkWidget *label;
	
	table = gtk_table_new(3, 2, FALSE);
	
	label = gtk_label_new(_("File for the list: "));
	gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 0, 1);
	label = gtk_label_new(_("File for the forms: "));
	gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 1, 2);

#ifdef USE_GNOME
	entry = gnome_file_entry_new(NULL, _("File for the list"));
	gtk_object_set_data(GTK_OBJECT(table), "list", 
			gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(entry)));
#else
	entry = gtk_entry_new();
	gtk_object_set_data(GTK_OBJECT(table), "list", entry);
#endif
	gtk_widget_show(entry);
	gtk_table_attach(GTK_TABLE(table), entry, 1, 2, 0, 1,
			GTK_EXPAND | GTK_FILL | GTK_SHRINK, GTK_SHRINK,
			0, 0);
#ifdef USE_GNOME
	entry = gnome_file_entry_new(NULL, _("File for the forms"));
	gtk_object_set_data(GTK_OBJECT(table), "forms", 
			gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(entry)));
#else
	entry = gtk_entry_new();
	gtk_object_set_data(GTK_OBJECT(table), "forms", entry);
#endif
	gtk_widget_show(entry);
	gtk_table_attach(GTK_TABLE(table), entry, 1, 2, 1, 2,
			GTK_EXPAND | GTK_FILL | GTK_SHRINK, GTK_SHRINK,
			0, 0);

	label = gtk_label_new(_("Note that both files may be the same"));
	gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 2, 2, 3);

	/* TODO: default value for filenames */

	gtk_widget_show(table);
	return table;
}

static gchar* fill_windows_list(GtkWidget *list, subtable *st)
{
	/* duplicated from view/filter/filter.c */
	/* diffs with the other version:
	 *  - this one returns the name of the first windows
	 */
	GtkWidget *its_parent, *li;
	view *v;
	GList *tmp = g_list_first(list_windows);
	gchar *name, *ret_name = NULL;
	gabywindow *window;

	gtk_list_clear_items(GTK_LIST(list), 0, -1);

	while ( tmp != NULL ) {
		window = tmp->data;
		tmp = g_list_next(tmp);
		if ( ! GTK_IS_OBJECT(window->widget) ) continue;
		its_parent = window->parent;
		if ( GTK_IS_WINDOW(its_parent) ) {
			v = window->view;
			name = window->name;
			if ( v->subtable->table == st->table &&
					v->type->capabilities & FILTERABLE ) {
				if ( ret_name == NULL ) ret_name = name;
				li = gtk_list_item_new_with_label(name);
				gtk_widget_show(li);
				gtk_container_add(GTK_CONTAINER(list), li);
			}
		}
	}

	return ret_name;
}

static GtkWidget* page_select_records(subtable *st)
{
	GtkWidget *vbox;
	GtkWidget *vbox2, *rb, *combo;
	gchar *name;

	vbox = gtk_vbox_new(FALSE, 0);

	rb = gtk_radio_button_new_with_label(NULL, _("Every records"));
	gtk_widget_show(rb);
	gtk_box_pack_start(GTK_BOX(vbox), rb, FALSE, FALSE, 0);

	vbox2 = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox2);
	gtk_box_pack_start(GTK_BOX(vbox), vbox2, FALSE, FALSE, 0);
	rb = gtk_radio_button_new_with_label_from_widget(
			GTK_RADIO_BUTTON(rb), _("Records showed in: "));
	gtk_widget_show(rb);
	gtk_box_pack_start(GTK_BOX(vbox2), rb, FALSE, FALSE, 0);
	combo = gtk_combo_new();
	gtk_widget_show(combo);
	gtk_box_pack_start(GTK_BOX(vbox), combo, FALSE, FALSE, 0);
	name = fill_windows_list(GTK_COMBO(combo)->list, st);
	if ( name == NULL ) {
		gtk_widget_set_sensitive(vbox2, FALSE);
	} else {
		gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(combo)->entry), name);
	}

	gtk_object_set_data(GTK_OBJECT(vbox), "radio", rb);
	gtk_object_set_data(GTK_OBJECT(vbox), "cb", combo);
	
	gtk_widget_show(vbox);
	return vbox;
}

static void html2_print( subtable *st, char *filename, FILE *file, int *dec)
{
	GtkWidget *dialog;
	GtkWidget *vbox;
#ifndef USE_GNOME
	GtkWidget *button;
#endif
	GtkWidget *notebook;
	GtkWidget *wid;
	int tab[] = { 0, 1, 2, -1 };

#ifdef USE_GNOME
	dialog = gnome_dialog_new(_("HTML output"), 
				GNOME_STOCK_BUTTON_OK, 
				GNOME_STOCK_BUTTON_CANCEL, 
				NULL );
	vbox = GNOME_DIALOG(dialog)->vbox;
	
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, 
			GTK_SIGNAL_FUNC(html2_print_ok_callback), dialog);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, 
				GTK_SIGNAL_FUNC(close_dialog), dialog);
#else /* ! USE_GNOME */
	dialog = gtk_dialog_new();
	gtk_window_set_title(GTK_WINDOW(dialog), _("HTML output"));
	vbox = GTK_DIALOG(dialog)->vbox;

	gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);

	button = gtk_button_new_with_label(_("OK"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), 
					button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
			GTK_SIGNAL_FUNC(html2_print_ok_callback), dialog);

	button = gtk_button_new_with_label(_("Cancel"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), 
					button, TRUE, TRUE, 0);
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
			GTK_SIGNAL_FUNC(close_dialog), dialog);
	
#endif
	gtk_object_set_data(GTK_OBJECT(dialog), "dec", dec);
	
	notebook = gtk_notebook_new();
	gtk_widget_show(notebook);

#define PAGE_FORMS	0
	wid = page_select_fields(st, _("Print the forms"), NULL);
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), wid, 
					gtk_label_new(_("Forms")));
#define PAGE_LIST	1
	wid = page_select_fields(st, _("Print the list"), tab);
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), wid, 
					gtk_label_new(_("List")));
#define PAGE_OUTPUT	2
	wid = page_select_output();
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), wid, 
					gtk_label_new(_("Output")));
#define PAGE_RECORDS	3
	wid = page_select_records(st);
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), wid, 
					gtk_label_new(_("Records")));
	gtk_widget_show(notebook);
	gtk_box_pack_start(GTK_BOX(vbox), notebook, TRUE, TRUE, 0);
	
	gtk_object_set_data(GTK_OBJECT(dialog), "notebook", notebook);

	gtk_object_set_data(GTK_OBJECT(dialog), "st", st);
	gtk_widget_show(dialog);
}

static void page_output_get_info(GtkWidget *wid, char *fforms, char *flist)
{
	GtkWidget *entryforms, *entrylist;

	entryforms = gtk_object_get_data(GTK_OBJECT(wid), "forms");
	entrylist  = gtk_object_get_data(GTK_OBJECT(wid), "list");

	strcpy(fforms, gtk_entry_get_text(GTK_ENTRY(entryforms)));
	strcpy(flist, gtk_entry_get_text(GTK_ENTRY(entrylist)));
}

static GList* page_records_get_info(GtkWidget *wid, subtable *st)
{
	GtkToggleButton *rb = gtk_object_get_data(GTK_OBJECT(wid), "radio");
	GtkCombo *combo = gtk_object_get_data(GTK_OBJECT(wid), "cb");
	gchar *text;
	gabywindow *win;
	condition *c;
	
	/* radio is the 'records showed in view:' one */

	if ( ! rb->active ) {
		/* == every records */
		return get_conditional_records_list(st, NULL);
	}

	/* records showed in view */
	text = gtk_entry_get_text(GTK_ENTRY(combo->entry));
	
	win = get_window_by_name(text);

	if ( win == NULL ) { /* has the window disappeared ? */
		return NULL;
	}
	
	debug_print("[html2:prgi] window->name: %s\n", win->name);
	
	c = gtk_object_get_data(GTK_OBJECT(win->widget), "condition");
	
	return get_conditional_records_list(st, c);
}


static void html2_print_ok_callback(GtkWidget *but, GtkWidget *dialog)
{
	GtkWidget *forms_page, *list_page, *output_page, *records_page;
	GtkNotebook *notebook;
	int *dec;
	gboolean print_forms, print_list;
	int *forms_fields, *list_fields;
	subtable *st;
	char file_forms[PATH_MAX], file_list[PATH_MAX];
	struct html2_part pform, plist;
	GList *records;
	gboolean res;
	
	/* get the variables */
	notebook = gtk_object_get_data(GTK_OBJECT(dialog), "notebook");
	st = gtk_object_get_data(GTK_OBJECT(dialog), "st");

	forms_page  = gtk_notebook_get_nth_page(notebook, PAGE_FORMS);
	list_page   = gtk_notebook_get_nth_page(notebook, PAGE_LIST);
	output_page = gtk_notebook_get_nth_page(notebook, PAGE_OUTPUT);
	records_page = gtk_notebook_get_nth_page(notebook, PAGE_RECORDS);

	forms_fields = g_new(int, st->nb_fields+1);
	list_fields = g_new(int, st->nb_fields+1);
	
	/* */
	page_select_fields_get_info(st, forms_page, &print_forms, forms_fields);
	page_select_fields_get_info(st, list_page, &print_list, list_fields);

	if ( ! ( print_forms || print_list ) ) {
		g_free(forms_fields);
		g_free(list_fields);

		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("You need to print something !"));
		gaby_perror_in_a_box();
		return;
	}
	
	page_output_get_info(output_page, file_forms, file_list);
#ifdef DEBUG_GABY
	debug_print("[hpoc] f1:%s, f2:%s\n", file_forms, file_list);
#endif
	if ( print_list && strlen(file_list) == 0 ) {
		g_free(forms_fields);
		g_free(list_fields);

		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("You forgot to select a filename for the list !"));
		gaby_perror_in_a_box();
		gtk_notebook_set_page(notebook, PAGE_OUTPUT);
		return;
	}
	
	if ( print_forms && strlen(file_forms) == 0 ) {
		g_free(forms_fields);
		g_free(list_fields);

		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("You forgot to select a filename for the forms !"));
		gaby_perror_in_a_box();
		gtk_notebook_set_page(notebook, PAGE_OUTPUT);
		return;
	}

	records = page_records_get_info(records_page, st);
#ifdef DEBUG_GABY
	if ( records ) {
		debug_print("first record: %d\n", 
				GPOINTER_TO_INT(records->data));
	} else {
		debug_print("no record !!!\n");
	}
#endif
	if ( records == NULL ) { /* no records */
		g_free(forms_fields);
		g_free(list_fields);

		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("No records would be printed !"));
		gaby_perror_in_a_box();
		gtk_notebook_set_page(notebook, PAGE_RECORDS);
		return;
	}

	/* we're now ready to output */
	pform.todo = print_forms;
	pform.filename = file_forms;
	pform.fields = forms_fields;
	plist.todo = print_list;
	plist.filename = file_list;
	plist.fields = list_fields;

	res = html2_real_print(st, records, &pform, &plist);
	
	g_free(forms_fields);
	g_free(list_fields);
	g_list_free(records);

	dec = gtk_object_get_data(GTK_OBJECT(dialog), "dec");
#ifdef USE_GNOME
	gnome_dialog_close(GNOME_DIALOG(dialog));
#else
	gtk_widget_destroy(dialog);
#endif
	(*dec)--;
}

static void html2_print_fast( subtable *s, char *filename, FILE *file, int *dec)
{
	(*dec)--;
}


#define HTML_HEADER
"<html>\n"\
"  <head>\n"\
"    <title>%s</title>\n"\
"  </head>\n"\
"  <body>\n\n"\
"    <h1>%s</h1>\n\n"

#define HTML_END
"  </body>\n"\
"</html>\n"

static FILE* html2_forms_template(subtable *st,
				  struct html2_part *fo, struct html2_part *li)
{
	FILE *f;

	f = fopen(fo->filename, "w+");
	if ( f == NULL ) {
		return NULL;
	}

	fprintf(f, HTML_HEADER, st->i18n_name, st->i18n_name);
	if ( li->todo ) {
		fputs("<!-- gaby list -->\n\n<hr>\n", f);
	}
	fputs("<!-- gaby forms -->\n\n", f);
	fputs(HTML_END, f);
	
	return f;
}

static FILE* html2_list_template(subtable *st, struct html2_part *li)
{
	FILE *f;

	f = fopen(li->filename, "w+");
	if ( f == NULL ) {
		return NULL;
	}

	fprintf(f, HTML_HEADER, st->i18n_name, st->i18n_name);
	fputs("<!-- gaby list -->\n\n<hr>\n", f);
	fputs(HTML_END, f);
	
	return f;
}

static gboolean html2_real_print( subtable *st, GList *records,
				  struct html2_part *forms,
				  struct html2_part *list)
{
	FILE *f_forms = NULL, *f_list = NULL;
	gboolean same = FALSE;
	
	debug_print("[html2_real_print] --\n");

	/* I wish I could code this function in Python :) */

	if ( forms->todo ) {
		f_forms = fopen(forms->filename, "r+");
		if ( f_forms == NULL ) {
			f_forms = html2_forms_template(st, forms, list);
		}
		if ( f_forms == NULL ) {
			return FALSE;
		}
	}

	if ( list->todo ) {
		if ( strcmp(forms->filename, list->filename) == 0 && f_forms ) {
			f_list = f_forms;
			same = TRUE;
		} else {
			f_list = fopen(list->filename, "r+");
			if ( f_list == NULL ) {
				f_list = html2_list_template(st, list);
			}
			if ( f_list == NULL ) {
				return FALSE;
			}
		}
	}

	list->f = f_list;
	forms->f = f_forms;

	if ( ! html2_dump_list(st, records, forms, list) ) {
		if ( f_list ) fclose(f_list);
		if ( f_forms && ! same ) fclose(f_forms);
		
		return FALSE;
	}

	if ( ! html2_dump_forms(st, records, forms, list) ) {
		if ( f_list ) fclose(f_list);
		if ( f_forms && ! same ) fclose(f_forms);
		
		return FALSE;
	}

	if ( f_list ) fclose(f_list);
	if ( f_forms && ! same ) fclose(f_forms);
		
	return TRUE;
}

static gboolean html2_dump_list ( subtable *st, GList *records,
				  struct html2_part *forms,
				  struct html2_part *list)
{
	char tmp[1000], *s;
	record *r;
	FILE *f;
	char relative_forms_filename[PATH_MAX];
	int curf;
	GString *str;

	if ( forms->todo ) {
		if ( strchr(forms->filename, '/') ) {
			strcpy( relative_forms_filename,
					strrchr(forms->filename, '/')+1 );
		} else {
			strcpy( relative_forms_filename, forms->filename);
		}
	}

#ifdef HAVE_TMPNAM
	s = tmpnam(tmp);
#else
	sprintf(tmp, "%s/gabyhtml_XXXXXX", g_get_tmp_dir());
	s = mktemp(tmp);
#endif
	debug_print("[html2] tmp list will be in %s\n", s);

	f = fopen(s, "w+");

	fputs("<table border=1>\n  <tr>", f);
	if ( forms->todo ) {
		fputs("<th>", f);
		fputs(_("Form"), f);
	}
		
	curf = 0;
	while ( list->fields[curf] != -1 ) {
		fputs("<th>", f);
		fputs(st->fields[ list->fields[curf] ].i18n_name, f);
		curf++;
	}
	fputs("\n", f);

	while ( records != NULL ) {
		if ( GPOINTER_TO_INT(records->data) == -1 )
			break; /* TODO: i think this is a legacy test */
		r = st->table->records[GPOINTER_TO_INT(records->data)];
		records = g_list_next(records);
		fputs("  <tr>", f);
		if ( forms->todo ) {
			char ahref[PATH_MAX*2];
			sprintf(ahref, "<td><a href=\"%s#%d\">%s</a>",
					relative_forms_filename,
					r->id, _("click"));
			fputs(ahref, f);
		}
		curf = 0;
		while ( list->fields[curf] != -1 ) {
			fputs("<td>", f);
			str = get_subtable_stringed_field(st, r, 
					list->fields[curf]);
			fputs(str->str, f);			
			g_string_free(str, 1);
			curf++;
		}
		fputs("\n", f);
	}

	fputs("</table>\n", f);

	fclose(f);
#ifndef DEBUG_GABY
	unlink(s);
#endif

	return TRUE;
}

static gboolean html2_dump_forms( subtable *st, GList *records,
				  struct html2_part *forms,
				  struct html2_part *list)
{
	char tmp[1000], *s;
	record *r;
	FILE *f;
	int curf;
	GString *str;

#ifdef HAVE_TMPNAM
	s = tmpnam(tmp);
#else
	sprintf(tmp, "%s/gabyhtml_XXXXXX", g_get_tmp_dir());
	s = mktemp(tmp);
#endif
	debug_print("[html2] tmp form will be in %s\n", s);

	f = fopen(s, "w+");

	fputs("<table border=1>\n  <tr>", f);
	
	while ( records != NULL ) {
		if ( GPOINTER_TO_INT(records->data) == -1 )
			break; /* TODO: i think this is a legacy test */
		
		r = st->table->records[GPOINTER_TO_INT(records->data)];
		records = g_list_next(records);
		
		curf = 0;
		fputs("<ul>\n", f);
		while ( forms->fields[curf] != -1 ) {
			str = get_subtable_stringed_field( st, r, 
							   forms->fields[curf]);
			if ( str->str[0] != 0 ) {
				sprintf(tmp, "<li>%s: %s</li>\n", 
					st->fields[forms->fields[curf]].i18n_name,
					str->str);
				fputs(tmp, f);
			}
			g_string_free(str, 1);
			curf++;
		}
		fputs("</ul>\n\n", f);
	}

	fclose(f);
#ifndef DEBUG_GABY
	unlink(s);
#endif

	return TRUE;
}

