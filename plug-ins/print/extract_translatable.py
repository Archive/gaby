#! /usr/bin/env python

import sys
import string

strs = []

mode = 0
if sys.argv[1][-3:] == 'xml':
	mode = 1

f = open(sys.argv[1])
lines = f.readlines()
f.close()
if mode == 0: # look for _("")
	for l in lines:
		t = string.find(l, '_(')
		if t == -1: continue
		if l[t+2] == '"':
			str = l [ t+3 : string.find(l, '"', t+3) ]
		else:
			str = l [ t+3 : string.find(l, "'", t+3) ]
		strs.append(str)
elif mode == 1: # ...
	for l in lines:
		t = string.find(l, 'label=')
		if t == -1: continue
		str = l [ t+7 : string.find(l, '"', t+8) ]
		strs.append(str)

for s in strs:
	print '_("%s")' % s

