"""
Gaby Merging print plug-in
Copyright (c) 2000 Frederic Peters
Released under the terms of the GPL

Status: not installed by default
"""


import sys
import string

d = gaby.PrintDict

_ = gaby.gettext

f = open(d['file'])
file = f.read()
f.close()

st = d['subtable']

if string.find(d['file'], '/') <> -1:
	origfile = d['file'][ string.rfind(d['file'], '/')+1 : ]
else:
	origfile = d['file']

for id, i in map(None, d['records'], range(len(d['records']))):
	r = st.get_record_no(id)
	f = open(d['dir'] + '/' + str(i) + origfile, 'w')
	
	file2 = file
	for nf in range(len(st.fields), 0, -1):
		s1 = 'field' + str(nf)
		s2 = r[nf-1]
		file2 = string.replace(file2, s1, s2)
		s1 = 'FIELD' + str(nf)
		s2 = string.upper(r[nf-1])
		file2 = string.replace(file2, s1, s2)
	f.write(file2)
	f.close()

