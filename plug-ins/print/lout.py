"""
Gaby Lout print plug-in
Copyright (c) 2000 Emmanuel Jeandel
Released under the terms of the GPL
"""

# the informations we want are in gaby.PrintDict

import sys
import os
import string
#sys.stderr = open('/dev/null', 'w')

d = gaby.PrintDict

_ = gaby.gettext

if len(d['file']) == 0:
	gaby.message_dialog(2, _('You should set a file'))
	raise 'Error'

if len(d['fields']) == 0:
	gaby.message_dialog(2, 
			_('It would be a good idea to print something...'))
	raise 'Error'

st = d['subtable']

if d['file'] == '-': # stdout
	d['file'] = '-.lout'

#print d['file'][-4:]

if   d['file'][-5:] == '.lout':
	loutfile = d['file']
	psfile    = ''
elif d['file'][-3:] == '.ps':
	loutfile = d['file'][:-2] + 'lout'
	psfile    = d['file']
else:
	loutfile = d['file'] + '.lout'
	psfile = ''

if '/' in d['file'][1:]:
	import string
	try:
		os.chdir( d['file'] [: string.rfind(d['file'], '/')] )
	except:
		gaby.message_dialog(3, _('Bad directory !') )
		raise 'Error'

try:
	if loutfile == '-.lout':
		f = sys.stdout
	else:
		f = open(loutfile, 'w')
except:
	gaby.message_dialog(3, 
			_('Impossible to open file: %s') % loutfile)
	raise 'Error'

f.write( """@SysInclude { tab }
@SysInclude { doc }
@Doc @Text @Begin
@Display @Heading { %s }
""" % st.i18n_name )

f.write('@DP\n @HAdjust @Tab above {yes} \n below {yes} \n between \
	 {single} \n side {double}\n  ' )

def escape_string(s):
	s = string.replace(s, '\\', '"\\\\"')
	s = string.replace(s, '{' , '"{"')
	s = string.replace(s, '}' , '"}"')
	s = string.replace(s, '&' , '"&"')
	s = string.replace(s, '#' , '"#"')
	s = string.replace(s, '/' , '"/"')
	s = string.replace(s, '^' , '"^"')
	s = string.replace(s, '~' , '"~"')
	return s

def letter(i):
	c = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[i]
	return c

f.write(' @Fmta { ')
for fi in d['fields'][:-1]:
	f.write('@Col @Heading %c ! ' % letter(fi) )
f.write('@Col @Heading %c }\n ' % letter(d['fields'][-1]) )

f.write(' @Fmtb { ')
for fi in d['fields'][:-1]:
	f.write('@Col lines @Break %c ! ' % letter(fi) )
f.write('@Col lines @Break %c }\n ' % letter(d['fields'][-1]) )

f.write(' { ')
f.write('@Rowa ');	
for fi in d['fields']:
	f.write('%c ' % letter(fi) )
	f.write('{ %s } ' % escape_string(st.i18n_fields[fi]) )

f.write('\n ' )

rs = []
for id in d['records']:
	rs.append(st.get_record_no(id))

if d['order'] <> -1:
	sortfield = d['order']
	def cmpfunc(x, y):
		if len(y[sortfield]) == 0: return 0
		if len(x[sortfield]) == 0: return 1
		return cmp(x[sortfield], y[sortfield])
	rs.sort( lambda x,y: cmpfunc(x, y) )

for r in rs:
	f.write('@Rowb ');	
	for fi in d['fields']:
		f.write('%c ' % letter(fi) )
		f.write('{ %s } ' % escape_string(r[fi]) )
	f.write('\n')

f.write('} \n @DP \n @End @Text \n')

f.close()

if len(psfile) > 0:
	rs = os.system( 'lout %s ' %loutfile + ' -o %s ' %  psfile )
	if rs <> 0:
		gaby.message_dialog(3, _('Lout failed with %s') % loutfile )
		raise 'Error'

if d['view_output'] == 1:
	if len(psfile) > 0:
		os.system("gv %s &" % psfile)
	elif len(loutfile) > 0:
		pass
else:
	gaby.message_dialog(1, _('%s created succesfully') % d['file'] )

