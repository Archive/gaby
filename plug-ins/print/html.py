"""
Gaby HTML print plug-in
Copyright (c) 2000 Frederic Peters
Released under the terms of the GPL
"""

# the informations we want are in gaby.PrintDict

import sys
import string
#sys.stderr = open('/dev/null', 'w')

d = gaby.PrintDict

_ = gaby.gettext

if len(d['forms']) != 0 and len(d['fileforms']) == 0:
	gaby.message_dialog(2, _('You should set a file for the forms'))
	raise 'Error'

if len(d['list'])  != 0 and len(d['filelist'] ) == 0:
	gaby.message_dialog(2, _('You should set a file for the list'))
	raise 'Error'

if len(d['list']) == 0 and len(d['form']) == 0:
	gaby.message_dialog(2, 
			_('It would be a good idea to print something...'))
	raise 'Error'

st = d['subtable']
rs = []
for id in d['records']:
	rs.append(st.get_record_no(id))

if d['order'] <> -1:
	sortfield = d['order']
	def cmpfunc(x, y):
		if len(y[sortfield]) == 0: return 0
		if len(x[sortfield]) == 0: return 1
		return cmp(x[sortfield], y[sortfield])
	rs.sort( lambda x,y: cmpfunc(x, y) )


if len(d['forms']):
	sf = []
	for r, i in map(None, rs, range(len(rs))):
		sf.append('<ul>\n')
		sf.append('<a name="r%d"></a>\n' % i)
		for f in d['forms']:
			if len(r[f]) == 0:
				continue
			s = string.replace(r[f], '\n', '<br>')
			sf.append('<li>%s: %s</li>\n' % \
						(st.i18n_fields[f], s))
		sf.append('</ul>\n\n')

if len(d['list']):
	sl = []
	sl.append('<table border=1>\n<tr>')
	if len(d['forms']) > 0:
		sl.append('<th>%s' % _('Form'))
	for f in d['list']:
		sl.append('<th>%s' % st.i18n_fields[f])
	sl.append('\n')
	if d['fileforms'] == d['filelist']:
		fforforms = ''
	else:
		fforforms = d['fileforms']
	for r, i in map(None, rs, range(len(rs))):
		sl.append('<tr>')
		if len(d['forms']) > 0:
			sl.append('<td><a href="%s#r%d">*</a>' % (fforforms,i) )
		for f in d['list']:
			s = string.replace(r[f], '\n', '<br>')
			sl.append('<td>%s' % s)
		sl.append('\n')
	sl.append('</table>\n\n')

done = 0
if d['fileforms'] == d['filelist']:
	try:
		if d['fileforms'] == '-':
			f = sys.stdout
		else:
			f = open(d['fileforms'], 'w')
	except:
		gaby.message_dialog(3, 
			_('Impossible to open file: %s') % d['fileforms'])
		raise 'Error'
	f.write( '<html><head><title>%s</title></head>\n<body>\n\n' % \
								st.i18n_name )
	f.writelines(sl)
	f.write('\n<hr>\n')
	f.writelines(sf)
	f.write( '\n</body></html>\n' )
	f.close()
	done = 1

if len(d['list']) <> 0 and not done:
	try:
		if d['filelist'] == '-':
			f = sys.stdout
		else:
			f = open(d['filelist'], 'w')
	except:
		gaby.message_dialog(3,
			_('Impossible to open file for the list') )
		raise 'Error'
	f.write( '<html><head><title>%s</title></head>\n<body>\n\n' % \
								st.i18n_name )
	f.writelines(sl)
	f.close()

if len(d['forms']) <> 0 and not done:
	try:
		if d['fileforms'] == '-':
			f = sys.stdout
		else:
			f = open(d['fileforms'], 'w')
	except:
		gaby.message_dialog(3, 
			_('Impossible to open file for the forms') )
		raise 'Error'
	f.write( '<html><head><title>%s</title></head>\n<body>\n\n' % \
								st.i18n_name )
	f.writelines(sf)
	f.close()


# TODO: message with the name(s) of the file(s)
gaby.message_dialog(1, _('HTML file(s) created succesfully') )

