"""
Gaby LaTeX print plug-in
Copyright (c) 2000 Frederic Peters
Released under the terms of the GPL
"""

# the informations we want are in gaby.PrintDict

import sys
import os
import string
#sys.stderr = open('/dev/null', 'w')

d = gaby.PrintDict

_ = gaby.gettext

if len(d['file']) == 0:
	gaby.message_dialog(2, _('You should set a file'))
	raise 'Error'

if len(d['fields']) == 0:
	gaby.message_dialog(2, 
			_('It would be a good idea to print something...'))
	raise 'Error'

st = d['subtable']

if d['file'] == '-': # stdout
	d['file'] = '-.tex'

#print d['file'][-4:]

if   d['file'][-4:] == '.tex':
	latexfile = d['file']
	dvifile   = ''
	psfile    = ''
	pdffile   = ''
elif d['file'][-4:] == '.dvi':
	latexfile = d['file'][:-3] + 'tex'
	dvifile   = d['file']
	psfile    = ''
	pdffile   = ''
elif d['file'][-3:] == '.ps':
	latexfile = d['file'][:-2] + 'tex'
	dvifile   = d['file'][:-2] + 'dvi'
	psfile    = d['file']
	pdffile   = ''
elif d['file'][-4:] == '.pdf':
	latexfile = d['file'][:-3] + 'tex'
	dvifile   = d['file'][:-3] + 'dvi'
	psfile    = d['file'][:-3] + 'ps'
	pdffile   = d['file']
else:
	latexfile = d['file'] + '.tex'
	dvifile = ''
	psfile = ''
	pdffile = ''

if '/' in d['file'][1:]:
	import string
	try:
		os.chdir( d['file'] [: string.rfind(d['file'], '/')] )
	except:
		gaby.message_dialog(3, _('Bad directory !') )
		raise 'Error'

try:
	if latexfile == '-.tex':
		f = sys.stdout
	else:
		f = open(latexfile, 'w')
except:
	gaby.message_dialog(3, 
			_('Impossible to open file: %s') % latexfile)
	raise 'Error'

f.write( """\\documentclass{article}
\\usepackage[T1]{fontenc}
\\usepackage{fullpage}
\\usepackage{longtable}

\\begin{document}
\\title{%s}
\\author{}
\\date{\\today}

\\maketitle
""" % st.i18n_name )

f.write('\\begin{longtable}[c]{|*{%d}{p{0.3\\linewidth}|}}\n' % \
							len(d['fields']) )
f.write('\\hline\n')

for fi in d['fields'][:-1]:
	f.write('\\textbf{%s} & ' % st.i18n_fields[fi] )
f.write('\\textbf{%s}' % st.i18n_fields[ d['fields'][-1] ] )
f.write('\\endfirsthead\n')
f.write('\\hline\n')

for fi in d['fields'][:-1]:
	f.write('\\textbf{%s} & ' % st.i18n_fields[fi] )
f.write('\\textbf{%s}' % st.i18n_fields[ d['fields'][-1] ] )
f.write('\\\\\n')

for fi in d['fields'][:-1]:
	f.write('\\textit{%s} & ' % _('More') )
f.write('\\textit{%s} \\endhead\n' % _('More') )

f.write('\\multicolumn{%d}{|c|}{\\textit{%s$\\ldots$}}' % \
				( len(d['fields']), _("To be continued") ) )
f.write('\\\\ \\hline \\endfoot\n\endlastfoot\n')

f.write('\\hline\n')

rs = []
for id in d['records']:
	rs.append(st.get_record_no(id))

if d['order'] <> -1:
	sortfield = d['order']
	def cmpfunc(x, y):
		if len(y[sortfield]) == 0: return 0
		if len(x[sortfield]) == 0: return 1
		return cmp(x[sortfield], y[sortfield])
	rs.sort( lambda x,y: cmpfunc(x, y) )

# this replaces characters with special meaning in LaTeX with their LaTeX
# versions (thanks to Hsiu-Khuern Tang <hktang@stanford.edu> for pointing this)
#
# (is this efficent to do this for each and every field or should I put
# everything in one long string and then call string.replace on it ?)
def escape_string(s):
	s = string.replace(s, '\\', '$\\backslash$ ')
	s = string.replace(s, '{' , '$\\{$ ')
	s = string.replace(s, '}' , '$\\}$ ')
	s = string.replace(s, '$' , '\\$ ')
	s = string.replace(s, '&' , '\\&')
	s = string.replace(s, '#' , '\\#')
	s = string.replace(s, '_' , '\\_')
	s = string.replace(s, '%' , '\\%')
	s = string.replace(s, '^' , "\\char'136")
	s = string.replace(s, '~' , "\\char'176")
	return s



for r in rs:
	for fi in d['fields'][:-1]:
		f.write('%s & ' % escape_string(r[fi]) )
	f.write('%s \\\\ \\hline\n' % escape_string( r[ d['fields'][-1] ] )  )


f.write('\\end{longtable}\n\\end{document}\n\n')

f.close()

if len(dvifile) > 0:
	rs = os.system( 'latex %s --interaction batchmode' % latexfile )
	if rs <> 0:
		gaby.message_dialog(3, _('LaTeX failed with %s') % latexfile )
		raise 'Error'
		
	if len(psfile) > 0:
		os.system( 'dvips %s -o %s' % ( dvifile, psfile) )
		if len(pdffile) > 0:
			os.system( 'ps2pdf %s' % psfile )


#for s in [ latexfile, dvifile, psfile, pdffile ]:
#	print '###', s

if d['view_output'] == 1:
	if len(pdffile) > 0:
		os.system("xpdf %s &" % pdffile)
	elif len(psfile) > 0:
		os.system("gv %s &" % psfile)
	elif len(dvifile) > 0:
		os.system("xdvi %s &" % dvifile)
	elif len(latexfile) > 0:
		pass
else:
	gaby.message_dialog(1, _('%s created succesfully') % d['file'] )

