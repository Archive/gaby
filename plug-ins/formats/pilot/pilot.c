/*  Gaby
 *  Copyright (C) 1998-2001 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <format_plugin.h>

gboolean pilot_load_file (struct location *loc)
{
	return TRUE;
}

gboolean pilot_save_file (struct location *loc)
{
	FILE *f;
	table *t;
	record *r;
	int i, j, k;
	GString *str;
	int field_nos[5] = { 0, 1, 9, 13 };
	
#ifdef DEBUG_GABY
	debug_print("Writing %s\n", loc->filename);
#endif

	f = fopen(loc->filename, "w");
	if ( f == NULL ) {
		gaby_errno = FILE_WRITE_ERROR;
		gaby_message = g_strdup(loc->filename);
		gaby_perror_in_a_box();
		return FALSE;
	}

	t = loc->table;

	/* records */
	for ( i=0; i < t->max_records; i++ ) {
		r = t->records[i];
		if ( r == NULL || r->id == 0 )
			continue;
		if ( loc->type != NULL && r->file_loc != loc )
			continue;

		for ( k=0; k < 4; k++ ) {
			j = field_nos[k];
			str = get_table_stringed_field(t, r, j);
			while ( strchr(str->str, '\n') )
				strchr(str->str, '\n')[0] = ' ';
			fprintf(f, "\"%s\",", str->str);
			g_string_free(str, 1);
		}
		fputs("\"0\"\n", f);

	}
	fclose(f);
	
	return TRUE;
}

