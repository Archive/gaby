/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <format_plugin.h>

gboolean addressbook_load_file(struct location *loc)
{
	FILE *f;
	char st[500];
	char *s;
	record *r;
	int i;
	static int lid=0;

#ifdef DEBUG_GABY
	debug_print("[addressbook:load_file] -- \n");
#endif

	if ( loc->table->nb_fields != 17 ) {
		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("addressbook format works currently only with standard desc.gaby.\n"));
		gaby_perror_in_a_box();
		return FALSE;
	}

	f = fopen(loc->filename, "r");
	if ( f == NULL ) {
		gaby_errno = FILE_READ_ERROR;
		gaby_message = g_strdup(loc->filename);
		gaby_perror_in_a_box();
		return FALSE;
	}

	fgets(st, 500, f);
	while ( ! feof(f) ) {
		if ( st[0] == '#' ) {
			/* I am not sure if it might happen */
			fgets(st, 500, f);
			continue;
		}
		r = g_malloc(sizeof(record));
		r->id = loc->offset + lid++;
		r->file_loc = loc;
		r->cont = g_new0(union data, loc->table->nb_fields);
								/* thanks Ron */
		s = st;
		for ( i=0; i < 15; i++ ) {
#ifdef DEBUG_GABY
			debug_print("Adding field %d\n", i);
#endif
			if ( loc->table->fields[i].type == T_STRING || 
				loc->table->fields[i].type == T_STRINGS ) {
				
				r->cont[i].str = g_string_new(s);
			} else {
				r->cont[i].anything = NULL;
			}
			
			if ( i != 14 && strchr(s, ';') == NULL ) {
				fgets(st, 500, f);
				break;
			}
			if ( i != 14 ) strchr(s, ';')[0] = 0;

#ifdef DEBUG_GABY
			debug_print("s is %s\n", s);
#endif
			switch ( i ) {
				case 0 : /* firstname */
				case 1 : /* lastname */
				case 2 : /* add on */
				case 3 : /* street */
				case 4 : /* country */
				case 5 : /* zip */
				case 6 : /* city */
				case 8 : /* phonepriv */
				case 9 : /* phonework */
				case 10 : /* fax */
				case 11 : /* email */
				case 12 : /* www */
				case 13 : /* category */
				case 14 : /* remark */
				{
					r->cont[i].str = g_string_assign( 
						r->cont[i].str, s);
				} break;
				case 7 : /* birthday */
				{
					r->cont[7].date = g_date_new();
					g_date_set_parse(r->cont[7].date, s);
					if  ( r->cont[7].date->month == 
							G_DATE_BAD_MONTH ) {
						g_date_free(r->cont[7].date);
						r->cont[7].date = NULL;
					}
				} break;
				default: break;
			}
			s += strlen(s) + 1;
		}
		
		if ( i != 15 ) continue;

		/* let's create state/province and cell phone */
		r->cont[6].str = g_string_new("");
		r->cont[11].str = g_string_new("");

#ifdef DEBUG_GABY
		debug_print("adding %p\n", r->cont[0].str->str);
#endif
		record_add(loc->table, r, FALSE, TRUE );
#ifdef DEBUG_GABY
		debug_print("%p added\n", r->cont[0].str->str);
#endif
				
		fgets(st, 500, f);
	}

	fclose(f);
	
	return TRUE;
}

gboolean addressbook_save_file(struct location *loc)
{
	table *t = loc->table;
	FILE *f;
	char st[500];
	char *s;
	int i, j;
	record *r;

#ifdef DEBUG_GABY
	debug_print("[addressbook:save_file] -- \n");
#endif

	if ( loc->table->nb_fields != 17 ) {
		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("addressbook format works currently only with standard desc.gaby.\n"));
		gaby_perror_in_a_box();
		return FALSE;
	}

	f = fopen(loc->filename, "w");
	if ( f == NULL ) {
		gaby_errno = FILE_WRITE_ERROR;
		gaby_message = g_strdup(loc->filename);
		gaby_perror_in_a_box();
		return FALSE;
	}

	for ( i=0; i<t->max_records; i++ ) {
		r = t->records[i];
		if ( r == NULL || r->id == 0 )
			continue;
		if ( loc->type != NULL && r->file_loc != loc )
			continue;

		s = st;
		
		for ( j=0; j<15; j++ ) {
			switch ( j ) {
				case 0: /* firstname */
				{
					if ( r->cont[0].str )
						sprintf(s,r->cont[0].str->str);
				} break;
				case 1: /* lastname */
				{
					if ( r->cont[1].str )
						sprintf(s,r->cont[1].str->str);
				} break;
				case 2: /* add on */
				{
					if ( r->cont[2].str )
						sprintf(s,r->cont[2].str->str);
				} break;
				case 3 : /* street */
				{
					if ( r->cont[3].str )
						sprintf(s,r->cont[3].str->str);
				} break;
				case 4 : /* country */
				{
					if ( r->cont[7].str )
						sprintf(s,r->cont[7].str->str);
				} break;
				case 5 : /* zip */
				{
					if ( r->cont[4].str )
						sprintf(s,r->cont[4].str->str);
				} break;
				case 6 : /* city */
				{
					if ( r->cont[5].str )
						sprintf(s,r->cont[5].str->str);
					if ( r->cont[6].str && 
							r->cont[6].str->len){
						s += strlen(s);
						sprintf(s, " / %s", 
							r->cont[6].str->str );
					}
				} break;
				case 7 : /* birthday */
				{
					if ( r->cont[8].date )
						sprintf(s,"%d.%d.%d", \
							r->cont[8].date->day, \
							r->cont[8].date->month,\
							r->cont[8].date->year \
							);
				} break;
				case 8 : /* phonepriv */
				{
					if ( r->cont[9].str )
						sprintf(s,r->cont[9].str->str);
				} break;
				case 9 : /* phonework */
				{
					if ( r->cont[10].str )
						sprintf(s,r->cont[10].str->str);
				} break;
				case 10 : /* fax */
				{
					if ( r->cont[12].str )
						sprintf(s,r->cont[12].str->str);
				} break;
				case 11 : /* email */
				{
					if ( r->cont[13].str )
						sprintf(s,r->cont[13].str->str);
				} break;
				case 12 : /* www */
				{
					if ( r->cont[14].str )
						sprintf(s,r->cont[14].str->str);
				} break;
				case 13 : /* category */
				{
					if ( r->cont[15].str )
						sprintf(s,r->cont[15].str->str);
				} break;
				case 14 : /* remark */
				{
					if ( r->cont[16].str ) {
						sprintf(s,r->cont[16].str->str);
						while ( strchr(s, '\n') )
							strchr(s, '\n')[0]=' ';
					}
				} break;
			}
			if ( j != 14 ) {
				s += strlen(s)+1;
				s[-1] = ';';
				s[0] = 0;
			}
		}
		st[strlen(st)+1] = 0;
		st[strlen(st)] = '\n';
		fputs(st, f);
	}
	
	fclose(f);

	return TRUE;
}


