/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <format_plugin.h>

#define APPINDEX_PREFIX "http://freshmeat.net/appindex/"
#define LINE_LEN	40960	/* said to be enough */

static gboolean load_db (FILE * f, table *t);
static FILE* makepipe (FILE *in, char *cmd, char *filename);

gboolean appindex_load_file (struct location *loc)
{
	FILE *f = NULL;
	char *extension = strrchr(loc->filename, '.');
	char sign[100];

	if ( extension == NULL ) return FALSE;

	if ( strcmp(extension, ".db") == 0 ) {
		f = fopen(loc->filename, "r");
	} else {
		f = fopen(loc->filename, "r");
		if ( strcmp(extension, ".bz2" ) == 0 ) {
			f = makepipe(f, "bzip2 -dc", loc->filename );
		}
		if ( strcmp(extension, ".gz" ) == 0 ) {
			f = makepipe(f, "gzip -dc", loc->filename );
		}
	}
	if ( f == NULL ) return FALSE;
	fgets(sign, 99, f);
	if ( strcmp(sign, "%appindex database%\n") != 0 ) {
		fclose(f);
		return FALSE;
	}
	return ( load_db(f, loc->table) );
}

extern char **environ;

static FILE* makepipe (FILE *in, char *cmd, char *filename)
{
	char *argv[4];
	int fds[2];
	char cmdline[0x100];
	FILE *f;

	pipe (fds);
	if (fork () == 0)
	{
		close (fds[0]);
		if (in)
			dup2 (fileno (in), 0);
		dup2 (fds[1], 1);
		close (fds[1]);
		argv[0] = "sh";
		argv[1] = "-c";
		strcpy (cmdline, cmd);
		if (filename)
		{
			strcat (cmdline, " ");
			strcat (cmdline, filename);
		}
		argv[2] = cmdline;
		argv[3] = 0;
		execve ("/bin/sh", argv, environ);
		_exit (127);
	}
	if (in)
		fclose (in);
	close (fds[1]);
	f = fdopen (fds[0], "r");
	return f;
}

static int getstr (FILE * db, char *string)
{
	guint16 len;

	if (fread (&len, 1, 2, db) != 2)
		return -1;
	if (len > LINE_LEN)
		return -1;
	if (fread (string, 1, len, db) != len)
		return -1;
	string[len] = 0;

	return len;
}

static gboolean load_db (FILE * f, table *t)
{
	char *line;
	record *r;

	line = (char *) g_malloc (LINE_LEN);
	if (!line)
		return FALSE;

	getstr (f, line);
	if (strcmp (line, "Version1"))
	{
		g_print ("Unrecognized database format.");
		return FALSE;
	}

	getstr (f, line);

	while (getstr (f, line) > 0)
	{
		r = g_new0(record, 1);
		r->cont = g_new0(union data, t->nb_fields);
		
		r->cont[0].str = g_string_new (line);

#define GET_ITEM(x) \
	if (getstr(f,line)==-1)  \
		return FALSE;  \
	set_table_stringed_field(t, r, x, line);
		GET_ITEM ( 2);	/* ver_stable */
		GET_ITEM ( 3);	/* ver_devel */
		GET_ITEM ( 1);	/* category */
		GET_ITEM ( 4);	/* license */
		GET_ITEM ( 5);	/* homepage */
		GET_ITEM ( 6);	/* download */
		GET_ITEM ( 7);	/* changelog */
		GET_ITEM ( 8);	/* deb */
		GET_ITEM ( 9);	/* rpm */
		GET_ITEM (10);	/* link */
		GET_ITEM (11);	/* oneliner */
		GET_ITEM (12);	/* description */
		GET_ITEM (13);	/* author */
		GET_ITEM (14);	/* coauthor */
		GET_ITEM (15);	/* dependancy */
#undef GET_ITEM
		record_add(t, r, FALSE, TRUE);
		/* Skip further items */
		do { getstr (f, line); } while (line[0] != 0);
	}

	g_free(line);
	
	fclose(f);

	return TRUE;
}

gboolean appindex_save_file (struct location *loc)
{
	return FALSE;
}

