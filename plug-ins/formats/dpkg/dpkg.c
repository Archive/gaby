/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <format_plugin.h>

gboolean dpkg_load_file (struct location *loc)
{
	table *t = loc->table;
	FILE *f;
	char s[200];
	record *r;
	int index=1;
	int i;
	char *str_fields[] = {
		"Package: ",
		"Priority: ",
		"Section: ",
		"Installed-Size: ",
		"Maintainer: ",
		"Architecture: ",
		"Source: ",
		"Version: ",
		"Replaces: ",
		"Depends: ",
		"Recommends: ",
		"Suggests: ",
		"Conflicts: ",
		"Filename: ",
		"Size: ",
		"MD5sum: ",
		"Description: ",
	};
	int nb_fields = sizeof(str_fields)/sizeof(char*);
	GString *cur_str;
	
#ifdef DEBUG_GABY
	debug_print("Loading %s\n", loc->filename);
#endif

	if ( strcmp(t->name, "Packages") != 0 ) {
		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("dpkg format works only with standard gaby-apt."));
		gaby_perror_in_a_box();
		return FALSE;
	}
	
	f = fopen(loc->filename, "r" );
	if ( f == NULL ) {
		gaby_errno = FILE_READ_ERROR;
		gaby_message = g_strdup(loc->filename);
		gaby_perror_in_a_box();
		return FALSE;
	}

	fgets(s, 200, f);
#ifdef DEBUG_GABY
#if 0	/* this seems to be fixed */
	g_print("I will now write a lot of dots, if gaby segfaults before\n");
	g_print("the last one (there's one space before it), you should \n");
	g_print("try to run gaby-apt as 'export MALLOC_CHECK_=0; ./gaby-apt'\n");
	g_print("and sending me a bug report with your libc, glib, gtk, ...\n");
	g_print("version numbers (and gaby's version, of course).\n");
	g_print("Thanks, here comes the dots : ");
#endif
#endif
	while ( ! feof(f) ) {
#ifdef DEBUG_GABY
#if 0
	putchar('.'); fflush(stdout);
#endif
#endif
		while ( strncmp(s, "Package: ", 9) != 0 && ! feof(f) ) {
			fgets(s, 200, f);
		}
		
		if ( feof(f) ) break;

		r = g_new(record, 1);
		r->file_loc = loc;
		r->id = loc->offset + index++;
		r->cont = g_new0(union data, t->nb_fields);
		for ( i=0; i<t->nb_fields; i++) {
			if ( t->fields[i].type == T_STRING || 
					t->fields[i].type == T_STRINGS) {
				r->cont[i].str = g_string_new("");
			} else {
				r->cont[i].anything = NULL;
			}
		}

		while ( 1 ) {
			i = 0;
			while (	i < nb_fields && strncmp(str_fields[i], s, 
					strlen(str_fields[i])) != 0 ) i++;
			if ( i == nb_fields ) {
				fgets(s, 200, f);
				continue;
			}
			s[strlen(s)-1] = 0;
			cur_str = r->cont[i].str;
			cur_str = g_string_assign(cur_str, 
					s+strlen(str_fields[i]));
			if ( i == nb_fields-1 ) {
				/* loading long description */
				cur_str = r->cont[i+1].str;
				
				fgets(s, 200, f);
				do {
					s[strlen(s)-1] = 0;
					if ( s[1] == '.' ) {
						cur_str = g_string_append( 
								cur_str, "\n");
					} else {
						cur_str = g_string_append( 
								cur_str, s);
					}
					fgets(s, 200, f);
				} while ( strlen(s) > 1 );
				break;
			}
			fgets(s, 200, f);
		}
		record_add(t, r, FALSE, TRUE);
	}
#if DEBUG_GABY
	debug_print(" . wow, it seems to work !\n");
#endif
	fclose(f);
	
	return TRUE;
}

gboolean dpkg_save_file (struct location *loc)
{
	gaby_errno = CUSTOM_MESSAGE;
	gaby_message = g_strdup(_("Saving in dpkg format is too dangerous to be possible.\n"));
	gaby_perror_in_a_box();
	return TRUE;
}

