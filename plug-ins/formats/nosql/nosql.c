/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <format_plugin.h>

gboolean nosql_load_file (struct location *loc)
{
	FILE *f;
	table *t;
	int i;
	char st[501], *s;
	record *r;
	int index=1;
	
#ifdef DEBUG_GABY
	debug_print("Reading %s\n", loc->filename);
#endif

	f = fopen(loc->filename, "r");
	if ( f == NULL ) {
		gaby_errno = FILE_READ_ERROR;
		gaby_message = g_strdup(loc->filename);
		gaby_perror_in_a_box();
		return FALSE;
	}

	i = 1;
	fgets(st, 500, f);
	while ( ! feof(f) ) {
		if ( st[0] == '#' ) {
			fgets(st, 500, f);
			continue;
		}
		s = st;
		while ( strchr(s, '\t') ) {
			s = strchr(s, '\t') + 1;
#ifdef DEBUG_GABY
			debug_print("[nosql:load_file] st is %s", s);
#endif
			i++;
		}
		break;
	}
	
	t = loc->table;

	if ( feof(f) || i != t->nb_fields ) {
		gaby_errno = CUSTOM_ERROR;
		gaby_message = g_strdup(_("This is not a suitable NoSQL file."));
#ifdef DEBUG_GABY
		debug_print("[nosql:load_file] nb_fields:%d (should be %d)\n",
				i, t->nb_fields );
#endif
		fclose(f);
		gaby_perror_in_a_box();
		return FALSE;
	}

	fgets(st, 500, f); /* the dashes line */
	
#ifdef DEBUG_GABY
	debug_print("[nosql:load_file] st : %s", st);
#endif
	while ( ! feof(f) ) {
		fgets(st, 500, f);
		s = st;
		i=0;
		r = g_malloc(sizeof(record));
		r->id = loc->offset + index++;
		r->file_loc = loc;
		r->cont = g_new0(union data, t->nb_fields);
		while ( strchr(s, '\t') ) {
			strchr(s, '\t')[0] = 0;
			set_table_stringed_field(t, r, i, s);
			i++;
			s += strlen(s)+1;
		}
		if ( i+1 == t->nb_fields) {
			record_add(t, r, FALSE, TRUE);
		} else {
#ifdef DEBUG_GABY
			debug_print("[nosql:load_file] this record isn't valid\n");
#endif
			record_free(t, r);
		}
	}

	return TRUE;
}

gboolean nosql_save_file (struct location *loc)
{
	FILE *f;
	table *t;
	record *r;
	int i, j, k;
	GString *str;
	char st[80];
	
#ifdef DEBUG_GABY
	debug_print("Writing %s\n", loc->filename);
#endif

	f = fopen(loc->filename, "w");
	if ( f == NULL ) {
		gaby_errno = FILE_WRITE_ERROR;
		gaby_message = g_strdup(loc->filename);
		gaby_perror_in_a_box();
		return FALSE;
	}

	t = loc->table;

	/* header */
	for ( j=0; j < t->nb_fields; j++ ) {
		strcpy(st, t->fields[j].i18n_name);
		while ( strchr(st, ' ') ) strchr(st, ' ')[0] = '_';		
		fputs(st, f);
		if ( j == (t->nb_fields-1) ) {
			fputc('\n', f);
		} else {
			fputc('\t', f);
		}
	}
	for ( j=0; j < t->nb_fields; j++ ) {
		for (k=0; k < strlen(t->fields[j].i18n_name); k++ ) {
			fputc('-', f);
		}
		if ( j == (t->nb_fields-1) ) {
			fputc('\n', f);
		} else {
			fputc('\t', f);
		}
	}

	/* records */
	for ( i=0; i < t->max_records; i++ ) {
		r = t->records[i];
		if ( r == NULL || r->id == 0 )
			continue;
		if ( loc->type != NULL && r->file_loc != loc )
			continue;

		for ( j=0; j < t->nb_fields; j++ ) {
			str = get_table_stringed_field(t, r, j);
			while ( strchr(str->str, '\n') )
				strchr(str->str, '\n')[0] = ' ';
			while ( strchr(str->str, '\t') )
				strchr(str->str, '\t')[0] = ' ';
			fputs(str->str, f);
			if ( j == (t->nb_fields-1) ) {
				fputc('\n', f);
			} else {
				fputc('\t', f);
			}
			g_string_free(str, 1);
		}

	}
	
	return TRUE;
}

