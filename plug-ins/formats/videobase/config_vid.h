/*
    Videobase A database program to keep track of your videocollection and
              videos you've seen.
              
    Copyright (C) 1998  Ron Bessems
    Contact me via email at R.E.M.W.Bessems@stud.tue.nl

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/


#define MAXREC 10000
#define DELIM 0xA8
#define _VERSION "0.22"
#define TITLE "Videobase"
#define BANNER "Copyright 1998 (c) Ron Bessems \nGNU Public License \nVideobase comes with ABSOLUTELY NO WARRANTY."
#define GLOBAL_CONFIG "/etc/videobase.rc"
#define LOCAL_DIR "/share/locale"
#define TEXTDOMAIN "videobase"
#define MAXSTRING 300
