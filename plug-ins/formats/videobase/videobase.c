/*  Gaby - Videobase import/export function.
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <format_plugin.h>

#include "type.h"
#include "database.h"

/* fields_ok -- checks if the fields that this plug-in needs are 
                available and have the right type.
*/
static gboolean
fields_ok (table * table)
{
  gboolean ok;
  field *fld;
  field_type type = T_STRING;
  int i;

  ok = TRUE;			/* Innocent until proven guilty */

  if (table->nb_fields!=10)
    ok=FALSE; 

  for (i = 0; i < table->nb_fields; i++)
    {
      fld = &table->fields[i];
#ifdef DEBUG_GABY
      g_print ("%s\n", fld->name);
#endif
      switch (i)
	{
	case 0:
	case 1:
	case 2:
	case 6:
	case 7:
	case 8:
	  type = T_STRING;
	  break;
	case 3:
	  type = T_INTEGER;
	  break;
	case 4:
	case 5:
	  type = T_DATE;
	  break;
	case 9:
	  type = T_STRINGS;
	  break;
	}

      /* If the type in the table differs from the type we need
         than guilty ! */

      if (type != fld->type)
	ok = FALSE;
    }

  return ok;

}


gboolean
videobase_load_file (struct location * loc)
{
  table *t = loc->table;
  struct tm *dt;
  struct list *lijst;
  struct veld *field;
  record *r;
  int i;
  static int lid = 0;


  if (fields_ok (t) == FALSE)
    {
      gaby_errno = CUSTOM_WARNING;
      gaby_message = g_strdup (_ ("Videobase format works only with standard "
                                  "fields, eg desc.videobase  !.\n"));
      gaby_perror_in_a_box ();
      return FALSE;
    }


  /* return;      nah */

  lijst = NULL;
  lijst = load_list (lijst, loc->filename);


  if (lijst == NULL)
    {
      gaby_errno = FILE_READ_ERROR;
      gaby_message = g_strdup (loc->filename);
      gaby_perror_in_a_box ();
      return FALSE;
    }

  move_first (lijst);
  do
    {
      r = g_new(record, 1);
      r->id = loc->offset + lid++;
      r->file_loc = loc;
      r->cont = g_new0 (union data, t->nb_fields);
      field = get_data (lijst);
      for (i = 0; i < 10; i++)
	{
	  switch (i)
	    {
	    case 0:		/* name */
	      {
		r->cont[0].str = g_string_new (field->naam);
	      }
	      break;
	    case 1:		/* director */
	      {
		r->cont[1].str = g_string_new (field->regiseur);
	      }
	      break;
	    case 2:		/* cast */
	      {
		r->cont[2].str = g_string_new (field->cast);
	      }
	      break;
	    case 3:		/* length */
	      {
		r->cont[3].i = field->speelduur / 60;
	      }
	      break;
	    case 4:		/* releasedate */
	      {
		dt = localtime (&field->datum_release);
		r->cont[4].date = g_date_new_dmy (
						   dt->tm_mday,
						   dt->tm_mon + 1,
						   dt->tm_year);
	      }
	      break;
	    case 5:		/* dateseen */
	      {
		dt = localtime (&field->datum_gezien);
		r->cont[5].date = g_date_new_dmy (
						   dt->tm_mday,
						   dt->tm_mon + 1,
						   dt->tm_year);
	      }
	      break;
	    case 6:		/* genre */
	      {
		r->cont[6].str = g_string_new (field->genre);
	      }
	      break;
	    case 7:		/* tape */
	      {
		r->cont[7].str = g_string_new (field->band);
	      }
	      break;
	    case 8:		/* counter */
	      {
		r->cont[8].str = g_string_new (field->teller);
	      }
	      break;
	    case 9:		/* remarks */
	      {
		r->cont[9].str = g_string_new (field->opmerkingen);
	      }
	      break;

	    }
	}
      record_add (loc->table, r, FALSE, TRUE);
    }
  while (move_next (lijst) == 0);

  free_list (lijst);

  return TRUE;

}


gboolean
videobase_save_file (struct location * loc)
{
  table *t = loc->table;
  struct list *lijst;
  struct veld *field;
  struct tm dt;
  int i, j;
  record *r;

#ifdef DEBUG_GABY
  debug_print ("Videobase Writing %s\n", loc->filename);
#endif

  if (fields_ok (t) == FALSE)
    {
      gaby_errno = CUSTOM_WARNING;
      gaby_message = g_strdup (_ ("Videobase format works only with standard "
                                  "fields, eg desc.videobase  !.\n"));
      gaby_perror_in_a_box ();
      return FALSE;
    }


  lijst = NULL;

  for (i = 0; i < t->max_records; i++)
    {
      r = t->records[i];
      if (r == NULL || r->id == 0)
	continue;
      if (loc->type != NULL && r->file_loc != loc)
	continue;

      field = g_new0(struct veld, 1);

      for (j = 0; j < 15; j++)
	{
	  switch (j)
	    {
	    case 0:		/* name */
	      {
		if (r->cont[0].str)
		  strncat (field->naam, r->cont[0].str->str, 99);
	      }
	      break;
	    case 1:		/* director */
	      {
		if (r->cont[1].str)
		  strncat (field->regiseur, r->cont[1].str->str, 99);
	      }
	      break;
	    case 2:		/* cast */
	      {
		if (r->cont[2].str)
		  strncat (field->cast, r->cont[2].str->str, 99);
	      }
	      break;
	    case 3:		/* length  */
	      {
		field->speelduur = r->cont[3].i * 60;
	      }
	      break;
	    case 4:		/* release date */
	      {
		g_date_to_struct_tm (r->cont[4].date, &dt);
		field->datum_release = mktime (&dt);
	      }
	      break;
	    case 5:		/* date seen */
	      {
		g_date_to_struct_tm (r->cont[5].date, &dt);
		field->datum_gezien = mktime (&dt);
		if (field->datum_gezien < 0)
		  field->datum_gezien = 0;

	      }
	      break;
	    case 6:		/* genre */
	      {
		if (r->cont[6].str)
		  strncat (field->genre, r->cont[6].str->str, 19);
	      }
	      break;
	    case 7:		/* tape */
	      {
		if (r->cont[7].str)
		  strncat (field->band, r->cont[7].str->str, 10);
	      }
	      break;
	    case 8:		/* counter */
	      {
		if (r->cont[8].str)
		  strncat (field->teller, r->cont[8].str->str, 30);
	      }
	      break;
	    case 9:		/* remarks */
	      {
		if (r->cont[9].str)
		  strncat (field->opmerkingen, r->cont[9].str->str, 100);
	      }
	      break;
	    }

	}
      lijst = add_to_list (lijst, field, sizeof (struct veld));
    }
  if (save_list (lijst, loc->filename) == 0)
    {
      free_list (lijst);
      return TRUE;
    }
  else
    {
      free_list (lijst);
      return FALSE;
    }
}
