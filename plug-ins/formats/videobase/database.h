/*  Database header file
    14-7-1998
    This is a DLL (double linked list) implementation.
    Copyright (C) 1998  Ron Bessems
    Contact me via email at R.E.M.W.Bessems@stud.tue.nl

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    Note:
    To change the lengte of a data field call the change_data function.  
 
    TODO:
    A way to add a field on a 
    specific position. At this point the field is added to the end of 
    the database, and the DP (database pointer) is moved there.

*/

#include "config_vid.h"


/* Database error, this is the new way to check for an error

   Implemented for :
      	* add_to_list
	* add_to_list_basic (internal function)
	* change_data
	* get_key
	* get_free_key
	* set_filter
	* sort_list	
   Values :
 	0 No error.
      	1 Maximum amount of records reached.
	2 Malloc error
	3 No such key.
	4 No lijst.
	5 No compare function.

	
*/


#ifndef DATABASE
extern int database_error;
#endif
#ifdef DATABASE
int database_error;
#endif


/* this is the core of the individual fields */
struct field
{
        void *data;		   /* Data pointer		   */
	int  size;		   /* Size of that pointer	   */
	unsigned int key;	   /* Unique Key of field	   */
        struct field *vorige;	   /* The previous field 	   	
					When changin this changes the save
					and load functions also !!

								   */
        struct field *volgende;	   /* the next field in line       */
};


/* this is the structure you would use to specify a DLL */
struct list
{
	struct field *eerste;        /* FP (first pointer)             */
	struct field *huidige;	     /* DP (database pointer)          */
	struct field *laatste;       /* LP (last pointer)              */
        int totaal;		     /* TC (total counter              */	
	struct field *lookup[MAXREC];/* FAST KEY lookup table  */
        int (*filter)(const void *); 
                                   /* This function should return
                                	1 for in filter
                                        0 for not in filter
                                    */
                                    
}; 

/* Return values: 
	0   OK !
	-1  No list
	-2  Empty list
	-3  No current item
	-4  No next/previous item
	-5  Write error
        -6  No such record	
	add_to_list and load_list return a NULL on a malloc  file error. 
*/


/* gets the current field key */
unsigned int get_key		(struct list *lijst);
/* changes the data contents and frees the old */
int change_data			(struct list *lijst,void *data, int size,int key);

/* sets the filter for the list */
int set_filter			(struct list *lijst,int (*filter)(const void *));
/* sort the list */
int		sort_list	(struct list *lijst,int (*comp)(const void *,const void *));
/* this adds a data structure to the double linked list */
struct list* 	add_to_list	(struct list *lijst,void *data,int size);
/* this removes the entiry linked list and clears the mem */
int		free_list 	(struct list *lijst);
/* This retrieves the data stored in the current item */
void* 		get_data    	(struct list *lijst);
/* Move to the next field */
int  		move_next    	(struct list *lijst);
/* Move to the first field */
int  		move_first   	(struct list *lijst);
/* Move to the last field */
int  		move_last     	(struct list *lijst);
/* Move the the previous field */
int  		move_previous 	(struct list *lijst);
/* Delete current field */
int  		delete_current	(struct list *lijst);
/* Save the chain */
int 		save_list 	(struct list *lijst, char *naam);
/* Add to or begin a new chain with data from file.. */
struct list* 	load_list	(struct list *lijst, char *naam);
/* Jump to specific field  DEPRECIATED USE GOTO_KEY */
int 		goto_field	(struct list *lijst,int veld);
/* Jump to specific field depending on the key */
int		goto_key	(struct list *lijst,int key);
/* Returns the total number of records */
int 		get_total_field (struct list *lijst);

