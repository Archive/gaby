/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <format_plugin.h>

static void vcard_parse_str(char *str);

gboolean vcard_load_file (struct location *loc)
{
	table *t = loc->table;
	FILE *f;
	char s[512];
	char *ts;
	record *r;
	int index=1;
	int i;
	
#ifdef DEBUG_GABY
	debug_print("Loading %s\n", loc->filename);
#endif

	if ( t->nb_fields != 17 ) {
		gaby_errno = CUSTOM_WARNING;
		gaby_message = g_strdup(_("vCard format currently only works with standard desc.gaby."));
		gaby_perror_in_a_box();
		return FALSE;
	}


	f = fopen(loc->filename, "r" );
	if ( f == NULL ) {
		gaby_errno = FILE_READ_ERROR;
		gaby_message = g_strdup(loc->filename);
		gaby_perror_in_a_box();
		return FALSE;
	}

	fgets(s, 512, f);
	while ( ! feof(f) ) {
		vcard_parse_str(s);
		while ( g_strncasecmp(s, "BEGIN:VCARD", 11) != 0 && ! feof(f) ){
			fgets(s, 512, f);
		}
		
		if ( feof(f) ) break;

		r = g_new0(record, 1);
		r->file_loc = loc;
		r->id = loc->offset + index++;
		r->cont = g_new0(union data, t->nb_fields);
		for ( i=0; i<t->nb_fields; i++) {
			if ( t->fields[i].type == T_STRING || 
					t->fields[i].type == T_STRINGS) {
				r->cont[i].str = g_string_new("");
			} else {
				r->cont[i].anything = NULL;
			}
		}

		while ( g_strncasecmp(s, "END:VCARD", 9) != 0 ) {
			strchr(s,'\n')[0] = 0;
			if ( strchr(s,'\r') ) strchr(s,'\r')[0] = 0;
				/* it is in dos format : end of line is \r\n */

			if ( strchr(s, ':') != NULL ) {
				strchr(s, ':')[0] = 0;
				ts = s+strlen(s)+1;
			} else {
				ts = s;
			}

#ifdef DEBUG_GABY
			debug_print("[vcard:load_file] s : %s, ts : %s\n", s, ts);
#endif
						
			if ( strcasecmp(s, "N") == 0 ) {
				
				if ( strchr(ts,';') ) {
					strchr(ts,';')[0] = 0;
					if ( strchr(ts+strlen(ts)+1, ';') ) {
						strchr(ts+strlen(ts)+1, ';')[0] = 0;
						/* WISHLIST
						 *  this could be put in (more)
						 */
					}
					r->cont[0].str = g_string_assign( 
						r->cont[0].str, ts+strlen(ts)+1);
				}
				r->cont[1].str = g_string_assign( 
							r->cont[1].str, ts);
			}

			if ( strcasecmp(s, "ORG" ) == 0 ) {
				r->cont[2].str = g_string_assign( 
							r->cont[2].str, ts);
			}

			if ( strcasecmp(s, "ADR;HOME" ) == 0 ) {
				/* the format is :
				 *  ?;street2;street1;city;state;zip;country
				 */
				i = 0;
				while ( *ts != 0 ) {
					if ( strchr(ts, ';') != NULL )
						strchr(ts, ';')[0] = 0;
					switch ( i ) {
						case 0: {} break;
						case 1:
						{
							r->cont[3].str = g_string_assign(r->cont[3].str, ts);
						} break;
						case 2:
						{
							r->cont[3].str = g_string_prepend(r->cont[3].str, ts);
						} break;
						case 3:
						{
							r->cont[5].str = g_string_assign(r->cont[5].str, ts);
						} break;
						case 4:
						{
							r->cont[6].str = g_string_assign(r->cont[6].str, ts);
						} break;
						case 5:
						{
							r->cont[4].str = g_string_assign(r->cont[4].str, ts);
						} break;
						case 6:
						{
							r->cont[7].str = g_string_assign(r->cont[7].str, ts);
						} break;
					}
					ts += strlen(ts)+1;
					i++;
				}
			}
			
			if ( strcasecmp(s, "UID" ) == 0 ) {
#if 0 /* this uses the birthday field */
				r->cont[8].str = g_string_assign(
							r->cont[8].str, ts);
#else /* while there is a field ready for that */

				/* a problem may occur if there is a mix of
				 * record with and without uid fields. That
				 * is a real problem :( */
				r->id = loc->offset + atoi(s);
#endif
			}
		   
			if ( strcasecmp(s, "TEL;HOME" ) == 0 ) {
				r->cont[9].str = g_string_assign( 
							r->cont[9].str, ts);
			}

			if ( strcasecmp(s, "TEL;WORK" ) == 0 ) {
				r->cont[10].str = g_string_assign( 
							r->cont[10].str, ts);
			}			
			if ( strcasecmp(s, "TEL;CELL" ) == 0 ) {
				r->cont[11].str = g_string_assign( 
							r->cont[11].str, ts);
			}
			
			if ( strcasecmp(s, "TEL;FAX" ) == 0 ) {
				r->cont[12].str = g_string_assign( 
							r->cont[12].str, ts);
			}

			if ( strcasecmp(s, "EMAIL;INTERNET" ) == 0 ) {
				r->cont[13].str = g_string_assign ( 
							r->cont[13].str, ts );
			}

			if ( strcasecmp(s, "URL" ) == 0 ) {
				r->cont[14].str = g_string_assign ( 
							r->cont[14].str, ts );
			}

			if ( g_strncasecmp(s, "CATEGORIES",10 ) == 0 ) {
				r->cont[15].str = g_string_assign ( 
							r->cont[15].str, ts );
			}

			if ( g_strncasecmp(s, "NOTE", 4 ) == 0 ) {
				r->cont[16].str = g_string_assign ( 
							r->cont[16].str, ts );
			}
			
			fgets(s, 512, f);
			vcard_parse_str(s);
		}
#ifdef DEBUG_GABY
		debug_print("vcard plugin : %s %s\n", r->cont[0].str->str, r->cont[1].str->str);
#endif
		record_add(t, r, FALSE, TRUE);
	}

	fclose(f);
	
	return TRUE;
}

static void vcard_parse_str(char *str)
{
	/* lines may be :
	 *    begin:          vcard
	 * or :
	 *    BEGIN:VCARD
	 *
	 * this functions deals (not wonderfully with that)
	 */
	char *s;
	
	if ( ! ( s = strchr(str, ':') ) )
		return;
	if ( *(s+1) == '\0' ) return;
	while ( *s && isspace((int)(*s)) ) {
		memmove(s, s+1, strlen(s+1)+1);
	}

#ifdef DEBUG_GABY
	debug_print("[vcard_parse_str] parsed str : %s", str);
#endif

}

gboolean vcard_save_file (struct location *loc)
{
	table *t = loc->table;
	FILE *f;
	int i;
	char s[500];
	record *r;

#ifdef DEBUG_GABY
	debug_print("Writing %s\n", loc->filename );
#endif

	f = fopen(loc->filename, "w");
	if ( f == NULL ) {
		gaby_errno = FILE_WRITE_ERROR;
		gaby_message = g_strdup(loc->filename);
		gaby_perror_in_a_box();
		return FALSE;
	}

	for ( i=0; i<t->max_records; i++ ) {
		r = t->records[i];
		if ( r == NULL || r->id == 0 )
			continue;
		if ( loc->type != NULL && r->file_loc != loc )
			continue;
		
		fputs("BEGIN:VCARD\n", f);
		fprintf(f, "FN:%s %s\n", r->cont[0].str->str,
					 r->cont[1].str->str );
		fprintf(f, "N:%s;%s\n",  r->cont[1].str->str,
					 r->cont[0].str->str );
	
		if ( r->cont[2].str != NULL && r->cont[2].str->len > 0 )
			fprintf(f, "ORG:%s\n", r->cont[2].str->str );

		if ( ( r->cont[3].str != NULL && r->cont[3].str->len > 0 ) || 
		     ( r->cont[4].str != NULL && r->cont[4].str->len > 0 ) || 
		     ( r->cont[5].str != NULL && r->cont[5].str->len > 0 ) || 
		     ( r->cont[6].str != NULL && r->cont[6].str->len > 0 ) || 
		     ( r->cont[7].str != NULL && r->cont[7].str->len > 0 ) ) {
			
			fprintf(f, "ADR;HOME:;;%s;%s;%s;%s;%s\n", 
				( r->cont[3].str == NULL ) ? 
						"" : r->cont[3].str->str, 
				( r->cont[5].str == NULL ) ? 
						"" : r->cont[5].str->str, 
				( r->cont[6].str == NULL ) ? 
						"" : r->cont[6].str->str, 
				( r->cont[4].str == NULL ) ? 
						"" : r->cont[4].str->str, 
				( r->cont[7].str == NULL ) ? 
						"" : r->cont[7].str->str );
		}
		
		if ( r->cont[8].str != NULL && r->cont[8].str->len > 0 ) {
#if 0
			fprintf(f, "UID:%s\n", r->cont[8].str->str );
#else
			fprintf(f, "UID:%d\n", r->id - loc->offset );
#endif
		}
		
		if ( r->cont[9].str != NULL && r->cont[9].str->len > 0 )
			fprintf(f, "TEL;HOME:%s\n", r->cont[9].str->str );
		
		if ( r->cont[10].str != NULL && r->cont[10].str->len > 0 )
			fprintf(f, "TEL;WORK:%s\n", r->cont[10].str->str );
		
		if ( r->cont[11].str != NULL && r->cont[11].str->len > 0 )
			fprintf(f, "TEL;CELL:%s\n", r->cont[11].str->str );

		if ( r->cont[12].str != NULL && r->cont[12].str->len > 0 )
			fprintf(f, "TEL;FAX:%s\n", r->cont[12].str->str );
		
		if ( r->cont[13].str != NULL && r->cont[13].str->len > 0 )
			fprintf(f, "EMAIL;INTERNET:%s\n", r->cont[13].str->str );
		
		if ( r->cont[14].str != NULL && r->cont[14].str->len > 0 )
			fprintf(f, "URL:%s\n", r->cont[14].str->str );

		if ( r->cont[15].str != NULL && r->cont[15].str->len > 0 )
			fprintf(f, "CATEGORIES:%s\n", r->cont[15].str->str );

		if ( r->cont[16].str != NULL && r->cont[16].str->len > 0 )
			fprintf(f, "NOTE:%s\n", r->cont[16].str->str );

		fputs("END:VCARD\n\n", f);
	}

	fclose(f);
	
	
	return TRUE;
}

