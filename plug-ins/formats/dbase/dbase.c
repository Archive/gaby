/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <format_plugin.h>

static gboolean db3_process(struct location *loc);

gboolean dbase_load_file (struct location *loc)
{
	db3_process(loc);
	
	return TRUE;
}

gboolean dbase_save_file (struct location *loc)
{

	return TRUE;
}

#define DB_FL_BROWSE	0x01
#define DB_FL_INFO  	0x02
#define DB_FL_DESCR 	0x04
#define DB_FL_RESERVE	0x08
#define DB_FL_OMIT	0x10
#define DB_FL_TRIM	0x20

typedef struct dbase_head { 
	unsigned char	version;		/* 03 for dbIII and 83 for dbIII w/memo file */
	unsigned char	l_update[3];		/* yymmdd for last update*/
	unsigned long	count;			/* number of records in file*/
	unsigned short	header;			/* length of the header
						 * includes the \r at end
						 */
	unsigned short	lrecl;			/* length of a record
						 * includes the delete
						 * byte
						 */
	unsigned char   reserv[20];
} DBASE_HEAD;

#define DB_FLD_CHAR  'C'
#define DB_FLD_NUM   'N'
#define DB_FLD_LOGIC 'L'
#define DB_FLD_MEMO  'M'
#define DB_FLD_DATE  'D'

typedef struct dbase_fld {
	char		name[11];	/*field name*/
	char		type;		/*field type*/
			/* A-T uses large data model but drop it for now */
	char		*data_ptr;	/*pointer into buffer*/
	unsigned char	length;		/*field length*/
	char		dec_point;	/*field decimal point*/
	char		fill[14];
} DBASE_FIELD;

typedef struct fld_list {
	struct fld_list *next;
	DBASE_FIELD     *fld;
	char            *data;
} FLD_LIST;

static FLD_LIST* db3_read_dic(int dbfile, DBASE_HEAD *dbhead);
static void db3_print_recs(int cnt, int dbfile, DBASE_HEAD *dbhead,
			   FLD_LIST *db_fld_root, struct location *loc);
static void db3_print(FLD_LIST *db_fld_root, struct location *loc);
static FLD_LIST* stack_field(DBASE_FIELD *fld, FLD_LIST *db_fld_root, char *Buffer);

static gboolean db3_process(struct location *loc)
{
	DBASE_HEAD dbhead={0};
	FLD_LIST *db_fld_root;
	int dbfile;
	int nb_fields;
	char *dbfn = loc->filename;
		
	dbfile=open(dbfn,O_RDONLY);
	if (dbfile == -1) {
		gaby_errno = FILE_READ_ERROR;
		gaby_message = g_strdup(dbfn);
		gaby_perror_in_a_box();
		return FALSE;
	}
	
	db_fld_root = db3_read_dic(dbfile, &dbhead);
#if 0
	if ( nb_fields != loc->table->nb_fields ) return FALSE;
#endif
	
	db3_print_recs(dbhead.count, dbfile, &dbhead, db_fld_root, loc);
	
	close(dbfile);

	return TRUE;
}


/******************************************************
  db3_read_dic()
  This function is called with a file name to
  read to create a record type to support the
  dbase file
  returns the list of fields (NULL if the file isn't supported)
 ******************************************************/

static FLD_LIST* db3_read_dic(int dbfile, DBASE_HEAD *dbhead)
{
	int fields, nb_fields;
	DBASE_FIELD *fld;
	FLD_LIST *db_fld_root = NULL;
	char *Buffer;

	if(dbfile==-1) {
		printf("open failed");
		return NULL;
	}
	read(dbfile,dbhead,sizeof(DBASE_HEAD));
	if( !(dbhead->version==3 || dbhead->version==0x83) ) {
#ifdef DEBUG_GABY
		debug_print ("Version %d not supported\n",dbhead->version);
		if(dbhead->version==0x8b ) {
			debug_print ("dBase IV - partially known...\n");
		}
#endif
		return NULL;
	}

#ifdef DEBUG_GABY
	printf("File version  : %d\n",dbhead->version);
	printf("Last update   : %02d/%02d/%2d\n", 
						dbhead->l_update[1],
						dbhead->l_update[2],
						dbhead->l_update[0]);
	printf("Number of recs: %ld\n", dbhead->count);
	printf("Header length : %d\n", dbhead->header);
	printf("Record length : %d\n", dbhead->lrecl);
#endif

	Buffer=g_malloc(dbhead->lrecl);

	nb_fields=(dbhead->header-1)/32-1;
	fields = nb_fields;

	while(fields--) {
		fld=(DBASE_FIELD *)malloc(sizeof(DBASE_FIELD));
		read(dbfile,fld,sizeof(DBASE_FIELD));
		db_fld_root = stack_field(fld, db_fld_root, Buffer);
	}
	read(dbfile,Buffer,1);	/* read the silly little \r 0x0d character */

#if 0
	/* this was in the source but I (FP) wonder why (doesn't work with,
	 * works without */
	
	read(dbfile,Buffer,1);	/* strange, it only works if we read another byte */
#endif

	g_free(Buffer);

	return db_fld_root;
}

/******************************************************
  db3_print_recs()
  Read records and print the data
 ******************************************************/

static void db3_print_recs(int cnt, int dbfile, DBASE_HEAD *dbhead,
			   FLD_LIST *db_fld_root, struct location *loc)
{
	int bytes;
	char *Buffer = g_malloc(dbhead->lrecl);

	while (cnt) {
		
		bytes=read(dbfile,Buffer,dbhead->lrecl);
		
		if(bytes!=dbhead->lrecl)
			break;
		/* Check if deleted == '*' */
		if(Buffer[0] != '*') {
			db3_print(db_fld_root, loc);
			cnt--;
		}
	}
	g_free(Buffer);
	return;
}


/******************************************************
  db3_print()
  Print a single record
 ******************************************************/

static void db3_print(FLD_LIST *db_fld_root, struct location *loc)
{
	FLD_LIST *temp;
	char buf_work[255];
	static int lid=0;
	char *tmp;
	record *r;
	int fn=0;

	r = g_malloc(sizeof(record));
	r->id = loc->offset + lid++;
	r->cont = g_new0(union data, loc->table->nb_fields);
	
	temp=db_fld_root;
	while (temp) {
		memcpy(buf_work,temp->data,temp->fld->length);
		buf_work[temp->fld->length] = '\0';
		
		for (tmp=buf_work+strlen(buf_work)-1; isspace ((int)*tmp); tmp--);
		*(++tmp) = '\0';

		set_table_stringed_field(loc->table, r, fn, buf_work);
		fn++;
		temp=temp->next;
	}
	record_add(loc->table, r, FALSE, TRUE);

	return;
}

/******************************************************
  stack_field()
  Add a field to the linked list of fields
 ******************************************************/

static FLD_LIST* stack_field(DBASE_FIELD *fld, FLD_LIST *db_fld_root, char *Buffer)
{
	FLD_LIST *list, *temp;

	list = (FLD_LIST *)calloc(1,sizeof(FLD_LIST));
	list->fld = fld;
	if(!db_fld_root) {
		list->data=Buffer+1;	/*skip delete byte*/
		db_fld_root=list;
		return db_fld_root;
	}
	temp=db_fld_root;
	while(temp->next) temp=temp->next;
	temp->next=list;
	list->data = temp->data + temp->fld->length;
	return db_fld_root;
}

