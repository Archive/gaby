/*  Gaby
 *  Copyright (C) 1998-2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <tables.h>
#include <records.h>
#include <f_config.h>
#include <windows.h>		/* for update_bound_windows */
#include <f_desc.h>		/* for field_get_property */

#include <gtk_config_dlg.h>	/* for gaby_property_box_changed */
#include <gtk_menu.h>		/* for gaby_quit_callback */
#include <gtk_main.h>		/* for statusbar_message */

#ifndef USE_GNOME
#include "pixmaps/new.xpm"
#include "pixmaps/remove.xpm"
#include "pixmaps/previous.xpm"
#include "pixmaps/next.xpm"
#include "pixmaps/exit.xpm"
#include "pixmaps/sort.xpm"
#endif

#include "pixmaps/notfound.xpm"

mstatic void form_create ( gabywindow *w, gboolean first );
mstatic void form_fill ( gabywindow *win );
mstatic void form_save ( gabywindow *win );
mstatic GtkWidget* form_configure (ViewPluginData *vpd);
mstatic void form_get_config();

#ifndef FOLLOW_MIGUEL

int init_view_plugin (ViewPluginData *vpd)
{
	vpd->view_create = form_create;
	vpd->view_fill = form_fill;
	vpd->view_save = form_save;
	vpd->configure = form_configure;
	vpd->view_records = NULL;
	
	vpd->name = "form";
	vpd->i18n_name = _("Form");
	vpd->type = ONE_RECORD;
	vpd->capabilities = EDITABLE;

#ifdef DEBUG_GABY
	debug_print("form plugin : reading config\n");
#endif
	form_get_config();

#ifdef DEBUG_GABY
	debug_print("Initialization of view plugin '%s' done succesfully.\n", 
			vpd->i18n_name );
#endif

	return 0;
}

#endif /* ! FOLLOW_MIGUEL */

static GtkWidget* line_add( GtkWidget *tabl, st_field *f, int i,
			    int *win_height, gboolean first, gabywindow *win );
static GtkWidget* create_record_field(st_field *f, gabywindow *window );
static GtkWidget* create_recordslist_field(st_field *f, gabywindow *window );
static GtkWidget* create_multimedia_field( st_field *f, gabywindow *window );

static void form_fill_real(GtkWidget *w[], subtable *f, int id);
static void form_empty(GtkWidget *w[], GList *t, subtable *f);

static void fill_record_field(GtkWidget *w, subtable *f, int id, int field_no);
static void fill_multimedia_field( GtkWidget *w, subtable *f, int id,
				   int field_no);
static gint field_image_config (GtkWidget *widget, GdkEventConfigure *event);
static void play_sound (GtkWidget *but, gpointer data);
static void field_image_load(GtkWidget *wi, gchar *filename);
static void file_field_select_file(GtkWidget *but, GtkWidget *entry);

#ifdef USE_GNOME
static void gnome_toolbar_create ( gabywindow *windpw, gboolean first ); 
#else

static GtkWidget* form_toolbar_create ( gabywindow *window, gboolean first );
static GtkWidget* create_pixmap(GtkWidget *widget, gchar **data);

#endif

static void calendar_changed(GtkWidget *cal, gabywindow *window);
static void form_toolbar_new(GtkWidget *toolbar, gabywindow *window);
static void form_toolbar_remove(GtkWidget *toolbar, gabywindow *window);
static void form_toolbar_first(GtkWidget *toolbar, gabywindow *window);
static void form_toolbar_next(GtkWidget *toolbar, gabywindow *window);
static void form_toolbar_prev(GtkWidget *toolbar, gabywindow *window);

static void form_toolbar_sort(GtkWidget *toolbar, gabywindow *window);

static record* form_record_save(gabywindow *window, record *r);
static gboolean record_is_different(record *r, gabywindow *window);
static gboolean record_is_not_empty(gabywindow *window);

/* this could be moved in src/ */
#if 0
static gboolean fill_date(GDate *date, gchar *s);
#endif

static gboolean text_for_strings;
static gboolean cal_for_date;
static gboolean spin_for_numeric;
static gboolean check_for_boolean;

mstatic void form_create ( gabywindow *win, gboolean first )
{
	GtkWidget *vbox;
	GtkWidget *sclwin;
	GtkWidget *table;
#if 0
	GtkWidget *vspace;
#endif
#ifndef USE_GNOME
	GtkWidget *toolbar;
#endif
	int *sort_by;
	int i;
	GtkWidget **right;
	record *r;
	int win_height=0;
#ifdef DEBUG_GABY
	int ddd=0;
#endif
	gint height, width;
	int *id = &(win->id);
	view *form = win->view;
	gboolean *updated = &(win->updated);
	char sectname[100];
	GList *options, *o_it;
	gboolean create_toolbar = TRUE;
	gchar *str;

	sprintf(sectname, "Form:%s", win->view->subtable->name);
	options = get_plugin_options(sectname);
	o_it = options;
	while ( o_it != NULL ) {
		str = o_it->data;
		o_it = g_list_next(o_it);
		if ( strcmp(str, "no toolbar") == 0 ) {
#ifdef DEBUG_GABY
			debug_print("[form_create] without toolbar !]\n");
#endif
			create_toolbar = FALSE;
		}
		g_free(str);
	}
	g_list_free(options);
	
	vbox = gtk_vbox_new(FALSE,0);
	gtk_widget_show(vbox);
	win->widget = vbox;
	
	sort_by = g_malloc(sizeof(int));
	*sort_by = -1;
	
	r = table_first(form->subtable->table, *sort_by);
	
	*id = ( r == NULL ? 0 : r->id );
		
	/*gtk_object_set_data(GTK_OBJECT(vbox), "window", win );*/

	/* most of those could be accessed from "window" */
	gtk_object_set_data(GTK_OBJECT(vbox), "sort_by", sort_by );

	if ( create_toolbar == TRUE ) {
#ifdef USE_GNOME

#if 0
	if ( first ) {
		gnome_app_set_toolbar(app, GTK_TOOLBAR(toolbar));
	} else {
/*		gtk_box_pack_start (GTK_BOX(vbox), toolbar, FALSE, TRUE, 0 );*/
		if ( GNOME_IS_APP(parent) ) {
			gnome_app_set_toolbar(GNOME_APP(parent), GTK_TOOLBAR(toolbar));
		}
	}
#endif
	
		gnome_toolbar_create(win, first);
#else
		toolbar = form_toolbar_create(win, first);
		gtk_box_pack_start (GTK_BOX(vbox), toolbar, FALSE, TRUE, 0 );
#endif
	}
/*
	vspace=gtk_label_new("");
	gtk_widget_set_usize(vspace,20,5);	
	gtk_box_pack_start(GTK_BOX(vbox),vspace,FALSE,FALSE,0);
	gtk_widget_show(vspace);
*/
	sclwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_set_border_width (GTK_CONTAINER (sclwin), 5);
	gtk_widget_show(sclwin);
	gtk_object_set_data(GTK_OBJECT(vbox), "scrollbar",
		GTK_SCROLLBAR(GTK_SCROLLED_WINDOW(sclwin)->vscrollbar));
		
	gtk_box_pack_start (GTK_BOX (vbox), sclwin, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy ( GTK_SCROLLED_WINDOW (sclwin),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	
	table = gtk_table_new ( form->subtable->nb_fields, 2, FALSE);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(sclwin),table);
	gtk_widget_show(table);
	gtk_table_set_row_spacings (GTK_TABLE (table), 5);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);

	right = g_malloc(sizeof(GtkWidget *)*form->subtable->nb_fields);
	gtk_object_set_data(GTK_OBJECT(vbox), "right", right );

#ifdef DEBUG_GABY
	debug_print("form_create, step %d\n", ++ddd);
#endif
	for ( i=0; i<form->subtable->nb_fields; i++ ) {
		right[i] = line_add(table, &form->subtable->fields[i],
				i, &win_height, first, win);
	}
	
#ifdef DEBUG_GABY
	debug_print("form_create, step %d\n", ++ddd);
#endif
	
#if 0
	sprintf(sectname, "Form:%s", v->subtable->name);
	geometry = get_plugin_options(sectname);
#endif
	
	height = get_config_int(appname, form->subtable->name, "form_height",
			(form->subtable->nb_fields < 10 ) ? win_height : 300);
	width = get_config_int(appname, form->subtable->name, "form_width",400);
	
	gtk_widget_set_usize (GTK_WIDGET(sclwin), 400, 
			(form->subtable->nb_fields < 10 ) ? win_height : 300);

	*updated = FALSE;

	return;
}

static void show_description ( GtkWidget *win, field *f )
{
	property *desc = field_get_property(f, "description");
	if ( desc == NULL ) {
		statusbar_message(f->i18n_name, NULL);
	} else {
		statusbar_message((gchar*)(desc->val), NULL);
	}
#ifdef DEBUG_GABY
	debug_print("[form:line_add] height : %d\n", win->allocation.height );
#endif
}

static void move_scrolled_win ( GtkWidget *wid, GtkWidget *win )
{
	/* 'entry with focus should always be visible'
	 * this looks simple but it's not (at all) a straight forward thing to
	 * achieve. Something 'logic' would be :
	 * 	if ( not_visible(widget) ) move_scrollbar
	 * but it's not possible to get the not_visible with gtk functions
	 * (GTK_WIDGET_VISIBLE is true even if not visible :( )
	 */
	GtkWidget *scrollbar = gtk_object_get_data(GTK_OBJECT(win),"scrollbar");
	GtkAdjustment *adjust = GTK_SCROLLBAR(scrollbar)->range.adjustment;
	gint bottom, top;
	
#ifdef DEBUG_GABY
	/*debug_print("[form:msw] field : %s -", f->name );*/
	if ( GTK_WIDGET_DRAWABLE(wid) ) debug_print("drawable - ");
	if ( GTK_WIDGET_MAPPED(wid) ) debug_print("mapped - ");
	if ( GTK_WIDGET_APP_PAINTABLE(wid) ) debug_print("app-paintable - ");
	if ( GTK_WIDGET_VISIBLE(wid) ) debug_print("visible - ");
	debug_print("\n");
	debug_print("wid->allocation->{x,y} : %d, %d (top-left)\n",
			wid->allocation.x, wid->allocation.y );
	debug_print("wid->allocation->{x,y} : %d, %d (right-bottom)\n",
			wid->allocation.x + wid->allocation.width,
			wid->allocation.y + wid->allocation.height );
	debug_print("adjust : lower : %f, upper : %f, value : %f\n",
			adjust->lower, adjust->upper, adjust->value );
	debug_print("scrollbar->height : %d\n", scrollbar->allocation.height );
#endif
	bottom = wid->allocation.y + wid->allocation.height;
	top = wid->allocation.y;

	if ( bottom > (int)(adjust->value + scrollbar->allocation.height ) ) {
#ifdef DEBUG_GABY
		debug_print("Move to the bottom!\n");
#endif
		gtk_adjustment_set_value(adjust, (float)
			(bottom - scrollbar->allocation.height + 3));
	} else {
		if ( top < (int)(adjust->value) ) {
#ifdef DEBUG_GABY
			debug_print("Move to the top!\n");
#endif
			gtk_adjustment_set_value(adjust, (float)top);
		}
	}
	
}

#if 0
static void widget_expose_event ( GtkWidget *wid, GdkEventExpose *event,
				  st_field *f )
{
#ifdef DEBUG_GABY
	g_print("[form:wee] field : %s\n", f->name );
#endif
}

static void widget_noexpose_event ( GtkWidget *wid, GdkEventAny *event,
				    st_field *f )
{
#ifdef DEBUG_GABY
	g_print("[form:wne] field : %s\n", f->name );
#endif
}
#endif

static void field_changed ( GtkWidget *wid, gabywindow *window)
{
	window->updated = TRUE;
}

static GtkWidget* line_add( GtkWidget *tabl, st_field *f, int i,
			    int *win_height, gboolean first, gabywindow *win )
/* it should be possible to wipe a few parameters */
{
	/* TODO (post-2.0): getting correct size of widgets
	 *  the win_height thing is _ugly_ and won't work right with
	 *  users'exotic configurations.
	 */
	/* TODO (post-2.0): rewriting this with an array of function pointer
	 * to get rid of the huge (and ugly) switch block
	 */
	table *t = win->view->subtable->table;
	GtkWidget *label;
	GtkWidget *wid, *wid2 = NULL;
	int type;
	GtkAdjustment *adj;
	float min, max, def;
	int nblines;
	property *prop;

#ifdef DEBUG_GABY
	debug_print("Creating line with field %s\n", f->i18n_name);
#endif
	label = gtk_label_new ( f->i18n_name );
	gtk_table_attach (GTK_TABLE (tabl), label, 0, 1, i, i+1,
			 GTK_FILL, GTK_EXPAND | GTK_FILL, 5, 0 );
	gtk_widget_show(label);

#ifdef DEBUG_GABY
	debug_print("field type : %d\n", f->type);
#endif

	type = f->type;
	if      ( type == T_STRINGS && ! text_for_strings  ) type = T_STRING;
	else if ( type == T_DATE    && ! cal_for_date      ) type = T_STRING;
	else if ( type == T_INTEGER && ! spin_for_numeric  ) type = T_STRING;
	else if ( type == T_REAL    && ! spin_for_numeric  ) type = T_STRING;
	else if ( type == T_BOOLEAN && ! check_for_boolean ) type = T_STRING;
#if !defined HAVE_ESD && ! ( defined HAVE_IMLIB || defined HAVE_GDKPIXBUF )
	else if ( type == T_MULTIMEDIA )		     type = T_STRING;
#endif
	else if ( type == T_DECIMAL )			     type = T_STRING;
	
#ifdef DEBUG_GABY
	debug_print("line_add, step 0\n");
#endif
	switch ( type ) {
		case T_STRING: /* entry */
		{
			wid = gtk_entry_new();
			gtk_entry_set_editable(GTK_ENTRY(wid), TRUE);
#ifdef DEBUG_GABY
			/*debug_print("[form:line_add] font ascent:%d, descent:%d\n", wid->style->font->ascent, wid->style->font->descent);
			debug_print("[form:line_add] font height:%d\n",
				gdk_text_height(wid->style->font, "fp", 2));*/
#endif
			*win_height += 30;
			gtk_signal_connect(GTK_OBJECT(wid), "changed",
					GTK_SIGNAL_FUNC(field_changed), win);
		} break;
		case T_STRINGS: /* text box */
		{
			prop = field_get_property( &t->fields[f->no], "lines" );
			nblines = ( prop == NULL ) ? 5 :
						((union data*)prop->val)->i;
#ifdef DEBUG_GABY
			debug_print("[line_add] t_strings with %d lines\n",
							nblines );
#endif
			wid = gtk_text_new(NULL, NULL);
			gtk_text_set_editable(GTK_TEXT(wid), TRUE);
			gtk_text_set_word_wrap(GTK_TEXT(wid), TRUE);
			gtk_widget_set_usize(wid, -1, nblines*18);
			*win_height += (nblines*18)+5;
			gtk_signal_connect(GTK_OBJECT(wid), "changed",
					GTK_SIGNAL_FUNC(field_changed), win);
		} break;
		case T_DATE: /* gtk calendar */
		{
			/* WISHLIST : use gnome-date
			 *  (if available and the user wants it)
			 */
			wid = gtk_calendar_new();
			gtk_signal_connect(GTK_OBJECT(wid),
				"month_changed", calendar_changed, win);
			gtk_signal_connect(GTK_OBJECT(wid),
				"day_selected", calendar_changed, win);
			*win_height += 140;
		} break;
		case T_INTEGER: /* spin button */
		{
			prop = field_get_property( &t->fields[f->no], "min" );
			min = (prop==NULL) ? 0 : ((union data*)prop->val)->i;
			prop = field_get_property( &t->fields[f->no], "max" );
			max = (prop==NULL) ? 0 : ((union data*)prop->val)->i;
			prop = field_get_property( &t->fields[f->no],"default");
			def = (prop==NULL) ? min : ((union data*)prop->val)->i;
			adj = (GtkAdjustment*)gtk_adjustment_new(def, min, max,\
							1, (max-min)/10, 0);
			wid = gtk_spin_button_new(adj, 0, 0);
			*win_height += 30;
			gtk_signal_connect(
				GTK_OBJECT(&(GTK_SPIN_BUTTON(wid)->entry)), 
				"changed", GTK_SIGNAL_FUNC(field_changed),
				win);
		} break;
		case T_REAL: /* spin button */
		{
			prop = field_get_property( &t->fields[f->no], "min" );
			min = (prop==NULL) ? 0 : ((union data*)prop->val)->d;
			prop = field_get_property( &t->fields[f->no], "max" );
			max = (prop==NULL) ? 0 : ((union data*)prop->val)->d;
			prop = field_get_property( &t->fields[f->no],"default");
			def = (prop==NULL) ? min : ((union data*)prop->val)->d;
			adj = (GtkAdjustment*)gtk_adjustment_new(def, min, max,\
							1, (max-min)/10, 0);
			wid = gtk_spin_button_new(adj, 0, 0);
			*win_height += 30;
			gtk_signal_connect(
				GTK_OBJECT(&(GTK_SPIN_BUTTON(wid)->entry)), 
				"changed", GTK_SIGNAL_FUNC(field_changed),
				win);
		} break;
		case T_BOOLEAN: /* boolean value */
		{
			wid = gtk_check_button_new ();
			*win_height += 30;
			gtk_signal_connect(GTK_OBJECT(wid), "clicked",
					GTK_SIGNAL_FUNC(field_changed), win);
		} break;
		case T_RECORD:
		{
			wid = create_record_field(f, win);
			*win_height += 40;
		} break;
		case T_RECORDS:
		{
			wid = create_recordslist_field(f, win);
			*win_height += 155;
		} break;
		case T_MULTIMEDIA:
		{
			wid = create_multimedia_field(f, win);
			*win_height += 30;
		} break;
		case T_FILE:
		{
			GtkWidget *but;
			GtkWidget *table;
			GtkWidget *child;

			table = gtk_table_new(1, 2, FALSE);
			but = gtk_button_new_with_label("...");
			gtk_widget_show(but);
			gtk_table_attach (GTK_TABLE (table), but, 1, 2, 0, 1, 0, 0, 2, 2);
			child = gtk_entry_new();
			gtk_widget_show(child);
			gtk_table_attach (GTK_TABLE (table), child, 0, 1, 0, 1,
					GTK_EXPAND | GTK_FILL,
					GTK_EXPAND | GTK_FILL,
					2, 2);
			gtk_signal_connect(GTK_OBJECT(but), "clicked",
						file_field_select_file, child );
			gtk_signal_connect(GTK_OBJECT(child), "changed", 
					GTK_SIGNAL_FUNC(field_changed), win);
			*win_height += 30;
			gtk_widget_show(table);
			wid = table;
			wid2 = child;
		} break;
		default:
		{
			wid = gtk_label_new(_("Unsupported type of field"));
			*win_height += 20;
		} break;
	}
		
#ifdef DEBUG_GABY
	debug_print("line_add, step 1\n");
#endif
	gtk_widget_show(wid);
	
	if ( type != T_MULTIMEDIA ) {
		gtk_table_attach (GTK_TABLE (tabl), wid, 1, 2, i, i+1,
			GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			0, 0 );
	} else {
		gtk_table_attach (GTK_TABLE (tabl), wid, 1, 2, i, i+1,
			GTK_EXPAND, GTK_EXPAND,
			0, 0 );
	}
	
	if ( first && get_config_bool("view", "form", "use_statusbar", TRUE) ) {
		gtk_signal_connect (GTK_OBJECT(wid), "grab_focus",
				GTK_SIGNAL_FUNC(show_description),
				&t->fields[f->no] );
	}
	gtk_signal_connect (GTK_OBJECT(wid), "grab_focus",
			GTK_SIGNAL_FUNC(move_scrolled_win), win->widget );
#if 0
	gtk_signal_connect (GTK_OBJECT(wid), "expose_event",
			GTK_SIGNAL_FUNC(widget_expose_event), f );
	gtk_signal_connect (GTK_OBJECT(wid), "no_expose_event",
			GTK_SIGNAL_FUNC(widget_noexpose_event), f );
#endif
	
	return ( wid2 ? wid2 : wid );

}

static void calendar_changed(GtkWidget *cal, gabywindow *window)
{
	window->updated = TRUE;
	/*gtk_object_set_data(GTK_OBJECT(cal), "changed", GINT_TO_POINTER(TRUE));*/
}

static void record_field_dialog(GtkWidget *dialog, gint button_number)
{
	record *r;
	int i;
	table *t = gtk_object_get_data(GTK_OBJECT(dialog), "table");
	subtable *f = gtk_object_get_data(GTK_OBJECT(dialog), "subtable");
	int f_n =GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(dialog),"f_n"));
	GtkWidget *wid = gtk_object_get_data(GTK_OBJECT(dialog), "widget");
	gabywindow *win = gtk_object_get_data(GTK_OBJECT(wid), "window");
	
#ifdef DEBUG_GABY
	debug_print("[gaby:form:rfd] button : %d\n", button_number);
#endif
	r = gtk_object_get_data(GTK_OBJECT(dialog), "record");
	
	if ( button_number == 0 ) {	/* OK */
		record_add(t, r, FALSE, FALSE);
		update_windows(win);
		fill_record_field(wid, f, -(r->id), f_n);
		
	} else {			/* Cancel, we free */
		for ( i=0; i < t->nb_fields; i++ ) {
			if ( r->cont[i].anything == NULL )
				continue;
			switch ( t->fields[i].type ) {
				case T_STRING:
				case T_STRINGS:
				{
					g_string_free(r->cont[i].str, 1);
				} break;
				case T_DATE:
				{
					if ( r->cont[i].date != NULL )
						g_date_free(r->cont[i].date);
				} break;
				default: break;
			}
		}
		g_free(r->cont);
		g_free(r);
	}
#ifdef USE_GNOME
	gnome_dialog_close(GNOME_DIALOG(dialog));
#endif
}

#ifndef USE_GNOME
static void record_field_dialog_gtk(GtkWidget *button, gint button_number)
{
	GtkWidget *dialog = gtk_object_get_data(GTK_OBJECT(button), "dialog");

	record_field_dialog(dialog, button_number);
	gtk_widget_destroy(dialog);
}
#endif
	
static void record_field_refresh_clicked (GtkWidget *but, st_field *sf)
{
	GtkWidget *recordwid = gtk_object_get_data(GTK_OBJECT(but),"recordwid");
	gabywindow *window=gtk_object_get_data(GTK_OBJECT(recordwid), "window");
	int fn = 0;
	subtable *st = window->view->subtable;
	int i;
	
	for ( i=0; i<st->nb_fields;i++ ) {
		if ( sf == &st->fields[i] ) {
			fn = i;
			break;
		}
	}

	fill_record_field(recordwid, st, window->id, fn);
}
	
static void record_field_new_clicked (GtkWidget *but, st_field *sf)
{
	GtkWidget *recordwid = gtk_object_get_data(GTK_OBJECT(but),"recordwid");
	gabywindow *window =gtk_object_get_data(GTK_OBJECT(recordwid),"window");
	GtkWidget *form_box = window->widget;
	GtkWidget *dialog;
	GtkWidget *vbox;
	GtkWidget *parent = window->parent;
	GList *views = list_views;
	view *form = window->view;
	table *t = (field_get_property( &form->subtable->table->fields[sf->no], "from"))->val;
	view *vi = NULL;
	GtkWidget *pp = NULL;
	record *r = NULL;
	GString *title;
	int i;
	gabywindow *newwindow;
#ifndef USE_GNOME
	GtkWidget *button;
#endif

#ifdef DEBUG_GABY
	debug_print("[form:rfnc] form_box : %p, parent : %p\n", form_box, parent);
#endif

	while ( views != NULL ) {
		vi = (view*)views->data;
		views = g_list_next(views);
		
		if ( vi->subtable->table != t )
			continue;
		if ( strcmp(vi->type->name, "miniform") != 0 )
			continue;
#if 1
		newwindow = g_new0(gabywindow, 1);
		newwindow->view = vi;
		vi->type->view_create ( newwindow, FALSE );
		pp = newwindow->widget;
		r = g_new0(record, 1);
		r->cont = g_new0(union data, t->nb_fields);
		gtk_object_set_data(GTK_OBJECT(pp), "record", r);
#endif
		break;
	}

	title = g_string_new(_("New record in subtable : "));
	title =	g_string_append(title, vi->subtable->i18n_name);

#ifdef USE_GNOME
	dialog = gnome_dialog_new(title->str,
			GNOME_STOCK_BUTTON_OK,
			GNOME_STOCK_BUTTON_CANCEL ,
/*			GNOME_STOCK_BUTTON_HELP	,	*/
			NULL);
	if ( parent != NULL ) {
		gnome_dialog_set_parent(GNOME_DIALOG(dialog),
				GTK_WINDOW(parent));
	}

	gtk_signal_connect(GTK_OBJECT(dialog), "clicked",
					record_field_dialog, NULL);
	
	vbox = GNOME_DIALOG(dialog)->vbox;
	

#else /* ! USE_GNOME */
	
	dialog = gtk_dialog_new();
	gtk_window_set_title(GTK_WINDOW(dialog), title->str );

	vbox = GTK_DIALOG(dialog)->vbox;

	button = gtk_button_new_with_label(_("OK"));
	gtk_object_set_data(GTK_OBJECT(button), "dialog", dialog);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
			button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
				record_field_dialog_gtk, GINT_TO_POINTER(0));
	gtk_widget_show(button);
	
	button = gtk_button_new_with_label(_("Cancel"));
	gtk_object_set_data(GTK_OBJECT(button), "dialog", dialog);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
			button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
				record_field_dialog_gtk, GINT_TO_POINTER(1));
	gtk_widget_show(button);
		
#endif /* ! USE_GNOME */
	
	g_string_free(title, 1);
	
	gtk_object_set_data(GTK_OBJECT(dialog), "table", t);
	gtk_object_set_data(GTK_OBJECT(dialog), "subtable", form->subtable);
	gtk_object_set_data(GTK_OBJECT(dialog), "widget", recordwid);

	for ( i=0; i<form->subtable->nb_fields;i++ ) {
		if ( sf == &form->subtable->fields[i] ) {
			gtk_object_set_data(GTK_OBJECT(dialog),
						"f_n", GINT_TO_POINTER(i));
		}
	}
	
	if ( pp == NULL ) {
		g_print("Unable to find a convenient little form.\n");
		g_free(r->cont);
		g_free(r);
#ifdef USE_GNOME
		gnome_dialog_close(GNOME_DIALOG(dialog));
#else
		gtk_widget_destroy(dialog);
#endif
	} else {
		gtk_object_set_data(GTK_OBJECT(dialog), "record", r);
		gtk_box_pack_start(GTK_BOX(vbox), pp, TRUE, TRUE, 0 );
		gtk_widget_show(dialog);
	}
	
}

static GtkWidget* create_record_field( st_field *f, gabywindow *window)
{
#if 0
	GtkWidget *box = gtk_hbox_new(FALSE,0);
	GtkWidget *entry;
	GtkWidget *button;

	gtk_object_set_data(GTK_OBJECT(box), "field", f);
	
	entry = gtk_entry_new();
	gtk_entry_set_editable(GTK_ENTRY(entry), FALSE);
	gtk_box_pack_start (GTK_BOX(box), entry, TRUE, TRUE, 0);
	gtk_object_set_data(GTK_OBJECT(box), "entry", entry);
	gtk_widget_show(entry);
	
	button = gtk_button_new_with_label(_("..."));
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
					record_field_clicked, box);
	gtk_box_pack_start (GTK_BOX(box), button, TRUE, TRUE, 0);
	gtk_object_set_data(GTK_OBJECT(box), "button", button);
	gtk_widget_show(button);
	
	return box;
#else
	GtkWidget *combo;
	GtkWidget *box;
	GtkWidget *button;

	box = gtk_hbox_new(FALSE, 0);
	gtk_object_set_data(GTK_OBJECT(box), "window", window );
	
	combo = gtk_combo_new();
	gtk_entry_set_editable(GTK_ENTRY(GTK_COMBO(combo)->entry), FALSE);
	gtk_signal_connect(GTK_OBJECT(GTK_COMBO(combo)->entry), "changed",
			field_changed, window );
	gtk_box_pack_start(GTK_BOX(box), combo, TRUE, TRUE, 0 );
	gtk_object_set_data(GTK_OBJECT(box), "combo", combo );
	gtk_widget_show(combo);
	
	button = gtk_button_new_with_label(_("New"));
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
					record_field_new_clicked, f);
	gtk_container_set_border_width (GTK_CONTAINER (button), 5);
	gtk_object_set_data(GTK_OBJECT(button), "recordwid", box );
	gtk_box_pack_start(GTK_BOX(box), button, TRUE, TRUE, 0 );
	gtk_widget_show(button);
	
	button = gtk_button_new_with_label(_("Refresh"));
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
					record_field_refresh_clicked, f);
	gtk_container_set_border_width (GTK_CONTAINER (button), 5);
	gtk_object_set_data(GTK_OBJECT(button), "recordwid", box );
	gtk_box_pack_start(GTK_BOX(box), button, TRUE, TRUE, 0 );
	gtk_widget_show(button);
	
	return box;
#endif
	
}

static void file_field_select_file_cancel(GtkWidget *but, GtkFileSelection *fs)
{
	gtk_widget_destroy(GTK_WIDGET(fs));
}

static void file_field_select_file_ok(GtkWidget *but, GtkWidget *entry)
{
	GtkWidget *fs = but;
	gchar *st;
	
	while ( fs->parent ) fs = fs->parent;
	
	st = g_strdup(gtk_file_selection_get_filename(GTK_FILE_SELECTION(fs)));
	gtk_entry_set_text(GTK_ENTRY(entry), st);

	gtk_widget_destroy(fs);
}

static void file_field_select_file(GtkWidget *but, GtkWidget *entry)
{
	GtkFileSelection *fs;
	
	fs = GTK_FILE_SELECTION(gtk_file_selection_new(_("Select a file")));
	gtk_file_selection_set_filename(fs,
					gtk_entry_get_text(GTK_ENTRY(entry)));
	
	gtk_signal_connect(GTK_OBJECT(fs->ok_button), "clicked",
					file_field_select_file_ok, entry );
	gtk_signal_connect(GTK_OBJECT(fs->cancel_button), "clicked",
					file_field_select_file_cancel, fs );
/*	gtk_widget_hide(fs->help_button);	*/
	gtk_file_selection_hide_fileop_buttons(fs);

	gtk_widget_show(GTK_WIDGET(fs));
}

static void mm_field_select_file_cancel(GtkWidget *but, GtkFileSelection *fs)
{
	gtk_widget_destroy(GTK_WIDGET(fs));
}

static void recordslist_field_dialog(GtkWidget *dialog, gint button_number)
{
	record *r;
	int i;
	table *t = gtk_object_get_data(GTK_OBJECT(dialog), "table");
	GtkWidget *wid = gtk_object_get_data(GTK_OBJECT(dialog), "widget");
	gabywindow *win = gtk_object_get_data(GTK_OBJECT(wid), "window-parent");
	
#ifdef DEBUG_GABY
	debug_print("[gaby:form:rfd] button : %d\n", button_number);
#endif
	r = gtk_object_get_data(GTK_OBJECT(dialog), "record");
	
	if ( button_number == 0 ) {	/* OK */
		record_add(t, r, FALSE, FALSE);
		update_windows(win);
		form_fill(win);
	} else {			/* Cancel, we free */
		for ( i=0; i < t->nb_fields; i++ ) {
			if ( r->cont[i].anything == NULL )
				continue;
			switch ( t->fields[i].type ) {
				case T_STRING:
				case T_STRINGS:
				{
					g_string_free(r->cont[i].str, 1);
				} break;
				case T_DATE:
				{
					if ( r->cont[i].date != NULL )
						g_date_free(r->cont[i].date);
				} break;
				default: break;
			}
		}
		g_free(r->cont);
		g_free(r);
	}
#ifdef USE_GNOME
	gnome_dialog_close(GNOME_DIALOG(dialog));
#endif
}

#ifndef USE_GNOME
static void recordslist_field_dialog_gtk(GtkWidget *button, gint button_number)
{
	GtkWidget *dialog = gtk_object_get_data(GTK_OBJECT(button), "dialog");

	recordslist_field_dialog(dialog, button_number);
	gtk_widget_destroy(dialog);
}
#endif

static void recordslist_add_clicked (GtkWidget *but, st_field *sf)
{
	/* creating a 'miniform' */
	GtkWidget *listwid = gtk_object_get_data(GTK_OBJECT(but), "listwid");
	gabywindow *window = gtk_object_get_data(GTK_OBJECT(listwid), "window-parent");
	GtkWidget *dialog;
	GtkWidget *vbox;
	GList *views = list_views;
	table *t;
	view *form = window->view;
	view *vi = NULL;
	GtkWidget *pp = NULL;
	record *r = NULL;
	GString *title;
	int i;
	gabywindow *newwindow;
#ifndef USE_GNOME
	GtkWidget *button;
#endif

	if ( window->updated ) form_save(window);
	if ( window->id == 0 ) {
		debug_print("we just saved and it is still at 0; not good\n");
		gaby_errno = CUSTOM_ERROR;
		gaby_message = g_strdup(_("You need to be on a saved record\nto add a record in this list!"));
		gaby_perror_in_a_box();
		return;
	}

	t = sf->v->subtable->table;

	while ( views != NULL ) {
		vi = (view*)views->data;
		views = g_list_next(views);
		
		if ( vi->subtable->table != t )
			continue;
		if ( strcmp(vi->type->name, "miniform") != 0 )
			continue;
#if 1
		newwindow = g_new0(gabywindow, 1);
		newwindow->view = vi;
		vi->type->view_create ( newwindow, FALSE );
		pp = newwindow->widget;
		r = g_new0(record, 1);
		r->cont = g_new0(union data, t->nb_fields);
		gtk_object_set_data(GTK_OBJECT(pp), "record", r);
		
		{
			/* I need to remember how I made T_RECORDS work :( */
			/* the question is 'why a GList ?' */
			GList *rel = g_list_first(sf->link_format);
			int field_nb = GPOINTER_TO_INT(rel->data);
			r->cont[field_nb].i = window->id;
		}

			
#endif
		break;
	}

	title = g_string_new(_("New record in subtable : "));
	title =	g_string_append(title, vi->subtable->i18n_name);

#ifdef USE_GNOME
	dialog = gnome_dialog_new(title->str,
			GNOME_STOCK_BUTTON_OK,
			GNOME_STOCK_BUTTON_CANCEL ,
/*			GNOME_STOCK_BUTTON_HELP	,	*/
			NULL);
	if ( window->parent != NULL ) {
		gnome_dialog_set_parent(GNOME_DIALOG(dialog), 
						GTK_WINDOW(window->parent));
	}

	gtk_signal_connect(GTK_OBJECT(dialog), "clicked",
					recordslist_field_dialog, NULL);
	
	vbox = GNOME_DIALOG(dialog)->vbox;

#else /* ! USE_GNOME */
	
	dialog = gtk_dialog_new();
	gtk_window_set_title(GTK_WINDOW(dialog), title->str );

	vbox = GTK_DIALOG(dialog)->vbox;

	button = gtk_button_new_with_label(_("OK"));
	gtk_object_set_data(GTK_OBJECT(button), "dialog", dialog);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
			button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
			recordslist_field_dialog_gtk, GINT_TO_POINTER(0));
	gtk_widget_show(button);
	
	button = gtk_button_new_with_label(_("Cancel"));
	gtk_object_set_data(GTK_OBJECT(button), "dialog", dialog);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area),
			button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
			recordslist_field_dialog_gtk, GINT_TO_POINTER(1));
	gtk_widget_show(button);
		
#endif /* ! USE_GNOME */
	
	g_string_free(title, 1);
	
	gtk_object_set_data(GTK_OBJECT(dialog), "table", t);
	gtk_object_set_data(GTK_OBJECT(dialog), "subtable", vi->subtable);
	gtk_object_set_data(GTK_OBJECT(dialog), "widget", listwid);

	for ( i=0; i<form->subtable->nb_fields;i++ ) {
		if ( sf == &form->subtable->fields[i] ) {
			gtk_object_set_data(GTK_OBJECT(dialog),
						"f_n", GINT_TO_POINTER(i));
		}
	}
	
	if ( pp == NULL ) {
		g_print("Unable to find a convenient little form.\n");
		g_free(r->cont);
		g_free(r);
#ifdef USE_GNOME
		gnome_dialog_close(GNOME_DIALOG(dialog));
#else
		gtk_widget_destroy(dialog);
#endif
	} else {
		gtk_object_set_data(GTK_OBJECT(dialog), "record", r);
		gtk_box_pack_start(GTK_BOX(vbox), pp, TRUE, TRUE, 0 );
		gtk_widget_show(dialog);
	}
	
}

static void recordslist_remove_clicked (GtkWidget *but, st_field *sf)
{
	GtkWidget *listwid = gtk_object_get_data(GTK_OBJECT(but), "listwid");
	gabywindow *rwindow = gtk_object_get_data(GTK_OBJECT(listwid), "window-parent");
	gabywindow *win = gtk_object_get_data(GTK_OBJECT(listwid), "window");
	GList *tmp;
	ViewPluginData *vpd;

	record_remove_id(win->view->subtable->table, win->id);
	
	tmp = win->what;
	if ( tmp != NULL ) g_list_free(tmp);
	win->what = get_related_records(sf, rwindow->id);
	vpd = win->view->type;
	vpd->view_fill(win);
}

static GtkWidget* create_recordslist_field( st_field *f, gabywindow *window)
{
	GtkWidget *box, *bbox;
	GtkWidget *wid;
	gabywindow *sublist;
	GtkWidget *button;
	
	sublist = g_new0(gabywindow, 1);
	sublist->view = f->v;
	sublist->parent = window->widget;
	sublist->name = g_strdup("{[--sublist--]}");

	f->v->type->view_create(sublist, FALSE);
	/* I don't know if it is a good idea ... */
	list_windows = g_list_append(list_windows, sublist);

	wid = sublist->widget;
	
	box = gtk_vbox_new(FALSE, 5);
	gtk_object_set_data(GTK_OBJECT(box), "window-parent", window);
	gtk_box_pack_start(GTK_BOX(box), wid, TRUE, TRUE, 5 );
	gtk_widget_show(box);

	bbox = gtk_hbutton_box_new();
	gtk_box_pack_start(GTK_BOX(box), bbox, TRUE, TRUE, 5 );
	gtk_widget_show(bbox);
	
	button = gtk_button_new_with_label(_("Add"));
	gtk_object_set_data(GTK_OBJECT(button), "listwid", box);
	gtk_box_pack_start(GTK_BOX(bbox), button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
						recordslist_add_clicked, f);
	gtk_widget_show(button);
	
#if 0
	button = gtk_button_new_with_label(_("Modify"));
	gtk_widget_set_sensitive(button, FALSE);
	gtk_box_pack_start(GTK_BOX(bbox), button, TRUE, TRUE, 0 );
	gtk_widget_show(button);
#endif
		
	button = gtk_button_new_with_label(_("Remove"));
	gtk_object_set_data(GTK_OBJECT(button), "listwid", box);
	/*gtk_widget_set_sensitive(button, FALSE);*/
	gtk_box_pack_start(GTK_BOX(bbox), button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
						recordslist_remove_clicked, f);
	gtk_widget_show(button);
	
	gtk_object_set_data(GTK_OBJECT(box), "window", sublist);
	gtk_widget_set_usize(wid, 190, 100 );
	gtk_widget_set_usize(box, 210, 150 );
	
	return box;
}

static void mm_field_select_file_ok(GtkWidget *but, GtkFileSelection *fs)
{
	GtkWidget *child = gtk_object_get_data(GTK_OBJECT(fs), "child");
	gabywindow *window = gtk_object_get_data(GTK_OBJECT(fs), "window");
	gchar *type = gtk_object_get_data(GTK_OBJECT(child), "type");
	
	gtk_object_set_data(GTK_OBJECT(child), "value",
			g_strdup(gtk_file_selection_get_filename(fs)));
	if ( type == NULL || strcmp(type, "image") == 0 ) {
		field_image_load(child, gtk_file_selection_get_filename(fs));
	} if ( strcmp(type, "sound") == 0 ) {
		/* nothing to do for sounds */;
	}
	gtk_widget_destroy(GTK_WIDGET(fs));
	
	window->updated = TRUE;
}


static void mm_field_select_file (GtkWidget *but, GtkWidget *child)
{
	/* when the gnome file open dialog box will be finished I'll have one
	 * more #ifdef USE_GNOME but for now, it's only GtkFileSelection */
	GtkFileSelection *fs;
	gabywindow *win = gtk_object_get_data(GTK_OBJECT(but), "window");
	
	fs = GTK_FILE_SELECTION(gtk_file_selection_new(_("Select a file")));
	gtk_object_set_data(GTK_OBJECT(fs), "child", child);
	gtk_object_set_data(GTK_OBJECT(fs), "window", win);
	gtk_file_selection_set_filename(fs,
			(gchar*)gtk_object_get_data(GTK_OBJECT(child),"value"));
	
	gtk_signal_connect(GTK_OBJECT(fs->ok_button), "clicked",
			mm_field_select_file_ok, fs );
	gtk_signal_connect(GTK_OBJECT(fs->cancel_button), "clicked",
			mm_field_select_file_cancel, fs);
/*	gtk_widget_hide(fs->help_button);	*/
	gtk_file_selection_hide_fileop_buttons(fs);

	gtk_widget_show(GTK_WIDGET(fs));
	
}

static GtkWidget* create_multimedia_field( st_field *f, gabywindow *window)
{
	GtkWidget *but;
	GtkWidget *table;
	GtkWidget *child = NULL;
	property *prop;
	gchar *type;
	view *v = window->view;

	table = gtk_table_new(1, 2, FALSE);
	but = gtk_button_new_with_label("...");
	gtk_widget_show(but);
	gtk_table_attach (GTK_TABLE (table), but, 1, 2, 0, 1, 0, 0, 2, 2);

	prop = field_get_property( &v->subtable->table->fields[f->no], "type" );
	type = ( prop != NULL ) ? prop->val : NULL;

	if ( type == NULL || strcmp(type, "image") == 0 ) {
		child = gtk_drawing_area_new();
		gtk_signal_connect (GTK_OBJECT(child), "configure_event",
				(GtkSignalFunc) field_image_config, NULL);
	} else if ( strcmp(type, "sound") == 0 ) {
		child = gtk_button_new_with_label(_("Play sound"));
		gtk_signal_connect (GTK_OBJECT(child), "clicked",
				(GtkSignalFunc) play_sound, NULL);
	}
	gtk_widget_show(child);
	gtk_object_set_data(GTK_OBJECT(table), "child", child);
	gtk_table_attach (GTK_TABLE (table), child, 0, 1, 0, 1,
			GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	gtk_object_set_data(GTK_OBJECT(child), "type", type);
	gtk_object_set_data(GTK_OBJECT(but), "window", window);
	gtk_signal_connect(GTK_OBJECT(but), "clicked",
					mm_field_select_file, child );

	gtk_widget_show(table);
	return table;
}

static void sort_clicked (GtkWidget *wid, GtkWidget *mi)
{
	int *sort_by;
	int new_sort_by;
	GtkWidget **menu_items;
	
	sort_by = gtk_object_get_data(GTK_OBJECT(wid), "sort_by");
	new_sort_by = GPOINTER_TO_INT(
			gtk_object_get_data(GTK_OBJECT(mi), "which"));

	menu_items = gtk_object_get_data(GTK_OBJECT(wid), "sort_menu_items");

	gtk_item_toggle(GTK_ITEM(menu_items[*sort_by+1])); 
	gtk_item_toggle(GTK_ITEM(menu_items[new_sort_by+1])); 

	*sort_by = new_sort_by;
	
}

static GtkWidget* create_sort_menu ( gabywindow *window )
{
	GtkWidget *menu;
	GtkWidget **menu_items;
	GtkWidget *menu_item;
	view *v;
	table *t;
	int i;
	GSList *group = NULL;
	
	v = window->view;
	t = v->subtable->table;
	
	menu = gtk_menu_new();

	menu_items = g_malloc(sizeof(GtkWidget*)*(t->nb_fields+1));
	
		/* kind of sort :)*/
	menu_items[0] = gtk_radio_menu_item_new_with_label( group, _("None") );
	
	group = gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM (menu_items[0]));
	
	gtk_menu_append(GTK_MENU(menu), menu_items[0]);
	gtk_object_set_data(GTK_OBJECT(menu_items[0]),"which", (gpointer)(-1) );
	gtk_signal_connect_object(GTK_OBJECT(menu_items[0]), "activate",
		GTK_SIGNAL_FUNC(sort_clicked), (gpointer)(window->widget));
	gtk_widget_show(menu_items[0]);

	menu_item = gtk_menu_item_new();
	gtk_menu_append(GTK_MENU(menu), menu_item);
	gtk_widget_show(menu_item);

	for (i=0; i < t->nb_fields; i++) {
		menu_items[i+1] = gtk_radio_menu_item_new_with_label( group, 
						t->fields[i].i18n_name);
		group = gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM 
							(menu_items[i+1]));
		gtk_menu_append(GTK_MENU(menu), menu_items[i+1]);
		
		gtk_object_set_data(GTK_OBJECT(menu_items[i+1]), "which",
				GINT_TO_POINTER(i) );
		gtk_signal_connect_object(GTK_OBJECT(menu_items[i+1]),
				"activate", GTK_SIGNAL_FUNC(sort_clicked),
				(gpointer)(window->widget));
		gtk_widget_show(menu_items[i+1]);
	}

	gtk_object_set_data(GTK_OBJECT(window->widget), "sort_menu_items", menu_items );

	return menu;
}

mstatic void form_fill ( gabywindow *window )
{
	GtkWidget *wid = window->widget;
	GtkWidget **right;
	subtable *f = window->view->subtable;
	int *id = &(window->id);
	
	debug_print("[form:fill] filling with record %d\n", *id);

	right = gtk_object_get_data(GTK_OBJECT(wid), "right");
	
	form_fill_real(right, f, *id);
#ifdef DEBUG_GABY
	debug_print("[form:fill] end\n");
#endif
	window->updated = FALSE;
}

static void form_fill_real(GtkWidget *w[], subtable *f, int id)
{
	int i;
	record *r;
	GtkAdjustment *adj;
	GString *str;
	ViewPluginData *vpd;
	int type;
	GList *tmp;
	gabywindow *sublist;

	if ( id == 0 ) {
		form_empty(w, list_tables, f);
		r = record_defaults(f->table);
	} else {
		r = get_record_no(f->table, id);
		if ( r == NULL ) {
			form_fill_real(w, f, 0);
			return;
		}
	}

	for ( i=0; i<f->nb_fields; i++ ) {
		if ( id == 0 && r->cont[f->fields[i].no].anything == NULL )
			continue;
#ifdef DEBUG_GABY
		debug_print("filling field (type %d) (n� %d)\n",
			f->fields[i].type,
			f->fields[i].no );
		if ( f->fields[i].type == T_STRING ) {
			debug_print("%p  ", r->cont[f->fields[i].no].str);
			if ( r->cont[f->fields[i].no].str != NULL ) {
/*				debug_print("%p ", r->cont[f->fields[i].no].str->str);
				debug_print("%s\n", r->cont[f->fields[i].no].str->str);*/
			}
		}
#endif
		type = f->fields[i].type;
		if      ( type == T_STRINGS && ! text_for_strings  )
							type = T_STRING;
		else if ( type == T_DATE    && ! cal_for_date      )
							type = T_STRING;
		else if ( type == T_INTEGER && ! spin_for_numeric  )
							type = T_STRING;
		else if ( type == T_REAL    && ! spin_for_numeric  )
							type = T_STRING;
		else if ( type == T_BOOLEAN && ! check_for_boolean )
							type = T_STRING;
#if !defined HAVE_ESD && ! ( defined HAVE_IMLIB || defined HAVE_GDKPIXBUF )
		else if ( type == T_MULTIMEDIA )	type = T_STRING;
#endif
		else if ( type == T_DECIMAL )		type = T_STRING;
		else if ( type == T_FILE )		type = T_STRING;
	
		switch ( type ) {
			case T_STRING:
			{
				str = get_subtable_stringed_field(f, r, i );
				gtk_entry_set_text(GTK_ENTRY(w[i]), str->str);
				g_string_free(str, 1);
			} break;
			case T_BOOLEAN:
			{
				gtk_toggle_button_set_active (
						GTK_TOGGLE_BUTTON(w[i]),
						r->cont[f->fields[i].no].b );
			} break;
			case T_STRINGS:
			{
				gtk_text_set_point(GTK_TEXT(w[i]), 0);
				gtk_text_forward_delete(GTK_TEXT(w[i]),
					gtk_text_get_length(GTK_TEXT(w[i])));
				gtk_text_insert(GTK_TEXT(w[i]), NULL,
					NULL, NULL,
					r->cont[f->fields[i].no].str->str,-1);
				gtk_text_set_point(GTK_TEXT(w[i]), 0);
			} break;
			case T_DATE:
			{
				if ( r->cont[f->fields[i].no].date != NULL ) {
				   gtk_calendar_select_month(GTK_CALENDAR(w[i]),
					g_date_month(r->cont[f->fields[i].no].date) - 1,
					g_date_year(r->cont[f->fields[i].no].date));
				   gtk_calendar_select_day( GTK_CALENDAR(w[i]),
					g_date_day(r->cont[f->fields[i].no].date) );
				} else {
					/*
					 *  I don't believe there is a way to
					 *  tell GtkCalendar that there are
					 *  _NO_ dates to show.
					 *  So we have to show something and
					 *  this might be a default date for
					 *  the field (if given) or an
					 *  arbitrary date decided right now.
					 *  Currently it is the 2nd way which
					 *  is far quicker to implement. The
					 *  date is january 1st 1999 but
					 *  something like 'today' would be
					 *  nice :)
					 *
					 *  Update (June 14th 1999):
					 *  Another problem _was_ that the
					 *  default value was saved, this
					 *  should now be fixed with the
					 *  'changed' attribute but ymmv
					 */
					gtk_calendar_select_month(
						GTK_CALENDAR(w[i]), 0, 1999);
					gtk_calendar_select_day(
						GTK_CALENDAR(w[i]), 1);
				}
/*				gtk_object_set_data(GTK_OBJECT(w[i]),
					"changed", GINT_TO_POINTER(FALSE));*/
			} break;
			case T_INTEGER:
			{
				adj = gtk_spin_button_get_adjustment(GTK_SPIN_BUTTON(w[i]));
				gtk_adjustment_set_value(adj, r->cont[f->fields[i].no].i);
				gtk_spin_button_set_adjustment(GTK_SPIN_BUTTON(w[i]), adj);
			} break;
			case T_REAL:
			{
				adj = gtk_spin_button_get_adjustment(GTK_SPIN_BUTTON(w[i]));
				gtk_adjustment_set_value(adj, r->cont[f->fields[i].no].d);
				gtk_spin_button_set_adjustment(GTK_SPIN_BUTTON(w[i]), adj);
	
			} break;
			case T_RECORD:
			{
#if 0
				gtk_object_set_data(GTK_OBJECT(w[i]), "r_id",
						GINT_TO_POINTER(
						r->cont[f->fields[i].no].i));
				wid = gtk_object_get_data(GTK_OBJECT(w[i]),
						"entry" );
#endif
				fill_record_field(w[i], f, id, i);
			} break;
			case T_RECORDS:
			{
				sublist = gtk_object_get_data (
						GTK_OBJECT(w[i]), "window" );
				tmp = sublist->what;
				if ( tmp != NULL ) g_list_free(tmp);
				sublist->what =
					get_related_records(&f->fields[i], id);
				vpd = sublist->view->type;
#ifdef DEBUG_GABY
				debug_print("[form:ffr] calling view_fill ...");
#endif
				vpd->view_fill ( sublist );
#ifdef DEBUG_GABY
				debug_print("done.\n");
#endif
			} break;
			case T_MULTIMEDIA:
			{
				fill_multimedia_field(w[i], f, id, i);
			} break;
			default:
			{
				;
			} break;
		}
	}

	if ( id == 0 ) {
#ifdef DEBUG_GABY
		debug_print("[form:ffr] freeing r->cont\n");
#endif
		g_free(r->cont);
#ifdef DEBUG_GABY
		debug_print("[form:ffr] freeing r\n");
#endif
		g_free(r);
	}

}

static void fill_record_field(GtkWidget *w, subtable *f, int id, int field_no)
{
	GString *str;
	record *r;
	GtkCombo *combo = GTK_COMBO(gtk_object_get_data(GTK_OBJECT(w),"combo"));
	GtkWidget *li;
	int i;
	table *t;
	char *oldstr;

	/* 
	 * I don't use gtk_combo_set_popdown_strings which is the easy
	 * way to fill a gtkcombo but which needs a full GList of what
	 * to put in.
	 */

#ifdef DEBUG_GABY
	debug_print("[form:frf] w : %p, f : %s, id : %d, field_no : %d\n",
			w, f->i18n_name, id, field_no );
#endif
	gtk_list_clear_items(GTK_LIST(combo->list), 0, -1 );
/*	t = f->table->fields[f->fields[field_no].no].more.from;*/
	t = (field_get_property( &f->table->fields[f->fields[field_no].no],
				"from"))->val;
	
	str = g_string_new("");
	li = gtk_list_item_new_with_label(str->str);
	gtk_widget_show(li);
	gtk_container_add(GTK_CONTAINER(combo->list), li);
	g_string_free(str, 1);
		
	for ( i=0; i < t->max_records; i++ ) {
		r = t->records[i];
		if ( r == NULL || r->id == 0 ) 
			continue;
		
		str = get_subtable_record_field(f, r, field_no);
		
		li = gtk_list_item_new_with_label(str->str);
		gtk_widget_show(li);
		gtk_container_add(GTK_CONTAINER(combo->list), li);
	
		g_string_free(str, 1);
	}
	
	if ( id == 0 ) {
		gtk_entry_set_text(GTK_ENTRY(combo->entry), "");
	} else {
		if ( id > 0 ) {
			str = get_subtable_stringed_field_id(f, id, field_no );
		} else {
			id = -id;
			r = get_record_no(t, id);
			str = get_subtable_record_field(f, r, field_no);
		}
		oldstr = gtk_entry_get_text(GTK_ENTRY(combo->entry));
		if ( strcmp(str->str, oldstr) != 0 )
			gtk_entry_set_text(GTK_ENTRY(combo->entry), str->str);
		g_string_free(str, 1);
	}

	gtk_object_set_data(GTK_OBJECT(w), "r_id", GINT_TO_POINTER(id));
	
}

static void field_sound_load(GtkWidget *wi, gchar *filename)
{
	if ( strlen(filename) == 0 ) {
		gtk_widget_set_sensitive(wi, FALSE);
	} else {
		gtk_widget_set_sensitive(wi, TRUE);
	}
}

static void play_sound (GtkWidget *but, gpointer data)
{
	gchar *filename = gtk_object_get_data(GTK_OBJECT(but), "value");

#ifdef DEBUG_GABY
	debug_print("[form:play_sound] playing %s\n", filename);
#endif

#ifdef USE_GNOME

#ifdef HAVE_ESD
	esd_play_file("gaby", filename, 1);
#else
	gnome_sound_play(filename);	/* it used to work :( */
#endif /* ! HAVE_ESD_H */

#else /* ! USE_GNOME */
	
#ifdef HAVE_ESD
	/* 'cause without it you can't have sound (well, you can but it's
	 * harder for me)
	 */
	
#if 0	/* 'cause it doesn't work without for undetermined reasons ,
	 * I believe this has to do with a mismatch between linked libraries
	 * for this one and for gaby but I am not sure
	 */
	
	/*
	 * this is nice but it freezes gaby while playing long file (buffer too
	 * small in esd/audiofile ?)
	 */
	esd_play_file("gaby", filename, 1);
#endif /* 0 */
	
#endif /* HAVE_ESD */

#endif /* ! USE_GNOME */
}

static void field_image_load(GtkWidget *wi, gchar *filename)
{
#if defined(HAVE_GDKPIXBUF) || defined(HAVE_IMLIB)

#  ifdef HAVE_GDKPIXBUF
	GdkPixbuf *im = NULL;
	GError *error;
#  else
	GdkImlibImage *im = NULL;
#  endif
	GdkPixmap *pmap = NULL, *mask = NULL;
	int ww, hh;
	
	debug_print("[form:field_image_load] begin (filename = %s)\n",filename);
	
	if ( wi->window == NULL ) {
		return;
	}

#  ifdef HAVE_GDKPIXBUF
	im = gdk_pixbuf_new_from_file(filename, &error);
	if ( ! im ) {
		debug_print("[form:field_image_load] image failed to load\n");
		im = gdk_pixbuf_new_from_xpm_data((const char**)notfound_xpm);
	}
	
	ww = gdk_pixbuf_get_width(im);
	hh = gdk_pixbuf_get_height(im);
		
	gtk_widget_set_usize(wi, ww, hh);

	gdk_pixbuf_render_pixmap_and_mask(im, &pmap, &mask, 1);
	gdk_window_set_back_pixmap(wi->window, pmap, FALSE);
	gdk_window_clear(wi->window);
	gdk_window_shape_combine_mask(wi->window, mask, 0, 0);

#  else /* ! HAVE_GDKPIXBUF */

	/* copied (then adapted) from Electric Eyes (by Rasterman) since
	 * gdk_imlib documentation isn't documentation :)
	 */
	im = gdk_imlib_load_image(filename);
	if ( ! im ) {
		debug_print("[form:field_image_load] image failed to load\n");
		im = gdk_imlib_create_image_from_xpm_data(notfound_xpm);
	}

	if ( im ) {
		ww = im->rgb_width;
		hh = im->rgb_height;

		/*
		 * this does _not_ reduce the size if (ww,hh) < (oldww,oldhh)
		 * and I don't know how to reduce the size :(
		 * 
		 * (I tried gdk_window_resize, gtk_widget_queue_resize,
		 * gtk_widget_size_request, ...)
		 * 
		 */
		gtk_widget_set_usize(wi, ww, hh);

		gdk_imlib_render(im, ww, hh);
		pmap = gdk_imlib_move_image(im);
		mask = gdk_imlib_move_mask(im);
		gdk_window_set_back_pixmap(wi->window, pmap, FALSE);
		gdk_window_clear(wi->window);
		gdk_window_shape_combine_mask(wi->window, mask, 0, 0);
		gdk_imlib_free_pixmap(pmap);
	} else {
		gdk_window_clear(wi->window);
		gtk_widget_set_usize(wi, 30, 10);
	}
#  endif /* ! HAVE_GDKPIXBUF */
	
	debug_print("[form:field_image_load] end\n");

#endif /* defined(HAVE_GDKPIXBUF) || defined(HAVE_IMLIB) */

}

static gint field_image_config (GtkWidget *widget, GdkEventConfigure *event)
{
	gchar *name;
	static gboolean firsttime = TRUE;
	
	if ( firsttime == FALSE ) return TRUE;
#ifdef DEBUG_GABY
	debug_print("[form:field_mm_config] ...\n");
#endif
	
	name = gtk_object_get_data(GTK_OBJECT(widget), "value");

	field_image_load(widget, name);

	firsttime = FALSE;
	
	return TRUE;
}

static void fill_multimedia_field( GtkWidget *w, subtable *f, int id,
				   int field_no)
{
	GString *str;
	GtkWidget *child;
	gchar *type;
	
#ifdef DEBUG_GABY
	debug_print("[form:fill_multimedia_field] begin (%d %d)\n", id, field_no);
#endif
	
	str = get_subtable_stringed_field_id(f, id, field_no);

	child = gtk_object_get_data(GTK_OBJECT(w), "child");
	gtk_object_set_data(GTK_OBJECT(child), "value", g_strdup(str->str));
	
	type = gtk_object_get_data(GTK_OBJECT(child), "type");
	
	gtk_widget_show(child);
	if      ( type == NULL || strcmp((char*)(type), "image") == 0 )
		field_image_load(child, str->str);
	else if ( strcmp((char*)(type), "sound") == 0 )
		field_sound_load(child, str->str);
	
	g_string_free(str, 1);
	
#ifdef DEBUG_GABY
	debug_print("[form:fill_multimedia_field] end\n");
#endif

}

static void form_empty(GtkWidget *w[], GList *t, subtable *f)
{
	int i;
	int type;
	gabywindow *sublist;

	/*  this function does nothing more than emptying
	 *  (sp? grammar? :( ) the form, it is next filled in
	 *  form_fill_real with the default values
	 */
	
	for ( i=0; i<f->nb_fields; i++ ) {
		type = f->fields[i].type;
		
		if      (type==T_STRINGS && ! text_for_strings ) type=T_STRING;
		else if (type==T_DATE    && ! cal_for_date     ) type=T_STRING;
		else if (type==T_INTEGER && ! spin_for_numeric ) type=T_STRING;
		else if (type==T_REAL    && ! spin_for_numeric ) type=T_STRING;
		else if (type==T_BOOLEAN && ! check_for_boolean) type=T_STRING;
#ifndef USE_GNOME /* bad test, should be for Imlib and ESD */
		else if ( type == T_MULTIMEDIA )		 type=T_STRING;
#endif
		else if ( type == T_DECIMAL )			 type=T_STRING;
		else if ( type == T_FILE )			 type=T_STRING;
	
		switch ( type ) {
			case T_STRING:
			{
				gtk_entry_set_text(GTK_ENTRY(w[i]), "");
			} break;
			case T_STRINGS:
			{
				gtk_text_set_point(GTK_TEXT(w[i]), 0);
				gtk_text_forward_delete(GTK_TEXT(w[i]),
					gtk_text_get_length(GTK_TEXT(w[i])) );
			} break;
			case T_INTEGER:
			case T_REAL:
			{
				gtk_spin_button_set_value(
						GTK_SPIN_BUTTON(w[i]), 0 );
			} break;
			case T_RECORD:
			{
#if 0
				gtk_object_set_data(GTK_OBJECT(w[i]), "r_id", 0);
				wid = gtk_object_get_data(GTK_OBJECT(w[i]),
						"entry" );
#else
#endif
/*				gtk_entry_set_text(GTK_ENTRY(wid), "");
*/
				fill_record_field(w[i], f, 0, i);
			} break;
			case T_RECORDS:
			{
				sublist = gtk_object_get_data(
						GTK_OBJECT(w[i]), "window");
				sublist->what =
					get_related_records(&f->fields[i], -1);
				sublist->view->type->view_fill(sublist);
			} break;
			case T_MULTIMEDIA:
			{
				fill_multimedia_field(w[i], f, 0, i);
			} break;
			case T_DATE:
			{
/*				gtk_object_set_data(GTK_OBJECT(w[i]),"changed",\
						GINT_TO_POINTER(FALSE));*/
			} break;
			case T_BOOLEAN:
			{
				gtk_toggle_button_set_active (
						GTK_TOGGLE_BUTTON(w[i]), FALSE);
			} break;
		}
	}

}

#ifndef USE_GNOME
static void delete_button_clicked(GtkWidget *but, GtkWidget *dialog)
{
	int nb = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(but), "nb"));

	gtk_object_set_data(GTK_OBJECT(dialog), "nb", GINT_TO_POINTER(nb));
	gtk_widget_hide(dialog);

	gtk_main_quit(); /* this quit the modal dialog, not gaby :) */
}
#endif

static void form_toolbar_remove(GtkWidget *toolbar, gabywindow *window)
{
	GtkWidget *wi = window->widget;
	record *r;
	table *t;
	view *v;
	int *id, *sort_by;
	GtkWidget *d, *vbox, *l;
#ifndef USE_GNOME
	GtkWidget *but;
#endif
	int button = -1;
	
	if ( window->id == 0 ) return;

	if ( get_config_bool("view", "form", "config_deletion", TRUE) ) {
#ifdef USE_GNOME
		d = gnome_dialog_new(_("Warning"),
				GNOME_STOCK_BUTTON_YES,
				GNOME_STOCK_BUTTON_NO,
				NULL);
		vbox = GNOME_DIALOG(d)->vbox;
#else /* ! USE_GNOME */
		d = gtk_dialog_new();
		
		but = gtk_button_new_with_label(_("Yes"));
		gtk_object_set_data(GTK_OBJECT(but), "nb", GINT_TO_POINTER(0));
		gtk_signal_connect(GTK_OBJECT(but), "clicked",
				GTK_SIGNAL_FUNC(delete_button_clicked), d);
		gtk_widget_show(but);
		gtk_box_pack_start(GTK_BOX(GTK_DIALOG(d)->action_area),
				but, TRUE, TRUE, 0 );

		but = gtk_button_new_with_label(_("No"));
		gtk_object_set_data(GTK_OBJECT(but), "nb", GINT_TO_POINTER(1));
		gtk_signal_connect(GTK_OBJECT(but), "clicked",
				GTK_SIGNAL_FUNC(delete_button_clicked), d );
		gtk_widget_show(but);
		gtk_box_pack_start(GTK_BOX(GTK_DIALOG(d)->action_area),
				but, TRUE, TRUE, 0 );
		gtk_window_set_modal(GTK_WINDOW(d), TRUE);

		vbox = GTK_DIALOG(d)->vbox;
#endif /* ! USE_GNOME */
		
		l = gtk_label_new(_("Are you sure ?"));
		gtk_widget_show(l);
		gtk_box_pack_start (GTK_BOX (vbox), l, TRUE, TRUE, 0);
		
#ifdef USE_GNOME
		button = gnome_dialog_run (GNOME_DIALOG (d));
		gtk_widget_destroy(d);
#else /* ! USE_GNOME */
		gtk_widget_show(d);
		gtk_main();
		button=GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(d),"nb"));
#endif /* ! USE_GNOME */

#ifdef DEBUG_GABY
		debug_print("[form_toolbar_remove] button : %d\n", button);
#endif
		if ( button != 0 ) return;

	}

	id = &(window->id);

	sort_by = gtk_object_get_data(GTK_OBJECT(wi), "sort_by");
	
	v = window->view;
	t = v->subtable->table;

	if ( window->id == 0 ) {
		/* we were adding a record but we changed our mind */
		/* let's go back to the last record */
		
		r = table_last(t, *sort_by);
		if ( r != NULL ) window->id = r->id;
#ifdef DEBUG_GABY
		debug_print("id : %d\n", window->id);
#endif
		form_fill(window);

		return;
	}
	
	/* What record do you want to see today ? (I'll rewrite this)
	 * sth like :
	 *   if first -> newly first
	 *   else     -> previous record
	 */
	record_remove_id(t, window->id);
	r = table_first(t, *sort_by);
	if ( r != NULL ) window->id = r->id;

	form_fill(window);

	/* we warn other windows about this change */
	update_windows(window);
}

static void form_toolbar_first(GtkWidget *toolbar, gabywindow *window)
{
	GtkWidget *wi = window->widget;
	int *id;
	subtable *f = window->view->subtable;
	record *r;
	int *sort_by;
	
	sort_by = gtk_object_get_data(GTK_OBJECT(wi), "sort_by");
	
	id = &(window->id);
	
	if ( window->id == 0 ) {
		if ( form_record_save(window, NULL) ) {
			form_toolbar_first(toolbar, window);
			return;
		}
	} else {
		r = get_record_no(f->table, *id);
		if ( record_is_different(r, window) ) {
			form_record_save(window, r);
		}
	}
	
	r = table_first(f->table, *sort_by);
	window->id = ( r == NULL ) ? 0 : r->id;

	form_fill(window);
	update_bound_windows(window);
}

static void form_toolbar_last(GtkWidget *toolbar, gabywindow *window)
{
	GtkWidget *wi = window->widget;
	int *id;
	subtable *f = window->view->subtable;
	record *r;
	int *sort_by;
	
	sort_by = gtk_object_get_data(GTK_OBJECT(wi), "sort_by");
	
	id = &(window->id);
	
	if ( window->id == 0 ) {
		if ( form_record_save(window, NULL) ) {
			form_toolbar_last(toolbar, window);
			return;
		}
	} else {
		r = get_record_no(f->table, *id);
		if ( record_is_different(r, window) ) {
			form_record_save(window, r);
		}
	}
	
	r = table_last(f->table, *sort_by);
	window->id = ( r == NULL ) ? 0 : r->id;

	form_fill(window);
	update_bound_windows(window);
}

static void form_toolbar_next(GtkWidget *toolbar, gabywindow *window)
{
	GtkWidget *wi = window->widget;
	int *id;
	subtable *f = window->view->subtable;
	record *r;
	int *sort_by;
	
	sort_by = gtk_object_get_data(GTK_OBJECT(wi), "sort_by");
	debug_print("[form_toolbar_next] sort_by : %d\n", *sort_by);
	
	id = &(window->id);
	
	if ( window->id == 0 ) {
		debug_print("[form_toolbar_next] It is a new record");
		if ( record_is_not_empty(window) ) {
			debug_print(" and it's not empty, let's save it\n");
			if ( form_record_save(window, NULL) ) {
				form_toolbar_next(toolbar, window);
				return;
			}
		}
		debug_print("\n");
		r = table_last(f->table, *sort_by);
		if ( r != NULL ) window->id = r->id;
		form_fill(window);
		return;
	}

	r = get_record_no(f->table, window->id);
	
	if ( record_is_different(r, window) ) {
		debug_print("[form_toolbar_next] uh oh it looks changed\n");
		form_record_save(window, r);
	}
	
	r = table_next(f->table, r, *sort_by);
	window->id = ( r == NULL ) ? 0 : r->id;

	debug_print("[form_toolbar_next] I'll fill with record %d\n", *id);
	
	form_fill(window);

	update_bound_windows(window);
}

static void form_toolbar_prev(GtkWidget *toolbar, gabywindow *window)
{
	GtkWidget *wi = window->widget;
	int *id;
	subtable *f = window->view->subtable;
	record *r;
	int *sort_by;
	
	sort_by = gtk_object_get_data(GTK_OBJECT(wi), "sort_by");
	
	id = &(window->id);
	
	if ( window->id == 0 ) {

		if ( record_is_not_empty(window) ) {
			debug_print("[form:prev/1] form_record_save\n");
			if ( form_record_save(window, NULL) ) {
				form_toolbar_prev(toolbar, window);
				return;
			}
			/* note to self: the current way wipes what the user
			 * typed. It could perhaps stay on the record instead
			 * ... (to be investigated) */
		}
		r = table_last(f->table, *sort_by);
		if ( r != NULL ) window->id = r->id;
		form_fill(window);
		return;
	}
	
	debug_print("[form:prev] get_record_no with id:%d\n", window->id);
	r = get_record_no(f->table, window->id);
	debug_print("[form:prev] got id:%d\n", r->id);

	if ( record_is_different(r, window) ) {
#ifdef DEBUG_GABY
		debug_print("[form:prev/2] form_record_save (id:%d:%d)\n", window->id, r->id);
#endif
		form_record_save(window, r);
	}

	r = table_prev(f->table, r, *sort_by);
	window->id = ( r == NULL ) ? 0 : r->id;
	
	form_fill(window);
	update_bound_windows(window);
}

static void form_toolbar_new(GtkWidget *toolbar, gabywindow *window)
{
	subtable *f = window->view->subtable;
	int *id = &(window->id);

	if ( window->id == 0 ) {
#ifdef DEBUG_GABY
		debug_print("[form:new] form_record_save\n");
#endif
		if ( form_record_save(window, NULL) != NULL )
			form_toolbar_new(toolbar, window);
		return;
	}
	
	if ( record_is_different(get_record_no(f->table, *id), window) )
		form_record_save(window, get_record_no(f->table, *id));

	*id = 0;
	
	form_fill(window);
}

static void form_toolbar_sort(GtkWidget *toolbar, gabywindow *window)
{
	GtkWidget *wi = window->widget;
	GtkWidget *menu;

	menu = gtk_object_get_data(GTK_OBJECT(wi), "sort_menu");

	/*
	 * gtk tutorial doesn't explain how to popup a menu from
	 * a toolbar so I did it my way :)
	 */
	gtk_menu_popup (GTK_MENU(menu), NULL, NULL, NULL, NULL,
					0, 0);
	
}

static gchar* form_record_field_get_text( GtkWidget *wid )
{
	GtkWidget *combo = gtk_object_get_data(GTK_OBJECT(wid), "combo");
	gchar *s = gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(combo)->entry));
	return s;
}

static gchar* form_multimedia_field_get_text( GtkWidget *wid )
{
	GtkWidget *child = gtk_object_get_data(GTK_OBJECT(wid), "child");
	gchar *s = gtk_object_get_data(GTK_OBJECT(child), "value" );

	return s;
}

/*
 * form_get_record
 * @wi:
 * @r:
 *
 * Description:
 * This function creates a record filled with the current value of the view
 *
 * Return: @r or a newly allocated record* if @r was NULL
 **/
static record* form_get_record ( gabywindow *window, record *r )
{
	GtkWidget **w;
	subtable *f;
	int i;
	int *id;
	union data *dt;
	gchar *tmp;
	int type;
	GtkWidget *wi = window->widget;

	id = &(window->id);
	w = gtk_object_get_data(GTK_OBJECT(wi), "right");
	f = window->view->subtable;
	
	if ( r == NULL ) {
		r = g_new0(record, 1);
		r->id = 0;
		r->cont = g_new0(union data, f->table->nb_fields);
	} else {
		r = record_duplicate(window->view->subtable->table, r);
		r->id = 0;
	}
	
	for ( i=0; i<f->nb_fields; i++ ) {
		dt = &r->cont[f->fields[i].no];
		type = f->fields[i].type;
		if      ( type == T_STRINGS && ! text_for_strings  )
								type = T_STRING;
		else if ( type == T_DATE    && ! cal_for_date      )
								type = T_STRING;
		else if ( type == T_INTEGER && ! spin_for_numeric  )
								type = T_STRING;
		else if ( type == T_REAL    && ! spin_for_numeric  )
								type = T_STRING;
		else if ( type == T_BOOLEAN && ! check_for_boolean )
								type = T_STRING;
#ifndef USE_GNOME /* bad test, should be for Imlib and ESD */
		else if ( type == T_MULTIMEDIA )	type = T_STRING;
#endif
		else if ( type == T_FILE )		type = T_STRING;

		switch ( type ) {
			case T_STRING:
			{
				tmp = gtk_entry_get_text(GTK_ENTRY(w[i]));
				set_subtable_stringed_field(f, r, i, tmp);
			} break;
			case T_STRINGS:
			{
				tmp = gtk_editable_get_chars(
						GTK_EDITABLE(w[i]), 0, -1);
				set_subtable_stringed_field(f, r, i, tmp);
				g_free(tmp);
			} break;
			case T_DATE:
			{
				if ( dt->date == NULL ) {
					dt->date = g_date_new();
				}
				
				g_date_set_dmy(
					dt->date,\
					GTK_CALENDAR(w[i])->selected_day,
					GTK_CALENDAR(w[i])->month + 1,
					GTK_CALENDAR(w[i])->year );
			} break;
			case T_INTEGER:
			{
				dt->i = gtk_spin_button_get_value_as_int(
					GTK_SPIN_BUTTON(w[i]));
			} break;
			case T_REAL:
			{
				dt->d = gtk_spin_button_get_value_as_float(
					GTK_SPIN_BUTTON(w[i]));
			} break;
			case T_RECORD:
			{
				tmp = form_record_field_get_text(w[i]);
				set_subtable_stringed_field(f, r, i, tmp );
			} break;
			case T_MULTIMEDIA:
			{
				tmp = form_multimedia_field_get_text(w[i]);
				set_subtable_stringed_field(f, r, i, tmp );
			} break;
			case T_BOOLEAN:
			{
				dt->b = gtk_toggle_button_get_active(
						GTK_TOGGLE_BUTTON(w[i]));
			} break;
			case T_RECORDS:	{} break;
		}
	}
	
	return r;
}

static record* form_record_save(gabywindow *window, record *r)
{
	subtable *f;
	int *id;
	gboolean a_new_one = FALSE;

	if ( r == NULL ) a_new_one = TRUE;
	
	r = form_get_record(window, r);

	id = &(window->id);
	f = window->view->subtable;
	
	if ( a_new_one == TRUE ) {
		if ( ! record_is_not_empty(window) ) {
#ifdef DEBUG_GABY
			debug_print("[form:frs] record not saved because it was empty\n");
#endif
			record_free(f->table, r);
			return NULL;
		}
#ifdef DEBUG_GABY
		debug_print("[form:frs] record_add(...)\n");
#endif
		if ( record_add(f->table, r, TRUE, FALSE) == -1 ) {
			record_free(f->table, r);
			
			gaby_errno = SAVE_RECORD_ERROR; 
			gaby_perror_in_a_box();

			return NULL;
		}
		*id = r->id;
		debug_print("[form:fres] new id: %d\n", r->id);
	} else {
		r->id = *id;
		record_modify(f->table, r);
	}
	update_windows(window);

	return r;
}

mstatic void form_save ( gabywindow *window )
{
	GtkWidget *wi = window->widget;
	int *id;
	subtable *f;
	record *r = NULL;
	int *sort_by;
	
	f = window->view->subtable;
	sort_by = gtk_object_get_data(GTK_OBJECT(wi), "sort_by");
	
	id = &(window->id);
	r = get_record_no(f->table, *id);
	
	if ( *id == 0 ) {
		if ( record_is_not_empty(window) ) {
			r = form_record_save(window, NULL);
			*id = r->id;
		} else {
			return;
		}
	} else {
		if ( record_is_different(r, window) ) {
			form_record_save(window, r);
		}
		*id = r->id;
	}

	form_fill(window);
	update_bound_windows(window);

	return;
}

static gboolean record_is_different(record *r, gabywindow *window)
{
#if 0
	GtkWidget **w;
	GList *t;
	subtable *f;
	int i;
	int *id;
	GDate *date;
	gboolean tmpb;
	gchar *tmp;
	union data *field;

#ifdef DEBUG_GABY
	debug_print("[form:rid] begin\n");
#endif
	/*  this function should build a record* from the current window and
	 *  _then_ comparing this record to @r. The first part should be shared
	 *  with record_save while the second part could have its place in the
	 *  main gaby sources */

	w = gtk_object_get_data(GTK_OBJECT(wi), "right");
	t = gtk_object_get_data(GTK_OBJECT(wi), " tables ");
	f = ((view*)gtk_object_get_data(GTK_OBJECT(wi), " view "))->subtable;
	
	for ( i=0; i<f->nb_fields; i++ ) {
/*
		if ( r->cont[f->fields[i].no].anything == NULL )
			return TRUE;
*/
		field = &r->cont[f->fields[i].no];
		switch ( f->fields[i].type ) {
			case T_STRING:
			{
				tmp = gtk_entry_get_text(GTK_ENTRY(w[i]));
				if ( field->str == NULL ) {
					if ( strlen(tmp) != 0 )
						return TRUE;
				} else {
					if ( strcmp(tmp,field->str->str) != 0 )
						return TRUE;
				}
				/* 80 columns are not enough :( */
			} break;
			case T_STRINGS:
			{
				if ( text_for_strings ) {
					tmp = gtk_editable_get_chars(
						GTK_EDITABLE(w[i]), 0, -1);
				} else {
					tmp = gtk_entry_get_text(GTK_ENTRY(w[i]));
				}
				if ( field->str == NULL ) {
					if ( strlen(tmp) != 0 ) {
						if ( text_for_strings )
							g_free(tmp);
						return TRUE;
					}
				} else {
					if ( strcmp(tmp,field->str->str) != 0 ){
						if ( text_for_strings )
							g_free(tmp);
						return TRUE;
					}
				}
				if ( text_for_strings )
					g_free(tmp);
			} break;
			case T_DATE:
			{
				date = g_date_new();
				if ( cal_for_date ) {
					if ( GPOINTER_TO_INT(
						gtk_object_get_data(
							GTK_OBJECT(w[i]),
							"changed")) == FALSE){
						g_date_free(date);
						date = NULL;
					} else {
						g_date_set_dmy(date,
							GTK_CALENDAR(
							w[i])->selected_day,
							GTK_CALENDAR(
							w[i])->month+1,
							GTK_CALENDAR(
							w[i])->year );
					}
				} else {
					tmp = gtk_entry_get_text(
							GTK_ENTRY(w[i]));
					if ( !fill_date(date, tmp )) {
						if ( date != NULL )
							g_date_free(date);
						date = NULL;
					}
				}
				if ( r->cont[f->fields[i].no].date == NULL &&
						date != NULL ) {
					g_date_free(date);
					return TRUE;
				}
				if ( r->cont[f->fields[i].no].date != NULL ) {
					if ( g_date_compare(date,
							field->date ) != 0 ) {
						if ( date != NULL )
							g_date_free(date);
						return TRUE;
					}
				}
				if ( date != NULL )
					g_date_free(date);
			} break;
			case T_INTEGER:
			{
				tmp = gtk_entry_get_text(GTK_ENTRY(w[i]));
				if ( atoi(tmp) != r->cont[f->fields[i].no].i) {
					return TRUE;
				}
			} break;
			case T_REAL:
			{
				tmp = gtk_entry_get_text(GTK_ENTRY(w[i]));
				if ( atof(tmp) != r->cont[f->fields[i].no].d) {
					return TRUE;
				}
			} break;
			case T_RECORD:
			{
#ifdef DEBUG_GABY
				debug_print("[form:rid] orig : %d\n",
						r->cont[f->fields[i].no].i);
#endif
				tmp = form_record_field_get_text(w[i]);
				if ( id_record_field( tmp, f, i) !=
						r->cont[f->fields[i].no].i ) {
					return TRUE;
				}
			} break;
			case T_RECORDS:
			{
				/*
				 * records are not really part of the record
				 * therefore they should never return TRUE
				 * in this function
				 */
			} break;
			case T_MULTIMEDIA:
			{
				tmp = form_multimedia_field_get_text(w[i]);
				if ( strcmp( tmp,
					r->cont[f->fields[i].no].str->str)
						!= 0 )
				{
					return TRUE;
				}
			} break;
			case T_BOOLEAN:
			{
				if ( check_for_boolean ) {
					tmpb = gtk_toggle_button_get_active(\
						GTK_TOGGLE_BUTTON(w[i]));
				} else {
					tmp = gtk_entry_get_text(GTK_ENTRY(w[i]));
					tmpb = ( strcmp(tmp, _("yes")) == 0 );
				}
				if ( r->cont[f->fields[i].no].b != tmpb )
					return TRUE;
			} break;
			default:
			{
				return TRUE;
			} break;
		}
	}
#ifdef DEBUG_GABY
	debug_print("[form:rid] end\n");
#endif

	return FALSE;
#else /* ! 0 */
	record *ra;
	subtable *f;
	gboolean tmpb;
	
	f = window->view->subtable;
	ra = form_get_record(window, NULL);
	tmpb = records_are_different(f->table, r, ra);
	record_free(f->table, ra);

	return tmpb;
#endif /* ! 0 */

}

static gboolean record_is_not_empty ( gabywindow *window )
{
	GtkWidget *wi = window->widget;
	GtkWidget **w;
	GList *t;
	subtable *f;
	int *id;
	record *r;
	gboolean res;

	id = &(window->id);
	w = gtk_object_get_data(GTK_OBJECT(wi), "right");
	t = list_tables;
	f = window->view->subtable;

#if 0
	/*
	 *  check the record against record_default(), not against
	 *  empty strings !
	 *
	 *  this means this function will create r = record_defaults() then
	 *  call record_is_different() which should be changed to compare two
	 *  records instead of one record and the widgets.
	 */
	for ( i=0; i<f->nb_fields; i++ ) {
		switch ( f->table->fields[f->fields[i].no].type ) {
			case T_STRING:
			{
				tmp = gtk_entry_get_text(GTK_ENTRY(w[i]));
				if ( strlen(tmp) != 0 ) {
					return TRUE;
				}
				/* 80 columns are not enough :( */
			} break;
			case T_STRINGS:
			{
				tmp = gtk_editable_get_chars(
						GTK_EDITABLE(w[i]), 0, -1);
				if ( strlen(tmp) != 0 ) {
					g_free(tmp);
					return TRUE;
				}
				g_free(tmp);
			} break;
			default:
			{
				return TRUE;
			} break;
		}
	}
	return FALSE;
#else
	r = record_defaults(f->table);
	res = record_is_different(r, window);
	g_free(r->cont);
	g_free(r);
	
	return res;
#endif
	
}

#ifdef USE_GNOME


static GnomeUIInfo toolbar[] = {
/* 0 */	{ GNOME_APP_UI_ITEM,
		N_("New"),N_("Create a new record"),
		form_toolbar_new, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
		GNOME_STOCK_PIXMAP_NEW, 0, 0, NULL },
/* 1 */	{ GNOME_APP_UI_ITEM,
		N_("Remove"),N_("Remove this record"),
		form_toolbar_remove, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
		GNOME_STOCK_PIXMAP_STOP, 0, 0, NULL },
/* 2 */	{ GNOME_APP_UI_SEPARATOR },

/* 3 */	{ GNOME_APP_UI_ITEM,
		N_("First"), N_("Go to the first record"),
		form_toolbar_first, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
		GNOME_STOCK_PIXMAP_FIRST, 0, 0, NULL },
/* 4 */	{ GNOME_APP_UI_ITEM,
		N_("Previous"), N_("Go to the previous record"),
		form_toolbar_prev, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
		GNOME_STOCK_PIXMAP_BACK, 0, 0, NULL },
/* 5 */	{ GNOME_APP_UI_ITEM,
		N_("Next"), N_("Go to the next record"),
		form_toolbar_next, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
		GNOME_STOCK_PIXMAP_FORWARD, 0, 0, NULL },
/* 6 */	{ GNOME_APP_UI_ITEM,
		N_("Last"), N_("Go to the last record"),
		form_toolbar_last, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
		GNOME_STOCK_PIXMAP_LAST, 0, 0, NULL },
#define EXIT_BUTTON	10
/* 7 */	{ GNOME_APP_UI_SEPARATOR },
/* 8 */	{ GNOME_APP_UI_ITEM,
		N_("Sort"), N_("Sort the records"),
		form_toolbar_sort, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
		GNOME_STOCK_PIXMAP_FONT, 0, 0, NULL },
		/* WISHLIST: a GNOME_STOCK_PIXMAP_SORT would be welcome */
/* 9 */	{ GNOME_APP_UI_SEPARATOR },
/*10 */	{ GNOME_APP_UI_ITEM,
		N_("Exit"), N_("Exit Gaby"),
		gaby_quit_callback, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
		GNOME_STOCK_PIXMAP_EXIT, 0, 0, NULL },
/*11 */	{ GNOME_APP_UI_ENDOFINFO }
};

static void gnome_toolbar_create ( gabywindow *window, gboolean first )
{
	GtkWidget *parent = window->widget;
	GtkWidget *grandparent = window->parent;
	GList *tables = list_tables;
	GnomeUIInfo *tb = toolbar;
	GtkWidget *sort_menu;
	GnomeApp *ta;
	gboolean show_firstlast;
	
	show_firstlast = get_config_bool("view", "form", "show_firstlast", FALSE);
	sort_menu = create_sort_menu ( window );
	
	gtk_object_set_data(GTK_OBJECT(parent), "sort_menu", sort_menu);

	while ( tb->type != GNOME_APP_UI_ENDOFINFO ) {
		tb->user_data = window;
		tb++;
	}
	
	if ( first ) {
		ta = GNOME_APP(app);
		toolbar[EXIT_BUTTON].user_data = tables;
	} else {
		toolbar[EXIT_BUTTON-1].type = GNOME_APP_UI_ENDOFINFO;
		ta = GNOME_APP(grandparent);
	}
	
	if ( ta != NULL )
		gnome_app_create_toolbar(ta, toolbar);

	if ( ! show_firstlast ) {
		gtk_widget_hide( toolbar[3].widget );
		gtk_widget_hide( toolbar[6].widget );
	}
}

#else /* ! USE_GNOME */

static GtkWidget* form_toolbar_create( gabywindow *window, gboolean first )
{
	/*
	 * might the toolbar be a 'sub'-plugin ? this would give every win
	 * which looks like a form a tested toolbar (if they want).
	 * Anyway I could put the code in a different file, it's not (yet)
	 * important enough to be in a 3k plugin :) And it would definitely be
	 * easier to just have it statically. And it could even be used as a
	 * code base for particular needs.
	 */
	
	GtkWidget *pm;
	GtkWidget *tb;
	GtkWidget *sort_menu;
	GList *tables = list_tables;
	
	tb = gtk_toolbar_new(GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_BOTH );
	
	gtk_toolbar_set_button_relief(GTK_TOOLBAR(tb), GTK_RELIEF_HALF);
	
	gtk_widget_show(tb);
	
	/* this allow to create toolbar pixmaps without problems :) */
	pm = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_realize(pm);

	gtk_toolbar_append_item( GTK_TOOLBAR(tb), _("New"),
			_("Create a new record"), NULL,
			create_pixmap(pm, new_xpm),
			GTK_SIGNAL_FUNC(form_toolbar_new), window );
	
	gtk_toolbar_append_item( GTK_TOOLBAR(tb), _("Remove"),
			_("Remove this record"), NULL,
			create_pixmap(pm, remove_xpm),
			GTK_SIGNAL_FUNC(form_toolbar_remove), window );
	
	gtk_toolbar_append_space(GTK_TOOLBAR(tb));

	gtk_toolbar_append_item( GTK_TOOLBAR(tb), _("Previous"),
			_("Go to the previous record"), NULL,
			create_pixmap(pm, previous_xpm),
			GTK_SIGNAL_FUNC(form_toolbar_prev), window );
	
	gtk_toolbar_append_item( GTK_TOOLBAR(tb), _("Next"),
			_("Go to the next record"), NULL,
			create_pixmap(pm, next_xpm),
			GTK_SIGNAL_FUNC(form_toolbar_next), window );

	gtk_toolbar_append_space(GTK_TOOLBAR(tb));

	sort_menu = create_sort_menu(window);
	
	gtk_object_set_data(GTK_OBJECT(window->widget), "sort_menu", sort_menu);
	gtk_toolbar_append_item( GTK_TOOLBAR(tb), _("Sort"),
			_("Sort the records"), NULL,
			create_pixmap(pm, sort_xpm),
			GTK_SIGNAL_FUNC(form_toolbar_sort), window );
		
	if ( first == TRUE ) {
		gtk_toolbar_append_space(GTK_TOOLBAR(tb));
		gtk_toolbar_append_item( GTK_TOOLBAR(tb), _("Exit"),
			_("Exit Gaby"),
			NULL, create_pixmap(pm, exit_xpm),
			GTK_SIGNAL_FUNC(gaby_quit_callback), tables );
	}

	gtk_widget_destroy(pm);
		
	return tb;
}

/*
 * create_pixmap, convenience function to create a pixmap widget, from data
 * From geg, by David Bryant (d_bryant@lincoln.college.adelaide.edu.au)
 */
static GtkWidget * create_pixmap(GtkWidget *widget, gchar **data)
{
	GtkStyle *style;
	GdkBitmap *mask;
	GdkPixmap *gdk_pixmap;
	GtkWidget *gtk_pixmap;
	style = gtk_widget_get_style(widget);
	g_assert(style != NULL);
	gdk_pixmap = gdk_pixmap_create_from_xpm_d(widget->window, &mask,
			&style->bg[GTK_STATE_NORMAL], data);
	g_assert(gdk_pixmap != NULL);
	gtk_pixmap = gtk_pixmap_new(gdk_pixmap, mask);
	g_assert(gtk_pixmap != NULL);
	gtk_widget_show(gtk_pixmap);
	return(gtk_pixmap);
}

#endif /* ! USE_GNOME */

#if 0
static gboolean fill_date(GDate *date, gchar *s)
{

	if ( strlen(s) == 0 )
		return FALSE;

	g_date_set_parse(date, s);

	return TRUE;

}
#endif


/*
 * configure box stuff
 */


static GtkWidget* configure_widget = NULL;
#if 0
static gchar** configure_save();
#else
static void configure_save();
#endif
static void configure_apply();

static gboolean f_text_for_strings;
static gboolean f_cal_for_date;
static gboolean f_spin_for_numeric;
static gboolean f_check_for_boolean;

mstatic GtkWidget* form_configure (ViewPluginData *vpd)
{
	GtkWidget *vbox, *vbox_sw;
	GtkWidget *frame;
	GtkWidget *check_strings, *check_date, *check_numeric, *check_boolean;
	GtkWidget *check_status, *check_confirm, *check_firstlast;
	GtkWidget *label;
	
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox);

	frame = gtk_frame_new(_("Specialized widgets"));
	gtk_widget_show(frame);
	gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(frame), 5 );
	
	vbox_sw = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox_sw);
	gtk_container_add(GTK_CONTAINER(frame), vbox_sw);
	gtk_container_set_border_width(GTK_CONTAINER(vbox_sw), 5 );

	check_strings = gtk_check_button_new_with_label(
			_("Text widgets for 'strings' type"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_strings),
			f_text_for_strings );
	gtk_signal_connect_object(GTK_OBJECT(check_strings), "clicked",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(vbox) );
	gtk_widget_show(check_strings);
	gtk_object_set_data(GTK_OBJECT(vbox), "check_strings", check_strings);
	gtk_box_pack_start(GTK_BOX(vbox_sw), check_strings, FALSE, FALSE, 0 );
	
	check_date = gtk_check_button_new_with_label(
			_("Calendar widgets for 'date' type"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_date),
			f_cal_for_date );
	gtk_signal_connect_object(GTK_OBJECT(check_date), "clicked",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(vbox) );
	gtk_widget_show(check_date);
	gtk_object_set_data(GTK_OBJECT(vbox), "check_date", check_date);
	gtk_box_pack_start(GTK_BOX(vbox_sw), check_date, FALSE, FALSE, 0 );
	
	check_numeric = gtk_check_button_new_with_label(
			_("Spinbutton widgets for 'integer' and 'real' types"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_numeric),
			f_spin_for_numeric );
	gtk_signal_connect_object(GTK_OBJECT(check_numeric), "clicked",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(vbox) );
	gtk_widget_show(check_numeric);
	gtk_object_set_data(GTK_OBJECT(vbox), "check_numeric", check_numeric);
	gtk_box_pack_start(GTK_BOX(vbox_sw), check_numeric, FALSE, FALSE, 0 );

	check_boolean = gtk_check_button_new_with_label(
			_("Check button widgets for boolean type"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_boolean),
			f_check_for_boolean );
	gtk_signal_connect_object(GTK_OBJECT(check_boolean), "clicked",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(vbox) );
	gtk_widget_show(check_boolean);
	gtk_object_set_data(GTK_OBJECT(vbox), "check_boolean", check_boolean);
	gtk_box_pack_start(GTK_BOX(vbox_sw), check_boolean, FALSE, FALSE, 0 );
	
	label = gtk_label_new(_("Those changes will only be applied the next time you launch gaby."));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox_sw), label, FALSE, FALSE, 0 );
	
	frame = gtk_frame_new(_("Miscellaneous"));
	gtk_widget_show(frame);
	gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(frame), 5 );
	
	vbox_sw = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox_sw);
	gtk_container_add(GTK_CONTAINER(frame), vbox_sw);
	gtk_container_set_border_width(GTK_CONTAINER(vbox_sw), 5 );

	check_status = gtk_check_button_new_with_label(
			_("Use statusbar to show field description"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_status),
			get_config_bool("view", "form", "use_statusbar", TRUE));
	gtk_signal_connect_object(GTK_OBJECT(check_status), "clicked",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(vbox) );
	gtk_widget_show(check_status);
	gtk_object_set_data(GTK_OBJECT(vbox), "check_status", check_status);
	gtk_box_pack_start(GTK_BOX(vbox_sw), check_status, FALSE, FALSE, 0 );
	
	check_confirm = gtk_check_button_new_with_label(
			_("Confirm record deletion"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_confirm),
		get_config_bool("view", "form", "confirm_deletion", TRUE));
	gtk_signal_connect_object(GTK_OBJECT(check_confirm), "clicked",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(vbox) );
	gtk_widget_show(check_confirm);
	gtk_object_set_data(GTK_OBJECT(vbox), "check_confirm", check_confirm);
	gtk_box_pack_start(GTK_BOX(vbox_sw), check_confirm, FALSE, FALSE, 0 );
	
	check_firstlast = gtk_check_button_new_with_label(
				_("Show first/last record buttons"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check_firstlast),
		get_config_bool("view", "form", "show_firstlast", FALSE));
	gtk_signal_connect_object(GTK_OBJECT(check_firstlast), "clicked",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(vbox) );
	gtk_widget_show(check_firstlast);
	gtk_object_set_data(GTK_OBJECT(vbox),"check_firstlast",check_firstlast);
	gtk_box_pack_start(GTK_BOX(vbox_sw), check_firstlast, FALSE, FALSE, 0 );
	
	gtk_object_set_data(GTK_OBJECT(vbox), "name", vpd->i18n_name);
	gtk_object_set_data(GTK_OBJECT(vbox), "cfg_save", configure_save);
	gtk_object_set_data(GTK_OBJECT(vbox), "cfg_apply", configure_apply);

	configure_widget = vbox;
	return vbox;
}

static void configure_apply()
{
	/* Sorry but I can't change widgets on the fly */
}

#if 0
static gchar** configure_save()
{
	GtkWidget *check_strings = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "check_strings");
	GtkWidget *check_date = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "check_date");
	GtkWidget *check_numeric = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "check_numeric");
	gchar **cfg;
	cfg = g_new0(gchar*, 4);

	cfg[0] = g_new0(gchar, 30);
	cfg[1] = g_new0(gchar, 30);
	cfg[2] = g_new0(gchar, 30);

	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_strings)))
		strcpy(cfg[0], "use_textbox_widgets     TRUE");
	else	strcpy(cfg[0], "use_textbox_widgets     FALSE");
	
	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_date)))
		strcpy(cfg[1], "use_calendar_widgets    TRUE");
	else	strcpy(cfg[1], "use_calendar_widgets    FALSE");
	
	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_numeric)))
		strcpy(cfg[2], "use_spinbutton_widgets  TRUE");
	else	strcpy(cfg[2], "use_spinbutton_widgets  FALSE");

	return cfg;
}
#else /* ! 0 */

static void configure_save()
{
	GtkWidget *check_strings = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "check_strings");
	GtkWidget *check_date = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "check_date");
	GtkWidget *check_numeric = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "check_numeric");
	GtkWidget *check_boolean = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "check_boolean");
	GtkWidget *check_status = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "check_status");
	GtkWidget *check_confirm = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "check_confirm");
	GtkWidget *check_firstlast = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "check_firstlast");

	write_config_bool("view", "form", "use_textbox_widgets",
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_strings)));
	
	write_config_bool("view", "form", "use_calendar_widgets",
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_date)));
	
	write_config_bool("view", "form", "use_spinbutton_widgets",
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_numeric)));
	
	write_config_bool("view", "form", "use_check_widgets",
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_boolean)));
	
	write_config_bool("view", "form", "use_statusbar",
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_status)));
	
	write_config_bool("view", "form", "confirm_deletion",
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_confirm)));
	
	write_config_bool("view", "form", "show_firstlast",
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check_firstlast)));
}

#endif /* ! 0 */

mstatic void form_get_config()
{
	/* default is TRUE because it was like that in 1.0 and I like it that
	 * way; don't you ? :) */
	text_for_strings = get_config_bool("view", "form",
					"use_textbox_widgets", TRUE );
	
	/* default is FALSE because I find easier to enter dates in entry
	 * widgets but this doesn't look nice like the calendar does 
	 * -> do we want to impress at first look ? */
	cal_for_date = get_config_bool("view", "form",
					"use_calendar_widgets", FALSE );

	/* default is FALSE because there is not yet a good way to define the
	 * range of values */
	spin_for_numeric = get_config_bool("view", "form",
					"use_spinbutton_widgets", FALSE );
	
	/* default is FALSE because it is untested */
	check_for_boolean = get_config_bool("view", "form",
					"use_check_widgets", FALSE );
	
	f_text_for_strings = text_for_strings;
	f_cal_for_date = cal_for_date;
	f_spin_for_numeric = spin_for_numeric;
	f_check_for_boolean = check_for_boolean;
}

#if 0
static void parse_config(char **lines)
{
	int i;
	char *config_strings[] = {
		"use_textbox_widgets",
		"use_calendar_widgets",
		"use_spinbutton_widgets"
	};
	char **s = config_strings;

	/* defaults */
	text_for_strings = TRUE;
	cal_for_date = TRUE;
	spin_for_numeric = FALSE; /* default is false 'cause not well tested */
	
	i = 0;
	while ( lines[i] != NULL ) {
		if ( strncmp(lines[i], s[0], strlen(s[0])) == 0 ) {
			if ( strstr(lines[i], "FALSE") != NULL )
				text_for_strings = FALSE;
		}
		if ( strncmp(lines[i], s[1], strlen(s[1])) == 0 ) {
			if ( strstr(lines[i], "FALSE") != NULL )
				cal_for_date = FALSE;
		}
		if ( strncmp(lines[i], s[2], strlen(s[2])) == 0 ) {
			if ( strstr(lines[i], "TRUE") != NULL )
				spin_for_numeric = TRUE;
		}
		
		g_free(lines[i++]);
	}
	g_free(lines);

	f_text_for_strings = text_for_strings;
	f_cal_for_date = cal_for_date;
	f_spin_for_numeric = spin_for_numeric;
	
}
#endif /* 0 */

