/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <tables.h>
#include <records.h>
#include <f_config.h>
#include <windows.h>
#include <f_desc.h>
#include <gtk_main.h>
#include <gtk_config_dlg.h>

mstatic void canvas_create ( gabywindow *window, gboolean first );
mstatic void canvas_fill ( gabywindow *window );
mstatic GtkWidget* canvas_configure (ViewPluginData *vpd);
mstatic void canvas_get_config();

static void update_configure_box();

#ifndef FOLLOW_MIGUEL
int init_view_plugin (ViewPluginData *vpd)
{
	vpd->view_create = canvas_create;
	vpd->view_fill = canvas_fill;
	vpd->view_records = NULL;
	vpd->configure = canvas_configure;

	vpd->name = "canvas";
	vpd->i18n_name = _("Canvas");
	vpd->type = ONE_RECORD;
	vpd->capabilities = NONE;

	canvas_get_config();

	return 0;
}
#endif

/* those are the fonts defined in gnomecard, we could perhaps use others */
#define DEFAULT_H1_FONTSET "-adobe-helvetica-medium-r-normal-*-18-*-*-*-p-*-*-*,-cronyx-helvetica-medium-r-normal-*-20-*-*-*-p-*-koi8-r,-*-*-medium-r-normal-*-18-*-*-*-*-*-ksc5601.1987-0,*"
#define DEFAULT_H2_FONTSET "-adobe-helvetica-medium-r-*-*-14-*-*-*-p-*-*-*,-cronyx-helvetica-medium-r-normal-*-17-*-*-*-p-*-koi8-r,-*-*-medium-r-no-*-*-14-*-*-*-*-*-ksc5601.1987-0,*"
#define DEFAULT_NORMAL_FONTSET "-adobe-helvetica-medium-r-*-*-12-*-*-*-p-*-*-*,-cronyx-helvetica-medium-r-normal-*-14-*-*-*-p-*-koi8-r,-*-*-medium-r-*-*-12-*-*-*-*-*-ksc5601.1987-0,*"
#define DEFAULT_FTEXT_FONTSET "-adobe-helvetica-medium-r-*-*-10-*-*-*-p-*-*-*,-cronyx-helvetica-medium-r-normal-*-11-*-*-*-p-*-koi8-r,-*-*-medium-r-*-*-10-*-*-*-*-*-ksc5601.1987-0,*"

static char* h1_fontset;
static char* h2_fontset;
static char* normal_fontset;
static char* ftext_fontset;

#define DEFAULT_HEADER_BOX_COLOR  "#f0e68c"	/* khaki */
#define DEFAULT_LABEL_BOX_COLOR   "#bdb76b"	/* dark khaki */

#define DEFAULT_HEADER_TEXT_COLOR "#000000"	/* black */
#define DEFAULT_LABEL_TEXT_COLOR  "#000000"	/* black */
#define DEFAULT_BODY_TEXT_COLOR   "#000000"	/* black */

#define DEFAULT_BACKGROUND_COLOR  "#ffffff"	/* white */

static char* header_box_color;
static char* label_box_color;
static char* header_text_color;
static char* label_text_color;
static char* body_text_color;
static char* background_color;

#define BORDER_SIZE	2;			/* Pixels font and box */
#define BORDER_SIZE_2	4;			/* Twice that */
#define GAP_SIZE	4;			/* Gap between two boxes */

static GnomeUIInfo go_menu[];

static GnomeCanvasGroup* canvas_create_real(GtkWidget *canvas, GList *format,
					    subtable *st, int *height);
static void drag_data_received(GtkWidget *widget, GdkDragContext *context,
			       gint x, gint y, GtkSelectionData *selection_data,
			       guint info, guint time);
static void canvas_clicked(GtkWidget *canvas, GdkEventButton *event);

mstatic void canvas_create ( gabywindow *window, gboolean first )
{
	GtkWidget *canvas;
	GnomeCanvasGroup *root;
	GList *format;
	gdouble x1, x2, y1, y2;
	int *id;
	record *r;
	char sectname[100];
	static GtkTargetEntry target_table[] = {
		{ "application/x-color", 0, 1 }
	};
	int height;
	
	sprintf(sectname, "Canvas:%s", window->view->subtable->name);
	format = get_plugin_options(sectname);
	if ( format == NULL ) {
		gaby_errno = CUSTOM_ERROR;
		gaby_message = g_strdup(
			_("No canvas format were defined for this subtable.\n"\
			  "Ask me (fpeters@theridion.com) how to write them"));
		gaby_perror_in_a_box();
		return;
	}
	
	canvas = gnome_canvas_new();
	window->widget = canvas;
	
	go_menu[0].user_data = window;
	go_menu[1].user_data = window;
	go_menu[2].user_data = window;
	go_menu[3].user_data = window;
	
	id = &(window->id);
	
	r = table_first(window->view->subtable->table, -1);
	*id = ( r == NULL ? 0 : r->id );
	
	root = canvas_create_real(canvas, format,
					window->view->subtable, &height);
	g_list_free(format);
	
	gtk_widget_set_usize(canvas, -1, height);

	gtk_signal_connect (GTK_OBJECT(canvas), "drag_data_received",
			GTK_SIGNAL_FUNC(drag_data_received), NULL );
	gtk_drag_dest_set (GTK_WIDGET(canvas),
			GTK_DEST_DEFAULT_MOTION |
			GTK_DEST_DEFAULT_HIGHLIGHT |
			GTK_DEST_DEFAULT_DROP,
			target_table, 1, GDK_ACTION_COPY );
	
	gtk_signal_connect (GTK_OBJECT(canvas), "button_release_event",
			GTK_SIGNAL_FUNC(canvas_clicked), NULL );

	/* all done */

	gnome_canvas_item_get_bounds(GNOME_CANVAS_ITEM(root), &x1,&y1,&x2,&y2);
	gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas), x1, y1, x2, y2);
	
	gtk_widget_show(canvas);

	return;
}

static void update_box_color_bg(GList *items, gchar *color)
{
	GnomeCanvasItem *item;
	GList *tmp = ( items == NULL ) ? NULL : g_list_first(items);
	
	while ( tmp != NULL ) {
		item = tmp->data;
		gnome_canvas_item_set( item,
				"fill_color", color,
				"outline_color", color,
				NULL);
		tmp = g_list_next(tmp);
	}
}

static void update_text_color(GList *items, gchar *color, gboolean it)
{
	GnomeCanvasItem *item;
	GList *tmp = ( items == NULL ) ? NULL : g_list_first(items);

	while ( tmp != NULL ) {
		item = tmp->data;
		if ( it == FALSE || GPOINTER_TO_INT(gtk_object_get_data(\
					GTK_OBJECT(item), "type")) < 2) {
			gnome_canvas_item_set( item, "fill_color", color, NULL);
		}
		tmp = g_list_next(tmp);
	}
}

static void drag_data_received(GtkWidget *widget, GdkDragContext *context,
			       gint x, gint y, GtkSelectionData *selection_data,
			       guint info, guint time)
{
	GnomeCanvasItem *item;
	GtkStyle *style;
	guint16 *data = (guint16 *)selection_data->data;
	char color[20];
	int i;
	GList *h1h2_boxes, *ftext_boxes;
	GList *h1h2_texts, *ftext_texts;
	GList *items;
	
	/* TODO (post-2.0): reactivate color drag and drop
	 * (disabled since it doesn't work when upper left corner is not (0,0)
	 * (delayed since it is not useful)
	 */
	return;

#ifdef DEBUG_GABY
	debug_print("[drag_data_received] begin\n");
#endif
	h1h2_boxes = gtk_object_get_data (GTK_OBJECT(widget),"h1h2_boxes");
	ftext_boxes = gtk_object_get_data(GTK_OBJECT(widget),"ftext_boxes");
	h1h2_texts = gtk_object_get_data (GTK_OBJECT(widget),"h1h2_texts");
	ftext_texts = gtk_object_get_data(GTK_OBJECT(widget),"ftext_texts");
	items = gtk_object_get_data(GTK_OBJECT(widget), "items");
	
	if ( info == 1 ) {
		
		if ( selection_data->length != 8 )
			return;

#ifdef DEBUG_GABY
		debug_print("[drag_data_received] got a color at (%d,%d)\n", x, y);
#endif

		item = gnome_canvas_get_item_at(GNOME_CANVAS(widget),
						(double)x, (double)y );

		sprintf(color, "#%02x%02x%02x",data[0]/256,data[1]/256,data[2]/256 ); 

		if ( item == NULL ) { /* background was hit */
#ifdef DEBUG_GABY
			debug_print("[drag_data_received] background !\n");
#endif
			style = gtk_style_copy(gtk_widget_get_style(widget));
			for ( i=0; i<5; i++ ) {
				style->bg[i].red = data[0];
				style->bg[i].green = data[1];
				style->bg[i].blue = data[2];
			}
			gtk_widget_set_style(widget, style);
			strcpy(background_color, color);
			update_configure_box();
			return;
		}
		
		if ( GNOME_IS_CANVAS_RE(item) ) { /* a rectangle (or ellipse)*/
			if ( g_list_find(h1h2_boxes, item) ) {
				update_box_color_bg(h1h2_boxes, color);
				strcpy(header_box_color, color);
				update_configure_box();
				return;
			}
			if ( g_list_find(ftext_boxes, item) ) {
				update_box_color_bg(ftext_boxes, color);
				strcpy(label_box_color, color);
				update_configure_box();
				return;
			}
			return;
		}
		
		if ( GNOME_IS_CANVAS_TEXT(item) ) { /* a text */
			if ( g_list_find(h1h2_texts, item) ) {
				update_text_color(h1h2_texts, color, FALSE);
				strcpy(header_text_color, color);
				update_configure_box();
				return;
			}
			if ( g_list_find(ftext_texts, item) ) {
				update_text_color(ftext_texts, color, FALSE);
				strcpy(label_text_color, color);
				update_configure_box();
				return;
			}
			update_text_color(items, color, TRUE);
			strcpy(body_text_color, color);
			update_configure_box();
			return;
		}

	}
}

static GnomeCanvasGroup* canvas_create_real(GtkWidget *canvas, GList *format,
					    subtable *st, int *height)
{
	GtkStyle *style;
	GnomeCanvasGroup *root;
	GdkFont *h1_font, *h2_font, *ftext_font, *normal_font;
	GnomeCanvasItem *item;
	GList *h1h2_boxes = NULL, *ftext_boxes = NULL;
	GList *h1h2_texts = NULL, *ftext_texts = NULL;
	GList *items = NULL;
	char str[3];
	gushort red, green, blue;
	int i;
	double y;
	gchar *sp, *stmp;
	int type;
	int size = 0;
	int warning = 0;
	static char *no_font = "No font";
	
	gtk_widget_pop_visual();
	gtk_widget_pop_colormap();

	style = gtk_style_copy(gtk_widget_get_style(canvas));
	
	/* this is _ugly_ (tm) but I need X11 color strings for canvasitem and
	 * it wouldn't be logic to use rgb for the background and I don't know
	 * about X11 programming. (I believe I have to use XParseColor to get
	 * the rgb color out of a string but nothing more ...)
	 */
	
	str[2] = 0;

	str[0] = background_color[1];
	str[1] = background_color[2];
	sscanf(str, "%hx", &red);
	red *= 256;

	str[0] = background_color[3];
	str[1] = background_color[4];
	sscanf(str, "%hx", &green);
	green *= 256;

	str[0] = background_color[5];
	str[1] = background_color[6];
	sscanf(str, "%hx", &blue);
	blue *= 256;

#ifdef DEBUG_GABY
	debug_print("[canvas_create_real] red : %hd, green : %hd, blue : %hd\n",
			red, green, blue );
#endif

	for ( i=0; i<5; i++ ) {
		style->bg[i].red = red;
		style->bg[i].green = green;
		style->bg[i].blue = blue;
	}
			
	gtk_widget_set_style(GTK_WIDGET(canvas), style);

	root = GNOME_CANVAS_GROUP(gnome_canvas_root(GNOME_CANVAS(canvas)));

	if ( (h1_font = gdk_fontset_load(h1_fontset)) == NULL ) {
		h1_font = gdk_fontset_load(DEFAULT_H1_FONTSET);
		warning = 1;
	}
	if ( (h2_font = gdk_fontset_load(h2_fontset)) == NULL ) {
		h2_font = gdk_fontset_load(DEFAULT_H2_FONTSET);
		warning = 1;
	}
	if ( (ftext_font = gdk_fontset_load(ftext_fontset)) == NULL ) {
		ftext_font = gdk_fontset_load(DEFAULT_FTEXT_FONTSET);
		warning = 1;
	}
	if ( (normal_font = gdk_fontset_load(normal_fontset)) == NULL ) {
		normal_font = gdk_fontset_load(DEFAULT_NORMAL_FONTSET);
		warning = 1;
	}
	if (warning == 1) {
#if 1
		/* We can't do this because App (gnomeapp) exists but the main
		 * window hasn't been realized yet. gnome_app_warning segfaults.
		 *
		 * Update: Fixed 1999-10-21 in gtk_error_dlg.c
		 */
		gaby_message = no_font;
		gaby_errno = CUSTOM_WARNING;
		gaby_perror_in_a_box();
#else
		g_print("WARNING: %s\n", no_font);
#endif
	}

#ifdef DEBUG_GABY
	debug_print("[canvas_create_real] ascent %d, descent: %d\n",
	      h1_font->ascent, h1_font->descent);
#endif

	/* items spec : */

	/* h1 box :
	 *    - x1:5, x2:200, y2:y1+30
	 *    - fill_color, outline_color : header_box_color
	 *    - width_pixels : 1
	 * h1 text:
	 *    - text : "..."
	 *    - x:10, y:y1+15
	 *    - fontset : NAME_FONTSET (to replace by H1_FONTSET)
	 *    - anchor : GTK_ANCHOR_NORTH_WEST
	 *    - fill_color : header_text_color
	 */
	
	/* normal text :
	 *    - text : "..."
	 *    - x:10, y:'prev'+15 (+25 (relative to h1_text) if prev was h1)
	 *    - fontset : CANVAS_FONTSET
	 *    - anchor : GTK_ANCHOR_NORTH_WEST
	 *    - fill_color : body_text_color
	 */
	
	/*
	 * h2 box :
	 *    - x1:5, x2:200, y1:'prev'+10, y2:y1+20
	 *    - fill_color, outline_color : header_box_color
	 *    - width_pixels : 1
	 * h2 text :
	 *    - text : '...'
	 *    - x:10, y:y1+10
	 *    - fontset : TITLE_FONTSET (to replace by H2_FONTSET)
	 *    - anchor : GTK_ANCHOR_NORTH_WEST
	 *    - fill_color : header_text_color
	 */
	
	/* f box :
	 *    - x1:10, x2:62, y1:'cur'-6, y2:'cur'+6
	 *    - fill_color, outline_color : label_box_color
	 *    - width_pixels : 0
	 * f text :
	 *    - x:60, y:'prev'+15 (+20 (relative to h2_text) if prev was h2)
	 *    - fontset : COMMENT_FONTSET
	 *    - anchor : GTK_ANCHOR_EAST
	 *    - fill_color : label_text_color
	 * next to f text:
	 *    - x:70, y:'cur'
	 *    - fontset : COMMENT_FONTSET
	 *    - anchor : GTK_ANCHOR_NORTH_WEST
	 *    - fill_color : body_text_color
	 */

	/* Update:
	 * x/y co-ordinates and box sizes are now calculated according
	 * to the font_sets.
	 */
	
	y = 5.0;
	while ( format != NULL ) {
		sp = format->data;
		stmp = sp;
		
		type = 0;
		if ( strncmp(stmp, "<h1>", 4) == 0 ) {
			type = 2;
			size = h1_font->ascent + h1_font->descent +
				BORDER_SIZE_2;
			h1h2_boxes = g_list_append(h1h2_boxes,
				gnome_canvas_item_new(root,
					gnome_canvas_rect_get_type(),
					"x1", 5.0,
					"y1", y,
					"x2", 200.0,
					"y2", y + size,
					"fill_color", header_box_color,
					"outline_color", header_box_color,
					"width_pixels", 1,
					NULL ));
			y += BORDER_SIZE;
			stmp += 4;
		}
			
		if ( strncmp(stmp, "<h2>", 4) == 0 ) {
			type = 3;
			size = h2_font->ascent + h2_font->descent +
				BORDER_SIZE_2;
			h1h2_boxes = g_list_append(h1h2_boxes,
				gnome_canvas_item_new(root,
					gnome_canvas_rect_get_type(),
					"x1", 5.0,
					"y1", y,
					"x2", 200.0,
					"y2", y + size,
					"fill_color", header_box_color,
					"outline_color", header_box_color,
					"width_pixels", 1,
					NULL ));
			y += BORDER_SIZE;
			stmp += 4;
		}

		if ( strncmp(stmp, "<f ", 3) == 0 ) {
			type = 1;
			size = ftext_font->ascent + ftext_font->descent +
				BORDER_SIZE_2;
			ftext_boxes = g_list_append(ftext_boxes,
				gnome_canvas_item_new(root,
					gnome_canvas_rect_get_type(),
					"x1", 10.0,
					"y1", y,
					"x2", 62.0,
					"y2", y + size,
					"fill_color", label_box_color,
					"outline_color", label_box_color,
					"width_pixels", 0,
					NULL ));
			stmp += 3;
			y += BORDER_SIZE;
			strchr(stmp, '>')[0] = 0;
			ftext_texts = g_list_append(ftext_texts,
					gnome_canvas_item_new(root,
					gnome_canvas_text_get_type(),
					"text", stmp,
					"x", 60.0,
					"y", y,
					"fontset", ftext_fontset,
					"anchor", GTK_ANCHOR_NORTH_EAST,
					"fill_color", label_text_color,
					NULL ));
			stmp += strlen(stmp)+1;
		}
				
		switch ( type ) {
			case 1: /* 'f'-text */
			{
				item = gnome_canvas_item_new(root,
					gnome_canvas_text_get_type(),
					"text", "",
					"x", 70.0,
					"y", y,
					"fontset", ftext_fontset,
					"anchor", GTK_ANCHOR_NORTH_WEST,
					"fill_color", body_text_color,
					NULL);
				y += size + GAP_SIZE;
			} break;
			case 2: /* h1 text */
			{
				item = gnome_canvas_item_new(root,
					gnome_canvas_text_get_type(),
					"text", "",
					"x", 10.0,
					"y", y,
					"fontset", h1_fontset,
					"anchor", GTK_ANCHOR_NORTH_WEST,
					"fill_color", header_text_color,
					NULL);
				h1h2_texts = g_list_append(h1h2_texts, item);
				y += size + GAP_SIZE;
			} break;
			case 3: /* h2 text */
			{
				item = gnome_canvas_item_new(root,
					gnome_canvas_text_get_type(),
					"text", "",
					"x", 10.0,
					"y", y,
					"fontset", h2_fontset,
					"anchor", GTK_ANCHOR_NORTH_WEST,
					"fill_color", header_text_color,
					NULL );
				h1h2_texts = g_list_append(h1h2_texts, item);
				y += size + GAP_SIZE;
			} break;
			default:
			case 0: /* normal text */
			{
				size = normal_font->ascent +
					normal_font->descent +
					BORDER_SIZE_2;
				item = gnome_canvas_item_new(root,
					gnome_canvas_text_get_type(),
					"text", "",
					"x", 10.0,
					"y", y,
					"fontset", normal_fontset,
					"anchor", GTK_ANCHOR_NORTH_WEST,
					"fill_color", body_text_color,
					NULL);
				y += size + GAP_SIZE;
			} break;
		}
		
		gtk_object_set_data(GTK_OBJECT(item), "type",
						GINT_TO_POINTER(type) );
		
		while ( *stmp == ' ') stmp++;
		if ( strchr(stmp, '\n') ) strchr(stmp, '\n')[0] = 0;
		if ( strchr(stmp, ',') != NULL ||
				subtable_get_field_no(st, stmp) != -2) {
			
			i=0;
			while ( strchr(stmp, ',') != NULL ) {
				strchr(stmp, ',')[0] = 0;
				while ( stmp[strlen(stmp)-1] == ' ')
					stmp[strlen(stmp)-1] = 0;
#ifdef DEBUG_GABY
				debug_print("[canvas_create_real] field : %s\n",
						stmp);
#endif
				sprintf(str, "%d", i);
				/* the ultimate coding style
				 * 
				 * enjoy */
				gtk_object_set_data(GTK_OBJECT(item), str,
					GINT_TO_POINTER(
						subtable_get_field_no(st, stmp)
					));
			
				i++;
				stmp += strlen(stmp);
				while ( *stmp == 0 || *stmp == ' ') stmp++;
			}
			sprintf(str, "%d", i);
			i++;
			gtk_object_set_data(GTK_OBJECT(item), str,
				GINT_TO_POINTER(
					subtable_get_field_no(st, stmp)));
			
			sprintf(str, "%d", i);
			gtk_object_set_data(GTK_OBJECT(item), str,
						GINT_TO_POINTER(-2));
			
#ifdef DEBUG_GABY
			debug_print("[canvas_create_real] field : %s\n", stmp);
#endif
			items = g_list_append(items, item);
		} else {
			gnome_canvas_item_set(item, "text", stmp, NULL);
		}
		
		g_free(format->data);
		format = g_list_next(format);
	}

	gtk_object_set_data(GTK_OBJECT(canvas), "h1h2_boxes", h1h2_boxes);
	gtk_object_set_data(GTK_OBJECT(canvas), "ftext_boxes", ftext_boxes);
	gtk_object_set_data(GTK_OBJECT(canvas), "h1h2_texts", h1h2_texts);
	gtk_object_set_data(GTK_OBJECT(canvas), "ftext_texts", ftext_texts);
	gtk_object_set_data(GTK_OBJECT(canvas), "items", items);
	
	*height = (int)y;

#ifdef DEBUG_GABY
	debug_print("[canvas_create_real] the end\n");
#endif
	return root;
}

mstatic void canvas_fill( gabywindow *window )
{
	GtkWidget *wid = window->widget;
	subtable *f;
	int *id;
	GList *items;
	GnomeCanvasItem *item;
	GString *str, *tmp_str;
	record *r;
	gchar dstr[3];
	int i, field_no;
	GnomeCanvasGroup *root;
	gdouble x1, x2, y1, y2;
	
#ifdef DEBUG_GABY
	debug_print("[canvas_fill] filling canvas");
#endif
	f = window->view->subtable;
	id = &(window->id);
	
	r = get_record_no(f->table, *id);
	
	str = g_string_new("");
	items = gtk_object_get_data(GTK_OBJECT(wid), "items");
	items = g_list_first(items);
	while ( items != NULL ) {
		item = items->data;
		i=0;
		str = g_string_assign(str, "");
		do {
			sprintf(dstr, "%d", i);
			field_no = GPOINTER_TO_INT(
				gtk_object_get_data(GTK_OBJECT(item), dstr ));
#ifdef DEBUG_GABY
			debug_print("[canvas_fill] field number : %d %s\n",
					field_no, dstr );
#endif
			if ( field_no == -2 ) break;
			
			tmp_str = get_subtable_stringed_field(f, r, field_no);
			if ( str->len != 0 ) {
				str = g_string_append(str, " ");
			}
			str = g_string_append(str, tmp_str->str);
			g_string_free(tmp_str, 1);
			i++;
		} while ( 1 );
		
#ifdef DEBUG_GABY
		debug_print("[canvas_fill] this line : %s\n", str->str);
#endif
		gnome_canvas_item_set(item, "text", str->str, NULL);
		
		items = g_list_next(items);
	}

#ifdef DEBUG_GABY
	debug_print("[canvas_fill] getting root : %s\n", str->str);
#endif
	root = GNOME_CANVAS_GROUP(gnome_canvas_root(GNOME_CANVAS(wid)));
#ifdef DEBUG_GABY
	debug_print("[canvas_fill] setting scrollbars : %s\n", str->str);
#endif
	gnome_canvas_item_get_bounds(GNOME_CANVAS_ITEM(root), &x1,&y1,&x2,&y2);
	gnome_canvas_set_scroll_region(GNOME_CANVAS(wid), x1, y1, x2, y2);
	
	g_string_free(str, 1);
}

static void first_record(GtkWidget *w, gabywindow *window)
{
	record *r;
	view *v;
	int *id;

	if ( window == NULL ) {
#ifdef DEBUG_GABY
		debug_print("[canvas:first_record] gtk didn't give window\n");
#endif
		return;
	}
	v = window->view;
	id = &(window->id);
	
	r = table_first(v->subtable->table, -1);
	*id = ( r == NULL ) ? 0 : r->id;
	
	canvas_fill(window);
	update_bound_windows(window);
}

static void previous_record(GtkWidget *w, gabywindow *window)
{
	record *r;
	view *v;
	int *id;

	if ( window == NULL ) {
#ifdef DEBUG_GABY
		debug_print("[canvas:first_record] gtk didn't give window\n");
#endif
		return;
	}
	v = window->view;
	id = &(window->id);

	r = get_record_no(v->subtable->table, *id);
	r = table_prev(v->subtable->table, r, -1);
	*id = ( r == NULL ) ? 0 : r->id;
	
	canvas_fill(window);
	update_bound_windows(window);
}

static void next_record(GtkWidget *w, gabywindow *window)
{
	record *r;
	view *v;
	int *id;

	if ( window == NULL ) {
#ifdef DEBUG_GABY
		debug_print("[canvas:first_record] gtk didn't give window\n");
#endif
		return;
	}
	v = window->view;
	id = &(window->id);

	r = get_record_no(v->subtable->table, *id);
	r = table_next(v->subtable->table, r, -1);
	*id = ( r == NULL ) ? 0 : r->id;
	
	canvas_fill(window);
	update_bound_windows(window);
}

static void last_record(GtkWidget *w, gabywindow *window)
{
	record *r;
	view *v;
	int *id;

	if ( window == NULL ) {
#ifdef DEBUG_GABY
		debug_print("[canvas:first_record] gtk didn't give window\n");
#endif
		return;
	}
	v = window->view;
	id = &(window->id);

	r = table_last(v->subtable->table, -1);
	*id = ( r == NULL ) ? 0 : r->id;
	
	canvas_fill(window);
	update_bound_windows(window);
}

static GnomeUIInfo go_menu[] = {
	{GNOME_APP_UI_ITEM, N_("First"), N_("First record"),
		first_record, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_FIRST, 0, 0, NULL},

	{GNOME_APP_UI_ITEM, N_("Previous"), N_("Previous record"),
		previous_record, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BACK, 0, 0, NULL},

	{GNOME_APP_UI_ITEM, N_("Next"), N_("Next record"),
		next_record, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_FORWARD, 0, 0, NULL},

	{GNOME_APP_UI_ITEM, N_("Last"), N_("Last record"),
		last_record, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_LAST, 0, 0, NULL},

	GNOMEUIINFO_END
};

static void canvas_clicked(GtkWidget *canvas, GdkEventButton *event)
{
	gabywindow *window = gtk_object_get_data(GTK_OBJECT(canvas), "window");
	GtkWidget *menu;
#ifdef DEBUG_GABY
	debug_print("[canvas_clicked] ... (button:%d)\n", event->button);
#endif
	if ( event->button != 3 ) return;
	
	menu = gtk_object_get_data(GTK_OBJECT(canvas), "menu");
	if ( menu == NULL ) {
#ifdef DEBUG_GABY
		debug_print("[canvas_clicked] creating menu.\n");
#endif
		menu = gtk_menu_new();
		gnome_app_fill_menu_with_data(&(GTK_MENU(menu)->menu_shell),
				go_menu, NULL, FALSE, 0, window );
		gtk_object_set_data(GTK_OBJECT(canvas), "menu", menu);
	}

#ifdef DEBUG_GABY
	debug_print("[canvas_clicked] showing menu.\n");
#endif
	gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, 0, 0 );
}

/* configuration functions */

static void configure_apply();
static void configure_save();
static GtkWidget* configure_widget = NULL;

static void stupid_XParseColor(char *color, guint8 *red, guint8 *green, guint8 *blue)
{
	char col[8], t;
	int tred, tgreen, tblue;
	if ( strlen(color) != 7 ) return;
	
	strcpy(col, color);
	t = col[3]; col[3] = 0;
	sscanf(col+1, "%x", &tred);
	col[3] = t; t = col[5]; col[5] = 0;
	sscanf(col+3, "%x", &tgreen);
	col[5] = t;
	sscanf(col+5, "%x", &tblue);

	*red = tred;
	*green = tgreen;
	*blue = tblue;
}

static void reset_defaults(GtkWidget *but, GtkWidget *table)
{
	GtkWidget *color_picker;
	guint8 red, green, blue;
	
	color_picker = gtk_object_get_data ( GTK_OBJECT(table), "header_box");
	stupid_XParseColor(DEFAULT_HEADER_BOX_COLOR, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );
	color_picker = gtk_object_get_data ( GTK_OBJECT(table), "label_box");
	stupid_XParseColor(DEFAULT_LABEL_BOX_COLOR, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );
	color_picker = gtk_object_get_data ( GTK_OBJECT(table), "header_text");
	stupid_XParseColor(DEFAULT_HEADER_TEXT_COLOR, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );
	color_picker = gtk_object_get_data ( GTK_OBJECT(table), "label_text");
	stupid_XParseColor(DEFAULT_LABEL_TEXT_COLOR, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );
	color_picker = gtk_object_get_data ( GTK_OBJECT(table), "body_text");
	stupid_XParseColor(DEFAULT_BODY_TEXT_COLOR, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );
	color_picker = gtk_object_get_data ( GTK_OBJECT(table), "background");
	stupid_XParseColor(DEFAULT_BACKGROUND_COLOR, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );

}

mstatic GtkWidget* canvas_configure (ViewPluginData *vpd)
{
	GtkWidget *table;
	GtkWidget *label, *button;
	GtkWidget *color_picker;
	guint8 red, green, blue;
	
#ifdef DEBUG_GABY
	debug_print("[canvas:configure] red:%x, green:%x, blue:%x\n",
							red, green, blue );
#endif
	
	table = gtk_table_new(7, 2, FALSE);

	label = gtk_label_new(_("Background color"));
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
			GTK_FILL, 0, GNOME_PAD, GNOME_PAD );
	color_picker = gnome_color_picker_new();
	stupid_XParseColor(background_color, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );
	gtk_widget_show(color_picker);
	gtk_signal_connect(GTK_OBJECT(color_picker), "color_set",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(table) );
	gtk_object_set_data(GTK_OBJECT(table), "background", color_picker);
	gtk_table_attach(GTK_TABLE(table), color_picker, 1, 2, 0, 1,
			0, 0, GNOME_PAD, GNOME_PAD );

	label = gtk_label_new(_("Header box color"));
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
			GTK_FILL, 0, GNOME_PAD, GNOME_PAD );
	color_picker = gnome_color_picker_new();
	stupid_XParseColor(header_box_color, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );
	gtk_widget_show(color_picker);
	gtk_signal_connect(GTK_OBJECT(color_picker), "color_set",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(table) );
	gtk_object_set_data(GTK_OBJECT(table), "header_box", color_picker);
	gtk_table_attach(GTK_TABLE(table), color_picker, 1, 2, 1, 2,
			0, 0, GNOME_PAD, GNOME_PAD );

	label = gtk_label_new(_("Label box color"));
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3,
			GTK_FILL, 0, GNOME_PAD, GNOME_PAD );
	color_picker = gnome_color_picker_new();
	stupid_XParseColor(label_box_color, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );
	gtk_widget_show(color_picker);
	gtk_signal_connect(GTK_OBJECT(color_picker), "color_set",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(table) );
	gtk_object_set_data(GTK_OBJECT(table), "label_box", color_picker);
	gtk_table_attach(GTK_TABLE(table), color_picker, 1, 2, 2, 3,
			0, 0, GNOME_PAD, GNOME_PAD );

	label = gtk_label_new(_("Header text color"));
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 3, 4,
			GTK_FILL, 0, GNOME_PAD, GNOME_PAD );
	color_picker = gnome_color_picker_new();
	stupid_XParseColor(header_text_color, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );
	gtk_widget_show(color_picker);
	gtk_signal_connect(GTK_OBJECT(color_picker), "color_set",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(table) );
	gtk_object_set_data(GTK_OBJECT(table), "header_text", color_picker);
	gtk_table_attach(GTK_TABLE(table), color_picker, 1, 2, 3, 4,
			0, 0, GNOME_PAD, GNOME_PAD );

	label = gtk_label_new(_("Label text color"));
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 4, 5,
			GTK_FILL, 0, GNOME_PAD, GNOME_PAD );
	color_picker = gnome_color_picker_new();
	stupid_XParseColor(label_text_color, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );
	gtk_widget_show(color_picker);
	gtk_signal_connect(GTK_OBJECT(color_picker), "color_set",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(table) );
	gtk_object_set_data(GTK_OBJECT(table), "label_text", color_picker);
	gtk_table_attach(GTK_TABLE(table), color_picker, 1, 2, 4, 5,
			0, 0, GNOME_PAD, GNOME_PAD );

	label = gtk_label_new(_("Body text color"));
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 5, 6,
			GTK_FILL, 0, GNOME_PAD, GNOME_PAD );
	color_picker = gnome_color_picker_new();
	stupid_XParseColor(body_text_color, &red, &green, &blue);
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			red, green, blue, 0 );
	gtk_widget_show(color_picker);
	gtk_signal_connect(GTK_OBJECT(color_picker), "color_set",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(table) );
	gtk_object_set_data(GTK_OBJECT(table), "body_text", color_picker);
	gtk_table_attach(GTK_TABLE(table), color_picker, 1, 2, 5, 6,
			0, 0, GNOME_PAD, GNOME_PAD );

	button = gtk_button_new_with_label(_("Set back to defaults"));
	gtk_signal_connect(GTK_OBJECT(button), "clicked", reset_defaults,table);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(table) );
	gtk_widget_show(button);
	gtk_table_attach(GTK_TABLE(table), button, 0, 2, 6, 7,
			0, 0, GNOME_PAD, GNOME_PAD );
	
	gtk_object_set_data(GTK_OBJECT(table), "name", vpd->i18n_name);
	gtk_object_set_data(GTK_OBJECT(table), "cfg_save", configure_save);
	gtk_object_set_data(GTK_OBJECT(table), "cfg_apply", configure_apply);

	configure_widget = table;
	gtk_widget_show(table);
	return table;
}

static void configure_apply()
{
	GtkWidget *wid, *color_picker;
	GList *tmp = list_windows;
	ViewPluginData *vpd;
	GList *items;
	guint8 red, green, blue;
	char color[20];
	GtkStyle *style;
	int i;
	gabywindow *otherwin;

#ifdef DEBUG_GABY
	debug_print("[canvas::configure_apply] ...\n");
#endif
	
	color_picker = gtk_object_get_data ( GTK_OBJECT(configure_widget),
				"header_box");
	gnome_color_picker_get_i8( GNOME_COLOR_PICKER(color_picker),
				&red, &green, &blue, NULL );
	sprintf(color, "#%02x%02x%02x", red, green, blue ); 
	strcpy(header_box_color, color);
	
	color_picker = gtk_object_get_data ( GTK_OBJECT(configure_widget),
				"label_box");
	gnome_color_picker_get_i8( GNOME_COLOR_PICKER(color_picker),
				&red, &green, &blue, NULL );
	sprintf(color, "#%02x%02x%02x", red, green, blue ); 
	strcpy(label_box_color, color);

	color_picker = gtk_object_get_data ( GTK_OBJECT(configure_widget),
				"header_text");
	gnome_color_picker_get_i8( GNOME_COLOR_PICKER(color_picker),
				&red, &green, &blue, NULL );
	sprintf(color, "#%02x%02x%02x", red, green, blue ); 
	strcpy(header_text_color, color);
	
	color_picker = gtk_object_get_data ( GTK_OBJECT(configure_widget),
				"label_text");
	gnome_color_picker_get_i8( GNOME_COLOR_PICKER(color_picker),
				&red, &green, &blue, NULL );
	sprintf(color, "#%02x%02x%02x", red, green, blue ); 
	strcpy(label_text_color, color);
	
	color_picker = gtk_object_get_data ( GTK_OBJECT(configure_widget),
				"body_text");
	gnome_color_picker_get_i8( GNOME_COLOR_PICKER(color_picker),
				&red, &green, &blue, NULL );
	sprintf(color, "#%02x%02x%02x", red, green, blue ); 
	strcpy(body_text_color, color);
	
	color_picker = gtk_object_get_data ( GTK_OBJECT(configure_widget),
				"background");
	gnome_color_picker_get_i8( GNOME_COLOR_PICKER(color_picker),
				&red, &green, &blue, NULL );
	sprintf(color, "#%02x%02x%02x", red, green, blue ); 
	strcpy(background_color, color);
	
	while ( tmp != NULL ) {
		otherwin = tmp->data;
		tmp = g_list_next(tmp);
		vpd = otherwin->view->type;
		if ( strcmp(vpd->name, "canvas") == 0 ) {
			wid = otherwin->widget;
			
			style = gtk_style_copy(gtk_widget_get_style(wid));
			for ( i=0; i<5; i++ ) {
				style->bg[i].red = red*256;
				style->bg[i].green = green*256;
				style->bg[i].blue = blue*256;
			}
			gtk_widget_set_style(wid, style);

			items = gtk_object_get_data (
					GTK_OBJECT(wid), "h1h2_boxes");
			update_box_color_bg(items, header_box_color);
						
			items = gtk_object_get_data (
					GTK_OBJECT(wid), "ftext_boxes");
			update_box_color_bg(items, label_box_color);
			
			items = gtk_object_get_data (
					GTK_OBJECT(wid), "h1h2_texts");
			update_text_color(items, header_text_color, FALSE);

			items = gtk_object_get_data (
					GTK_OBJECT(wid), "ftext_texts");
			update_text_color(items, label_text_color, FALSE);

			items = gtk_object_get_data(
					GTK_OBJECT(wid), "items");
			update_text_color(items, body_text_color, TRUE);
		}
	}

#ifdef DEBUG_GABY
	debug_print("[canvas::configure_apply] end\n");
#endif
}

static void configure_save()
{
	configure_apply();
	
	write_config_str("view","canvas","background_color" ,background_color);
	write_config_str("view","canvas","header_box_color" ,header_box_color);
	write_config_str("view","canvas","header_text_color",header_text_color);
	write_config_str("view","canvas","label_box_color"  ,label_box_color);
	write_config_str("view","canvas","label_text_color" ,label_text_color);
	write_config_str("view","canvas","body_text_color"  ,body_text_color);
}

mstatic void canvas_get_config()
{
	background_color = get_config_str("view", "canvas",
			"background_color",g_strdup(DEFAULT_BACKGROUND_COLOR) );
	header_box_color = get_config_str("view", "canvas",
			"header_box_color",g_strdup(DEFAULT_HEADER_BOX_COLOR) );
	label_box_color = get_config_str("view", "canvas",
			"label_box_color",g_strdup(DEFAULT_LABEL_BOX_COLOR) );
	header_text_color = get_config_str("view", "canvas",
			"header_text_color",g_strdup(DEFAULT_HEADER_TEXT_COLOR));
	label_text_color = get_config_str("view", "canvas",
			"label_text_color",g_strdup(DEFAULT_LABEL_TEXT_COLOR) );
	body_text_color = get_config_str("view", "canvas",
			"body_text_color",g_strdup(DEFAULT_BODY_TEXT_COLOR) );
	h1_fontset = get_config_str("view", "canvas",
		"h1_fontset", g_strdup(DEFAULT_H1_FONTSET));
	h2_fontset = get_config_str("view", "canvas",
		"h2_fontset", g_strdup(DEFAULT_H2_FONTSET));
	normal_fontset = get_config_str("view", "canvas",
		"normal_fontset", g_strdup(DEFAULT_NORMAL_FONTSET));
	ftext_fontset = get_config_str("view", "canvas",
		"ftext_fontset", g_strdup(DEFAULT_FTEXT_FONTSET));
}

static void update_configure_box()
{
	GtkWidget *color_picker;
	guint8 red, green, blue;
	char *items_names[] = {
		"background",  "header_box",
		"label_box", "header_text", "body_text"
	};
	char *color[] = {
		background_color, header_box_color,
		label_box_color, header_text_color, body_text_color
	};
	int i;

	if ( GTK_IS_WIDGET(configure_widget) ) {
		for (i=0; i<5; i++) {
			color_picker = gtk_object_get_data (
				GTK_OBJECT(configure_widget), items_names[i]);
			if ( GNOME_IS_COLOR_PICKER(color_picker) ) {
				stupid_XParseColor(color[i],&red,&green,&blue);
				gnome_color_picker_set_i8(
					GNOME_COLOR_PICKER(color_picker),
					red, green, blue, 0 );
			}
		}
	}
}

