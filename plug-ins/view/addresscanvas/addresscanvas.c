/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#if 0 /* this plug-in is obsolete */

#include <gaby.h>
#include <tables.h>
#include <records.h>
#include <f_config.h>
#include <v_main.h>

static GtkWidget* addresscanvas_create ( struct CommonInfos *ci, view *v,
					  gboolean first, ViewPluginData *vpd,
					  GtkWidget *parent );
static void addresscanvas_fill(GtkWidget *wid);
static GtkWidget* addresscanvas_configure (ViewPluginData *vpd);
static void addresscanvas_get_config();

#ifndef FOLLOW_MIGUEL
int init_view_plugin (ViewPluginData *vpd)
{
	vpd->view_create = addresscanvas_create;
	vpd->view_fill = addresscanvas_fill;
	vpd->configure = addresscanvas_configure;

	vpd->name = g_string_new("addresscanvas");
	vpd->i18n_name = g_string_new(_("Address canvas"));
	vpd->type = ONE_RECORD;
	vpd->capabilities = NONE;

	addresscanvas_get_config();

	return 0;
}
#endif

/* those are the fonts defined in gnomecard, we could perhaps use others */
#define NAME_FONTSET "-adobe-helvetica-medium-r-normal-*-18-*-*-*-p-*-*-*,-cronyx-helvetica-medium-r-normal-*-20-*-*-*-p-*-koi8-r,-*-*-medium-r-normal-*-18-*-*-*-*-*-ksc5601.1987-0,*"
#define TITLE_FONTSET "-adobe-helvetica-medium-r-*-*-14-*-*-*-p-*-*-*,-cronyx-helvetica-medium-r-normal-*-17-*-*-*-p-*-koi8-r,-*-*-medium-r-no-*-*-14-*-*-*-*-*-ksc5601.1987-0,*"
#define COMMENT_FONTSET "-adobe-helvetica-medium-r-*-*-10-*-*-*-p-*-*-*,-cronyx-helvetica-medium-r-normal-*-11-*-*-*-p-*-koi8-r,-*-*-medium-r-*-*-10-*-*-*-*-*-ksc5601.1987-0,*"
#define CANVAS_FONTSET "-adobe-helvetica-medium-r-*-*-12-*-*-*-p-*-*-*,-cronyx-helvetica-medium-r-normal-*-14-*-*-*-p-*-koi8-r,-*-*-medium-r-*-*-12-*-*-*-*-*-ksc5601.1987-0,*"

#define DEFAULT_HEADER_BOX_COLOR  "#f0e68c"	/* khaki */
#define DEFAULT_LABEL_BOX_COLOR   "#bdb76b"	/* dark khaki */

#define DEFAULT_HEADER_TEXT_COLOR "#000000"	/* black */
#define DEFAULT_LABEL_TEXT_COLOR  "#000000"	/* black */
#define DEFAULT_BODY_TEXT_COLOR   "#000000"	/* black */

#define DEFAULT_BACKGROUND_COLOR  "#ffffff"	/* white */

static char* header_box_color;
static char* label_box_color;
static char* header_text_color;
static char* label_text_color;
static char* body_text_color;
static char* background_color;

static GnomeCanvasGroup* canvas_create_real(GtkWidget *canvas);
static void drag_data_received(GtkWidget *widget, GdkDragContext *context,
			       gint x, gint y, GtkSelectionData *selection_data,
			       guint info, guint time);

static GtkWidget* addresscanvas_create ( struct CommonInfos *ci, view *v,
					  gboolean first, ViewPluginData *vpd,
					  GtkWidget *parent )
{
	GtkWidget *canvas;
	GnomeCanvasGroup *root;
	gdouble x1, x2, y1, y2;
	int *id;
	record *r;
	static GtkTargetEntry target_table[] = {
		{ "application/x-color", 0, 1 }
	};
		
	if ( strcmp(v->subtable->name, "Address Book") != 0 ) {
#ifdef DEBUG_GABY
		debug_print("[canvas_create] v->st->name == %s\n",
							v->subtable->name );
#endif
		return NULL;	/* and you should handle this ! */
	}

	canvas = gnome_canvas_new();
	
	gtk_widget_set_usize(canvas, -1, 250);
	
	id = g_malloc(sizeof(int));
	
	r = table_first(v->subtable->table, -1);
	*id = ( r == NULL ? 0 : r->id );
	
	gtk_object_set_data(GTK_OBJECT(canvas), " id ", id);
	gtk_object_set_data(GTK_OBJECT(canvas), " view ", v );
	gtk_object_set_data(GTK_OBJECT(canvas), " vpd ", vpd);
	gtk_object_set_data(GTK_OBJECT(canvas), " parent ", parent);

	root = canvas_create_real(canvas);

	gtk_signal_connect (GTK_OBJECT(canvas), "drag_data_received",
			GTK_SIGNAL_FUNC(drag_data_received), NULL );

	gtk_drag_dest_set (GTK_WIDGET(canvas),
			GTK_DEST_DEFAULT_MOTION |
			GTK_DEST_DEFAULT_HIGHLIGHT |
			GTK_DEST_DEFAULT_DROP,
			target_table, 1, GDK_ACTION_COPY );

	/* all done */

	gnome_canvas_item_get_bounds(GNOME_CANVAS_ITEM(root), &x1,&y1,&x2,&y2);
	gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas), x1, y1, x2, y2);
	
	gtk_widget_show(canvas);

	return canvas;
}

static void update_box_color_bg(GList *items, gchar *color)
{
	GnomeCanvasItem *item;
	GList *tmp = ( items == NULL ) ? NULL : g_list_first(items);
	
	while ( tmp != NULL ) {
		item = tmp->data;
		gnome_canvas_item_set( item,
				"fill_color", color,
				"outline_color", color,
				NULL);
		/*
		 *  updating the configure box (if present)
		 */
		tmp = g_list_next(tmp);
	}
	
}

static void drag_data_received(GtkWidget *widget, GdkDragContext *context,
			       gint x, gint y, GtkSelectionData *selection_data,
			       guint info, guint time)
{
	GnomeCanvasItem *item, *item2;
	GtkStyle *style;
	guint16 *data = (guint16 *)selection_data->data;
	char color[20];
	int i;
	GList *header_box_bg = gtk_object_get_data(GTK_OBJECT(widget),
							"header_box_bg" );
	GList *label_box_bg = gtk_object_get_data(GTK_OBJECT(widget),
							"label_box_bg"  );
	
	if ( info == 1 ) {
		
		if ( selection_data->length != 8 )
			return;

#ifdef DEBUG_GABY
		debug_print("[drag_data_received] got a color at (%d,%d)\n", x, y);
#endif

		item = gnome_canvas_get_item_at(GNOME_CANVAS(widget),
						(double)x, (double)y );

		sprintf(color, "#%x%x%x",data[0]/256,data[1]/256,data[2]/256 ); 

		if ( item == NULL ) { /* background was hit */
			style = gtk_style_copy(gtk_widget_get_style(widget));
	
			for ( i=0; i<5; i++ ) {
				style->bg[i].red = data[0];
				style->bg[i].green = data[1];
				style->bg[i].blue = data[2];
			}
			
			gtk_widget_set_style(widget, style);

			strcpy(background_color, color);

			return;
		}
		
		if ( GNOME_IS_CANVAS_RE(item) ) { /* a rectangle (or ellipse)*/
			if ( g_list_find(header_box_bg, item) ) {
				update_box_color_bg(header_box_bg, color);
				strcpy(header_box_color, color);
				return;
			}

			if ( g_list_find(label_box_bg, item) ) {
				update_box_color_bg(label_box_bg, color);
				strcpy(label_box_color, color);
				return;
			}

			return;
		}
		
		if ( GNOME_IS_CANVAS_TEXT(item) ) { /* a text */
			/*
			 *  update other text item
			 */
			gnome_canvas_item_set( item,
					"fill_color", color,
					NULL);
			return;
		}

	}
}

static GnomeCanvasGroup* canvas_create_real(GtkWidget *canvas)
{
	GtkStyle *style;
	GnomeCanvasGroup *root;
	GdkFont *name_font, *title_font, *canvas_font, *comment_font;
	GnomeCanvasItem *name_item, *street_item, *zipcity_item,
			*state_item, *country_item, *phone_item,
			*fax_item, *email_item, *web_item;
	GList *header_box_bg = NULL;
	GList *label_box_bg = NULL;
	char str[3];
	gushort red, green, blue;
	int i;
	
	gtk_widget_pop_visual();
	gtk_widget_pop_colormap();

	style = gtk_style_copy(gtk_widget_get_style(canvas));
	
	/* this is _ugly_ (tm) but I need X11 color strings for canvasitem and
	 * it wouldn't be logic to use rgb for the background and I don't know
	 * about X11 programming. (I believe I have to use XParseColor to get
	 * the rgb color out of a string but nothing more ...)
	 */
	
	str[2] = 0;

	str[0] = background_color[1];
	str[1] = background_color[2];
	sscanf(str, "%hx", &red);
	red *= 256;

	str[0] = background_color[3];
	str[1] = background_color[4];
	sscanf(str, "%hx", &green);
	green *= 256;

	str[0] = background_color[5];
	str[1] = background_color[6];
	sscanf(str, "%hx", &blue);
	blue *= 256;

#ifdef DEBUG_GABY
	debug_print("[canvas_create_real] red : %hd, green : %hd, blue : %hd\n",
			red, green, blue );
#endif

	for ( i=0; i<5; i++ ) {
		style->bg[i].red = red;
		style->bg[i].green = green;
		style->bg[i].blue = blue;
	}
			
	gtk_widget_set_style(GTK_WIDGET(canvas), style);

	root = GNOME_CANVAS_GROUP(gnome_canvas_root(GNOME_CANVAS(canvas)));

	name_font = gdk_fontset_load (NAME_FONTSET);
	title_font = gdk_fontset_load (TITLE_FONTSET);
	canvas_font = gdk_fontset_load (CANVAS_FONTSET);
	comment_font = gdk_fontset_load (COMMENT_FONTSET);

	header_box_bg = g_list_append(header_box_bg,
		gnome_canvas_item_new(root, gnome_canvas_rect_get_type(),
			"x1", 5.0,
			"y1", 5.0,
			"x2", 200.0,
			"y2", 35.0,
			"fill_color", header_box_color,
			"outline_color", header_box_color,
			"width_pixels", 1,
			NULL)
		);
	
	name_item = gnome_canvas_item_new(root, gnome_canvas_text_get_type(),
			"text", "",
			"x", 10.0,
			"y", 20.0,
			"fontset", NAME_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", header_text_color,
			NULL );
	gtk_object_set_data(GTK_OBJECT(canvas), "name_item", name_item);

	street_item = gnome_canvas_item_new(root, gnome_canvas_text_get_type(),
			"text", "",
			"x", 10.0,
			"y", 45.0,
			"fontset", CANVAS_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", body_text_color,
			NULL );
	gtk_object_set_data(GTK_OBJECT(canvas), "street_item", street_item);
	
	zipcity_item = gnome_canvas_item_new(root, gnome_canvas_text_get_type(),
			"text", "",
			"x", 10.0,
			"y", 60.0,
			"fontset", CANVAS_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", body_text_color,
			NULL );
	gtk_object_set_data(GTK_OBJECT(canvas), "zipcity_item", zipcity_item);
	
	state_item = gnome_canvas_item_new(root, gnome_canvas_text_get_type(),
			"text", "",
			"x", 10.0,
			"y", 75.0,
			"fontset", CANVAS_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", body_text_color,
			NULL );
	gtk_object_set_data(GTK_OBJECT(canvas), "state_item", state_item);
	
	country_item = gnome_canvas_item_new(root, gnome_canvas_text_get_type(),
			"text", "",
			"x", 10.0,
			"y", 90.0,
			"fontset", CANVAS_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", body_text_color,
			NULL );
	gtk_object_set_data(GTK_OBJECT(canvas), "country_item", country_item);
	
	header_box_bg = g_list_append(header_box_bg,
		gnome_canvas_item_new (root, gnome_canvas_rect_get_type (),
			"x1", 5.0,
			"y1", 100.0,
			"x2", 200.0,
			"y2", 120.0,
			"fill_color", header_box_color,
			"outline_color", header_box_color,
			"width_pixels", 0,
			NULL)
		);

	gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", _("Phone Numbers"),
			"x", 10.0,
			"y", 110.0,
			"fontset",  TITLE_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", header_text_color,
			NULL);

	label_box_bg = g_list_append(label_box_bg,
		gnome_canvas_item_new (root, gnome_canvas_rect_get_type (),
			"x1", 10.0,
			"y1", 124.0,
			"x2", 65.0,
			"y2", 136.0,
			"fill_color", label_box_color,
			"outline_color", label_box_color,
			"width_pixels", 0,
			NULL)
		);
	
	gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", _("Private :"),
			"x", 60.0,
			"y", 130.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_EAST,
			"fill_color", label_text_color,
			NULL);
	
	phone_item = gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", "",
			"x", 70.0,
			"y", 130.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", body_text_color,
			NULL);
	gtk_object_set_data(GTK_OBJECT(canvas), "privatephone_item",phone_item);
	
	label_box_bg = g_list_append(label_box_bg,
		gnome_canvas_item_new (root, gnome_canvas_rect_get_type (),
			"x1", 10.0,
			"y1", 139.0,
			"x2", 65.0,
			"y2", 151.0,
			"fill_color", label_box_color,
			"outline_color", label_box_color,
			"width_pixels", 0,
			NULL)
		);
	
	gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", _("Work :"),
			"x", 60.0,
			"y", 145.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_EAST,
			"fill_color", label_text_color,
			NULL);
	phone_item = gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", "",
			"x", 70.0,
			"y", 145.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", body_text_color,
			NULL);
	gtk_object_set_data(GTK_OBJECT(canvas), "workphone_item",phone_item);
	
	label_box_bg = g_list_append(label_box_bg,
	gnome_canvas_item_new (root, gnome_canvas_rect_get_type (),
			"x1", 10.0,
			"y1", 154.0,
			"x2", 65.0,
			"y2", 166.0,
			"fill_color", label_box_color,
			"outline_color", label_box_color,
			"width_pixels", 0,
			NULL)
		);
	
	gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", _("Cellular :"),
			"x", 60.0,
			"y", 160.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_EAST,
			"fill_color", label_text_color,
			NULL);
	phone_item = gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", "",
			"x", 70.0,
			"y", 160.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", body_text_color,
			NULL);
	gtk_object_set_data(GTK_OBJECT(canvas), "cellphone_item",phone_item);
	
	header_box_bg = g_list_append(header_box_bg,
		gnome_canvas_item_new (root, gnome_canvas_rect_get_type (),
			"x1", 5.0,
			"y1", 170.0,
			"x2", 200.0,
			"y2", 190.0,
			"fill_color", header_box_color,
			"outline_color", header_box_color,
			"width_pixels", 0,
			NULL)
		);

	gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", _("Others"),
			"x", 10.0,
			"y", 180.0,
			"fontset",  TITLE_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", header_text_color,
			NULL);

	label_box_bg = g_list_append(label_box_bg,
		gnome_canvas_item_new (root, gnome_canvas_rect_get_type (),
			"x1", 10.0,
			"y1", 194.0,
			"x2", 65.0,
			"y2", 206.0,
			"fill_color", label_box_color,
			"outline_color", label_box_color,
			"width_pixels", 0,
			NULL)
		);
	gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", _("Fax :"),
			"x", 60.0,
			"y", 200.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_EAST,
			"fill_color", label_text_color,
			NULL);
	
	fax_item = gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", "",
			"x", 70.0,
			"y", 200.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", body_text_color,
			NULL);
	gtk_object_set_data(GTK_OBJECT(canvas), "fax_item",fax_item);
	
	label_box_bg = g_list_append(label_box_bg,
		gnome_canvas_item_new (root, gnome_canvas_rect_get_type (),
			"x1", 10.0,
			"y1", 209.0,
			"x2", 65.0,
			"y2", 221.0,
			"fill_color", label_box_color,
			"outline_color", label_box_color,
			"width_pixels", 0,
			NULL)
		);
	gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", _("E-Mail :"),
			"x", 60.0,
			"y", 215.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_EAST,
			"fill_color", label_text_color,
			NULL);
	
	email_item = gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", "",
			"x", 70.0,
			"y", 215.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", body_text_color,
			NULL);
	gtk_object_set_data(GTK_OBJECT(canvas), "email_item",email_item);
	
	label_box_bg = g_list_append(label_box_bg,
		gnome_canvas_item_new (root, gnome_canvas_rect_get_type (),
			"x1", 10.0,
			"y1", 224.0,
			"x2", 65.0,
			"y2", 236.0,
			"fill_color", label_box_color,
			"outline_color", label_box_color,
			"width_pixels", 0,
			NULL)
		);
	gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", _("Web site :"),
			"x", 60.0,
			"y", 230.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_EAST,
			"fill_color", label_text_color,
			NULL);
	
	web_item = gnome_canvas_item_new (root, gnome_canvas_text_get_type (),
			"text", "",
			"x", 70.0,
			"y", 230.0,
			"fontset",  COMMENT_FONTSET,
			"anchor", GTK_ANCHOR_WEST,
			"fill_color", body_text_color,
			NULL);
	gtk_object_set_data(GTK_OBJECT(canvas), "web_item", web_item);
	
	gtk_object_set_data(GTK_OBJECT(canvas), "header_box_bg", header_box_bg);
	gtk_object_set_data(GTK_OBJECT(canvas), "label_box_bg", label_box_bg);

	return root;
}

static void addresscanvas_fill(GtkWidget *wid)
{
	subtable *f;
	int *id;
	GnomeCanvasItem *item;
	GString *str, *tmp_str;
	record *r;
	
	f = ((view*)gtk_object_get_data(GTK_OBJECT(wid), " view "))->subtable;
	id = gtk_object_get_data(GTK_OBJECT(wid), " id ");
	
	r = get_record_no(f->table, *id);
	
	item = gtk_object_get_data(GTK_OBJECT(wid), "name_item");
	str = get_subtable_stringed_field(f, r, 0); /* first name */
	tmp_str = get_subtable_stringed_field(f, r, 1); /* last name */
	str = g_string_append(str, " ");
	str = g_string_append(str, tmp_str->str);
	gnome_canvas_item_set(item, "text", str->str, NULL);
	g_string_free(tmp_str, 1);
	g_string_free(str, 1);
	
	item = gtk_object_get_data(GTK_OBJECT(wid), "street_item");
	str = get_subtable_stringed_field(f, r, 3); /* street */
	gnome_canvas_item_set(item, "text", str->str, NULL);
	g_string_free(str, 1);
	
	/* I draw '<zip code> <city>' but I don't know if it is the same for
	 * canada and/or us and/or other
	 */
	item = gtk_object_get_data(GTK_OBJECT(wid), "zipcity_item");
	str = get_subtable_stringed_field(f, r, 4); /* zip code */
	tmp_str = get_subtable_stringed_field(f, r, 5); /* city */
	str = g_string_append(str, " ");
	str = g_string_append(str, tmp_str->str);
	gnome_canvas_item_set(item, "text", str->str, NULL);
	g_string_free(tmp_str, 1);
	g_string_free(str, 1);
	
	str = get_subtable_stringed_field(f, r, 6); /* state/province */
	if ( str->len != 0 ) {
		item = gtk_object_get_data(GTK_OBJECT(wid), "state_item");
		gnome_canvas_item_set(item, "text", str->str, NULL);
		item =  gtk_object_get_data(GTK_OBJECT(wid), "country_item");
	} else {
		item =  gtk_object_get_data(GTK_OBJECT(wid), "country_item");
		gnome_canvas_item_set(item, "text", "", NULL);
		item = gtk_object_get_data(GTK_OBJECT(wid), "state_item");
	}
	g_string_free(str, 1);
	str = get_subtable_stringed_field(f, r, 7); /* country */
	gnome_canvas_item_set(item, "text", str->str, NULL);
	g_string_free(str, 1);
	
	item = gtk_object_get_data(GTK_OBJECT(wid), "privatephone_item" );
	str = get_subtable_stringed_field(f, r, 9 ); /* private phone */
	gnome_canvas_item_set(item, "text", str->str, NULL);
	g_string_free(str, 1);
	
	item = gtk_object_get_data(GTK_OBJECT(wid), "workphone_item" );
	str = get_subtable_stringed_field(f, r, 10 ); /* work phone */
	gnome_canvas_item_set(item, "text", str->str, NULL);
	g_string_free(str, 1);
	
	item = gtk_object_get_data(GTK_OBJECT(wid), "cellphone_item" );
	str = get_subtable_stringed_field(f, r, 11 ); /* cell phone */
	gnome_canvas_item_set(item, "text", str->str, NULL);
	g_string_free(str, 1);
	
	item = gtk_object_get_data(GTK_OBJECT(wid), "fax_item" );
	str = get_subtable_stringed_field(f, r, 12 ); /* fax */
	gnome_canvas_item_set(item, "text", str->str, NULL);
	g_string_free(str, 1);
	
	item = gtk_object_get_data(GTK_OBJECT(wid), "email_item" );
	str = get_subtable_stringed_field(f, r, 13 ); /* email */
	gnome_canvas_item_set(item, "text", str->str, NULL);
	g_string_free(str, 1);
	
	item = gtk_object_get_data(GTK_OBJECT(wid), "web_item" );
	str = get_subtable_stringed_field(f, r, 14 ); /* web site */
	gnome_canvas_item_set(item, "text", str->str, NULL);
	g_string_free(str, 1);
	
}


/* configuration functions */

static GtkWidget* configure_widget = NULL;
static void configure_apply();
static void configure_save();

static GtkWidget* addresscanvas_configure (ViewPluginData *vpd)
{
	GtkWidget *table;
	GtkWidget *label;
	GtkWidget *color_picker;
	
	table = gtk_table_new(5, 2, FALSE);

	label = gtk_label_new(_("Header box color"));
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1,
			GTK_FILL, 0, GNOME_PAD, GNOME_PAD );
	color_picker = gnome_color_picker_new();
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			240, 230, 140, 0 );	/* khaki */
	gtk_widget_show(color_picker);
	gtk_object_set_data(GTK_OBJECT(table), "header_box", color_picker);
	gtk_table_attach(GTK_TABLE(table), color_picker, 1, 2, 0, 1,
			0, 0, GNOME_PAD, GNOME_PAD );

	label = gtk_label_new(_("Label box color"));
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
			GTK_FILL, 0, GNOME_PAD, GNOME_PAD );
	color_picker = gnome_color_picker_new();
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			189, 183, 107, 0 );	/* dark khaki */
	gtk_widget_show(color_picker);
	gtk_object_set_data(GTK_OBJECT(table), "label_box", color_picker);
	gtk_table_attach(GTK_TABLE(table), color_picker, 1, 2, 1, 2,
			0, 0, GNOME_PAD, GNOME_PAD );

	label = gtk_label_new(_("Header text color"));
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3,
			GTK_FILL, 0, GNOME_PAD, GNOME_PAD );
	color_picker = gnome_color_picker_new();
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			0, 0, 0, 0 );	/* black */
	gtk_widget_show(color_picker);
	gtk_object_set_data(GTK_OBJECT(table), "header_text", color_picker);
	gtk_table_attach(GTK_TABLE(table), color_picker, 1, 2, 2, 3,
			0, 0, GNOME_PAD, GNOME_PAD );

	label = gtk_label_new(_("Label text color"));
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 3, 4,
			GTK_FILL, 0, GNOME_PAD, GNOME_PAD );
	color_picker = gnome_color_picker_new();
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			0, 0, 0, 0 );	/* black */
	gtk_widget_show(color_picker);
	gtk_object_set_data(GTK_OBJECT(table), "label_text", color_picker);
	gtk_table_attach(GTK_TABLE(table), color_picker, 1, 2, 3, 4,
			0, 0, GNOME_PAD, GNOME_PAD );

	label = gtk_label_new(_("Body text color"));
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 4, 5,
			GTK_FILL, 0, GNOME_PAD, GNOME_PAD );
	color_picker = gnome_color_picker_new();
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(color_picker),
			0, 0, 0, 0 );	/* black */
	gtk_widget_show(color_picker);
	gtk_object_set_data(GTK_OBJECT(table), "body_text", color_picker);
	gtk_table_attach(GTK_TABLE(table), color_picker, 1, 2, 4, 5,
			0, 0, GNOME_PAD, GNOME_PAD );

	gtk_object_set_data(GTK_OBJECT(table), "name", vpd->i18n_name->str);
	gtk_object_set_data(GTK_OBJECT(table), "cfg_save", configure_save);
	gtk_object_set_data(GTK_OBJECT(table), "cfg_apply", configure_apply);

	configure_widget = table;
	gtk_widget_show(table);
	return table;
}

static void configure_apply()
{
	;
}

static void configure_save()
{
	;
}

static void addresscanvas_get_config()
{
	background_color = get_config_str("view", "addresscanvas",
			"background_color",g_strdup(DEFAULT_BACKGROUND_COLOR) );
	header_box_color = get_config_str("view", "addresscanvas",
			"header_box_color",g_strdup(DEFAULT_HEADER_BOX_COLOR) );
	label_box_color = get_config_str("view", "addresscanvas",
			"label_box_color",g_strdup(DEFAULT_LABEL_BOX_COLOR) );
	header_text_color = get_config_str("view", "addresscanvas",
			"header_text_color",g_strdup(DEFAULT_HEADER_TEXT_COLOR));
	label_text_color = get_config_str("view", "addresscanvas",
			"label_text_color",g_strdup(DEFAULT_LABEL_TEXT_COLOR) );
	body_text_color = get_config_str("view", "addresscanvas",
			"body_text_color",g_strdup(DEFAULT_BODY_TEXT_COLOR) );

}

#endif /* 0 - this plug-in is obsolete */

