/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* TODO (post-2.0): it would be nice if there was _one_ header file for plug-ins
 * delayed since this is a code wishlist issue */
#include <gaby.h>
#include <tables.h>
#include <records.h>
#include <f_config.h>
#include <windows.h>
#include <gtk_config_dlg.h>	/* for gaby_property_box_changed */
#include <gtk_menu.h>	/* for actions_menu_for_table */


mstatic void xlist_create ( gabywindow *window, gboolean first );
mstatic void xlist_fill ( gabywindow *window );
mstatic GtkWidget* xlist_configure (ViewPluginData *vpd);
mstatic void xlist_get_config();
mstatic void xlist_get_records(gabywindow *window, GList **records);

#ifndef FOLLOW_MIGUEL
int init_view_plugin (ViewPluginData *vpd)
{
	vpd->view_create = xlist_create;
	vpd->view_fill = xlist_fill;
	vpd->configure = xlist_configure;
	vpd->view_records = xlist_get_records;
	
	vpd->name = "xlist";
	vpd->i18n_name = _("Extended List");
	vpd->type = ALL_RECORDS;
	vpd->capabilities = FILTERABLE;

	xlist_get_config();

#ifdef DEBUG_GABY
	debug_print("Initialization of view plugin '%s' done succesfully.\n",
			vpd->i18n_name );
#endif

	return 0;
}
#endif

static GtkWidget* create_search_box ( gabywindow *window );
static GtkWidget* create_field_list_menu ( gabywindow *window );
static GtkWidget* create_popup_menu( gabywindow *window );

static void clist_click_column (GtkCList *clist, gint column, gabywindow *win);

static void list_fill_real_all (GtkWidget *cl, subtable *l, gchar *ss,
				gint sf, gint id);
static void list_fill_real_some (GtkWidget *cl, subtable *l, gchar *ss,
				 GList *what, gint sf, gint id);

static void clist_mouse_event (GtkWidget *clist, GdkEventButton *event,
			       gabywindow *win );
static void select_record( GtkCList *clist, gint row, gint column,
		    GdkEventButton *event, gabywindow *window );
static void unselect_record( GtkCList *clist, gint row, gint column,
		    GdkEventButton *event, int *id );

#if 0
static void change_record(GtkWidget *t, GtkWidget *win);
#endif

static void search_event(GtkWidget *entry, gabywindow *w);
static void search_field_selected ( GtkWidget *mi, gabywindow *w );

static int (*strncmp_fct) (const char *s1, const char *s2, unsigned int n);
static gint xlist_compare(GtkCList *clist, gconstpointer p1, gconstpointer p2);

mstatic void xlist_create ( gabywindow *window, gboolean first )
{
	GtkWidget *vbox, *hbox;
	GtkWidget *clist;
	GtkWidget *sclwin;
#ifdef USE_GNOME_1
	GtkWidget *dock_item;
#endif
	int i;
	subtable *subt = window->view->subtable;
	int *id = &(window->id);

#ifdef DEBUG_GABY
	debug_print("%s\n", _("Extended List"));
#endif
	
	*id = 0;

	vbox = gtk_vbox_new(FALSE, 5);
	window->widget = vbox;
	gtk_widget_show(vbox);
	
	sclwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sclwin),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_widget_show(sclwin);

	clist = gtk_clist_new(subt->nb_fields+1);
	gtk_widget_set_usize(clist, -1, 350); /* we hard code size *
					       * this is bad :(    */
	gtk_clist_set_column_visibility (GTK_CLIST(clist), 0, FALSE);
	gtk_clist_set_row_height(GTK_CLIST(clist), 20 );
	gtk_clist_set_compare_func(GTK_CLIST(clist), xlist_compare);
	gtk_object_set_data(GTK_OBJECT(clist), "window", window);
	
	gtk_signal_connect(GTK_OBJECT(clist), "click_column",
			GTK_SIGNAL_FUNC(clist_click_column), window );
	gtk_signal_connect(GTK_OBJECT(clist), "button_press_event",
			GTK_SIGNAL_FUNC(clist_mouse_event), window);
	gtk_signal_connect(GTK_OBJECT(clist), "select_row",
			GTK_SIGNAL_FUNC(select_record), window);
	gtk_signal_connect(GTK_OBJECT(clist), "unselect_row",
			GTK_SIGNAL_FUNC(unselect_record), id);
	
	gtk_widget_show(clist);
	gtk_container_add (GTK_CONTAINER (sclwin), clist);

	gtk_clist_column_titles_show (GTK_CLIST (clist));
	
	for ( i=1; i< subt->nb_fields+1;i++ ) {
		char buf[100];
		int width;
		gtk_clist_set_column_title( GTK_CLIST (clist), i ,
				subt->fields[i-1].i18n_name );
#ifdef DEBUG_GABY
		debug_print("[xlist:create] col %d : %s\n", i, subt->fields[i-1].i18n_name);
#endif
		sprintf(buf, "col%d_width", i);
		width = get_config_int("gaby_xlist", 
			window->view->subtable->name, buf, 80 );
		gtk_clist_set_column_width (GTK_CLIST (clist), i, width);
	}

	gtk_box_pack_start (GTK_BOX (vbox), sclwin, TRUE, TRUE, 0);

	hbox = create_search_box(window);

#ifdef USE_GNOME_1
	if ( GNOME_IS_APP(window->parent)) {
		/* EXCLUSIVE means it will fill the whole width */
		dock_item = gnome_dock_item_new ( "Search box",
				GNOME_DOCK_ITEM_BEH_EXCLUSIVE |
				GNOME_DOCK_ITEM_BEH_NEVER_VERTICAL);
		gtk_container_add(GTK_CONTAINER(dock_item), hbox);
		gnome_dock_layout_add_item(GNOME_APP(window->parent)->layout,
				GNOME_DOCK_ITEM(dock_item),
				GNOME_DOCK_BOTTOM, 0, 0, 0 );
	} else {
		/* back to the gtk behaviour */
		gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);
	}
#else
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);
#endif

	gtk_object_set_data(GTK_OBJECT(vbox), "clist", clist );
	gtk_object_set_data(GTK_OBJECT(vbox), "actmenu",
						create_popup_menu(window));

	gtk_widget_set_usize(vbox, 400, 400);
	gtk_container_border_width(GTK_CONTAINER(vbox), 5);
	
	return;
}

static void save_column_width ( GtkWidget *mi, gabywindow *window)
{
	GtkCList *clist;
	GtkCListColumn *col;
	int i;
	char buf[100];

	clist = gtk_object_get_data(GTK_OBJECT(window->widget), "clist");
	for ( i=1; i<clist->columns; i++ ) {
		col = &(clist->column[i]);
#ifdef DEBUG_GABY
		debug_print("[xlist:save_column_width] col %d: %d\n", i,
				col->width );
#endif
		sprintf(buf, "col%d_width", i);
		write_config_int("gaby_xlist", window->view->subtable->name,
				buf, col->width );
	}
	
}

static GtkWidget* create_popup_menu( gabywindow *window )
{
	GtkWidget *menu, *item;
	
	menu = actions_menu_for_table(window->view->subtable->table,window);
	
	if ( menu != NULL ) {
		item = gtk_menu_item_new();
		gtk_menu_append(GTK_MENU(menu), item);
		gtk_widget_show(item);
	} else {
		menu = gtk_menu_new();
	}

	item = gtk_menu_item_new_with_label( _("Save columns' width") );
	gtk_signal_connect(GTK_OBJECT(item), "activate", 
			GTK_SIGNAL_FUNC(save_column_width), window );
	gtk_menu_append(GTK_MENU(menu), item);
	gtk_widget_show(item);
	
	return menu;
}

static GtkWidget* create_search_box( gabywindow *window )
{
	GtkWidget *vbox = window->widget;
	GtkWidget *hbox;
	GtkWidget *label, *entry, *field_list;
	
	hbox = gtk_hbox_new ( FALSE , 5);
	gtk_widget_show(hbox);
	
	label = gtk_label_new(_("Search for"));
	gtk_widget_show(label);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 5);

	entry = gtk_entry_new();
	gtk_widget_show(entry);
	gtk_signal_connect (GTK_OBJECT(entry), "changed",
			GTK_SIGNAL_FUNC(search_event), window);
	gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, FALSE, 5);
	
	label = gtk_label_new(_("in"));
	gtk_widget_show(label);
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, FALSE, 5);

	field_list = gtk_option_menu_new();
	gtk_option_menu_set_menu(GTK_OPTION_MENU(field_list),
					create_field_list_menu(window));
	gtk_widget_show(field_list);
	gtk_box_pack_start (GTK_BOX (hbox), field_list, TRUE, FALSE, 5);	
	
	gtk_object_set_data(GTK_OBJECT(vbox), "search_entry", entry);
	gtk_object_set_data(GTK_OBJECT(vbox), "search_fieldlist", field_list);

	return hbox;
}

static gint xlist_compare(GtkCList *clist, gconstpointer p1, gconstpointer p2)
{
	gabywindow *win = gtk_object_get_data(GTK_OBJECT(clist), "window");
	view *v = win->view;
	GtkCListRow *row1 = (GtkCListRow*)p1;
	GtkCListRow *row2 = (GtkCListRow*)p2;
#if 0	/* this would be better but isn't fast enough */
	record *r1 = get_record_no(v->subtable->table,
				atoi( GTK_CELL_TEXT(row1->cell[0])->text ));
	record *r2 = get_record_no(v->subtable->table,
				atoi( GTK_CELL_TEXT(row2->cell[0])->text ));
#endif
	int field_no = v->subtable->fields[clist->sort_column-1].no;
	char *text1;
	char *text2;
	GDate d1, d2;
	
	text1 = GTK_CELL_TEXT (row1->cell[clist->sort_column])->text;
	text2 = GTK_CELL_TEXT (row2->cell[clist->sort_column])->text;
	
	if (!text2)
		return (text1 != NULL);

	if (!text1)
		return -1;
	
	switch ( v->subtable->table->fields[field_no].type ) {
		case T_STRING:
		case T_STRINGS:
		case T_RECORD:
		case T_BOOLEAN:
		case T_MULTIMEDIA:
		{
			return strcmp(text1, text2);
		} break;
		case T_INTEGER:
		{
			return ( atoi(text1) < atoi(text2) ) ? -1 : 1 ;
		} break;
		case T_REAL:
		case T_DECIMAL:
		{
			return ( atof(text1) < atof(text2) ) ? -1 : 1 ;
		} break;
		case T_DATE:
		{
			g_date_set_parse(&d1, text1);
			g_date_set_parse(&d2, text2);
			if ( d2.month == G_DATE_BAD_MONTH ) 
				return 1;
			if ( d1.month == G_DATE_BAD_MONTH ) return -1;
			return ( g_date_compare(&d1, &d2));
		} break;
		default: { return -1; } break;
	}
}

static void clist_click_column (GtkCList *clist, gint column, gabywindow *win)
{
	/* it strangely looks like something seen in testgtk
	 * wait ... it _is_ in testgtk :) 
	 * (the difference is our home-made sort function)
	 */
	view *v = win->view;

#ifdef DEBUG_GABY
	debug_print("[clist_click_column] Sorting %s\n",
			v->subtable->fields[column-1].i18n_name );
#endif

	if ( column == clist->sort_column ) {
		if ( clist->sort_type == GTK_SORT_ASCENDING ) {
			clist->sort_type = GTK_SORT_DESCENDING;
		} else {
			clist->sort_type = GTK_SORT_ASCENDING;
		}
	} else {
		gtk_clist_set_sort_column(clist, column);
	}
	gtk_clist_sort(clist);

/* if you ever saw gaby 1.0 you should say 'wooow' and thanks gtk+ */
}

mstatic void xlist_fill ( gabywindow *window )
{
	subtable *l;
	GtkWidget *clist;
	GtkWidget *t;
	gchar *search_string;
	gint search_field;
	GList *what;
	condition *c;
	view *v;
	int *id;
	gboolean foo = FALSE;
	GtkWidget *w = window->widget;
	int width, height, hoffset, voffset;
	
	v = window->view;
	l = v->subtable;
	
	clist = gtk_object_get_data(GTK_OBJECT(w), "clist");

	c = gtk_object_get_data(GTK_OBJECT(w), "condition");

	t = gtk_object_get_data(GTK_OBJECT(w), "search_entry");
	search_string = gtk_entry_get_text(GTK_ENTRY(t));
	t = gtk_object_get_data(GTK_OBJECT(w), "search_fieldlist");
	search_field = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(t), "which"));
	what = window->what;

	id = &(window->id);
	
#ifdef DEBUG_GABY
	debug_print("[xlist_fill] clist informations:\n");
	debug_print(" width:%d, height:%d, hoffset:%d, voffset:%d\n",
			GTK_CLIST(clist)->clist_window_width,
			GTK_CLIST(clist)->clist_window_height,
			GTK_CLIST(clist)->hoffset,
			GTK_CLIST(clist)->voffset );
	width = GTK_CLIST(clist)->clist_window_width;
	height = GTK_CLIST(clist)->clist_window_height;
	hoffset = GTK_CLIST(clist)->hoffset;
	voffset = GTK_CLIST(clist)->voffset;
#endif
	if ( what == NULL && c == NULL ) {
#ifdef DEBUG_GABY
		debug_print("[xlist:fill] let's fill with everybody\n");
#endif
		list_fill_real_all(clist, l, search_string, search_field, *id);
	} else {
#ifdef DEBUG_GABY
		debug_print("[xlist:fill] looks like you don't want everybody\n");
#endif
		if ( what == NULL ) {
			what = get_conditional_records_list(v->subtable, c);
			foo = TRUE;
		}
		list_fill_real_some(clist, l, search_string, what, 
							search_field, *id);
	}

	if ( foo == TRUE ) {
		g_list_free(what);
	}

#ifdef DEBUG_GABY
	debug_print("[xlist_fill] clist informations (after):\n");
	debug_print(" width:%d, height:%d, hoffset:%d, voffset:%d\n",
			GTK_CLIST(clist)->clist_window_width,
			GTK_CLIST(clist)->clist_window_height,
			GTK_CLIST(clist)->hoffset,
			GTK_CLIST(clist)->voffset );
#endif
}

#define USE_CURSOR

#ifdef USE_SQL
#  ifndef USE_CURSOR
#    define USE_CURSOR
#  endif
#endif

static void list_fill_real_all (GtkWidget *cl, subtable *l, gchar *ss,
				gint sf, gint id)
{
	record *r;
	int j;
	int row;
	table *p;
	GString *strs[l->nb_fields];
	gchar *a_line[l->nb_fields+1];
	int len_ss;
#ifdef USE_CURSOR
	gabycursor *cs;
#endif
	int i=0;

#ifdef DEBUG_GABY
	debug_print("[xlist:lfra] begin, sf:%d\n", sf);
#endif
	
	gtk_clist_freeze(GTK_CLIST(cl));
	gtk_clist_clear(GTK_CLIST(cl));
	len_ss = strlen(ss);
	
	a_line[0] = g_malloc(sizeof(gchar)*8);

	p = l->table;

	/* this currently scans _every_ records then check if they corresponds
	 * to the search string (ss). This may be optimized using indexes; like
	 * that:
	 * 	for ( i=index_get_first_with(table, field_no, ss);
	 *  			i <= index_get_last_with(table, field_no, ss);
	 *  			i++) {
	 *  		r = p->records[ p->indexes[field_no][i] ];
	 *  		// ...current code...
	 *  	}
	 * the main issue is the time it will take to build the index is it
	 * doesn't already exist.
	 */
	
#ifndef USE_CURSOR
	for ( i=0; i < p->max_records; i++ ) {
		r = p->records[i];
		if ( r == NULL || r->id == 0 )
			continue;
#else
	cs = cursor_declare(p);

	r = cursor_get_first(cs);
	while ( r ) {
#endif	
		sprintf(a_line[0], "%d", r->id);

#ifdef DEBUG_GABY
/*		if ( l->table->nb_records > 5 && l->table->nb_records < 30 ) {
			debug_print("[xlist:lfra] id : %d\n", r->id);
			debug_print("[xlist:lfra] %d (i) on %d (p->max_records)\n",
					i, p->max_records);
		}*/
#endif
		
		for ( j=0; j<l->nb_fields; j++) {
			/*
			if ( r->cont[l->fields[j].no].anything == NULL ) {
				a_line[j+1] = nothing;
				continue;
			}
			*/

			strs[j] = get_subtable_stringed_field(l, r, j );
			a_line[j+1] = strs[j]->str;
			
		}

#ifdef DEBUG_GABY
		if ( l->table->nb_records > 5 && l->table->nb_records < 30 ) {
			debug_print("[xlist:lfra] a_line[sf+1] : %s, ss : %s\n",
					a_line[sf+1], ss );
		}
#endif

		if ( strncmp_fct(a_line[sf+1], ss, len_ss) == 0 ) {
#ifdef DEBUG_GABY
			if ( l->table->nb_records < 30 ) {
				debug_print("[xlist:lfra] adding %s, %s\n",
							a_line[1], a_line[2] );
			}
#endif
			row = gtk_clist_append(GTK_CLIST(cl), a_line);
			gtk_clist_set_row_data(GTK_CLIST(cl), row, 
						GINT_TO_POINTER(i));
			
			if ( r->id == id ) {
				/* when using gnomecard the focus stays on the
				 * first row and that doesn't looks good
				 * let's play with gtk+ and hope he won't
				 * change its structure */
				GTK_CLIST(cl)->focus_row = row;
				gtk_clist_select_row(GTK_CLIST(cl), row, 0);
				gtk_clist_moveto(GTK_CLIST(cl), row, -1,
						0.5, 0.5 );
			}
		}
		
		for ( j=0; j<l->nb_fields; j++) {
			g_string_free(strs[j], 1);
		}
#ifdef USE_CURSOR
		r = cursor_get_next(cs);
		i++;
#endif
	}

	debug_print("[xlist:lfra] a_line[0] : %p, a_line : %p\n", a_line[0], a_line);

	cursor_free(cs);
	g_free(a_line[0]);

	gtk_clist_thaw(GTK_CLIST(cl));

	debug_print("[xlist:lfra] finished\n");
}

static void list_fill_real_some (GtkWidget *cl, subtable *l, gchar *ss,
				 GList *what, gint sf, gint id)
{
	record *r;
	gchar *a_line[l->nb_fields+1];
	int j;
	table *p;
	GString *strs[l->nb_fields];
	int row;
	int numrec;
	
	gtk_clist_freeze(GTK_CLIST(cl));
	gtk_clist_clear(GTK_CLIST(cl));
	
	p = l->table;
	
	a_line[0] = g_malloc(sizeof(gchar)*8);
	
	what = g_list_first(what);
	
	while ( what != NULL ) {
		numrec = GPOINTER_TO_INT(what->data);
		if ( numrec == -1 )
			break;
		
		r = p->records[numrec];
		what = g_list_next(what);
		
		sprintf(a_line[0], "%d", r->id);
		
		for ( j=0; j<l->nb_fields; j++) {
			strs[j] = get_subtable_stringed_field(l, r, j );
			a_line[j+1] = strs[j]->str;
		}
		
		if ( strncmp_fct(a_line[sf+1], ss, strlen(ss)) == 0 ) {
			
			row = gtk_clist_append(GTK_CLIST(cl), a_line);
			gtk_clist_set_row_data(GTK_CLIST(cl), row, 
						GINT_TO_POINTER(numrec));
			
			if ( r->id == id ) {
				gtk_clist_select_row(GTK_CLIST(cl), row, 0);
			}
			
			for ( j=0; j<l->nb_fields; j++) {
				g_string_free(strs[j], 1);
			}
		}
	}

	g_free(a_line[0]);

	gtk_clist_thaw(GTK_CLIST(cl));
}

static GtkWidget* create_field_list_menu ( gabywindow *win )
{
	GtkWidget *menu;
	GtkWidget *menu_item;
	subtable *subt = win->view->subtable;
	GSList *group = NULL;
	int i;
	
	menu = gtk_menu_new();
	
	for ( i=0; i< subt->nb_fields;i++ ) {
		menu_item = gtk_radio_menu_item_new_with_label(group, subt->fields[i].i18n_name);
		group = gtk_radio_menu_item_group(
				GTK_RADIO_MENU_ITEM(menu_item));
		gtk_object_set_data(GTK_OBJECT(menu_item), "which", 
				GINT_TO_POINTER(i));
		gtk_signal_connect(GTK_OBJECT(menu_item), "activate",
				GTK_SIGNAL_FUNC(search_field_selected), win);
		gtk_widget_show(menu_item);
		gtk_menu_append(GTK_MENU(menu), menu_item);
	}
	
	return menu;
}

static void select_record( GtkCList *clist, gint row, gint column,
			   GdkEventButton *event, gabywindow *window )
{
	/*
	 * we could also put actions in menu, sth like :
	 *  Actions - Mail
	 *            Phone
	 *  Open in - Form 1
	 *            ...
	 * 
	 * and sth like 'set default to' ->
	 *   right click : the menu
	 *   left click  : the default 'action'
	 *
	 * and it should be moved in src/
	 */
	
#if 0 /* windows binding is the elegant way to achieve this goal :) 
       * this menu should now be used for actions, not for selecting records in
       * remote forms.
       */
	
	int *id = gtk_object_get_data(GTK_OBJECT(win), " id ");
	gchar *name;
	GtkWidget *a_win;
	GtkWidget *t;
	GtkWidget *menu;
	gchar *a_line;
	view *otherv, *ourv;
	int i=0;
	GList *aw = g_list_first(all_wins);

	gtk_clist_get_text(clist, row, 0, &a_line);
	*id = atoi(a_line);

	menu = gtk_menu_new();
	
#ifdef DEBUG_GABY
	debug_print("Button : %d, new id : %d", event->button, *id);
	gtk_clist_get_text(clist, row, 1, &a_line);
	debug_print(" %s\n", a_line);
#endif

	ourv = gtk_object_get_data(GTK_OBJECT(win), " view ");
	
	while ( aw != NULL ) {
		a_win = aw->data;
		name = gtk_object_get_data(GTK_OBJECT(a_win), " name ");
		otherv = gtk_object_get_data(GTK_OBJECT(a_win), " view ");
		if ( otherv->subtable->table == ourv->subtable->table && 
				a_win != win ) {
			t = gtk_menu_item_new_with_label(name);
			gtk_menu_append(GTK_MENU(menu), t);
			gtk_object_set_data(GTK_OBJECT(t), " id ", id);
			gtk_signal_connect(GTK_OBJECT(t), "activate",
						change_record, a_win);
			gtk_widget_show(t);
			i++;
		}
		aw = g_list_next(aw);
	}

	if ( i != 0 ) {
		gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, 0, 0);
	}
#else /* ! 0 */
	GtkWidget *win = window->widget;
	GtkWidget *menu = gtk_object_get_data(GTK_OBJECT(win), "actmenu");
	int *id = &(window->id);
	gchar *a_line;
	
#ifdef DEBUG_GABY
	debug_print("Button : %d\n", ( event == NULL ) ? -1 : event->button);
#endif
	if ( menu != NULL && event != NULL && event->button == 3 ) {
		gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, 0, 0);
	} else {
		gtk_clist_get_text(clist, row, 0, &a_line);
		*id = atoi(a_line);
		update_bound_windows(window);
	}
#endif /* ! 0 */
}

static void unselect_record( GtkCList *clist, gint row, gint column,
		    GdkEventButton *event, int *id )
{
	*id = 0;
}

static void clist_mouse_event (GtkWidget *clist, GdkEventButton *event,
			       gabywindow *window )
{
	int row, col;
	/* we only want to catch the 3rd button */
	if ( event == NULL || event->button != 3 )
		return;
	if ( gtk_clist_get_selection_info(GTK_CLIST(clist),
			(gint)(event->x), (gint)(event->y), &row, &col) == 0 )
		return;
#ifdef DEBUG_GABY
	debug_print("[clist_mouse_event] row : %d, col : %d\n", row, col);
#endif
	gtk_clist_select_row(GTK_CLIST(clist), row, col);
	select_record(GTK_CLIST(clist), row, col, event, window);
}

#if 0
static void change_record(GtkWidget *t, GtkWidget *win)
{
	int *id_orig = gtk_object_get_data(GTK_OBJECT(t), " id ");
	int *id_dest = gtk_object_get_data(GTK_OBJECT(win), " id ");
	ViewPluginData *vpd = gtk_object_get_data(GTK_OBJECT(win), " vpd ");
	if ( id_dest == NULL )
		return;
	
	*id_dest = *id_orig;
	
	vpd->view_fill(win);
	
}
#endif

static void search_event(GtkWidget *entry, gabywindow *w)
{
	/*
	GtkWidget *optmenu = gtk_object_get_data(GTK_OBJECT(w), "search_fieldlist");
	gchar *st = gtk_entry_get_text(GTK_ENTRY(entry));
	*/

#ifdef DEBUG_GABY
	debug_print("[xlist:search_event] I have to fill again the list ...\n");
#endif
	xlist_fill(w);
	
}

static void search_field_selected ( GtkWidget *mi, gabywindow *window)
{
	int which = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(mi), "which"));
	GtkWidget *t;
	GtkWidget *wid = window->widget;
	GtkWidget *fieldlist = gtk_object_get_data(GTK_OBJECT(wid), "search_fieldlist");
	gchar *search_string;
	
	gtk_object_set_data(GTK_OBJECT(fieldlist), "which", GINT_TO_POINTER(which));
	
	t = gtk_object_get_data(GTK_OBJECT(wid), "search_entry");
	search_string = gtk_entry_get_text(GTK_ENTRY(t));
	
#ifdef DEBUG_GABY
	debug_print("Now searching in column %d\n", which);
#endif
	if ( strlen(search_string) != 0 )
		xlist_fill(window);
}

mstatic void xlist_get_records(gabywindow *window, GList **records)
{
	GtkCList *clist;
	int i;
	
	clist = GTK_CLIST(gtk_object_get_data(GTK_OBJECT(window->widget),
				"clist"));
	*records = NULL;
	for ( i=0; i<clist->rows; i++ ) {
		*records = g_list_append(*records,
					 gtk_clist_get_row_data(clist,i));
	}
	/* TODO (post-2.0): xlist_get_records working with gabysql
	 * currently it doesn't because it uses 'fast ids' which are
	 * a total non-sense in an sql world...
	 */
}

/*
 * Below are configurations functions; should they be moved in a
 * different source file ?
 */

static GtkWidget* configure_widget = NULL;
static void configure_apply();
static void configure_save();

mstatic GtkWidget* xlist_configure (ViewPluginData *vpd)
{
	GtkWidget *vbox;
	GtkWidget *case_check;
	
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox);

	case_check = gtk_check_button_new_with_label(
					_("Case sensitive search"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(case_check),
				( (void*)strncmp_fct == (void*)strncmp) );
	gtk_signal_connect_object(GTK_OBJECT(case_check), "clicked",
		GTK_SIGNAL_FUNC(gaby_property_box_changed), GTK_OBJECT(vbox) );
	
	gtk_widget_show (case_check);
	gtk_object_set_data(GTK_OBJECT(vbox), "case_check", case_check);
	gtk_box_pack_start (GTK_BOX (vbox), case_check, FALSE, FALSE, 0);

	gtk_object_set_data(GTK_OBJECT(vbox), "name", vpd->i18n_name);
	gtk_object_set_data(GTK_OBJECT(vbox), "cfg_save", configure_save);
	gtk_object_set_data(GTK_OBJECT(vbox), "cfg_apply", configure_apply);

	configure_widget = vbox;
	return vbox;
}

static void configure_apply()
{
	GtkWidget *case_check = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "case_check" );
	
	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(case_check)) ) {
		strncmp_fct = (void*)strncmp;
	} else {
		strncmp_fct = (void*)g_strncasecmp;
	}
	
}

static void configure_save()
{
	GtkWidget *case_check = gtk_object_get_data(
			GTK_OBJECT(configure_widget), "case_check" );
#ifdef USE_GNOME
	configure_apply();
#endif
	write_config_bool("view", "xlist", "case_sensitive_search",
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(case_check)) );

}

mstatic void xlist_get_config()
{
	gboolean val;
	
	val = get_config_bool("view", "xlist", "case_sensitive_search", FALSE);

	strncmp_fct = ( val == TRUE ) ? strncmp : g_strncasecmp;
	
}

