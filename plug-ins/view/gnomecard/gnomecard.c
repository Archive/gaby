/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <tables.h>
#include <records.h>
#include <f_config.h>
#include <gtk_main.h>
#include <gtk_menu.h>
#include <gtk_dialogs.h>
#include <windows.h>

#ifdef USE_GNOME
#include "pixmaps/cardnew.xpm"
#include "pixmaps/cardedit.xpm"
#endif
/*#include <w_manage.h>*/

mstatic void gnomecard_create ( gabywindow *window, gboolean first );
mstatic void gnomecard_fill ( gabywindow *window );
static gint gnomecard_form_delete(GtkWidget *wid, GdkEvent *event, gpointer data);

#ifndef FOLLOW_MIGUEL
int init_view_plugin (ViewPluginData *vpd)
{
	vpd->view_create = gnomecard_create;
	vpd->view_fill = gnomecard_fill;
	vpd->configure = NULL;
	vpd->view_records = NULL;

	vpd->name = "gnomecard";
	vpd->i18n_name = _("GnomeCard");
	vpd->type = ALL_RECORDS;
	vpd->capabilities = NONE;

	return 0;
}
#endif

/* This should be a list instead */
static gchar *plugin_author = "Berend De Schouwer <bds@ucs.co.za>";

#if USE_GNOME_1
static GnomeStockPixmapEntry *gnomecard_pentry_new(gchar **xpm_data, gint size);
static void gnomecard_init_stock(void);
#endif
static void gnomecard_save(GtkWidget *w, gabywindow *window);
static void gnomecard_append(GtkWidget *w, gabywindow *window);
static void gnomecard_commit(GtkWidget *w, gabywindow *window);
static void gnomecard_cancel(GtkWidget *w, gabywindow *window);
static void gnomecard_first_card(GtkWidget *w, gabywindow *window);
static void gnomecard_prev_card(GtkWidget *w, gabywindow *window);
static void gnomecard_next_card(GtkWidget *w, gabywindow *window);
static void gnomecard_last_card(GtkWidget *w, gabywindow *window);
static void gnomecard_new_card(GtkWidget *w, gabywindow *window);
static void gnomecard_delete_current_card(GtkWidget *w, gabywindow *window);
static void gnomecard_edit_card(GtkWidget *w, gabywindow *wform);
static void preferences_clicked(GtkWidget *w, gpointer data);
static gboolean gnomecard_delete_view (GtkWidget *win, gpointer data);

static GnomeUIInfo file_menu[] = {
/* 0 */	GNOMEUIINFO_MENU_SAVE_ITEM(gnomecard_save, NULL),
/* 1 */	{GNOME_APP_UI_ITEM, N_("_Append..."),
		N_("Add the contents of another card file"),
		gnomecard_append, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
		GNOME_STOCK_MENU_OPEN, 0, 0, NULL},
/* 2 */	GNOMEUIINFO_SEPARATOR,
/* 3 */	GNOMEUIINFO_MENU_EXIT_ITEM(gaby_quit_callback, NULL),
/* 4 */	GNOMEUIINFO_END
};

static GnomeUIInfo go_menu[] = {
	{GNOME_APP_UI_ITEM, N_("First"), N_("First card"),
		gnomecard_first_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_FIRST, 0, 0, NULL},

	{GNOME_APP_UI_ITEM, N_("Prev"), N_("Previous card"),
		gnomecard_prev_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_BACK, 0, 0, NULL},

	{GNOME_APP_UI_ITEM, N_("Next"), N_("Next card"),
		gnomecard_next_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_FORWARD, 0, 0, NULL},

	{GNOME_APP_UI_ITEM, N_("Last"), N_("Last card"),
		gnomecard_last_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_LAST, 0, 0, NULL},

	GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu[] = {
	{GNOME_APP_UI_ITEM, N_("Add"), N_("Create new card"),
		gnomecard_new_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, "GnomeCardNewMenu", 0, 0, NULL},

	{GNOME_APP_UI_ITEM, N_("Modify"), N_("Edit card"),
		gnomecard_edit_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, "GnomeCardEditMenu", 0, 0, NULL},

	{GNOME_APP_UI_ITEM, N_("Delete"), N_("Erase card"),
		gnomecard_delete_current_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_CLOSE, 0, 0, NULL},
	
	GNOMEUIINFO_SEPARATOR,
	
	{GNOME_APP_UI_SUBTREE, N_("Go"), N_("Change current card"),
		go_menu, NULL, NULL, GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},
	
	GNOMEUIINFO_END
};

static GnomeUIInfo settings_menu[] = {
	GNOMEUIINFO_MENU_PREFERENCES_ITEM(preferences_clicked, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
	GNOMEUIINFO_HELP("gaby"),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_ABOUT_ITEM(about_gaby_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo gnomecard_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE(file_menu),
	GNOMEUIINFO_MENU_EDIT_TREE(edit_menu),
	GNOMEUIINFO_MENU_SETTINGS_TREE(settings_menu),
	GNOMEUIINFO_MENU_HELP_TREE(help_menu),
	GNOMEUIINFO_END
};

static GnomeUIInfo gnomecard_toolbar[] = {
/* 0 */	{GNOME_APP_UI_ITEM, N_("Save"), N_("Save changes"),
		gnomecard_save, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_SAVE, 0, 0, NULL},

/* 1 */	{GNOME_APP_UI_SEPARATOR},
	
/* 2 */	{GNOME_APP_UI_ITEM, N_("Add"), N_("Create new card"),
		gnomecard_new_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, "GnomeCardNew", 0, 0, NULL},
/* 3 */	{GNOME_APP_UI_ITEM, N_("Modify"), N_("Edit card"),
		gnomecard_edit_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, "GnomeCardEdit", 0, 0, NULL},
/* 4 */	{GNOME_APP_UI_ITEM, N_("Delete"), N_("Delete card"),
		gnomecard_delete_current_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_CLOSE, 0, 0, NULL},
	
/* 5 */	{GNOME_APP_UI_SEPARATOR},
	
/* 6 */	{GNOME_APP_UI_ITEM, N_("First"), N_("First card"),
		gnomecard_first_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_FIRST, 0, 0, NULL},
/* 7 */	{GNOME_APP_UI_ITEM, N_("Prev"), N_("Previous card"),
		gnomecard_prev_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_BACK, 0, 0, NULL},
/* 8 */	{GNOME_APP_UI_ITEM, N_("Next"), N_("Next card"),
		gnomecard_next_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_FORWARD, 0, 0, NULL},
/* 9 */	{GNOME_APP_UI_ITEM, N_("Last"), N_("Last card"),
		gnomecard_last_card, NULL, NULL,
		GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_LAST, 0, 0, NULL},
	
/*10 */	{GNOME_APP_UI_SEPARATOR},

/*11 */	{ GNOME_APP_UI_ITEM,
		N_("Exit"), N_("Exit Gaby"),
		gaby_quit_callback, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
		GNOME_STOCK_PIXMAP_EXIT, 0, 0, NULL },
	
/*12 */	{GNOME_APP_UI_ENDOFINFO}
};

mstatic void gnomecard_create ( gabywindow *window, gboolean first )
{
	GtkWidget *table, *pane;
	gabywindow *wlist, *wcanvas, *wform;
	GtkWidget *w;
	view *vtmp, *list = NULL, *canvas = NULL, *form = NULL;
	GList *tmp;
	GList *bound_windows = NULL;
	int *id;
	struct window_info wi = { NULL, -1, -1, -1, -1, FALSE };
	GtkWidget *bbox, *button, *hsep;
	
#ifdef DEBUG_GABY
	debug_print("[gnomecard_create] --\n");
#endif

	/* First load some data structures, then create the gtk boxes.
	 * We need the seperate views before we create the toolbar or
	 * menus to load the menus with the pointers.
	 *
	 * But form->view_create creates a toolbar and menus!  So we
	 * must be first - so pack form in an hbox.
	 */

	/*
	win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	*/
	
#if 0
#if USE_GNOME
        win = gnome_app_new("gaby", "edit");
#else /* ! USE_GNOME */
        win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        gtk_window_set_title(GTK_WINDOW (win), "edit");
        gtk_window_set_policy (GTK_WINDOW (win), TRUE, TRUE, FALSE);
        gtk_container_border_width(GTK_CONTAINER(win), 5);
#endif /* USE_GNOME */
#endif
	
	pane = gtk_hpaned_new();
	window->widget = pane;
	
#ifdef DEBUG_GABY
	debug_print("[gnomecard_create] menus and toolbar created\n");
#endif

	/* we know we face desc.gnomecard and we use that fact */
	
	tmp = g_list_first(list_views);
	while ( tmp != NULL ) {
		vtmp = tmp->data;
		tmp = g_list_next(tmp);
#ifdef DEBUG_GABY
		debug_print("[gnomecard_create] name : %s\n",
						vtmp->type->name );
#endif
		if ( strcmp(vtmp->type->name, "xlist") == 0 ) {
			list = vtmp;
			continue;
		}
		if ( strcmp(vtmp->type->name, "canvas") == 0 ) {
			canvas = vtmp;
			continue;
		}
		if ( strcmp(vtmp->type->name, "form") == 0 ) {
			form = vtmp;
			continue;
		}
	}

	if ( list == NULL || canvas == NULL || form == NULL ) {
		/* that doesn't look like desc.gnomecard - aie aie */
#ifdef DEBUG_GABY
		debug_print("[gnomecard_create] wrong plug-in\n");
#endif
		return;
	}
	
#if USE_GNOME_1
	/* Load additional pixmaps into gnome-stock. */
	gnomecard_init_stock();
#endif
	
	/* first, prev, next, last */
	gnomecard_toolbar[6].user_data = window;
	go_menu[0].user_data = window;
	gnomecard_toolbar[7].user_data = window;
	go_menu[1].user_data = window;
	gnomecard_toolbar[8].user_data = window;
	go_menu[2].user_data = window;
	gnomecard_toolbar[9].user_data = window;
	go_menu[3].user_data = window;
	
	/* save */
	gnomecard_toolbar[0].user_data = window;
	file_menu[0].user_data = window;
	
	/* remove */
	gnomecard_toolbar[4].user_data = window;
	edit_menu[2].user_data = window;
	
	/* exit */
	file_menu[3].user_data = list_tables;
	gnomecard_toolbar[11].user_data = list_tables;
	
	wi.view = form;
	wform = new_view_create(&wi);
	/* edit and create */
	gnomecard_toolbar[2].user_data = window;
	edit_menu[0].user_data = window;
	gnomecard_toolbar[3].user_data = wform;
	edit_menu[1].user_data = wform;

	help_menu[2].user_data = plugin_author;

	/* we don't want our form window to be destroyed by an irresponsable
	 * user so we disconnect the default handler and put our that hides the
	 * window instead of destroying it. */
	gtk_signal_disconnect_by_func(GTK_OBJECT(wform->parent),
					GTK_SIGNAL_FUNC(delete_view), NULL );
	gtk_signal_connect(GTK_OBJECT(wform->parent), "delete_event",
			GTK_SIGNAL_FUNC(gnomecard_form_delete), NULL);
	
	/* We make it more userfriendly by adding buttons wform doesn't need. */
	hsep = gtk_hseparator_new();
	gtk_box_pack_start(GTK_BOX(wform->widget), hsep, FALSE, FALSE, 0);
	gtk_widget_show(hsep);
	bbox = gtk_hbutton_box_new();
	gtk_box_pack_start(GTK_BOX(wform->widget), bbox, FALSE, FALSE, 5);
	gtk_hbutton_box_set_layout_default(GTK_BUTTONBOX_SPREAD);
	gtk_widget_show(bbox);
	
#ifdef USE_GNOME_1
	button = gnome_stock_button(GNOME_STOCK_BUTTON_APPLY);
#else
	button = gtk_button_new_with_label(_("Apply"));
#endif
	gtk_box_pack_start(GTK_BOX(bbox), button, TRUE, TRUE, 0);
	gtk_widget_show(button);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
		GTK_SIGNAL_FUNC(gnomecard_commit), wform);
/*	GTK_WIDGET_SET_FLAGS(button, GTK_CAN_DEFAULT);
	gtk_widget_grab_default(button);*/

#ifdef USE_GNOME_1
	button = gnome_stock_button(GNOME_STOCK_BUTTON_CANCEL);
#else
	button = gtk_button_new_with_label(_("Cancel"));
#endif
	gtk_box_pack_start(GTK_BOX(bbox), button, TRUE, TRUE, 0);
	gtk_widget_show(button);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
		GTK_SIGNAL_FUNC(gnomecard_cancel), wform);
	
	gnome_app_create_menus(GNOME_APP(window->parent), gnomecard_menu);
	gnome_app_create_toolbar(GNOME_APP(window->parent), gnomecard_toolbar);

#ifdef DEBUG_GABY
	debug_print("[gnomecard_create] creating list\n");
#endif
	wlist = g_new0(gabywindow, 1);
	wlist->view = list;
	wlist->parent = pane;
	list->type->view_create(wlist, FALSE);
#ifdef DEBUG_GABY
	debug_print("[gnomecard_create] creating canvas\n");
#endif
	wcanvas = g_new0(gabywindow, 1);
	wcanvas->view = canvas;
	wcanvas->parent = pane;
	canvas->type->view_create( wcanvas, FALSE );
#ifdef DEBUG_GABY
	debug_print("[gnomecard_create] creating form\n");
#endif
	
	list->type->view_fill(wlist);
	canvas->type->view_fill(wcanvas);

#ifdef DEBUG_GABY
	debug_print("[gnomecard_create] wlist : %p, wcanvas : %p, wform: %p\n",\
				wlist, wcanvas, wform );
#endif

	/* this doesn't work (the widget isn't shown) (the only way i
	 * found to show the canvas is with gtk_container_add(hbox,wc) )
	 * I didn't manage to show both the canvas and another view in the same
	 * box (this works with every view _except_ the canvas).
	 * (Fix should probably be in ../addresscanvas/addresscanvas.c)
	 *
	 * Update: I had to copy from gnomecard, the canvas is only shown in
	 * the table _if_ there are 2 scrollbars. Strange.
	 * 
	 */
#if 0
	gtk_box_pack_start(GTK_BOX(hbox), wcanvas, FALSE, FALSE, 0 );
#endif

	gtk_paned_add1(GTK_PANED(pane), wlist->widget);
	
	table = gtk_table_new(2, 2, FALSE);
	gtk_table_set_row_spacings(GTK_TABLE(table), 4);
	gtk_table_set_col_spacings(GTK_TABLE(table), 4);
	gtk_paned_add2(GTK_PANED(pane), table);

	gtk_table_attach(GTK_TABLE(table), wcanvas->widget, 0, 1, 0, 1,
			GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			0, 0);
	w = gtk_hscrollbar_new (GTK_LAYOUT(wcanvas->widget)->hadjustment);
	gtk_widget_set_usize(w, 200, -1);
	gtk_table_attach (GTK_TABLE (table), w,
			0, 1, 1, 2,
			GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			GTK_FILL,
			0, 0);
	gtk_widget_show (w);
	w = gtk_vscrollbar_new (GTK_LAYOUT (wcanvas->widget)->vadjustment);
	gtk_table_attach (GTK_TABLE (table), w,
			1, 2, 0, 1,
			GTK_FILL,
			GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			0, 0);
	gtk_widget_show (w);
	gtk_widget_show(wcanvas->widget);
	gtk_widget_show(table);
	
	gtk_widget_show(wlist->widget);
		
	bound_windows = NULL;
	bound_windows = g_list_append(bound_windows, window);
	wlist->bound_windows = bound_windows;
	bound_windows = NULL;
	bound_windows = g_list_append(bound_windows, wlist);
	wcanvas->bound_windows = bound_windows;
	
	id = &(window->id);
	*id = 0;
	
	gtk_object_set_data(GTK_OBJECT(pane), "list", wlist);
	gtk_object_set_data(GTK_OBJECT(pane), "canvas", wcanvas);

	gtk_widget_show(pane);

	/* Now do the form. */
#if 0
	gtk_container_add(GTK_CONTAINER(win), wform);
#endif

	bound_windows = NULL;
	bound_windows = g_list_append(bound_windows, window);
	wform->bound_windows = bound_windows;

	bound_windows = NULL;
	bound_windows = g_list_append(bound_windows, wform);
	window->bound_windows = bound_windows;

	/* This gets set in the form plug-in - don't overwrite */
	/*
	gtk_object_set_data(GTK_OBJECT(wform), " view ", v);
	*/

#if 0
	vpd = gtk_object_get_data(GTK_OBJECT(wform), " vpd ");
	gtk_object_set_data(GTK_OBJECT(win), " vpd ", vpd);
	v = gtk_object_get_data(GTK_OBJECT(wform), " view ");
	gtk_object_set_data(GTK_OBJECT(win), " view ", v);
	id = gtk_object_get_data(GTK_OBJECT(wform), " id ");
	gtk_object_set_data(GTK_OBJECT(win), " id ", vpd);
#endif

	/* Need this to call the form's view_save function */
	gtk_object_set_data(GTK_OBJECT(pane), "form-window", wform);

	return;
}

#ifdef USE_GNOME_1
static GnomeStockPixmapEntry *gnomecard_pentry_new(gchar **xpm_data, gint size)
{
        GnomeStockPixmapEntry *pentry;
        
        pentry = g_malloc(sizeof(GnomeStockPixmapEntry));
        pentry->data.type = GNOME_STOCK_PIXMAP_TYPE_DATA;
        pentry->data.width = size;
        pentry->data.height = size;
        pentry->data.label = NULL;
        pentry->data.xpm_data = xpm_data;
        
        return pentry;
}

static void gnomecard_init_stock(void)
{
        GnomeStockPixmapEntry *pentry;
        gchar **xpms[] = { cardnew_xpm, cardedit_xpm, NULL };
        gchar *names[] = { "New", "Edit" };
        gchar stockname[22];
        int i;
        
        for (i = 0; xpms[i]; i++) {
                snprintf(stockname, 22, "GnomeCard%s", names[i]);
                pentry = gnomecard_pentry_new(xpms[i], 24);
                gnome_stock_pixmap_register(stockname, GNOME_STOCK_PIXMAP_REGULAR, pentry);
                snprintf(stockname, 22, "GnomeCard%sMenu", names[i]);
                pentry = gnomecard_pentry_new(xpms[i], 16);
                gnome_stock_pixmap_register(stockname, GNOME_STOCK_PIXMAP_REGULAR, pentry);
        }
}
#endif

static gint gnomecard_form_delete(GtkWidget *wid, GdkEvent *event, gpointer data)
{
#ifdef DEBUG_GABY
	debug_print("[gnomecard_form_delete] hiding form window\n");
#endif
	gtk_widget_hide(wid);
	return TRUE;
}

static void gnomecard_commit(GtkWidget *w, gabywindow *wform)
{
	/* save the current data. */
	wform->view->type->view_save(wform);
	/* update. */
	wform->view->type->view_fill(wform);
	/* hide. */
	gtk_widget_hide(wform->parent);
}

static void gnomecard_cancel(GtkWidget *w, gabywindow *window)
{
	int *id = &(window->id);
	view *v = window->view;
	table *t = v->subtable->table;
	record *r;
	int *sort_by;
	
	if ( *id == 0 ) {
		/* Assume new card - remove. */
		sort_by = gtk_object_get_data(GTK_OBJECT(window->widget),
			"sort_by");
		r = table_last(t, *sort_by);
		if ( r != NULL ) *id = r->id;
		window->view->type->view_fill(window);
	} else {
		/* Assume old card - revert changes! */
		window->view->type->view_fill(window);
	}

	gtk_widget_hide(window->parent);
}


mstatic void gnomecard_fill ( gabywindow *window )
{
	gabywindow *wcanvas = gtk_object_get_data(
				GTK_OBJECT(window->widget), "canvas" );
	gabywindow *wlist = gtk_object_get_data(
				GTK_OBJECT(window->widget), "list" );
#ifdef DEBUG_GABY
	debug_print("[gnomecard_fill] filling gnomecard\n");
#endif

	wcanvas->id = window->id;
	wlist->id = window->id;
	wcanvas->view->type->view_fill(wcanvas);
	wlist->view->type->view_fill(wlist);
	
	update_bound_windows(window);
}

static void gnomecard_save(GtkWidget *w, gabywindow *window)
{
	view *v = window->view;

#ifdef DEBUG_GABY
	debug_print("[gnomecard_save] saving table\n");
#endif
	table_save_file(v->subtable->table);
}

static void gnomecard_append(GtkWidget *w, gabywindow *window)
{
	view *v = window->view;
	
	import_export_dialog(v->subtable, 0);
}

static void gnomecard_first_card(GtkWidget *w, gabywindow *window)
{
	int *id = &(window->id);
	view *v = window->view;
	record *r;
	GtkWidget *win = window->widget;
	gabywindow *wform = gtk_object_get_data(GTK_OBJECT(win), "form-window");
	
	/* Before we move, save the current data. */
	wform->view->type->view_save(wform);

	r = table_first(v->subtable->table, -1);
	*id = ( r == NULL ) ? 0 : r->id;

	gnomecard_fill(window);
}

static void gnomecard_prev_card(GtkWidget *w, gabywindow *window)
{
	int *id = &(window->id);
	view *v = window->view;
	record *r;
	GtkWidget *win = window->widget;
	gabywindow *wform = gtk_object_get_data(GTK_OBJECT(win), "form-window");
	
	/* Before we move, save the current data. */
	wform->view->type->view_save(wform);

	r = get_record_no(v->subtable->table, *id);
	r = table_prev(v->subtable->table, r, -1);
	*id = ( r == NULL ) ? 0 : r->id;

	gnomecard_fill(window);
}

static void gnomecard_next_card(GtkWidget *w, gabywindow *window)
{
   	int *id = &(window->id);
	view *v = window->view;
	record *r;
	GtkWidget *win = window->widget;
	gabywindow *wform = gtk_object_get_data(GTK_OBJECT(win), "form-window");
	
	/* Before we move, save the current data. */
	wform->view->type->view_save(wform);
	
	r = get_record_no(v->subtable->table, *id);
	r = table_next(v->subtable->table, r, -1);
	*id = ( r == NULL ) ? 0 : r->id;

	gnomecard_fill(window);
}

static void gnomecard_last_card(GtkWidget *w, gabywindow *window)
{
	int *id = &(window->id);
	view *v = window->view;
	record *r;
	GtkWidget *win = window->widget;
	gabywindow *wform = gtk_object_get_data(GTK_OBJECT(win), "form-window");
	
	/* Before we move, save the current data. */
	wform->view->type->view_save(wform);

	r = table_last(v->subtable->table, -1);
	*id = ( r == NULL ) ? 0 : r->id;

	gnomecard_fill(window);
}

static void gnomecard_new_card(GtkWidget *w, gabywindow *window)
{
	GtkWidget *win = window->widget;
	gabywindow *wform = gtk_object_get_data(GTK_OBJECT(win), "form-window");
 	int *id = &(wform->id);

	/*
	 * We are going to get the form plug-in to do all the hard work.
	 * That's okay, 'coz its the only one that allows the user to
	 * do modifications anyway.
	 */
	
	/*
	 * Save blindly...
	 */
	wform->view->type->view_save(wform);

#ifdef DEBUG_GABY
	debug_print("[gnomecard_new_card] getting new record\n");
#endif
	*id = 0;

#ifdef DEBUG_GABY
	debug_print("[gnomecard_new_card] filling info\n");
#endif
	wform->view->type->view_fill(wform);

	/*
	 * Since we created new, the obvious thing to do is fill data,
	 * so make it easy for the user.
	 */
	gtk_widget_show(wform->parent);
}

static void gnomecard_edit_card(GtkWidget *w, gabywindow *wform)
{
	gtk_widget_show(wform->parent);
}

static void gnomecard_delete_current_card(GtkWidget *w, gabywindow *window)
{
	int *id = &(window->id);
	view *v = window->view;
	table *t;
	record *r;

	t = v->subtable->table;

	record_remove_id(t, *id);
	r = table_first(t, -1);
	*id = ( r != NULL ) ? r->id : 0;

	gnomecard_fill(window);
}

static void preferences_clicked(GtkWidget *w, gpointer data)
{
	GtkWidget *a;
	
	a = configure_dialog (NULL);
	if ( a != NULL ) gtk_widget_show(a);
}

static gboolean gnomecard_delete_view (GtkWidget *win, gpointer data)
{
#ifdef DEBUG_GABY
	debug_print("[gnomecard_delete_view] -- \n");
#endif
	return TRUE;
}

