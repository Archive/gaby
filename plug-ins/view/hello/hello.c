/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <tables.h>
#include <records.h>
#include <windows.h>

mstatic void hello_create ( gabywindow *window, gboolean first );
mstatic void hello_fill ( gabywindow *window );

#ifndef FOLLOW_MIGUEL
int init_view_plugin (ViewPluginData *vpd)
{
	vpd->view_create = hello_create;
	vpd->view_fill = hello_fill;
	vpd->configure = NULL;
	vpd->view_records = NULL;

	vpd->name = "hello";
	vpd->i18n_name = _("Hello");
	vpd->type = ONE_RECORD;
	vpd->capabilities = NONE;

	return 0;
}
#endif

static void previous_clicked(GtkWidget *button, gabywindow *window);
static void next_clicked(GtkWidget *button, gabywindow *window);

mstatic void hello_create ( gabywindow *window, gboolean first )
{
	GtkWidget *vbox;
	GtkWidget *label;
	GtkWidget *hbb;
	GtkWidget *button;
	int *id = &(window->id);
	record *r;
	
	r = table_first(window->view->subtable->table, -1);
	*id = ( r == NULL ? 0 : r->id );
	
	vbox = gtk_vbox_new(FALSE,0);
	window->widget = vbox;
	
	label = gtk_label_new("");
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, TRUE, 0);
	
	hbb = gtk_hbutton_box_new();
	gtk_widget_show(hbb);
	gtk_box_pack_start(GTK_BOX(vbox), hbb, FALSE, TRUE, 0);

	button = gtk_button_new_with_label(_("Previous"));
	gtk_widget_show(button);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
				GTK_SIGNAL_FUNC(previous_clicked), window);
	gtk_container_add(GTK_CONTAINER(hbb), button);

	button = gtk_button_new_with_label(_("Next"));
	gtk_widget_show(button);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
				GTK_SIGNAL_FUNC(next_clicked), window);
	gtk_container_add(GTK_CONTAINER(hbb), button);
	
	gtk_widget_show(vbox);
	
	return;
}

static void previous_clicked(GtkWidget *button, gabywindow *window)
{
	record *r;
	view *v = window->view;
	int *id = &(window->id);

	r = get_record_no(v->subtable->table, *id);
	r = table_prev(v->subtable->table, r, -1);
	*id = ( r == NULL ) ? 0 : r->id;
	hello_fill(window);

	update_bound_windows(window);
}

static void next_clicked(GtkWidget *button, gabywindow *window)
{
	record *r;
	view *v = window->view;
	int *id = &(window->id);

	r = get_record_no(v->subtable->table, *id);
	r = table_next(v->subtable->table, r, -1);
	*id = ( r == NULL ) ? 0 : r->id;
	hello_fill(window);

	update_bound_windows(window);
}

mstatic void hello_fill ( gabywindow *window )
{
	view *v = window->view;
	int *id = &(window->id);
	GtkWidget *win = window->widget;
	GtkWidget *label = gtk_object_get_data(GTK_OBJECT(win), "label");
	GString *str;
	
	if ( *id == 0 ) {
		gtk_label_set_text(GTK_LABEL(label), _("Hello, world !"));
		return;
	}
	
	str = get_subtable_stringed_field_id(v->subtable, *id, 0 );
	
	str = g_string_prepend(str, _("Hello, "));
	gtk_label_set_text(GTK_LABEL(label), str->str);
#ifdef DEBUG_GABY
	debug_print("[hello:fill] str : %s\n", str->str);
#endif
	g_string_free(str, 1);
}

