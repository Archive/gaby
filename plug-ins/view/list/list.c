/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <tables.h>
#include <records.h>
#include <windows.h>

mstatic void list_create ( gabywindow *window, gboolean first );
mstatic void list_fill ( gabywindow *window );

#ifndef FOLLOW_MIGUEL
int init_view_plugin (ViewPluginData *vpd)
{
	vpd->view_create = list_create;
	vpd->view_fill = list_fill;
	vpd->configure = NULL;
	vpd->view_records = NULL;

	vpd->name = "list";
	vpd->i18n_name = _("List");
	vpd->type = ALL_RECORDS;
	vpd->capabilities = FILTERABLE;

#ifdef DEBUG_GABY
	debug_print("Initialization of view plugin '%s' done succesfully.\n",
			vpd->i18n_name );
#endif

	return 0;
}
#endif /* ! FOLLOW_MIGUEL */

static void clist_click_column (GtkCList *clist, gint column, gpointer data);
static void list_fill_real_all (GtkWidget *cl, subtable *l, int id);
static void list_fill_real_some (GtkWidget *cl,subtable *l,GList *what, int id);
static void select_record( GtkCList *clist, gint row, gint column,
				GdkEventButton *event, gabywindow *win );
static void unselect_record( GtkCList *clist, gint row, gint column,
				GdkEventButton *event, gabywindow *win );


mstatic void list_create ( gabywindow *window, gboolean first )
{
	GtkWidget *clist;
	GtkWidget *sclwin;
	int i;
	subtable *subt = window->view->subtable;
	int *id;

#ifdef DEBUG_GABY
	debug_print("%s\n", _("List"));
#endif

	id = &(window->id);
	*id = 0;
	
/* clist widget is the primary thing to explain you need gtk+ >= 1.1.5
 * this is sad since I really liked the gray with thin white lines background
 * in gtk+ 1.0.
 * 
 * (currently you can have this with pixmaps themes (MacOS and Marble3d come
 *  to my mind) but another way is to simply put this in gtkrc :

 style "clist"
{
  bg[PRELIGHT] = "#dddddd"
  fg[PRELIGHT] = "#000000"
  base[NORMAL] = "#eeeeee"
  fg[SELECTED] = "#ffffff"
  bg[SELECTED] = "#333399"
}
class "GtkCList" style "clist"
widget_class "GtkCList" style "clist"

 * I don't know if #eeeeee is the same color as in 1.0 but it's kind of)
 * 
 * ok ok I stop comments and start code :)
 */
	sclwin = gtk_scrolled_window_new (NULL, NULL);
	window->widget = sclwin;
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sclwin),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_widget_show(sclwin);

	clist = gtk_clist_new(subt->nb_fields+1);
	gtk_clist_set_column_visibility (GTK_CLIST(clist), 0, FALSE);
	gtk_clist_set_row_height(GTK_CLIST(clist), 20 );
	
	gtk_signal_connect(GTK_OBJECT(clist), "click_column",
			GTK_SIGNAL_FUNC(clist_click_column), NULL );
	gtk_signal_connect(GTK_OBJECT(clist), "select_row",
			GTK_SIGNAL_FUNC(select_record), window );
	gtk_signal_connect(GTK_OBJECT(clist), "unselect_row",
			GTK_SIGNAL_FUNC(unselect_record), window );
	gtk_object_set_data(GTK_OBJECT(sclwin), "clist", clist );
	
	gtk_widget_show(clist);
	gtk_container_add (GTK_CONTAINER (sclwin), clist);

	gtk_clist_column_titles_show (GTK_CLIST (clist));
	
	for ( i=1; i< subt->nb_fields+1;i++ ) {
		gtk_clist_set_column_title( GTK_CLIST (clist), i ,
				subt->fields[i-1].i18n_name );
#ifdef DEBUG_GABY
		debug_print("[list:create] col %d : %s\n", i, subt->fields[i-1].i18n_name);
#endif
		gtk_clist_set_column_auto_resize(GTK_CLIST(clist), i, TRUE);
	}
		
	if ( subt->nb_fields == 2 ) {
		gtk_clist_set_column_width(GTK_CLIST(clist), 1, 200);
		gtk_clist_set_column_auto_resize(GTK_CLIST(clist), 1, FALSE);
	}

	gtk_widget_set_usize(sclwin, 400, 400);

	return;
}

static void clist_click_column (GtkCList *clist, gint column, gpointer data)
{
	if ( column == clist->sort_column ) {
		if ( clist->sort_type == GTK_SORT_ASCENDING ) {
			clist->sort_type = GTK_SORT_DESCENDING;
		} else {
			clist->sort_type = GTK_SORT_ASCENDING;
		}
	} else {
		gtk_clist_set_sort_column(clist, column);
	}
	gtk_clist_sort(clist);

/* if you ever saw gaby 1.0 you should say 'wooow' and thanks gtk+ */
}

mstatic void list_fill ( gabywindow *w )
{
	subtable *l;
	GtkWidget *clist;
	GList *what;
	int *id;
	condition *c;
	gboolean foo=FALSE;
	
	l = w->view->subtable;
	clist = gtk_object_get_data(GTK_OBJECT(w->widget), "clist");
	what = w->what;
	id = &(w->id);
	c = gtk_object_get_data(GTK_OBJECT(w->widget), "condition");

	if ( what == NULL && c == NULL) {
#ifdef DEBUG_GABY
		debug_print("[lf] Filling with all records\n");
#endif
		list_fill_real_all(clist, l, *id);
	} else {
#ifdef DEBUG_GABY
		debug_print("[lf] Filling with some records\n");
#endif
		if ( what == NULL ) {
			what = get_conditional_records_list(l, c);
			foo = TRUE;
		}
		list_fill_real_some(clist, l, what, *id);
	}
	
	if ( foo == TRUE ) {
		g_list_free(what);
	}
}

static void list_fill_real_all (GtkWidget *cl, subtable *l, int id)
{
	record *r;
	gchar *a_line[l->nb_fields+1];
	int i, j;
	table *p;
	GString *strs[l->nb_fields];
	int row;
	
	gtk_clist_freeze(GTK_CLIST(cl));
	gtk_clist_clear(GTK_CLIST(cl));
	
	a_line[0] = g_malloc(sizeof(gchar)*8);

	p = l->table;
	
	for ( i=0; i < p->max_records; i++ ) {
		
		r = p->records[i];
		if ( r == NULL || r->id == 0 )
			continue;
		
		sprintf(a_line[0], "%d", r->id);
		
		for ( j=0; j<l->nb_fields; j++) {
			strs[j] = get_subtable_stringed_field(l, r, j );
			a_line[j+1] = strs[j]->str;
		}
		
		row = gtk_clist_append(GTK_CLIST(cl), a_line);
		if ( r->id == id ) {
			gtk_clist_select_row(GTK_CLIST(cl), row, 0);
		}
		
	}

	g_free(a_line[0]);

	gtk_clist_thaw(GTK_CLIST(cl));
	
}

static void list_fill_real_some (GtkWidget *cl, subtable *l, GList *what,int id)
{
	record *r;
	gchar *a_line[l->nb_fields+1];
	int j;
	table *p;
	GString *strs[l->nb_fields];
	int row;
	
	gtk_clist_clear(GTK_CLIST(cl));
	gtk_clist_freeze(GTK_CLIST(cl));
	
#ifdef DEBUG_GABY
	debug_print("[list:lfrs] starting \n");
#endif

	p = l->table;
	
	a_line[0] = g_malloc(sizeof(gchar)*8);
	
	what = g_list_first(what);
	
	while ( what != NULL ) {
		if ( GPOINTER_TO_INT(what->data) == -1 )
			break;
		
		r = p->records[GPOINTER_TO_INT(what->data)];
		what = g_list_next(what);
		
		sprintf(a_line[0], "%d", r->id);
		
		for ( j=0; j<l->nb_fields; j++) {
			strs[j] = get_subtable_stringed_field(l, r, j );
			a_line[j+1] = strs[j]->str;
		}
		
		row = gtk_clist_append(GTK_CLIST(cl), a_line);
		if ( r->id == id ) {
			gtk_clist_select_row(GTK_CLIST(cl), row, 0);
		}
		
		
		for ( j=0; j<l->nb_fields; j++ ) {
			g_string_free(strs[j], 1);
		}
		
	}

#ifdef DEBUG_GABY
	debug_print("[list:lfrs] above g_free(a_line[0])\n");
#endif

	g_free(a_line[0]);

	gtk_clist_thaw(GTK_CLIST(cl));

#ifdef DEBUG_GABY
	debug_print("[list:lfrs] finished\n");
#endif
}

static void select_record(GtkCList *clist, gint row, gint column, 
			  GdkEventButton *event, gabywindow *win )
{
	int *id = &(win->id);
	gchar *a_line;

	gtk_clist_get_text(clist, row, 0, &a_line);
	*id = atoi(a_line);

	update_bound_windows(win);
}

static void unselect_record( GtkCList *clist, gint row, gint column,
				GdkEventButton *event, gabywindow *win )
{
	win->id = 0;
}

/*
 * Configure stuffs
 */


/*
static GtkWidget* configure_widget = NULL;

static GtkWidget* configure (ViewPluginData *vpd)
{
	GtkWidget *vbox;
	GtkWidget *label;
	
	if ( configure_widget && configure_widget->window )
		return configure_widget;

	vbox = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox);

	label = gtk_label_new(_("I can't (yet) be configured."));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);

	gtk_object_set_data(GTK_OBJECT(vbox), "name", vpd->i18n_name->str);

	configure_widget = vbox;
	return vbox;
}
*/
