/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <tables.h>
#include <records.h>

mstatic void genealogy_create ( gabywindow *window, gboolean first );
mstatic void genealogy_fill ( gabywindow *window );

#ifndef FOLLOW_MIGUEL
int init_view_plugin (ViewPluginData *vpd)
{
	vpd->view_create = genealogy_create;
	vpd->view_fill = genealogy_fill;
	vpd->configure = NULL;
	vpd->view_records = NULL;

	vpd->name = "genealogy";
	vpd->i18n_name = _("Genealogy");

	vpd->type = ONE_RECORD;
	vpd->capabilities = NONE;

	return 0;
}
#endif

static void previous_clicked(GtkWidget *button, gabywindow *window);
static void next_clicked(GtkWidget *button, gabywindow *window);
	
mstatic void genealogy_create( gabywindow *window, gboolean first )
{
	GtkWidget *vbox, *vbox2, *vbox3;
	GtkWidget *label;
	GtkWidget *label_father, *label_mother;
	GtkWidget *hbb;
	GtkWidget *table_parents;
	GtkWidget *button;
	GtkWidget *clist;
	int *id = &(window->id);
	record *r;
	int i;
	view *v = window->view;

	r = table_first(v->subtable->table, -1);
	*id = ( r == NULL ? 0 : r->id );

	vbox = gtk_vbox_new(FALSE, 0);
	
	table_parents = gtk_table_new(3, 2, TRUE);
	label = gtk_label_new(v->subtable->fields[8].i18n_name);
	gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table_parents), label,
					0, 1, 0, 1 );
	label = gtk_label_new(v->subtable->fields[9].i18n_name);
	gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table_parents), label,
					1, 2, 0, 1 );
	
	label_father = gtk_label_new("");
	gtk_object_set_data(GTK_OBJECT(vbox), "label_father", label_father);
	gtk_widget_show(label_father);
	gtk_table_attach_defaults(GTK_TABLE(table_parents), label_father,
					0, 1, 1, 2 );
	label_mother = gtk_label_new("-");
	gtk_object_set_data(GTK_OBJECT(vbox), "label_mother", label_mother);
	gtk_widget_show(label_mother);
	gtk_table_attach_defaults(GTK_TABLE(table_parents), label_mother,
					1, 2, 1, 2 );
	
	button = gtk_button_new_with_label(_("Center on"));
	gtk_widget_show(button);
	gtk_table_attach_defaults(GTK_TABLE(table_parents), button,
					0, 1, 2, 3 );
	button = gtk_button_new_with_label(_("Center on"));
	gtk_widget_show(button);
	gtk_table_attach_defaults(GTK_TABLE(table_parents), button,
					1, 2, 2, 3 );
	
	gtk_widget_show(table_parents);
	gtk_box_pack_start(GTK_BOX(vbox), table_parents, FALSE, TRUE, 0);

	vbox2 = gtk_vbox_new(FALSE, 5);
	gtk_widget_show(vbox2);
	gtk_box_pack_start(GTK_BOX(vbox), vbox2, FALSE, TRUE, 0);

	/* TODO (post-2.0): use mini-form ?
	 * delayed since I don't expect anybody to want to use this view
	 */
	
	label = gtk_label_new("");
	gtk_object_set_data(GTK_OBJECT(vbox), "first name", label);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox2), label, FALSE, TRUE, 0);

	/* this clist has men or women as column title and children from them
	 * in row (the first column is child's birthday */
	clist = gtk_clist_new(20);
	gtk_widget_show(clist);
	gtk_object_set_data(GTK_OBJECT(vbox), "clist", clist);
	gtk_clist_column_titles_show(GTK_CLIST(clist));
	gtk_clist_set_column_title(GTK_CLIST(clist), 0,
				v->subtable->fields[11].i18n_name );
	
	for ( i=1; i<20; i++ ) {
		gtk_clist_set_column_visibility(GTK_CLIST(clist), i, FALSE);
		/* TODO (post-2.0): I could perhaps put a widget as column title
	 	 * delayed since I don't expect anybody to want to use this view
		 */
	}
	gtk_box_pack_start(GTK_BOX(vbox), clist, FALSE, TRUE, 0);
	
	hbb = gtk_hbutton_box_new();
	gtk_widget_show(hbb);
	gtk_box_pack_start(GTK_BOX(vbox), hbb, FALSE, TRUE, 0);

	button = gtk_button_new_with_label(_("Previous"));
	gtk_widget_show(button);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
				GTK_SIGNAL_FUNC(previous_clicked), window);
	gtk_container_add(GTK_CONTAINER(hbb), button);

	button = gtk_button_new_with_label(_("Next"));
	gtk_widget_show(button);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
				GTK_SIGNAL_FUNC(next_clicked), window);
	gtk_container_add(GTK_CONTAINER(hbb), button);
	
	gtk_widget_show(vbox);
	
	return;
}

static void previous_clicked(GtkWidget *button, gabywindow *window)
{
	record *r;
	view *v = window->view;
	int *id = &(window->id);

	r = get_record_no(v->subtable->table, *id);
	r = table_prev(v->subtable->table, r, -1);
	*id = r->id;
	genealogy_fill(window);
}

static void next_clicked(GtkWidget *button, gabywindow *window)
{
	record *r;
	view *v = window->view;
	int *id = &(window->id);

	r = get_record_no(v->subtable->table, *id);
	r = table_next(v->subtable->table, r, -1);
	*id = r->id;
	genealogy_fill(window);
}

mstatic void genealogy_fill ( gabywindow *window )
{
	view *v = window->view;
	GtkWidget *win = window->widget;
	int *id = &(window->id);
	GtkWidget *label = gtk_object_get_data(GTK_OBJECT(win), "first name");
	GtkWidget *clist = gtk_object_get_data(GTK_OBJECT(win), "clist");
	record *r, *r_father, *r_mother, *marriage, *r_huswif, *r_child;
	GString *str1, *str2, *str3;
	gchar child_name[100];
	GList *marriages, *children, *tmpl, *tmpm;
	int col;
	int i;
	gchar *row[20];
	gchar *empty = "";
	
	if ( *id == 0 ) {
		gtk_label_set_text(GTK_LABEL(label), "");
		label = gtk_object_get_data(GTK_OBJECT(win), "label_father");
		gtk_label_set_text(GTK_LABEL(label), "");
		label = gtk_object_get_data(GTK_OBJECT(win), "label_mother");
		gtk_label_set_text(GTK_LABEL(label), "");
		return;
	}
	
	r = get_record_no(v->subtable->table, *id);

	str1 = get_subtable_stringed_field(v->subtable, r, 0 );
	str2 = get_subtable_stringed_field(v->subtable, r, 2 );

	str3 = g_string_new(str1->str);
	g_string_free(str1,1);
	str3 = g_string_append(str3, " ");
	str3 = g_string_append(str3, str2->str);
	g_string_free(str2,1);
	
	gtk_label_set_text(GTK_LABEL(label), str3->str);

	g_string_free(str3, 1);
	
	r_father = get_record_no(v->subtable->table, r->cont[8].i);
	r_mother = get_record_no(v->subtable->table, r->cont[9].i);
	str1 = get_subtable_stringed_field(v->subtable, r, 8 );
#ifdef DEBUG_GABY
	debug_print("[genealogy_fill] father : %s\n", str1->str);
#endif
	label = gtk_object_get_data(GTK_OBJECT(win), "label_father");
	gtk_label_set_text(GTK_LABEL(label), str1->str);
	g_string_free(str1, 1);
	
	str2 = get_subtable_stringed_field(v->subtable, r, 9 );
#ifdef DEBUG_GABY
	debug_print("[genealogy_fill] mother : %s\n", str2->str);
#endif
	label = gtk_object_get_data(GTK_OBJECT(win), "label_mother");
	gtk_label_set_text(GTK_LABEL(label), str2->str);
	g_string_free(str2, 1);

	/*
	 * filling the clist :
	 *  1. Marriages for column titles
	 *  2. Children for rows
	 */
	
	gtk_clist_freeze(GTK_CLIST(clist));
	gtk_clist_clear(GTK_CLIST(clist));
	for ( i=1; i<20; i++ ) {
		gtk_clist_set_column_visibility(GTK_CLIST(clist), i, FALSE);
	}
	
	marriages = get_related_records(&v->subtable->fields[16], *id);
	children = get_related_records(&v->subtable->fields[17], *id);
	tmpl = marriages;

	col = 1;
	while ( tmpl != NULL ) {
#ifdef DEBUG_GABY
		debug_print("[genealogy:fill] pos of marriage : %d\n",
				GPOINTER_TO_INT(tmpl->data));
#endif
		if ( GPOINTER_TO_INT(tmpl->data) == -1 ) {
#ifdef DEBUG_GABY
			debug_print("[genealogy:fill] this person was never married\n");
#endif
			tmpl = g_list_next(tmpl);
			continue;
		}
		
		marriage = v->subtable->fields[16].v->subtable->table->records[GPOINTER_TO_INT(tmpl->data)];

		gtk_clist_set_column_visibility(GTK_CLIST(clist), col, TRUE );

#ifdef DEBUG_GABY
		debug_print("[genealogy:fill] id of marriage: %d\n", marriage->id);
		debug_print("[genealogy:fill] husband : %d\n", marriage->cont[0].i);
		debug_print("[genealogy:fill] wife : %d\n", marriage->cont[1].i);
#endif

		/* TODO (post-2.0): custom values for boolean
	 	 * delayed since I don't expect anybody to want to use this view
		 *  this won't work with i18n, the way to resolve that could be
		 *  something like :
		 *  	gender:defined
		 *  		types=male,female,unkown
		 *  		types_i18n_fr=homme,femme,inconnu
		 *  		...
		 *  and the values could be but in an option menu or a combo
		 *  box :) definitely cooler :)
		 *  another wy could be to extend the string type to sth like:
		 *  	gender:string
		 *  		allowed=male,female,unknown
		 *  		...
		 *  I'll think about that ...
		 */
		if ( r->cont[7].str->str[0] == 'M' ) {
			/* man */
			r_huswif = get_record_no(v->subtable->table,
					marriage->cont[1].i );
			if ( r_huswif != NULL ) {
				str1 = get_subtable_stringed_field(
					v->subtable->fields[16].v->subtable,
					marriage, 1 );
#ifdef DEBUG_GABY
				debug_print("[genealogy:fill] this is a man and his wife is %s %s\n", r_huswif->cont[0].str->str, r_huswif->cont[2].str->str);
#endif
			}
		} else {
			/* woman */
			r_huswif = get_record_no(v->subtable->table,
					marriage->cont[0].i );
			if ( r_huswif != NULL ) {
				str1 = get_subtable_stringed_field(
					v->subtable->fields[16].v->subtable,
					marriage, 0 );
#ifdef DEBUG_GABY
				debug_print("[genealogy:fill] this is a woman and her husband is %s %s\n", r_huswif->cont[0].str->str, r_huswif->cont[2].str->str);
#endif
			}
		}
#ifdef DEBUG_GABY
		debug_print("[genealogy:fill] idem : %s\n", str1->str);
#endif
		gtk_clist_set_column_title(GTK_CLIST(clist), col, str1->str);
		g_string_free(str1, 1);
		
		/* filling with children */
		if (children != NULL || GPOINTER_TO_INT(children->data) == -1) {
			tmpm = g_list_first(children);
			while ( tmpm != NULL ) {
				if ( GPOINTER_TO_INT(tmpm->data) == -1 ) {
					tmpm = g_list_next(tmpm);
					continue;
				}
				r_child = v->subtable->fields[17].v->subtable->table->records[GPOINTER_TO_INT(tmpm->data)];
				tmpm->data = GINT_TO_POINTER(-1);
#ifdef DEBUG_GABY
				debug_print("[genealogy:fill] children subtable name : %s\n", v->subtable->fields[17].v->subtable->i18n_name);
#endif
				str2 = get_subtable_stringed_field(v->subtable->fields[17].v->subtable, r_child, 2);
				row[0] = str2->str;
				str3 = get_subtable_stringed_field(v->subtable->fields[17].v->subtable, r_child, 0);
				strcpy(child_name, str3->str);
				g_string_free(str3, 1);
				str3 = get_subtable_stringed_field(v->subtable->fields[17].v->subtable, r_child, 1);

				child_name[strlen(child_name)+1] = 0;
				child_name[strlen(child_name)] = ' ';
#ifdef DEBUG_GABY
				debug_print("[g:f] child name : %s (& %s)\n", str3->str, child_name);
#endif
				strcpy(child_name+strlen(child_name),str3->str);
				g_string_free(str3, 1);
				
				for ( i=1; i <20; i++ ) {
					if ( i == col ) {
						row[i] = child_name;
					} else {
						row[i] = empty;
					}
				}

				gtk_clist_append(GTK_CLIST(clist), row);

				g_string_free(str2, 1);
				
				tmpm = g_list_next(tmpm);
			}
			;
		}
		
		col++;
		tmpl = g_list_next(tmpl);
	}

	g_list_free(marriages);
	
	gtk_clist_thaw(GTK_CLIST(clist));
	
}

static GtkWidget* configure(ViewPluginData *vpd)
{
	return NULL;
}

