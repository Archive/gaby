/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <tables.h>
#include <records.h>

mstatic void miniform_create ( gabywindow *window, gboolean first );
mstatic void miniform_fill ( gabywindow *window );

#ifndef FOLLOW_MIGUEL
int init_view_plugin (ViewPluginData *vpd)
{
	vpd->view_create = miniform_create;
	vpd->view_fill = miniform_fill;
	vpd->configure = NULL;
	vpd->view_records = NULL;

	vpd->name = "miniform";
	vpd->i18n_name = _("Mini Form");
	vpd->type = ONE_RECORD;
	vpd->capabilities = EDITABLE;

	return 0;
}
#endif

static void field_changed ( GtkWidget *entry, gabywindow *window );

mstatic void miniform_create ( gabywindow *window, gboolean first )
{
	GtkWidget *table;
	GtkWidget *label;
	GtkWidget *entry;
	int i;
	st_field *sf;

	if ( window->view->subtable->nb_fields > 5 )
		return;		/* sorry angel but this is too much
				   for little form */
	
	table = gtk_table_new ( window->view->subtable->nb_fields, 2, FALSE );
	gtk_container_border_width(GTK_CONTAINER(table), 5 );
	gtk_table_set_row_spacings (GTK_TABLE (table), 5);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);

	for ( i=0; i<window->view->subtable->nb_fields; i++ ) {
		sf = &window->view->subtable->fields[i];
		label = gtk_label_new(sf->i18n_name);
		gtk_table_attach_defaults(GTK_TABLE(table), label, 0,1,i,i+1);
		gtk_widget_show(label);
		entry = gtk_entry_new();
		gtk_table_attach_defaults(GTK_TABLE(table), entry, 1,2,i,i+1);
		gtk_object_set_data(GTK_OBJECT(entry), "number",
				GINT_TO_POINTER(i));
		if ( sf->type == T_RECORD || sf->type == T_RECORDS ) {
			gtk_entry_set_editable(GTK_ENTRY(entry), FALSE);
			gtk_entry_set_text(GTK_ENTRY(entry),
					_("Not editable in a mini-form"));
		} else {
			gtk_signal_connect(GTK_OBJECT(entry), "changed",
				GTK_SIGNAL_FUNC(field_changed), window );
		}
		gtk_widget_show(entry);
	}
		
	gtk_widget_show(table);
	
	window->widget = table;
	
	return;
}

mstatic void miniform_fill ( gabywindow *window )
{
	;
}

static void field_changed ( GtkWidget *entry, gabywindow *window )
{
	GtkWidget *table = window->widget;
	int number = GPOINTER_TO_INT(
			gtk_object_get_data(GTK_OBJECT(entry), "number"));
	view *v = window->view;
	record *r = gtk_object_get_data(GTK_OBJECT(table), "record");
	st_field sf = v->subtable->fields[number];
	union data *d;
	gchar *string;
	
	if ( r == NULL )
		return;

	d = &r->cont[sf.no];
	
	string = gtk_entry_get_text(GTK_ENTRY(entry));

#if 0
	switch ( sf.type ) {
		case T_STRING:
		case T_STRINGS:
		case T_INTEGER:
		{
			set_subtable_stringed_field(v->subtable, r, number,
					string );
		} break;
		case T_DATE:
		{
			if ( d->date == NULL )
				d->date = g_date_new();
			g_date_set_parse(d->date, string );
		} break;
		default:
		{
			;
		} break;
	}
#endif
	set_subtable_stringed_field(v->subtable, r, number, string );
}


