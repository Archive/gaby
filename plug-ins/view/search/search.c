/*  Gaby
 *  Copyright (C) 1998-2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gaby.h>
#include <f_desc.h>	/* for subtable_get_field_no */
#include <tables.h>
#include <records.h>
#include <windows.h>

mstatic void search_create ( gabywindow *window, gboolean first );
mstatic void search_fill ( gabywindow *window );

#ifndef FOLLOW_MIGUEL
int init_view_plugin (ViewPluginData *vpd)
{
	vpd->view_create = search_create;
	vpd->view_fill = search_fill;
	vpd->configure = NULL;
	vpd->view_records = NULL;

	vpd->name = "search";
	vpd->i18n_name = _("Search");
	vpd->type = FILTER;
	vpd->capabilities = NONE;

#ifdef DEBUG_GABY
	debug_print("Initialization of view plugin '%s' done succesfully.\n",
			vpd->i18n_name );
#endif

	return 0;
}
#endif

static gabywindow* get_first_window(subtable *st);
static void fill_windows_list(GtkWidget *list, subtable *st);
static void update_clicked(GtkWidget *but, gabywindow *window);
static void find_clicked(GtkWidget *but, gabywindow *window);
static void findnext_clicked(GtkWidget *but, gabywindow *window);
static void realfind(gabywindow *window, int from);

mstatic void search_create ( gabywindow *window, gboolean first )
{
	GtkWidget *vbox, *hbox, *sep, *button;
	GtkWidget *affected_window, *update, *label, *menu, *om, *entry;
	gabywindow *win;
	GSList *group;
	view *v = window->view;
	int j;
	
	vbox = gtk_vbox_new (FALSE, 5);
	window->widget = vbox;
	
	hbox = gtk_hbox_new (FALSE, 5);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE, 0);
	
	label = gtk_label_new(_("Affected window :"));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0 );
	
	win = get_first_window(v->subtable);
	affected_window = gtk_combo_new();
	fill_windows_list(GTK_COMBO(affected_window)->list, v->subtable);
	gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(affected_window)->entry),
							win->name );
	gtk_widget_show(affected_window);
	gtk_box_pack_start(GTK_BOX(hbox), affected_window, TRUE, TRUE, 0 );

	update = gtk_button_new_with_label(_("Update"));
	gtk_signal_connect(GTK_OBJECT(update), "clicked",
				GTK_SIGNAL_FUNC(update_clicked), window );
	gtk_widget_show(update);
	gtk_box_pack_start(GTK_BOX(hbox), update, TRUE, TRUE, 0 );

	sep = gtk_hseparator_new();
	gtk_widget_show(sep);
	gtk_box_pack_start(GTK_BOX(vbox), sep, FALSE, FALSE, 3 );

	hbox = gtk_hbox_new (FALSE, 5);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 3 );
	
	om = gtk_option_menu_new();
	menu = gtk_menu_new();
	group = NULL;
	for ( j=0; j < v->subtable->nb_fields; j++ ) {
		GtkWidget *li = gtk_radio_menu_item_new_with_label (group,
					v->subtable->fields[j].i18n_name );
		group = gtk_radio_menu_item_group(GTK_RADIO_MENU_ITEM(li));
		gtk_widget_show(li);
		gtk_menu_append(GTK_MENU(menu), li);
	}
	gtk_widget_show(menu);
	gtk_option_menu_set_menu(GTK_OPTION_MENU(om), menu);
	gtk_widget_show(om);
	gtk_box_pack_start(GTK_BOX(hbox), om, FALSE, FALSE, 3 );

	entry = gtk_entry_new();
	gtk_widget_show(entry);
	gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, FALSE, 3 );
	
	sep = gtk_hseparator_new();
	gtk_widget_show(sep);
	gtk_box_pack_start(GTK_BOX(vbox), sep, FALSE, FALSE, 3 );

	hbox = gtk_hbox_new (FALSE, 5);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 3 );
	
	button = gtk_button_new_with_label(_("Find"));
	gtk_signal_connect(GTK_OBJECT(button), "clicked", find_clicked,window);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 3 );

	button = gtk_button_new_with_label(_("Find next"));
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
						findnext_clicked,window);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 3 );

	gtk_object_set_data(GTK_OBJECT(vbox), "affwincombo", affected_window);
	gtk_object_set_data(GTK_OBJECT(vbox), "field_combo", group);
	gtk_object_set_data(GTK_OBJECT(vbox), "field_entry", entry);
	
	gtk_widget_show(vbox);
	return;
}

mstatic void search_fill ( gabywindow *window )
{
	;
}

static gabywindow* get_first_window(subtable *st)
{
	GtkWidget *its_parent;
	view *v;
	GList *tmp = g_list_first(list_windows);
	gabywindow *window;

	while ( tmp != NULL ) {
		window = tmp->data;
		tmp = g_list_next(tmp);
		if ( ! GTK_IS_OBJECT(window->widget) ) continue;
		its_parent = window->parent;
		if ( GTK_IS_WINDOW(its_parent) ) {
			v = window->view;
			if ( v->subtable->table == st->table &&
					v->type->type != FILTER ) {
				return window;
			}
		}
	}

	return NULL;
}

static void fill_windows_list(GtkWidget *list, subtable *st)
{
	GtkWidget *its_parent, *li;
	view *v;
	GList *tmp = g_list_first(list_windows);
	gchar *name;
	gabywindow *window;

	gtk_list_clear_items(GTK_LIST(list), 0, -1);

	while ( tmp != NULL ) {
		window = tmp->data;
		tmp = g_list_next(tmp);
		if ( ! GTK_IS_OBJECT(window->widget) ) continue;
		its_parent = window->parent;
		if ( GTK_IS_WINDOW(its_parent) ) {
			v = window->view;
			name = window->name;
			if ( v->subtable->table == st->table &&
					v->type->type != FILTER ) {
				
				li = gtk_list_item_new_with_label(name);
				gtk_widget_show(li);
				gtk_container_add(GTK_CONTAINER(list), li);
			}
		}
	}
}

static void update_clicked(GtkWidget *but, gabywindow *window)
{
	GtkWidget *box = window->widget;
	GtkWidget *combo = gtk_object_get_data(GTK_OBJECT(box), "affwincombo");
	view *v = window->view;

	fill_windows_list(GTK_COMBO(combo)->list, v->subtable);
}

static void find_clicked(GtkWidget *but, gabywindow *window)
{
	realfind(window, 0);
}

static void findnext_clicked(GtkWidget *but, gabywindow *window)
{
	int from = GPOINTER_TO_INT(gtk_object_get_data( 
				GTK_OBJECT(window->widget), "from"));
	realfind(window, from+1);
}

static void realfind(gabywindow *window, int from)
{
	GtkWidget *box = window->widget;
	gabywindow *affwin;
	GtkWidget *affwinc = gtk_object_get_data(GTK_OBJECT(box),"affwincombo");
	GSList *fcombo = gtk_object_get_data(GTK_OBJECT(box), "field_combo");
	GtkWidget *entry = gtk_object_get_data(GTK_OBJECT(box), "field_entry");
	view *v = window->view;
	int fn;
	gchar *what;
	int pos, id;
	ViewPluginData *vpd;

	affwin = get_window_by_name( gtk_entry_get_text( 
				GTK_ENTRY(GTK_COMBO(affwinc)->entry)));
	if ( affwin == NULL ) {
		gaby_message = g_strdup( _("It looks like you destroyed the window you selected."));
		gaby_errno = CUSTOM_ERROR;
		gaby_perror_in_a_box();
		return;
	}

	/* filter.c does a g_list_find(list_windows, affwin) here but I now
	 * thinks it is redundant */
	
	/* retrieve field number */
	fn = v->subtable->nb_fields-1;
	while ( fcombo != NULL ) {
		GtkCheckMenuItem *item = fcombo->data;
		if ( item->active ) break;
		fn--;
		fcombo = g_slist_next(fcombo);
	}

	what = gtk_entry_get_text(GTK_ENTRY(entry));
	
#ifdef DEBUG_GABY
	debug_print("[search:apply_clicked] serching %s in '%s'\n",
				what, v->subtable->fields[fn].i18n_name );
#endif
	
	/* OTOD: (in records.c) generic search function [DONE: 000105]
	 * it would have this form: search(table*, int fn, char*, int from);
	 * the 'int from' is important since it will allow 'find next'
	 * (it will use g_strcasecmp)
	 */

	pos = subtable_search_record(v->subtable, fn, what, from);
	if ( pos == -1 ) {
		if ( from == 0 ) {
			gaby_message = g_strdup( _("No record found" ));
		} else {
			gaby_message = g_strdup( _("No more record found" ));
		}
		gaby_errno = CUSTOM_ERROR;
		gaby_perror_in_a_box();
		return;
	}

	gtk_object_set_data(GTK_OBJECT(window->widget), "from",
						GINT_TO_POINTER(pos));
	id = v->subtable->table->records[pos]->id;

#ifdef DEBUG_GABY
	debug_print("[search:apply_clicked] search result: %d (id==%d)\n", 
								pos, id);
#endif
	affwin->id = id;
	vpd = affwin->view->type;
	vpd->view_fill(affwin);
	update_bound_windows(affwin);
}

