#! /usr/bin/env python
# Created by Emmanuel Jeandel; released as GPL
# Needs Python 2.0 !

import sys
import os
import struct
import string

try:
	filename = sys.argv[1]
except:
	print 'Usage: %s <filename>' % sys.argv[0]
	print 'Note: you should not specify the .dbf extension'
	sys.exit(1)

def norm(s):
	s = string.replace(s, '\0', '')
	s = string.replace(s, '\32', '')
	s = string.replace(s, '\015\012', '\\n')
	s = string.replace(s, '\012', '\\n')
	s = string.replace(s, '\012', '\\n')
	s = string.replace(s, ';', '\\;')
	return s

def findtype(s):
	if s == 'C':
		return "String"
	elif s == 'M':
		return "Strings"
	elif s == 'D':
		return "Date"
	elif s == 'N':
		return "Integer"	
	else:
		# s== 'L' (logic) or 'F' or ... 
		print "Warning : " + s + " Not implemented"
		return "String"

def process(s):
	(name, type, size) = s
	# firstly: removing useless spaces 
	data = norm(string.rstrip(dbf.read(size)))
	if type == "String": # text
		return data
	elif type == "Date":
		return  data[0:4] + " "  +data[4:6] + " " +data[6:8]
	elif type == "Integer":
		if data == '':
			return ""
		else:
			# converting to remove zeros
			return str(int(data))
	elif type == "Strings": # memo
		if data == '':
			return ""
		else:
			return mem[int(data)-1]

def processHeader(dbf):
	memo = 1
	(format, day1, day2, day3) = struct.unpack("4c", dbf.read(4))	
	form = ord(format)
	if form == 3:
		print "Format: FoxBase+, FoxPro, dBaseIII+, dBaseIV, no memo"
		memo = 0
	elif form == 131:
		print "Format: FoxBase+, dBaseIII+ with memo"
	elif form == 245:
		print "Format: FoxPro with memo"
	elif form == 139:
		print "Format: dBaseIV with memo"
	elif form == 142:
		print "Format: dBaseIV with SQL Table"
		print "Warning: not Implemented"
		
	print "Last modified : %2.2d %2.2d %2.2d" % (ord(day1),ord(day2),ord(day3))
	(numofRecords,headersize, recordsize) = struct.unpack("<lhh", dbf.read(8))
	numofFields=   (headersize - 33) / 32
	dbf.seek(32)
	fields = [(norm(name),findtype(type),ord(size)) for (name, type, dummy, size, dummy) in [struct.unpack("<11sc4sc15s", dbf.read(32)) for i in range(numofFields)]]
	return (numofRecords, fields,memo)

def processMemo(memo):
	(nbNotes,) = struct.unpack("<l", memo.read(4))
	memo.seek(508,1)
	return [norm(memo.read(512)) for i in range(nbNotes)]

def printDesc(filename, fields):
	print "Begin tables"
	print "\t"+filename
	for (name, type, size) in fields:
		print "\t\t"+name+":"+type
	print "End"

	print "Begin subtables"
	print "\t"+filename+":"+filename
	print "\t\t viewable as form, xlist, filter."
	print "End"
	print "Begin misc"
	print "\tMain window"
	print "\t\tform:"+filename
	print "\t\t\tvisible = yes"
	print "End"

dbf = open(filename + ".dbf")

(numofRecords, fields, memo) = processHeader(dbf)
# we can now read the memo field
if (memo == 1):
	memo = open(filename+".dbt")
	mem = processMemo(memo)

dbf.seek(1,1)
def glob(s):
	y,s = s
	return s

result = map(glob, filter( lambda (y, z):  y != '*', [(dbf.read(1), map(process, fields)) for i in range(numofRecords)]))

final = map(lambda x,y:str(y+1)+';'+string.join(x,';'), result, range(len(result)))


# printing description file
printDesc(filename, fields)
# printing table
for i in final: print i

