#! /bin/sh

PY_PREFIX=`python -c 'import sys ; print sys.prefix'`
PY_VERSION=`python -c 'import sys ; print sys.version[0:3]'`

if [ -e $PY_PREFIX/lib/libpython$PY_VERSION.so ]
then
	echo "shared version already there"
	exit
fi

mkdir .extract
(cd .extract; ar xv $PY_PREFIX/lib/python$PY_VERSION/config/libpython$PY_VERSION.a)
gcc -shared -o libpython$PY_VERSION.so .extract/*.o
rm -rf .extract

cp libpython$PY_VERSION.so $PY_PREFIX/lib

