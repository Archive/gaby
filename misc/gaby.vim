" Vim syntax file
" Language:	Gaby description file
" Maintainer:	Frederic Peters <fpeters@swing.be>
" Last change:	1999 May 29

" this is beta 'cause I have other things to do than learning how to write
" cool Vim syntax files :)

" to use this script simply put :
" 	au BufNewFile,BufRead desc.*    source <path to>/gaby.vim
" in your ~/.vimrc

" Remove any old syntax stuff hanging around
syn clear

syn match	gabySection	"^Begin.*"
syn match	gabySection	"^End.*"
syn keyword	gabyFieldtype	string strings date integer real multimedia decimal

syn match	gabyFieldName	"^\t\t[A-Za-z(].*:"
syn match	gabyI18n	"^\t*.i18n_.."
syn match	gabyComment	"^#.*$"

if !exists("did_gabydesc_syntax_inits")
  let did_gabydesc_syntax_inits = 1
  " The default methods for highlighting.  Can be overridden later
  hi link gabySection		Special
  hi link gabyI18n		PreProc
  hi link gabyFieldtype		Type
  hi link gabyFieldName		Constant
  hi link gabyComment		Comment
endif

let b:current_syntax = "gaby"

" vim: ts=8
