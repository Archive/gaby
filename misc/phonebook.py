#! /usr/bin/env python

import gaby
import sys
import os
import cgi
import pwd

sys.stderr = sys.stdout

print 'Content-type: text/html'
print

form = cgi.FieldStorage()

if not form.has_key('login'):
	print '''\
<html><head><title>Phone Book - Login</title></head><body>
<form method=get>
Username: <input name=login></input><br>
<input type=submit></input>
</form>
</body></html>'''
	sys.exit()

try:
	home_dir = pwd.getpwnam( form['login'].value )[5]
except KeyError:
	print 'Nobody with that username'
	sys.exit()

if len(form.keys()) == 0 or \
		not (form.has_key('person') or form.has_key('record')):
	print '''\
<html><head><title>Phone Book - Search</title></head><body>
<form method=get>
Search for: <input name=person></input><br>
<input type=submit value=GO!></input>
<input type=hidden name=login value=%s></input>
</form>
</body></html>''' % form['login'].value
	sys.exit()

print '<html><head><title>Phone Book - Result</title></head><body>'

os.environ['HOME'] = home_dir

gaby.load_database('gaby')

if form.has_key('person'):
	subtable = gaby.get_subtable_by_name('Phone Book')
	found = 0
	print '<table>'
	for id in subtable.fast_records_list():
		r = subtable.get_record_no(id)
		if r[1] == form['person'].value:
			print '<tr><td><a href=phonebook.py?login=%s&record=%d>' % ( form['login'].value, id ),
			print '<img src=ball.png alt="-"></a>',
			print '<td>', r[0], r[1], '<td>', r[2], '</tr>'
			found = 1
	print '</table>'
	if found == 0:
		print '<strong>Nobody with that name in the database</strong>'
elif form.has_key('record'):
	subtable = gaby.get_subtable_by_name('Address Book')
	r = subtable.get_record_no ( int(form['record'].value) )
	i=0
	print '<table>'
	for fn in subtable.fields:
		if len(r[i]) != 0:
			print '<tr><td>', '%s:' % fn, '<td>', r[i], '</tr>'
		i = i+1
	print '</table>'
else:
	pass

print '</body></html>'

