#! /usr/bin/env python

'''This script takes a cddb-record file (usually from ~/.cddb) and add its
contents to the tables used in gaby-gcd'''

import gaby
import sys
import string

if len(sys.argv) == 1 or sys.argv[1] in [ '--help', '-h', '--usage', '-?' ]:
	print 'Usage: cddb_import cddb_file'
	sys.exit(0)

try:
	f = open(sys.argv[1])
except:
	print 'Failed to open', sys.argv[1]
	sys.exit(1)

lines = f.readlines()
f.close()

i = 0
while lines[i] != '# Track frame offsets:\n':
	i = i + 1

i = i + 1
pos = []
while lines[i] != '# \n':
	pos.append( int(lines[i][2:]))
	i = i + 1

l = range(0, len(pos)-1)
l.reverse()

for j in l:
	pos[j+1] = pos[j+1] - pos[j]
	pos[j+1] = pos[j+1] / 75

pos = pos[1:] # we discard the first offset

def add(x,y): return x+y
length = reduce(add, pos)

i = i + 1  # we should be on the line 'Disc length'
pos.append( int(lines[i][14:-8]) - length )

while ( lines[i][0] != 'D'):
	i = i + 1
title = lines[i+1][7:-1]
year  = lines[i+2][6:-1]
genre = lines[i+3][7:-1]
i = i + 4

tracks = []
j = 0
while ( lines[i][0] == 'T'):
	t = lines[i][ string.find(lines[i], '=')+1 : -1 ]
	tracks.append( (t, pos[j] ) )
	i, j = i + 1, j + 1

print tracks

gaby.load_database('gcd')

st_tracks = gaby.get_subtable_by_name('Tracks with CDs')
st_cds = gaby.get_subtable_by_name('CDs')

record = [ title, '', genre, '' ]
id = st_cds.set_record_no(0, record)
print id

for p in st_cds.fast_records_list():
	print st_cds.get_record_no(p)

i = 1
for t in tracks:
	duration = '%d:%d' % divmod(t[1], 60)
	record = [ str(i), t[0], duration, id ]
	i = i + 1
	st_tracks.set_record_no(0, record)
	
for p in st_tracks.fast_records_list():
	print st_tracks.get_record_no(p)

st_tracks.save()
st_cds.save()

sys.exit(0)

