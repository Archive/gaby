
create table AddressBook (
	id		integer		primary key,
	firstname	varchar(50),
	lastname	varchar(50),
	more		varchar(50),
	street		varchar(100),
	zip		varchar(8),
	city		varchar(60),
	state		varchar(30),
	country		varchar(30),
	birthday	varchar(20),
	privatephone	varchar(20),
	workphone	varchar(20),
	cellphone	varchar(20),
	fax		varchar(20),
	email		varchar(50),
	website		varchar(50),
	category	varchar(30),
	remarks		varchar(250)
);

create table Zip (
	id		integer		primary key,
	zip		varchar(10),
	city		varchar(50)
);

