#! /usr/bin/perl -w
# this _really_ simple script convert a gaby database from the format used
# in (0.x.x and) 1.0 to the format used in (1.8.x, 1.9.x and) 2.0
# 
# Mon, 28 Dec 1998 17:50:48 +0100 - Frederic Peters

$FILESRC=shift || die "$0 source-filename target-filename\n";
$FILETGT=shift || die "$0 source-filename target-filename\n";

-e $FILESRC || die "$FILESRC must exist.\n";
(not -e $FILETGT) || die "$FILETGT must not exist.\n";

open INPUT, $FILESRC;
open OUTPUT, ">$FILETGT";

$linenb=1;

while ( <INPUT> ) {
	next if (/^#/);
	print OUTPUT ($linenb, ';', $_);
	$linenb++;
}

close INPUT;
close OUTPUT;

print "I think it is (correctly) done.\n";


