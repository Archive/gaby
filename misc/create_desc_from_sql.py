#! /usr/bin/python

import sys
from pg import DB

db = DB('gabysql')
tables = db.get_tables()

print 'Begin tables'

type_mapping = {'int'	: 'integer',
		'text'	: 'string',
		'date'	: 'date' }

for t in tables:
	res = db.query('select * from %s limit 1' % t)
	if res.fieldname(0) != 'id':
		sys.stderr.write('%s has no \'id\' as first field; skipped.\n'
					% t)
		continue
	print '\t%s:%s' % (t, t)
	atts = db.get_attnames(t)
	for f in res.listfields()[1:]:
		ft = atts[f]
		print '\t\t%s:%s' % (f, type_mapping[ft])

print 'End'

