#! /usr/bin/env python

import sys
import gaby

gaby.load_database('gaby')
st = gaby.get_subtable_by_name('Address Book')

records = []
for id in st.fast_records_list():
	records.append( st.get_record_no(id) )

def cmpfunc(x, y):
	if len(y[1]) == 0: return 0
	if len(x[1]) == 0: return 1
	return cmp(x[1], y[1])

records.sort( lambda x,y: cmpfunc(x, y) )

print '<html><head><title>Address Book</title></head>\n<body><table>'

for r in records:
	print '<tr><td>', r[0], '<td>', r[1]

print '</table></html>'

sys.exit(0)

