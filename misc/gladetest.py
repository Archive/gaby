#! /usr/bin/python

# this file has to be copied to ~/.gaby/scripts and will be used in the gaby
# --as glade 'demo'

window = gaby.get_current_window()

import sys
sys.stderr.write(window.name + '\n')

import gtk

wid = window.get_widget('entry-First Name')
entry = gtk.GtkEntry(_obj = wid)
wid = window.get_widget('frame-testarea')
frame = gtk.GtkWidget(_obj = wid)

text = entry.get_text()
frame.set_sensitive(text == 'Fr�d�ric')

