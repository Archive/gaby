#! /usr/bin/env python
# Created by Frederic Peters; released as GPL
# Needs Python 1.5 !

import sys
import string

try:
	lines = open(sys.argv[1]).readlines()
	f = open(sys.argv[2], 'w')
except:
	print 'Usage: convert inputfile outputfile'
	sys.exit(0)

for i in range(0, len(lines)):
	l = string.split(lines[i][:-1], ',')
	l = map(lambda x: x[1:-1], l)
	l.insert(0, str(i+1))
	f.write(string.join(l, ';') + '\n')

f.close()

