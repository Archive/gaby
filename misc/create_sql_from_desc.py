#! /usr/bin/env python

import sys
import string

try:
	f = open(sys.argv[1])
except:
	print 'Usage: create_sql_from_desc.py descfile'
	sys.exit(1)

lines = f.readlines()
f.close()

intables = 0
intable = 0

tables = []
t = ''
for l in lines:
	if l == 'Begin tables\n':
		intables = 1
		continue
	if intables == 1 and l == 'End\n':
		break
	if intables == 1 and l[0] == '\t' and l[1] != '\t':
		if intable == 1:
			t = t[:-2] + '\n);\n'
			tables.append(t)
		tname = string.split(l[1:], ':')[0]
		if tname[-1] == '\n': tname = tname[:-1]
		t = 'create table %s (\n' % tname
		t = t + '\tid\tinteger\t\tprimary key,\n'
		intable = 1
		continue
	if intable == 1 and l[0:2] == '\t\t' and l[2] != '\t':
		fname = string.split(l[2:], ':')[0]
		fname = string.replace(fname, ' ', '_')
		fname = string.replace(fname, '/', '_')
		t = t + '\t%s\tvarchar(50),\n' % fname

t = t[:-2] + '\n);\n'
tables.append(t)

for t in tables:
	print t

