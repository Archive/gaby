
/*
   Loads'o stuff waiting for a better place to be put in

   Gaby Description Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */


#ifndef _SHARED_H
#  define _SHARED_H

void popup_dialog (void (*function) (gchar *, gpointer), gchar * caption,
		   gchar * text, gchar * defaultt, gpointer data);

void popup_locale_dialog (void (*function) (doublestring *, gpointer),
			  gchar * caption, gchar * text,
			  gchar * deftext,
			  gchar * defloc,
			  gpointer wid);
GtkWidget *xpm_label_box (char **xpme, gchar * label_text, gint order, gint * key);
GtkWidget *scroll_new (GtkWidget *);
void trim (gchar *);

gint check_name (gchar *, int table);
gboolean is_table_uniq(gchar *name, GList *tables);
gboolean is_subtable_uniq(gchar *name, GList *subtables);
gboolean is_field_uniq(gchar *name, GList *fields);
gboolean is_subfield_uniq(gchar *name, GList *subfields);

void error_dialog (gchar * tekst, gchar * caption);


void dialog (char *tekst, char *caption, int options,
	     void (*yes) (gpointer data),
	     void (*no) (gpointer data),
	     void (*cancel) (gpointer data), gpointer data);

char* get_untranslated_string(char *string, doublestring *table);
char* get_translated_string(char *string, doublestring *table);

#endif /* ! _SHARED_H */

