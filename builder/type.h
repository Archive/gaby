/*
   All the structures the builder uses.

   Gaby Description Builder
   Copyright (C) 1999  Ron Bessems
   Copyright (C) 2000  Frederic Peters


   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */


enum field_type
{
	T_STRING = 0,
	T_STRINGS = 1,
	T_INTEGER = 2,
	T_REAL = 3,
	T_DATE = 4,
	T_BOOLEAN = 5,
	T_MULTIMEDIA = 6,
	T_DECIMAL = 7,
	T_FILE = 8,
	T_RECORD = 9,		/* those are for links between tables. */
	T_RECORDS = 10,		/* later. */
};

typedef struct _location	location;
typedef struct _table_location  tlocation;
typedef struct _table		table;
typedef struct _field		field;
typedef struct _subtable	subtable;
typedef struct _subfield	subfield;
typedef struct _misc_s		misc_s;
typedef struct _file_s		file_s;
typedef struct _doublestring	doublestring;

struct _location
{
	gchar *path;
	GList *prop;
	GtkWidget *window;
};

struct _table_location
{
	gchar *name_of_table;
	GList *locations;
	GtkWidget *window;
};  

struct _misc_s
{
	gchar *name_of_subtable;
	gchar *view_as;
	GList *bind_to;
	gboolean visible;
	gboolean main;
	GtkWidget *window;
};

struct _subtable
{
	gchar *name;
	gchar *name_of_table;
	GList *viewable_as;
	GList *subfields;
	GList *i18n_name;
	GList *i18n_desc;
	GtkWidget *propwindow;
	GtkWidget *window;
};

struct _subfield
{
	gchar *name;
	gchar *name_of_field;
	GList *i18n_name;
	GList *i18n_desc;
	GtkWidget *window;
};


struct _file_s
{
	gchar *name;
	GtkWidget *table;
	GtkWidget *preview;
	GtkWidget *subtablewin;
	GtkWidget *actionwin;
	GtkWidget *locationwin;
	GtkWidget *miscwin;
	GList *tables;
	GList *subtables;
	GList *miscs;
	GList *tlocations;
	gchar *actions;
	gchar *plugin_options;
	gboolean changed;
};

struct _field
{
	gchar *name;
	GList *properties;
	GList *i18n_name;
	GList *i18n_desc;
	enum field_type type;
	GtkWidget *window;
};

struct _table
{
	gchar *name;
	gchar *alias;
	GList *fields;
	GtkWidget *window;
};

struct _doublestring
{
	gchar *name;
	gchar *locale;
};


