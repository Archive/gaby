/*
   subtable properties window

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */



#include "main.h"
#include "icons/ja.xpm"

static GtkAccelGroup *accel;




typedef struct _main_update info1;

struct _main_update
  {
    GtkWidget *namelist;
    GtkWidget *desclist;
    GtkWidget *window;
    file_s *s;
    subtable *stab;
  };




/* ******************************************************************** */
/* Events                                                               */
/* ******************************************************************** */


static void
add_name_ok (doublestring *dse, gpointer data)
{
  GtkWidget *wid;
  info1 *info;
  file_s *s;
  doublestring *ds;
  int row;
  gchar *names[2];
  subtable *stab;

  info = data;
  wid = info->namelist;
  s = info->s;
  stab = info->stab;

  trim (dse->name);
  trim (dse->locale);
  if (check_name (dse->name, 0) != 0)
    return;

  if (strlen (dse->name) == 0)
    return;

  if (stab == NULL)
    {
      g_warning ("found unexpected null pointer.\n");
      return;
    }

  ds = new_i18n ();
  ds->name = dse->name;
  ds->locale = dse->locale;
  names[0] = dse->name;
  names[1] = dse->locale;
  row = gtk_clist_append (GTK_CLIST (wid), names);
  gtk_clist_set_row_data (GTK_CLIST (wid), row, ds);

  stab->i18n_name = g_list_append (stab->i18n_name, ds);
  g_free (dse);

  s->changed = TRUE;

}


static void
add_name_clicked (GtkWidget * widget, info1 *info)
{
  popup_locale_dialog (add_name_ok, _ ("Question"),
		_ ("Enter Name field in other language"), NULL, NULL, 
				(gpointer)info);
}


static void
edit_name_ok (doublestring *dse, gpointer data)
{
  GtkWidget *wid;
  info1 *info;
  GList *loop;
  GList *selection;
  file_s *s;
  subtable *stab;
  doublestring *ds;
  GtkVisibility vis;
  doublestring *names;
  gchar *strd[2];

  int row;
  int row2;


  info = data;
  stab = info->stab;
  wid = info->namelist;
  s = info->s;


  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  ds = gtk_clist_get_row_data (GTK_CLIST (wid), row);

  if (ds->name != NULL)
    {
      g_free (ds->name);
      ds->name = NULL;
    }
  ds->name = dse->name;
  if (ds->locale != NULL)
    {
      g_free (ds->locale);
      ds->locale = NULL;
    }
  ds->locale = dse->locale;
  g_free (dse);

  gtk_clist_freeze (GTK_CLIST (wid));
  gtk_clist_clear (GTK_CLIST (wid));
  loop = stab->i18n_name;
  g_list_first (loop);
  while (loop != NULL)
    {
      names = loop->data;
      strd[0] = names->name;
      strd[1] = names->locale;
      row2 = gtk_clist_append (GTK_CLIST (wid), strd);
      gtk_clist_set_row_data (GTK_CLIST (wid), row2, names);
      loop = loop->next;
    }

  vis = gtk_clist_row_is_visible (GTK_CLIST (wid), row + 1);
  if (vis != GTK_VISIBILITY_FULL)
    gtk_clist_moveto (GTK_CLIST (wid), row, 0, 0, 0);

  gtk_clist_select_row (GTK_CLIST (wid), row, 0);
  gtk_clist_thaw (GTK_CLIST (wid));


  s->changed = TRUE;


}

static void
edit_name_clicked (GtkWidget * widget, info1 *info)
{
  GList *selection;
  GtkWidget *list;
  // subtable *stab;
  doublestring *ds;

  int row;
  list = info->namelist;

  // stab = gtk_object_get_user_data (GTK_OBJECT (list));
  
  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  ds = gtk_clist_get_row_data (GTK_CLIST (list), row);

  popup_locale_dialog (edit_name_ok, _ ("Question"),
		       _ ("Enter description"), ds->name, ds->locale, 
			(gpointer )info);

}



#ifdef GABY_KNOWS_DESCS
static void
edit_desc_ok (doublestring *dse, GtkWidget * wid)
{
  GList *loop;
  GList *selection;
  file_s *s;
  subtable *stab;
  doublestring *ds;
  doublestring *names;
  gchar *strd[2];
  GtkVisibility vis;
  int row;
  int row2;


  stab = gtk_object_get_user_data (GTK_OBJECT (wid));

  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    return;
  row = (int) selection->data;
  ds = gtk_clist_get_row_data (GTK_CLIST (wid), row);

  if (ds->name != NULL)
    {
      g_free (ds->name);
      ds->name = NULL;
    }
  ds->name = dse->name;
  if (ds->locale != NULL)
    {
      g_free (ds->locale);
      ds->locale = NULL;
    }
  ds->locale = dse->locale;
  g_free (dse);

  gtk_clist_freeze (GTK_CLIST (wid));

  gtk_clist_clear (GTK_CLIST (wid));
  loop = stab->i18n_desc;
  g_list_first (loop);
  while (loop != NULL)
    {
      names = loop->data;
      strd[0] = names->name;
      strd[1] = names->locale;
      row2 = gtk_clist_append (GTK_CLIST (wid), strd);
      gtk_clist_set_row_data (GTK_CLIST (wid), row2, names);
      loop = loop->next;
    }

  vis = gtk_clist_row_is_visible (GTK_CLIST (wid), row + 1);
  if (vis != GTK_VISIBILITY_FULL)
    gtk_clist_moveto (GTK_CLIST (wid), row, 0, 0, 0);

  gtk_clist_select_row (GTK_CLIST (wid), row, 0);
  gtk_clist_thaw (GTK_CLIST (wid));

  s = gtk_object_get_data (GTK_OBJECT (wid), "FILEINFO");
  s->changed = TRUE;


}



static void
edit_desc_clicked (GtkWidget * widget, GtkWidget * list)
{
  GList *selection;
  // uct subtalbe *stab;
  doublestring *ds;

  int row;

  // b = gtk_object_get_user_data (GTK_OBJECT (list));

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = (int) selection->data;
  ds = gtk_clist_get_row_data (GTK_CLIST (list), row);

  popup_locale_dialog (edit_desc_ok, _ ("Question"),
		       _ ("Enter description"), ds->name, ds->locale, list);
}

#endif


static void
delete_name_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  file_s *s;
  GList *selection;
  subtable *stab;
  doublestring *ds;

  int row;

  list = info->namelist;
  stab = info->stab;
  s = info->s;

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  ds = gtk_clist_get_row_data (GTK_CLIST (list), row);

  stab->i18n_name = g_list_remove (stab->i18n_name, ds);
  gtk_clist_remove (GTK_CLIST (list), row);

  delete_i18n (ds);

  /* g_free (ds->name);
     g_free (ds->locale);
     g_free (ds); */


  s->changed = TRUE;


}

#ifdef GABY_KNOWS_DESCS
static void
add_desc_ok (doublestring *dse, GtkWidget * wid)
{
  file_s *s;
  doublestring *ds;
  int row;
  gchar *names[2];
  subtable *stab;

  trim (dse->name);
  trim (dse->locale);
  if (check_name (dse->name, 0) != 0)
    return;

  if (strlen (dse->name) == 0)
    return;


  stab = gtk_object_get_user_data (GTK_OBJECT (wid));
  if (stab == NULL)
    {
      g_warning ("found unexpected null pointer.\n");
      return;
    }

  ds = new_i18n ();
  (ds)->name = dse->name;
  (ds)->locale = dse->locale;
  names[0] = dse->name;
  names[1] = dse->locale;
  row = gtk_clist_append (GTK_CLIST (wid), names);
  gtk_clist_set_row_data (GTK_CLIST (wid), row, ds);

  stab->i18n_desc = g_list_append (stab->i18n_desc, ds);
  g_free (dse);


  s = gtk_object_get_data (GTK_OBJECT (wid), "FILEINFO");
  s->changed = TRUE;


}

static void
add_desc_clicked (GtkWidget * widget, GtkWidget * list)
{
  popup_locale_dialog (add_desc_ok, _ ("Question"),
		       _ ("Enter description"), NULL, NULL, list);
}

static void
delete_desc_clicked (GtkWidget * widget, GtkWidget * list)
{

  file_s *s;
  GList *selection;
  subtable *stab;
  doublestring *ds;

  int row;

  stab = gtk_object_get_user_data (GTK_OBJECT (list));

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = (int) selection->data;
  ds = gtk_clist_get_row_data (GTK_CLIST (list), row);

  stab->i18n_desc = g_list_remove (stab->i18n_desc, ds);
  gtk_clist_remove (GTK_CLIST (list), row);

  delete_i18n (ds);
  /* g_free (ds->name);
     g_free (ds->locale);
     g_free (ds); */

  s = gtk_object_get_data (GTK_OBJECT (list), "FILEINFO");
  s->changed = TRUE;



}

#endif


static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{				/* Yess we want to close */
  return FALSE;
}

static void
destroy (GtkWidget * widget, info1 *info)
{
  info->stab->propwindow = NULL;
  g_free(info);
}


static void
close_win (GtkWidget * wid, info1 *info)
{
  gtk_widget_destroy (info->window);
}




static void
name_dclicked (GtkWidget * widget, GdkEventButton * event,
	       info1 *info)
{

  if (event->type == GDK_2BUTTON_PRESS)
    {
      edit_name_clicked (widget, info);
    }
}

#ifdef GABY_KNOWS_DESCS
static void
desc_dclicked (GtkWidget * widget, GdkEventButton * event,
	       GtkWidget * list)
{

  if (event->type == GDK_2BUTTON_PRESS)
    {
      edit_desc_clicked (widget, list);
    }
}
#endif


/* ******************************************************************** */
/* Create win                                                           */
/* ******************************************************************** */



void
  create_subtableprop_window
  (subtable *stab, file_s *s, gchar * caption)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *window;
  GtkWidget *addname;
  GtkWidget *delname;
  GtkWidget *hbox2;
  GtkWidget *i18nname;
  GtkWidget *namelist;
  GtkWidget *frame;
  GtkWidget *label;
  GtkWidget *closebut;
  GtkWidget *scroll;
  int key;
  gchar *titles[2];
  gchar buffer[1000];
  GList *loop;
  gint row;
  doublestring *names;
  gchar *strd[2];
  info1 *info;


  if (stab->propwindow != NULL)
    {
      gdk_window_raise (stab->propwindow->window);
      return;
    }

  info = g_malloc(sizeof(info1));

  strcpy (buffer, _ ("Subtable properties: "));
  strcat (buffer, caption);
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), buffer);

  accel = gtk_accel_group_new ();

  /* WM */
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_event), info);
  /* Destroy */
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (destroy), info);

  gtk_widget_show (window);

  gtk_object_set_user_data (GTK_OBJECT (window), stab);
  stab->propwindow = window;
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  hbox = gtk_hbox_new (FALSE, 0);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("I18N name"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);

  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  titles[0] = _ ("Name");
  titles[1] = _ ("Locale");
  i18nname = gtk_clist_new_with_titles (2, titles);
  namelist = i18nname;
  gtk_widget_set_usize (i18nname, 200, 100);
  gtk_widget_show (i18nname);
  gtk_signal_connect (GTK_OBJECT (i18nname), "button_press_event",
		      GTK_SIGNAL_FUNC (name_dclicked), info);
  gtk_clist_set_column_width (GTK_CLIST (i18nname), 0, 150);
  // gtk_object_set_user_data (GTK_OBJECT (i18nname), stab);
  // gtk_object_set_data (GTK_OBJECT (i18nname), "FILEINFO", s);
  gtk_clist_set_selection_mode (GTK_CLIST (i18nname), GTK_SELECTION_BROWSE);
  gtk_widget_show (vbox);
  scroll = scroll_new (i18nname);
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);
  loop = stab->i18n_name;
  g_list_first (loop);
  while (loop != NULL)
    {
      names = loop->data;
      strd[0] = names->name;
      strd[1] = names->locale;
      row = gtk_clist_append (GTK_CLIST (i18nname), strd);
      gtk_clist_set_row_data (GTK_CLIST (i18nname), row, names);
      loop = loop->next;
    }





  hbox2 = gtk_hbox_new (TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox2, FALSE, TRUE, 0);
  gtk_widget_show (hbox2);

  /* The buttons */
  addname = gtk_button_new_with_label (_ ("Add"));
  gtk_box_pack_start (GTK_BOX (hbox2), addname, FALSE, TRUE, 0);
  gtk_widget_show (addname);
  gtk_signal_connect (GTK_OBJECT (addname), "clicked",
		      GTK_SIGNAL_FUNC (add_name_clicked), info);

  addname = gtk_button_new_with_label (_ ("Edit"));
  gtk_box_pack_start (GTK_BOX (hbox2), addname, FALSE, TRUE, 0);
  gtk_widget_show (addname);
  gtk_signal_connect (GTK_OBJECT (addname), "clicked",
		      GTK_SIGNAL_FUNC (edit_name_clicked), info);

  delname = gtk_button_new_with_label (_ ("Delete"));
  gtk_box_pack_start (GTK_BOX (hbox2), delname, FALSE, TRUE, 0);
  gtk_widget_show (delname);
  gtk_signal_connect (GTK_OBJECT (delname), "clicked",
		      GTK_SIGNAL_FUNC (delete_name_clicked), info);

#ifdef GABY_KNOWS_DESCS
  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("I18N Description"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);
  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  titles[0] = _ ("Description");
  titles[1] = _ ("Locale");
  i18nname = gtk_clist_new_with_titles (2, titles);
  gtk_widget_set_usize (i18nname, 200, 100);
  gtk_widget_show (i18nname);
  gtk_signal_connect (GTK_OBJECT (i18nname), "button_press_event",
		      GTK_SIGNAL_FUNC (desc_dclicked), i18nname);
  gtk_clist_set_column_width (GTK_CLIST (i18nname), 0, 150);
  gtk_object_set_user_data (GTK_OBJECT (i18nname), stab);
  gtk_object_set_data (GTK_OBJECT (i18nname), "FILEINFO", s);
  gtk_widget_show (vbox);
  gtk_clist_set_selection_mode (GTK_CLIST (i18nname), GTK_SELECTION_BROWSE);
  scroll = scroll_new (i18nname);
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);
  loop = stab->i18n_desc;
  g_list_first (loop);
  while (loop != NULL)
    {
      names = loop->data;
      strd[0] = names->name;
      strd[1] = names->locale;
      row = gtk_clist_append (GTK_CLIST (i18nname), strd);
      gtk_clist_set_row_data (GTK_CLIST (i18nname), row, names);
      loop = loop->next;
    }



  hbox2 = gtk_hbox_new (TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox2, FALSE, TRUE, 0);
  gtk_widget_show (hbox2);

  /* The buttons */
  addname = gtk_button_new_with_label (_ ("Add"));
  gtk_box_pack_start (GTK_BOX (hbox2), addname, FALSE, TRUE, 0);
  gtk_widget_show (addname);

  gtk_signal_connect (GTK_OBJECT (addname), "clicked",
		      GTK_SIGNAL_FUNC (add_desc_clicked), i18nname);

  addname = gtk_button_new_with_label (_ ("Edit"));
  gtk_box_pack_start (GTK_BOX (hbox2), addname, FALSE, TRUE, 0);
  gtk_widget_show (addname);
  gtk_signal_connect (GTK_OBJECT (addname), "clicked",
		      GTK_SIGNAL_FUNC (edit_desc_clicked), i18nname);


  delname = gtk_button_new_with_label (_ ("Delete"));
  gtk_box_pack_start (GTK_BOX (hbox2), delname, FALSE, TRUE, 0);
  gtk_widget_show (delname);

  gtk_signal_connect (GTK_OBJECT (delname), "clicked",
		      GTK_SIGNAL_FUNC (delete_desc_clicked), i18nname);

#endif

  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("Misc"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, TRUE, 5);

  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  gtk_widget_show (vbox);

  closebut = gtk_button_new ();
  label = xpm_label_box (ja_xpm, _ ("_Close"), 1, &key);
  gtk_widget_show (label);
  gtk_container_add (GTK_CONTAINER (closebut), label);
  gtk_box_pack_end (GTK_BOX (vbox), closebut, FALSE, TRUE, 0);
  gtk_widget_show (closebut);
  gtk_signal_connect (GTK_OBJECT (closebut), "clicked",
		      GTK_SIGNAL_FUNC (close_win), info);
  gtk_widget_add_accelerator (closebut, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);


  gtk_container_add (GTK_CONTAINER (window), hbox);
  gtk_widget_show (hbox);
  /*gtk_accel_group_attach (accel, GTK_OBJECT (window));*/

  info->namelist = namelist;
  info->window = window;
  info->s = s;
  info->stab = stab;

}
