/*
   Generic file operation dialogs             

   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */

#include "main.h"


GtkWidget *save_as_dialog = NULL;
GtkWidget *load_dialog = NULL;
gpointer file_data;

void (*callback_func) () = NULL;
void (*callback_func_cancel) () = NULL;

gchar FILE_SAVE[1000];
gchar dnd_file[1000];
gchar *DEFAULT_FILE = "~/.gaby/";

gint
load_generic (gchar * name, gpointer * data)
{
  gint x;
  x = load_x (name, data);
  if (x == 0)
    set_file_changed (0, data);
  return x;
}

gint
save_generic (gchar * name, gpointer * data)
{
  gint x;
  x = save_x (name, data);
  if (x == 0)
    set_file_changed (0, data);
  return x;
}

gint
new_generic (gpointer * data)
{
  new_x (data);
  set_file_changed (0, data);
  return 0;
}


static void
save_file (char *naam)
{
  gint x;
  x = save_generic (naam, file_data);

  if (callback_func != NULL && x == 0)
    callback_func ();
}

void
load_file (char *naam)
{
  load_generic (naam, file_data);
  return;
}

/* ********************************************************************* */
/* Save as functions                                                     */
/* ********************************************************************* */

static void
overwrite_ja_clicked (gpointer data)
{
  save_file (FILE_SAVE);
}

static void
overwrite_nee_clicked (gpointer data)
{
  save_as_fileop (file_data);
}


static void
overwrite_cancel_clicked (gpointer data)
{
  if (callback_func_cancel != NULL)
	callback_func_cancel();
}


static void
overwrite_create (char *filename)
{
  char buffer[1000];
  strncpy (FILE_SAVE, filename, 1000);
  sprintf (buffer, _ ("File %s exists, overwrite ?"), filename);
  dialog (buffer, _ ("Warning !"), 3,
	  overwrite_ja_clicked,
	  overwrite_nee_clicked,
	  overwrite_cancel_clicked, NULL);

}

static void
save_as_ok_clicked (GtkWidget * widget, GtkFileSelection * fs)
{
  FILE *fp;
  char *file;

  file = gtk_file_selection_get_filename (GTK_FILE_SELECTION (save_as_dialog));
  fp = fopen (file, "r");
  if (fp != NULL)
    {
      overwrite_create (file);
      fclose (fp);
    }
  else
    {
      save_file (file);
    }
  gtk_widget_destroy (GTK_WIDGET (save_as_dialog));
  save_as_dialog = NULL;
}



static void
save_as_destroy (GtkWidget * widget, gpointer data)
{
  save_as_dialog = NULL;
}

static void
save_as_cancel (GtkWidget *wid, GtkWidget *window)
{

  gtk_widget_destroy(window);
  if (callback_func_cancel != NULL)
	callback_func_cancel();
}

void
save_as_fileop (gpointer data)
{
  gchar *name;

  /* just one window please */
  if (save_as_dialog != NULL)
    return;

  file_data = data;
  save_as_dialog = gtk_file_selection_new (_ ("Save"));

  gtk_signal_connect (GTK_OBJECT (save_as_dialog), "destroy",
		      (GtkSignalFunc) save_as_destroy, &save_as_dialog);
  /* Connect the ok_button to file_ok_sel function */
  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (save_as_dialog)->ok_button),
	     "clicked", (GtkSignalFunc) save_as_ok_clicked, save_as_dialog);

  /* Connect the cancel_button to destroy the widget */
  gtk_signal_connect(GTK_OBJECT (
		GTK_FILE_SELECTION (save_as_dialog)->cancel_button),
	        "clicked", (GtkSignalFunc) save_as_cancel, save_as_dialog);

  name = get_file_name (file_data);
  if (name != NULL)
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (save_as_dialog),
				     get_file_name (file_data));
  else
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (save_as_dialog),
				     DEFAULT_FILE);


  gtk_widget_show (save_as_dialog);
  return;
}

/* ********************************************************************* */
/* Save functions                                                        */
/* ********************************************************************* */

void
save_fileop (gpointer data)
{
  gchar *name;
  file_data = data;
  name = get_file_name (data);
  if (name == NULL)
    {
      save_as_fileop (file_data);
      return;
    }
  if (strcmp (name, "") == 0)
    save_as_fileop (file_data);
  else
    save_file (get_file_name (data));
}


/* ********************************************************************* */
/* Gewijzigd functions                                                   */
/* ********************************************************************* */



static void
gewijzigd_ja_clicked (gpointer data)
{
  save_fileop (file_data);
}

static void
gewijzigd_nee_clicked (gpointer data)
{
  if (callback_func != NULL)
    callback_func ();
}

static void
gewijzigd_cancel_clicked (gpointer data)
{
  callback_func = NULL;
	cancel_x(file_data);
}




void
gewijzigd_dialog_create (char *tekst, gpointer data)
{
  file_data = data;
  if (get_file_changed (file_data) == 1)
    {
      dialog (tekst, _ ("Warning !"), 3,
	      gewijzigd_ja_clicked,
	      gewijzigd_nee_clicked,
	      gewijzigd_cancel_clicked, NULL);
    }
  else
    {
      if (callback_func != NULL)
	callback_func ();
    }

}


/* ********************************************************************* */
/* Nieuw functions                                                       */
/* ********************************************************************* */

static void
new_database_do ()
{
  new_generic (file_data);
#ifdef DEBUG
  debug_print ("NEW\n");
#endif	/* DEBUG */
  callback_func = NULL;
}

void
new_fileop (gpointer data)
{
  file_data = data;
  callback_func = new_database_do;
  gewijzigd_dialog_create (_ ("The file has changed. Save before\ncreating a new one ?"), file_data);
}

/* ********************************************************************* */
/* Open Fuctions                                                         */
/* ********************************************************************* */



static void
load_file_ok_clicked (GtkWidget * w, GtkFileSelection * fs)
{
  char *file;
  file = gtk_file_selection_get_filename (GTK_FILE_SELECTION (load_dialog));
  load_file (file);
  gtk_widget_destroy (load_dialog);
  load_dialog = NULL;
}

static void
destroy_load_file (GtkWidget * widget, gpointer data)
{
  load_dialog = NULL;
}


static void
open_database_do ()
{
  if (load_dialog != NULL)
    return;

  load_dialog = gtk_file_selection_new (_ ("Open"));
  gtk_signal_connect (GTK_OBJECT (load_dialog), "destroy",
		      (GtkSignalFunc) destroy_load_file, &load_dialog);
  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (load_dialog)->ok_button),
	      "clicked", (GtkSignalFunc) load_file_ok_clicked, load_dialog);
  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (load_dialog)->cancel_button),
			     "clicked", (GtkSignalFunc) gtk_widget_destroy,
			     GTK_OBJECT (load_dialog));

  gtk_file_selection_set_filename (GTK_FILE_SELECTION (load_dialog),
				   DEFAULT_FILE);


  gtk_widget_show (load_dialog);
  callback_func = NULL;
}



void
open_fileop (gpointer data)
{
  file_data = data;
  callback_func = open_database_do;
  gewijzigd_dialog_create (_ ("The file has changed. Save before\nopening the new file ?"), file_data);
}

/* ********************************************************************* */
/* DND                                                                   */
/* ********************************************************************* */

static void
drop_open_database_do ()
{
  if (dnd_file == NULL)
    {
      printf ("Error did not receive any name.\n");
      return;
    }

  callback_func = NULL;
  load_file (dnd_file);
}


void
drop_open_fileop (gpointer data, gchar * name)
{
  char *n;
  if (name == NULL)
    {
      printf ("Error did not receive any name.\n");
      return;
    }
  if (strncmp (name, "file:", 5) != 0)
    {
      error_dialog (_ ("Cannot open non-local files"), _ ("Error"));
      return;
    }
  callback_func = drop_open_database_do;
  /* Cut of the 5 first chars that say file:X */
  n = name + 5;
  /* Remove the enter char at the end. */
  strncpy (dnd_file, n, 999);
  if (dnd_file[strlen (dnd_file) - 2] == 13)
    dnd_file[strlen (dnd_file) - 2] = '\0';
  /* The usual */
  gewijzigd_dialog_create (_ ("The file has changed. Save before\nopening the new file ?"), file_data);
}

/* ********************************************************************* */
/* Close Fuctions                                                        */
/* ********************************************************************* */

static void
definite_close ()
{
  stop_x (file_data);
}

static void
file_close ()
{
  close_x (file_data);
}



void
close_prog_fileop (gpointer data)
{
#ifdef DEBUG
  debug_print ("close_prog_fileop\n");
#endif
  file_data = data;
  callback_func = definite_close;
  gewijzigd_dialog_create (_ ("File has changed. Save ?"), file_data);
}

void
close_fileop (gpointer data)
{
#ifdef DEBUG
  debug_print ("close_fileop\n");
#endif	/* DEBUG */
  file_data = data;
  callback_func = file_close;
  gewijzigd_dialog_create (_ ("File had changed. Save ?"), file_data);

}



/* ********************************************************************* */
/* Save Your Self                                                        */
/* ********************************************************************* */

static void
file_sys_ok()
{
  close_x (file_data);

  debug_print("Interaction\n");
#ifdef USE_GNOME
  gnome_interaction_key_return ((int)file_data,FALSE);
#endif


  callback_func = NULL;
  callback_func_cancel = NULL;
}

static void
file_sys_cancel()
{

  debug_print("Interaction\n");
#ifdef USE_GNOME
  gnome_interaction_key_return ((int)file_data,FALSE);
#endif

  callback_func = NULL;
  callback_func_cancel = NULL;
}

void
sys_fileop(gpointer data)
{
#ifdef DEBUG
  debug_print ("sys_fileop\n");
#endif	/* DEBUG */
  file_data = data;
  callback_func = file_sys_ok;
  callback_func_cancel = file_sys_cancel;
  gewijzigd_dialog_create (_ ("File had changed. Save ?"), file_data);

}

