/*
   Location window

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Copyright (C) 2000  Frederic Peters

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */


#include "main.h"
#include "icons/ja.xpm"
#include "icons/edit.xpm"
#include "icons/stock_add.xpm"
#include "icons/stock_remove.xpm"

extern doublestring format_names[];

typedef struct _main_update info1;
typedef struct _edit_update info2;

struct _edit_update
  {
    GtkWidget *p_entry;		/* path        */
    GtkWidget *f_combo;		/* format      */
    GtkWidget *ro_check;	/* readonly    */
    GtkWidget *rr_spin;		/* reread      */
    GtkWidget *window;		/* main window */

    file_s *s;
    location *loc;
  };

struct _main_update
  {
    GtkWidget *list;		/* locations   */
    GtkWidget *combo;		/* table       */
    GtkWidget *window;	 	/* main window */

    file_s *s;
    tlocation *tloc;
  };

/* ********************************************************************* */
/* input win								 */
/* ********************************************************************* */


static void
ok_clicked(GtkWidget *widget, info2 *info)
{
  gtk_widget_destroy(info->window);
  update_all();
}
  	

static gboolean
edit_delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  return FALSE;
}

static void
edit_destroy (GtkWidget * widget, info2 *info)
{
  info->loc->window = NULL;
  remove_update(info->window);
  g_free(info);
}



static void
p_entry_changed (GtkWidget * widget, info2 *info)
{
  g_free(info->loc->path);
  info->loc->path = g_strdup(gtk_entry_get_text(GTK_ENTRY(info->p_entry)));
  info->s->changed = TRUE;
}


static void f_combo_changed (GtkWidget * widget, info2 *info)
{
	set_option(&info->loc->prop, "FORMAT",
			get_untranslated_string(
				gtk_entry_get_text(GTK_ENTRY(widget)),
				format_names));
	info->s->changed = TRUE;
}

static void
ro_check_clicked (GtkWidget * widget, info2 *info)
{
  if (GTK_TOGGLE_BUTTON(widget)->active )
  {
    set_option(&info->loc->prop,"READONLY","TRUE");
  }
  else
  {
    set_option(&info->loc->prop,"READONLY","FALSE");
  }
  info->s->changed = TRUE;
}


static void
rr_spin_changed (GtkWidget * widget, info2 *info)
{
  gchar buffer[100];
  gint time;
  time = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(info->rr_spin));
  sprintf(buffer,"%d",time);
  set_option(&info->loc->prop,"REREAD",buffer);
}

static void update_edit_window(GtkWidget *window, gpointer data)
{
	info2 *info;
	gchar *temp;
	GList *list_formats = NULL;
	int i=0;
	info = data;

	gtk_entry_set_text(GTK_ENTRY(info->p_entry),info->loc->path);

	gtk_signal_handler_block_by_func(GTK_OBJECT(GTK_COMBO(info->f_combo)->entry),
			f_combo_changed,info); 

	while ( format_names[i++].name ) {
		list_formats = g_list_append(list_formats, format_names[i-1].locale);
	}
	gtk_combo_set_popdown_strings(GTK_COMBO(info->f_combo), list_formats);
	g_list_free(list_formats);

	if (get_option(&info->loc->prop,"FORMAT") == NULL)
	{
		set_option(&info->loc->prop,"FORMAT", format_names[0].name);
	}

	gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(info->f_combo)->entry),
				get_translated_string(
					get_option(&info->loc->prop,"FORMAT"),
					format_names));

	gtk_signal_handler_unblock_by_func(
			GTK_OBJECT(GTK_COMBO(info->f_combo)->entry), f_combo_changed,info); 

	temp = get_option(&info->loc->prop,"READONLY");
	if (temp == NULL)
	{
		set_option(&info->loc->prop,"READONLY","FALSE");
		temp = "FALSE";
	}
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(info->ro_check),
			strcmp(temp,"FALSE")==0 ? FALSE:TRUE);

	temp = get_option(&info->loc->prop,"REREAD");
	if (temp == NULL)
	{
		set_option(&info->loc->prop,"REREAD","0");
		temp = "0";
	}
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(info->rr_spin),atoi(temp));

}



/* ********************************************************************* */
/* Create edit win							 */
/* ********************************************************************* */





static void 
create_edit_view(location *loc, file_s *s)
{
  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWidget *frame;
  GtkWidget *window;
  GtkWidget *entry;
  GtkWidget *button;
  GtkWidget *combo;
  GtkWidget *label;
  GtkWidget *readonly;
  GtkAdjustment *adj;
  GtkWidget *spin;
  static GtkAccelGroup *accel;
  info2 *info;
  int key;
 
  if (loc->window != NULL)
  {
    gdk_window_raise (loc->window->window);
    return;
  }

  accel = gtk_accel_group_new();

  info = g_malloc ( sizeof(info2));

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), _("Edit"));
  loc->window = window;
  
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		    GTK_SIGNAL_FUNC (edit_delete_event), info);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		    GTK_SIGNAL_FUNC (edit_destroy), info);


  gtk_widget_show (window);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  hbox = gtk_hbox_new(FALSE,5);
  gtk_container_add(GTK_CONTAINER(window),hbox);
  gtk_widget_show(hbox);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_label(GTK_FRAME(frame), _("location options"));
  gtk_box_pack_start(GTK_BOX(hbox),frame, TRUE, TRUE, 5);
  gtk_widget_show(frame);

  vbox = gtk_vbox_new(FALSE,5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add(GTK_CONTAINER(frame),vbox);
  gtk_widget_show(vbox);

  label = gtk_label_new(_("Path"));
  gtk_box_pack_start(GTK_BOX(vbox),label,FALSE,FALSE,5);
  gtk_widget_show(label);
  
  entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(vbox),entry,FALSE,FALSE,5);
  gtk_signal_connect (GTK_OBJECT (entry), 
		"changed", GTK_SIGNAL_FUNC (p_entry_changed), info);
  gtk_widget_show(entry);

  label = gtk_label_new(_("Format"));
  gtk_box_pack_start(GTK_BOX(vbox),label,FALSE,FALSE,5);
  gtk_widget_show(label);
 
  combo = gtk_combo_new();
  gtk_box_pack_start(GTK_BOX(vbox),combo,FALSE,TRUE,0);
  gtk_widget_show(combo);
  gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo)->entry), 
		"changed", GTK_SIGNAL_FUNC (f_combo_changed), info);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_label(GTK_FRAME(frame), _("Options"));
  gtk_box_pack_start(GTK_BOX(hbox),frame, TRUE, TRUE, 5);
  gtk_widget_show(frame);

  vbox = gtk_vbox_new(FALSE,5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add(GTK_CONTAINER(frame),vbox);
  gtk_widget_show(vbox);


  readonly = gtk_check_button_new_with_label(_("Read-only"));
  gtk_box_pack_start(GTK_BOX(vbox),readonly,FALSE,TRUE,0);
  gtk_signal_connect (GTK_OBJECT (readonly), 
		"clicked", GTK_SIGNAL_FUNC (ro_check_clicked), info);
  gtk_widget_show(readonly);


  label = gtk_label_new("Reread [minutes]");
  gtk_misc_set_alignment(GTK_MISC(label),0,0.5);
  gtk_box_pack_start(GTK_BOX(vbox),label,FALSE,TRUE,0);
  gtk_widget_show(label);

  adj = (GtkAdjustment *) gtk_adjustment_new(0.0,0.0,60.0,1.0,10.0,0.0);
  spin = gtk_spin_button_new(adj,0,0);
  gtk_spin_button_set_wrap (GTK_SPIN_BUTTON(spin),FALSE);
  gtk_box_pack_start(GTK_BOX(vbox),spin,FALSE,TRUE,0);
  gtk_widget_show(spin);
  gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
                             GTK_SIGNAL_FUNC (rr_spin_changed),
                             info);

  button = gtk_button_new();
  label = xpm_label_box (ja_xpm, _ ("_Close"), 1, &key);
  gtk_widget_show (label);
  gtk_container_add (GTK_CONTAINER (button), label);
  gtk_box_pack_end(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), 
		"clicked", GTK_SIGNAL_FUNC (ok_clicked), info);
  gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);


  /* The the info */
  info->p_entry = entry;
  info->f_combo = combo;
  info->ro_check = readonly;
  info->rr_spin = spin;
  info->window = window;
  info->loc = loc;
  info->s = s;


  
  /*gtk_accel_group_attach (accel, GTK_OBJECT (window));  */

  add_update(window, info, update_edit_window);
  update_widget(window);

}

/* ********************************************************************* */
/* Main misc win							 */
/* ********************************************************************* */

static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  return FALSE;
}


static void
combo_changed(GtkWidget *entry, info1 *info)
{
  gchar *temp;
  

  temp =  gtk_entry_get_text(GTK_ENTRY(entry));
  g_free(info->tloc->name_of_table);
  info->tloc->name_of_table = g_strdup(temp);
  info->s->changed = TRUE;
  update_all_e(info->tloc->window);

}



static void
destroy (GtkWidget * widget, info1 *info)
{
  file_s *s;

  if (info == NULL)
  {
     debug_print("ERror in destroy info\n");
  }

  s = info->s; 
  remove_update(widget);
  info->tloc->window = NULL;
  g_free(info);
}


static void
update_main(GtkWidget *window, gpointer data)
{
  GList *selection;
  info1 *info;
  table *tab;
  location *loc;
  GList *loop;
  GList *labels;
  gchar *temp;
  gint row;
  gint srow;

  info = data;

  selection = GTK_CLIST(info->list)->selection;
  if (selection != NULL)
    srow = GPOINTER_TO_INT(selection->data);
  else
    srow = 0;

  gtk_clist_clear(GTK_CLIST(info->list));
  
  loop = g_list_first(info->tloc->locations);
  while (loop!=NULL)
  {
    loc = loop->data;
    row = gtk_clist_append(GTK_CLIST(info->list),&loc->path);
    gtk_clist_set_row_data(GTK_CLIST(info->list),row,loc);
    loop = loop->next;
  }
  gtk_clist_select_row(GTK_CLIST(info->list),srow,0);
  
  gtk_signal_handler_block_by_func(GTK_OBJECT(GTK_COMBO(info->combo)->entry),
			combo_changed,info); 


  gtk_list_clear_items(GTK_LIST(GTK_COMBO(info->combo)->list),0,-1);
  
  labels = NULL;
  loop = info->s->tables;
  loop = g_list_first(loop);
  while (loop != NULL)
  {
    tab = loop->data;
    labels = g_list_append(labels,  tab->name);
    loop = loop->next;
  }
  
  labels = g_list_append(labels,"others");

  if (labels != NULL)
  {
    gtk_combo_set_popdown_strings(GTK_COMBO(info->combo),labels);
    g_list_free(labels);
  }
 

 
  if (info->tloc->name_of_table==NULL)
  {
    tab = g_list_first(info->s->tables)->data;
    info->tloc->name_of_table = g_malloc(strlen(tab->name)+1);
    strcpy(info->tloc->name_of_table,tab->name);
  }

  temp =  gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(info->combo)->entry));
  if (strcmp(temp,info->tloc->name_of_table)!=0)
    gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(info->combo)->entry),
			info->tloc->name_of_table);
  
  gtk_signal_handler_unblock_by_func(GTK_OBJECT(GTK_COMBO(info->combo)->entry),
			combo_changed,info); 

}


static void
add_clicked (GtkWidget * wid, info1 *info)
{
  location *loc ;
  tlocation *tloc;
  file_s *s;

  s =  info->s;
  tloc = info->tloc;

  loc = new_location();

  if (s  == NULL)
  {
	return;
  }
  
  tloc->locations = g_list_append(tloc->locations,loc);

  loc->path =  g_strdup("~/.gaby/");
  update_widget(info->window);
  s->changed = TRUE; 
}

static void
edit_clicked(GtkWidget *wid, info1 *info)
{
  GList *selection;
  location *loc;
  file_s *s;
  gint row;

  s = info->s; 
  selection = GTK_CLIST(info->list)->selection;
  if (selection==NULL)
	return;
  row = GPOINTER_TO_INT(selection->data);
  loc = gtk_clist_get_row_data(GTK_CLIST(info->list),row);
  create_edit_view(loc,s);
}

static void
del_clicked(GtkWidget *wid, info1 *info)
{
  GList *selection;
  location *loc;
  gint row;

 
  selection = GTK_CLIST(info->list)->selection;
  if (selection==NULL)
	return;
  row = GPOINTER_TO_INT(selection->data);
  loc = gtk_clist_get_row_data(GTK_CLIST(info->list),row);
  info->tloc->locations = g_list_remove(info->tloc->locations,loc);
  delete_location(loc);
  update_widget(info->window);
  info->s->changed = TRUE;  
}

static void
close_clicked(GtkWidget *wid, info1 *info)
{
  gtk_widget_destroy(info->window);
}


static void
clist_dclicked (GtkWidget * widget, GdkEventButton * event,
                info1 *info)
{
  if (event->type == GDK_2BUTTON_PRESS)
    {
      edit_clicked (widget, info);
    }
}

/* *********************************************************************** */
/* Create window                                                           */
/* *********************************************************************** */


void create_location_window(tlocation *tloc, gchar * caption, file_s *s)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *label;
  GtkWidget *frame;
  GtkWidget *combo;
  info1 *info;
  int key;
  static GtkAccelGroup *accel;

  GtkWidget *list;

  if (tloc == NULL)
    return;

  if (tloc->window != NULL)
  {
    gdk_window_raise (tloc->window->window);
    return;
  }

  info = g_malloc(sizeof(info1));

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), caption);
  tloc->window= window;

  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		    GTK_SIGNAL_FUNC (delete_event), info);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		    GTK_SIGNAL_FUNC (destroy), info);


  gtk_widget_show (window);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  accel = gtk_accel_group_new();

  hbox = gtk_hbox_new(FALSE,5);
  gtk_container_add(GTK_CONTAINER(window),hbox);
  gtk_widget_show(hbox);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_label(GTK_FRAME(frame),_("Locations"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);


  vbox = gtk_vbox_new(FALSE,5);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);
  gtk_widget_show(vbox);
  gtk_container_add(GTK_CONTAINER(frame),vbox);

  combo = gtk_combo_new();
  gtk_box_pack_start(GTK_BOX(vbox),combo,FALSE,TRUE,0);
  gtk_widget_show(combo);
  gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo)->entry), 
		"changed", GTK_SIGNAL_FUNC (combo_changed), info);
 

  list = gtk_clist_new(1);
  gtk_box_pack_start(GTK_BOX(vbox),list,TRUE,TRUE,5);
  gtk_clist_set_column_width(GTK_CLIST(list),0,150);
  gtk_clist_set_selection_mode (GTK_CLIST (list), GTK_SELECTION_BROWSE);
  gtk_widget_show(list);
  gtk_signal_connect (GTK_OBJECT (list), "button_press_event",
                      GTK_SIGNAL_FUNC (clist_dclicked), info);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_label(GTK_FRAME(frame),_("Options"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, TRUE, 5);

  vbox = gtk_vbox_new(FALSE,5);
  gtk_container_add(GTK_CONTAINER(frame),vbox);
  gtk_widget_show(vbox);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);

  button = gtk_button_new();
  label = xpm_label_box(add_xpm, _("_Add location"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_start(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (add_clicked), info);
  gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);




  button = gtk_button_new();
  label = xpm_label_box(remove_xpm, _("_Delete location"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_start(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (del_clicked), info);
   gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);



    
 
  button = gtk_button_new();
  label = xpm_label_box(edit_xpm, _("_Edit location"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_start(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (edit_clicked), info);
  gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);



  
  button = gtk_button_new();
  label = xpm_label_box(ja_xpm, _("_Close"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_end(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (close_clicked), info);
  gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);



  /* Set the info struct */  
  info->list = list;
  info->combo = combo;
  info->window = window;

  info->s = s;
  info->tloc = tloc;

  /* Add to the update list */
  add_update(window, info, update_main);
  /*gtk_accel_group_attach (accel, GTK_OBJECT (window));  */

  /* update the window */
  update_widget(window);
}
