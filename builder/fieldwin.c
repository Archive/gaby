/*
   Fields window

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */



#include "main.h"
#include "icons/ja.xpm"
#include "icons/stock_add.xpm"
#include "icons/stock_remove.xpm"
#include "icons/edit.xpm"
#include "icons/up.xpm"
#include "icons/down.xpm"


static GtkAccelGroup *accel;

typedef struct _main_update info1;

struct _main_update
  {
    GtkWidget *list;
    GtkWidget *entry;
    GtkWidget *window;
    table *tab;
    file_s *s;
  };

/* ******************************************************************** */
/* Events                                                               */
/* ******************************************************************** */

static void
down_field_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *wid;
  table *tab;
  field *fld;
  file_s *s;
  int row;
  int row2;
  int len;
  GList *selection;
  GList *loop;
  GtkVisibility vis;

  wid = info->list;
  tab = info->tab;
  s = info->s;

  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    {
      return;
    }
  else
    {
      row = GPOINTER_TO_INT(selection->data);
    }

  fld = gtk_clist_get_row_data (GTK_CLIST (wid), row);

  len = g_list_length (tab->fields);
  if (row >= len - 1)
    return;


  tab->fields = g_list_remove (tab->fields, fld);
  tab->fields = g_list_insert (tab->fields, fld, row + 1);

  gtk_clist_freeze (GTK_CLIST (wid));
  gtk_clist_clear (GTK_CLIST (wid));
  loop = tab->fields;
  g_list_first (loop);
  while (loop != NULL)
    {
      fld = loop->data;
      row2 = gtk_clist_append (GTK_CLIST (wid), &fld->name);
      gtk_clist_set_row_data (GTK_CLIST (wid), row2, fld);
      loop = loop->next;
    }
  gtk_clist_select_row (GTK_CLIST (wid), row + 1, 0);

  vis = gtk_clist_row_is_visible (GTK_CLIST (wid), row + 1);
  if (vis != GTK_VISIBILITY_FULL)
    gtk_clist_moveto (GTK_CLIST (wid), row + 1, 0, 1, 0);


  gtk_clist_thaw (GTK_CLIST (wid));
  s->changed = TRUE;


  update_all ();

}



static void
up_field_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *wid;
  table *tab;
  field *fld;
  file_s *s;
  int row;
  int row2;
  int len;
  GList *selection;
  GList *loop;
  GtkVisibility vis;

  wid = info->list;
  s = info->s;
  tab = info->tab;


  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    {
      return;
    }
  else
    {
      row = GPOINTER_TO_INT(selection->data);
    }

  if (row <= 0)
    return;
  fld = gtk_clist_get_row_data (GTK_CLIST (wid), row);

  len = g_list_length (tab->fields);

  tab->fields = g_list_remove (tab->fields, fld);
  tab->fields = g_list_insert (tab->fields, fld, row - 1);

  gtk_clist_freeze (GTK_CLIST (wid));
  gtk_clist_clear (GTK_CLIST (wid));
  loop = tab->fields;
  g_list_first (loop);
  while (loop != NULL)
    {
      fld = loop->data;
      row2 = gtk_clist_append (GTK_CLIST (wid), &fld->name);
      gtk_clist_set_row_data (GTK_CLIST (wid), row2, fld);
      loop = loop->next;
    }
  gtk_clist_select_row (GTK_CLIST (wid), row - 1, 0);

  vis = gtk_clist_row_is_visible (GTK_CLIST (wid), row + 1);
  if (vis != GTK_VISIBILITY_FULL)
    gtk_clist_moveto (GTK_CLIST (wid), row - 1, 0, 0, 0);


  gtk_clist_thaw (GTK_CLIST (wid));

  s->changed = TRUE;

  update_all ();

}







static void
rename_field_ok (gchar * name, gpointer data)
{
  info1 *info;
  GtkWidget *wid;
  table *tab;
  field *fld;
  file_s *s;
  int row;
  int row2;
  GList *selection;
  GList *loop;

  info =data;
  wid = info->list;
  s = info->s;
  tab = info->tab;

  trim (name);
  if (check_name (name, 0) != 0)
    return;
  if (strlen (name) == 0)
    return;

  if (is_field_uniq(name,tab->fields)==FALSE)
  {
    error_dialog(
        _("Cannot rename the field because the name is used already."),
        _("Warning"));
    return;
  }




  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    {
      row = -1;
    }
  else
    {
      row = GPOINTER_TO_INT(selection->data);
    }

  fld = gtk_clist_get_row_data (GTK_CLIST (wid), row);
  if (fld == NULL)
    return;



  if (fld->name != NULL)
    {

      rename_fields (s, tab->name, fld->name, name);	
  						/* rename all references in
						   the subfields to this
						 */
      g_free (fld->name);
      fld->name = NULL;
    }
  fld->name = name;

  gtk_clist_clear (GTK_CLIST (wid));
  loop = tab->fields;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      fld = loop->data;
      row2 = gtk_clist_append (GTK_CLIST (wid), &fld->name);
      gtk_clist_set_row_data (GTK_CLIST (wid), row2, fld);
      loop = loop->next;
    }

  s->changed = TRUE;


  gtk_clist_select_row (GTK_CLIST (wid), row, 0);
  /* ADD THE VISIBILITY CHECK */
  update_all ();
}



static void
rename_field_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  GList *selection;
  field *fld;
  int row;

  list = info->list;

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    {
      return;
    }
  else
    {
      row = GPOINTER_TO_INT(selection->data);
    }


  fld = gtk_clist_get_row_data (GTK_CLIST (list), row);
  popup_dialog (rename_field_ok, _ ("Question"),
		_ ("Enter Name of new field"), fld->name, (gpointer )info);
}





static void
add_field_ok (gchar * name, gpointer data)
{
  GtkWidget *wid;
  info1 *info;
  table *tab;
  field *fld;
  file_s *s;
  int row;
  GList *selection;

  info = data;
  wid = info->list;
  s = info->s;
  tab = info->tab;

  trim (name);
  if (check_name (name, 0) != 0)
    return;
  if (strlen (name) == 0)
    return;

  if (is_field_uniq(name,tab->fields)==FALSE)
  {
    error_dialog(
        _("Cannot add the field because the name is used already."),
        _("Warning"));
    return;
  }


  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    {
      row = -1;
    }
  else
    {
      row = GPOINTER_TO_INT(selection->data);
    }



  fld = new_field ();
  fld->name = name;

  if (row == -1)
    {
      row = gtk_clist_append (GTK_CLIST (wid), &name);
      tab->fields = g_list_append (tab->fields, fld);
    }
  else
    {
      row = gtk_clist_insert (GTK_CLIST (wid), row, &name);
      tab->fields = g_list_insert (tab->fields, fld, row);
    }

  s->changed = TRUE;


  gtk_clist_set_row_data (GTK_CLIST (wid), row, fld);

  update_all ();
}


static void
add_field_clicked (GtkWidget * widget, info1 *info)
{
  popup_dialog (add_field_ok, _ ("Question"),
		_ ("Enter Name of new field"), NULL,(gpointer )info);
}

static void
edit_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  GList *selection;
  file_s *s;
  field *fld;
  int row;

  list = info->list;
  s = info->s;

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  fld = gtk_clist_get_row_data (GTK_CLIST (list), row);
  s->changed = TRUE;


  create_fieldprop_window (fld, s, fld->name);

}



static void
field_dclicked (GtkWidget * widget, GdkEventButton * event,
		info1  *info)
{
  if (event->type == GDK_2BUTTON_PRESS)
    {
      edit_clicked (widget, info);
    }
}





static void
delete_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  GList *selection;
  table *tab;
  field *fld;
  int row;
  file_s *s;
  
  list = info->list;
  tab = info->tab;
  s = info->s;

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  fld = gtk_clist_get_row_data (GTK_CLIST (list), row);

  s->changed = TRUE;

  tab->fields = g_list_remove (tab->fields, fld);
  gtk_clist_remove (GTK_CLIST (list), row);

  delete_field (fld);
  update_all ();
}



static void
alias_changed (gchar *name, gpointer data)
{
  info1 *info;
  GtkWidget *entry;
  file_s *s;
  table *tab;
  table *tab2;
  gint t;
  GList *loop;  

  info = data;
  entry = info->entry;
  tab = info->tab;
  s = info->s;
  

  trim (name);
  if (check_name (name, 10) != 0)
    return;

  /* Check for duplicate aliases */
  t = 0;
  loop = g_list_first(s->tables);
  while (loop != NULL)
  {
    tab2 = loop->data;
    if  ( tab2->alias != NULL || strcmp(tab2->name , name)==0)
      {
	if ( strcmp(tab2->alias , name)==0 )
          t =1;
      }
    loop = loop->next;
  }

  if (t!=0)
    {
      error_dialog(_("Alias is used already."),_("Cannot use alias"));
      return;
    }



  if (tab->alias != NULL)
    g_free (tab->alias);
  tab->alias = name;
  gtk_label_set_text(GTK_LABEL(entry),name);

  s->changed = TRUE;

  update_all ();
}



static void
change_alias_clicked (GtkWidget * widget, info1 *info)
{
  gchar *text;
  text = info->tab->alias;
  popup_dialog (alias_changed, _ ("Change"),
		_ ("Enter alias of table"), text, (gpointer)info);
}




static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  return FALSE;
}

static void
destroy (GtkWidget * widget, info1 *info)
{
  info->tab->window = NULL;
  g_free(info);
}


static void
close_win (GtkWidget * wid, info1 *info)
{
  gtk_widget_destroy (info->window);
}


/* ******************************************************************** */
/* Create win                                                           */
/* ******************************************************************** */



void
create_field_window (table *tab, file_s *s, gchar * caption)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *hbox2;
  GtkWidget *fieldlist;
  GtkWidget *addfield;
  GtkWidget *delfield;
  GtkWidget *editfield;
  GtkWidget *window;
  GtkWidget *frame;
  GtkWidget *lab;
  GtkWidget *scroll;
  GtkWidget *alias;
  GtkWidget *button;
  gchar buffer[1000];
  GList *loop;
  field *fld;
  info1 *info;
  gint row;
  gint key;


  if (tab->window != NULL)
    {
      gdk_window_raise (tab->window->window);
      return;
    }

  info = g_malloc (sizeof(info1));

  strcpy (buffer, _ ("Table: "));
  strcat (buffer, caption);
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_policy(GTK_WINDOW(window),FALSE,TRUE,TRUE);
  gtk_window_set_title (GTK_WINDOW (window), buffer);

  accel = gtk_accel_group_new ();

  /* WM */
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_event), info);
  /* Destroy */
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (destroy), info);

  gtk_widget_show (window);


  tab->window = window;
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);
  hbox = gtk_hbox_new (FALSE, 5);

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 4);
  gtk_widget_show (vbox);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("Alias"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, TRUE, 0);

  hbox2 = gtk_hbox_new (FALSE, 5);
  gtk_container_add (GTK_CONTAINER (frame), hbox2);
  gtk_container_set_border_width (GTK_CONTAINER (hbox2), 5);
  gtk_widget_show (hbox2);

  alias = gtk_label_new ("");
  // gtk_entry_set_editable (GTK_ENTRY (alias), FALSE);
  gtk_box_pack_start (GTK_BOX (hbox2), alias, FALSE, TRUE, 0);
  gtk_widget_show (alias);
  if (tab->alias != NULL)
    gtk_label_set_text (GTK_LABEL (alias), tab->alias);

  button = gtk_button_new_with_label(_("Change"));
  gtk_box_pack_end(GTK_BOX(hbox2),button,FALSE,TRUE,5);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (change_alias_clicked), info);

 


  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("Fields"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (vbox), frame, TRUE, TRUE, 0);

  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);



  /* Clist */
  fieldlist = gtk_clist_new (1);
  gtk_widget_set_usize (fieldlist, 200, 100);
  gtk_widget_show (fieldlist);
  gtk_widget_show (vbox);
  gtk_clist_set_selection_mode (GTK_CLIST (fieldlist), GTK_SELECTION_BROWSE);
  gtk_signal_connect (GTK_OBJECT (fieldlist), "button_press_event",
		      GTK_SIGNAL_FUNC (field_dclicked), info);
  scroll = scroll_new (fieldlist);
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);

  /* button frame */
  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("Options"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, TRUE, 5);

  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);


  /* The buttons */
  addfield = gtk_button_new ();
  lab = xpm_label_box (add_xpm, _ ("_Insert field"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addfield), lab);
  gtk_box_pack_start (GTK_BOX (vbox), addfield, FALSE, TRUE, 0);
  gtk_widget_show (addfield);
  gtk_signal_connect (GTK_OBJECT (addfield), "clicked",
		      GTK_SIGNAL_FUNC (add_field_clicked), info);
  gtk_widget_add_accelerator (addfield, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);



  delfield = gtk_button_new ();
  lab = xpm_label_box (remove_xpm, _ ("_Delete field"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (delfield), lab);
  gtk_box_pack_start (GTK_BOX (vbox), delfield, FALSE, TRUE, 0);
  gtk_widget_show (delfield);
  gtk_signal_connect (GTK_OBJECT (delfield), "clicked",
		      GTK_SIGNAL_FUNC (delete_clicked), info);
  gtk_widget_add_accelerator (delfield, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);




  editfield = gtk_button_new ();
  lab = xpm_label_box (edit_xpm, _ ("_Rename field"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (editfield), lab);
  gtk_box_pack_start (GTK_BOX (vbox), editfield, FALSE, TRUE, 0);
  gtk_widget_show (editfield);
  gtk_signal_connect (GTK_OBJECT (editfield), "clicked",
		      GTK_SIGNAL_FUNC (rename_field_clicked), info);
  gtk_widget_add_accelerator (editfield, "clicked", accel, key, 
		GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);




  hbox2 = gtk_hbox_new (TRUE, 2);
  gtk_widget_show (hbox2);
  gtk_box_pack_start (GTK_BOX (vbox), hbox2, FALSE, TRUE, 0);

  editfield = gtk_button_new ();
  lab = xpm_label_box (up_xpm, _ ("Up"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (editfield), lab);
  gtk_box_pack_start (GTK_BOX (hbox2), editfield, FALSE, TRUE, 0);
  gtk_widget_show (editfield);
  gtk_signal_connect (GTK_OBJECT (editfield), "clicked",
		      GTK_SIGNAL_FUNC (up_field_clicked), info);


  editfield = gtk_button_new ();
  lab = xpm_label_box (down_xpm, _ ("Down"), 2, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (editfield), lab);
  gtk_box_pack_start (GTK_BOX (hbox2), editfield, FALSE, TRUE, 0);
  gtk_widget_show (editfield);
  gtk_signal_connect (GTK_OBJECT (editfield), "clicked",
		      GTK_SIGNAL_FUNC (down_field_clicked), info);


  editfield = gtk_button_new ();
  lab = xpm_label_box (edit_xpm, _ ("_Edit field"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (editfield), lab);
  gtk_box_pack_start (GTK_BOX (vbox), editfield, FALSE, TRUE, 0);
  gtk_widget_show (editfield);
  gtk_signal_connect (GTK_OBJECT (editfield), "clicked",
		      GTK_SIGNAL_FUNC (edit_clicked), info);
  gtk_widget_add_accelerator (editfield, "clicked", accel, key, 
		GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);




  editfield = gtk_button_new ();
  lab = xpm_label_box (ja_xpm, _ ("_Close"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (editfield), lab);
  gtk_box_pack_end (GTK_BOX (vbox), editfield, FALSE, TRUE, 0);
  gtk_widget_show (editfield);
  gtk_signal_connect (GTK_OBJECT (editfield), "clicked",
		      GTK_SIGNAL_FUNC (close_win), info);
  gtk_widget_add_accelerator (editfield, "clicked", accel, key, 
		GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);




  gtk_widget_show (vbox);

  gtk_container_add (GTK_CONTAINER (window), hbox);
  gtk_widget_show (hbox);

  loop = tab->fields;
  g_list_first (loop);
  while (loop != NULL)
    {
      fld = loop->data;
      row = gtk_clist_append (GTK_CLIST (fieldlist), &fld->name);
      gtk_clist_set_row_data (GTK_CLIST (fieldlist), row, fld);
      loop = loop->next;
    }
  /*gtk_accel_group_attach (accel, GTK_OBJECT (window));*/


  info->window = window;
  info->list =fieldlist;
  info->entry = alias;
  info->s = s;
  info->tab =tab;

}
