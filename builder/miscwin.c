/*
   Misc window

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */


#include "main.h"
#include "icons/ja.xpm"
#include "icons/edit.xpm"
#include "icons/stock_add.xpm"
#include "icons/stock_remove.xpm"


typedef struct _main_update info1;
typedef struct _edit_update info2;


struct _edit_update
  {
    GtkWidget *combo1;
    GtkWidget *combo2;
    GtkWidget *check1;
    GtkWidget *check2;
    GtkWidget *window;
    GtkWidget *list;
    file_s **s;
    misc_s *misc;
  };

struct _main_update
  {
    GtkWidget *list;
    GtkWidget *window;
    file_s **s;
  };

/* ********************************************************************* */
/* input win								 */
/* ********************************************************************* */


static void
main_clicked(GtkWidget *widget, info2 *info)
{
  misc_s *misc;

  GList *loop;

  misc = info->misc;
  loop = g_list_first((*info->s)->miscs);
  while (loop != NULL)
  {
    misc = loop->data;
    misc->main = FALSE;
    loop = loop->next;
  }


  if (GTK_TOGGLE_BUTTON(widget)->active )
  {
    gtk_widget_set_sensitive(info->check1,FALSE);
    info->misc->main = TRUE;
  }
  else
  {
    misc->main = FALSE;
  }
  update_all_e(widget);
  (*info->s)->changed = TRUE;
}


static void
visible_clicked(GtkWidget *widget, info2 *info)
{
  misc_s *misc;

  misc = info->misc; 

  if (GTK_TOGGLE_BUTTON(widget)->active )
  {
    misc->visible = TRUE;
  }
  else
  {
    misc->visible = FALSE;
  }
  (*info->s)->changed =TRUE;
}


static void
ok_clicked(GtkWidget *widget, info2 *info)
{
  gtk_widget_destroy(info->window);
}
  	

static gboolean
edit_delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  return FALSE;
}

static void
edit_destroy (GtkWidget * widget, info2 *info)
{
  misc_s *s;

  s = info->misc;
 
  if (info!=NULL)
  {
    remove_update(info->combo1);
    remove_update(info->combo2);
    remove_update(info->check1);
    remove_update(info->list);
    g_free(info);
  }
  else
  {
    debug_print("AAAAH CRITICAL i will crash within 2 clicks.\n");
    return;
  }
  update_all(); 
  s->window = NULL;
}

static void
combo1_changed(GtkWidget *widget, info2 *info)
{
  misc_s *misc;
  gchar *temp;


  misc = info->misc;

  if (misc == NULL)
    return;
  temp =  gtk_entry_get_text(GTK_ENTRY(widget));
  if (temp==NULL)
	return;
  g_free(misc->name_of_subtable); 
  misc->name_of_subtable = g_malloc(strlen(temp)+1);
  strcpy(misc->name_of_subtable,temp);
  update_all_e(info->combo1);
  (*info->s)->changed =TRUE;
}

static void
combo2_changed(GtkWidget *widget, info2 *info)
{
  misc_s *misc;
  gchar *temp;

  misc = info->misc; 

  if (misc == NULL)
  {
    return;
  }
  temp =  gtk_entry_get_text(GTK_ENTRY(widget));
  if (temp==NULL)
	return;

  reset_view_as((*info->s), misc->name_of_subtable, temp);

  g_free(misc->view_as); 
  misc->view_as = g_strdup(temp);
  (*info->s)->changed =TRUE;

  update_all_e(info->combo2);
  
}



static void
list_clicked (GtkWidget *widget,
		gint srow,
		gint column,
		GdkEventButton *event,
		info2 *info)
{
  GList *sel;
  GList *loop;
  misc_s *misc;
  gint row;
  doublestring *ds;
 
  info->misc->bind_to = free_i18n( info->misc->bind_to); 

  sel = GTK_CLIST(info->list)->selection;  
  loop = g_list_first(sel);
  while (loop != NULL)
  {
    row = GPOINTER_TO_INT(loop->data);
    misc = gtk_clist_get_row_data(GTK_CLIST(info->list),row);
    ds = new_i18n();
    ds->name = g_strdup (misc->name_of_subtable);
    ds->locale = g_strdup (misc->view_as);
    info->misc->bind_to = g_list_append(info->misc->bind_to,ds);
    loop = loop->next;
  }
}



static void
update_list (GtkWidget *widget, gpointer data)
{
  GList *loop;
  GList *loop2;
  info2 *info;
  misc_s *misc;
  gint row;
  gchar *d[3];
  doublestring *ds;
  
  info=data;


  gtk_clist_clear(GTK_CLIST(info->list));

  gtk_signal_handler_block_by_func (GTK_OBJECT(info->list), list_clicked,
              info);

  loop = g_list_first((*info->s)->miscs);
  while (loop != NULL)
  {
     misc = loop->data;
     d[0]=misc->name_of_subtable;
     d[1]=misc->view_as;
     row = gtk_clist_append(GTK_CLIST(info->list), d);
     gtk_clist_set_row_data(GTK_CLIST(info->list), row, misc);

     loop2 = g_list_first(info->misc->bind_to);
     while (loop2 != NULL)
     {
       ds = loop2->data;
       if (strcmp(misc->name_of_subtable,ds->name)==0 &&
	   strcasecmp(misc->view_as,ds->locale)==0)
       {
	 gtk_clist_select_row(GTK_CLIST(info->list), row, 0);
       }
       loop2 = loop2->next;
     }
     loop = loop->next;
  } 

  gtk_signal_handler_unblock_by_func (GTK_OBJECT(info->list), list_clicked,
              info);

}





static void
update_check_buttons(GtkWidget *button, gpointer data)
{
  info2 *info;
  info = data;
  
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(info->check1),
		info->misc->main );
  if (info->misc->main == TRUE)
    gtk_widget_set_sensitive(info->check1,FALSE);
  else
    gtk_widget_set_sensitive(info->check1,TRUE);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(info->check2),
		info->misc->visible );
}

 

static void
update_combo1(GtkWidget *combo1, gpointer data)
{
  info2 *info;
  file_s *s;
  misc_s *misc;
  subtable *stab=NULL;
  GList *labels;
  GList *loop;
  gchar *temp;

  info = data;
  s = (*info->s);
  misc = info->misc;

  if (s->subtables == NULL)
  {
    return;
  }

  gtk_signal_handler_block_by_func(GTK_OBJECT(GTK_COMBO(info->combo1)->entry),
			combo1_changed,info); 


  gtk_list_clear_items(GTK_LIST(GTK_COMBO(info->combo1)->list),0,-1);
  
  labels = NULL;
  loop = s->subtables;
  loop = g_list_first(loop);
  while (loop != NULL)
  {
    stab = loop->data;
    labels = g_list_append(labels,  stab->name);
    loop = loop->next;
  }
  if (labels != NULL)
  {
    gtk_combo_set_popdown_strings(GTK_COMBO(info->combo1),labels);
    g_list_free(labels);
  }
 

 
  if (misc->name_of_subtable==NULL)
  {
    stab = g_list_first(s->subtables)->data;
    misc->name_of_subtable = g_malloc(strlen(stab->name)+1);
    strcpy(misc->name_of_subtable,stab->name);
  }

  temp =  gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(info->combo1)->entry));
  if (strcmp(temp,misc->name_of_subtable)!=0)
    gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(info->combo1)->entry),
			misc->name_of_subtable);
  
  gtk_signal_handler_unblock_by_func(GTK_OBJECT(GTK_COMBO(info->combo1)->entry),
			combo1_changed,info); 


} 


static void
update_combo2(GtkWidget *window, gpointer data)
{
  info2 *info;
  file_s *s;
  misc_s *misc;
  subtable *stab=NULL;
  GList *loop;
  GList *loop2;
  gchar *temp;
  gboolean found;


  info = data;
  s = (*info->s);
  misc = info->misc;

  gtk_signal_handler_block_by_func(GTK_OBJECT(GTK_COMBO(info->combo2)->entry),
			combo2_changed,info); 

  loop = g_list_first(s->subtables);
  while (loop != NULL)
  {
    stab = loop->data;
    if (strcmp(stab->name,misc->name_of_subtable)==0)
    {
      gtk_list_clear_items(GTK_LIST(GTK_COMBO(info->combo2)->list),0,-1);
      if (g_list_length(stab->viewable_as) != 0)
        gtk_combo_set_popdown_strings(GTK_COMBO(info->combo2),
			g_list_first(stab->viewable_as));
      /* See if the view as is still in the list */
      found = FALSE;
      loop2 = g_list_first(stab->viewable_as);
      while ( loop2 != NULL && misc->view_as!=NULL)
      {
        if ( strcmp(loop2->data,misc->view_as)==0)
		found = TRUE;
        loop2 = loop2->next;
      }
      if (found ==FALSE)
      {
	g_free(misc->view_as);
        misc->view_as = NULL;
      }

      if (stab->viewable_as == NULL)
       {
         debug_print("AHHH viewable_as == NULL \n");
	 return;
       }

      if (misc->view_as==NULL)
        {
          temp = g_list_first(stab->viewable_as)->data;
          misc->view_as = g_malloc(strlen(temp)+1);
          strcpy(misc->view_as,temp);
        }
    }
    loop = loop->next;
  }
  temp =  gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(info->combo2)->entry));
  if (strcmp(temp,misc->view_as)!=0)
    gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(info->combo2)->entry),
			misc->view_as);

  gtk_signal_handler_unblock_by_func(GTK_OBJECT(GTK_COMBO(info->combo2)->entry),
			combo2_changed,info); 


} 

/* ********************************************************************* */
/* Create edit win							 */
/* ********************************************************************* */




static void 
create_edit_view(misc_s *misc, file_s **s)
{
  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWidget *frame;
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *combo;
  GtkWidget *label;
  GtkWidget *list;
  static GtkAccelGroup *accel;
  info2 *info;
  int key;
 
 
  if (misc->window != NULL)
  {
    gdk_window_raise (misc->window->window);
    return;
  }

  info = g_malloc (sizeof(info2));

  accel = gtk_accel_group_new();

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), _("Edit"));
  misc->window = window;
  
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		    GTK_SIGNAL_FUNC (edit_delete_event), info);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		    GTK_SIGNAL_FUNC (edit_destroy), info);


  gtk_widget_show (window);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  hbox = gtk_hbox_new(FALSE,5);
  gtk_container_add(GTK_CONTAINER(window),hbox);
  gtk_widget_show(hbox);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_label(GTK_FRAME(frame), _("View options"));
  gtk_box_pack_start(GTK_BOX(hbox),frame, TRUE, TRUE, 5);
  gtk_widget_show(frame);

  vbox = gtk_vbox_new(FALSE,5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add(GTK_CONTAINER(frame),vbox);
  gtk_widget_show(vbox);

  label = gtk_label_new(_("Subtable"));
  gtk_box_pack_start(GTK_BOX(vbox),label,TRUE,FALSE,5);
  gtk_widget_show(label);
  

  combo = gtk_combo_new();
  gtk_box_pack_start(GTK_BOX(vbox),combo,FALSE,TRUE,0);
  gtk_widget_show(combo);
  gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo)->entry), 
		"changed", GTK_SIGNAL_FUNC (combo1_changed), info);
  info->combo1 = combo;

  label = gtk_label_new(_("View as"));
  gtk_box_pack_start(GTK_BOX(vbox),label,TRUE,FALSE,5);
  gtk_widget_show(label);
 
  combo = gtk_combo_new();
  gtk_box_pack_start(GTK_BOX(vbox),combo,FALSE,TRUE,0);
  gtk_widget_show(combo);
  gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo)->entry), 
		"changed", GTK_SIGNAL_FUNC (combo2_changed), info);

  info->window = window;
  info->combo2 = combo;
  info->misc = misc;
  info->s = s;

  /* Frame 2 */


  frame = gtk_frame_new(NULL);
  gtk_frame_set_label(GTK_FRAME(frame), _("Bind window to (use with care)"));
  gtk_box_pack_start(GTK_BOX(hbox),frame, TRUE, TRUE, 5);
  gtk_widget_show(frame);

  vbox = gtk_vbox_new(FALSE,5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add(GTK_CONTAINER(frame),vbox);
  gtk_widget_show(vbox);

  list = gtk_clist_new(2);
  info->list = list;
  gtk_clist_set_selection_mode(GTK_CLIST(list),GTK_SELECTION_MULTIPLE);
  gtk_box_pack_start(GTK_BOX(vbox),list,TRUE,TRUE,0);
  gtk_widget_show(list);
  gtk_signal_connect (GTK_OBJECT (info->list), 
		"select_row", GTK_SIGNAL_FUNC (list_clicked), info);
  gtk_signal_connect (GTK_OBJECT (info->list), 
		"unselect_row", GTK_SIGNAL_FUNC (list_clicked), info);
  // gtk_clist_set_column_width(GTK_CLIST(list),0,100);
  gtk_clist_set_column_title(GTK_CLIST(list),0,_("Subtable"));
  gtk_clist_set_column_title(GTK_CLIST(list),1,_("View as"));
  gtk_clist_column_titles_show(GTK_CLIST(list));

  /* Frame 3 */

  frame = gtk_frame_new(NULL);
  gtk_frame_set_label(GTK_FRAME(frame), _("Options"));
  gtk_box_pack_start(GTK_BOX(hbox),frame, FALSE, TRUE, 5);
  gtk_widget_show(frame);

  vbox = gtk_vbox_new(FALSE,5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add(GTK_CONTAINER(frame),vbox);
  gtk_widget_show(vbox);


  button = gtk_check_button_new_with_label(_("Main view"));
  gtk_box_pack_start(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_signal_connect (GTK_OBJECT (button), 
		"clicked", GTK_SIGNAL_FUNC (main_clicked), info);
  gtk_widget_show(button);

  info->check1 = button;

  button = gtk_check_button_new_with_label(_("Visible"));
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),TRUE);
  gtk_box_pack_start(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_signal_connect (GTK_OBJECT (button), 
		"clicked", GTK_SIGNAL_FUNC (visible_clicked), info);
  gtk_widget_show(button);


  info->check2 = button;


  button = gtk_button_new();
  label = xpm_label_box (ja_xpm, _ ("_Close"), 1, &key);
  gtk_widget_show (label);
  gtk_container_add (GTK_CONTAINER (button), label);
  gtk_box_pack_end(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), 
		"clicked", GTK_SIGNAL_FUNC (ok_clicked), info);
  gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);




  
  /*gtk_accel_group_attach (accel, GTK_OBJECT (window)); */
  add_update(info->list, info, update_list); 
  add_update(info->combo1, info, update_combo1);
  add_update(info->combo2, info, update_combo2);
  add_update(info->check1, info, update_check_buttons);
  update_widget(info->combo1);
  update_widget(info->combo2);
  update_widget(info->check1);
  update_widget(info->list);

}

/* ********************************************************************* */
/* Main misc win							 */
/* ********************************************************************* */

#ifndef NOTEBOOKVIEW
static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  return FALSE;
}

static void
destroy (GtkWidget * widget, info1 *info)
{
  file_s *s;
  s = (*info->s);
  remove_update(widget);
  g_free(info);
  s->miscwin = NULL;
}

#endif


static void
update_main(GtkWidget *window, gpointer data)
{
  info1 *info;
  misc_s *misc;
  GList *loop;
  gchar *list[5];
  int row;
  info = data;

  gtk_clist_clear(GTK_CLIST(info->list));
  
  loop = g_list_first((*info->s)->miscs);
  while (loop!=NULL)
  {
    misc = loop->data;
    list[0] = misc->name_of_subtable;
    list[1] = misc->view_as;
    if (misc->main == TRUE)
      list[2] = _("Yes");
    else
      list[2] = _("No");
    if (misc->visible == TRUE)
      list[3] = _("Yes");
    else
      list[3] = _("No");
    row = gtk_clist_append(GTK_CLIST(info->list),list);
    gtk_clist_set_row_data(GTK_CLIST(info->list),row,misc);
    loop = loop->next;
  }

}


static void
add_clicked (GtkWidget * wid, info1 *info)
{
  GtkWidget *list;
  misc_s *misc;
  file_s *s;
  subtable *stab;


  list = info->list;
  s = (*info->s);

  if (g_list_length(s->subtables)==0)
  {
    error_dialog(_("No subtables defined, please define one first"),
			_("Warning"));
    return;
  }

 
  misc = new_misc_s();

  if (s == NULL)
  {
	return;
  }
  
  if (g_list_length(s->miscs) == 0 )
    misc->main = TRUE;

  s->miscs = g_list_append(s->miscs,misc);

  stab = g_list_first(s->subtables)->data;  

  misc->name_of_subtable =  g_strdup(stab->name);
  if (g_list_length(stab->viewable_as)>0)
    misc->view_as = g_strdup ( g_list_first(stab->viewable_as)->data);
 update_widget(info->window);
  s->changed = TRUE;  
}

static void
edit_clicked(GtkWidget *wid, info1 *info)
{
  GList *selection;
  GtkWidget *list;
  misc_s *misc;
  file_s *s;
  gint row;
  
  list = info->list;
  s = (*info->s);

  selection = GTK_CLIST(list)->selection;
  if (selection==NULL)
	return;
  row = GPOINTER_TO_INT(selection->data);
  misc = gtk_clist_get_row_data(GTK_CLIST(list),row);
  create_edit_view(misc,info->s);

}

static void
del_clicked(GtkWidget *wid, info1 *info)
{
  GList *selection;
  GtkWidget *list;
  GtkWidget *window;
  misc_s *misc;
  file_s *s;
  gint row;
  list = info->list;
  s = (*info->s); 
  window = info->window;

  selection = GTK_CLIST(list)->selection;
  if (selection==NULL)
	return;
  row = GPOINTER_TO_INT(selection->data);
  misc = gtk_clist_get_row_data(GTK_CLIST(list),row);



  delete_view_as((*info->s), misc->name_of_subtable, misc->view_as);

  s->miscs = g_list_remove(s->miscs,misc);
  delete_misc_s(misc);
  // update_widget(window);
  update_all();
  s->changed = TRUE;  

}

#ifndef NOTEBOOKVIEW
static void
close_clicked(GtkWidget *wid, info1 *info)
{
  gtk_widget_destroy(info->window);
}
#endif

static void
clist_dclicked (GtkWidget * widget, GdkEventButton * event,
                info1 *info)
{
  if (event->type == GDK_2BUTTON_PRESS)
    {
      edit_clicked (widget, info);
    }
}

/* *********************************************************************** */
/* Create window                                                           */
/* *********************************************************************** */


GtkWidget *
create_misc_window(gchar * caption, file_s **s)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *label;
  GtkWidget *frame;
  info1 *info;
  int key;
  static GtkAccelGroup *accel;

  GtkWidget *list;

  if ((*s)->miscwin != NULL)
  {
    gdk_window_raise ((*s)->miscwin->window);
    return (*s)->miscwin;
  }

  info = g_malloc ( sizeof(info1));

#ifndef NOTEBOOKVIEW
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), caption);
  (*s)->miscwin = window;

  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		    GTK_SIGNAL_FUNC (delete_event), info);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		    GTK_SIGNAL_FUNC (destroy), info);
#else
  window = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(window),GTK_SHADOW_NONE);
#endif


  gtk_widget_show (window);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  accel = gtk_accel_group_new();

  hbox = gtk_hbox_new(FALSE,5);
  gtk_container_add(GTK_CONTAINER(window),hbox);
  gtk_widget_show(hbox);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_label(GTK_FRAME(frame),_("Views"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);


  vbox = gtk_vbox_new(TRUE,0);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);
  gtk_widget_show(vbox);
  gtk_container_add(GTK_CONTAINER(frame),vbox);

  list = gtk_clist_new(4);
  gtk_clist_set_column_title(GTK_CLIST(list),0,"Subtable");
  gtk_clist_set_column_title(GTK_CLIST(list),1,"View as");
  gtk_clist_set_column_title(GTK_CLIST(list),2,"Main");
  gtk_clist_set_column_title(GTK_CLIST(list),3,"Visible");
  gtk_clist_column_titles_show(GTK_CLIST(list));
  gtk_box_pack_start(GTK_BOX(vbox),list,TRUE,TRUE,0);
  gtk_clist_set_column_width(GTK_CLIST(list),0,150);
  gtk_clist_set_selection_mode (GTK_CLIST (list), GTK_SELECTION_BROWSE);
  gtk_widget_show(list);
  gtk_signal_connect (GTK_OBJECT (list), "button_press_event",
                      GTK_SIGNAL_FUNC (clist_dclicked), info);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_label(GTK_FRAME(frame),_("Options"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, TRUE, 5);

  vbox = gtk_vbox_new(FALSE,5);
  // gtk_box_pack_start(GTK_BOX(vbox),hbox,FALSE,TRUE,0);
  gtk_container_add(GTK_CONTAINER(frame),vbox);
  gtk_widget_show(vbox);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);

  button = gtk_button_new();
  label = xpm_label_box(add_xpm, _("_Add View"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_start(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (add_clicked), info);
  gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);




  button = gtk_button_new();
  label = xpm_label_box(remove_xpm, _("_Delete View"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_start(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (del_clicked), info);
   gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);



    
 
  button = gtk_button_new();
  label = xpm_label_box(edit_xpm, _("_Edit View"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_start(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (edit_clicked), info);
  gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);



#ifndef NOTEBOOKVIEW  
  button = gtk_button_new();
  label = xpm_label_box(ja_xpm, _("_Close"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_end(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (close_clicked), info);
   gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);
#endif



  info->list = list;
  info->s = s;
  info->window = window;

  add_update(window, info, update_main);
  /*gtk_accel_group_attach (accel, GTK_OBJECT (window));  */
  update_widget(window);

  return window;
}
