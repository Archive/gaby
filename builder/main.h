/*
   Main routines.    

   Gaby Description Builder
   Copyright (C) 1999  Ron Bessems
   Copyright (C) 2000  Frederic Peters

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */

#include "../config.h"

/* FIXME: GTK_ENABLE_BROKEN defined for GtkText */
#define GTK_ENABLE_BROKEN

#ifdef USE_GNOME
#  include <gnome.h>
#else
#  include <gtk/gtk.h>	
#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#ifdef HAVE_STRING_H
#  include <string.h>
#endif

#ifdef HAVE_UNISTD_h
#  include <unistd.h>
#endif

#ifdef ENABLE_NLS
#  ifdef HAVE_LOCALE_H
#    include <locale.h>
#  endif
#  ifdef HAVE_GETTEXT
#    include <libintl.h>
#  else
#    include <intl/libintl.h>
#  endif
#  define _(String) gettext (String)
#  define gettext_noop(String) (String)
#  define N_(String) (String)
#else /* ! ENABLE_NLS */
#  define _(String) (String)
#  define N_(String) (String)
#endif /* ! ENABLE_NLS */

#define BVERSION "1.0 (for Gaby " ## VERSION ## ")"

/* this is for debugging, if you really don't want any useless data you should
 * #define debug_print(format, args...)		;
 */
#ifdef __GNUC__
#  define debug_print(format, args...) if (debug_mode) \
						fprintf(stderr, format, ##args)
#else
#  define debug_print			 printf
#endif

#ifndef DEBUG_MODE_DECLARED
extern int debug_mode;
#endif

#include "type.h"

#include "shared.h"
#include "tablewin.h"
#include "previewwin.h"
#include "shared.h"
#include "desc.h"
#include "fileops.h"
#include "subtable.h"
#include "actions.h"
#include "locations.h"
#include "miscwin.h"
#include "dataops.h"
#include "actions.h"
#include "buttonwin.h"
#include "fieldprop.h"
#include "fieldwin.h"
#include "subfieldprop.h"
#include "subfieldwin.h"
#include "subtable.h"
#include "subtableprop.h"
#include "locationwin.h"
#include "sessionman.h"

GList *multimedia_type;
GtkWidget *Window;

void add_update     (GtkWidget * widget, gpointer data,
		       void (*update_func) (GtkWidget * widget, gpointer data));
void remove_update  (GtkWidget * widget);
void update_widget  (GtkWidget * widget);
void update_all     ();
void update_all_e   (GtkWidget *e);

#define NOTEBOOKVIEW

