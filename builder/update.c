/*
   Window Update routines

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


   We get an update in version 0.7 (or somewhat later :)

 */


#include "main.h"

struct update
  {
    GtkWidget *widget;
    gpointer data;
    void (*update_func) (GtkWidget * widget, gpointer data);
  };

static GList *update_list = NULL;

void 
add_update (GtkWidget * widget, gpointer data,
	    void (*update_func) (GtkWidget * widget, gpointer data))
{
  struct update *ud;
#ifdef DEBUG_GABY
  debug_print ("Add update\n");
  debug_print ("Number of items in list after addition : %d\n",
	   g_list_length (update_list) + 1);
#endif
  ud = g_malloc (sizeof (struct update));
  ud->widget = widget;
  ud->data = data;
  ud->update_func = update_func;
  update_list = g_list_append (update_list, ud);

}

void
remove_update (GtkWidget * widget)
{
  GList *ul = NULL;
  struct update *ud;
#ifdef DEBUG_GABY
  debug_print ("Remove update\n");
#endif
  ul = update_list;
  ul = g_list_first (ul);
  while (ul != NULL)
    {
      ud = ul->data;
      if (ud->widget == widget)
	{
#ifdef DEBUG_GABY
	  debug_print ("Remove !\n");
#endif
	  update_list = g_list_remove (update_list, ud);
	  ul = g_list_first (update_list);
	  g_free (ud);
	}
      if (ul != NULL)
	ul = ul->next;
    }
}



void 
update_widget (GtkWidget * widget)
{
  GList *ul = NULL;
  struct update *ud;
#ifdef DEBUG_GABY
  debug_print ("Widget update\n");
#endif
  ul = update_list;
  ul = g_list_first (ul);
  while (ul != NULL)
    {
      ud = ul->data;
      if (ud->widget == widget)
	{
#ifdef DEBUG_GABY
	  debug_print ("Callback !\n");
#endif
	  ud->update_func (ud->widget, ud->data);
	}
      ul = ul->next;
    }
}


void 
update_all ()
{
  GList *ul = NULL;
  struct update *ud;
#ifdef DEBUG_GABY
  debug_print ("Update all\n");
#endif	/* DEBUG_GABY */
  ul = update_list;
  ul = g_list_first (ul);
  while (ul != NULL)
    {
      ud = ul->data;
      ud->update_func (ud->widget, ud->data);
      ul = ul->next;
    }
}

/* Update all widget expect for e  Needed to avoid endless recursive 
   updates */
void 
update_all_e (GtkWidget *e)
{
  GList *ul = NULL;
  struct update *ud;
#ifdef DEBUG_GABY
  debug_print ("Update all\n");
#endif	/* DEBUG_GABY */
  ul = update_list;
  ul = g_list_first (ul);
  while (ul != NULL)
    {
      ud = ul->data;
      if (ud->widget != e)
        ud->update_func (ud->widget, ud->data);
      ul = ul->next;
    }
}

