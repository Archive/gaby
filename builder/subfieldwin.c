/*
   Sub Fields window

   Gaby Databases Builder
   Copyright (C) 1999  Ron Bessems
   Copyright (C) 2000  Frederic Peters

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This window works, but there are some things i don't yet like...
   Ron


 */


#include "main.h"
#include "icons/ja.xpm"
#include "icons/edit.xpm"
#include "icons/up.xpm"
#include "icons/down.xpm"
#include "icons/stock_first.xpm"
#include "icons/stock_last.xpm"
#include "icons/stock_right_arrow.xpm"
#include "icons/stock_left_arrow.xpm"

#ifdef DEBUG_GABY
#define DEBUG
#endif

extern doublestring view_names[];

static GtkAccelGroup *accel;


typedef struct _update_list info1;


struct _update_list
  {
    GtkWidget *window;
    GtkWidget *fieldlist;
    GtkWidget *subfieldlist;
    GtkWidget *combo;
    file_s *s;
    subtable *stab; 
    table *tab;
  };


/* *********************************************************************** */
/* Events								   */
/* *********************************************************************** */


static void
remove_all_clicked (GtkWidget * widget, info1 *info)
{
  subtable *stab;
  GtkWidget *window;
  stab = info->stab;
  window = info->window;

  if (stab == NULL)
    {
      debug_print ("Warning unexpected NULL pointer\n");
      return;
    }
  stab->subfields = free_subfields (stab->subfields);
  update_widget (window); 
  info->s->changed =TRUE;
}



static void
add_all_clicked (GtkWidget * widget, info1 *info)
{
  subtable *stab;
  table *table;
  field *fld;
  subfield *sfld;
  GtkWidget *window;
  GList *loop;



  stab = info->stab;
  window = info->window; 
  table = info->tab;


  if (stab == NULL || table == NULL)
    {
      debug_print ("Warning unexpected NULL pointer\n");
      return;
    }


  stab->subfields = free_subfields (stab->subfields);
  loop = table->fields;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      fld = loop->data;
      sfld = new_subfield ();
      sfld->name = g_malloc (strlen (fld->name) + 1);
      sfld->name_of_field = g_malloc (strlen (fld->name) + 1);
      strcpy (sfld->name, fld->name);
      strcpy (sfld->name_of_field, fld->name);
      stab->subfields = g_list_append (stab->subfields, sfld);
      loop = loop->next;
    }
  update_widget (window);
  info->s->changed =TRUE;
}


static void
right_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  GtkWidget *window;
  GList *selection;
  subtable *stab;
  subfield *sfld;
  gint row;

  stab = info->stab;
  window = info->window;
  list = info->subfieldlist;

  selection = GTK_CLIST (list)->selection;

  if (selection == NULL)
    {
      return;
    }
  else
    {
      row = GPOINTER_TO_INT(selection->data);
    }
  sfld = gtk_clist_get_row_data (GTK_CLIST (list), row);
  stab->subfields = g_list_remove (stab->subfields, sfld);
  delete_subfield (sfld);
  update_widget (window);
  info->s->changed =TRUE;
}


static void
left_clicked (GtkWidget * widget, info1 *info)
{
  GList *selection;
  GtkWidget *window;
  subtable *stab;
  GtkWidget *list;
  gint row;
  field *fld;
  subfield *sfld;

  list = info->fieldlist;
  stab = info->stab;
  window = info->window;

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    {
      return;
    }
  else
    {
      row = GPOINTER_TO_INT(selection->data);
    }
  fld = gtk_clist_get_row_data (GTK_CLIST (list), row);

  sfld = new_subfield ();
  sfld->name = g_malloc (strlen (fld->name) + 1);
  sfld->name_of_field = g_malloc (strlen (fld->name) + 1);
  strcpy (sfld->name, fld->name);
  strcpy (sfld->name_of_field, fld->name);
  stab->subfields = g_list_append (stab->subfields, sfld);
  update_widget (window);
  info->s->changed =TRUE;
}





static void
down_field_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *wid;
  subtable *stab;
  subfield *fld;
  int row;
  int len;
  GList *selection;

  wid = info->subfieldlist;
  stab = info->stab; 
 
  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    {
      return;
    }
  else
    {
      row = GPOINTER_TO_INT(selection->data);
    }

  fld = gtk_clist_get_row_data (GTK_CLIST (wid), row);

  len = g_list_length (stab->subfields);
  if (row >= len - 1)
    return;


  stab->subfields = g_list_remove (stab->subfields, fld);
  stab->subfields = g_list_insert (stab->subfields, fld, row + 1);
  gtk_clist_select_row (GTK_CLIST (wid), row + 1, 0);
  info->s->changed= TRUE; 
  update_widget (info->window);
}



static void
up_field_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *wid;
  subtable *stab;
  subfield *fld;
  int row;
  int len;
  GList *selection;

  wid = info->subfieldlist;
  stab = info->stab;

  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    {
      return;
    }
  else
    {
      row = GPOINTER_TO_INT(selection->data);
    }

  if (row <= 0)
    return;
  fld = gtk_clist_get_row_data (GTK_CLIST (wid), row);

  len = g_list_length (stab->subfields);

  stab->subfields = g_list_remove (stab->subfields, fld);
  stab->subfields = g_list_insert (stab->subfields, fld, row - 1);

  info->s->changed = TRUE;

  gtk_clist_select_row (GTK_CLIST (wid), row - 1, 0);
  update_widget (info->window);
 
}


static void
check_clicked (GtkWidget * but, info1 *info)
{
  gchar *name;
  gchar *name2 = NULL;
  GList *loop;
  subtable *stab;

  stab = info->stab;

  if (stab == NULL)
    {
      debug_print ("Unexpected NULL pointer\n");
      return;
    }

  name = gtk_object_get_user_data (GTK_OBJECT (but));
  info->s->changed =TRUE;

  
  if (GTK_TOGGLE_BUTTON (but)->active)	/* add to list */
    {
      if (g_list_length(stab->viewable_as)==1)
      {
        if ( strcmp(stab->viewable_as->data,name)==0)
          return; 
      }
      name2 = g_malloc (strlen (name) + 1);
      strcpy (name2, name);
      stab->viewable_as = g_list_append (stab->viewable_as, name2);
      update_all();
    }
  else
    /* remove from list */
    {
      if (g_list_length(stab->viewable_as) == 1)
      {
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(but),TRUE);
      }
      else
      {        
        loop = stab->viewable_as;
        loop = g_list_first (loop);
        while (loop)
 	  {
	    name2 = loop->data;
	    if (strcasecmp (name, name2) == 0)
	      {
	        stab->viewable_as = g_list_remove (stab->viewable_as, name2);
	        g_free (name2);
                update_all();
	        return;
	      }
	    loop = loop->next;
	  } /* while */
       } /* if length */
    } /* toggle */
}

static void
combo_changed (GtkWidget * widget, info1 *info)
{
  gchar *table;
  subtable *stab;
  GtkWidget *window;
#ifdef DEBUG
  debug_print ("In combo func\n");
#endif 
  window = info->window;
  table = gtk_entry_get_text (GTK_ENTRY (widget));
  stab =  info->stab;
  if (stab->name_of_table != NULL)
    {
      g_free (stab->name_of_table);
    }
  stab->name_of_table = g_malloc (strlen (table) + 1);
  strcpy (stab->name_of_table, table);

  gtk_object_set_data (GTK_OBJECT (window), "NOUP", "asdf");
  update_widget (window);
  gtk_object_set_data (GTK_OBJECT (window), "NOUP", NULL);
  info->s->changed =TRUE;
}


static void
edit_clicked (GtkWidget * widget, info1 *info)
{
  GList *selection;
  GtkWidget *list;
  subfield *fld;
  int row;
 
  list = info->subfieldlist;

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  fld = gtk_clist_get_row_data (GTK_CLIST (list), row);

  create_subfieldprop_window (fld, info->s, fld->name);

}



static void
field_dclicked (GtkWidget * widget, GdkEventButton * event,
		info1 *info)
{
  if (event->type == GDK_2BUTTON_PRESS)
    {
      edit_clicked (widget, info);
    }
}



static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  return FALSE;
}

static void
destroy (GtkWidget * widget, info1 *info)
{

  remove_update (widget);
  info->stab->window=NULL;
  g_free(info);
}


static void
close_win (GtkWidget * wid, info1 *info)
{
  gtk_widget_destroy (info->window);
}


static gboolean
isin_field (gchar * tabname, gchar * name, GList * tables)
{
  table *tab;
  field *fld;
  GList *loop1;
  GList *loop2;
  loop1 = tables;
  loop1 = g_list_first (loop1);
  while (loop1 != NULL)		/* outer loop search for the correct
				   table */
    {
      tab = loop1->data;
      if (strcasecmp (tab->name, tabname) == 0)
	{
	  loop2 = tab->fields;
	  loop2 = g_list_first (loop2);
	  while (loop2 != NULL)	/* inner loop search for the field */
	    {
	      fld = loop2->data;
	      if (strcasecmp (fld->name, name) == 0)
		return TRUE;
	      loop2 = loop2->next;
	    }
	}
      loop1 = loop1->next;
    }
  return FALSE;
}

static void
update_subfieldwin (GtkWidget * window, gpointer data)
{
  info1 *info;
  GList *loop;
  GList *loop2;
  gboolean in;
  subfield *sfld;
  field *fld = NULL;
  table *tab = NULL;
  table *foundtab;
  file_s *s;
  gint row;
  gint rowsubfield;
  gint rowfield;
  GList *selection;
  GtkVisibility vis;
  GdkColor color;
  GdkColormap *colormap;

#ifdef DEBUG
  debug_print("Updating subfield window\n");
#endif


  info = data;
  s = info->s;

  selection = GTK_CLIST (info->fieldlist)->selection;
  if (selection == NULL)
    {
      rowfield = -1;
    }
  else
    {
      rowfield = GPOINTER_TO_INT(selection->data);
    }

  selection = GTK_CLIST (info->subfieldlist)->selection;
  if (selection == NULL)
    {
      rowsubfield = -1;
    }
  else
    {
      rowsubfield = GPOINTER_TO_INT(selection->data);
    }


  gtk_clist_clear (GTK_CLIST (info->fieldlist));
  gtk_clist_clear (GTK_CLIST (info->subfieldlist));
  foundtab = NULL;
  loop2 = NULL;		/* Use this to fill the combo box simultanious */
  loop = s->tables;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      tab = loop->data;
      loop2 = g_list_append (loop2, tab->name);
      if (strcmp (tab->name, info->stab->name_of_table) == 0)
	foundtab = tab;
      loop = loop->next;
    }

  if (foundtab == NULL)
    {
#ifdef DEBUG
      debug_print ("Cannot find requested table\n");
#endif
      loop = g_list_first (s->tables);
      if (loop != NULL)
      	foundtab = loop->data;

      if (foundtab == NULL)
      {
#ifdef DEBUG
        debug_print("Warning no tables left !!!! subtable win no good!\n");
#endif
        return;
      }


      g_free (info->stab->name_of_table);
      info->stab->name_of_table = g_malloc (strlen (foundtab->name) + 1);
      strcpy (info->stab->name_of_table, foundtab->name);
    }


  if (gtk_object_get_data (GTK_OBJECT (window), "NOUP") == NULL)
    {
      /* temp. block the combo box signal #@$%#$%#~@ */
      gtk_signal_handler_block_by_func (GTK_OBJECT
		 (GTK_COMBO (info->combo)->entry), combo_changed, info);
      gtk_combo_set_popdown_strings (GTK_COMBO (info->combo), loop2);
      gtk_entry_set_text (GTK_ENTRY (GTK_COMBO 
		(info->combo)->entry), info->stab->name_of_table);
      gtk_signal_handler_unblock_by_func (GTK_OBJECT 
		(GTK_COMBO (info->combo)->entry), combo_changed, info);
    } 
  g_list_free (loop2);

  loop2 = NULL;
  gtk_clist_freeze (GTK_CLIST (info->fieldlist));
  gtk_clist_freeze (GTK_CLIST (info->subfieldlist));


  loop = foundtab->fields;
  info->tab = foundtab;
  g_list_first (loop);
  while (loop != NULL)
    {
      fld = loop->data;
      /* Check if item is not in sublist */
      in = FALSE;
      loop2 = info->stab->subfields;
      loop2 = g_list_first (loop2);
      while (loop2 != NULL)
	{
	  sfld = loop2->data;
	  if (strcasecmp (sfld->name_of_field, fld->name) == 0)
	    {
	      in = TRUE;
	    }
	  loop2 = loop2->next;
	}
      if (in == FALSE)
	{
#ifdef DEBUG
	  debug_print ("Name fieldlist :%s\n", fld->name);
#endif
	  row = gtk_clist_append (GTK_CLIST (info->fieldlist), &fld->name);
	  gtk_clist_set_row_data (GTK_CLIST (info->fieldlist), row, fld);
	}
      loop = loop->next;
    }
  colormap = gdk_window_get_colormap (info->subfieldlist->window);
  color.red = 1.0 * 65535.0;
  color.green = 0.80 * 65535.0;
  color.blue = 0.80 * 65535.0;
  gdk_color_alloc (colormap, &color);
  loop = info->stab->subfields;	/* This is a subtable */
  g_list_first (loop);
  while (loop != NULL)
    {
      sfld = loop->data;
      row = gtk_clist_append (GTK_CLIST (info->subfieldlist),
			      &sfld->name_of_field);
      if (isin_field (foundtab->name, sfld->name_of_field, s->tables) == FALSE)
	{
	  gtk_clist_set_background (GTK_CLIST (info->subfieldlist), row, &color);
	}
      gtk_clist_set_row_data (GTK_CLIST (info->subfieldlist), row, sfld);
      loop = loop->next;
    }

  if (rowfield >= g_list_length (foundtab->fields))
    rowfield = g_list_length (foundtab->fields) - 1;
  gtk_clist_select_row (GTK_CLIST (info->fieldlist), rowfield, 0);
  vis = gtk_clist_row_is_visible (GTK_CLIST (info->fieldlist), rowfield);
  if (vis != GTK_VISIBILITY_FULL)
    gtk_clist_moveto (GTK_CLIST (info->fieldlist), rowfield, 0, 1, 0);

  if (rowsubfield >= g_list_length (info->stab->subfields))
    rowsubfield = g_list_length (info->stab->subfields) - 1;
  gtk_clist_select_row (GTK_CLIST (info->subfieldlist), rowsubfield, 0);
  vis = gtk_clist_row_is_visible (GTK_CLIST (info->subfieldlist), rowsubfield);
  if (vis != GTK_VISIBILITY_FULL)
    gtk_clist_moveto (GTK_CLIST (info->subfieldlist), rowsubfield, 0, 1, 0);

  gtk_clist_thaw (GTK_CLIST (info->fieldlist));
  gtk_clist_thaw (GTK_CLIST (info->subfieldlist));

}



gboolean
is_viewable (GList * va, gchar * name)
{
  GList *loop;

  loop = va;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      if (strcasecmp (loop->data, name) == 0)
	return TRUE;
      loop = loop->next;
    }
  return FALSE;
}


void
create_subfield_window (subtable *tab, file_s *s, gchar * caption)
{
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkWidget *vbox3;
  GtkWidget *hbox;
  GtkWidget *hbox22;
  GtkWidget *hbox3;
  GtkWidget *fieldlist;
  GtkWidget *combo;
  GtkWidget *subfieldlist;
  GtkWidget *addfield;
  GtkWidget *editfield;
  GtkWidget *window;
  GtkWidget *frame;
  GtkWidget *lab;
  GtkWidget *scroll;
  GtkWidget *addall;
  GtkWidget *removeall;
  GtkWidget *addright;
  GtkWidget *addleft;
  GtkWidget *checkbox;
  info1 *info;
  gchar buffer[1000];
  gint key;
  int i;


  if (tab->window != NULL)
    {
      gdk_window_raise (tab->window->window);
      return;
    }

  info = g_malloc (sizeof (info1) );

  strcpy (buffer, _ ("Subtable: "));
  strcat (buffer, caption);
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_policy(GTK_WINDOW(window),FALSE,TRUE,TRUE);
  gtk_window_set_title (GTK_WINDOW (window), buffer);

  accel = gtk_accel_group_new ();

  /* WM */
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_event), info);
  /* Destroy */
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (destroy), info);

  gtk_widget_show (window);


  tab->window = window;
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);
  hbox = gtk_hbox_new (FALSE, 5);

  vbox3 = gtk_vbox_new (FALSE, 5);
  gtk_box_pack_start (GTK_BOX (hbox), vbox3, TRUE, TRUE, 0);
  gtk_widget_show (vbox3);


  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("Fields"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (vbox3), frame, TRUE, TRUE, 0);

  vbox = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);


  /* Clist */
  subfieldlist = gtk_clist_new (1);
  gtk_widget_set_usize (subfieldlist, 150, 100);
  gtk_widget_show (subfieldlist);
  gtk_widget_show (vbox);
  gtk_clist_set_selection_mode (GTK_CLIST (subfieldlist), GTK_SELECTION_BROWSE);
  gtk_signal_connect (GTK_OBJECT (subfieldlist), "button_press_event",
		      GTK_SIGNAL_FUNC (field_dclicked), info);
  scroll = scroll_new (subfieldlist);
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);

  vbox2 = gtk_vbox_new (TRUE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox2), 5);
  gtk_box_pack_start (GTK_BOX (vbox), vbox2, FALSE, TRUE, 0);
  // gtk_container_add (GTK_CONTAINER (vbox), vbox2);

  hbox22 = gtk_hbox_new (TRUE, 2);
  gtk_widget_show (hbox22);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox22, FALSE, TRUE, 0);

  editfield = gtk_button_new ();
  lab = xpm_label_box (up_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (editfield), lab);
  gtk_box_pack_start (GTK_BOX (hbox22), editfield, FALSE, TRUE, 0);
  gtk_widget_show (editfield);
  gtk_signal_connect (GTK_OBJECT (editfield), "clicked",
		      GTK_SIGNAL_FUNC (up_field_clicked), info);


  editfield = gtk_button_new ();
  lab = xpm_label_box (down_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (editfield), lab);
  gtk_box_pack_start (GTK_BOX (hbox22), editfield, FALSE, TRUE, 0);
  gtk_widget_show (editfield);
  gtk_signal_connect (GTK_OBJECT (editfield), "clicked",
		      GTK_SIGNAL_FUNC (down_field_clicked), info);




  /* The buttons */
  addfield = gtk_button_new ();
  lab = xpm_label_box (stock_last_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addfield), lab);
  gtk_box_pack_start (GTK_BOX (vbox2), addfield, FALSE, FALSE, 0);
  gtk_widget_show (addfield);
  gtk_widget_add_accelerator (addfield, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);
  removeall = addfield;


  /* The buttons */
  addfield = gtk_button_new ();
  lab = xpm_label_box (stock_right_arrow_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addfield), lab);
  gtk_box_pack_start (GTK_BOX (vbox2), addfield, FALSE, FALSE, 0);
  gtk_widget_show (addfield);
  gtk_widget_add_accelerator (addfield, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);
  addright = addfield;


  /* The buttons */
  addfield = gtk_button_new ();
  lab = xpm_label_box (stock_left_arrow_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addfield), lab);
  gtk_box_pack_start (GTK_BOX (vbox2), addfield, FALSE, FALSE, 0);
  gtk_widget_show (addfield);
  gtk_widget_add_accelerator (addfield, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);
  addleft = addfield;

  /* The buttons */
  addfield = gtk_button_new ();
  lab = xpm_label_box (stock_first_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addfield), lab);
  gtk_box_pack_start (GTK_BOX (vbox2), addfield, FALSE, FALSE, 0);
  gtk_widget_show (addfield);
  gtk_widget_add_accelerator (addfield, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);
  addall = addfield;

  gtk_widget_show (vbox2);



  /* Clist */
  fieldlist = gtk_clist_new (1);
  gtk_widget_set_usize (fieldlist, 150, 100);
  gtk_widget_show (fieldlist);
  gtk_widget_show (vbox);
  gtk_clist_set_selection_mode (GTK_CLIST (fieldlist), GTK_SELECTION_BROWSE);
/*  gtk_signal_connect (GTK_OBJECT (fieldlist), "button_press_event",
   GTK_SIGNAL_FUNC (field_dclicked), fieldlist); */
  scroll = scroll_new (fieldlist);
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);



  gtk_signal_connect (GTK_OBJECT (removeall), "clicked",
		      GTK_SIGNAL_FUNC (remove_all_clicked), info);
  gtk_signal_connect (GTK_OBJECT (addall), "clicked",
		      GTK_SIGNAL_FUNC (add_all_clicked), info);
  gtk_signal_connect (GTK_OBJECT (addright), "clicked",
		      GTK_SIGNAL_FUNC (right_clicked), info);
  gtk_signal_connect (GTK_OBJECT (addleft), "clicked",
		      GTK_SIGNAL_FUNC (left_clicked), info);




  hbox3 = gtk_hbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (hbox3), 0);
  gtk_box_pack_start (GTK_BOX (vbox3), hbox3, FALSE, TRUE, 0);
  gtk_widget_show (hbox3);

  /* button frame */
  lab = gtk_label_new ("Table");
  gtk_box_pack_start (GTK_BOX (hbox3), lab, FALSE, TRUE, 5);
  gtk_widget_show (lab);

  combo = gtk_combo_new ();
  gtk_box_pack_end (GTK_BOX (hbox3), combo, FALSE, TRUE, 5);
  gtk_entry_set_editable (GTK_ENTRY (GTK_COMBO (combo)->entry), FALSE);
  gtk_widget_show (combo);
  gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo)->entry), 
		"changed", GTK_SIGNAL_FUNC (combo_changed), info);



  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("Options"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, TRUE, 5);

  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);


  i = 0;
  while ( view_names[i].name ) {
      checkbox = gtk_check_button_new_with_label(_(view_names[i].locale));
      gtk_box_pack_start (GTK_BOX (vbox), checkbox, FALSE, TRUE, 0);
      gtk_widget_show (checkbox);
      gtk_object_set_user_data (GTK_OBJECT (checkbox), view_names[i].name);
      if (is_viewable (tab->viewable_as, view_names[i].name) == TRUE)
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (checkbox), TRUE);

      gtk_signal_connect (GTK_OBJECT (checkbox), "clicked",
			  GTK_SIGNAL_FUNC (check_clicked), info);
      i++;
    }


  editfield = gtk_button_new ();
  lab = xpm_label_box (edit_xpm, _ ("_Edit field"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (editfield), lab);
  gtk_box_pack_start (GTK_BOX (vbox), editfield, FALSE, TRUE, 0);
  gtk_widget_show (editfield);
  gtk_signal_connect (GTK_OBJECT (editfield), "clicked",
		      GTK_SIGNAL_FUNC (edit_clicked), info);
  gtk_widget_add_accelerator (editfield, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);




  editfield = gtk_button_new ();
  lab = xpm_label_box (ja_xpm, _ ("_Close"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (editfield), lab);
  gtk_box_pack_end (GTK_BOX (vbox), editfield, FALSE, TRUE, 0);
  gtk_widget_show (editfield);
  gtk_signal_connect (GTK_OBJECT (editfield), "clicked",
		      GTK_SIGNAL_FUNC (close_win), info);
  gtk_widget_add_accelerator (editfield, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);




  gtk_widget_show (vbox);

  gtk_container_add (GTK_CONTAINER (window), hbox);
  gtk_widget_show (hbox);

  info->window = window;
  info->fieldlist = fieldlist;
  info->subfieldlist = subfieldlist;
  info->combo = combo;
  info->s = s;
  info->stab = tab;
  info->tab = NULL;
  add_update (window, info, update_subfieldwin);

  update_widget (window);

  /*gtk_accel_group_attach (accel, GTK_OBJECT (window));*/
}
