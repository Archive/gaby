/*
   Data options

   Gaby Description Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */


/* misc ****************************************************************** */

void delete_view_as(file_s *s, gchar *name_of_subtable, gchar *view_as);
void reset_view_as(file_s *s, gchar *name_of_subtable, gchar *view_as);


/* Properties ************************************************************* */
void set_option(GList **properties, gchar *property, gchar *value);
gchar * get_option(GList **properties, gchar *property);

/* Renaming & Delete of dependencies ************************************** */
void rename_fields (file_s *s, gchar *name_of_table,
		gchar * old_name, gchar * new_name);
void rename_tables (file_s *s, gchar * old_name, gchar * new_name);
void rename_subtables (file_s *s, gchar * old_name, gchar *new_name);
void delete_subtables (file_s *s, gchar * name);
void delete_tables(file_s *s, gchar * name);


/* Create new structs ***************************************************** */
tlocation *new_tlocation();
location *new_location();

misc_s *new_misc_s();
table *new_table ();
subtable *new_subtable ();
field *new_field ();
subfield *new_subfield ();
doublestring *new_i18n ();
file_s *new_desc ();

/* Delete ***************************************************************** */
void delete_location(location *loc);
void delete_tlocation(tlocation *tloc);
void delete_misc_s(misc_s *s);
void delete_table (table *tab);
void delete_subtable (subtable *tab);
void delete_field (field *fld);
void delete_subfield (subfield *fld);
void delete_i18n (doublestring *ds);
void delete_desc (file_s *s);

/* Delete list **Always return NULL**************************************** */
GList *free_locations (GList * locs);
GList *free_tlocations (GList * tlocs);
GList *free_misc_s (GList * miscs);
GList *free_tables (GList * tables);
GList *free_subtables (GList * subtables);
GList *free_fields (GList * fields);
GList *free_subfields (GList * subfields);
GList *free_i18n (GList * i18n);
