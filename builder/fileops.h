/*
   Generic Fileoperations dialogs (ahum)

   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */


void save_as_fileop (gpointer data);	/* Called by menu       */
void save_fileop (gpointer data);	/* Called by menu       */
void new_fileop (gpointer data);	/* Called by menu       */
void open_fileop (gpointer data);	/* Called by menu       */
void close_prog_fileop (gpointer data);		/* Called by menu       */
void close_fileop (gpointer data);	/* Called by menu       */
void drop_open_fileop (gpointer data, gchar * name);	/* Called by DND       */
void sys_fileop(gpointer data);
