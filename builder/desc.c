/*
   Description file handling

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl
   Copyright (C) 2000 Frederic Peters

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


 */


#include <errno.h>
#include "main.h"


enum field_type
ftype (char *text)
{
  enum field_type type;

  type = -1;
  if (strcasecmp (text, "Strings") == 0)
    type = T_STRINGS;
  if (strcasecmp (text, "String") == 0)
    type = T_STRING;
  if (strcasecmp (text, "Integer") == 0)
    type = T_INTEGER;
  if (strcasecmp (text, "Real") == 0)
    type = T_REAL;
  if (strcasecmp (text, "Decimal") == 0)
    type = T_DECIMAL;
  if (strcasecmp (text, "Date") == 0)
    type = T_DATE;
  if (strcasecmp (text, "Boolean") == 0)
    type = T_BOOLEAN;
  if (strncasecmp (text, "Record ",7) == 0)
    type = T_RECORD;
  if (strncasecmp (text, "Records",7) == 0)
    type = T_RECORDS;
  if (strcasecmp (text, "Multimedia") == 0)
    type = T_MULTIMEDIA;
  if (strcasecmp (text, "File") == 0)
    type = T_FILE;
  if (type == -1)
    {
      type = T_STRING;
    }
  return type;

}

/* Count the number of tabs at the beginning of the line. */
int
numtab (char *str)
{
  int num;
  int teller;
  num = 0;
  for (teller = 0; teller < strlen (str); teller++)
    {
      if (str[teller] != '\t')
	return num;
      num++;
    }
  return num;
}

/* This is my brand new compress_string routine. */
static void 
split (gchar *str, gchar sep, gchar **left, gchar **right)
{
  gchar *part;

  /* find the separator */
  part = strchr(str,sep);
  
  /* devide and conquer */
  if (part != NULL)
  {
    part[0]='\0';
    *left = str;
    *right = part +1;
  }
  else
  {
    *left =str;
    *right = NULL;
  }
  trim(*left);
  trim(*right);
}

static gint
split_option (gchar *buffer, gchar sep, gchar **left, gchar **right)
{
  split (buffer,sep,left,right);
  if (strncasecmp(*left,"i18n_",5)==0)
    {
      *left = *left + 5;      
      return 1;
    }
  if (strncasecmp(*left,"description_",12)==0)
   {
     *left = *left +12;
     return 2;
   }
   if (strncasecmp(*left,"description",11)==0)
   {
     *left = *left +11;
     return 2;
   } 
   if (strcasecmp(*left,"format")==0)
   {
     return 3;
   }
   if (strcasecmp(*left,"default")==0)
   {
     return 4;
   }
   if (strcasecmp(*left,"type")==0)
   {
     return 5;
   }
   return 0;
} 



/* ********************************************************************* */
/* Error dialog 							 */
/* ********************************************************************* */

static void
close_error(GtkWidget *wid, GtkWidget * window)
{
  gtk_widget_destroy(window);

}

static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  return FALSE;
}

static void
destroy (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  gtk_grab_remove(widget);
}

void
create_error_window (gchar * caption, gchar * errortext)
{
  GtkWidget *button;
  GtkWidget *vbox;
  GtkWidget *text;
  GtkWidget *window;
  GtkWidget *scroll;
  GtkWidget *label;

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), caption);

  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
                      GTK_SIGNAL_FUNC (delete_event), NULL);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
                      GTK_SIGNAL_FUNC (destroy), NULL);


  gtk_widget_show (window);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  /* The box */
  vbox = gtk_vbox_new (FALSE, 5);

  /* The label */
  label = gtk_label_new(_("There where errors reading the desc file"));
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);
  gtk_widget_show(label);

  /* text */
  text = gtk_text_new (NULL, NULL);
  gtk_widget_set_usize (text, 300, 100);
  gtk_widget_show (text);
  scroll = scroll_new (text);
  gtk_text_insert(GTK_TEXT(text),NULL,NULL,NULL,errortext,strlen(errortext));
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);

  button = gtk_button_new_with_label (_("Close"));
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      GTK_SIGNAL_FUNC (close_error), window);
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, TRUE, 0);
  gtk_widget_show (button);

  gtk_container_add (GTK_CONTAINER (window), vbox);
  gtk_widget_show (vbox);
  gtk_grab_add(window);
  return;
}






/* ********************************************************************* */
/* New loading routine							 */
/* ********************************************************************* */


static void
add_error_text(gchar **errortext, gint line, gchar *error, gchar *text)
{
  gchar errorb[1000];
  gboolean first=FALSE;
  errorb[0]=0;

  sprintf(errorb,"Line %02d, %s: %s,\n",line, error, text);
  if (*errortext == NULL)
    first = TRUE;

  *errortext = realloc(*errortext,
	(first == FALSE ? strlen(*errortext):0) + strlen(errorb) + 2);

  if (first == TRUE)
    *errortext[0]='\0';

  strcat ( *errortext,errorb );
}


int
load_tables (file_s *s, FILE * in, gchar **errortext)
{
  gchar buffer[1001];
  gchar *left;
  gchar *right;
  gint linec;
  gboolean InTables=FALSE;
  gboolean InTable=FALSE;
  gboolean InField=FALSE;
  table *tab=NULL; 
  field *fld=NULL;
  doublestring *ds;
 

  /* Is file non-existant */
  if (in == NULL)
    return -1;
 
  /* Back to the beginning */
  rewind(in);
  linec = 0;

  while (feof (in) == 0 && ferror (in) == 0)
    {
      fgets (buffer, 1000, in);
      linec++;
      switch (numtab (buffer))
	{
	case 0:
	  {
            if (strcasecmp(buffer,"Begin Tables\n")==0)
              {
                InTables = TRUE;
              }
            if (strcasecmp(buffer,"End\n")==0 && InTables == TRUE)
             {
               InTables = FALSE;
               InTable = FALSE;
               InField = FALSE;
             }
          } break;

        
        /* Add the table */

        case 1:
          {
            if ( InTables == TRUE )
              {
                tab = new_table();
                s->tables = g_list_append(s->tables,tab);
		split ( buffer,':', &left, &right);
                tab->name = g_strdup(left);
                tab->alias = g_strdup(right);
                InTable  = TRUE;
              }
          } break;

        /* Add the fields */

        case 2:
          {
            if ( InTables == TRUE && InTable == TRUE )
              {
		fld = new_field();
                tab->fields = g_list_append(tab->fields,fld);
                split ( buffer,':', &left, &right);
                fld->name = g_strdup (left);
                fld->type = ftype(right);
                if (fld->type == T_RECORD || fld->type == T_RECORDS)
                  {
                    add_error_text(errortext,linec,"Record(s) field type not"
						" supported",buffer);
                    fld->type = T_STRING;
                  }
                InField = TRUE; 
              } 
           } break;

          /* Add the field options */

        case 3:
          {
            if ( InTables == TRUE && InTable == TRUE && InField == TRUE )
              {
                switch (split_option(buffer,'=',&left,&right))
                  {
                  default: /* Unkown option */
                    {
			add_error_text(errortext,linec,"Unkown field option",
							buffer);
                    } break;
                  case 1: /* i18n name */
                    {
                      ds = new_i18n();
                      ds->name = g_strdup(right);
                      ds->locale = g_strdup(left);
		      fld->i18n_name = g_list_append(fld->i18n_name,ds);
                    } break;
                  case 2: /* i18n desc */
                    {
                      ds = new_i18n();
                      ds->name = g_strdup(right);
                      ds->locale = g_strdup(left);
		      fld->i18n_desc = g_list_append(fld->i18n_desc,ds);
                    } break;
                  case 3: /* Format */
                    {
                      set_option(&fld->properties,"FORMAT", right);
                    } break;
                  case 4: /* Default value */
                    {
                      set_option(&fld->properties,"DEFAULT", right);
                    } break;
                  case 5: /* MM type */
                    {
                      set_option(&fld->properties,"TYPE" , right);
                    } break;
                  }
              }
          } break;
       default:
         {
           if (InTables == TRUE)
             {
               trim(buffer);
               add_error_text(errortext,linec,"Unkown indentation depth",
	     		buffer);
             }
         }  break;
       } /* switch */

    } /* while != eof */

  if (errno == EISDIR)
    {
      return -2;
    }
 
  return 0; 
}




void
set_viewable_as_new (subtable *tab, gchar * buffer)
{
  gchar *str;
  gchar *name;
  gint begin;
  gint end;
  trim (buffer + 11);
  begin = 11;
  do
    {
      str = strchr (buffer + begin, ',');
      if (str == NULL)
	{
	  end = strlen (buffer) - 1;
	}
      else
	{
	  end = str - buffer;
	}
      name = g_malloc (end - begin + 2);
      name[0] = '\0';
      strncat (name, buffer + begin, end - begin);
      trim(name);
      tab->viewable_as = g_list_append (tab->viewable_as, name);
      begin = end + 1;
    }
  while (str != NULL);
}


/* Add's all the fields to the subfield */
void
with_every_field
	(subtable *stab, file_s *s)
{
  table *tab;
  subfield *sfld;
  field *fld;
  GList *loop;
  GList *loop2;
  stab->subfields = NULL;
  loop = s->tables;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      tab = loop->data;
      if (strcasecmp (tab->name, stab->name_of_table) == 0)
	{
	  loop2 = tab->fields;
	  loop2 = g_list_first (loop2);
	  while (loop2 != NULL)
	    {
	      fld = loop2->data;
              sfld = new_subfield();
              sfld->name = g_strdup(fld->name);
              sfld->name_of_field = g_strdup(fld->name);
#ifdef DEBUG
	      debug_print ("Adding: %s\n", sfld->name);
#endif
	      stab->subfields = g_list_append (stab->subfields, sfld);
	      loop2 = loop2->next;
	    }
	}
      loop = loop->next;
    }
  return;
}



int
load_subtables (file_s *s, FILE * in, gchar **errortext)
{
  gchar buffer[1001];
  gchar *left;
  gchar *right;
  gint linec;
  gboolean InsubTables=FALSE;
  gboolean InsubTable=FALSE;
  gboolean InsubField=FALSE;
  gboolean InsubFields=FALSE;
  gboolean Ok=FALSE;
  subtable *stab=NULL; 
  subfield *sfld=NULL;
  doublestring *ds;
   

  /* Is file non-existant */
  if (in == NULL)
    return -1;
 
  /* Back to the beginning */
  rewind(in);
  linec = 0;

  while (feof (in) == 0 && ferror (in) == 0)
    {
      fgets (buffer, 1000, in);
      linec++;
      switch (numtab (buffer))
	{
	case 0:
	  {
            if (strcasecmp(buffer,"Begin SubTables\n")==0)
              {
                InsubTables = TRUE;
              }
            if (strcasecmp(buffer,"End\n")==0 && InsubTables == TRUE)
             {
               InsubTables = FALSE;
               InsubTable = FALSE;
               InsubFields = FALSE;
               InsubField = FALSE;
             }
          } break;

        
        /* Add the subtable */

        case 1:
          {
            if ( InsubTables == TRUE )
              {
                stab = new_subtable();
                s->subtables = g_list_append(s->subtables,stab);
		split ( buffer,':', &left, &right);
                stab->name = g_strdup(left);
                stab->name_of_table = g_strdup(right);
                InsubTable  = TRUE;
              }
          } break;

        /* Get the subtable options */

        case 2:
          {
           InsubFields = FALSE; 
           if ( InsubTables == TRUE && InsubTable == TRUE)
              {
                trim(buffer);
                Ok = FALSE;

                 /* I18N options */

          	if (strncasecmp(buffer,"i18n_",5)==0)
                {
                  split(buffer,'=',&left,&right);
                  left = left+5;
                  ds = new_i18n();
                  ds->name = g_strdup(right);
                  ds->locale = g_strdup(left);
                  stab->i18n_name = g_list_append(stab->i18n_name,ds); 
                  Ok=TRUE;
                }  
          	if (strncasecmp(buffer,"description_",12)==0)
                {
                  split(buffer,'=',&left,&right);
                  left = left+12;
                  ds = new_i18n();
                  ds->name = g_strdup(right);
                  ds->locale = g_strdup(left);
                  stab->i18n_desc = g_list_append(stab->i18n_desc,ds); 
                  Ok=TRUE;
                }  
          	if (strncasecmp(buffer,"description",11)==0)
                {
                  split(buffer,'=',&left,&right);
                  left = left+11;
                  ds = new_i18n();
                  ds->name = g_strdup(right);
                  ds->locale = g_strdup(left);
                  stab->i18n_desc = g_list_append(stab->i18n_desc,ds); 
                  Ok=TRUE;
                } 


                /* Viewable as option */
                

                if (strncasecmp(buffer,"viewable as",11)==0)
                {
                  set_viewable_as_new(stab, buffer);
                  Ok=TRUE;
                }



                /* Field options */

                if (strcasecmp(buffer,"with only the following fields")==0)
                {
                  InsubFields = TRUE;
                  Ok=TRUE;
                }
                if (strcasecmp(buffer,"with every fields." )==0)
                {
                  with_every_field(stab,s);
                  Ok=TRUE;
                } 

                /* Check if the option has been processed */
                if (Ok == FALSE)
                  {
                    add_error_text(errortext,linec,"Unkown subfield option",
			buffer);
                  }

              } /* if in subtable */
            } break;

          /* Get the fields */

          case 3:
            {
               if (InsubFields==TRUE && InsubTables == TRUE && InsubTable==TRUE)
                 {
       	           sfld = new_subfield();
                   split (buffer,':',&left,&right);
                   sfld->name_of_field = g_strdup(left);
                   sfld->name = g_strdup(right);
                   stab->subfields = g_list_append(stab->subfields,sfld);
                   InsubField=TRUE;
                 }
            } break;

          /* Get the i18n name and desc for the subfield */

          case 4:
            {
              if (InsubFields==TRUE && InsubTables == TRUE &&
                  InsubTable==TRUE && InsubField ==TRUE)
                {
                  trim(buffer);
            	  if (strncasecmp(buffer,"i18n_",5)==0)
                    {
                      split(buffer,'=',&left,&right);
                      left = left+5;
                      ds = new_i18n();
                      ds->name = g_strdup(right);
                      ds->locale = g_strdup(left);
                      sfld->i18n_name = g_list_append(sfld->i18n_name,ds); 
                    }  
          	  if (strncasecmp(buffer,"description_",12)==0)
                    {
                      split(buffer,'=',&left,&right);
                      left = left+12;
                      ds = new_i18n();
                      ds->name = g_strdup(right);
                      ds->locale = g_strdup(left);
                      sfld->i18n_desc = g_list_append(sfld->i18n_desc,ds); 
                    }  
          	  if (strncasecmp(buffer,"description",11)==0)
                    {
                      split(buffer,'=',&left,&right);
                      left = left+11;
                      ds = new_i18n();
                      ds->name = g_strdup(right);
                      ds->locale = g_strdup(left);
                      sfld->i18n_desc = g_list_append(sfld->i18n_desc,ds); 
                    } 
                }
            } break;

          /* Unkown indent depth */

          default :
            {
              if (InsubTables == TRUE)
                {
                  trim(buffer);
                  add_error_text(errortext,linec,"Unkown indentation depth",
			buffer);
                }
            } break;

        } /* switch */

    } /* while != eof */

  if (errno == EISDIR)
    {
      return -2;
    }
 
  return 0; 
}




int
load_misc (file_s *s, FILE * in)
{
  gint tables=0;
  gint window=0;
  gint mwin= 0;
  gint owin = 0;
  gint linec=0;
  gint bind=0;
  gchar buffer[1000];
  gchar *left;
  gchar *right;
  misc_s *misc=NULL;
  doublestring *ds;

  rewind (in);
  linec = 0;

  if (in == NULL)
    return -1;

  if (s->miscs != NULL)
    {
      debug_print ("Warning misc !=NULL where it should be\n");
    }

  while (feof (in) == 0 && ferror (in) == 0)
    {
      fgets (buffer, 1000, in);
      linec++;
      switch (numtab (buffer))
	{
	case 0:
	  if (strcasecmp (buffer, "Begin misc\n") == 0)
	    {
	      tables = 1;
	    }
	  if (strcasecmp (buffer, "End\n") == 0 && tables == 1)
	    {
	      tables = 0;
	      window = 0;
              mwin = 0 ;
              owin = 0;
              bind =0;
	    }
	  /* start  en of tables */
	  break;
        case 1:
	  {
            if (strcasecmp(buffer,"\tMain window\n")==0)
            {
              mwin=1;
              owin=0;
              bind=0;
            }
            if (strcasecmp(buffer,"\tOther windows\n")==0)
            {
              mwin=0;
              owin=1;
              bind=0;
            }
          } break;
        case 2:
         {
           if (mwin==1 || owin ==1)
           {
	     misc = new_misc_s();
             split (buffer,':',&left, &right);
             misc->view_as = g_strdup(left);
             misc->name_of_subtable = g_strdup (right);
	     if (mwin ==1 )
	       misc->main=TRUE;
             else 
               misc->main=FALSE;
	     s->miscs = g_list_append(s->miscs,misc);
           }
         } break;
        case 3:
         {
           if ((mwin==1 || owin ==1) && (misc != NULL))
           {
             split (buffer,':',&left,&right);
             if (strcasecmp(left,"visible")==0)
	     {
               if (strcasecmp(right,"no")==0)
               {
                 misc->visible=FALSE;
               }
               else
                 misc->visible=TRUE;
             }
             if (strcasecmp(left,"bind to")==0)
              {
                 bind = 1;
              }
           } 
         } break;
	case 4:
	  {
            if ((mwin==1 || owin ==1 ) && misc != NULL && bind==1)
              {
                split (buffer,':',&left, &right);
		ds = new_i18n();
		ds->name = g_strdup(right);
		ds->locale = g_strdup(left);	
                misc->bind_to = g_list_append(misc->bind_to, ds);
              }
           }break;
	default:
          if (tables ==1)
	    debug_print("%d : Unexpected option !!\n",linec);
	  break;
	}
    }
  if (errno == EISDIR)
    {
      return -1;
    }
  return 0;
}







gint
load_actions (file_s *s, FILE * in)
{
  int len;
  int now;
  int tables;
  gchar buffer[1000];
  int linec;
  rewind (in);
  linec = 0;
  if (in == NULL)
    return -1;
  len = 1;
  tables = 0;
  if (s->actions != NULL)
    {
      debug_print ("Warning actions !=NULL where is should be\n");
    }
  s->actions = g_malloc (1);
  s->actions[0] = '\0';

  while (feof (in) == 0 && ferror (in) == 0)
    {
      fgets (buffer, 1000, in);
      linec++;
      switch (numtab (buffer))
	{
	case 0:
	  if (strcasecmp (buffer, "Begin actions\n") == 0)
	    {
	      tables = 1;
	    }
	  if (strcasecmp (buffer, "End\n") == 0 && tables == 1)
	    {
	      tables = 0;
	    }
	  /* start  en of tables */
	  break;
	default:
	  if (tables == 1)
	    {
	      now = strlen (buffer);
	      if (now != 0)
		{
		  len += now;
		  s->actions = g_realloc (s->actions, len);
		  strcat (s->actions, buffer);
		}
	    }
	  break;
	}
    }
  if (errno == EISDIR)
    {
      return -1;
    }
  return 0;
}



int
load_locations (file_s *s, FILE * in, gchar **errortext)
{
  gint tables=0;
  gint window=0;
  gint mwin= 0;
  gint owin = 0;
  gint option = 0;
  gint linec=0;
  gchar buffer[1000];
  gchar *left;
  gchar *right;
  tlocation *tloc=NULL;
  location *loc=NULL; 
  

  rewind (in);
  linec = 0;

  if (in == NULL)
    return -1;

  while (feof (in) == 0 && ferror (in) == 0)
    {
      fgets (buffer, 1000, in);
      linec++;
      switch (numtab (buffer))
	{
	case 0:
	  if (strcasecmp (buffer, "Begin locations\n") == 0)
	    {
	      tables = 1;
	    }
	  if (strcasecmp (buffer, "End\n") == 0 && tables == 1)
	    {
	      tables = 0;
	      window = 0;
              mwin = 0 ;
              owin = 0;
	    }
	  /* start  en of locations */
	  break;
        case 1:
	  {
            if (tables == 1)
              {
                tloc = new_tlocation();
                trim (buffer);
                tloc->name_of_table = g_strdup(buffer);
                s->tlocations = g_list_append (s->tlocations, tloc);
                mwin = 1;
                owin = 0;
              }
          } break;
        case 2:
         {
           if (mwin==1)
           {
             loc = new_location();
             trim(buffer);
             loc->path = g_strdup(buffer);
             tloc->locations = g_list_append(tloc->locations,loc);
	     owin = 1;
           }
         } break;
        case 3:
         {
           if ((mwin==1 || owin ==1) )
           {
             split (buffer,'=',&left, &right);
#ifdef DEBUG_GABY
             debug_print("left :[%s] right : [%s]\n",left,right);
#endif
             option = 0;
             if (strcasecmp(left,"format")==0)
               {
		 set_option(&loc->prop,"FORMAT",right);
                 option =1;
               }
             if (strcasecmp(left,"reread")==0)
               {
		 set_option(&loc->prop,"REREAD",right);
                 option =1;
               }
             if (strcasecmp(left,"read-only")==0)
               {
		 set_option(&loc->prop,"READONLY",right);
                 option =1;
               } 
             if (option ==0)
               {
	         add_error_text(errortext,linec,"Unkown option for "
                                          "locations", buffer);
               }
           } 
         } break;
	default:
          if (tables ==1)
            {
              trim(buffer);
              add_error_text(errortext,linec,"Unkown indentation depth for "
                                          "locations", buffer);
            }
	  break;
	}
    }
  if (errno == EISDIR)
    {
      return -1;
    }
  return 0;
}






gint
load_options (file_s *s, FILE * in)
{
  int len;
  int now;
  int tables;
  gchar buffer[1000];
  int linec;
  rewind (in);
  linec = 0;
  if (in == NULL)
    return -1;
  len = 1;
  tables = 0;
  if (s->plugin_options != NULL)
    {
      debug_print ("Warning actions !=NULL where is should be\n");
    }
  s->plugin_options = g_malloc (1);
  s->plugin_options[0] = '\0';

  while (feof (in) == 0 && ferror (in) == 0)
    {
      fgets (buffer, 1000, in);
      linec++;
      switch (numtab (buffer))
	{
	case 0:
	  if (strcasecmp (buffer, "Begin plugins_options\n") == 0)
	    {
	      tables = 1;
	    }
	  if (strcasecmp (buffer, "End\n") == 0 && tables == 1)
	    {
	      tables = 0;
	    }
	  /* start  en of tables */
	  break;
	default:
	  if (tables == 1)
	    {
	      now = strlen (buffer);
	      if (now != 0)
		{
		  len += now;
		  s->plugin_options = g_realloc (s->plugin_options, len);
		  strcat (s->plugin_options, buffer);
		}
	    }
	  break;
	}
    }
  if (errno == EISDIR)
    {
      return -1;
    }
  return 0;
}







int
load_desc (gchar * name, file_s *s)
{
  FILE *in;
  gchar *errortext=NULL;

  in = fopen (name, "r");
  if (in == NULL)
    {
      return -1;
    }
  if (load_tables (s, in, &errortext) != 0)
    {
      return -1;
    }
  if (load_subtables (s, in, &errortext) != 0)
    {
      return -1;
    }
  if (load_actions (s, in) != 0)
    {
      return -1;
    }
  if (load_options (s, in) != 0)
    {
      return -1;
    }
  if (load_locations (s, in, &errortext) != 0)
    {
      return -1;
    }
  if (load_misc (s, in) != 0)
    {
      return -1;
    }


  fclose (in);
  if (errortext != NULL)
    {
      create_error_window(_("Warning"),errortext);
    }

  g_free(errortext);

  s->changed = FALSE;

  return 0;
}

/* ************************************************************************* */
/* Save                                                                      */
/* ************************************************************************* */




static int
print_disk (gchar * s, gpointer * fp)
{
  FILE *out;
  out = (FILE *) fp;
  fprintf (out, "%s", s);
  return 0;
}



int
save_desc (gchar * name, file_s *s)
{
  FILE *in;
  in = fopen (name, "w");
  if (in == NULL)
    return -1;
  if (print_table (s, (gpointer *) in, print_disk) != 0)
    {
      fclose (in);
      return -1;
    }
  fclose (in);
  return 0;
}



/* ************************************************************************* */
/* Printing                                                                  */
/* ************************************************************************* */


static void
print_i18n (GList * dubbel, gint veld,
	    int (*print_function) (gchar *, gpointer *), gpointer * d)
{
  GList *loop;
  doublestring *ds;
  gchar buf[1000];
  if (dubbel == NULL)
    return;
  loop = dubbel;		/* just to be sure */
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      ds = loop->data;
      switch (veld)
	{
	case 1:
	  sprintf (buf, "\t\t\ti18n_%s=%s\n", ds->locale, ds->name);
	  break;
	case 2:
	  if (ds->locale != NULL)
	    {
	      if (strlen (ds->locale) != 0)
		{
		  sprintf (buf, "\t\t\tdescription_%s=%s\n", ds->locale,
			   ds->name);
		}
	      else
		{
		  sprintf (buf, "\t\t\tdescription=%s\n", ds->name);
		}
	    }
	  else
	    {
	      sprintf (buf, "\t\t\tdescription=%s\n", ds->name);
	    }
	  break;
	case 3:
	  sprintf (buf, "\t\t\t\ti18n_%s=%s\n", ds->locale, ds->name);
	  break;
	case 4:
	  if (ds->locale != NULL)
	    {
	      if (strlen (ds->locale) != 0)
		{
		  sprintf (buf, "\t\t\t\tdescription_%s=%s\n", ds->locale,
			   ds->name);
		}
	      else
		{
		  sprintf (buf, "\t\t\t\tdescription=%s\n", ds->name);
		}
	    }
	  else
	    {
	      sprintf (buf, "\t\t\t\tdescription=%s\n", ds->name);
	    }
	  break;
	case 5:
	  sprintf (buf, "\t\ti18n_%s=%s\n", ds->locale, ds->name);
	  break;
	case 6:
	  if (ds->locale != NULL)
	    {
	      if (strlen (ds->locale) != 0)
		{
		  sprintf (buf, "\t\tdescription_%s=%s\n", ds->locale,
			   ds->name);
		}
	      else
		{
		  sprintf (buf, "\t\tdescription=%s\n", ds->name);
		}
	    }
	  else
	    {
	      sprintf (buf, "\t\tdescription=%s\n", ds->name);
	    }
	  break;



	}
      print_function (buf, d);
      loop = loop->next;
    }

}



/* OK i need to use some functions for this */
/* note by Pitr: oh yeah... */
int print_table (file_s *s, gpointer * d,
		 int (*print_function) (gchar *, gpointer *))
{
	GList *trav;
	GList *fields;
	gchar buffer[1000];

	table *tab;
	field *field;
	subtable *stab;
	subfield *fld;
	misc_s *misc;
	location *loc;
	tlocation *tloc; 
	gchar *stemp;
	gint temp;
	doublestring *ds;


	trav = s->tables;
	g_list_first (trav);
	sprintf (buffer, "Begin tables\n");
	print_function (buffer, d);
	while (trav)
	{
		tab = trav->data;
		if (tab->alias != NULL && strlen (tab->alias) > 0)
			sprintf (buffer, "\t%s:%s\n", tab->name, tab->alias);
		else
			sprintf (buffer, "\t%s\n", tab->name);
		print_function (buffer, d);

		if (tab->fields != NULL)
		{
			fields = tab->fields;
			g_list_first (fields);
			while (fields)
			{
				field = fields->data;
				switch (field->type)
				{
					case T_STRING:
						sprintf (buffer, "\t\t%s:String\n", field->name);
						break;
					case T_STRINGS:
						sprintf (buffer, "\t\t%s:Strings\n", field->name);
						break;
					case T_INTEGER:
						sprintf (buffer, "\t\t%s:Integer\n", field->name);
						break;
					case T_REAL:
						sprintf (buffer, "\t\t%s:Real\n", field->name);
						break;
					case T_DECIMAL:
						sprintf (buffer, "\t\t%s:Decimal\n", field->name);
						break;
					case T_DATE:
						sprintf (buffer, "\t\t%s:Date\n", field->name);
						break;
					case T_BOOLEAN:
						sprintf (buffer, "\t\t%s:Boolean\n", field->name);
						break;
					case T_RECORD:
						sprintf (buffer, "\t\t%s:Record\n", field->name);
						break;
					case T_RECORDS:
						sprintf (buffer, "\t\t%s:Records\n", field->name);
						break;
					case T_MULTIMEDIA:
						sprintf (buffer, "\t\t%s:Multimedia\n", field->name);
						break;
					case T_FILE:
						sprintf (buffer, "\t\t%s:File\n", field->name);
						break;
				}

				print_function (buffer, d);
				print_i18n (field->i18n_name, 1, print_function, d);
				print_i18n (field->i18n_desc, 2, print_function, d);

				if (get_option(&field->properties,"DEFAULT")!= NULL)
				{
					sprintf (buffer, "\t\t\tdefault=%s\n", 
							get_option(&field->properties,"DEFAULT"));
					print_function (buffer, d);
				}


				if (get_option(&field->properties,"FORMAT")!= NULL)
				{
					sprintf (buffer, "\t\t\tformat=%s\n", 
							get_option(&field->properties,"FORMAT"));
					print_function (buffer, d);
				}

				if (get_option(&field->properties,"TYPE")!= NULL)
				{
					sprintf (buffer, "\t\t\ttype=%s\n", 
							get_option(&field->properties,"TYPE"));
					print_function (buffer, d);
				}


				fields = fields->next;
			}
		}
		trav = trav->next;
	}
	sprintf (buffer, "End\n");
	print_function (buffer, d);



	/* SUBTABLE PRINTING */


	sprintf (buffer, "\nBegin subtables\n");
	print_function (buffer, d);
	trav = s->subtables;
	trav = g_list_first (trav);
	while (trav)
	{
		stab = trav->data;
		sprintf (buffer, "\t%s:%s\n", stab->name, stab->name_of_table);
		print_function (buffer, d);

		print_i18n (stab->i18n_name, 5, print_function, d);
		print_i18n (stab->i18n_desc, 6, print_function, d);
		if (stab->viewable_as != NULL)
		{
			fields = stab->viewable_as;
			fields = g_list_first (fields);
			strcpy (buffer, "\t\tviewable as ");
			while (fields != NULL)
			{
				strcat (buffer, fields->data);
				strcat (buffer, ",");
				fields = fields->next;
			}
		}
		temp = strlen (buffer);

		buffer[temp + 1] = '\0';
		buffer[temp] = '\n';
		buffer[temp - 1] = '.';

		print_function (buffer, d);

		if (stab->subfields != NULL)
		{
			sprintf (buffer, "\t\twith only the following fields\n");
			print_function (buffer, d);
			fields = stab->subfields;
			fields = g_list_first (fields);

			while (fields != NULL)
			{
				fld = fields->data;
				sprintf (buffer, "\t\t\t%s:%s\n", fld->name_of_field, fld->name);
				print_function (buffer, d);
				print_i18n (fld->i18n_name, 3, print_function, d);
				print_i18n (fld->i18n_desc, 4, print_function, d);

				fields = fields->next;
			}
		}
		trav = trav->next;
	}

	sprintf (buffer, "End\n");
	print_function (buffer, d);

	/* ACTIONS */

	if (s->actions != NULL)
	{
		if (strlen(s->actions)!=0)
		{
			sprintf (buffer, "\nBegin actions\n");
			print_function (buffer, d);
			print_function (s->actions, d);
			sprintf (buffer, "End\n");
			print_function (buffer, d);
		}
	}

	/* Plugin options */


	if (s->plugin_options != NULL)
	{
		if (strlen(s->plugin_options)!=0)
		{
			sprintf (buffer, "\nBegin plugins_options\n");
			print_function (buffer, d);
			print_function (s->plugin_options, d);
			sprintf (buffer, "End\n");
			print_function (buffer, d);
		}
	}


  /* LOCATION PRINTING */



	if (s->tlocations != NULL)
	{
		sprintf(buffer,"\nBegin locations\n");
		print_function(buffer,d);
		trav = g_list_first(s->tlocations);
		while (trav != NULL)
		{
			tloc = trav->data;
			trav = g_list_next(trav);
			sprintf(buffer,"\t%s\n",tloc->name_of_table);
			print_function(buffer,d);
			fields = g_list_first(tloc->locations);
			while (fields != NULL)
			{
				loc = fields->data;
				fields = g_list_next(fields);
				sprintf(buffer,"\t\t%s\n",loc->path);
				print_function(buffer,d);
				stemp = get_option(&loc->prop,"FORMAT");
				if (stemp != NULL)
				{
					if (strcasecmp(stemp,"gaby")!=0)
					{
						sprintf(buffer,"\t\t\tformat=%s\n",stemp);
						print_function(buffer,d);
					}
				}
				stemp = get_option(&loc->prop,"REREAD");
				if (stemp != NULL)
				{
					if (atoi(stemp)>0)
					{
						sprintf(buffer,"\t\t\treread=%s\n",stemp);
						print_function(buffer,d);
					}
				}
				stemp = get_option(&loc->prop,"READONLY");
				if (stemp != NULL)
				{
					if (strcasecmp(stemp,"YES")==0)
					{
						sprintf(buffer,"\t\t\tread-only=%s\n",stemp);
						print_function(buffer,d);
					}
					/* options */
				}
			}
		}
	}
	if ( s->tlocations ) {
		sprintf (buffer, "End\n");
		print_function (buffer, d);
	}


	/* Misc printing */


	temp = 0;
	if (s->miscs != NULL)
	{
		sprintf (buffer, "\nBegin misc\n");
		print_function (buffer, d);


		trav = g_list_first(s->miscs);
		while (trav != NULL)
		{
			misc = trav->data;
			if (misc->main == TRUE)
			{
				sprintf (buffer, "\tMain window\n");
				print_function (buffer, d);
				sprintf (buffer, "\t\t%s:%s\n",misc->view_as,misc->name_of_subtable);
				print_function (buffer, d);
				if (misc->visible == TRUE)
					sprintf (buffer, "\t\t\tvisible = yes\n");
				else
					sprintf (buffer, "\t\t\tvisible = no\n");
				print_function (buffer, d);
				if ( misc->bind_to != NULL)
				{
					sprintf (buffer, "\t\t\tbind to\n");
					print_function (buffer, d);
					fields = g_list_first(misc->bind_to);
					while (fields!= NULL)
					{
						ds = fields->data;
						sprintf (buffer, "\t\t\t\t%s:%s\n",ds->locale,ds->name);
						print_function (buffer, d);
						fields = fields ->next;
					}
				}
			}
			trav = trav->next;
		}

		trav = g_list_first(s->miscs);
		while (trav != NULL)
		{
			misc = trav->data;
			if (misc->main == FALSE)
			{
				if (temp == 0) 
				{
					sprintf (buffer, "\tOther windows\n");
					print_function (buffer, d);
					temp = 1;
				}
				sprintf (buffer, "\t\t%s:%s\n",misc->view_as,misc->name_of_subtable);
				print_function (buffer, d);
				if (misc->visible == TRUE)
					sprintf (buffer, "\t\t\tvisible = yes\n");
				else
					sprintf (buffer, "\t\t\tvisible = no\n");
				print_function (buffer, d);
				if ( misc->bind_to != NULL)
				{
					sprintf (buffer, "\t\t\tbind to\n");
					print_function (buffer, d);
					fields = g_list_first(misc->bind_to);
					while (fields!= NULL)
					{
						ds = fields->data;
						sprintf (buffer, "\t\t\t\t%s:%s\n",ds->locale,ds->name);
						print_function (buffer, d);
						fields = fields ->next;
					}
				}
			}
			trav = trav->next;
		}

		sprintf (buffer, "End\n");
		print_function (buffer, d);

	}

	return 0;

}
