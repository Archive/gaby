/*
   Main routines

   Gaby Databases Builder
   Copyright (C) 1999  Ron Bessems
   Copyright (C) 2000  Frederic Peters

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */

#define DEBUG_MODE_DECLARED
int debug_mode;

#include "main.h"

#ifdef USE_GNOME
#  include "druid.h"
#endif

doublestring view_names[] = {
	{ "form",	N_("Form") },
	{ "list",	N_("List") },
	{ "xlist",	N_("Extended List") },
	{ "canvas",	N_("Canvas") },
	{ "gladeform",  N_("Glade Form") },
	{ "search",	N_("Search") },
	{ "filter",	N_("Filter") },
	{ NULL, 	NULL }
};

doublestring type_names[] = {
	{ "string",	N_("String") },
	{ "strings",	N_("Strings") },
	{ "integer",	N_("Integer") },
	{ "real",	N_("Real") },
	{ "date",	N_("Date") },
	{ "boolean",	N_("Boolean") },
	{ "multimedia",	N_("Multimedia") },
	{ "decimal",	N_("Decimal") },
	{ "file",	N_("File name") },
	/*{ "record",	N_("Record") },
	{ "records",	N_("Records") },*/
	{ NULL,		NULL }
};

doublestring format_names[] = {
	{ "gaby",	N_("Gaby (native)") },
	{ "gaby1",	N_("Gaby (old native)") },
	{ "videobase",	N_("Video Base") },
	{ "dpkg",	N_("Debian packages file") },
	{ "nosql",	N_("NoSQL") },
	{ "vcard",	N_("vCard") },
	{ "addressbook",N_("Address Book") },
	{ NULL,		NULL }
};

#ifdef DAMN_DOCU
void
test1()
{
	void *sec;
	char *secname;
	gnome_config_push_prefix("/foo");
	sec = gnome_config_init_iterator_sections("/foo");
	while ((sec=gnome_config_iterator_next(sec,&secname,NULL))!=NULL)
	{
		debug_print("Sec: %s\n",secname);
		g_free(secname);
	}
	gnome_config_pop_prefix();
}

#endif


int main (int argc, char *argv[])
{
	int i;

#ifdef ENABLE_NLS
	setlocale(LC_ALL,"");
	bindtextdomain(GETTEXT_PACKAGE,LOCALEDIR);
	textdomain(GETTEXT_PACKAGE);
	gtk_set_locale();
#endif

#ifdef USE_GNOME
	/* Damn gnome update your documents PLEASE */
	gnome_init("Builder", BVERSION, argc, argv);
#else
	gtk_init (&argc, &argv);
#endif

	i=0;
	while ( view_names[i++].name )
		view_names[i-1].locale = _(view_names[i-1].locale);
	
	i=0;
	while ( type_names[i++].name )
		type_names[i-1].locale = _(type_names[i-1].locale);
	
	i=0;
	while ( format_names[i++].name )
		format_names[i-1].locale = _(format_names[i-1].locale);
	
	multimedia_type = g_list_append (multimedia_type, "sound");
	multimedia_type = g_list_append (multimedia_type, "image");

	create_button_window (_("GDB - Gaby Databases Builder"));

	connect_to_session();

#ifdef USE_GNOME
	/* TODO (post-2.0): preference box to disable the druid
	 * delayed since nobody knows how to use the builder for now (sorry but
	 * I need a reason to delay it and was not able to find a good one)
	 * */
	launch_createdb_druid();
#else
	gtk_widget_show(Window);
#endif

	gtk_main ();
	return 0;

}
