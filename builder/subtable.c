
/*
   SubTable window 

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */





#include "main.h"
#ifndef NOTEBOOKVIEW
#include "icons/ja.xpm"
#endif
#include "icons/stock_add.xpm"
#include "icons/stock_remove.xpm"
#include "icons/edit.xpm"
#include "icons/i18n.xpm"

extern doublestring view_names[];

static GtkAccelGroup *accel;

typedef struct _update_main info1;

struct _update_main
  {
    GtkWidget *list;
    GtkWidget *window;
    file_s **s;
  };



static void
update_subtable_win(GtkWidget *widget, gpointer data)
{
  gint row,row2;
  GList *loop;
  GList *selection;
  GtkWidget *wid;
  info1 *info;
  file_s *s;
  subtable *stab;

  info = data;
  wid = info->list;
  s = *info->s;


  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    row = 0;
  else
    row = GPOINTER_TO_INT(selection->data);

  gtk_clist_freeze (GTK_CLIST (wid));
  gtk_clist_clear (GTK_CLIST (wid));

  loop = s->subtables;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      stab = loop->data;
      row2 = gtk_clist_append (GTK_CLIST (wid), &stab->name);
      gtk_clist_set_row_data (GTK_CLIST (wid), row2, stab);
      loop = loop->next;
    }
  gtk_clist_select_row (GTK_CLIST (wid), row, 0);
  gtk_clist_thaw (GTK_CLIST (wid));
  /* ADD THE VISIBILITY CHECK */


}




/* ************************************************************************ */
/* Events								    */
/* ************************************************************************ */

static void
rename_table_ok (gchar * name, gpointer data)
{
  GtkWidget *wid;
  info1 *info;
  GList *selection;
  subtable *tab;
  file_s *s;
  int row;

  info = data;
  wid = info->list;
  s= *info->s; 

  trim (name);
  if (check_name (name, 1) != 0)
    return;


  if (is_subtable_uniq(name,s->subtables)==FALSE)
  {
    error_dialog(
        _("Cannot rename the subtable because the name is used already."),
        _("Warning"));
    return;
  }


  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  tab = gtk_clist_get_row_data (GTK_CLIST (wid), row);

  if (tab == NULL && s == NULL)
    return;
  if (tab->name != NULL)
    {

      rename_subtables(s, tab->name, name);
      update_all();
      g_free (tab->name);
      tab->name = NULL;
    }
  tab->name = name;


}


static void
rename_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  GList *selection;
  subtable *tab;
  int row;

  list = info->list;
  
  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  tab = gtk_clist_get_row_data (GTK_CLIST (list), row);

  popup_dialog (rename_table_ok, _ ("Rename"),
		_ ("Enter Name of subtable"), tab->name, (gpointer)info);

}




static void
add_table_ok (gchar * name, gpointer data)
{
  GtkWidget *wid;
  info1 *info;
  subtable *tab;
  file_s *s;
  int row;

  info =data;
  wid =info->list;
  s = *info->s;

  trim (name);
  if (check_name (name, 0) != 0)
    return;

  if (strlen (name) == 0)
    return;


  if (is_subtable_uniq(name,s->subtables)==FALSE)
  {
    error_dialog(
        _("Cannot add the subtable because the name is used already."),
        _("Warning"));
    return;
  }



  /*  tab = g_malloc (sizeof (subtable)); */
  tab = new_subtable ();
  tab->name = name;
  tab->name_of_table = g_malloc (strlen (name) + 1);
  strcpy (tab->name_of_table, name);
  tab->subfields = NULL;
  tab->window = NULL;
  // tab->viewable_as = 0;
  row = gtk_clist_append (GTK_CLIST (wid), &name);
  gtk_clist_set_row_data (GTK_CLIST (wid), row, tab);

  s->changed = TRUE;
  s->subtables = g_list_append (s->subtables, tab);
  update_all();
}


static void
add_table_clicked (GtkWidget * widget, info1 *info)
{
 
  if (g_list_length((*info->s)->tables)==0)
  {
    error_dialog(_("There aren't any tables defined, you need\n"
		"them before subtables can be made."),_("Warning"));
    return;
  }

  popup_dialog (add_table_ok, _ ("Question"),
		_ ("Enter Name of new subtable"), NULL, (gpointer)info);
}

static void
edit_clicked (GtkWidget * widget, info1 *info) 
{
  GtkWidget *list;
  GList *selection;
  file_s *s;
  subtable *tab;
  gchar *temp;
  int row;

  list = info->list;
  s = *info->s;

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  tab = gtk_clist_get_row_data (GTK_CLIST (list), row);
  if (g_list_length(tab->viewable_as)==0)
  {
    temp = g_strdup(view_names[0].name);
    tab->viewable_as = g_list_append(tab->viewable_as,temp);
  }
  create_subfield_window (tab, s, tab->name);
}



static void
i18n_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  GList *selection;
  file_s *s;
  subtable *tab;
  int row;
 
  list = info->list; 
  s = *info->s;

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  tab = gtk_clist_get_row_data (GTK_CLIST (list), row);
  create_subtableprop_window (tab, s, tab->name);
}




static void
table_dclicked (GtkWidget * widget, GdkEventButton * event,
		info1 *info)
{
  if (event->type == GDK_2BUTTON_PRESS)
    {
      edit_clicked (widget, info);
    }
}




static void
delete_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  GList *selection;
  subtable *tab;
  file_s *s;
  int row;

  list = info->list;
  s = *info->s;

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  tab = gtk_clist_get_row_data (GTK_CLIST (list), row);

  delete_subtables(s, tab->name);

  delete_subtable (tab);
  s->subtables = g_list_remove (s->subtables, tab);
  s->changed = TRUE;
  gtk_clist_remove (GTK_CLIST (list), row);
  update_all();
}


#ifndef NOTEBOOKVIEW
static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, GtkWidget * wid)
{
  return FALSE;
}

static void
destroy (GtkWidget * widget, info1 *info)
{
  remove_update((*info->s)->subtablewin);
  (*info->s)->subtablewin = NULL;
  g_free(info);
}


static void
close_win (GtkWidget * wid, info1 *info)
{
  gtk_widget_destroy (info->window);
}

#endif

/* *********************************************************************** */
/* Create window 							   */
/* *********************************************************************** */

GtkWidget *
create_subtable_window (gchar * caption, file_s **f)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *tablelist;
  GtkWidget *addtable;
  GtkWidget *deltable;
  GtkWidget *edittable;
  GtkWidget *window;
  GtkWidget *lab;
  GtkWidget *frame;
  GtkWidget *scroll;
  int key;
  info1 *info;

  if ((*f)->subtablewin != NULL)
    {
      /* thank you gimp source */
      gdk_window_raise ((*f)->subtablewin->window);
      return (*f)->subtablewin;
    }

  info = g_malloc (sizeof(info1));

  /* The window */
#ifndef NOTEBOOKVIEW
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_policy(GTK_WINDOW(window),FALSE,TRUE,TRUE);
  gtk_window_set_title (GTK_WINDOW (window), caption);
  // gtk_object_set_user_data (GTK_OBJECT (window), f);
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_event), info);
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (destroy), info);
#else
  window = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(window),GTK_SHADOW_NONE);
#endif 

  (*f)->subtablewin = window;

  gtk_widget_show (window);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  hbox = gtk_hbox_new (FALSE, 5);


  accel = gtk_accel_group_new ();

  /* The main frame */
  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("Subtables"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);

  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);


  /* Clist */
  tablelist = gtk_clist_new (1);
  gtk_widget_set_usize (tablelist, 200, 100);
  gtk_widget_show (tablelist);
  gtk_widget_show (vbox);
  gtk_clist_set_selection_mode (GTK_CLIST (tablelist), GTK_SELECTION_BROWSE);
  // gtk_object_set_user_data (GTK_OBJECT (tablelist), f);
  gtk_signal_connect (GTK_OBJECT (tablelist), "button_press_event",
		      GTK_SIGNAL_FUNC (table_dclicked), info);
  scroll = scroll_new (tablelist);
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);

  /* The button frame */
  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("Options"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, TRUE, 5);

  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  /* The buttons */
  addtable = gtk_button_new ();
  lab = xpm_label_box (add_xpm, _ ("_Add Subtable"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addtable), lab);
  gtk_box_pack_start (GTK_BOX (vbox), addtable, FALSE, TRUE, 0);
  gtk_widget_show (addtable);
  gtk_signal_connect (GTK_OBJECT (addtable), "clicked",
		      GTK_SIGNAL_FUNC (add_table_clicked), info);
  gtk_widget_add_accelerator (addtable, "clicked", accel, key, 
			GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

  deltable = gtk_button_new ();
  lab = xpm_label_box (remove_xpm, _ ("_Delete Subtable"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (deltable), lab);
  gtk_box_pack_start (GTK_BOX (vbox), deltable, FALSE, TRUE, 0);
  gtk_widget_show (deltable);
  gtk_signal_connect (GTK_OBJECT (deltable), "clicked",
		      GTK_SIGNAL_FUNC (delete_clicked), info);
  gtk_widget_add_accelerator (deltable, "clicked", accel, key, 
			GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);



  deltable = gtk_button_new ();
  lab = xpm_label_box (edit_xpm, _ ("_Rename Subtable"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (deltable), lab);
  gtk_box_pack_start (GTK_BOX (vbox), deltable, FALSE, TRUE, 0);
  gtk_widget_show (deltable);
  gtk_signal_connect (GTK_OBJECT (deltable), "clicked",
		      GTK_SIGNAL_FUNC (rename_clicked), info);
  gtk_widget_add_accelerator (deltable, "clicked", accel, key, 
		GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);



  edittable = gtk_button_new ();
  lab = xpm_label_box (edit_xpm, _ ("_Edit Subtable"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (edittable), lab);
  gtk_box_pack_start (GTK_BOX (vbox), edittable, FALSE, TRUE, 0);
  gtk_widget_show (edittable);
  gtk_signal_connect (GTK_OBJECT (edittable), "clicked",
		      GTK_SIGNAL_FUNC (edit_clicked), info);
  gtk_widget_add_accelerator (edittable, "clicked", accel, key, 
		GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);



  edittable = gtk_button_new ();
  lab = xpm_label_box (i18n_xpm, _ ("_Edit i18n"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (edittable), lab);
  gtk_box_pack_start (GTK_BOX (vbox), edittable, FALSE, TRUE, 0);
  gtk_widget_show (edittable);
  gtk_signal_connect (GTK_OBJECT (edittable), "clicked",
		      GTK_SIGNAL_FUNC (i18n_clicked), info);
  gtk_widget_add_accelerator (edittable, "clicked", accel, key, 
			GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);




#ifndef NOTEBOOKVIEW

  edittable = gtk_button_new ();
  lab = xpm_label_box (ja_xpm, _ ("_Close"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (edittable), lab);

  gtk_box_pack_end (GTK_BOX (vbox), edittable, FALSE, TRUE, 0);
  gtk_widget_show (edittable);
  gtk_signal_connect (GTK_OBJECT (edittable), "clicked",
		      GTK_SIGNAL_FUNC (close_win), info);
  gtk_widget_add_accelerator (edittable, "clicked", accel, key, 
		GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

#endif


  gtk_widget_show (vbox);

  gtk_container_add (GTK_CONTAINER (window), hbox);
  gtk_widget_show (hbox);

  /*gtk_accel_group_attach (accel, GTK_OBJECT (window));*/

  info->list = tablelist; 
  info->window = window;
  info->s = f;

  add_update(info->window, info, update_subtable_win);
  update_all();
 
  return window;
}
