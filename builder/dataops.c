/*
   Data options

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Copyright (C) 2000  Frederic Peters

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */

#include "main.h"
extern doublestring view_names[];

/* ******************************************************************** */
/* properties options  (CASE SENSITIVE)                                 */
/* ******************************************************************** */

void
set_option(GList **properties, gchar *property, gchar *value)
{
  GList *loop;
  doublestring *ds;
  loop = g_list_first(*properties);
  
  while (loop != NULL)
  {
    ds = loop->data;
    if (strcmp(ds->name,property)==0)
    {
      if ( value == NULL)  /* If value == NULL then remove from list */
      {
	*properties = g_list_remove(*properties, ds);
	return;
      }
      g_free(ds->locale);  /* Change */
      ds->locale = g_strdup(value);
      return;
    }
    loop = loop->next;
  }
  /* No previous option found so add one */
  ds = g_malloc (sizeof (doublestring));
  ds->name = g_strdup (property);
  ds->locale = g_strdup(value);
  *properties = g_list_append(  *properties, ds);
  return;
}


gchar *
get_option(GList **properties, gchar *property)
{
  GList *loop;
  doublestring *ds;
  loop = g_list_first(*properties);
  
  while (loop != NULL)
  {
    ds = loop->data;
    if (strcmp(ds->name,property)==0)
    {
	return ds->locale;
    }
    loop = loop->next;
  }
  return NULL;
}






/* ******************************************************************** */
/* Update list functions                                                */
/* ******************************************************************** */


static void
subtable_tree_rename_fields (file_s *s, gchar *name_of_table, 
	gchar * old_name, gchar * new_name)
{
  subfield *sfld;
  subtable *stab;
  GList *loop;
  GList *loop2;
  if (s == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("Rename called on NULL file_struct. \n");
#endif
      return;
    }
  loop = s->subtables;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      stab = loop->data;
      if (stab == NULL)
	{
	  return;
	}
      if (strcmp(stab->name,name_of_table)==0)
      {
        loop2 = stab->subfields;
        loop2 = g_list_first (loop2);
        while (loop2 != NULL)
	  {
	    sfld = loop2->data;
	    if (sfld == NULL)
	      {
	        return;
	      }
      	    if (strcmp (sfld->name_of_field, old_name) == 0)
	      {
	        g_free (sfld->name_of_field);
	        sfld->name_of_field =
	  	   g_malloc (strlen (new_name) + 1);
	        strcpy (sfld->name_of_field, new_name);
	      }
	    loop2 = loop2->next;
	  }
        }
      loop = loop->next;
    }
}



static void
subtable_tree_rename_table
   (file_s *s, gchar * old_name, gchar * new_name)
{
  subtable *stab;
  GList *loop;
  if (s == NULL)
    {
      return;
    }
  loop = s->subtables;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      stab = loop->data;
      if (stab == NULL)
	{
	  return;
	}
      if (strcmp (stab->name_of_table, old_name) == 0)
	{
	  g_free (stab->name_of_table);
	  stab->name_of_table =
	    g_malloc (strlen (new_name) + 1);
	  strcpy (stab->name_of_table, new_name);
	}
      loop = loop->next;
    }
}

static void
misc_tree_rename_subtable(file_s *s, gchar *old_name, gchar *new_name)
{
  misc_s *misc;
  GList *loop;
  if (s == NULL)
    {
      return;
    }
  loop = s->miscs;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      misc = loop->data;
      if (misc == NULL)
	{
	  return;
	}
      if (strcmp (misc->name_of_subtable, old_name) == 0)
	{
	  g_free (misc->name_of_subtable);
	  misc->name_of_subtable =
	    g_malloc (strlen (new_name) + 1);
	  strcpy (misc->name_of_subtable, new_name);
	}
      loop = loop->next;
    }
}


/* This needs to be called for every misc_s struct */
static void
bind_struct_rename_subtable(misc_s *misc, gchar *oldname, gchar *newname)
{
  GList *loop;
  doublestring *ds;

  
  if (misc == NULL)
    return;

  loop = g_list_first(misc->bind_to);
  while (loop!=NULL)
  {
    ds = loop->data;
    if ( strcmp(ds->name,oldname)==0)
    {
      g_free(ds->name);
      ds->name = g_strdup(newname);
    }
    loop = loop->next;
  }
}

static void
bind_tree_rename_subtable(file_s *s, gchar *oldname, gchar *newname)
{
  GList *loop;
  misc_s *misc;

  loop = g_list_first(s->miscs);
  while (loop != NULL)
  {
    misc = loop->data;
    bind_struct_rename_subtable(misc, oldname, newname);
    loop = loop->next;   
  } 
}


static void
bind_struct_delete_subtable(misc_s *s, gchar *name)
{
  GList *loop; 
  doublestring *ds;

  loop = g_list_first(s->bind_to);
  while (loop != NULL)
  {
    ds = loop->data;
    if (strcmp(name, ds->name)==0)
    {
      s->bind_to = g_list_remove( s->bind_to, ds);
      delete_i18n(ds);
      loop = g_list_first(s->bind_to);      

    }
    else
      loop = loop->next;
  }
}

static void
bind_tree_delete_subtable(file_s *s, gchar *name)
{
  GList *loop;
  misc_s *misc;

  loop = g_list_first(s->miscs);
  while (loop != NULL)
  {
    misc = loop->data;
    bind_struct_delete_subtable(misc, name);
    loop = loop->next;   
  } 
}





static void
bind_struct_delete_view_as(misc_s *s, gchar *name, gchar *view)
{
  GList *loop;
  doublestring *ds;

  loop = g_list_first(s->bind_to);
  while (loop != NULL)
  {
    ds = loop->data;
    if (strcmp(name,ds->name)==0 && strcmp(view, ds->locale)==0)
    {
      s->bind_to = g_list_remove(s->bind_to, ds);
      delete_i18n(ds);
      loop = g_list_first(s->bind_to);
    }    
    else
      loop = loop->next;
  }
}

static void 
bind_tree_delete_view_as (file_s *s, gchar *name, gchar *view)
{
  GList *loop;
  misc_s *misc;
  
  loop = g_list_first(s->miscs);
  while (loop != NULL)
  {
    misc = loop->data;
    bind_struct_delete_view_as(misc, name, view);
    loop = loop->next;
  }
}


static void 
misc_tree_delete_subtable (file_s *s, gchar *name)
{
  misc_s *misc;
  misc_s *misc2;
  GList *loop;
  if (s == NULL)
    {
      return;
    }
  loop = s->miscs;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      misc = loop->data;
      if (misc == NULL)
	{
	  return;
	}
      if (strcmp (misc->name_of_subtable, name) == 0)
	{
		s->miscs = g_list_remove(s->miscs,misc);
 		loop = g_list_first( s->miscs);
		/* Make sure there is a main */
		if ( misc->main == TRUE && loop != NULL)
		{
		  misc2 = loop->data;
		  misc2->main = TRUE;
		}  		
		delete_misc_s(misc);
	}
      else
        loop = loop->next;
    }
}


static void
tlocation_tree_delete_table(file_s *s, gchar *name)
{
  GList *loop;
  tlocation *tloc;

  loop = g_list_first(s->tlocations);
  while (loop!=NULL)
  {
    tloc = loop->data;
    if (strcmp(tloc->name_of_table, name)==0)
      {
         s->tlocations = g_list_remove(s->tlocations,tloc);
         loop = g_list_first(s->tlocations);
         delete_tlocation(tloc);
      }
    loop = loop->next;
  }
}

static void
tlocation_tree_rename_table(file_s *s, gchar *old_name, gchar *new_name)
{
  GList *loop;
  tlocation *tloc;

  loop = g_list_first(s->tlocations);
  while (loop!=NULL)
  {
    tloc = loop->data;
    if (strcmp(tloc->name_of_table, old_name)==0)
      {
        g_free(tloc->name_of_table);
        tloc->name_of_table = g_strdup (new_name);
      }
    loop = loop->next;
  }
}









/* This needs to be called for every misc_s struct */
static void
bind_struct_rename_view_as(misc_s *misc, gchar *name, gchar *view)
{
  GList *loop;
  doublestring *ds;

  
  if (misc == NULL)
    return;

  loop = g_list_first(misc->bind_to);
  while (loop!=NULL)
  {
    ds = loop->data;
    if ( strcmp(ds->name, name )==0)
    {
      g_free(ds->locale);
      ds->locale = g_strdup(view);
    }
    loop = loop->next;
  }
}

static void
bind_tree_rename_view_as(file_s *s, gchar *name, gchar *viewas)
{
  GList *loop;
  misc_s *misc;

  loop = g_list_first(s->miscs);
  while (loop != NULL)
  {
    misc = loop->data;
    bind_struct_rename_view_as(misc, name, viewas);
    loop = loop->next;   
  } 
}










void delete_view_as(file_s *s, gchar *name_of_subtable, gchar *view_as)
{
  bind_tree_delete_view_as(s, name_of_subtable, view_as);
}

void
reset_view_as(file_s *s, gchar *name_of_subtable, gchar *view_as)
{
  bind_tree_rename_view_as(s, name_of_subtable, view_as);
}


void
rename_subtables(file_s *s, gchar *old_name, gchar *new_name)
{
  misc_tree_rename_subtable(s, old_name, new_name);
  bind_tree_rename_subtable(s, old_name, new_name);
}

void 
delete_subtables(file_s *s, gchar *name)
{
  bind_tree_delete_subtable(s, name); 
  misc_tree_delete_subtable(s, name);
}

void
delete_tables(file_s *s, gchar *name)
{
  /* subtables_tree_delete_table maybe ??? */
  tlocation_tree_delete_table(s, name);
}

void
rename_tables(file_s *s, gchar *old_name, gchar *new_name)
{
  subtable_tree_rename_table(s, old_name, new_name);
  tlocation_tree_rename_table(s, old_name, new_name);
}

void 
rename_fields(file_s *s, gchar *name_of_table, gchar *old_name, 
              gchar *new_name)
{
  
  subtable_tree_rename_fields(s,name_of_table,old_name, new_name);

}



/* ******************************************************************** */
/* New struct                                                           */
/* ******************************************************************** */


location *
new_location()
{
  location *loc;
#ifdef DEBUG_GABY
  debug_print ("Creating new location\n");
#endif
  loc = g_malloc (sizeof (location));
  loc->path = NULL;
  loc->prop = NULL;
  loc->window = NULL;
  return loc;
} 


tlocation *
new_tlocation()
{
  tlocation *tloc;
 #ifdef DEBUG_GABY
  debug_print ("Creating new tlocation\n");
#endif
  tloc = g_malloc (sizeof (tlocation));
  tloc->name_of_table = NULL;
  tloc->locations = NULL;
  tloc->window = NULL;
  return tloc;
} 




misc_s *
new_misc_s()
{
  misc_s *s;
 #ifdef DEBUG_GABY
  debug_print ("Creating new misc_s\n");
#endif
  s = g_malloc (sizeof (misc_s));
  s->name_of_subtable = NULL;
  s->view_as = NULL;
  s->bind_to = NULL;
  s->visible = TRUE;
  s->main = FALSE;
  s->window = NULL;
  return s;
} 


file_s *
new_desc ()
{
  file_s *s;
#ifdef DEBUG_GABY
  debug_print ("Creating new file_s\n");
#endif
  s = g_malloc (sizeof (file_s));
  s->preview = NULL;
  s->table = NULL;
  s->subtablewin = NULL;
  s->locationwin = NULL;
  s->actionwin = NULL;
  s->miscwin = NULL;
  s->name = NULL;
  s->tables = NULL;
  s->subtables = NULL;
  s->tlocations = NULL;
  s->actions = NULL;
  s->plugin_options = NULL;
  s->miscs = NULL;
  s->changed = FALSE;
  return s;
}


table *
new_table ()
{
  table *tab;
#ifdef DEBUG_GABY
  debug_print ("Creating new table\n");
#endif
  tab = g_malloc (sizeof (table));
  tab->name = NULL;
  tab->fields = NULL;
  tab->window = NULL;
  tab->alias = NULL;
  return tab;
}

subtable *
new_subtable ()
{
  subtable *tab;
#ifdef DEBUG_GABY
  debug_print ("Creating new subtable\n");
#endif
  tab = g_malloc (sizeof (subtable));
  tab->name = NULL;
  tab->name_of_table = NULL;
  /* initialize this on a default value */
  tab->viewable_as = NULL;
  tab->viewable_as = g_list_append(tab->viewable_as,
		g_strdup(view_names[0].name));

  tab->subfields = NULL;
  tab->propwindow = NULL;
  tab->window = NULL;
  tab->i18n_name = NULL;
  tab->i18n_desc = NULL;
  return tab;
}

field *
new_field ()
{
  field *fld;
#ifdef DEBUG_GABY
  debug_print ("Creating new field\n");
#endif
  fld = g_malloc (sizeof (field));
  fld->name = NULL;
  fld->properties = NULL;
  fld->i18n_name = NULL;
  fld->i18n_desc = NULL;
  fld->type = T_STRING;
  fld->window = NULL;
  return fld;
}


subfield *
new_subfield ()
{
  subfield *fld;
#ifdef DEBUG_GABY
  debug_print ("Creating new subfield\n");
#endif
  fld = g_malloc (sizeof (field));
  fld->name = NULL;
  fld->name_of_field = NULL;
  fld->i18n_name = NULL;
  fld->i18n_desc = NULL;
  fld->window = NULL;
  return fld;
}

doublestring *
new_i18n ()
{
  doublestring *ds;
#ifdef DEBUG_GABY
  debug_print ("Creating new dubblestring\n");
#endif
  ds = g_malloc (sizeof (doublestring));
  ds->name = NULL;
  ds->locale = NULL;
  return ds;

}

/* ******************************************************************** */
/* Delete struct                                                        */
/* ******************************************************************** */

void 
delete_location(location *loc)
{
  if (loc==NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("Delete called on NULL pointer location\n");
#endif
      return;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete location\n");
#endif

  if (loc->window != NULL)
    gtk_widget_destroy (loc->window);
  g_free (loc->path);
  loc->prop = free_i18n (loc->prop);
}

void
delete_tlocation(tlocation *tloc)
{
  if (tloc==NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("Delete called on NULL pointer tlocation\n");
#endif
      return;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete tlocation: %s\n", tloc->name_of_table);
#endif

  if (tloc->window != NULL)
    gtk_widget_destroy (tloc->window);
  g_free (tloc->name_of_table);
  tloc->locations =  free_locations(tloc->locations);
 
}
 

void 
delete_misc_s(misc_s *s)
{
  if (s == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("Delete called on NULL pointer misc_s\n");
#endif
      return;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete misc: %s\n", s->name_of_subtable);
#endif

  if (s->window != NULL)
    gtk_widget_destroy (s->window);

  s->bind_to = free_i18n (s->bind_to);

  g_free (s->name_of_subtable);
  g_free (s->view_as);
  g_free (s);
}



void
delete_table (table *tab)
{
  if (tab == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("Delete called on NULL pointer table\n");
#endif
      return;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete table: %s\n", tab->name);
#endif

  if (tab->window != NULL)
    gtk_widget_destroy (tab->window);
  tab->fields = free_fields (tab->fields);
  g_free (tab->name);
  g_free (tab->alias);
  g_free (tab);
}

void
delete_subtable (subtable *tab)
{
  GList *loop;
  if (tab == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("Delete called on NULL pointer subtable\n");
#endif
      return;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete subtable: %s\n", tab->name);
#endif
  if (tab->window != NULL)
    gtk_widget_destroy (tab->window);

  if (tab->propwindow != NULL)
    gtk_widget_destroy (tab->propwindow);



  free_subfields (tab->subfields);

  loop = tab->viewable_as;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      g_free (loop->data);
      loop = loop->next;
    }
  g_list_free (loop);

  tab->i18n_name = free_i18n (tab->i18n_name);
  tab->i18n_desc = free_i18n (tab->i18n_desc);
  g_free (tab->name);
  g_free (tab->name_of_table);
  g_free (tab);
}

void
delete_field (field *fld)
{
  if (fld == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("Delete called on NULL pointer field\n");
#endif
      return;
    }

  if (fld->window != NULL)
    gtk_widget_destroy (fld->window);
#ifdef DEBUG_GABY
  debug_print ("Delete field %s\n", fld->name);
#endif
  g_free (fld->name);
  fld->properties = free_i18n (fld->properties);
  fld->i18n_name = free_i18n (fld->i18n_name);
  fld->i18n_desc = free_i18n (fld->i18n_desc);
  g_free (fld);
}

void
delete_subfield (subfield *fld)
{
  if (fld == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("Delete called on NULL pointer field\n");
#endif
      return;
    }
  if (fld->window != NULL)
    gtk_widget_destroy (fld->window);
#ifdef DEBUG_GABY
  debug_print ("Field: %s\n", fld->name);
#endif
  g_free (fld->name);
  g_free (fld->name_of_field);
  fld->i18n_name = free_i18n (fld->i18n_name);
  fld->i18n_desc = free_i18n (fld->i18n_desc);
  g_free (fld);
}

void
delete_i18n (doublestring *ds)
{
  if (ds == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("Delete called on NULL pointer dubblestring\n");
#endif 
      return;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete i18n\n");
#endif

  g_free (ds->name);
  g_free (ds->locale);
  g_free (ds);

}


void
delete_desc (file_s *s)
{
  if (s == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("Delete desc called with struct s NULL\n");
#endif
      return;
    }

#ifndef NOTEBOOKVIEW
  if (s->table != NULL)
    gtk_widget_destroy (s->table);
 if (s->subtablewin != NULL)
    gtk_widget_destroy (s->subtablewin);
  if (s->actionwin != NULL)
    gtk_widget_destroy (s->actionwin);
  if (s->locationwin != NULL)
    gtk_widget_destroy (s->locationwin);
  if (s->miscwin != NULL)
    gtk_widget_destroy (s->miscwin);
#endif
  if (s->preview != NULL)
    gtk_widget_destroy (s->preview);
 
  free_tables (s->tables);
  free_subtables (s->subtables);
  free_misc_s(s->miscs);
  free_tlocations (s->tlocations);
  g_free (s->actions);
  g_free (s->plugin_options);
  g_free (s->name);
  g_free (s);


}


/* ******************************************************************** */
/* Delete list                                                          */
/* ******************************************************************** */



GList *
free_locations (GList * locs)
{
  GList *loop;
  if (locs == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("free_locs called on NULL glist\n");
#endif
      return NULL;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete GList locs\n");
#endif
  loop = locs;

  while (loop != NULL)
    {
      delete_location (loop->data);
      loop = loop->next;
    }
  g_list_free (locs);
  return NULL;
}


GList *
free_tlocations (GList * tlocs)
{
  GList *loop;
  if (tlocs == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("free_tlocs called on NULL glist\n");
#endif
      return NULL;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete GList tlocs\n");
#endif
  loop = tlocs;

  while (loop != NULL)
    {
      delete_tlocation (loop->data);
      loop = loop->next;
    }
  g_list_free (tlocs);
  return NULL;
}






GList *
free_misc_s (GList * miscs)
{
  GList *loop;
  if (miscs == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("free_misc_s called on NULL glist\n");
#endif
      return NULL;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete GList miscs\n");
#endif
  loop = miscs;
  while (loop != NULL)
    {
      delete_misc_s (loop->data);
      loop = loop->next;
    }
  g_list_free (miscs);
  return NULL;
}




GList *
free_tables (GList * tables)
{
  GList *loop;
  if (tables == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("free_tables called on NULL glist\n");
#endif
      return NULL;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete GList tables\n");
#endif
  loop = tables;
  while (loop != NULL)
    {
      delete_table (loop->data);
      loop = loop->next;
    }
  g_list_free (tables);
  return NULL;
}


GList *
free_subtables (GList * subtables)
{
  GList *loop;
  if (subtables == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("free_subtables called on NULL glist\n");
#endif
      return NULL;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete GList subtables\n");
#endif
  loop = subtables;
  while (loop != NULL)
    {
      delete_subtable (loop->data);
      loop = loop->next;
    }
  g_list_free (subtables);
  return NULL;
}


GList *
free_fields (GList * fields)
{
  GList *loop;
  if (fields == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("free_fields called on NULL glist\n");
#endif
      return NULL;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete GList fields\n");
#endif
  loop = fields;
  while (loop != NULL)
    {
      delete_field (loop->data);
      loop = loop->next;
    }
  g_list_free (fields);
  return NULL;

}

GList *
free_subfields (GList * subfields)
{
  GList *loop;
  if (subfields == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("free_fields called on NULL glist\n");
#endif
      return NULL;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete GList subfields\n");
#endif
  loop = subfields;
  while (loop != NULL)
    {
      delete_subfield (loop->data);
      loop = loop->next;
    }
  g_list_free (subfields);
  return NULL;
}


GList *
free_i18n (GList * i18n)
{
  GList *loop;
  if (i18n == NULL)
    {
#ifdef DEBUG_GABY
      debug_print ("free_i18n called on NULL glist\n");
#endif
      return NULL;
    }
#ifdef DEBUG_GABY
  debug_print ("Delete GList i18n\n");
#endif
  loop = i18n;
  while (loop != NULL)
    {
      delete_i18n (loop->data);
      loop = loop->next;
    }
  g_list_free (i18n);
  return NULL;
}
