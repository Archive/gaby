/*
   Table windows

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */


#include "main.h"
#ifndef NOTEBOOKVIEW
#include "icons/ja.xpm"
#endif
#include "icons/stock_add.xpm"
#include "icons/stock_remove.xpm"
#include "icons/edit.xpm"

static GtkAccelGroup *accel;

typedef struct _main_update info1;

struct _main_update
  {
    GtkWidget *list;
    GtkWidget *window;
    file_s **s;
  };



static void
update_table_win(GtkWidget *widget, gpointer data)
{
  info1 *info;
  GtkWidget *wid;
  gint row;
  gint row2;
  file_s *s;
  GList *loop;
  GList *selection;
  table *tab;
  info = data;
  wid = info->list;
  s = *info->s;

  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    row = 0;
  else
    row = GPOINTER_TO_INT(selection->data);

  gtk_clist_freeze (GTK_CLIST (wid));
  gtk_clist_clear (GTK_CLIST (wid));

  loop = s->tables;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      tab = loop->data;
      row2 = gtk_clist_append (GTK_CLIST (wid), &tab->name);
      gtk_clist_set_row_data (GTK_CLIST (wid), row2, tab);
      loop = loop->next;
    }
  gtk_clist_select_row (GTK_CLIST (wid), row, 0);
  gtk_clist_thaw (GTK_CLIST (wid));
 
}

/* ******************************************************************** */
/* Events                                                               */
/* ******************************************************************** */

static void
rename_table_ok (gchar * name, gpointer data)
{
  info1 *info;
  GtkWidget *wid;
  GList *selection;
  table *tab;
  file_s *s;
  int row;

  info =data;
  s = *info->s; 
  wid = info->list;

  trim (name);
  if (check_name (name, 1) != 0)
    return;

  if (is_table_uniq(name,s->tables)==FALSE)
  {
    error_dialog(
	_("Cannot rename the table because the name is used already."),
        _("Warning"));
    return;
  }


  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  tab = gtk_clist_get_row_data (GTK_CLIST (wid), row);

  if (tab == NULL && s == NULL)
    return;
  if (tab->name != NULL)
    {
      rename_tables (s, tab->name, name);	/* Rename all references */
      g_free (tab->name);
      tab->name = NULL;
    }
  tab->name = name;

/*
  gtk_clist_freeze (GTK_CLIST (wid));
  gtk_clist_clear (GTK_CLIST (wid));

  s->changed = TRUE;

  loop = s->tables;
  loop = g_list_first (loop);
  while (loop != NULL)
    {
      tab = loop->data;
      row2 = gtk_clist_append (GTK_CLIST (wid), &tab->name);
      gtk_clist_set_row_data (GTK_CLIST (wid), row2, tab);
      loop = loop->next;
    }
  gtk_clist_select_row (GTK_CLIST (wid), row, 0);
  gtk_clist_thaw (GTK_CLIST (wid));
*/

  /* ADD THE VISIBILITY CHECK */

  update_all ();
}


static void
rename_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  GList *selection;
  table *tab;
  file_s *s;
  int row;
 
  s = *info->s;
  list = info->list;
 
  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  tab = gtk_clist_get_row_data (GTK_CLIST (list), row);


  popup_dialog (rename_table_ok, _ ("Rename"),
		_ ("Enter Name of table"), tab->name, info);

}




static void
add_table_ok (gchar * name, gpointer data)
{
  info1 *info;
  GtkWidget *wid;
  table *tab;
  file_s *s;
  int row;

  info =data;
  s = *info->s;
  wid = info->list;

  trim (name);
  if (check_name (name, 0) != 0)
    return;

  if (strlen (name) == 0)
    return;

  if (is_table_uniq(name,s->tables)==FALSE)
  {
    error_dialog(_("Cannot add the table because the name is used already."),
                 _("Warning"));
    return;
  }

  tab = new_table ();
  tab->name = name;

  row = gtk_clist_append (GTK_CLIST (wid), &name);
  gtk_clist_set_row_data (GTK_CLIST (wid), row, tab);

  s->changed = TRUE;
  s->tables = g_list_append (s->tables, tab);
  update_all ();
}


static void
add_table_clicked (GtkWidget * widget, info1 *info)
{
  popup_dialog (add_table_ok, _ ("Question"),
		_ ("Enter Name of new table"), NULL, (gpointer )info);
}

static void
edit_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  GList *selection;
  file_s *s;
  table *tab;
  int row;

  list = info->list;
  s = *info->s;

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  tab = gtk_clist_get_row_data (GTK_CLIST (list), row);
  create_field_window (tab, s, tab->name);

}



static void
table_dclicked (GtkWidget * widget, GdkEventButton * event,
		info1 *info)
{
  if (event->type == GDK_2BUTTON_PRESS)
    {
      edit_clicked (widget, info);
    }
}




static void
delete_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  GList *selection;
  table *tab;
  file_s *s;
  int row;

  list = info->list;
  s = *info->s;
  

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  tab = gtk_clist_get_row_data (GTK_CLIST (list), row);

  delete_tables(s, tab->name);

  s->tables = g_list_remove (s->tables, tab);
  delete_table (tab);
  gtk_clist_remove (GTK_CLIST (list), row);
  
  s->changed= TRUE;

  update_all ();
}


#ifndef NOTEBOOKVIEW
static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, GtkWidget * wid)
{
  return FALSE;
}


static void
close_win (GtkWidget * wid, info1 *info)
{
  gtk_widget_destroy (info->window);
}


static void
destroy (GtkWidget * widget, info1 *info)
{
  remove_update(info->window); 
  (*info->s)->table = NULL; 
  g_free(info);
}



#endif

/* ******************************************************************** */
/* Create win                                                           */
/* ******************************************************************** */

GtkWidget *
create_table_window (gchar * caption, file_s **f)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *tablelist;
  GtkWidget *addtable;
  GtkWidget *deltable;
  GtkWidget *edittable;
  GtkWidget *window;
  GtkWidget *lab;
  GtkWidget *frame;
  GtkWidget *scroll;
  int key;
  info1 *info;

  if ((*f)->table != NULL)
    {
      /* thank you gimp source */
      gdk_window_raise ((*f)->table->window);
      return (*f)->table;
    }

  info = g_malloc(sizeof(info1));

  /* The window */
#ifndef NOTEBOOKVIEW
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), caption);
  gtk_window_set_policy(GTK_WINDOW(window),FALSE,TRUE,TRUE);
  (*f)->table = window;
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_event), info);
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (destroy), info);
#else
  window = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(window),GTK_SHADOW_NONE);
#endif 

  gtk_widget_show (window);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  hbox = gtk_hbox_new (FALSE, 5);


  accel = gtk_accel_group_new ();

  /* The main frame */
  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("Tables"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);

  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);


  /* Clist */
  tablelist = gtk_clist_new (1);
  gtk_widget_set_usize (tablelist, 200, 100);
  gtk_widget_show (tablelist);
  gtk_widget_show (vbox);
  gtk_clist_set_selection_mode (GTK_CLIST (tablelist), GTK_SELECTION_BROWSE);
  gtk_signal_connect (GTK_OBJECT (tablelist), "button_press_event",
		      GTK_SIGNAL_FUNC (table_dclicked), info);
  scroll = scroll_new (tablelist);
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);

  /* The button frame */
  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("Options"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, TRUE, 5);

  vbox = gtk_vbox_new (FALSE, 5);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  /* The buttons */
  addtable = gtk_button_new ();
  lab = xpm_label_box (add_xpm, _ ("_Add Table"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addtable), lab);
  gtk_box_pack_start (GTK_BOX (vbox), addtable, FALSE, TRUE, 0);
  gtk_widget_show (addtable);
  gtk_signal_connect (GTK_OBJECT (addtable), "clicked",
		      GTK_SIGNAL_FUNC (add_table_clicked), info);
  gtk_widget_add_accelerator (addtable, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);

  deltable = gtk_button_new ();
  lab = xpm_label_box (remove_xpm, _ ("_Delete Table"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (deltable), lab);
  gtk_box_pack_start (GTK_BOX (vbox), deltable, FALSE, TRUE, 0);
  gtk_widget_show (deltable);
  gtk_signal_connect (GTK_OBJECT (deltable), "clicked",
		      GTK_SIGNAL_FUNC (delete_clicked), info);
  gtk_widget_add_accelerator (deltable, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);



  deltable = gtk_button_new ();
  lab = xpm_label_box (edit_xpm, _ ("_Rename Table"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (deltable), lab);
  gtk_box_pack_start (GTK_BOX (vbox), deltable, FALSE, TRUE, 0);
  gtk_widget_show (deltable);
  gtk_signal_connect (GTK_OBJECT (deltable), "clicked",
		      GTK_SIGNAL_FUNC (rename_clicked), info);
  gtk_widget_add_accelerator (deltable, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);



  edittable = gtk_button_new ();
  lab = xpm_label_box (edit_xpm, _ ("_Edit Table"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (edittable), lab);
  gtk_box_pack_start (GTK_BOX (vbox), edittable, FALSE, TRUE, 0);
  gtk_widget_show (edittable);
  gtk_signal_connect (GTK_OBJECT (edittable), "clicked",
		      GTK_SIGNAL_FUNC (edit_clicked), info);
  gtk_widget_add_accelerator (edittable, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);


#ifndef NOTEBOOKVIEW
  edittable = gtk_button_new ();
  lab = xpm_label_box (ja_xpm, _ ("_Close"), 1, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (edittable), lab);

  gtk_box_pack_end (GTK_BOX (vbox), edittable, FALSE, TRUE, 0);
  gtk_widget_show (edittable);
  gtk_signal_connect (GTK_OBJECT (edittable), "clicked",
		      GTK_SIGNAL_FUNC (close_win), info);
  gtk_widget_add_accelerator (edittable, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);

#endif


  gtk_widget_show (vbox);

  gtk_container_add (GTK_CONTAINER (window), hbox);
  gtk_widget_show (hbox);

  /*gtk_accel_group_attach (accel, GTK_OBJECT (window));*/

  info->window = window;
  info->list = tablelist;
  info->s = f;
  add_update(window, info, update_table_win);
  update_all();
  return window; 

}
