/*
   Main window

   Gaby Databases Builder
   Copyright (C) 1999  Ron Bessems
   Copyright (C) 2000  Frederic Peters

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */





#include "main.h"

#include "icons/flag_s.xpm"
#include "icons/subtable_s.xpm"
#include "icons/table_s.xpm"
#include "icons/action_s.xpm"
#include "icons/ja_s.xpm"
#include "icons/eye2.xpm"

#ifndef NOTEBOOKVIEW
#include "icons/subtable.xpm"
#include "icons/eye.xpm"
#include "icons/flag.xpm"
#include "icons/table.xpm"
#include "icons/action.xpm"
#include "icons/ja.xpm"
#endif







struct buttons
  {
    GtkWidget *table;
    GtkWidget *subtable;
    GtkWidget *location;
    GtkWidget *action;
    GtkWidget *preview;
    GtkWidget *misc;
  };







file_s *s;
static struct buttons *but;
static GtkAccelGroup *accel; 
#ifdef USE_GNOME
gint Gnome_Ret_val=0;
#endif



void
enable_buttons ()
{
#ifndef NOTEBOOKVIEW
  gtk_widget_set_sensitive (but->table, TRUE);
  gtk_widget_set_sensitive (but->subtable, TRUE);
  gtk_widget_set_sensitive (but->action, TRUE);
  gtk_widget_set_sensitive (but->location, TRUE);
  gtk_widget_set_sensitive (but->preview, TRUE);
  gtk_widget_set_sensitive (but->misc, TRUE);
#endif
}




void
disable_buttons ()
{
#ifndef NOTEBOOKVIEW
  gtk_widget_set_sensitive (but->table, FALSE);
  gtk_widget_set_sensitive (but->subtable, FALSE);
  gtk_widget_set_sensitive (but->action, FALSE);
  gtk_widget_set_sensitive (but->location, FALSE);
  gtk_widget_set_sensitive (but->preview, FALSE);
  gtk_widget_set_sensitive (but->misc, FALSE);
#endif
}

/* ******************************************************************** */
/* Function exported for fileops                                        */
/* ******************************************************************** */

void 
cancel_x(gpointer data)
{
  debug_print("Cancel\n");
#ifdef USE_GNOME
  if (data!= NULL)
    gnome_interaction_key_return(*(int *)data,FALSE);
#endif
}

gint
load_x (gchar * name, gpointer data)
{
#ifdef DEBUG
  debug_print ("Load_x\n");
#endif
  /* delete the old one if the database loads */
  delete_desc (s);
  /* Set the pointer to NULL */
  /* Make room for the new struct */
  s = new_desc ();
  if (load_desc (name, s) != 0)
    {
      error_dialog (_ ("Cannot read the file, please check for \n" 
		       "read permissions"), _ ("Error"));
      return -1;
    }

  s->name = g_malloc (strlen (name) + 1);
  strcpy (s->name, name);
  enable_buttons ();
  update_all();
  return 0;
}

void
close_x (gpointer data)
{
#ifdef DEBUG
  debug_print ("close_x\n");
#endif
  delete_desc (s);
  s = NULL;
  disable_buttons ();

#ifdef USE_GNOME
  /* This one come from the session managent stuff */
  if (data != NULL)
  {
    debug_print("Ret val !!!\n");
    gnome_interaction_key_return(*(int *)data,FALSE);
  }
#endif


}

gint
save_x (gchar * name, gpointer data)
{
  gchar buffer[1000];
  gchar *sla;
#ifdef DEBUG
  debug_print ("Save_x\n");
#endif

  if (save_desc (name, s) != 0)
    {
      error_dialog (_ ("Cannot save the file, please check for \n" 
		       "write permissions"), _ ("Error"));
      return -1;
    }
  s->name = g_malloc (strlen (name) + 1);
  strcpy (s->name, name);
  sla = strrchr (s->name, '/') + 1;
  strcpy (buffer, _ ("Table: "));
  strcat (buffer, sla);
  if (s->table != NULL)
    {
      gtk_window_set_title (GTK_WINDOW (s->table), buffer);
    }
  if (s->preview != NULL)
    {
      gtk_window_set_title (GTK_WINDOW (s->preview), buffer);
    }


  return 0;
}

void
new_x (gpointer data)
{
#ifdef DEBUG
  debug_print ("new_x\n");
#endif
  delete_desc (s);
  s = NULL;
  s = new_desc (s);
  enable_buttons ();
  update_all();
}


void
stop_x (gpointer data)
{
  GtkWidget *w;
#ifdef DEBUG
  debug_print ("stop_x\n");
#endif



  if (s != NULL)
    delete_desc (s);
  w = data;
  gtk_widget_destroy (w);
  gtk_main_quit ();
}

gint
get_file_changed (gpointer data)
{
  if (s != NULL)
    return (s->changed == TRUE ? 1 : 0);
  else
    return 0;
}

void
set_file_changed (gint ch, gpointer data)
{
  if (s != NULL)
    {
      s->changed = (ch == 1 ? TRUE : FALSE);
    }
}

gchar *
get_file_name (gpointer data)
{
  if (s != NULL)
    return s->name;
  else
    return NULL;

}







/* ******************************************************************** */
/* Events                                                               */
/* ******************************************************************** */

static void
about_clicked (GtkWidget * wid, GtkWidget * list)
{
  /* ONe UGLY about box */
#ifdef USE_GNOME_1
  GtkWidget *about;
  const gchar *authors[]= {
	"Ron Bessems <R.E.M.W.Bessems@stud.tue.nl>",
	"Frederic Peters <fpeters@theridion.com>",
	NULL
	};

  about = gnome_about_new("Builder",BVERSION,
			  "(c) 1999 Ron Bessem, 2000 Frederic Peters",
			  authors,
			  _("The Gaby database builder"),
			  NULL);
  gtk_widget_show(about);



#else
  gchar buffer[1000];		/* enough for all this bla */
  sprintf (buffer, _ ("Gaby Description File builder version %s\n" 
                      "Copyright (c) 1999 Ron Bessems\n"
		      "Copyright (c) 2000 Frederic Peters\n"
                      "GNU General Public License\n\n"
		      "Authors:\nRon Bessems <R.E.M.W.Bessems@stud.tue.nl>" 
		      "\nFrederic Peters <fpeters@theridion.com>" 
		      "\n(by alphabetical order)" 
	   ), BVERSION);
  error_dialog (buffer, _("About"));
#endif
}

#ifdef TODO_MAKE_WINDOWS_AWARE_OF_A_NULL_DESC
static void
close_desc (GtkWidget * w, GtkWidget * wid)
{
  close_fileop (NULL);
}
#endif

static void
new_clicked (GtkWidget * wid, GtkWidget * list)
{
  new_fileop (NULL);
}


static void
file_open_clicked (GtkWidget * wid, GtkWidget * list)
{
  open_fileop (NULL);
}


static void
file_save_clicked (GtkWidget * wid, GtkWidget * list)
{
  if (s != NULL)
    save_fileop (NULL);
}

static void
file_save_as_clicked (GtkWidget * wid, GtkWidget * list)
{
  if (s != NULL)
  {
    save_as_fileop (NULL);
  }
}



static void
preview_clicked (GtkWidget * widget, GtkWidget * list)
{
  gchar buffer[1000];
  gchar *name;
  if (s == NULL)
    {
      return;
    }
  if (s->name == NULL)
    create_preview_window (_("Preview"), s);
  else
    {
      strcpy (buffer, _("Preview: "));
      name = strrchr (s->name, '/');
      strcat (buffer, name + 1);
      create_preview_window (buffer, s);
    }

}


static void
run_gaby_clicked (GtkWidget * widget, GtkWidget * list)
{
  gchar buffer[1000];

  if (s == NULL)
    {
      return;
    }

  if (g_list_length(s->subtables)==0)
  {
    error_dialog(_("You must define at least one subtable\nbefore you "
                   "can use gaby."),_("Warning"));
    return;
  }

  

  if (s->name==NULL)
    {
      error_dialog(_("Save the description file before running gaby."),
		   _("Warning"));
      return;
    } 

  save_fileop(NULL);

  buffer[0]=0;

  sprintf(buffer,"gaby --as %s &",strrchr(s->name,'.')+1);
  system(buffer);
}


#ifndef NOTEBOOKVIEW
static void
gaby_clicked (GtkWidget * widget, GtkWidget * list)
{
  gchar buffer[1000];
  gchar *name;

  if (s == NULL)
    {
      return;
    }

  if (g_list_length(s->subtables)==0)
  {
    error_dialog(_("You must define at least one subtable\nbefore you "
                   "can use gaby."),_("Warning"));
    return;
  }

  name = getenv("HOME");
  buffer[0]=0;
  strcpy(buffer,name);
  strcat(buffer,"/.gaby/desc.ihopethisnameisnotused1");
  if (save_desc(buffer,s)!=0)
	debug_print(_("Cannot save to the default path\n"));
  system("gaby --as ihopethisnameisnotused1 &");
}





static void
edit_table_clicked (GtkWidget * widget, GtkWidget * list)
{
  gchar buffer[1000];
  gchar *name;
  if (s == NULL)
    {
      s = new_desc (s);
    }
  if (s->name == NULL)
    create_table_window (_("Tables"), &s);
  else
    {
      strcpy (buffer, _("Tables: "));
      name = strrchr (s->name, '/');
      strcat (buffer, name + 1);
      create_table_window (buffer, &s);
    }

}

static void
subtable_clicked (GtkWidget * widget, GtkWidget * list)
{
  gchar buffer[1000];
  gchar *name;
  if (s == NULL)
    {
      s = new_desc (s);
    }
  if (s->name == NULL)
    create_subtable_window (_("Subtables"), &s);
  else
    {
      strcpy (buffer, _("Subtables: "));
      name = strrchr (s->name, '/');
      strcat (buffer, name + 1);
      create_subtable_window (buffer, &s);
    }

}


static void
action_clicked (GtkWidget * widget, GtkWidget * list)
{
  gchar buffer[1000];
  gchar *name;
  if (s == NULL)
    {
      s = new_desc (s);
    }
  if (s->name == NULL)
    create_action_window (_("Actions"), &s);
  else
    {
      strcpy (buffer, _("Actions: "));
      name = strrchr (s->name, '/');
      strcat (buffer, name + 1);
      create_action_window (buffer, &s);
    }

}

/* This will be the help function may spawn a web browser or 
   something */
#ifdef USE_WEB
static void
doc_clicked (GtkWidget * widget, GtkWidget * list)
{
  
}
#endif


static void
location_clicked (GtkWidget * widget, GtkWidget * list)
{
  gchar buffer[1000];
  gchar *name;
  if (s == NULL)
    {
      s = new_desc (s);
    }
  if (s->name == NULL)
    create_tlocation_window (_("Table locations"), &s);
  else
    {
      strcpy (buffer, _("Table locations: "));
      name = strrchr (s->name, '/');
      strcat (buffer, name + 1);
      create_tlocation_window (buffer, &s);
    }

}



static void
misc_clicked (GtkWidget * widget, GtkWidget * list)
{
  gchar buffer[1000];
  gchar *name;
  if (s == NULL)
    {
      s = new_desc (s);
    }
  if (s->name == NULL)
    create_misc_window (_("misc"), &s);
  else
    {
      strcpy (buffer, _("misc: "));
      name = strrchr (s->name, '/');
      strcat (buffer, name + 1);
      create_misc_window (buffer, &s);
    }

}


#endif


static gboolean
delete_event (GtkWidget * widget, GtkWidget * wid)
{
  close_prog_fileop ((gpointer) widget);
  return TRUE;
}

static void
destroy (GtkWidget * widget, gpointer * data)
{
#ifdef DEBUG
  debug_print ("button win destroy\n");
#endif
}


static void
close_win (GtkWidget * wid, GtkWidget * window)
{
  close_prog_fileop ((gpointer) Window);
}



static void
drag_data_received (GtkWidget * widget, GdkDragContext * contex,
		    gint x, gint y,
		    GtkSelectionData * selection_data, guint info,
		    guint time)
{
  gchar *p;
#ifdef DEBUG
  debug_print ("Drag & Drop event\n");
#endif
  p = selection_data->data;
  drop_open_fileop (NULL, p);
}



/* ******************************************************************** */
/* GUI building                                                         */
/* ******************************************************************** */


#ifdef USE_GNOME
static GnomeUIInfo toolbar[]={
        { GNOME_APP_UI_ITEM, N_("New"), N_("Opens a new file"),new_clicked,NULL,NULL,
        GNOME_APP_PIXMAP_STOCK,GNOME_STOCK_PIXMAP_NEW,0,0,NULL},
        { GNOME_APP_UI_ITEM, N_("Open"), N_("Opens a file"),file_open_clicked,NULL,NULL,
        GNOME_APP_PIXMAP_STOCK,GNOME_STOCK_PIXMAP_OPEN,0,0,NULL},
        { GNOME_APP_UI_ITEM, N_("Save"), N_("Save the file"),file_save_clicked,NULL,NULL,
        GNOME_APP_PIXMAP_STOCK,GNOME_STOCK_PIXMAP_SAVE,0,0,NULL},
        { GNOME_APP_UI_ITEM, N_("Save as"), N_("Saves the file under a different name"),file_save_as_clicked,NULL,NULL,
        GNOME_APP_PIXMAP_STOCK,GNOME_STOCK_PIXMAP_SAVE_AS,0,0,NULL},
        GNOMEUIINFO_SEPARATOR,
/*
        { GNOME_APP_UI_ITEM, N_("Add"), N_("Add a record"),add_record,NULL,NULL,        GNOME_APP_PIXMAP_STOCK,GNOME_STOCK_PIXMAP_ADD,0,0,NULL},

        { GNOME_APP_UI_ITEM, N_("Remove"), N_("Remove this record"),remove_record,NULL,NULL,
        GNOME_APP_PIXMAP_STOCK,GNOME_STOCK_PIXMAP_REMOVE,0,0,NULL},
*/

        { GNOME_APP_UI_ITEM, N_("Gaby"), N_("Save & Run gaby"),run_gaby_clicked,NULL,NULL,
        GNOME_APP_PIXMAP_DATA,eye2_xpm,0,0,NULL},
        GNOMEUIINFO_SEPARATOR,
        { GNOME_APP_UI_ITEM, N_("Exit"), N_("Close this app"),delete_event,NULL,NULL,
        GNOME_APP_PIXMAP_STOCK,GNOME_STOCK_PIXMAP_EXIT,0,0,NULL},
        GNOMEUIINFO_END
};












static GnomeUIInfo file_menu[]= {
  GNOMEUIINFO_MENU_NEW_ITEM("_New","Create a new database",new_clicked,NULL),
  GNOMEUIINFO_MENU_OPEN_ITEM(file_open_clicked,NULL),
  GNOMEUIINFO_MENU_SAVE_ITEM(file_save_clicked,NULL),
  GNOMEUIINFO_MENU_SAVE_AS_ITEM(file_save_as_clicked,NULL),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_NEW_ITEM("_Preview","Preview the description file",
			preview_clicked,NULL),
  GNOMEUIINFO_MENU_NEW_ITEM("_Save & Run Gaby","Save and Run gaby",
			run_gaby_clicked,NULL),
  GNOMEUIINFO_SEPARATOR,
#ifndef NOTEBOOKVIEW
  GNOMEUIINFO_MENU_CLOSE_ITEM(close_desc,NULL),
#endif
  GNOMEUIINFO_MENU_EXIT_ITEM(close_win,NULL),
  GNOMEUIINFO_END
};
  

static GnomeUIInfo about_menu[]={
  GNOMEUIINFO_MENU_ABOUT_ITEM(about_clicked,NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[]={
  GNOMEUIINFO_SUBTREE("_File",&file_menu),
  GNOMEUIINFO_SUBTREE("_Help",&about_menu),
  GNOMEUIINFO_END
};
#endif


#ifndef USE_GNOME
GtkWidget *
create_menu_item (char *labeltext, GtkAccelGroup * accel_group)
{
  GtkWidget *label;
  GtkWidget *menuitem;
  gint key;


  menuitem = gtk_menu_item_new ();
  label = gtk_accel_label_new ("");
  key = gtk_label_parse_uline (GTK_LABEL (label), labeltext);
  if (key < 255 && key > 0)
    {
      gtk_accel_label_set_accel_widget (GTK_ACCEL_LABEL (label), menuitem);
      gtk_widget_add_accelerator (menuitem, "activate", accel_group, 
				  key, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);
    }
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 1);
  gtk_container_add (GTK_CONTAINER (menuitem), label);
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_widget_show (label);

  return menuitem;

}
#endif

#ifndef USE_GNOME
GtkWidget *
create_menu (GtkWidget * window, GtkAccelGroup * accel)
{
  GtkWidget *menu;
  GtkWidget *menu_item;
  GtkWidget *file_item;
  GtkWidget *menu_bar;
  GtkWidget *lab;
  int key;

  menu_bar = gtk_menu_bar_new ();

  menu = gtk_menu_new ();

  menu_item = create_menu_item (_ ("_New"), accel);
  gtk_menu_append (GTK_MENU (menu), menu_item);
  gtk_widget_show (menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      GTK_SIGNAL_FUNC (new_clicked), NULL);

  menu_item = create_menu_item (_ ("_Open"), accel);
  gtk_menu_append (GTK_MENU (menu), menu_item);
  gtk_widget_show (menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      GTK_SIGNAL_FUNC (file_open_clicked), NULL);


  menu_item = create_menu_item (_ ("_Save"), accel);
  gtk_menu_append (GTK_MENU (menu), menu_item);
  gtk_widget_show (menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      GTK_SIGNAL_FUNC (file_save_clicked), NULL);


  menu_item = create_menu_item (_ ("Save _as"), accel);
  gtk_menu_append (GTK_MENU (menu), menu_item);
  gtk_widget_show (menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      GTK_SIGNAL_FUNC (file_save_as_clicked), NULL);


  menu_item = gtk_menu_item_new ();
  gtk_widget_show (menu_item);
  gtk_menu_append (GTK_MENU (menu), menu_item);

  menu_item = create_menu_item (_ ("_Preview"), accel);
  gtk_widget_show (menu_item);
  gtk_menu_append (GTK_MENU (menu), menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      GTK_SIGNAL_FUNC (preview_clicked), window);

  menu_item = create_menu_item (_ ("Save & _Run gaby"), accel);
  gtk_widget_show (menu_item);
  gtk_menu_append (GTK_MENU (menu), menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      GTK_SIGNAL_FUNC (run_gaby_clicked), window);




  menu_item = gtk_menu_item_new ();
  gtk_widget_show (menu_item);
  gtk_menu_append (GTK_MENU (menu), menu_item);


#ifndef NOTEBOOKVIEW

  menu_item = create_menu_item (_ ("_Close"), accel);
  gtk_widget_show (menu_item);
  gtk_menu_append (GTK_MENU (menu), menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      GTK_SIGNAL_FUNC (close_desc), window);
#endif

  menu_item = create_menu_item (_ ("E_xit"), accel);
  gtk_widget_show (menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      GTK_SIGNAL_FUNC (close_win), window);



  gtk_menu_append (GTK_MENU (menu), menu_item);

  file_item = gtk_menu_item_new ();
  lab = gtk_label_new ("");
  key = gtk_label_parse_uline (GTK_LABEL (lab), _ ("_File"));
  gtk_widget_add_accelerator (file_item, "activate_item", accel, 
			      key, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);
  gtk_misc_set_alignment (GTK_MISC (lab), 0.0, 1);
  gtk_container_add (GTK_CONTAINER (file_item), lab);
  gtk_label_set_justify (GTK_LABEL (lab), GTK_JUSTIFY_LEFT);
  gtk_widget_show (lab);
  gtk_widget_show (file_item);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (file_item), menu);

  gtk_menu_bar_append (GTK_MENU_BAR (menu_bar), file_item);




  /* about menu */
  menu = gtk_menu_new ();

  menu_item = create_menu_item (_ ("A_bout"), accel);
  gtk_menu_append (GTK_MENU (menu), menu_item);
  gtk_widget_show (menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      GTK_SIGNAL_FUNC (about_clicked), NULL);

  /* Web browser documentation
  menu_item = create_menu_item (_ ("_Readme"), accel);
  gtk_menu_append (GTK_MENU (menu), menu_item);
  gtk_widget_show (menu_item);
  gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
		      GTK_SIGNAL_FUNC (doc_clicked), NULL);
  */

  file_item = gtk_menu_item_new ();
  lab = gtk_label_new ("");
  key = gtk_label_parse_uline (GTK_LABEL (lab), _ ("_Help"));
  gtk_widget_add_accelerator (file_item, "activate_item", accel, 
			      key, GDK_MOD1_MASK, GTK_ACCEL_VISIBLE);
  gtk_misc_set_alignment (GTK_MISC (lab), 0.0, 1);
  gtk_container_add (GTK_CONTAINER (file_item), lab);
  gtk_label_set_justify (GTK_LABEL (lab), GTK_JUSTIFY_LEFT);
  gtk_widget_show (lab);
  gtk_widget_show (file_item);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (file_item), menu);
  gtk_menu_item_right_justify (GTK_MENU_ITEM (file_item));
  gtk_menu_bar_append (GTK_MENU_BAR (menu_bar), file_item);


  gtk_widget_show (menu_bar);
  return menu_bar;

}
#endif


void create_toolbar(GtkWidget *box, GtkWidget *dialog)
{
#ifndef USE_GNOME
#else
	gnome_app_create_toolbar(GNOME_APP(dialog),toolbar);
#endif
}


void
configure_dnd (GtkWidget * window)
{
  GtkTargetEntry table[] =
  {
    {"STRING", 0, 1},
    {"text/plain", 0, 1}
  };

  gtk_signal_connect (GTK_OBJECT (window), "drag_data_received",
		      GTK_SIGNAL_FUNC (drag_data_received), NULL);

  gtk_drag_dest_set (GTK_WIDGET (window),
		     GTK_DEST_DEFAULT_MOTION |
		     GTK_DEST_DEFAULT_HIGHLIGHT |
		     GTK_DEST_DEFAULT_DROP,
		     table, 2,
		     GDK_ACTION_COPY);

}





GtkWidget *
create_notebook()
{
  GtkWidget *note;
  GtkWidget *child;
  GtkWidget *label;
  gint i;
  s = new_desc();

  note = gtk_notebook_new();
  label = xpm_label_box(table_s_xpm,"Tables" ,1,&i);
  gtk_widget_show(label);
  child = create_table_window(NULL, &s);
  gtk_notebook_append_page(GTK_NOTEBOOK(note),child,label);

  label = xpm_label_box(subtable_s_xpm,"Subtables" ,1,&i);
  gtk_widget_show(label);
  child = create_subtable_window(NULL, &s); 
  gtk_widget_show(child);
  gtk_notebook_append_page(GTK_NOTEBOOK(note),child,label);

  label = xpm_label_box(flag_s_xpm,"Locations" ,1,&i);
  gtk_widget_show(label);
  child = create_tlocation_window(NULL, &s); 
  gtk_widget_show(child);
  gtk_notebook_append_page(GTK_NOTEBOOK(note),child,label);

  label = xpm_label_box(ja_s_xpm,"Misc" ,1,&i);
  gtk_widget_show(label);
  child = create_misc_window(NULL, &s); 
  gtk_widget_show(child);
  gtk_notebook_append_page(GTK_NOTEBOOK(note),child,label);


  label = xpm_label_box(action_s_xpm,"Actions" ,1,&i);
  gtk_widget_show(label);
  child = create_action_window(NULL, &s); 
  gtk_widget_show(child);
  gtk_notebook_append_page(GTK_NOTEBOOK(note),child,label);



  return note;


}







void
create_button_window (gchar * caption)
{
  GtkWidget *vbox2;
  GtkWidget *window;
  GtkWidget *note;

#ifndef NOTEBOOKVIEW
  GtkTooltips *tooltip;
  GtkWidget *lab;
  GtkWidget *addtable;
  int key;
  GtkWidget *table;
  int option;
#endif

#ifdef USE_GNOME
  window = gnome_app_new("Builder",NULL);
#else
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
#endif
  gtk_window_set_title (GTK_WINDOW (window), caption);

  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_event), window);
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (destroy), window);

  accel = gtk_accel_group_new ();

  s = NULL;
  but = g_malloc (sizeof (struct buttons));

  gtk_widget_realize(window);	/* so xpm_label_box won't issue warning */
  
  gtk_container_set_border_width (GTK_CONTAINER (window), 0);
  Window = window;
  /* The box */
  vbox2 = gtk_vbox_new (FALSE, 5);

  gtk_widget_show (vbox2);
#ifdef USE_GNOME
  gnome_app_set_contents(GNOME_APP(window),vbox2);
#else
  gtk_container_add (GTK_CONTAINER (window), vbox2);
#endif

#ifdef USE_GNOME
  gnome_app_create_menus(GNOME_APP(window),main_menu);
#else
  gtk_box_pack_start (GTK_BOX (vbox2), create_menu (window, accel), 
			FALSE, FALSE, 2);
#endif

  create_toolbar(vbox2, window);


#ifndef NOTEBOOKVIEW 
  tooltip = gtk_tooltips_new ();

  table = gtk_table_new (3, 2, TRUE);
  gtk_box_pack_start (GTK_BOX (vbox2), table, TRUE, TRUE, 1);
  option = GTK_FILL | GTK_EXPAND;

  /* Table */
  addtable = gtk_button_new ();
  lab = xpm_label_box (table_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addtable), lab);
  gtk_table_attach (GTK_TABLE (table), addtable, 0, 1, 0, 1, option, option, 0, 0);
  gtk_widget_show (addtable);
  gtk_signal_connect (GTK_OBJECT (addtable), "clicked",
		      GTK_SIGNAL_FUNC (edit_table_clicked), NULL);
  gtk_tooltips_set_tip (tooltip, addtable, _("Edit the tables."), NULL);
  but->table = addtable;


  /* Subtable */
  addtable = gtk_button_new ();
  lab = xpm_label_box (subtable_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addtable), lab);
  gtk_table_attach (GTK_TABLE (table), addtable, 1, 2, 0, 1, option, option, 0, 0);
  gtk_widget_show (addtable);
  gtk_signal_connect (GTK_OBJECT (addtable), "clicked",
		      GTK_SIGNAL_FUNC (subtable_clicked), window);
  gtk_tooltips_set_tip (tooltip, addtable,
			_("Edit subtables."), NULL);
  but->subtable = addtable;

  /* ACtions */
  addtable = gtk_button_new ();
  lab = xpm_label_box (action_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addtable), lab);
  gtk_table_attach (GTK_TABLE (table), addtable, 0, 1, 1, 2, option, option, 0, 0);
  gtk_widget_show (addtable);
  gtk_signal_connect (GTK_OBJECT (addtable), "clicked",
		      GTK_SIGNAL_FUNC (action_clicked), window);
  gtk_tooltips_set_tip (tooltip, addtable,
			_("Edit actions."), NULL);
  but->action = addtable;

  /* locations */
  addtable = gtk_button_new ();
  lab = xpm_label_box (flag_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addtable), lab);
  gtk_table_attach (GTK_TABLE (table), addtable, 1, 2, 1, 2, option, option, 0, 0);
  gtk_widget_show (addtable);
  gtk_signal_connect (GTK_OBJECT (addtable), "clicked",
		      GTK_SIGNAL_FUNC (location_clicked), window);
  gtk_tooltips_set_tip (tooltip, addtable,
			_("Edit locations."), NULL);
  but->location = addtable;


  /* preview */
  addtable = gtk_button_new ();
  lab = xpm_label_box (eye_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addtable), lab);
  gtk_table_attach (GTK_TABLE (table), addtable, 0, 1, 2, 3, option, option, 0, 0);
  gtk_widget_show (addtable);
  gtk_signal_connect (GTK_OBJECT (addtable), "clicked",
		      GTK_SIGNAL_FUNC (gaby_clicked), NULL);
  gtk_tooltips_set_tip (tooltip, addtable,
			_("Test the description file in gaby.\n"), NULL);
  but->preview = addtable;

#ifdef SHAME_NO_CLOSE_BUTTON	/* Close */
  addtable = gtk_button_new ();
  lab = xpm_label_box (ja_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addtable), lab);
  gtk_table_attach (GTK_TABLE (table), addtable, 1, 2, 2, 3, option, option, 0, 0);
  gtk_widget_show (addtable);
  gtk_signal_connect (GTK_OBJECT (addtable), "clicked",
		      GTK_SIGNAL_FUNC (close_win), window);
  gtk_tooltips_set_tip (tooltip, addtable,
			_("Exit Gaby Databases Builder."), NULL);
#endif

  /* Misc */
  addtable = gtk_button_new ();
  lab = xpm_label_box (ja_xpm, NULL, 3, &key);
  gtk_widget_show (lab);
  gtk_container_add (GTK_CONTAINER (addtable), lab);
  gtk_table_attach (GTK_TABLE (table), addtable, 1, 2, 2, 3,
		    option, option, 0, 0);
  gtk_widget_show (addtable);
  gtk_signal_connect (GTK_OBJECT (addtable), "clicked",
		      GTK_SIGNAL_FUNC (misc_clicked), NULL);
  gtk_tooltips_set_tip (tooltip, addtable,
			_("Edit misc options."), NULL);
  but->misc = addtable;
  gtk_tooltips_enable (tooltip);
  gtk_widget_show (table);

#else
  note = create_notebook();
  gtk_box_pack_start (GTK_BOX (vbox2), note, TRUE,TRUE, 2);
  gtk_widget_show(note);
#endif

  disable_buttons ();

  configure_dnd (window);
  /*gtk_accel_group_attach (accel, GTK_OBJECT (window));*/
  
}

