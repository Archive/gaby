/*
   Sessionmanangement.

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.



   Yuck, session management is disabled. This file is one BIG mess
   anybody like to make the builder sessionmanaged just wipe 
   this entire file.

   Ron.


*/






#include "main.h"

extern file_s *s;	/* from buttonwin.c */

/* ******************************************************************** */
/* Support Functuins                                                    */
/* ******************************************************************** */

#ifdef USE_GNOME
struct winsize 
{
	gint x;
	gint y;
	gint w;
	gint h;
};

void 
save_winsize(gchar *section, struct winsize *ws)
{
  gchar buffer[1000];
  
  sprintf(buffer,"%s/x",section);  
  gnome_config_set_int(buffer,ws->x);
  sprintf(buffer,"%s/y",section);  
  gnome_config_set_int(buffer,ws->y);
  sprintf(buffer,"%s/w",section);  
  gnome_config_set_int(buffer,ws->w);
  sprintf(buffer,"%s/h",section);  
  gnome_config_set_int(buffer,ws->h);
}

struct winsize *
load_winsize(gchar *section, struct winsize *ws)
{
  gchar buffer[1000];
  sprintf(buffer,"%s/x",section);  
  ws->x = gnome_config_get_int(buffer);
  sprintf(buffer,"%s/y",section);  
  ws->y = gnome_config_get_int(buffer);
  sprintf(buffer,"%s/w",section);  
  ws->w = gnome_config_get_int(buffer);
  sprintf(buffer,"%s/h",section);  
  ws->h = gnome_config_get_int(buffer);
   
  return ws;
}


void
set_winsize(GtkWidget *window, struct winsize *ws)
{
  gtk_widget_set_uposition(window,ws->x,ws->y);
  gtk_window_set_default_size(GTK_WINDOW(window),ws->w,ws->h);
  GTK_CONTAINER(window)->need_resize=TRUE;
  gtk_container_check_resize(GTK_CONTAINER(window));
  gtk_widget_show(window);
}

struct winsize *
get_winsize(GtkWidget *window, struct winsize *ws)
{

  gdk_window_get_geometry(window->window, &ws->x, &ws->y, &ws->w, &ws->h, NULL);

  gdk_window_get_position(window->window, &ws->x , &ws->y);

  return ws; 
}

/* ******************************************************************** */
/* Save state                                                           */
/* ******************************************************************** */


void
save_windows( GnomeClient *client)
{
  GList *loop1;
  GList *loop2;
  field *fld;
  table *tab;
  subtable *stab;
  subfield *sfld;
  gchar buffer[1000];
  struct winsize ws;
  
  if (s->name != NULL)
    gnome_config_set_string("Settings/filename",s->name);

  get_winsize(Window,&ws);
  save_winsize("mainwin",&ws);

  if (s == NULL)
	return;

  /* First the Main table chain */
  if (s->table!=NULL)
  { 
    get_winsize(s->table, &ws);
    save_winsize("tablewin",&ws);
   }


  loop1 = g_list_first(s->tables);
  while (loop1!= NULL)
  {
    tab = loop1->data;
    if (tab->window!= NULL)
    {
      get_winsize(tab->window,&ws);
      sprintf(buffer,"TABLE~%s",tab->name);
      save_winsize(buffer,&ws);    
    }
    loop1 = loop1->next;         
  }	

  loop1 = g_list_first(s->tables);
  while (loop1!=NULL)
  {
    tab=loop1->data;
    loop2 = tab->fields;
    while (loop2!=NULL)
    {
      fld = loop2->data;
      if (fld->window!=NULL)
      {
        get_winsize(fld->window, &ws);
        sprintf(buffer,"FIELD~%s",fld->name);
        save_winsize(buffer,&ws);
        strcat(buffer,"/name_of_table");
        gnome_config_set_string(buffer,tab->name);
      }
      loop2 = loop2->next;
    }
    loop1 = loop1->next;
  }


  /* Then the subtable chain */
  if (s->subtablewin!=NULL)
  { 
    get_winsize(s->subtablewin, &ws);
    save_winsize("subtablewin",&ws);
   }



  loop1 = g_list_first(s->subtables);
  while (loop1!= NULL)
  {
    stab = loop1->data;
    if (stab->window!= NULL)
    {
      get_winsize(stab->window,&ws);
      sprintf(buffer,"SUBTABLE~%s",stab->name);
      save_winsize(buffer,&ws);    
    }
    loop1 = loop1->next;         
  }	


  loop1 = g_list_first(s->subtables);
  while (loop1!=NULL)
  {
    stab=loop1->data;
    loop2 = stab->subfields;
    while (loop2!=NULL)
    {
      sfld = loop2->data;
      if (sfld->window!=NULL)
      {
        get_winsize(sfld->window, &ws);
        sprintf(buffer,"SUBFIELD~%s",sfld->name_of_field);
        save_winsize(buffer,&ws);
        strcat(buffer,"/name_of_subtable");
        gnome_config_set_string(buffer,stab->name);
      }
      loop2 = loop2->next;
    }
    loop1 = loop1->next;
  }


  /* The locations chain */
  if (s->locationwin!=NULL)
  { 
    get_winsize(s->locationwin, &ws);
    save_winsize("locationwin",&ws);
   }

  /* The misc chain */
  if (s->miscwin!=NULL)
  { 
    get_winsize(s->miscwin, &ws);
    save_winsize("miscwin",&ws);
   }





}

/* ******************************************************************** */
/* Load state                                                           */
/* ******************************************************************** */


void
popup_table_win(gchar *name)
{
  struct winsize ws;
  table *tab;
  GList *loop;

  load_winsize(name,&ws);
  loop = g_list_first(s->tables);
  while (loop != NULL)
  {
    tab = loop->data;
    if (strcmp(tab->name, name+6)==0)
    {
	create_field_window(tab,s,"asd");
	set_winsize(tab->window,&ws);
    }
    loop = loop->next;
  }
}


void
popup_field_win(gchar *name)
{
  struct winsize ws;
  gchar *name_of_table;
  table *tab;
  field *fld;
  GList *loop;
  GList *loop2;
  gchar buffer[1000];

  strcpy(buffer,name);
  strcat(buffer,"/name_of_table");
  load_winsize(name,&ws);
  name_of_table = gnome_config_get_string(buffer);
  if (name_of_table==NULL)
  {
	debug_print("!!!!%s\n",buffer);
	debug_print("HOOPWS\n");
	return;
  }
 
  loop = g_list_first(s->tables);
  while (loop != NULL)
  {
    tab = loop->data;
    if (strcmp(tab->name, name_of_table)==0)
    {
	loop2 = tab->fields;
        while (loop2 != NULL)
        {
           fld = loop2->data;
           if (strcmp(fld->name,name+6)==0)
           {
	      create_fieldprop_window(fld,s,"asd");
	      set_winsize(fld->window,&ws);
           }
           loop2=loop2->next;
        }
    }
    loop = loop->next;
  }
}


void
popup_subtable_win(gchar *name)
{
  struct winsize ws;
  subtable *stab;
  GList *loop;

  load_winsize(name,&ws);
  loop = g_list_first(s->subtables);
  while (loop != NULL)
  {
    stab = loop->data;
    if (strcmp(stab->name, name+9)==0)
    {
	create_subfield_window(stab,s,"asd");
	set_winsize(stab->window,&ws);
    }
    loop = loop->next;
  }
}



void
popup_subfield_win(gchar *name)
{
  struct winsize ws;
  gchar *name_of_subtable;
  subtable *stab;
  subfield *sfld;
  GList *loop;
  GList *loop2;
  gchar buffer[1000];

  strcpy(buffer,name);
  strcat(buffer,"/name_of_subtable");
  load_winsize(name,&ws);
  name_of_subtable = gnome_config_get_string(buffer);
  if (name_of_subtable==NULL)
  {
	debug_print("!!!!%s\n",buffer);
	debug_print("HOOPWS\n");
	return;
  }
 
  loop = g_list_first(s->subtables);
  while (loop != NULL)
  {
    stab = loop->data;
    if (strcmp(stab->name, name_of_subtable)==0)
    {
	loop2 = stab->subfields;
        while (loop2 != NULL)
        {
           sfld = loop2->data;
           if (strcmp(sfld->name_of_field,name+9)==0)
           {
	      create_subfieldprop_window(sfld,s,"asd");
	      set_winsize(sfld->window,&ws);
           }
           loop2=loop2->next;
        }
    }
    loop = loop->next;
  }
}







void restore_windows(GnomeClient *client)
{
  struct winsize ws;
  gchar *name;
  gchar *secname;
  void *sec;
  load_winsize("mainwin",&ws);
  set_winsize(Window, &ws);

  name = gnome_config_get_string("Settings/filename");
  load_x(name,NULL);

  if (gnome_config_has_section("tablewin")==TRUE)
  {
    create_table_window("asfd",&s);
    load_winsize("tablewin",&ws);
    set_winsize(s->table,&ws);
  }

  if (gnome_config_has_section("subtablewin")==TRUE)
  {
    create_subtable_window("asfd",&s);
    load_winsize("subtablewin",&ws);
    set_winsize(s->subtablewin,&ws);
  }

  if (gnome_config_has_section("locationwin")==TRUE)
  {
    create_tlocation_window("asfd",&s);
    load_winsize("locationwin",&ws);
    set_winsize(s->locationwin,&ws);
  }

  if (gnome_config_has_section("miscwin")==TRUE)
  {
    create_misc_window("asfd",&s);
    load_winsize("miscwin",&ws);
    set_winsize(s->miscwin,&ws);
  }




  sec = gnome_config_init_iterator_sections(
		gnome_client_get_config_prefix(client));
  while ((sec=gnome_config_iterator_next(sec,&secname,NULL))!=NULL)
  {
     if (strncmp(secname,"TABLE~",6)==0)
     {
	popup_table_win(secname);
     }
     if (strncmp(secname,"FIELD~",6)==0)
     {
	popup_field_win(secname);
     }


     if (strncmp(secname,"SUBTABLE~",9)==0)
     {
	popup_subtable_win(secname);
     }

     if (strncmp(secname,"SUBFIELD~",6)==0)
     {
	popup_subfield_win(secname);
     }

     g_free(secname);
  }

}


/* ******************************************************************** */
/* SessionManagement Events                                             */
/* ******************************************************************** */



static gint
die (GnomeClient *client, gpointer data)
{
  close_prog_fileop ((gpointer) Window);
  return TRUE;
}

#ifdef SESSIONMANANGED
static void
interact_during_shutdown(GnomeClient *client, gint key,
			GnomeDialogType dialog_type,
			gpointer data)
{


  debug_print("Key : %d\n",key);
  if (s->changed==FALSE)
    {
      gnome_interaction_key_return (key, FALSE);
    }
  else
    {
      sys_fileop(key);
    } 
}
#endif


static gint
save_my_self(GnomeClient *client,
                gint                    phase,
                GnomeRestartStyle       restarts,
                gint                    shutdown,
                GnomeInteractStyle      interacts,
                gint                    fast,
                gpointer                data)
{
#ifdef SESSIONMANAGED

  gchar *argv[3];
  gchar *prefix;
  gnome_config_push_prefix(gnome_client_get_config_prefix(client));

  /* Disabled unitl i figure out the right (TM) wat to do this

  gnome_client_request_interaction(client, GNOME_DIALOG_NORMAL,
			interact_during_shutdown, NULL);
  */
  /* NOW THIS IS OVERKILL */
  // save_windows(client);
  
  
  
  gnome_config_sync();
  gnome_config_pop_prefix();

  argv[0]="builder";
  gnome_client_set_restart_command(client,1,argv);

  prefix = gnome_client_get_config_prefix(client);
  argv[0] = "rm";
  argv[1] = "-r";
  argv[2] = gnome_config_get_real_path(prefix);
  gnome_client_set_discard_command(client,3,argv);
#endif
  return FALSE;
}

/* ******************************************************************** */
/* Connecting to SessionManagement                                      */
/* ******************************************************************** */

void
connect_to_session()
{
  GnomeClient *client;
  gint restarted;
  gint x,y,w,h;
  x=0;
  y=0;
  w=0;
  h=0;
  restarted=0;

  client = gnome_master_client();
  gtk_signal_connect(GTK_OBJECT(client),"save_yourself",
                GTK_SIGNAL_FUNC(save_my_self),NULL);
  gtk_signal_connect(GTK_OBJECT(client),"die",
                GTK_SIGNAL_FUNC(die), NULL);


  if ( GNOME_CLIENT_CONNECTED(client))
    {
         if ( (gnome_client_get_flags(client) & GNOME_CLIENT_RESTORED) != 0 )
         {
            restarted= 1;
            gnome_config_push_prefix(gnome_client_get_config_prefix(client));
		
            restore_windows(client);

            gnome_config_pop_prefix();
         }
     }
}


#else
void connect_to_session()
{
  /* Without gnome you're kidding right ? */
}

#endif

