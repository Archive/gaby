/*
   Field properties window

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl
   Copyright (C) 2000 Frederic Peters

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */

#include "main.h"
#include "icons/ja.xpm"

static GtkAccelGroup *accel;

extern doublestring type_names[];

typedef struct _main_update info1;

struct _main_update
  {
    GtkWidget *namelist;
    GtkWidget *desclist;
    GtkWidget *type;
    GtkWidget *format;
    GtkWidget *defaultt;
    GtkWidget *mmtype;
    GtkWidget *window;
    file_s *s;
    field *fld;
  };



/* ******************************************************************** */
/* Events                                                               */
/* ******************************************************************** */


static void
add_name_ok (doublestring *dse, gpointer data)
{
  GtkWidget *wid;
  info1 *info;
  file_s *s;
  doublestring *ds;
  int row;
  gchar *names[2];
  field *fld;

  info = data;
  wid =info->namelist;
  fld = info->fld; 
  s = info->s;

  trim (dse->name);
  trim (dse->locale);
  if (check_name (dse->name, 0) != 0)
    return;

  if (strlen (dse->name) == 0)
    return;

  if (fld == NULL)
    {
      g_warning ("found unexpected null pointer.\n");
      return;
    }

  ds = new_i18n ();
  ds->name = dse->name;
  ds->locale = dse->locale;
  names[0] = dse->name;
  names[1] = dse->locale;
  row = gtk_clist_append (GTK_CLIST (wid), names);
  gtk_clist_set_row_data (GTK_CLIST (wid), row, ds);

  fld->i18n_name = g_list_append (fld->i18n_name, ds);
  g_free (dse);

  s->changed = TRUE;

}


static void
add_name_clicked (GtkWidget * widget, info1 *info)
{
  popup_locale_dialog (add_name_ok, _ ("Question"),
		_ ("Enter Name field in other language"), NULL, NULL, 
				(gpointer)info);
}


static void
edit_name_ok (doublestring *dse, gpointer data)
{
  GtkWidget *wid;
  info1 *info;
  GList *loop;
  GList *selection;
  file_s *s;
  field *fld;
  doublestring *ds;
  GtkVisibility vis;
  doublestring *names;
  gchar *strd[2];

  int row;
  int row2;

  info = data;
  wid = info->namelist;
  fld = info->fld;
  s = info->s; 

  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  ds = gtk_clist_get_row_data (GTK_CLIST (wid), row);

  if (ds->name != NULL)
    {
      g_free (ds->name);
      ds->name = NULL;
    }
  ds->name = dse->name;
  if (ds->locale != NULL)
    {
      g_free (ds->locale);
      ds->locale = NULL;
    }
  ds->locale = dse->locale;
  g_free (dse);

  gtk_clist_freeze (GTK_CLIST (wid));
  gtk_clist_clear (GTK_CLIST (wid));
  loop = fld->i18n_name;
  g_list_first (loop);
  while (loop != NULL)
    {
      names = loop->data;
      strd[0] = names->name;
      strd[1] = names->locale;
      row2 = gtk_clist_append (GTK_CLIST (wid), strd);
      gtk_clist_set_row_data (GTK_CLIST (wid), row2, names);
      loop = loop->next;
    }

  vis = gtk_clist_row_is_visible (GTK_CLIST (wid), row + 1);
  if (vis != GTK_VISIBILITY_FULL)
    gtk_clist_moveto (GTK_CLIST (wid), row, 0, 0, 0);

  gtk_clist_select_row (GTK_CLIST (wid), row, 0);
  gtk_clist_thaw (GTK_CLIST (wid));


  s->changed = TRUE;


}

static void
edit_name_clicked (GtkWidget * widget,  info1 *info)
{
  GtkWidget *list;
  GList *selection;
  field *fld;
  doublestring *ds;

  int row;
  
  list = info->namelist;
  fld = info->fld;
  
  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  ds = gtk_clist_get_row_data (GTK_CLIST (list), row);

  popup_locale_dialog (edit_name_ok, _ ("Question"),
		       _ ("Enter description"), ds->name, ds->locale, 
		       (gpointer) info);

}



static void
edit_desc_ok (doublestring *dse, gpointer data)
{
  GtkWidget *wid;
  info1 *info;
  GList *loop;
  GList *selection;
  file_s *s;
  field *fld;
  doublestring *ds;
  doublestring *names;
  gchar *strd[2];
  GtkVisibility vis;
  int row;
  int row2;

  info = data; 
  wid = info->desclist;
  fld = info->fld;
  s = info->s;


  selection = GTK_CLIST (wid)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  ds = gtk_clist_get_row_data (GTK_CLIST (wid), row);

  if (ds->name != NULL)
    {
      g_free (ds->name);
      ds->name = NULL;
    }
  ds->name = dse->name;
  if (ds->locale != NULL)
    {
      g_free (ds->locale);
      ds->locale = NULL;
    }
  ds->locale = dse->locale;
  g_free (dse);

  gtk_clist_freeze (GTK_CLIST (wid));

  gtk_clist_clear (GTK_CLIST (wid));
  loop = fld->i18n_desc;
  g_list_first (loop);
  while (loop != NULL)
    {
      names = loop->data;
      strd[0] = names->name;
      strd[1] = names->locale;
      row2 = gtk_clist_append (GTK_CLIST (wid), strd);
      gtk_clist_set_row_data (GTK_CLIST (wid), row2, names);
      loop = loop->next;
    }

  vis = gtk_clist_row_is_visible (GTK_CLIST (wid), row + 1);
  if (vis != GTK_VISIBILITY_FULL)
    gtk_clist_moveto (GTK_CLIST (wid), row, 0, 0, 0);

  gtk_clist_select_row (GTK_CLIST (wid), row, 0);
  gtk_clist_thaw (GTK_CLIST (wid));

  s->changed = TRUE;


}




static void
edit_desc_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  GList *selection;
  field *fld;
  doublestring *ds;

  int row;
  list = info->desclist;
  fld = info->fld; 
 
  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  ds = gtk_clist_get_row_data (GTK_CLIST (list), row);

  popup_locale_dialog (edit_desc_ok, _ ("Question"),
		       _ ("Enter description"), ds->name, ds->locale,
			(gpointer ) info);
}




static void
delete_name_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  file_s *s;
  GList *selection;
  field *fld;
  doublestring *ds;

  int row;
  list = info->namelist;
  fld = info->fld;
  s = info->s;

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  ds = gtk_clist_get_row_data (GTK_CLIST (list), row);

  fld->i18n_name = g_list_remove (fld->i18n_name, ds);
  gtk_clist_remove (GTK_CLIST (list), row);

  delete_i18n (ds);

  s->changed = TRUE;


}

static void
add_desc_ok (doublestring *dse, gpointer data)
{
  info1 *info;
  GtkWidget *wid;
  file_s *s;
  doublestring *ds;
  int row;
  gchar *names[2];
  field *fld;

  info =data;
  fld = info->fld;
  s = info->s;
  wid = info->desclist;

  trim (dse->name);
  trim (dse->locale);
  if (check_name (dse->name, 0) != 0)
    return;

  if (strlen (dse->name) == 0)
    return;


  if (fld == NULL)
    {
      g_warning ("found unexpected null pointer.\n");
      return;
    }

  ds = new_i18n ();
  (ds)->name = dse->name;
  (ds)->locale = dse->locale;
  names[0] = dse->name;
  names[1] = dse->locale;
  row = gtk_clist_append (GTK_CLIST (wid), names);
  gtk_clist_set_row_data (GTK_CLIST (wid), row, ds);

  fld->i18n_desc = g_list_append (fld->i18n_desc, ds);
  g_free (dse);


  s->changed = TRUE;


}

static void
add_desc_clicked (GtkWidget * widget, info1 *info)
{
  popup_locale_dialog (add_desc_ok, _ ("Question"),
		       _ ("Enter description"), NULL, NULL,
			(gpointer )info);
}

static void
delete_desc_clicked (GtkWidget * widget, info1 *info)
{
  GtkWidget *list;
  file_s *s;
  GList *selection;
  field *fld;
  doublestring *ds;

  int row;
  list = info->desclist;
  fld = info->fld;
  s = info->s; 

  selection = GTK_CLIST (list)->selection;
  if (selection == NULL)
    return;
  row = GPOINTER_TO_INT(selection->data);
  ds = gtk_clist_get_row_data (GTK_CLIST (list), row);

  fld->i18n_desc = g_list_remove (fld->i18n_desc, ds);
  gtk_clist_remove (GTK_CLIST (list), row);

  delete_i18n (ds);
  /* g_free (ds->name);
     g_free (ds->locale);
     g_free (ds); */

  s->changed = TRUE;



}




static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  return FALSE;
}

static void
destroy (GtkWidget * widget, info1 *info)
{
  info->fld->window = NULL;
  g_free(info);
}


void
combo_set (GtkWidget * combo, field *fld)
{
  gchar *buffer;
  buffer = type_names[fld->type].locale;
#if 0
  switch (fld->type)
    {
    case T_STRING:
      buffer = "String";
      break;
    case T_STRINGS:
      buffer = "Strings";
      break;
    case T_INTEGER:
      buffer = "Integer";
      break;
    case T_REAL:
      buffer = "Real";
      break;
    case T_DATE:
      buffer = "Date";
      break;
    case T_BOOLEAN:
      buffer = "Boolean";
      break;
    case T_RECORD:
      buffer = "Record";
      break;
    case T_RECORDS:
      buffer = "Records";
      break;
    case T_MULTIMEDIA:
      buffer = "Multimedia";
      break;
    case T_DECIMAL:
      buffer = "Decimal";
      break;
    }
#endif
  gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (combo)->entry), buffer);
}

void
type_act (GtkWidget * wid, info1 *info)
{
	file_s *s;
	gchar *text;
	enum field_type type = T_STRING;
	field *fld;
	GtkWidget *type2;
	int i;

	fld = info->fld;
	s = info->s;
	type2 = info->mmtype;

	s->changed = TRUE;

	text = gtk_entry_get_text (GTK_ENTRY (wid));
	i = 0;
	while ( type_names[i].locale ) {
		if ( strcasecmp(text, type_names[i].locale) == 0 ) {
			type = i;
			break;
		}
		i++;
	}
#if 0
	type = T_STRING;
	if (strcasecmp (text, "Strings") == 0)
		type = T_STRINGS;
	if (strcasecmp (text, "String") == 0)
		type = T_STRING;
	if (strcasecmp (text, "Integer") == 0)
		type = T_INTEGER;
	if (strcasecmp (text, "Real") == 0)
		type = T_REAL;
	if (strcasecmp (text, "Date") == 0)
		type = T_DATE;
	if (strcasecmp (text, "Boolean") == 0)
		type = T_BOOLEAN;
	if (strcasecmp (text, "Record") == 0)
		type = T_RECORD;
	if (strcasecmp (text, "Records") == 0)
		type = T_RECORDS;
	if (strcasecmp (text, "Multimedia") == 0)
		type = T_MULTIMEDIA;
	if (strcasecmp (text, "Decimal") == 0)
		type = T_DECIMAL;
#endif
	fld->type = type;

	if (fld->type == T_MULTIMEDIA) {
		gtk_widget_set_sensitive(type2,TRUE);
		if (get_option(&fld->properties,"TYPE") == NULL) {
			gtk_option_menu_set_history(GTK_OPTION_MENU(type2),0);
		}
	} else {
		gtk_widget_set_sensitive(type2,FALSE);
	}

	return;
}


void
format_changed (GtkWidget * wid, info1 *info)
{
  field *fld;
  file_s *s;
  gchar *t;

  fld = info->fld;
  s = info->s;

  t = gtk_entry_get_text (GTK_ENTRY (wid));

  set_option(&fld->properties,"FORMAT",t);

  s->changed = TRUE;

}

void
default_changed (GtkWidget * wid, info1 *info)
{
  field *fld;
  file_s *s;
  gchar *t;

  fld = info->fld;
  s = info->s;

  t = gtk_entry_get_text (GTK_ENTRY (wid));

  set_option(&fld->properties,"DEFAULT",t);

  s->changed = TRUE;


}



void
type_changed (GtkWidget * wid, info1 *info)
{
  file_s *s;
  field *fld;
  gchar *text;

  text = gtk_object_get_user_data(GTK_OBJECT(wid));
  fld = info->fld;
  s = info->s;

  if (fld->type == T_MULTIMEDIA)
  {
    set_option(&fld->properties,"TYPE",text);
    s->changed = TRUE;
  }
}





static void
close_win (GtkWidget * wid, info1 *info)
{
  gtk_widget_destroy (info->window);
}




static void
name_dclicked (GtkWidget * widget, GdkEventButton * event,
	       info1 *info)
{

  if (event->type == GDK_2BUTTON_PRESS)
    {
      edit_name_clicked (widget, info);
    }
}


static void
desc_dclicked (GtkWidget * widget, GdkEventButton * event,
	       info1 *info)
{

  if (event->type == GDK_2BUTTON_PRESS)
    {
      edit_desc_clicked (widget, info);
    }
}



/* ******************************************************************** */
/* Create win                                                           */
/* ******************************************************************** */



void
create_fieldprop_window (field *fld, file_s *s, gchar * caption)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *window;
  GtkWidget *combo;
  GtkWidget *addname;
  GtkWidget *delname;
  GtkWidget *hbox2;
  GtkWidget *i18nname;
  GtkWidget *namelist;
  GtkWidget *desclist;
  GtkWidget *frame;
  GtkWidget *format;
  GtkWidget *defaultt;
  GtkWidget *label;
  GtkWidget *closebut;
  GtkWidget *scroll;
  GtkWidget *om, *om_menu, *om_item;
  info1 *info;
  int key;
  int tel;
  gchar *titles[2];
  gchar buffer[1000];
  GList *loop;
  gint row;
  doublestring *names;
  gchar *strd[2];
  int i;


  if (fld->window != NULL)
    {
      gdk_window_raise (fld->window->window);
      return;
    }

  info = g_malloc (sizeof(info1));

  strcpy (buffer, _ ("Field: "));
  strcat (buffer, caption);
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_policy(GTK_WINDOW(window),FALSE,TRUE,TRUE);
  gtk_window_set_title (GTK_WINDOW (window), buffer);

  accel = gtk_accel_group_new ();

  /* WM */
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_event), info);
  /* Destroy */
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (destroy), info);

  gtk_widget_show (window);

  fld->window = window;
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  hbox = gtk_hbox_new (FALSE, 0);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("I18N name"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);

  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  titles[0] = _ ("Name");
  titles[1] = _ ("Locale");
  i18nname = gtk_clist_new_with_titles (2, titles);
  namelist = i18nname;
  gtk_widget_set_usize (i18nname, 200, 100);
  gtk_widget_show (i18nname);
  gtk_signal_connect (GTK_OBJECT (i18nname), "button_press_event",
		      GTK_SIGNAL_FUNC (name_dclicked), info);
  gtk_clist_set_column_width (GTK_CLIST (i18nname), 0, 150);
  gtk_clist_set_selection_mode (GTK_CLIST (i18nname), GTK_SELECTION_BROWSE);
  gtk_widget_show (vbox);
  scroll = scroll_new (i18nname);
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);
  loop = fld->i18n_name;
  g_list_first (loop);
  while (loop != NULL)
    {
      names = loop->data;
      strd[0] = names->name;
      strd[1] = names->locale;
      row = gtk_clist_append (GTK_CLIST (i18nname), strd);
      gtk_clist_set_row_data (GTK_CLIST (i18nname), row, names);
      loop = loop->next;
    }





  hbox2 = gtk_hbox_new (TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox2, FALSE, TRUE, 0);
  gtk_widget_show (hbox2);

  /* The buttons */
  addname = gtk_button_new_with_label (_ ("Add"));
  gtk_box_pack_start (GTK_BOX (hbox2), addname, FALSE, TRUE, 0);
  gtk_widget_show (addname);
  gtk_signal_connect (GTK_OBJECT (addname), "clicked",
		      GTK_SIGNAL_FUNC (add_name_clicked), info);

  addname = gtk_button_new_with_label (_ ("Edit"));
  gtk_box_pack_start (GTK_BOX (hbox2), addname, FALSE, TRUE, 0);
  gtk_widget_show (addname);
  gtk_signal_connect (GTK_OBJECT (addname), "clicked",
		      GTK_SIGNAL_FUNC (edit_name_clicked), info);

  delname = gtk_button_new_with_label (_ ("Delete"));
  gtk_box_pack_start (GTK_BOX (hbox2), delname, FALSE, TRUE, 0);
  gtk_widget_show (delname);
  gtk_signal_connect (GTK_OBJECT (delname), "clicked",
		      GTK_SIGNAL_FUNC (delete_name_clicked), info);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("I18N Description"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);

  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  titles[0] = _ ("Description");
  titles[1] = _ ("Locale");
  i18nname = gtk_clist_new_with_titles (2, titles);
  desclist = i18nname;
  gtk_widget_set_usize (i18nname, 200, 100);
  gtk_widget_show (i18nname);
  gtk_signal_connect (GTK_OBJECT (i18nname), "button_press_event",
		      GTK_SIGNAL_FUNC (desc_dclicked), info);
  gtk_clist_set_column_width (GTK_CLIST (i18nname), 0, 150);
  gtk_widget_show (vbox);
  gtk_clist_set_selection_mode (GTK_CLIST (i18nname), GTK_SELECTION_BROWSE);
  scroll = scroll_new (i18nname);
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);
  loop = fld->i18n_desc;
  g_list_first (loop);
  while (loop != NULL)
    {
      names = loop->data;
      strd[0] = names->name;
      strd[1] = names->locale;
      row = gtk_clist_append (GTK_CLIST (i18nname), strd);
      gtk_clist_set_row_data (GTK_CLIST (i18nname), row, names);
      loop = loop->next;
    }



  hbox2 = gtk_hbox_new (TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox2, FALSE, TRUE, 0);
  gtk_widget_show (hbox2);

  /* The buttons */
  addname = gtk_button_new_with_label (_ ("Add"));
  gtk_box_pack_start (GTK_BOX (hbox2), addname, FALSE, TRUE, 0);
  gtk_widget_show (addname);

  gtk_signal_connect (GTK_OBJECT (addname), "clicked",
		      GTK_SIGNAL_FUNC (add_desc_clicked), info);

  addname = gtk_button_new_with_label (_ ("Edit"));
  gtk_box_pack_start (GTK_BOX (hbox2), addname, FALSE, TRUE, 0);
  gtk_widget_show (addname);
  gtk_signal_connect (GTK_OBJECT (addname), "clicked",
		      GTK_SIGNAL_FUNC (edit_desc_clicked), info);


  delname = gtk_button_new_with_label (_ ("Delete"));
  gtk_box_pack_start (GTK_BOX (hbox2), delname, FALSE, TRUE, 0);
  gtk_widget_show (delname);

  gtk_signal_connect (GTK_OBJECT (delname), "clicked",
		      GTK_SIGNAL_FUNC (delete_desc_clicked), info);



  frame = gtk_frame_new (NULL);
  gtk_frame_set_label (GTK_FRAME (frame), _ ("Misc"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, TRUE, 5);

  vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
  gtk_container_add (GTK_CONTAINER (frame), vbox);

  label = gtk_label_new (_ ("Type"));
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);
  gtk_widget_show (label);

  loop = NULL;
  i=0;
  while ( type_names[i].name ) {
	  loop = g_list_append(loop, type_names[i].locale);
	  i++;
  }
  
  combo = gtk_combo_new ();
  gtk_combo_set_popdown_strings (GTK_COMBO (combo), loop);
  gtk_widget_show (combo);
  gtk_box_pack_start (GTK_BOX (vbox), combo, FALSE, TRUE, 0);
  gtk_widget_show (vbox);
  gtk_entry_set_editable (GTK_ENTRY (GTK_COMBO (combo)->entry), FALSE);
  combo_set (combo, fld);

  gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo)->entry), "changed",
		      GTK_SIGNAL_FUNC (type_act), info);

  label = gtk_label_new (_ ("Format"));
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);
  gtk_widget_show (label);

  format = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (vbox), format, FALSE, TRUE, 0);
  gtk_entry_set_editable (GTK_ENTRY (format), TRUE);

  if (get_option(&fld->properties,"FORMAT") != NULL)
    gtk_entry_set_text (GTK_ENTRY (format), 
		get_option(&fld->properties,"FORMAT"));
  gtk_signal_connect (GTK_OBJECT (format), "changed",
		      GTK_SIGNAL_FUNC (format_changed), info);
  gtk_widget_show (format);

  label = gtk_label_new (_ ("Default value"));
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);
  gtk_widget_show (label);


  defaultt = gtk_entry_new ();
  gtk_box_pack_start (GTK_BOX (vbox), defaultt, FALSE, TRUE, 0);
  gtk_entry_set_editable (GTK_ENTRY (defaultt), TRUE);

  if (get_option(&fld->properties,"DEFAULT") != NULL)
    gtk_entry_set_text (GTK_ENTRY (defaultt), 
		get_option(&fld->properties,"DEFAULT"));
  gtk_signal_connect (GTK_OBJECT (defaultt), "changed",
		      GTK_SIGNAL_FUNC (default_changed), info);
  gtk_widget_show (defaultt);



  label = gtk_label_new (_ ("Multimedia file type"));
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);
  gtk_widget_show (label);


  om = gtk_option_menu_new();
  om_menu = gtk_menu_new();


  loop = g_list_first(multimedia_type);
  while (loop != NULL)
  {
    om_item = gtk_menu_item_new_with_label(loop->data);
    gtk_widget_show(om_item);
    gtk_menu_append(GTK_MENU(om_menu), om_item);
    gtk_object_set_user_data(GTK_OBJECT(om_item),loop->data);
    gtk_signal_connect (GTK_OBJECT (om_item), "activate",
  		      GTK_SIGNAL_FUNC (type_changed), info);
    loop = loop->next;
  }

  gtk_widget_show(om_menu);
  gtk_option_menu_set_menu(GTK_OPTION_MENU(om), om_menu);

  tel =0;
  if (get_option(&fld->properties,"TYPE") != NULL)
  {
    loop = g_list_first(multimedia_type);
    while (loop != NULL)
    {
      if (strcasecmp(get_option(&fld->properties,"TYPE"),loop->data)==0)
      {
        gtk_option_menu_set_history(GTK_OPTION_MENU(om),tel);
      }
      tel++;
      loop = loop->next;
    }
  }
  else
    gtk_option_menu_set_history(GTK_OPTION_MENU(om),0);
  
  gtk_widget_show(om);
  gtk_box_pack_start (GTK_BOX (vbox), om, FALSE, TRUE, 0);

  if (fld->type != T_MULTIMEDIA)
  {
     gtk_widget_set_sensitive(om,FALSE);
  }


  closebut = gtk_button_new ();
  label = xpm_label_box (ja_xpm, _ ("_Close"), 1, &key);
  gtk_widget_show (label);
  gtk_container_add (GTK_CONTAINER (closebut), label);
  gtk_box_pack_end (GTK_BOX (vbox), closebut, FALSE, TRUE, 0);
  gtk_widget_show (closebut);
  gtk_signal_connect (GTK_OBJECT (closebut), "clicked",
		      GTK_SIGNAL_FUNC (close_win), info);
  gtk_widget_add_accelerator (closebut, "clicked", accel, key, GDK_CONTROL_MASK,
			      GTK_ACCEL_VISIBLE);

  info->namelist = namelist;
  info->desclist = desclist;
  info->type =  combo;
  info->format = format;
  info->defaultt = defaultt;
  info->mmtype = om;
  info->window = window;
  info->s = s;
  info->fld = fld; 


  gtk_container_add (GTK_CONTAINER (window), hbox);
  gtk_widget_show (hbox);
  /*gtk_accel_group_attach (accel, GTK_OBJECT (window));*/

}
