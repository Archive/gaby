/*
   preliminary action window

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */

#include "main.h"

static void
update_actions(GtkWidget *text, gpointer data)
{
  file_s **s;
  gint t;
  s = data;

  t = gtk_text_get_length(GTK_TEXT(text));
  gtk_text_set_point(GTK_TEXT(text),0);
  gtk_text_forward_delete(GTK_TEXT(text),t);
  if ((*s)->actions != NULL)
    gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, (*s)->actions, -1);
}


static void
update_event (GtkWidget * widget, GtkWidget * text)
{
  file_s **s;
  s = gtk_object_get_user_data (GTK_OBJECT (text));

  g_free ((*s)->actions);
  (*s)->actions = gtk_editable_get_chars (GTK_EDITABLE (text), 0, -1);
#ifdef DEBUG
  debug_print ("Update\n");
#endif
  (*s)->changed =TRUE;
}

#ifndef NOTEBOOKVIEW

static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  return FALSE;
}

static void
destroy (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  file_s **s;
  s = gtk_object_get_user_data (GTK_OBJECT (widget));
  (*s)->actionwin = NULL;
}

#endif

GtkWidget *
create_action_window (gchar * caption, file_s **s)
{
  GtkWidget *vbox;
  GtkWidget *text;
  GtkWidget *window;
  GtkWidget *scroll;
  GtkWidget *button;
  GtkWidget *label;

  if ((*s)->actionwin != NULL)
    {
      gdk_window_raise ((*s)->actionwin->window);
      return (*s)->actionwin;
    }

#ifndef NOTEBOOKVIEW
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), caption);

  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_event), NULL);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (destroy), NULL);
#else
  window = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(window),GTK_SHADOW_NONE);
#endif

  (*s)->actionwin = window;
  gtk_object_set_user_data (GTK_OBJECT (window), s);

  gtk_widget_show (window);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  /* The box */
  vbox = gtk_vbox_new (FALSE, 5);

  label = gtk_label_new ("This is a unfinished part of GDB\n" 
			 "for now you must edit in this window.");
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);
  gtk_widget_show (label);

  /* text */
  text = gtk_text_new (NULL, NULL);
  gtk_widget_set_usize (text, 300, 100);
  gtk_widget_show (text);
  gtk_object_set_user_data (GTK_OBJECT (text), s);
  scroll = scroll_new (text);
  gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);
  gtk_text_set_editable (GTK_TEXT (text), TRUE);

  button = gtk_button_new_with_label ("Commit changes");
  gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, TRUE, 0);
  gtk_widget_show (button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (update_event), text);
  if ((*s)->actions != NULL)
    gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, (*s)->actions, -1);


  gtk_container_add (GTK_CONTAINER (window), vbox);
  gtk_widget_show (vbox);
  
  add_update(text, s, update_actions);

  return window;
}
