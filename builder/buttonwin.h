/*
   Main window

   Gaby Description Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

 */



void create_button_window (gchar * caption);
/* These function are for fileops.c */
gint load_x (gchar * name, gpointer data);
void close_x (gpointer data);
gint save_x (gchar * name, gpointer data);
void new_x (gpointer data);
void stop_x (gpointer data);
void cancel_x (gpointer data);
gint get_file_changed (gpointer data);
void set_file_changed (gint ch, gpointer data);
gchar *get_file_name (gpointer data);


