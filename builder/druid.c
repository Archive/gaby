/*  Gaby Databases Builder
 *  Copyright (C) 1998-1999 Ron Bessems
 *  Copyright (C) 2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "main.h"
extern file_s *s;
extern doublestring view_names[];

#ifndef USE_GNOME_1
void launch_createdb_druid()
{
	/* not available */;
}
#else

#include "icons/gnome-gaby-builder.xpm"

static GdkImlibImage* create_logo()
{
	GdkImlibImage *logo;
	
	logo = gdk_imlib_create_image_from_xpm_data (builder_icon_xpm);
	/* but that could use gnome_pixmap_file or... */

	return logo;
}

static void druid_page_start_next (GnomeDruidPage *page, GnomeDruid *druid,
				   gpointer data)
{
	GtkWidget *entry;
	GList *list;
	GnomeDruidPage *pagenext;
	
	list = g_list_find(druid->children, page);
	pagenext = list->next->data;
	entry = gtk_object_get_data(GTK_OBJECT(pagenext), "entry_name");
	gtk_widget_grab_focus(entry);
	gnome_druid_set_page(druid, pagenext);
}

static GtkWidget* druid_page_start()
{
	GtkWidget *page;
	
	page = gnome_druid_page_start_new_with_vals (
		_("Creating a new database"),
		/* WISHLIST (gnome_druid_page_start): auto-wrapped text */
_("This druid will help you to create quickly a new database.\n"
  "It won't be the most featureful database possible but\n"
  "something useful that you will be able to extend later.\n"),
		create_logo(), NULL);
		
	gtk_widget_show (page);
	
	gtk_signal_connect (GTK_OBJECT (page), "next",
			GTK_SIGNAL_FUNC (druid_page_start_next), NULL);
	return page;
}

static void druid_page_name_next (GnomeDruidPage *page, GnomeDruid *druid,
				  gpointer data)
{
	GtkWidget *entry;
	gchar *text;
	GList *list;
	GnomeDruidPage *pagenext;
	
	entry = gtk_object_get_data(GTK_OBJECT(page), "entry_name");
	text = gtk_entry_get_text(GTK_ENTRY(entry));
	if ( strlen(text) == 0 || strlen(text) > 20 || strchr(text, ' ')) {
		GtkWidget *msgbox;
		msgbox = gnome_warning_dialog (
			_("You have to give the database a name\n"
			  "(less than 20 characters, no spaces)") );
		gtk_widget_show(msgbox);
		return;
	}
	
	list = g_list_find(druid->children, page);
	pagenext = list->next->data;
	entry = gtk_object_get_data(GTK_OBJECT(pagenext), "entry_tablename");
	gtk_entry_set_text(GTK_ENTRY(entry), text);
	gtk_widget_grab_focus(entry);
	gnome_druid_set_page(druid, pagenext);
}

static GtkWidget* druid_page_name()
{
	GtkWidget *page;
	GtkWidget *druid_vbox;
	GtkWidget *label;
	GtkWidget *table;
	GtkWidget *entry_name;
	GtkWidget *entry_purpose;

	page = gnome_druid_page_standard_new_with_vals (
					_("Name and purpose"), create_logo() );

	druid_vbox = GNOME_DRUID_PAGE_STANDARD (page)->vbox;
	gtk_widget_show(druid_vbox);
	gtk_container_set_border_width (GTK_CONTAINER (druid_vbox), 5);

	label = gtk_label_new (
_("Each database has a name used to identify it to gaby "
  "(so it can be used from the File / Databases / Other menu "
  "or as parameter on Gaby startup)."
  "\n"
  "This name should be connected to the purpose of the database "
  "(don't name it \"fred\" even if it's your name)."
  "\n"
  "The purpose line should be a short description (a few words).") );
	gtk_label_set_line_wrap (GTK_LABEL(label), TRUE);
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX(druid_vbox), label, FALSE, FALSE, 0);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);

	table = gtk_table_new (2, 2, TRUE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (druid_vbox), table, TRUE, FALSE, 0);
	gtk_table_set_row_spacings (GTK_TABLE (table), 5);

	label = gtk_label_new (_("Name (one word): "));
	gtk_widget_show (label);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);

	label = gtk_label_new (_("Purpose: "));
	gtk_widget_show (label);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);

	entry_name = gtk_entry_new ();
	gtk_widget_show (entry_name);
	gtk_table_attach_defaults(GTK_TABLE (table), entry_name, 1, 2, 0, 1 );

	entry_purpose = gtk_entry_new ();
	gtk_widget_show (entry_purpose);
	gtk_table_attach_defaults(GTK_TABLE (table), entry_purpose, 1, 2, 1, 2);

	gtk_widget_show(page);
	
	gtk_object_set_data(GTK_OBJECT(page), "entry_name", entry_name);
	gtk_object_set_data(GTK_OBJECT(page), "entry_purpose", entry_purpose);
	gtk_signal_connect (GTK_OBJECT (page), "next",
			GTK_SIGNAL_FUNC (druid_page_name_next), NULL);

	return page;
}

static void druid_edittable_add_clicked(GtkWidget* button, GtkObject *page)
{
	GtkCList *clist = gtk_object_get_data(page, "clist");
	gchar *a_line[2];
	int row;

	a_line[0] = _("field");
	a_line[1] = _("String");

	row = gtk_clist_append(clist, a_line);
	gtk_clist_select_row(clist, row, 0);
}

static void druid_edittable_delete_clicked(GtkWidget* button, GtkObject *page)
{
	GtkCList *clist = gtk_object_get_data(page, "clist");
	if ( ! clist->selection ) return; /* shouldn't happen */
	gtk_clist_remove(clist, GPOINTER_TO_INT(clist->selection->data));
}

static void druid_edittable_select_row(GtkCList *clist, gint row, gint column,
				       GdkEventButton *event, GtkObject *page )
{
	GtkEntry *entry = gtk_object_get_data(page, "entry_fieldname");
	GtkCombo *combo = gtk_object_get_data(page, "combo_fieldtype");
	GtkWidget *button = gtk_object_get_data(page, "button_delete");
	gchar *cell;
	
	gtk_widget_set_sensitive(GTK_WIDGET(entry), TRUE);
	gtk_widget_set_sensitive(GTK_WIDGET(combo), TRUE);
	gtk_widget_set_sensitive(GTK_WIDGET(button), TRUE);

	gtk_clist_get_text(clist, row, 0, &cell);
	gtk_entry_set_text(entry, cell);
	gtk_widget_grab_focus(GTK_WIDGET(entry));
	gtk_entry_select_region(entry, 0, -1);
	gtk_clist_get_text(clist, row, 1, &cell);
	gtk_entry_set_text(GTK_ENTRY(combo->entry), cell);
}

static void druid_edittable_unselect_row(GtkCList *clist, gint row, gint column,
					 GdkEventButton *event, GtkObject *page)
{
	GtkEntry *entry = gtk_object_get_data(page, "entry_fieldname");
	GtkCombo *combo = gtk_object_get_data(page, "combo_fieldtype");
	GtkWidget *button = gtk_object_get_data(page, "button_delete");
	
	gtk_entry_set_text(entry, "");
	gtk_widget_set_sensitive(GTK_WIDGET(entry), FALSE);
	gtk_widget_set_sensitive(GTK_WIDGET(combo), FALSE);
	gtk_widget_set_sensitive(GTK_WIDGET(button), FALSE);
}

static void druid_edittable_fieldname_changed(GtkEntry *entry, GtkObject *page)
{
	GtkCList *clist = gtk_object_get_data(page, "clist");
	gchar *text = gtk_entry_get_text(entry);
	
	if ( ! clist->selection ) return; /* shouldn't happen */

	gtk_clist_set_text(clist, GPOINTER_TO_INT(clist->selection->data),
			0, text);
}

static void druid_edittable_fieldtype_changed(GtkEntry *entry, GtkObject *page)
{
	GtkCList *clist = gtk_object_get_data(page, "clist");
	gchar *text = gtk_entry_get_text(entry);
	
	if ( ! clist->selection ) return; /* shouldn't happen */

	gtk_clist_set_text(clist, GPOINTER_TO_INT(clist->selection->data),
			1, text);
}

static void druid_edittable_up(GtkWidget *but, GtkCList *clist)
{
	int row;

	if ( ! clist->selection ) return;
	
	row = GPOINTER_TO_INT(clist->selection->data);
	gtk_clist_swap_rows(clist, row, row-1);
}

static void druid_edittable_down(GtkWidget *but, GtkCList *clist)
{
	int row;

	if ( ! clist->selection ) return;
	
	row = GPOINTER_TO_INT(clist->selection->data);
	gtk_clist_swap_rows(clist, row, row+1);
}

static void druid_page_edittable_next(GtkObject *page, GnomeDruid *druid,
				      gpointer data)
{
	GtkWidget *entry;
	GtkCList *clist;
	gchar *text;
	GList *list;
	GnomeDruidPage *pagenext;
	
	entry = gtk_object_get_data(page, "entry_tablename");
	text = gtk_entry_get_text(GTK_ENTRY(entry));
	if ( strlen(text) == 0 ) {
		GtkWidget *msgbox;
		msgbox = gnome_warning_dialog (
			_("You have to give the table a name") );
		gtk_widget_show(msgbox);
		return;
	}
	
	clist = gtk_object_get_data(page, "clist");
	if ( clist->rows == 0 ) {
		GtkWidget *msgbox;
		msgbox = gnome_warning_dialog (
			_("You should add some fields (minimum 1)\n"
			  "to your table") );
		gtk_widget_show(msgbox);
		return;
	}

	list = g_list_find(druid->children, page);
	pagenext = list->next->data;
	gnome_druid_set_page(druid, pagenext);

}

static GtkWidget* druid_page_edittable()
{
	GtkWidget *page;
	GtkWidget *druid_vbox;
	GtkWidget *table;
	GtkWidget *label;
	GtkWidget *entry_tablename;
	GtkWidget *frame;
	GtkWidget *hbox;
	GtkWidget *vbox1;
	GtkWidget *sclwin, *clist;
	GtkWidget *hbuttonbox;
	GtkWidget *button;
	GtkWidget *vbox2;
	GtkWidget *entry_fieldname;
	GtkWidget *combo_fieldtype;
	GList *combo_items = NULL;
	
	page = gnome_druid_page_standard_new_with_vals(
						_("Fields"), create_logo() );

	druid_vbox = GNOME_DRUID_PAGE_STANDARD (page)->vbox;
	gtk_widget_show (druid_vbox);
	gtk_container_set_border_width (GTK_CONTAINER (druid_vbox), 5);

	table = gtk_table_new (1, 2, FALSE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (druid_vbox), table, FALSE, FALSE, 0);

	label = gtk_label_new (_("Name of the table: "));
	gtk_widget_show (label);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);

	entry_tablename = gtk_entry_new ();
	gtk_object_set_data(GTK_OBJECT(page), "entry_tablename", entry_tablename);
	/* WISHLIST: fill with the name of the database */
	gtk_widget_show (entry_tablename);
	gtk_table_attach_defaults (GTK_TABLE (table), entry_tablename, 1, 2, 0, 1);

	frame = gtk_frame_new (_("Fields"));
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (druid_vbox), frame, TRUE, TRUE, 0);

	hbox = gtk_hbox_new (FALSE, 5);
	gtk_widget_show (hbox);
	gtk_container_add (GTK_CONTAINER (frame), hbox);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);

	vbox1 = gtk_vbox_new (FALSE, 5);
	gtk_widget_show (vbox1);
	gtk_box_pack_start (GTK_BOX (hbox), vbox1, TRUE, TRUE, 0);


	sclwin = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (sclwin);
	gtk_box_pack_start (GTK_BOX (vbox1), sclwin, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sclwin),
					GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);

	clist = gtk_clist_new (2);
	gtk_object_set_data(GTK_OBJECT(page), "clist", clist);
	gtk_widget_show (clist);
	gtk_container_add (GTK_CONTAINER (sclwin), clist);
	gtk_clist_set_column_width (GTK_CLIST (clist), 0, 90);
/*	gtk_clist_set_column_width (GTK_CLIST (clist), 1, 80);*/
	gtk_clist_column_titles_show (GTK_CLIST (clist));

	gtk_signal_connect(GTK_OBJECT(clist), "select_row",
			GTK_SIGNAL_FUNC(druid_edittable_select_row), page);
	gtk_signal_connect(GTK_OBJECT(clist), "unselect_row",
			GTK_SIGNAL_FUNC(druid_edittable_unselect_row), page);

	label = gtk_label_new (_("Name"));
	gtk_widget_show (label);
	gtk_clist_set_column_widget (GTK_CLIST (clist), 0, label);

	label = gtk_label_new (_("Type"));
	gtk_widget_show (label);
	gtk_clist_set_column_widget (GTK_CLIST (clist), 1, label);

	hbuttonbox = gtk_hbutton_box_new ();
	gtk_widget_show (hbuttonbox);
	gtk_box_pack_start (GTK_BOX (vbox1), hbuttonbox, FALSE, TRUE, 0);

	button = gnome_stock_button (GNOME_STOCK_BUTTON_UP);
	gtk_widget_show (button);
	gtk_container_add (GTK_CONTAINER (hbuttonbox), button);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
						druid_edittable_up, clist);

	button = gnome_stock_button (GNOME_STOCK_BUTTON_DOWN);
	gtk_widget_show (button);
	gtk_container_add (GTK_CONTAINER (hbuttonbox), button);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
						druid_edittable_down, clist);

	frame = gtk_frame_new (_("Field informations"));
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 0);

	vbox2 = gtk_vbox_new (FALSE, 0);
	gtk_widget_show (vbox2);
	gtk_container_add (GTK_CONTAINER (frame), vbox2);

	table = gtk_table_new (2, 2, FALSE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (vbox2), table, TRUE, FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (table), 5);
	gtk_table_set_row_spacings (GTK_TABLE (table), 5);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);

	label = gtk_label_new (_("Name: "));
	gtk_widget_show (label);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);

	label = gtk_label_new (_("Type: "));
	gtk_widget_show (label);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);

	entry_fieldname = gtk_entry_new ();
	gtk_signal_connect(GTK_OBJECT(entry_fieldname), "changed",
				druid_edittable_fieldname_changed, page);
	gtk_object_set_data(GTK_OBJECT(page), "entry_fieldname", entry_fieldname);
	gtk_widget_show (entry_fieldname);
	gtk_table_attach( GTK_TABLE (table), entry_fieldname, 1, 2, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_widget_set_sensitive(entry_fieldname, FALSE);

	combo_fieldtype = gtk_combo_new ();
	gtk_combo_set_value_in_list(GTK_COMBO(combo_fieldtype), TRUE, TRUE);
	gtk_object_set_data(GTK_OBJECT(page), "combo_fieldtype", combo_fieldtype);
	gtk_widget_show (combo_fieldtype);
	gtk_table_attach (GTK_TABLE (table), combo_fieldtype, 1, 2, 1, 2,
			(GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			(GtkAttachOptions) (0), 0, 0);
	combo_items = g_list_append (combo_items, _("String"));
	combo_items = g_list_append (combo_items, _("Strings"));
	combo_items = g_list_append (combo_items, _("Integer"));
	combo_items = g_list_append (combo_items, _("Real"));
	combo_items = g_list_append (combo_items, _("Date"));
	gtk_combo_set_popdown_strings (GTK_COMBO(combo_fieldtype), combo_items);
	g_list_free (combo_items);
	gtk_widget_set_sensitive(combo_fieldtype, FALSE);

	gtk_entry_set_text (GTK_ENTRY(GTK_COMBO(combo_fieldtype)->entry),
							_("String"));
	gtk_signal_connect(GTK_OBJECT(GTK_COMBO(combo_fieldtype)->entry),
			"changed", druid_edittable_fieldtype_changed, page);

	hbuttonbox = gtk_hbutton_box_new ();
	gtk_widget_show (hbuttonbox);
	gtk_box_pack_start (GTK_BOX (vbox2), hbuttonbox, TRUE, TRUE, 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (hbuttonbox), GTK_BUTTONBOX_SPREAD);

	button = gtk_button_new_with_label (_("Add"));
	gtk_widget_show (button);
	gtk_container_add (GTK_CONTAINER (hbuttonbox), button);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
					druid_edittable_add_clicked, page);

	button = gtk_button_new_with_label (_("Delete"));
	gtk_widget_show (button);
	gtk_container_add (GTK_CONTAINER (hbuttonbox), button);
	gtk_widget_set_sensitive(button, FALSE);
	gtk_object_set_data(GTK_OBJECT(page), "button_delete", button); 
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
					druid_edittable_delete_clicked, page);

	gtk_widget_show(page);
	
	/* the druid won't go to the next page if a local handler is
	 * defined. (but it works for page_name !!!)
	 * (FIXED (done it another way))
	 */
	gtk_signal_connect (GTK_OBJECT (page), "next",
			GTK_SIGNAL_FUNC (druid_page_edittable_next), NULL);
	
	return page;
}

static void druid_appearance_cb_toggled(GtkToggleButton *cb, GtkWidget *tb)
{
	gtk_widget_set_sensitive(tb, gtk_toggle_button_get_active(cb) );
}

static GtkWidget* druid_page_appearance()
{
	GtkWidget *page;
	GtkWidget *druid_vbox;
	GtkWidget *table;
	GtkWidget *label;
	
	GtkWidget *comboviews;
	GList *comboviews_items = NULL;
	GtkWidget *frame;
	GtkWidget *cb;
	GtkWidget *tb;
	gchar* possible_views[] = {
		N_("Form"),
		N_("List"),
		N_("Extended list"),
		N_("Search"),
		N_("Filter"),
		NULL
	};
	GList *cb_views = NULL;
	int i;

	/* WISHLIST: page_apperance UI could be improved */

	page = gnome_druid_page_standard_new_with_vals (
					_("Appearance"), create_logo() );

	druid_vbox = GNOME_DRUID_PAGE_STANDARD (page)->vbox;
	gtk_widget_show (druid_vbox);
	gtk_container_set_border_width (GTK_CONTAINER (druid_vbox), 5);

	table = gtk_table_new (1, 2, FALSE);
	gtk_widget_show (table);
	gtk_box_pack_start (GTK_BOX (druid_vbox), table, TRUE, FALSE, 0);

	label = gtk_label_new (_("Startup view: "));
	gtk_widget_show (label);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);

	comboviews = gtk_combo_new ();
	gtk_object_set_data(GTK_OBJECT(page), "combo_views", comboviews);
	gtk_combo_set_value_in_list(GTK_COMBO(comboviews), TRUE, TRUE);
	gtk_widget_show (comboviews);
	gtk_table_attach (GTK_TABLE (table), comboviews, 1, 2, 0, 1,
			(GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			(GtkAttachOptions) (0), 0, 0);
	comboviews_items = g_list_append (comboviews_items, _("Form"));
	comboviews_items = g_list_append (comboviews_items, _("Extended List"));
	gtk_combo_set_popdown_strings (GTK_COMBO (comboviews), comboviews_items);
	g_list_free (comboviews_items);

	gtk_entry_set_text (GTK_ENTRY(GTK_COMBO(comboviews)->entry), _("Form"));

	frame = gtk_frame_new (_("Other views"));
	gtk_widget_show (frame);
	gtk_box_pack_start (GTK_BOX (druid_vbox), frame, TRUE, TRUE, 0);

	table = gtk_table_new (5, 2, FALSE);
	gtk_widget_show (table);
	gtk_container_add (GTK_CONTAINER (frame), table);
	gtk_table_set_row_spacings (GTK_TABLE (table), 5);
	gtk_table_set_col_spacings (GTK_TABLE (table), 5);

	for ( i = 0; possible_views[i]; i++ ) {
		cb = gtk_check_button_new_with_label(_(possible_views[i]));
		cb_views = g_list_append(cb_views, cb);
		gtk_widget_show (cb);
		gtk_table_attach_defaults(GTK_TABLE(table), cb, 0, 1, 0+i, 1+i);
		
		tb = gtk_toggle_button_new_with_label (_("Displayed on startup"));
		gtk_widget_show (tb);
		gtk_table_attach_defaults(GTK_TABLE(table), tb, 1, 2, 0+i, 1+i);
		gtk_widget_set_sensitive(tb, FALSE);

		gtk_object_set_data(GTK_OBJECT(cb), "tb", tb);

		gtk_signal_connect(GTK_OBJECT(cb), "toggled", 
					druid_appearance_cb_toggled, tb);
	}
	
/*	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb_views->data), TRUE);
*/	gtk_object_set_data(GTK_OBJECT(page), "cb_views",  cb_views );
	
	gtk_widget_show(page);
	return page;
}

static GtkWidget* druid_page_finish(GtkWidget *window)
{
	GtkWidget *page;
	
	page = gnome_druid_page_finish_new_with_vals (
		_("New database created"),
_("Your database will be created when you'll click on \"Finish\".\n\n"
  "You will then be able to edit it within the builder to add new\n"
  "tables, fields, ...\n\n"
  "Once you're done you will be able to use your database by\n"
  "typing its name in File/Databases/Other... within Gaby or by\n"
  "running \"gaby --as <database name>\".\n\n"
  "Have fun!\n\n"
  "(PS: if you create a database that you feel might be useful\n"
  "to other persons to don't hesitate to send it to me at\n"
  "fpeters@theridion.com so I can include it in the next release of Gaby."),
		create_logo(), NULL );
	gtk_widget_show(page);

	return page;
}

static void cancel_druid(GtkWidget *button, GtkWidget *window)
{
	gtk_widget_destroy(window);
	gtk_widget_show(Window);
}

static void finish_druid(GtkObject *blah, GnomeDruid *druid, GtkWidget *window)
{
	GList *list = druid->children;
	GtkWidget *page;
	GtkEntry *entry;
	GtkCList *clist;
	GtkToggleButton *cb, *tb;
	GtkCombo *combo;
	gchar *table_types[] = {
		"String", "Strings", "Integer", "Real", "Date", "Boolean",
		"Record", "Records", "Multimedia", "Decimal"
	};	/* TODO (post-2.0): do not duplicate type names everywhere !!!
		 * delayed since they're not everywhere; only here and in
		 * main.c */
	
	table *tab;
	field *fld;
	subtable *stab;
	subfield *sfld;
	misc_s *misc;

	GList *cblist;
	char fn[PATH_MAX];
	int i;
	gchar *str;

	s = new_desc();

	list = g_list_next(list);	/* going to 'name & purpose' page */
	page = list->data;
	entry = gtk_object_get_data(GTK_OBJECT(page), "entry_name");
	if ( getenv("HOME") ) {
		sprintf(fn, "%s/.gaby/desc.%s", getenv("HOME"), 
						gtk_entry_get_text(entry) );
	} else {
		sprintf(fn, "./desc.%s", gtk_entry_get_text(entry) );
	}
	s->name = g_strdup(fn);
	
	list = g_list_next(list);	/* going to 'edit table' page */
	page = list->data;
	entry = gtk_object_get_data(GTK_OBJECT(page), "entry_tablename");
	
	tab = new_table();
	s->tables = g_list_append(s->tables, tab);
	tab->name = g_strdup(gtk_entry_get_text(entry));
	
	stab = new_subtable();
	s->subtables = g_list_append(s->subtables, stab);
	stab->name = g_strdup(gtk_entry_get_text(entry));
	stab->name_of_table = g_strdup(gtk_entry_get_text(entry));

	clist = gtk_object_get_data(GTK_OBJECT(page), "clist");
	for ( i=0; i<clist->rows; i++ ) {
		gchar *text;
		int j;
		
		fld = new_field();
		tab->fields = g_list_append(tab->fields, fld);

		gtk_clist_get_text(clist, i, 1, &text);
		j = 0;
		while ( strcmp(text, _(table_types[j])) != 0 ) j++;
		fld->type = j;

		gtk_clist_get_text(clist, i, 0, &text);
		fld->name = g_strdup(text);
		
		sfld = new_subfield();
		sfld->name = g_strdup(text);
		sfld->name_of_field = g_strdup(text);
		stab->subfields = g_list_append(stab->subfields, sfld);
	}
	
	list = g_list_next(list);	/* going to 'appearance' page */
	page = list->data;
	
	combo = gtk_object_get_data(GTK_OBJECT(page), "combo_views");
	str = gtk_entry_get_text(GTK_ENTRY(combo->entry));

	i=0;
	while ( strcmp(view_names[i].locale, str) != 0 ) i++;
	misc = new_misc_s();
	misc->view_as = g_strdup(view_names[i].name);
	misc->name_of_subtable = g_strdup(stab->name);
	misc->main = TRUE;
	s->miscs = g_list_append(s->miscs, misc);
	
	cblist = gtk_object_get_data(GTK_OBJECT(page), "cb_views");
	/* removing the default 'list' view */
	stab->viewable_as = g_list_remove(stab->viewable_as,
						stab->viewable_as->data);
	stab->viewable_as = g_list_append(stab->viewable_as, 
						g_strdup(view_names[i].name) );
	
	i = 0;
	while ( cblist ) {
		gchar *st;
		int j;
		cb = cblist->data;
		cblist = g_list_next(cblist);
		i++;
		if ( gtk_toggle_button_get_active(cb) == FALSE ) continue;

		st = view_names[i-1].locale;
		j = 0;
		while ( strcmp(view_names[j].locale, st) != 0 ) j++;
		
		stab->viewable_as = g_list_append(stab->viewable_as, 
				g_strdup(view_names[j].name) );
	
		tb = gtk_object_get_data(GTK_OBJECT(cb), "tb");
		if ( gtk_toggle_button_get_active(tb) == TRUE ) {
			misc = new_misc_s();
			misc->view_as = g_strdup(view_names[j].name);
			misc->name_of_subtable = g_strdup(stab->name);
			misc->main = FALSE;
			s->miscs = g_list_append(s->miscs, misc);
		}
	}

	s->changed = TRUE;
	
	update_all();

	gtk_widget_destroy(window);
	gtk_widget_show(Window);
}

void launch_createdb_druid()
{
	GtkWidget *window;
	GtkWidget *druid;
	GtkWidget *druidpagestart;
	GtkWidget *druidpagename;
	GtkWidget *druidpageedittable;
	GtkWidget *druidpageappearance;
	GtkWidget *druidpagefinish;

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (window), _("New database druid"));

	druid = gnome_druid_new ();
	gtk_widget_show (druid);
	gtk_container_add (GTK_CONTAINER (window), druid);

	druidpagestart = druid_page_start();
	druidpagename = druid_page_name();
	druidpageedittable = druid_page_edittable();
	druidpageappearance = druid_page_appearance();
	druidpagefinish = druid_page_finish(window);
	
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druidpagestart));
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druidpagename));
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druidpageedittable));
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druidpageappearance));
	gnome_druid_append_page (GNOME_DRUID (druid),
				 GNOME_DRUID_PAGE (druidpagefinish));
	gnome_druid_set_page (GNOME_DRUID (druid),
			      GNOME_DRUID_PAGE (druidpagestart));

	gtk_signal_connect(GTK_OBJECT(druid), "cancel", cancel_druid, window);
	gtk_signal_connect(GTK_OBJECT(druidpagefinish), "finish",
							finish_druid, window);
	gtk_widget_show(window);
}

#endif

