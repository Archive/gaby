/*
   Misc window

   Gaby Databases Builder
   Copyright (C) 1998  Ron Bessems
   Contact me via email at R.E.M.W.Bessems@stud.tue.nl

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


 */


#include "main.h"
#ifndef NOTEBOOKVIEW
#include "icons/ja.xpm"
#endif
#include "icons/edit.xpm"
#include "icons/stock_add.xpm"
#include "icons/stock_remove.xpm"


typedef struct _main_update info1;

struct _main_update
  {
    GtkWidget *list;
    GtkWidget *window;
    file_s **s;
  };


/* ********************************************************************* */
/* Main misc win							 */
/* ********************************************************************* */


static void
update_main(GtkWidget *window, gpointer data)
{
  GList *selection;
  info1 *info;
  tlocation *tloc;
  GList *loop;
  gint row;
  gint srow;
  info = data;


  selection = GTK_CLIST(info->list)->selection;
  if (selection != NULL)
    srow = GPOINTER_TO_INT(selection->data);
  else
    srow = 0;

  gtk_clist_clear(GTK_CLIST(info->list));
  
  loop = g_list_first((*info->s)->tlocations);
  while (loop!=NULL)
  {
    tloc = loop->data;
    row = gtk_clist_append(GTK_CLIST(info->list),&tloc->name_of_table);
    gtk_clist_set_row_data(GTK_CLIST(info->list),row,tloc);
    loop = loop->next;
  }
  
  gtk_clist_select_row(GTK_CLIST(info->list),srow,0);

}
           

static void
add_clicked (GtkWidget * wid, info1 *info)
{
  tlocation *tloc ;
  table *tab;

  if (g_list_length((*info->s)->tables)==0)
  {
    error_dialog(_("No tables defined, please define one first"),
			_("Warning"));
    return;
  }

 
  tloc = new_tlocation();

  if (*info->s == NULL)
  {
	return;
  }
  
  (*info->s)->tlocations = g_list_append((*info->s)->tlocations,tloc);

  tab = g_list_first((*info->s)->tables)->data;  

  tloc->name_of_table =  g_strdup(tab->name);
  update_widget(info->window);
  (*info->s)->changed = TRUE; 
}

static void
edit_clicked(GtkWidget *wid, info1 *info)
{
  GList *selection;
  GtkWidget *list;
  tlocation *tloc;
  file_s *s;
  gint row;

  debug_print("ASDFASDFASDFASDF\n");
  list = info->list;
  s = *info->s; 
  selection = GTK_CLIST(list)->selection;
  if (selection==NULL)
	return;
  row = GPOINTER_TO_INT(selection->data);
  tloc  = gtk_clist_get_row_data(GTK_CLIST(list),row);

  create_location_window(tloc,"Locations",s);
  debug_print("ASD@#$@#$@#$FASDFASDFASDF\n");

}

static void
del_clicked(GtkWidget *wid, info1 *info)
{
  GList *selection;
  GtkWidget *window;
  GtkWidget *list;
  tlocation *tloc;
  file_s *s;
  gint row;

  s = *info->s ;
  window = info->window;
  list = info->list;

  selection = GTK_CLIST(list)->selection;
  if (selection==NULL)
	return;
  row = GPOINTER_TO_INT(selection->data);
  tloc = gtk_clist_get_row_data(GTK_CLIST(list),row);
  s->tlocations = g_list_remove(s->tlocations,tloc);
  delete_tlocation(tloc);
  update_widget(window);
  s->changed = TRUE;  
}



static void
clist_dclicked (GtkWidget * widget, GdkEventButton * event,
                info1 *info)
{
  debug_print("ASDF\n");
  if (event->type == GDK_2BUTTON_PRESS)
    {
      edit_clicked (widget, info);
    }
}




#ifndef NOTEBOOKVIEW
static void
close_clicked(GtkWidget *wid, info1 *info)
{
  gtk_widget_destroy(info->window);
}



static gboolean
delete_event (GtkWidget * widget, GdkEvent * event, gpointer data)
{
  return FALSE;
}

static void
destroy (GtkWidget * widget, info1 *info)
{
  remove_update(widget);
  (*info->s)->locationwin = NULL;
  g_free(info);
}

#endif

/* *********************************************************************** */
/* Create window                                                           */
/* *********************************************************************** */


GtkWidget *
create_tlocation_window(gchar * caption, file_s **s)
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *window;
  GtkWidget *button;
  GtkWidget *label;
  GtkWidget *frame;
  info1 *info;
  int key;
  static GtkAccelGroup *accel;

  GtkWidget *list;

  if ((*s)->locationwin != NULL)
  {
    gdk_window_raise ((*s)->locationwin->window);
    return NULL;
  }

  info = g_malloc (sizeof(info1));

#ifndef NOTEBOOKVIEW
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), caption);

  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		    GTK_SIGNAL_FUNC (delete_event), info);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		    GTK_SIGNAL_FUNC (destroy), info);
#else
  window = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(window),GTK_SHADOW_NONE);
#endif

  (*s)->locationwin = window;

  gtk_widget_show (window);
  gtk_container_set_border_width (GTK_CONTAINER (window), 5);

  accel = gtk_accel_group_new();

  hbox = gtk_hbox_new(FALSE,5);
  gtk_container_add(GTK_CONTAINER(window),hbox);
  gtk_widget_show(hbox);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_label(GTK_FRAME(frame),_("Tables"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, TRUE, TRUE, 5);


  vbox = gtk_vbox_new(TRUE,0);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);
  gtk_widget_show(vbox);
  gtk_container_add(GTK_CONTAINER(frame),vbox);

  list = gtk_clist_new(1);
  gtk_box_pack_start(GTK_BOX(vbox),list,TRUE,TRUE,0);
  gtk_clist_set_column_width(GTK_CLIST(list),0,150);
  gtk_clist_set_selection_mode (GTK_CLIST (list), GTK_SELECTION_BROWSE);
  gtk_widget_show(list);
  gtk_signal_connect (GTK_OBJECT (list), "button_press_event",
                      GTK_SIGNAL_FUNC (clist_dclicked), info);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_label(GTK_FRAME(frame),_("Options"));
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
  gtk_widget_show (frame);
  gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, TRUE, 5);

  vbox = gtk_vbox_new(FALSE,5);
  gtk_container_add(GTK_CONTAINER(frame),vbox);
  gtk_widget_show(vbox);
  gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);

  button = gtk_button_new();
  label = xpm_label_box(add_xpm, _("_Add location"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_start(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (add_clicked), info);
  gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);




  button = gtk_button_new();
  label = xpm_label_box(remove_xpm, _("_Delete location"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_start(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (del_clicked), info);
   gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);



    
 
  button = gtk_button_new();
  label = xpm_label_box(edit_xpm, _("_Edit location"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_start(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (edit_clicked), info);
  gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);



#ifndef NOTEBOOKVIEW 
  button = gtk_button_new();
  label = xpm_label_box(ja_xpm, _("_Close"),1,&key);
  gtk_widget_show(label);
  gtk_container_add(GTK_CONTAINER(button),label);
  gtk_box_pack_end(GTK_BOX(vbox),button,FALSE,TRUE,0);
  gtk_widget_show(button);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (close_clicked), info);
   gtk_widget_add_accelerator (button, "clicked", accel, key, GDK_CONTROL_MASK,                              GTK_ACCEL_VISIBLE);
#endif


  /* Set the info */
  info->list = list;
  info->window = window;
  info->s = s;

  add_update(info->window, info, update_main);
  /*gtk_accel_group_attach (accel, GTK_OBJECT (window));  */
  update_widget(info->window);

  return window;
}
