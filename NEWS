NEWS for veteran Gaby users
---------------------------

--- 2.0.3 - ???

Stable release. Ported to GTK+ 2.0 and Gnome 2.0. New Azerbaijani
Turkish and Portuguese translations.

--- 2.0.2 - July 7th 2001

Stable release. Fixed a serious bug in import/export.

--- 2.0.1 - June 28th 2001

Stable release. Added support to export address books to pilot-addresses
format and fixed misc bugs (mostly portability problems).

--- 2.0.0 - May 15th 2001 -- (codename: "The Magic Hour")

Stable release. Only change is the version number. Enjoy.

--- 1.9.98 - January 20th 2001

Added support for CSV file format import/export, improved support for
relations between tables, improved Python plug-in building and fixed bugs.

--- 1.9.97 - December 29th 2000

Added a new field type, improved a few GUI components, worked on the
documentation and binary packages and fixed several bugs.

--- 1.9.96 - December 10th 2000

Fixed a bug in the configure script.

--- 1.9.95 - December 9th 2000 -- (codename: "Let's See Who Goes Down First")

Added some new little features, fixed several bugs, improved documentation
and switched to "Stable release under the hood" mode (waiting your final
bug reports).

--- 1.9.25 - November 19th 2000

Improved seriously the possibility to use Gaby as a front-end to PostgreSQL,
added a (contributed) plug-in to create lout files, improved the database
builder and fixed several memory leaks.

--- 1.9.24 - September 27th 2000

Started the (experimental and incomplete!!!) possibility to use Gaby as a
front-end to PostgreSQL, improved some minor details and fixed some bugs.

--- 1.9.23 - August 31th 2000

Added a druid to help creating a new database in the builder, support for
a fixed number of decimals type of number (useful for money related fields),
a useful view plug-in based on libglade and Python and fixed GTK+ only version,
the embedded Python interpreter, configure.in and other bugs...

--- 1.9.22 - July 1st 2000

Improved error detection and reporting, fixed import/export functions and
several configure and makefile issues.

--- 1.9.21 - May 7th 2000

Fixed a Makefile problem that prevented Gaby to build correctly.

--- 1.9.20 - May 6th 2000

Added a new (better) print plug-in mechanism that uses XML files and Python,
an easy way to start new databases, a 'first minute in Gaby' dialogs series and
fixed several bugs.

I updated the INSTALL file to match the library requirements; you should really
read it if configure tells you that you misses some.

--- 1.9.19 - April 1st 2000

Added a (working) libglade plug-in that allow the use of forms defined within
Glade, began a new HTML print plug-in from scratch, improved minor things and
fixed several bugs.

I'd like to ask for special feedback about the HTML print plug-in interface;
are you ok with notebooks, do you have a feature request, ... ? Feedback about
the gladeform plug-in is also welcome but you'll have to figure how to use it
from the example (misc/desc.gabyglade and misc/gabyform.glade). I'll write
basic technical documentation about it for the next release.

--- 1.9.18 - March 4th 2000

Added an icon for Gnome, improved minor things and fixed some bugs.

--- 1.9.17 - January 26th 2000

Fixed screwed makefiles, it will now compile... (sorry for the inconvenience)

--- 1.9.16 - January 25th 2000

Added a common search plug-in, added (optional) back/forward buttons,
improved the Python module and fixed several bugs.

--- 1.9.15 - December 23th 1999

Minor improvements to the UI, great improvement on the scripting side (it can
now behave like a 'normal' Python module) and bug fixes.

--- 1.9.14 - October 23th 1999

Fixed several gtk-only and 'call me stupid' bugs.

--- 1.9.13 - October 22th 1999

Improvements to the form, canvas and gnomecard views, to the Python
interpreter as well as numerous bug fixes.

--- 1.9.12 - September 30th 1999

Fixed _lots_ of bugs (by adding a test suite) and added a new Danish
translation.

--- 1.9.11 - September 20th 1999

Improved (and speeded up) the indexes mechanism, added an 'home-brewed script'
option as well as new Japanese and Polish translations and bug fixes.

--- 1.9.10 - September 8th 1999

Rewrote the view plug-ins API in a cleaner way, added support for dBase (III
and ~IV) files as well as FreshMeat Appindex files, improved (a lot) file
loading (more than 10 times faster) and usual bug fixes.

This release is not named 1.9.9 because this number looked too much like '2.0
next week' - good reason.

--- 1.9.8 - August 15th 1999 -- (codename: "One Advice, Space")

No major changes but _lots_ of improvements and bug fixes notably in the Python
interpreter that can now do useful things. There are also new German, Norwegian
and Swedish translations.

--- 1.9.7 - June 12th 1999

A new version (0.8) of the description file builder, more support for filters,
a new canvas view, the possibility to run a stripped down Gaby that doesn't use
plug-ins, a Finnish translation and lots of bug fixes.

--- 1.9.6 - May 28th 1999

A new version (0.7) of the description file builder, filters (is, is not,
matches, is greater, ...) for the extended list view, bound windows can now be
defined in descfiles, import/export even in the plain-gtk version, improved
installation and a lot of bug fixes.

--- 1.9.5 - May 21th 1999

A new version (0.6) of the description file builder, a Spanish translation and
the usual bug fixes. The configuration files are now stored in
$(prefix)/etc/gaby (no more in /etc/gaby) if you want to keep the old location
add --sysconfdir=/etc to ./configure

--- 1.9.4 - May 18th 1999

Mostly bug fixes.

--- 1.9.3 - May 14th 1999

A new version (0.5) of the description file builder, a few improvements in the
GUI dialog boxes and lots of bug fixes.

--- 1.9.2 - May 13th 1999

A new version (0.4) of the description file builder, a new format plug-in to
read/write/import/export the videobase format and various bug fixes.

--- 1.9.1 - May 11th 1999

A 'tip of the day' dialog written in Python (and it is not yet possible to
avoid it) and bug fixes (the plain-gtk version _will_ compile now).

--- 1.9.0 - May 8th 1999 -- (codename: "The Ideal Crash")

Everything is new.

Everything was rewritten and Gaby is now a real personal databases manager
unlike 1.0 that was more an hacked addressbook than something else. But you
don't have to worry about simplicity since it is at least the same than
before and might even be improved.

1.9.0 (codenamed 'The Ideal Crash') is designed around plug-ins to keep a
light program while being able to fill almost every needs you could have for
keeping your data. This also allows you, developer, to easily contribute (don't
hesitate).

[For the record those entries are the announcements to Fresh Meat]

