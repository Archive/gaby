dnl Process this file with autoconf to produce a configure script.

AC_INIT(gaby, 2.0.3pre)
AC_CONFIG_SRCDIR(src)
AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)
AC_PROG_INTLTOOL

dnl 1.8.<date>  -> alpha
dnl 1.9.<digit> -> beta
dnl 2.0.0       -> stable

AM_CONFIG_HEADER(config.h)

rm -f .hints

dnl --- Checks for programs.
AC_PROG_CC
dnl Only use -Wall if we have gcc
if test "x$GCC" = "xyes"; then
  if test -z "`echo "$CFLAGS" | grep "\-Wall" 2> /dev/null`" ; then
      CFLAGS="$CFLAGS -Wall -g"
# or, if we want to profile :
#     CFLAGS="$CFLAGS -Wall -g -pg -a"
  fi
fi

AC_ISC_POSIX

AM_PROG_LIBTOOL
case "$host" in
	*-hp-*)
		# Mehdi Lavasani told me about this one. Thanks.
		AC_DEFINE(SO_EXTENSION, ".sl")
		;;
	*-pc-cygwin)
		# Nobody would want that. It is here for completeness :)
		AC_DEFINE(SO_EXTENSION, ".dll")
		;;
	*)
		AC_DEFINE(SO_EXTENSION, ".so")
		;;
esac

dnl --- check for Debian
AC_MSG_CHECKING(the system is Debian)
if test -f /etc/debian_version
then
	AC_DEFINE(USE_DEBIAN)
	AC_MSG_RESULT(yes)
else
	AC_MSG_RESULT(no)
fi

dnl --- for dtmfdial (in actions/net/)
AC_CHECK_HEADERS(linux/soundcard.h)

dnl --- regex
AC_CHECK_HEADERS(regex.h)

dnl --- Solaris doesn't have getopt.h (and getopt_long)
AC_CHECK_HEADERS(getopt.h)

AC_CHECK_FUNC(getopt, [AC_DEFINE(GETOPT_AVAILABLE)])
AC_CHECK_FUNC(getopt_long, [AC_DEFINE(GETOPTLONG_AVAILABLE)])

AC_HEADER_DIRENT()

AC_CHECK_FUNC(tmpnam, AC_DEFINE(HAVE_TMPNAM))

dnl --- Checks for libraries.

# I'll have a ncurses front-end, one day, ...
# AC_CHECK_LIB(ncurses, initscr, AC_DEFINE(HAVE_NCURSES))

dnl -- --disable-hints
AC_MSG_CHECKING("whether you want to skip configure hints")
AC_ARG_ENABLE(hints,
[  --disable-hints         Don't output hints to build a better Gaby],
[case "${enableval}" in
  yes) AC_MSG_RESULT("no"); hints=true ;;
  no)  AC_MSG_RESULT("yes"); hints=false ;;
  *) AC_MSG_ERROR(bad value ${enableval} for --disable-hints) ;;
esac],[AC_MSG_RESULT([no (default)]); hints=true])

dnl -- --disable-multimedia
AC_ARG_ENABLE(multimedia,
[  --disable-multimedia    build without sounds and pictures fields],
[case "${enableval}" in
  yes) multimedia=true ;;
  no)  multimedia=false ;;
  *) AC_MSG_ERROR(bad value ${enableval} for --disable-multimedia) ;;
esac],[multimedia=true])

PKG_CHECK_MODULES(GLIB, glib-2.0 gmodule-2.0, glib=true, glib=false)
AC_SUBST(GLIB_CFLAGS)
AC_SUBST(GLIB_LIBS)

if test x$glib = xfalse; then
	cat << _EOF_

You don't have GLIB 2.0 (in a version >= 1.3.any) (or I couldn't find it) and
this is a required library for Gaby.
If you know you have GLIB 2.0 library installed you should check you also
have development files installed. Those have names looking like libglib-dev,
glib-devel, ...

_EOF_
	exit 1
fi

gui=true
AC_ARG_ENABLE(gui,
[  --disable-gui           build a non-GUI version of Gaby (for Python)],
[case "${enableval}" in
	yes) gui=true;;
	no)  gui=false;;
esac])

PKG_CHECK_MODULES(GTK, gtk+-2.0, gtk=true, gtk=false)
AC_SUBST(GTK_CFLAGS)
AC_SUBST(GTK_LIBS)

if test x$gui = xtrue -a x$gtk = xfalse; then
	cat << _EOF_

You don't have GTK+ 2.0 (in a version >= 1.3.x) (or I couldn't find it) and
you did not specify --disable-gui on the command line to build a non-GUI
version of Gaby.

If you know you have GTK+ libraries installed you should check you also
have development files installed. Those have names looking like
libgtk-dev, gtk+-devel, ...

_EOF_
	exit 1
fi

# checks for multimedia fields (gdkpixbuf (or imlib) and esd)
if test "$multimedia" = "true"; then
	PKG_CHECK_MODULES(GDKPIXBUF, gdk-pixbuf-2.0, gdkpixbuf=true, gdkpixbuf=false)
	AC_SUBST(GDKPIXBUF_CFLAGS)
	AC_SUBST(GDKPIXBUF_LIBS)
	if test "$gdkpixbuf" = "true"; then
		image=true
		IMAGE_LDFLAGS="$GDKPIXBUF_LIBS"
		IMAGE_CFLAGS="$GDKPIXBUF_CFLAGS"
		AC_DEFINE(HAVE_GDKPIXBUF)
	fi
	
	AC_SUBST(IMAGE_LDFLAGS)
	AC_SUBST(IMAGE_CFLAGS)
	
	AM_PATH_ESD(0.2.8, esd=true, esd=false)
	if test "$esd" = "true"; then
		AC_DEFINE(HAVE_ESD)
		AC_SUBST(ESD_CFLAGS)
		AC_SUBST(ESD_LIBS)
	fi

fi

dnl --- check for libglade
PKG_CHECK_MODULES(LIBGLADE, libglade-2.0, libglade=true, libglade=false)
AC_SUBST(LIBGLADE_CFLAGS)
AC_SUBST(LIBGLADE_LIBS)
AM_CONDITIONAL(HAVE_LIBGLADE, test x$libglade = xtrue)
if test "$libglade" = "true"; then
	AC_DEFINE(HAVE_LIBGLADE)
fi

dnl --- check for NoSQL (2.0)
AC_CHECK_PROG(nosql, nosql, true, false)

dnl --- check for libxml
PKG_CHECK_MODULES(LIBXML, libxml-2.0, libxml=true, libxml=false)
AC_SUBST(LIBXML_CFLAGS)
AC_SUBST(LIBXML_LIBS)
AM_CONDITIONAL(HAVE_LIBXML, test x$libxml = xtrue)
if test x$libxml = xtrue ; then
	AC_DEFINE(HAVE_LIBXML)
fi

dnl --- check for Python, from Gnumeric
AC_CHECK_PROG(python_val, python, true, false)
if $python_val; then
	AC_MSG_CHECKING(for Python development files)
	PY_PREFIX=`python -c 'import sys ; print sys.prefix'`
	PY_EXEC_PREFIX=`python -c 'import sys ; print sys.exec_prefix'`
	changequote(<<, >>)dnl
	PY_VERSION=`python -c 'import sys ; print sys.version[0:3]'`
	changequote([, ])dnl
	PYTHON_H=$PY_PREFIX/include/python$PY_VERSION/Python.h
	LIBPYTHON_SO=$PY_PREFIX/lib/libpython$PY_VERSION.so
	if test ! -f $PYTHON_H; then
		cat << _EOF_ >> .hints

  You miss some Python development files. You should install a package
  called python-devel (Red Hat, Mandrake, ...) or python-dev (Debian, ...)
  if you want Python support included in Gaby.

_EOF_
	fi
 	
	if test -f $PYTHON_H; then
		AC_MSG_RESULT(yes)
		PY_LIBS="python$PY_VERSION"
		PY_LIB_LOC="-L$PY_EXEC_PREFIX/lib/python$PY_VERSION/config"
		PY_CFLAGS="-I$PY_PREFIX/include/python$PY_VERSION"
		PY_MAKEFILE="$PY_EXEC_PREFIX/lib/python$PY_VERSION/config/Makefile"
		PY_OTHER_LIBS=`sed -n -e 's/^LIBS=\(.*\)/\1/p' $PY_MAKEFILE`
		PY_EXTRA_LIBS="$PY_LOCALMODLIBS $PY_BASEMODLIBS $PY_OTHER_LIBS"
		PY_DYNLOAD="$PY_EXEC_PREFIX/lib/python$PY_VERSION/lib-dynload"
		PY_LIB_A="$PY_EXEC_PREFIX/lib/python$PY_VERSION/config/libpython$PY_VERSION.a"
		AC_SUBST(PY_LIBS)
		AC_SUBST(PY_LIB_LOC)
		AC_SUBST(PY_CFLAGS)
		AC_SUBST(PY_EXTRA_LIBS)
		AC_SUBST(PY_DYNLOAD)
		AC_SUBST(PY_LIB_A)
	else
		AC_MSG_RESULT(no)
		python_val=false
	fi
	
	AM_CONDITIONAL(USE_SHARED_PYTHON, test -f $LIBPYTHON_SO)
	#AM_CONDITIONAL(USE_SHARED_PYTHON, false)
	
	if test -f $PYTHON_H && test ! -f $LIBPYTHON_SO; then
	#if test -f $PYTHON_H; then
		echo "creating build environment for non-shared Python"
		rm -rf .extract
		mkdir .extract
		cd .extract
		ar xv $PY_PREFIX/lib/python$PY_VERSION/config/libpython$PY_VERSION.a > /dev/null
		for F in *.o
		do
			cp $F `basename $F .o`.lo
		done
		cd ..
	fi
		
fi

if $python_val && $gui; then
	# checking for pygtk
	AC_MSG_CHECKING(for pygtk)
	if test -f `gtk-config --prefix`/include/pygtk/pygtk.h; then
		AC_MSG_RESULT(yes)
		AC_DEFINE(HAVE_PYGTK)
	else
		AC_MSG_RESULT(no)
		cat << _EOF_ >> .hints
  You miss pygtk (a module that allows you to use gtk in Python programs).
  This means some functions in the Gaby python module will be disabled.
  On a Debian system you can install pygtk with 'apt-get install python-gtk'

_EOF_
	fi
fi
	
dnl --- Internationalization (in alphabetical order for convenience)
ALL_LINGUAS="az ca cs da de el en_CA en_GB es fi fr ga hr ja nl no pa pl pt pt_BR rw sq sr sr@Latn sv tr zh_CN"

GETTEXT_PACKAGE=gaby
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, "$GETTEXT_PACKAGE")
AM_GLIB_GNU_GETTEXT

test "x$prefix" = xNONE && prefix=$ac_default_prefix

AC_DEFINE_UNQUOTED(LOCALEDIR, "${prefix}/share/locale")
AC_DEFINE_UNQUOTED(DOCDIR, "${prefix}/doc/gaby")

dnl --- because Jouni K Seppanen <jks@iki.fi> wanted --sysconfdir=/home/jks/etc
dnl --- what a strange guy :)
dnl --- For others, sysconfdir is simply /etc

dnl --- this is no longer true, the default is back to $(prefix)/etc

tmp=$sysconfdir
test "$tmp" = '${prefix}/etc' && tmp=${prefix}/etc

AC_DEFINE_UNQUOTED(SYSCONF_DIR,"$tmp")
AC_SUBST(SYSCONF_DIR)

dnl --- where to install plug-ins ?

tmp=$libdir
test "$tmp" = '${exec_prefix}/lib' && tmp=${prefix}/lib

PLUGINS_DIR=$tmp/gaby/plug-ins
AC_DEFINE_UNQUOTED(PLUGINS_DIR,"$PLUGINS_DIR")
AC_SUBST(PLUGINS_DIR)

tmp=$datadir
test "$tmp" = '${prefix}/share' && tmp=${prefix}/share

SCRIPTS_DIR=$tmp/gaby/scripts/
AC_DEFINE_UNQUOTED(SCRIPTS_DIR,"$SCRIPTS_DIR")
AC_SUBST(SCRIPTS_DIR)

GLADE_DIR=$tmp/gaby/glade/
AC_DEFINE_UNQUOTED(GLADE_DIR,"$GLADE_DIR")
AC_SUBST(GLADE_DIR)

TIPS_FILE=$tmp/gaby/gaby_tips
AC_DEFINE_UNQUOTED(TIPS_FILE,"$TIPS_FILE")
AC_SUBST(TIPS_FILE)

dnl -- --enable-linkwithstatic
#AC_ARG_ENABLE(linkwithstatic,
#[  --enable-linkwithstatic  statically link against libxml and libglade],
#[case "${enableval}" in
#	yes) staticlink=true;;
#	no)  staticlink=false;;
#esac], [staticlink=false])
#
#AM_CONDITIONAL(STATIC_LINK, test x$staticlink = xtrue)
#
#if test "$libxml" = "true" -a "$staticlink" = "true"; then
#	LIBXML_LIBS=`xml-config --prefix`/lib/libxml.a -lz
#	AC_SUBST(LIBXML_LIBS)
#fi

dnl -- --enable-superfluous
AC_ARG_ENABLE(superfluous,
[  --enable-superfluous]    No comments, [AC_DEFINE(SUPERFLUOUS)])

dnl -- --enable-upgrade
AC_ARG_ENABLE(upgrade,
[  --enable-upgrade        let Gaby know about 1.0],
[AC_DEFINE(COMPATIBILITY)])

dnl -- --disable-debug
AC_ARG_ENABLE(debug,
[  --enable-debug          put useless debug informations in the binary],
[case "${enableval}" in
  yes) debug=true ;;
  no)  debug=false ;;
  *) AC_MSG_ERROR(bad value ${enableval} for --enable-debug) ;;
esac],[debug=true])
AM_CONDITIONAL(DEBUG_GABY, test x$debug = xtrue)

if "$debug" = "true" ; then
	AC_DEFINE(DEBUG_GABY)
fi

dnl -- --enable-postgresql (disabled in the 2.0 serie)
#echo -n "checking if you want to use PostgreSQL... "
#AC_ARG_ENABLE(postgresql,
#[  --enable-postgresql]     "EXPERIMENTAL!!!",
#[case "${enableval}" in
#  yes) postgresql=true ;;
#  no)  postgresql=false ;;
#  *) AC_MSG_ERROR(bad value ${enableval} for --enable-postgresql) ;;
#esac],[postgresql=false])
#
#if test "$postgresql" = "true"; then
#	CFLAGS="$CFLAGS -I/usr/include/postgresql/"
#	PQLIB="-lpq"
#	AC_SUBST(PQLIB)
#	AC_DEFINE(USE_POSTGRESQL)
#	echo "yes (bad idea...)"
#else
#	echo "no"
#fi
AC_SUBST(PQLIB)

dnl -- --enable-miguel-wishes (I should find a better name for this :) )
AC_ARG_ENABLE(miguel-wishes,
[  --enable-miguel-wishes  (..)],
[case "${enableval}" in
	yes) miguel_wishes=true;;
	no)  miguel_wishes=false;;
esac], [miguel_wishes=false])

AC_ARG_ENABLE(builder,
[  --enable-builder        enable gaby database builder (default)],
[case "${enableval}" in
	yes) builder=true ;;
	no)  builder=false ;;
esac], [builder=true])

dnl -- --disable-python
AC_ARG_ENABLE(python,
[  --enable-python         build the Python interpreter (default)],
[case "${enableval}" in
	yes) ;;
	no)  python_val=false;;
esac])

AC_ARG_ENABLE(gnome,
[  --enable-gnome          build the Gnome version of gaby (default)],
[case "${enableval}" in
	yes) gnome=true ;;
	no)  gnome=false ;;
esac], [gnome=true])

if test "$gui" = "false"; then
	gtk=false
	gnome=false
fi

AM_CONDITIONAL(MIGUEL_WISHES, $miguel_wishes)

if test "$python_val" = "false" && test "$miguel_wishes" = "true" ; then
	AC_MSG_WARN("you need Python if you want to follow Miguel wishes - I disabled them")
	miguel_wishes=false
fi

if test "$miguel_wishes" = "true"; then
	enable_static=yes
else
	enable_static=no
	if test "x$enable_shared" = "xno"; then
	  AC_MSG_ERROR(Gaby needs shared library support to build correctly.)
	fi
fi

if "$miguel_wishes" = "true" ; then
	AC_DEFINE(FOLLOW_MIGUEL)
fi

AM_CONDITIONAL(WITH_PYTHON, $python_val)

if "$gnome" = "true"; then
	PKG_CHECK_MODULES(GNOME, libgnomeui-2.0, libgnome=true, libgnome=false)
	AC_SUBST(GNOME_CFLAGS)
	AC_SUBST(GNOME_LIBS)

	if test "$libgnome" = "false"; then
		gnome=false
		cat << _EOF_ >> .hints
  
  You don't have Gnome developments files installed. This means Gaby will be
  built using plain GTK+ widgets which is not the best way to run Gaby.
  If you want to compile the Gnome version you should install them; those
  are included in a package often named gnome-devel (Red Hat, Mandrake, ...
  and in the Helix Gnome packages).
  On a Debian system you can simply run 'apt-get install libgnome-dev'
  
_EOF_
	else
		gnome=true
	fi
fi

if "$gnome" = "true" ; then
	AC_DEFINE(USE_GNOME)
elif "$gtk" = "true"; then
	GNOME_CFLAGS=
	GNOME_LIBS=
	GNOMEGNORBA_LIBS=
else
	AC_DEFINE(NO_GUI)
	GTK_LIBS=
	GNOME_CFLAGS=
	GNOME_LIBS=
fi

if "$python_val" = "true" ; then
	AC_DEFINE(USE_PYTHON)
fi

AC_SUBST(GNOME_CFLAGS)
AC_SUBST(GNOME_LIBS)

AM_CONDITIONAL(WITH_BUILDER, test x$builder = xtrue)
AM_CONDITIONAL(USE_GNOME, test x$gnome = xtrue)
AM_CONDITIONAL(WITHOUT_GUI, test x$gtk = xfalse)

if test -z "$libxml" || test -z "$python_val"; then
	cat << _EOF_ >> .hints
	
  Your system misses either Python or libxml that are both required if you
  want to print within Gaby - you should read the INSTALL file if you want
  to know where to get them.

_EOF_
fi

AC_OUTPUT(
Makefile
src/Makefile
src/tests/Makefile
builder/Makefile
plug-ins/Makefile
plug-ins/actions/Makefile
plug-ins/actions/backforward/Makefile
plug-ins/actions/cd/Makefile
plug-ins/actions/firsttime/Makefile
plug-ins/actions/gnomepim/Makefile
plug-ins/actions/net/Makefile
plug-ins/actions/scripts/Makefile
plug-ins/actions/stat/Makefile
plug-ins/formats/Makefile
plug-ins/formats/addressbook/Makefile
plug-ins/formats/appindex/Makefile
plug-ins/formats/csv/Makefile
plug-ins/formats/dbase/Makefile
plug-ins/formats/dpkg/Makefile
plug-ins/formats/gaby/Makefile
plug-ins/formats/gaby1/Makefile
plug-ins/formats/nosql/Makefile
plug-ins/formats/pilot/Makefile
plug-ins/formats/vcard/Makefile
plug-ins/formats/videobase/Makefile
plug-ins/interpreter/Makefile
plug-ins/interpreter/python/Makefile
plug-ins/print/Makefile
plug-ins/view/Makefile
plug-ins/view/addresscanvas/Makefile
plug-ins/view/canvas/Makefile
plug-ins/view/filter/Makefile
plug-ins/view/form/Makefile
plug-ins/view/genealogy/Makefile
plug-ins/view/gladeform/Makefile
plug-ins/view/gnomecard/Makefile
plug-ins/view/hello/Makefile
plug-ins/view/list/Makefile
plug-ins/view/miniform/Makefile
plug-ins/view/search/Makefile
plug-ins/view/xlist/Makefile
po/Makefile.in
data/Makefile
data/desc.gaby
doc/Makefile
doc/install-doc
doc/C/Makefile
doc/fr/Makefile
misc/Makefile
gaby.spec
)
dnl intl/Makefile

if "$miguel_wishes" = "true" ; then
	echo -n "Preparing files for --enable-miguel-wishes ..."
	cd src && python follow_miguel.py && cd ..
	echo " done."
else
	touch src/py_fm.h
	touch src/py_vplo.c
	touch src/py_gfbn.c
	touch src/py_lf.c
	touch src/py_lf2.c
	touch src/py_sf.c
	touch src/py_fm.h
	touch src/py_fake.c
fi

if [ "$hints" = "true" && test -f .hints ]
then
	cat << _EOF_

Here are some hints if you want a better configured Gaby:
=========================================================
_EOF_
	cat .hints
	read -t 5 2> /dev/null || sleep 2
fi

echo ""
echo -n "You may now type 'make' to "
if "$gnome" = "true" ; then
	echo -n "build the Gnome version of Gaby"
elif "$gtk" = "true"; then
	echo -n "build the plain-gtk version of Gaby"
else
	echo -n "build a non-GUI version of Gaby"
fi

if "$python_val" = "true" ; then
	echo " with Python support."
else
	echo " without Python support."
fi

cat << _EOF_

And don't forget to 'make install' after that.

Release notes (more news in NEWS, complete changelog in ChangeLog)
-------------

This release is a port of Gaby 2.0 to the Gnome 2.0 platform. It hasn't been
tested extensively (and since it depends on unstable libraries you won't even
be able to find a culprit. ah ah).

Since the new librairies use UTF-8 to encode characters you need to convert
your datafiles from whatever encoding they use (probably latin1) to UTF-8.
recode is a tool to do this and is probably available on your system.
Usage is 'recode latin1..utf-8 filenames'

Apart of those changes this release of Gaby is considered stable and won't
see future enhancements (except bug fixes).

But that doesn't mean Gaby development is stopped; a new unstable tree
has been created (gaby 2.1.* that will lead to gaby 2.2). More informations
about this will be posted on the website: http://www.theridion.com/gaby/

Enjoy!

Frederic Peters <fpeters@theridion.com>, Aprit 10th 2002

_EOF_

