/*  Gaby
 *  Copyright (C) 1998-2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#else
#  warning "no config.h !!!"
#  define PACKAGE "gaby"
#  define VERSION "unknown"
#endif /* HAVE_CONFIG_H */

#ifdef USE_POSTGRESQL
#  define USE_SQL
#  warning "Compiling SQL version -- highly unstable !!!"
#endif

#ifndef CODENAME
#  define CODENAME "The Magic Hour"
#endif

#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <time.h>

#ifdef HAVE_GETOPT_H
#  include <getopt.h>
#endif

#ifdef HAVE_REGEX_H
#  include <regex.h>
#endif

#ifdef HAVE_LOCALE_H
#  include <locale.h>
#endif

#ifdef NO_GUI
#  include <glib.h>
#else /* ! NO_GUI */
#  define GTK_ENABLE_BROKEN
#  define GTK_ENABLE_DEPRECATED
#  ifdef USE_GNOME 
#    include <gnome.h>
#  else
#    include <gtk/gtk.h>
#  endif
#  ifdef HAVE_IMLIB
#    include <gdk_imlib.h>
#  endif
#  ifdef HAVE_GDKPIXBUF
#    include <gdk-pixbuf/gdk-pixbuf.h>
#  endif
#  ifdef HAVE_ESD
#    include <esd.h>
#  endif
#endif /* ! NO_GUI */

#include <gmodule.h>

#ifdef HAVE_ESD
#  include <esd.h>
#endif

#ifndef NO_GUI
#  ifndef APP_DECLARED
#    ifdef USE_GNOME
extern GnomeApp *app;
#    else
extern GtkWidget *app;
#    endif
#  endif
#else
extern void *app;
#endif

#ifndef LIST_WINS_DECLARED
extern GList *list_windows;
#endif

#ifndef LISTS_DECLARED
extern GList *list_tables;
extern GList *list_subtables;
extern GList *list_views;
extern GList *list_actions;
extern GList *list_print_plugins;
#endif

#ifndef APPNAME_DECLARED
extern char *appname;
#endif

#ifndef LANGUAGE_DECLARED
extern char language[3];
#endif

/* this code snippet escaped from the autoconf documentation looking
 * for a better place :) */
#ifdef HAVE_DIRENT_H
#  include <dirent.h>
#  define NAMLEN(dirent) strlen((dirent)->d_name)
#else
#  define dirent direct
#  define NAMLEN(dirent) (dirent)->d_namlen
#  if HAVE_SYS_NDIR_H
#    include <sys/ndir.h>
#  endif
#  if HAVE_SYS_DIR_H
#    include <sys/dir.h>
#  endif
#  if HAVE_NDIR_H
#    include <ndir.h>
#  endif
#endif

#ifdef HAVE_GETTEXT
#  include <libintl.h>
#endif

#define gettext_noop(String) (String)
#ifndef N_
#  define N_(String) (String)
#endif
#ifndef _
#  define _(String) gettext (String)
#endif

#define INDEXES_ARE_ARRAYS

#include "struct.h"

#include "gabyerror.h"

#ifndef GABY_ERRNO_DECLARED
	/* those 2 ones are for plug-ins, read errors.h and do_action() in
	 * menu.c if you want to know more about them */
extern int   gaby_errno;
extern char* gaby_message;
#endif

#ifndef DEBUG_MODE_DECLARED
extern int debug_mode;
#endif

/* this is for debugging, if you really don't want any useless data you should
 * #define debug_print(format, args...)		;
 */
#ifdef DEBUG_GABY
#  ifdef __GNUC__
#    define debug_print(format, args...) if (debug_mode) \
						fprintf(stderr, format, ##args)
#  else
#    define debug_print			 printf
#  endif
#else /* ! DEBUG_GABY */
#  ifdef __GNUC__
#    define debug_print(format, args...)	;
#  else
	/* it should be possible to do sth better... */
#    define debug_print			printf
#  endif
#endif

#ifdef FOLLOW_MIGUEL
/* this is for plug-ins */
#define mstatic
#include "py_fm.h"
#else
#define mstatic static
#endif

