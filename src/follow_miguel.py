
# you may ask yourself questions about the name of this file.
# Here is the answer:

# Date: Sun, 30 May 1999 11:13:03 -0500
# From: Miguel de Icaza <miguel@nuclecu.unam.mx>
# Subject: Re: GABY and GNOME
# To: fpeters@multimania.com
#
# (...)
# Btw, I would like to see the code not use so many shared libraries by
# default, they are failing in misterious ways on my Linux machines, and
# it is very hard to investigate the problems.
# (...)
#

import os
import string

# I used to listdir() but that gives _every_ plug-ins while I only want
# those we'll have. Hence this ugly thing :(
pipe = os.popen('cd ../plug-ins/view/ && make showsubdirs')
ps = pipe.readlines()
pipe.close()
for a in ps:
	if ( a[0] == '#'):
		plugins = string.split(a[2:-1], ' ')

real_plugins = []

fm = open('py_fm.h', 'w')

fm.write( '#ifdef NO_GUI\n#define GtkWidget int\n#endif\n\n')

fake = open('py_fake.c', 'w')

def print_fake_line(fake, fkline):
	if ( len(fkline) == 0 ):
		return
	if ( fkline[0:4] == 'void' ):
		fake.write ( fkline[:-2] + ' {}\n' )
	else:
		fake.write ( fkline[:-2] + ' { return NULL; }\n' )

# we get the declarations

for p in plugins:
	filename = '../plug-ins/view/' + p + '/' + p + '.c'
	try:
		f = open(filename)
	except IOError:
		continue
	fm.write('/* ' + filename + ' */\n\n')
	line = f.readline()
	while ( len(line) > 0 and line[0:8] != 'mstatic ' ):
		line = f.readline()
	if ( len(line) <= 0 ):
		f.close()
		continue
	fkline = ''
	while ( line[0:8] == 'mstatic ' or line[0] == '\t' ):
		if ( line[0:8] == 'mstatic '):
			print_fake_line(fake, fkline)
			fm.write ( 'extern ' + line[8:] )
			fkline = line[8:]
		else:
			fm.write ( line )
			fkline = fkline + line
		line = f.readline()
	print_fake_line(fake, fkline)
	fake.write('\n')
	fm.write('\n\n')
	real_plugins.append(p)
	f.close()

vplo = open('py_vplo.c', 'w')

# we start the function
vplo.write('static ViewPluginData* viewplugin_load_one(GString *name)\n')
vplo.write('{\n')
vplo.write('\tViewPluginData *vpd = g_new0(ViewPluginData,1);\n')

# and we fill it
for p in real_plugins:
	filename = '../plug-ins/view/' + p + '/' + p + '.c'
	try:
		f = open(filename)
	except IOError:
		continue
	
	vplo.write( '\n\t/* the \'' + p + '\' view plug-in */\n' )
	line = f.readline()
	while ( len(line) > 0 and \
			line != 'int init_view_plugin (ViewPluginData *vpd)\n'):
		line = f.readline()
	if ( len(line) <= 0 ):
		f.close()
		continue
	line = f.readline()
	vplo.write('\tif ( strcmp(name->str, "' + p + '") == 0 ) {\n')
	while ( line != '}\n'):
		line = f.readline()
		if ( line[1:7] == 'return' ):
			vplo.write('\t\treturn vpd;\n')
		else:
			if ( line[0] == '#' ):
				vplo.write(line)
			else:
				vplo.write('\t' + line)
	f.close()

vplo.write('\treturn NULL;\n}\n\n')
vplo.close()

# we tell make what are the files we want to link to
f = open('Makefile.vp', 'w')
for p in real_plugins:
	f.write( '../plug-ins/view/' + p + '/.libs/lib' + p + '.a ')
f.close()

fm.write('/* The format plug-ins */\n\n')

plugins = os.listdir('../plug-ins/formats/')

lf = open('py_lf.c', 'w')
lf2 = open('py_lf2.c', 'w')
sf = open('py_sf.c', 'w')

mk = open('Makefile.fp', 'w')
for p in plugins:
	filename = '../plug-ins/formats/' + p + '/' + p + '.c'
	try:
		f = open(filename)
	except IOError:
		continue
	lookedfor = 'gboolean ' + p + '_load_file (struct location *loc)\n'
	line = f.readline()
	while ( len(line) > 0 ):
		if ( line == lookedfor ):
			mk.write( '../plug-ins/formats/' + p + \
						'/.libs/lib' + p + '.a ')

			fm.write ( 'extern ' + lookedfor[:-1] + ';\n' )
			fm.write ( 'extern gboolean ' + p + \
				'_save_file (struct location *loc);\n\n')
				
			lf.write ( 'if (strcmp(loc->type, "'+p+'") == 0 ) {\n')
			lf.write ( '\t' + p + '_load_file(loc);\n');
			lf.write ( '}\n' )
			
			lf2.write ( 'if (strcmp(loc->type, "'+p+'") == 0 ) {\n')
			lf2.write ( '\t' + p + '_load_file(loc);\n');
			lf2.write ( '}\n' )
			
			sf.write ( 'if (strcmp(loc->type, "'+p+'") == 0 ) {\n')
			sf.write ( '\t' + p + '_save_file(loc);\n');
			sf.write ( '\tcontinue;\n}\n' )
			break
		line = f.readline()
	f.close()

mk.close()

lf.close()
lf2.close()
sf.close()

pipe = os.popen('cd ../plug-ins/actions/ && make showsubdirs')
ps = pipe.readlines()
pipe.close()
for a in ps:
	if ( a[0] == '#'):
		plugins = string.split(a[2:-1], ' ')

gfbn = open('py_gfbn.c', 'w')
gfbn.write('static void get_function_by_name(gchar *name, action *a)\n{\n')

mk = open('Makefile.ap', 'w')
for p in plugins:
	filename = '../plug-ins/actions/' + p + '/' + p + '.c'
	try:
		f = open(filename)
	except IOError:
		continue
	line = f.readline()
	while ( len(line) > 0 and line[0:9] != '/* action' ):
		line = f.readline()
	f.close()
	if ( len(line) <= 0 ):
		continue
	
	mk.write( '../plug-ins/actions/' + p + '/.libs/lib' + p + '.a ')

	line = string.split(line, ':')[1]
	line = line[1:-4]
	fcts = string.split(line, ' ')
	for f in fcts:
		fm.write('extern void ' + p + '_' + f + \
				' (struct action_param *, int*);\n')
		gfbn.write('\tif ( strcmp(name, "' + f + '") == 0 && \\\n' + \
				'\t\t\t\tstrcmp(a->what.plugin->name, "' + \
				p + '") == 0 ) {\n')
		gfbn.write('\t\ta->function = ' + p + '_' + f + ';\n')
		gfbn.write('\t\treturn;\n\t}\n')
		fake.write('void ' + p + '_' + f + \
				' (struct action_param *p, int *i) { ; }\n')

mk.close()
gfbn.write('}\n')
gfbn.close()

pipe = os.popen('cd ../plug-ins/interpreter/ && make showsubdirs')
ps = pipe.readlines()
pipe.close()
for a in ps:
	if ( a[0] == '#'):
		plugins = string.split(a[2:-1], ' ')


# I don't know why .libs/libpython.a doesn't work while python.o does
mk = open('Makefile.ip', 'w')
for p in plugins:
	mk.write( '../plug-ins/interpreter/' + p + '/' + p + '.o ')
mk.close()

fm.close()

fake.close()

