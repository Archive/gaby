/*  Gaby
 *  Copyright (C) 1998-2002 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gaby.h"
#include "records.h"
#include "windows.h"
#include "f_desc.h"
#include "f_config.h"
#include "gtk_dialogs.h"
#include "actions.h"
#include "print_plg.h"

static GtkWidget* build_gtk_print_dialog(subtable *st, GList *pages,
					 GList *names);
static GtkWidget* page_misc(GList *wids);
static GtkWidget* page_select_order(subtable *st);
static GtkWidget* page_select_fields(subtable *st, char *checkbox,int selected);
static GtkWidget* page_select_records(subtable *st);

#if 0
static gchar* print_plugin_ok(char *filename, char *name)
{
	PrintPluginData *az = g_new0(PrintPluginData, 1);
	char *res = NULL;
	char dirname[PATH_MAX];
	char n[PATH_MAX];
	FILE *f;

	az->handle = g_module_open(filename, 0);
	if ( az->handle == NULL )
		return NULL;

	g_module_symbol(az->handle, "init_print_plugin", 
			(gpointer *)&az->init_print_plugin);
	if ( az->init_print_plugin == NULL )
		return NULL;
	
	if ( az->init_print_plugin(az) ) {
		sprintf(dirname, "%s/print/infos", PLUGINS_DIR);
		f = fopen(dirname, "r");
		strcpy(n, strrchr(filename, '/')+1+3);
		strchr(n, '.')[0] = 0;
		strcpy(dirname, n);
#ifdef DEBUG_GABY
		debug_print("[print_plugin_ok] n : %s\n", n);
#endif
		res = get_plugin_real_name(1, f, dirname);
		/* i18n_name + ' ' + name */
		strcpy(dirname+strlen(dirname)+1, n );
		dirname[strlen(dirname)] = ' ';
		fclose(f);
		return g_strdup(dirname);
	} else {
		g_module_close(az->handle);
		return NULL;
	}

}

GList* print_plugin_get_list(gchar *name)
{
	GList *list = NULL;
	char dirname[PATH_MAX];
	char filename[PATH_MAX];
	DIR *dir;
	struct dirent *ent;
	char *plugin_name;
	
	/* 
	 * why not allow the user to have the possibility to store plug-ins
	 * in his/her home directory (~/.gaby/plug-ins/...), this could be
	 * useful for non-root user who'd like to create one when root
	 * installed Gaby in a read-only directory
	 */
	sprintf(dirname, "%s/print", PLUGINS_DIR );
	
	dir = opendir(dirname);
	if ( dir == NULL ) { /* uh oh*/
		sprintf(filename, _("You don't have %s."), dirname);
		gaby_errno = CUSTOM_WARNING;
		gaby_perror_in_a_box();
		return NULL;
	}
	
	while ( 1 ) {
		ent = readdir(dir);
		if ( ent == NULL ) break;
		if ( strncmp(ent->d_name, "lib", 3) == 0 && 
			strncmp( ent->d_name + strlen(ent->d_name) - 3, 
				".so", 3) == 0 ) {
			sprintf(filename, "%s/%s", dirname, ent->d_name);
			debug_print("[ppgl] Is %s ok ?\n", filename);
			plugin_name = print_plugin_ok(filename, name);
			if ( plugin_name != NULL ) {
				list = g_list_append(list, plugin_name);
			}
		}
	}

	closedir(dir);

	return list;
}

#endif /* 0 */

#if 0 /* old print plug-in architecture */
static void print_real(GtkWidget *but, GtkWidget *dialog)
{
	GtkCList *list = gtk_object_get_data(GTK_OBJECT(dialog), "list");
	GList *subtables = list_subtables;
	PrintPluginData *ppd = gtk_object_get_data(GTK_OBJECT(dialog), "ppd");
	int row;
	gchar *text;
	subtable *st;
	char filename[PATH_MAX];
	char message[1000];
	
#ifdef DEBUG_GABY
	debug_print("[print_real] ppd : %p (%s)\n", ppd, ppd->name);
#endif
	if ( list->selection == NULL ) {
		gaby_message = g_strdup(_("You have to select a subtable."));
		gaby_errno = CUSTOM_ERROR;
		gaby_perror_in_a_box();
		return;
	}

	row = GPOINTER_TO_INT(list->selection->data);
	gtk_clist_get_text(list, row, 0, &text);

	st = get_subtable_pointer(subtables, text);

	if ( st == NULL ) /* this shouldn't happen */
		return;

#ifndef FOLLOW_MIGUEL
	if ( ppd->loaded == 0 ) {
#if 0
		sprintf(filename, "%s/print/lib%s.so", PLUGINS_DIR, ppd->name);
#else
		sprintf(filename, PLUGINS_DIR "/print/lib%s.so", ppd->name);
#endif
		ppd->handle = g_module_open(filename, 0);
		if ( ppd->handle == NULL ) {
			gaby_errno = LOAD_PLUGIN_ERROR;
			gaby_message = g_strdup(filename);
			gaby_perror_in_a_box();
			return;
		}
		ppd->loaded++;
	}

	g_module_symbol(ppd->handle, "init_print_plugin",
				(gpointer*)&ppd->init_print_plugin );
	if ( ppd->init_print_plugin == NULL ) {
		char fct_name[1000];
		sprintf(fct_name, "%s_init_print_plugin", ppd->name);
		g_module_symbol(ppd->handle, fct_name,
				(gpointer*)&ppd->init_print_plugin );
		if ( ppd->init_print_plugin == NULL ) {
			g_error("aie aie aie in print plug-in loading, "
				"please warn fpeters@swing.be\n");
			close_dialog(but, dialog);
			return;
		}
	}
	ppd->init_print_plugin(ppd);
#endif
	
	if ( get_config_bool("common", "print", "fast", FALSE) ) {
		sprintf(filename, "%s_output", appname);
		ppd->print_fast(st, filename, NULL, &ppd->loaded);
		sprintf(message, _("Output is now in %s."), filename);
		gaby_message = g_strdup(message);
		gaby_errno = CUSTOM_MESSAGE;
		gaby_perror_in_a_box();
	} else {
		ppd->print(st, NULL, NULL, &ppd->loaded);
	}

	close_dialog(but, dialog);
	
}

#endif

void print_plugin_open(PrintPluginData *ppd, subtable *st)
{
	GtkWidget *dlg;
	gchar filename[PATH_MAX];
	GList *pages = NULL;
	GList *names = NULL;

	debug_print("[print_real] printing st:%s with plug-in:%s\n",
			st->name, ppd->name);

	sprintf(filename, PLUGINS_DIR "/print/%s.xml", ppd->name);
#ifdef HAVE_LIBXML
	if ( print_load_xml(filename, &pages, &names) != 0) {
		debug_print("[print_real] xml failed to load!\n");
		gaby_errno = CUSTOM_ERROR;
		gaby_message = g_strdup(_("XML file failed to load\nImpossible to create user interface\nfor this plug-in"));
		gaby_perror_in_a_box();
		return;
	}
#endif

#if 0 /* useful to test xml loading */
	pages = g_list_first(pages);
	while ( pages != NULL ) {
		GabyPage *p = pages->data;
		pages = g_list_next(pages);
		debug_print("Page: label: %s, type: %d\n", p->label, p->type);
		switch ( p->type ) {
			case FIELDS:
			{
				GabyPageFields *pp = (GabyPageFields*)p;
				debug_print("\toptional: %d\n", pp->optional);
				debug_print("\tselection: %d\n", pp->selection);
			} break;
			case MISC:
			{
				GabyPageMisc *pp = (GabyPageMisc*)p;
				GList *tmp = pp->widgets;
				while ( tmp ) {
					GabyWidget *w = tmp->data;
					tmp = g_list_next(tmp);
					debug_print("\twidget: %d %s\n",
							w->type, w->label);
				}
			} break;
			default:
		}
	}

	if ( names ) names = g_list_first(names);
	while ( names != NULL ) {
		struct ref *r = names->data;
		debug_print("key: %s, type: %d, val: %p\n", r->key, r->type, r->pointer);
		names = g_list_next(names);
	}
	return;
#endif

	dlg = build_gtk_print_dialog(st, pages, names);
	gtk_object_set_data(GTK_OBJECT(dlg), "pp_name", ppd->name);
	gtk_object_set_data(GTK_OBJECT(dlg), "pages", pages);
	gtk_object_set_data(GTK_OBJECT(dlg), "names", names);
	gtk_object_set_data(GTK_OBJECT(dlg), "subtable", st);
	
	gtk_widget_show(dlg);
}

static void close_dialog(GtkWidget *but, GtkWidget *dialog)
{
#ifdef USE_GNOME
	gnome_dialog_close(GNOME_DIALOG(dialog));
#else
	gtk_widget_destroy(dialog);
#endif
}

static void print_real(GtkWidget *but, GtkWidget *dialog)
{
	GtkCList *list = gtk_object_get_data(GTK_OBJECT(dialog), "list");
	PrintPluginData *ppd = gtk_object_get_data(GTK_OBJECT(dialog), "ppd");
	int row;
	gchar *text;
	subtable *st;
	
	debug_print("[print_real] ppd : %p (%s)\n", ppd, ppd->name);
	if ( list->selection == NULL ) {
		gaby_message = g_strdup(_("You have to select a subtable."));
		gaby_errno = CUSTOM_ERROR;
		gaby_perror_in_a_box();
		return;
	}

	row = GPOINTER_TO_INT(list->selection->data);
	gtk_clist_get_text(list, row, 0, &text);

	st = get_subtable_by_name(text);

	if ( st == NULL ) /* this shouldn't happen */
		return;

	close_dialog(but, dialog);

	print_plugin_open(ppd, st);

}

static void real_print_ok_callback(GtkWidget *but, GtkWidget *dialog)
{
	char *pp_name = gtk_object_get_data(GTK_OBJECT(dialog), "pp_name");
	GList *names = gtk_object_get_data(GTK_OBJECT(dialog), "names");
	subtable *st = gtk_object_get_data(GTK_OBJECT(dialog), "subtable");
	GModule *python;
	int (*print_plugin_fct) (char*, subtable*,GList*, gboolean);

	python = get_interpreter("python", NULL);
	if ( python == NULL ) {
		gaby_errno = CUSTOM_ERROR;
		gaby_message = g_strdup(_("Unable to load Python plug-in\n"
					  "(required in order to print)."));
		gaby_perror_in_a_box();
		return;
	}

	g_module_symbol(python, "print_plugin_fct", 
					(gpointer*)&print_plugin_fct);
	if ( print_plugin_fct(pp_name, st, names, FALSE) != 0 )
		return;
	
#ifdef USE_GNOME
	gnome_dialog_close(GNOME_DIALOG(dialog));
#else
	gtk_widget_destroy(dialog);
#endif
}


static GtkWidget* build_gtk_print_dialog(subtable *st, GList *pages,
					 GList *names)
{
	GtkWidget *dialog;
	GtkWidget *vbox;
#ifndef USE_GNOME
	GtkWidget *button;
#endif
	GtkWidget *notebook;

#ifdef USE_GNOME
	dialog = gnome_dialog_new(_("Gaby Print Dialog Box"), 
				GNOME_STOCK_BUTTON_OK, 
				GNOME_STOCK_BUTTON_CANCEL, 
				NULL );
	vbox = GNOME_DIALOG(dialog)->vbox;
	
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, 
			GTK_SIGNAL_FUNC(real_print_ok_callback), dialog);
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, 
				GTK_SIGNAL_FUNC(close_dialog), dialog);
#else /* ! USE_GNOME */
	dialog = gtk_dialog_new();
	gtk_window_set_title(GTK_WINDOW(dialog), _("Gaby Print Dialog Box"));
	vbox = GTK_DIALOG(dialog)->vbox;

	gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);

	button = gtk_button_new_with_label(_("OK"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), 
					button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
			GTK_SIGNAL_FUNC(real_print_ok_callback), dialog);

	button = gtk_button_new_with_label(_("Cancel"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), 
					button, TRUE, TRUE, 0);
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
			GTK_SIGNAL_FUNC(close_dialog), dialog);
	
#endif
	notebook = gtk_notebook_new();
	gtk_widget_show(notebook);

	while ( pages != NULL ) {
		GabyPage *p = pages->data;
		GtkWidget *wid;
		switch ( p->type ) {
			case FIELDS:
			{
				GabyPageFields *pp = (GabyPageFields*)p;
				wid = page_select_fields(st, 
					pp->optional ? "Enable" : NULL,
					pp->selection);
			} break;
			case RECORDS:
			{
				wid = page_select_records(st);
			} break;
			case MISC:
			{
				GabyPageMisc *pp = (GabyPageMisc*)p;
				wid = page_misc(pp->widgets);
			} break;
			case ORDER:
			{
				wid = page_select_order(st);
			} break;
			default:
			{
				wid = gtk_label_new("Unknown window");
				gtk_widget_show(wid);
			} break;
		}
		p->real_widget = wid;
		gtk_notebook_append_page(GTK_NOTEBOOK(notebook), wid, 
						gtk_label_new(_(p->label)));
		
		pages = g_list_next(pages);
	}

	gtk_widget_show(notebook);
	gtk_box_pack_start(GTK_BOX(vbox), notebook, TRUE, TRUE, 0);
	
	gtk_object_set_data(GTK_OBJECT(dialog), "notebook", notebook);

	gtk_object_set_data(GTK_OBJECT(dialog), "st", st);

	return dialog;
}

static void row_changed( GtkCList *clist, gint row, gint column,
			 GdkEventButton *event, GtkWidget *wid)
{
#ifdef USE_GNOME
	gnome_dialog_set_sensitive(GNOME_DIALOG(wid), 0, 
					( clist->selection != NULL ) );
#else
	gtk_widget_set_sensitive(wid, ( clist->selection != NULL ) );
#endif
}

GtkWidget* print_dialog(PrintPluginData *ppd)
{
	GtkWidget *dialog = NULL;
	GtkWidget *vbox, *list, *label;
#ifndef USE_GNOME
	GtkWidget *buttonok;
	GtkWidget *button;
#endif
	GList *tmp;
	char title[100];
	
/*
	sprintf(plugin_file, "%s/print/lib%s.so", PLUGINS_DIR, ppd->name );
#ifdef DEBUG_GABY
	debug_print("[print_dialog] plugin : %s\n", plugin_file);
#endif
*/
	debug_print("[print_dialog] ppd : %p (%s)\n", ppd, ppd->name);

#if 0	/* 20000319: more than one print dialog is possible */
	if ( GTK_IS_WINDOW(dialog) == TRUE ) {
		gdk_window_raise((GdkWindow*)dialog->window);
		return dialog;
	}
#endif
	
	sprintf(title, "%s %s", _("Printing to"), ppd->i18n_name);
		
#ifdef USE_GNOME
	dialog = gnome_dialog_new(title, 
			GNOME_STOCK_BUTTON_OK, 
			GNOME_STOCK_BUTTON_CANCEL, 
			NULL);
	
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, 
				GTK_SIGNAL_FUNC(print_real), dialog );
	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 1, 
				GTK_SIGNAL_FUNC(close_dialog), dialog );
	
	vbox = GNOME_DIALOG(dialog)->vbox;
	
	gnome_dialog_close_hides(GNOME_DIALOG(dialog), FALSE);
	
#else /* ! USE_GNOME */
	dialog = gtk_dialog_new();
	gtk_window_set_title(GTK_WINDOW(dialog), title);
	vbox = GTK_DIALOG(dialog)->vbox;

	gtk_container_set_border_width(GTK_CONTAINER(vbox), 5 );
	
	button = gtk_button_new_with_label(_("OK"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), 
					button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
				GTK_SIGNAL_FUNC(print_real), dialog );
	buttonok = button;
	
	button = gtk_button_new_with_label(_("Cancel"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), 
					button, TRUE, TRUE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
				GTK_SIGNAL_FUNC(close_dialog), dialog );
#endif /* ! USE_GNOME */
	
	label = gtk_label_new(_("Select the subtable you want to print"));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 2 );
			
	debug_print("[print_dialog] ppd : %p (%s)\n", ppd, ppd->name);
	list = gtk_clist_new(1);
	tmp = g_list_first(list_subtables);
	while ( tmp != NULL ) {
		gtk_clist_append(GTK_CLIST(list), 
				&((subtable*)tmp->data)->i18n_name );
		tmp = g_list_next(tmp);
	}
	gtk_widget_show(list);

#ifdef USE_GNOME
	gtk_signal_connect(GTK_OBJECT(list), "select_row", 
						row_changed, dialog );
	gtk_signal_connect(GTK_OBJECT(list), "unselect_row", 
						row_changed, dialog );
	row_changed(GTK_CLIST(list), 0, 0, NULL, dialog);
#else
	gtk_signal_connect(GTK_OBJECT(list), "select_row", 
						row_changed, buttonok );
	gtk_signal_connect(GTK_OBJECT(list), "unselect_row", 
						row_changed, buttonok );
	row_changed(GTK_CLIST(list), 0, 0, NULL, buttonok);
#endif
	
	gtk_box_pack_start(GTK_BOX(vbox), list, TRUE, TRUE, 2 );
	
	debug_print("[print_dialog] ppd : %p (%s)\n", ppd, ppd->name);
	gtk_object_set_data(GTK_OBJECT(dialog), "ppd", ppd);
	gtk_object_set_data(GTK_OBJECT(dialog), "list", list);
		
	return dialog;
}

/* --- helper functions for plug-ins */

static void item_up(GtkWidget *button, GtkWidget *clist)
{
	if ( GTK_CLIST(clist)->focus_row == 0 )
		return;
	gtk_clist_swap_rows(GTK_CLIST(clist), GTK_CLIST(clist)->focus_row, 
			GTK_CLIST(clist)->focus_row-1);
	/* the focused row is set correctly in this case */
}

static void item_down(GtkWidget *button, GtkWidget *clist)
{
	if ( GTK_CLIST(clist)->focus_row == GTK_CLIST(clist)->rows-1 )
		return;
	gtk_clist_swap_rows(GTK_CLIST(clist), GTK_CLIST(clist)->focus_row, 
			GTK_CLIST(clist)->focus_row+1);
	/* the focused row is not set correctly in this case */
	GTK_CLIST(clist)->focus_row = GTK_CLIST(clist)->focus_row+1;
}

static void printcb_toggled(GtkToggleButton *cb, GtkWidget *hbox)
{
	gtk_widget_set_sensitive(hbox, cb->active);
}

/**
 * page_select_fields
 * @st: subtable
 * @checkbox: text of the enable checkbox (NULL if no checkbox)
 * @selected: initially selected fields (see examples for values)
 *
 * Description:
 * this creates a standard field selection page to use in print plug-ins
 *
 * Returns: the widget to put in the page
 **/
static GtkWidget* page_select_fields(subtable *st, char *checkbox, int selected)
{
	GtkWidget *vbox, *cb, *hbox;
	GtkWidget *clist, *buttonbox, *button;
	int i;

	vbox = gtk_vbox_new(FALSE, 0); /* the whole box */
	hbox = gtk_hbox_new(FALSE, 0); /* the box with the list and up/down */
	
	if ( checkbox ) {
		cb = gtk_check_button_new_with_label(checkbox);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb), TRUE);
		gtk_signal_connect(GTK_OBJECT(cb), "toggled", printcb_toggled,
				hbox );
		gtk_widget_show(cb);
		gtk_box_pack_start(GTK_BOX(vbox), cb, FALSE, FALSE, 2 );
		gtk_object_set_data(GTK_OBJECT(vbox), "cb", cb);
	}

	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2 );
	
	clist = gtk_clist_new(1);
	gtk_object_set_data(GTK_OBJECT(vbox), "list", clist);
	gtk_widget_show(clist);
	gtk_clist_set_selection_mode(GTK_CLIST(clist), GTK_SELECTION_MULTIPLE);
	for ( i=0; i<st->nb_fields; i++) {
		if ( st->fields[i].type == T_RECORDS ) continue;
		if ( st->fields[i].type == T_MULTIMEDIA ) continue;
		gtk_clist_append(GTK_CLIST(clist), &st->fields[i].i18n_name);
	}

	if ( selected == G_MAXINT ) {
		gtk_clist_select_all(GTK_CLIST(clist));
	} else {
		if ( selected > 0 ) {
			for ( i=0; i<selected; i++ ) {
				gtk_clist_select_row(GTK_CLIST(clist), i, 0);
			}
		}
		if ( selected < 0 ) {
			for ( i=0; i< -selected; i++) {
				gtk_clist_select_row(GTK_CLIST(clist),
							st->nb_fields-1-i, 0);
			}
		}
	}

	gtk_box_pack_start(GTK_BOX(hbox), clist, TRUE, TRUE, 0);

	buttonbox = gtk_vbutton_box_new();
	gtk_widget_show(buttonbox);
	gtk_button_box_set_layout(GTK_BUTTON_BOX(buttonbox), 
			GTK_BUTTONBOX_SPREAD );
	gtk_box_pack_start(GTK_BOX(hbox), buttonbox, FALSE, FALSE, 5 );
	
	button = gtk_button_new_from_stock(GTK_STOCK_GO_UP);
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
					GTK_SIGNAL_FUNC(item_up), clist );
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(buttonbox), button, FALSE, FALSE, 0);
	
	button = gtk_button_new_from_stock(GTK_STOCK_GO_DOWN);
	gtk_signal_connect(GTK_OBJECT(button), "clicked", 
					GTK_SIGNAL_FUNC(item_down), clist );
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(buttonbox), button, FALSE, FALSE, 0);
	
	gtk_object_set_data(GTK_OBJECT(vbox), "clist", clist);
	gtk_widget_show(vbox);

	return vbox;
}

static int cmp_int(int *a, int *b)
{
	return ( *a > *b );
}

/**
 * page_select_fields_get_info
 * @st: the subtable being printed
 * @page: the page as returned by page_select_fields()
 * @active: will be FALSE is the 'disable' checkbutton is on
 * @selected: (must be nb_fields+1 long)
 *
 * Description:
 * this returns the info from a select_fields page
 **/
void page_select_fields_get_info(subtable *st, GtkWidget *page,
				 gboolean *active, int *selected)
{
	GtkCList *clist = gtk_object_get_data(GTK_OBJECT(page), "clist");
	GtkToggleButton *cb = gtk_object_get_data(GTK_OBJECT(page), "cb");
	GList *tmp;
	int i, nb_fields;
	char *text;

	if ( cb != NULL ) {
		*active = cb->active;
		if ( *active == FALSE ) {
			selected[0] = -1;
			debug_print("page not selected --> skipped\n");
			return;
		}
	}

	tmp = clist->selection;
	nb_fields = g_list_length(tmp);
	
	if ( nb_fields == 0 ) {
		*active = FALSE;
		selected[0] = -1;
		debug_print("no items selected --> skipped\n");
		return;
	}
	
	for (i=0; i<nb_fields; i++) {
		selected[i] = GPOINTER_TO_INT(tmp->data);
		tmp = g_list_next(tmp);
	}
	
	/* overkill ? :) */
	qsort(selected, nb_fields, sizeof(int), (int (*)())cmp_int);
	
#ifdef DEBUG_GABY
	for ( i=0; i<nb_fields; i++) {
		debug_print("[psfgi] field %d is %d\n", i, selected[i]);
	}
#endif

	for (i=0; i<nb_fields; i++) {
		gtk_clist_get_text(clist, selected[i], 0, &text);
		selected[i] = subtable_get_field_no(st, text);
	}
	selected[i] = -1;
}



static gchar* fill_windows_list(GtkWidget *list, subtable *st)
{
	/* duplicated from view/filter/filter.c */
	/* diffs with the other version:
	 *  - this one returns the name of the first windows
	 */
	GtkWidget *its_parent, *li;
	view *v;
	GList *tmp = g_list_first(list_windows);
	gchar *name, *ret_name = NULL;
	gabywindow *window;

	gtk_list_clear_items(GTK_LIST(list), 0, -1);

	while ( tmp != NULL ) {
		window = tmp->data;
		tmp = g_list_next(tmp);
		if ( ! GTK_IS_OBJECT(window->widget) ) continue;
		its_parent = window->parent;
		if ( GTK_IS_WINDOW(its_parent) ) {
			v = window->view;
			name = window->name;
			if ( v->subtable->table == st->table && 
					v->type->capabilities & FILTERABLE ) {
				if ( ret_name == NULL ) ret_name = name;
				li = gtk_list_item_new_with_label(name);
				gtk_widget_show(li);
				gtk_container_add(GTK_CONTAINER(list), li);
			}
		}
	}

	return ret_name;
}

static GtkWidget* page_select_records(subtable *st)
{
	GtkWidget *vbox;
	GtkWidget *vbox2, *rb, *combo;
	gchar *name;

	vbox = gtk_vbox_new(FALSE, 0);

	rb = gtk_radio_button_new_with_label(NULL, _("Every records"));
	gtk_widget_show(rb);
	gtk_box_pack_start(GTK_BOX(vbox), rb, FALSE, FALSE, 0);

	vbox2 = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox2);
	gtk_box_pack_start(GTK_BOX(vbox), vbox2, FALSE, FALSE, 0);
	rb = gtk_radio_button_new_with_label_from_widget(
			GTK_RADIO_BUTTON(rb), _("Records showed in: "));
	gtk_widget_show(rb);
	gtk_box_pack_start(GTK_BOX(vbox2), rb, FALSE, FALSE, 0);
	combo = gtk_combo_new();
	gtk_widget_show(combo);
	gtk_box_pack_start(GTK_BOX(vbox), combo, FALSE, FALSE, 0);
	name = fill_windows_list(GTK_COMBO(combo)->list, st);
	if ( name == NULL ) {
		gtk_widget_set_sensitive(vbox2, FALSE);
	} else {
		gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(combo)->entry), name);
	}

	gtk_object_set_data(GTK_OBJECT(vbox), "radio", rb);
	gtk_object_set_data(GTK_OBJECT(vbox), "cb", combo);
	
	gtk_widget_show(vbox);
	return vbox;
}

void page_select_records_get_info(subtable *st, GtkWidget *page,
				  gboolean *all, GList **recs)
{
	GtkToggleButton *radio = gtk_object_get_data(GTK_OBJECT(page), "radio");
	GtkWidget *cb = gtk_object_get_data(GTK_OBJECT(page), "cb");
	GList *what;
	gabywindow *affwin;
	condition *c;

	debug_print("[psrgi] radio->active: %d\n", radio->active);
	if ( radio->active == 0 ) { /* every records */
		*all = TRUE;
		return;
	}
	*all = FALSE;
	*recs = NULL;

	affwin = get_window_by_name( gtk_entry_get_text( 
				GTK_ENTRY(GTK_COMBO(cb)->entry)));
	if ( affwin == NULL ) return;
	
	if ( affwin->view->type->view_records ) {
		/* the view has special handling */
		affwin->view->type->view_records(affwin, recs);
	} else { /* using generic way */
		what = gtk_object_get_data(GTK_OBJECT(affwin->widget), "what");
		if ( what == NULL ) {
			c = gtk_object_get_data(GTK_OBJECT(affwin->widget),
					"condition");
			if ( c == NULL ) {
				*all = TRUE;
				return;
			}
			*recs = get_conditional_records_list(st, c);
		} else {
			*recs = what;
		}
	}

}


static GtkWidget* page_misc(GList *wids)
{
	GtkWidget *table;
	GtkWidget *entry;
	GtkWidget *label;
	int i=0;
	
	table = gtk_table_new(g_list_length(wids), 2, FALSE);

	while ( wids ) {
		GabyWidget *w = wids->data;

		switch ( w->type ) {
			case FILE_SELECTION:
			{
				label = gtk_label_new(_(w->label));
				gtk_widget_show(label);
				gtk_table_attach_defaults(GTK_TABLE(table),
						label, 0, 1, i, i+1);
#ifdef USE_GNOME
				entry = gnome_file_entry_new(NULL, _(w->label));
#else
				entry = gtk_entry_new();
#endif
				gtk_widget_show(entry);
				gtk_table_attach(GTK_TABLE(table), entry,
					1, 2, i, i+1,
					GTK_EXPAND | GTK_FILL | GTK_SHRINK,
					GTK_SHRINK, 0, 0);
#ifdef USE_GNOME
				w->real_widget = gnome_file_entry_gtk_entry(
						GNOME_FILE_ENTRY(entry));
#else
				w->real_widget = entry;
#endif
			} break;
			case LABEL:
			{
				label = gtk_label_new(_(w->label));
				gtk_widget_show(label);
				gtk_table_attach_defaults(GTK_TABLE(table),
							label, 0, 2, i, i+1);
			} break;
			case CHECKBOX:
			{
				GtkWidget *cb = gtk_check_button_new_with_label(
						_(w->label));
				gtk_widget_show(cb);
				w->real_widget = cb;
				gtk_table_attach_defaults(GTK_TABLE(table),
							cb, 0, 2, i, i+1);
			} break;
			case ENTRY:
			{
				label = gtk_label_new(_(w->label));
				gtk_widget_show(label);
				gtk_table_attach_defaults(GTK_TABLE(table),
						label, 0, 1, i, i+1);
				entry = gtk_entry_new();
				gtk_widget_show(entry);
				gtk_table_attach(GTK_TABLE(table), entry,
					1, 2, i, i+1,
					GTK_EXPAND | GTK_FILL | GTK_SHRINK,
					GTK_SHRINK, 0, 0);
				w->real_widget = entry;
			} break;
			default:
			{
				label = gtk_label_new("???");
				gtk_widget_show(label);
				gtk_table_attach_defaults(GTK_TABLE(table),
							label, 0, 2, i, i+1);
			} break;
		}
		
		wids = g_list_next(wids);
		i++;
	}

	gtk_widget_show(table);

	return table;
}


static GtkWidget* page_select_order(subtable *st)
{
	GtkWidget *vbox;
	GtkWidget *label;
	GtkWidget *combo;
	GtkWidget *list;
	GtkWidget *li;
	int i;
	
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox);

	label = gtk_label_new(_("Sort the records on: "));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
	
	combo = gtk_combo_new();
	gtk_widget_show(combo);
	gtk_box_pack_start(GTK_BOX(vbox), combo, FALSE, FALSE, 0);

	gtk_entry_set_editable( GTK_ENTRY(GTK_COMBO(combo)->entry), FALSE);
	
	list = GTK_COMBO(combo)->list;
	gtk_list_clear_items(GTK_LIST(list), 0, -1);

	li = gtk_list_item_new_with_label(_("Do not sort"));
	gtk_widget_show(li);
	gtk_container_add(GTK_CONTAINER(list), li);
	for ( i=0; i<st->nb_fields; i++) {
		li = gtk_list_item_new_with_label(st->fields[i].i18n_name);
		gtk_widget_show(li);
		gtk_container_add(GTK_CONTAINER(list), li);
	}
	
	gtk_object_set_data(GTK_OBJECT(vbox), "entry", GTK_COMBO(combo)->entry);

	return vbox;
}

void page_select_order_get_info(subtable *st, GtkWidget *page, int *field)
{
	GtkWidget *entry = gtk_object_get_data(GTK_OBJECT(page), "entry");
	gchar *text;
	int i;

	text = gtk_entry_get_text(GTK_ENTRY(entry));

	for ( i=0; i<st->nb_fields; i++ ) {
		if ( strcmp(text, st->fields[i].i18n_name) == 0 ) {
			*field = i;
			return;
		}
	}
	*field = -1;
}

