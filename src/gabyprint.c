/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*
 * gabyprint is a command line tool to print databases from gaby with plug-ins
 * from gaby
 */

#define APP_DECLARED
#define APPNAME_DECLARED
#define LISTS_DECLARED
#define ALL_WINS_DECLARED
#define LANGUAGE_DECLARED

#include "errors.h"
#include "gaby.h"
#include "files.h"
#include "tables.h"
#include "records.h"
#include "print_plg.h"
#include "actions.h"

GList *list_windows = NULL;
GtkWidget *app = NULL;
gchar *appname;
char language[3];

GList *list_tables = NULL;
GList *list_subtables = NULL;
GList *list_views = NULL;
GList *list_actions;

/* fake functions so that dynamic linking works even without everyfiles */
#ifdef FOLLOW_MIGUEL
#include "py_fake.c"
#endif
 
void gaby_perror_in_a_box()
{
	/*gaby_errno = NO_ERROR;*/
	if ( gaby_message ) g_free(gaby_message);
}

void gaby_property_box_changed(gpointer data)
{
	return;
}

static void parse_options(int argc, char *argv[], char *name, char *subtable,
			  char *format)
{
	int c;
	
#ifdef GETOPTLONG_AVAILABLE
	static struct option long_options[] = {
		{ "as"      , 1, 0, 'a' },
		{ "subtable", 1, 0, 's' },
		{ "format"  , 1, 0, 'f' },
		{ "version" , 0, 0, 'V' },
		{ "help"    , 0, 0, 'h' },
		{ 0         , 0, 0,  0  }
	};
#endif

	opterr = 0;

	while (1) {
#ifdef GETOPTLONG_AVAILABLE
		c = getopt_long(argc, argv, "a:s:f:Vh", long_options, NULL);
#else
		c = getopt(argc, argv, "a:s:f:Vh");
#endif
		if ( c == -1 ) break;
		switch ( c ) {
			case 'a':
			{
				strcpy(name, optarg);
			} break;
			case 's':
			{
				strcpy(subtable, optarg);
			} break;
			case 'f':
			{
				strcpy(format, optarg);
			} break;
			case 'V':
			{
				g_print(_("gabyprint %s\n"), VERSION);
				g_print(_("Copyright (c) 1999 Frederic Peters\n"));
				exit(0);
			} break;
			case 'h':
			{
				g_print(_("Usage: gabyprint --as .. --subtable .. --format ..\n"));
				exit(0);
			} break;
		}
	}
	
}

#if 0
static void print_real(PrintPluginData *ppd, subtable *st, FILE *f)
{
#ifndef FOLLOW_MIGUEL
	char filename[PATH_MAX];
	
	sprintf(filename, "%s/print/lib%s.so", PLUGINS_DIR, ppd->name);
	ppd->handle = g_module_open(filename, 0);
	if ( ppd->handle == NULL )
		return;

	sprintf(filename, "%s_init_print_plugin", ppd->name);
	g_module_symbol(ppd->handle, filename, 
			(gpointer*)&ppd->init_print_plugin );
	if ( ppd->init_print_plugin == NULL ) {
		g_module_symbol(ppd->handle, "init_print_plugin", 
					(gpointer*)&ppd->init_print_plugin );
		if ( ppd->init_print_plugin == NULL ) {
			gaby_errno = CUSTOM_ERROR;
			gaby_message = g_strdup( 
				_("Unable to print in this format !\n"));
			gaby_perror_in_a_box();
			return;
		}
	}
	ppd->init_print_plugin(ppd);
#endif	

	if ( ppd->print_fast == NULL )
		return;

	ppd->print_fast(st, NULL, f, &ppd->loaded);
}
#endif

#ifdef HAVE_LIBXML
static void print_real(PrintPluginData *ppd, subtable *st, FILE *f)
{
	gchar filename[PATH_MAX];
	GList *pages = NULL;
	GList *names = NULL;
	GModule *python;
	int (*print_plugin_fct) (char*, subtable*, GList*, gboolean);

	debug_print("[print_real] printing st:%s with plug-in:%s\n",
			st->name, ppd->name);

	sprintf(filename, PLUGINS_DIR "/print/%s.xml", ppd->name);
	if ( print_load_xml(filename, &pages, &names) != 0)
		return;

	python = get_interpreter("python", NULL);
	g_module_symbol(python, "print_plugin_fct", 
					(gpointer*)&print_plugin_fct);
	if ( print_plugin_fct(ppd->name, st, names, TRUE) != 0 )
		return;
}
#endif /* HAVE_LIBXML */

int main(int argc, char *argv[])
{
#ifdef HAVE_LIBXML
	subtable *st;
	int i;
	GList *print_plugins;
	char name[30], subtable[30], format[30];
	gboolean load_everything = FALSE;
	PrintPluginData *ppd;
	FILE *f = NULL;

	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
	textdomain (GETTEXT_PACKAGE);

	strcpy(language, "en");
	if ( getenv("LANGUAGE") || getenv("LANG") ) {
		char *t = g_strdup_printf("%s", getenv("LANGUAGE") ?
				getenv("LANGUAGE")
				: getenv("LANG")   );
		t[2] = 0;
		strcpy(language, t);
		g_free(t);
	}
/*	gtk_set_locale();*/

	name[0] = 0;
	subtable[0] = 0;
	format[0] = 0;
	parse_options(argc, argv, name, subtable, format);
	if ( strlen(name) == 0 || strlen(subtable) == 0 || strlen(format) == 0){
		g_print(_("Parameters missing (try --help)\n"));
		exit(-1);
	}

#if 0
	if ( argc == 8 ) {
		f = fopen(argv[7], "w");
		if ( f == NULL )
			f = stdout;
	} else {
		f = stdout;
	}
#endif

	if ( ! tables_load_struct(name ) ) {
		g_print(_("Error loading the descfile (section: table)\n"));
		return 1;
	}
	
	if ( ! st_load_struct(NULL, name ) ) {
		g_print(_("Error loading the descfile (section: subtable)\n"));
		return 2;
	}

	st = get_subtable_by_name(subtable);
	if ( st == NULL ) {
		g_print("Unknown subtable (%s)\n", subtable);
		return 3;
	}
	
	for ( i=0; i < st->nb_fields; i++ ) {
		if ( st->fields[i].type == T_RECORD || 
				st->fields[i].type == T_RECORDS ) {
			load_everything = TRUE;
		}
	}

	if ( load_everything == TRUE ) {
		tables_load();
	} else {
		table_load_file(st->table);
	}

	print_plugins = printplugins_load();
	while ( print_plugins != NULL ) {
		ppd = print_plugins->data;
		print_plugins = g_list_next(print_plugins);
		if ( strcmp(ppd->i18n_name, format) == 0 || 
				strcmp(ppd->name, format) == 0 ) {
			print_real(ppd, st, f);
		}
	}

	
	return 0;
	
#else /* ! HAVE_LIBXML */
	fputs(_("Gaby was not compiled with support for print plug-ins !\n"),
			stderr );
	return 1;
#endif /* ! HAVE_LIBXML */
	
}


