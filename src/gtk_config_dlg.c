/*  Gaby
 *  Copyright (C) 1998-2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gaby.h"
#include "files.h"
#include "gtk_config_dlg.h"

static gboolean already_done (gchar *one, gchar **all);
static GList* get_actions_configure_boxes (GList *boxes);
static GList* get_views_configure_boxes (GList *boxes);
static GList* get_print_configure_boxes (GList *boxes);
static GList* get_common_configure_boxes (GList *boxes);

static GtkWidget *configure_window = NULL;

#ifdef USE_GNOME
static void gnome_apply_clicked(GtkWidget *widget, int page);
#else
static void save_clicked(GtkWidget *widget, GtkWidget *win);
static void apply_clicked(GtkButton *button, GtkWidget *win);
static void ok_clicked(GtkButton *button, GtkWidget *win);
#endif
	
static void close_clicked(GtkButton *button, GtkWidget *win);

static gboolean delete_event(GtkWidget *win, gpointer data);

GtkWidget* configure_dialog (gchar *active_page)
{
	GList *configureboxes = NULL;
	GtkWidget *notebook;
	GtkWidget *page_label, *page_box;
#ifndef USE_GNOME
	GtkWidget *main_vbox, *action_area;
	GtkWidget *buttonbox;
	GtkWidget *ok, *apply, *save, *close;
#endif
	int i;
	
	gchar *page_name;
	
	if ( configure_window && configure_window->window) {
		return configure_window;
	}
	
	configureboxes = get_common_configure_boxes(configureboxes);
	configureboxes = get_views_configure_boxes(configureboxes);
	configureboxes = get_actions_configure_boxes(configureboxes);
	configureboxes = get_print_configure_boxes(configureboxes);
	
	if ( configureboxes == NULL ) {
		gaby_errno = CUSTOM_MESSAGE;
		gaby_message = g_strdup(_("Nothing to configure !\n"));
		gaby_perror_in_a_box();
		return NULL;
	}
	
	configureboxes = g_list_first(configureboxes);
	
#ifdef USE_GNOME
	configure_window = gnome_property_box_new();
	notebook = GNOME_PROPERTY_BOX(configure_window)->notebook;
#else
	configure_window = gtk_dialog_new();
	
	gtk_container_border_width(GTK_CONTAINER(configure_window), 5);
	gtk_window_set_policy(GTK_WINDOW(configure_window), FALSE, FALSE, TRUE);
	
	main_vbox = GTK_DIALOG(configure_window)->vbox;
	gtk_widget_show(main_vbox);

	notebook = gtk_notebook_new();
	gtk_box_pack_start(GTK_BOX(main_vbox), notebook, TRUE, TRUE, 0);
	gtk_widget_show(notebook);
	gtk_container_border_width(GTK_CONTAINER(notebook), 5);
#endif
	
	gtk_window_set_title(GTK_WINDOW(configure_window), _("Preferences"));
	
	gtk_signal_connect(GTK_OBJECT(configure_window), "delete_event", 
			GTK_SIGNAL_FUNC(delete_event), NULL);
	gtk_object_set_data(GTK_OBJECT(configure_window), "pages", 
			configureboxes);
	
	i = 0;
	while ( configureboxes != NULL ) {
		page_box = configureboxes->data;
		page_name = gtk_object_get_data(GTK_OBJECT(page_box), "name");
#ifdef DEBUG_GABY
		debug_print("[configure_dialog] append_page : %p ", page_name);
		debug_print(" (%s)\n", page_name );
#endif
		page_label = gtk_label_new(page_name);
		gtk_widget_show(page_label);
		gtk_notebook_append_page(GTK_NOTEBOOK(notebook), 
				GTK_WIDGET(page_box), GTK_WIDGET(page_label));
		configureboxes = g_list_next(configureboxes);
		if ( active_page && strcmp(active_page, page_name) == 0 ) {
			gtk_notebook_set_page(GTK_NOTEBOOK(notebook), i);
		}
		i++;
	}

#ifdef USE_GNOME
	gtk_signal_connect(GTK_OBJECT(configure_window), "apply", 
			GTK_SIGNAL_FUNC(gnome_apply_clicked), configure_window);
	gtk_signal_connect(GTK_OBJECT(configure_window), "close", 
			GTK_SIGNAL_FUNC(close_clicked), configure_window );
	
#else
	action_area = GTK_DIALOG(configure_window)->action_area;
	
	buttonbox = gtk_hbutton_box_new();
	gtk_widget_show(buttonbox);
	gtk_box_pack_start(GTK_BOX(action_area), buttonbox, TRUE, TRUE, 0);

	ok = gtk_button_new_with_label(_("OK"));
	gtk_widget_show(ok);
	gtk_container_add(GTK_CONTAINER(buttonbox), ok);
	gtk_signal_connect(GTK_OBJECT(ok), "clicked", 
			GTK_SIGNAL_FUNC(ok_clicked), configure_window);

	apply = gtk_button_new_with_label(_("Apply"));
	gtk_widget_show(apply);
	gtk_container_add(GTK_CONTAINER(buttonbox), apply);
	gtk_signal_connect(GTK_OBJECT(apply), "clicked", 
			GTK_SIGNAL_FUNC(apply_clicked), configure_window);

	save = gtk_button_new_with_label(_("Save"));
	gtk_widget_show(save);
	gtk_container_add(GTK_CONTAINER(buttonbox), save);
	gtk_signal_connect(GTK_OBJECT(save), "clicked", 
			GTK_SIGNAL_FUNC(save_clicked), configure_window);

	close = gtk_button_new_with_label(_("Close"));
	gtk_widget_show(close);
	gtk_signal_connect(GTK_OBJECT(close), "clicked", 
			GTK_SIGNAL_FUNC(close_clicked), configure_window);
	gtk_container_add(GTK_CONTAINER(buttonbox), close);
#endif /* ! USE_GNOME */

	return configure_window;
}

static GList* get_actions_configure_boxes(GList *boxes)
{
	action *a;
	GList *actions = list_actions;
	gchar **done = g_new0(gchar*, g_list_length(actions)+1);
	ActionPluginData *apd;
	char plugin_name[PATH_MAX];
	GtkWidget *box;
	
	while ( actions != NULL && actions->data != NULL ) {
		a = actions->data;
		actions = g_list_next(actions);
		if ( a->type != PLUG_IN ) continue;
		if ( ! already_done(a->what.plugin->name, done) ) {
#ifdef DEBUG_GABY
			debug_print("Loading %s configuration box\n", 
						a->what.plugin->name);
#endif
			apd = a->what.plugin;

#ifdef FOLLOW_MIGUEL
			sprintf(plugin_name, "%s_configure", apd->name );
			g_module_symbol( 
				g_module_open(NULL, G_MODULE_BIND_LAZY), 
				plugin_name, (gpointer)&apd->configure );
#else
			if ( apd->loaded == 0 ) {
				sprintf(plugin_name, PLUGINS_DIR "/actions/lib%s" SO_EXTENSION,
						apd->name);
				apd->handle = g_module_open(plugin_name, 0);
				if ( apd->handle == NULL )
					continue;
			}
			if ( apd->configure == NULL ) {
				g_module_symbol(apd->handle, "configure", 
						(gpointer)&apd->configure );
				if ( apd->configure == NULL ) {
					g_module_close(apd->handle);
					apd->handle = NULL;
					continue;
				}
			}
#endif
			box = apd->configure();
			if ( box != NULL ) {
				gtk_object_set_data(GTK_OBJECT(box), "type", 
						GINT_TO_POINTER(1) );
				gtk_object_set_data(GTK_OBJECT(box),"apd",apd);
				gtk_object_set_data(GTK_OBJECT(box), 
						"gaby_name", a->what.plugin->name);
				boxes = g_list_append(boxes, box);
				apd->loaded++;
			} else {
				if ( apd->loaded == 0 ) {
					g_module_close(apd->handle);
					apd->handle = NULL;
				}
			}
		}
	}
	g_free(done);
	
	return boxes;
}

static gboolean already_done(gchar *one, gchar **all)
{
	
	while ( *all != NULL ) {
		if ( strcmp(one, *all) == 0 ) {
			return TRUE;
		}
		all++;
	}
	*all = one;
	return FALSE;
}

static GList* get_views_configure_boxes (GList *boxes)
{
	ViewPluginData *vpd;
	GtkWidget *box;
	GList *views = list_views;
	gchar **done = g_new0(gchar*, g_list_length(views)+1);
	view *v;

	while ( views != NULL ) {
		v = views->data;
		views = g_list_next(views);
		vpd = v->type;
		if ( vpd == NULL ) continue;
#ifdef DEBUG_GABY
		debug_print("[get_views_cfg_boxes] vpd->name : %s\n",vpd->name);
#endif
		if ( already_done( vpd->name, done) ) {
			continue;
		}

		if ( vpd->configure != NULL ) {
			box = vpd->configure(vpd);
			if ( box != NULL ) {
				gtk_object_set_data(GTK_OBJECT(box), "type", GINT_TO_POINTER(0) );
				gtk_object_set_data(GTK_OBJECT(box), "gaby_name", vpd->name);
				boxes = g_list_append(boxes, box);
			}
		}
	}
	
	g_free(done);
	
	return boxes;
}

static void configure_common_save ()
{
	GList *pages = gtk_object_get_data( 
			GTK_OBJECT(configure_window), "pages");
	GtkWidget *common = pages->data;
	GtkWidget *check;

	check = gtk_object_get_data(GTK_OBJECT(common), "fastprint" );
	write_config_bool ("common", "print", "fast", 
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check)));

	check = gtk_object_get_data(GTK_OBJECT(common), "tips" );
	write_config_bool ("common", "misc", "tips", 
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check)));
	
}

static void files_properties_save ()
{
	GList *pages = gtk_object_get_data( 
			GTK_OBJECT(configure_window), "pages");
	GtkWidget *fileprop = pages->next->data;
	GList *tables = gtk_object_get_data(GTK_OBJECT(fileprop), "tables");
	GtkWidget *check;
	GList *tmp;
	struct location *loc;

#ifdef DEBUG_GABY
	debug_print("[files_properties_save] tables : %p\n", tables);
#endif
	while ( tables != NULL ) {
		tmp = ((struct _table*)(tables->data))->locations;
		tables = g_list_next(tables);
		while ( tmp != NULL ) {
			loc = tmp->data;
			check = gtk_object_get_data(GTK_OBJECT(fileprop), 
					loc->filename);
			loc->disabled = gtk_toggle_button_get_active( 
						GTK_TOGGLE_BUTTON(check));
			write_config_bool("app", appname, loc->filename, 
						loc->disabled );
			tmp = g_list_next(tmp);
		}
	}
	
}

#if 0
static void select_location(GtkCList *clist, gint row, gint column,
			    GdkEventButton *event, GtkWidget *box )
{
	GtkWidget *check =gtk_object_get_data(GTK_OBJECT(box),"check_disabled");
	GList *tables = gtk_object_get_data(GTK_OBJECT(box), "tables");
	GtkWidget *label;
	GList *tmp;
	gchar *str;
	struct location *loc = NULL;

	gtk_clist_get_text(clist, row, 0, &str);
	
	while ( tables != NULL ) {
		tmp = ((struct _table*)(tables->data))->locations;
		tables = g_list_next(tables);
		while ( tmp != NULL ) {
			loc = tmp->data;
			if ( strcmp(loc->filename, str) == 0 ) break;
			tmp = g_list_next(tmp);
		}
		if ( tmp != NULL ) break;
	}
	if ( loc == NULL ) {
		/* uh oh */
#ifdef DEBUG_GABY
		debug_print("[select_location] the location doesn't exist !\n");
#endif
		return;
	}

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), loc->disabled);
	
	label = gtk_object_get_data(GTK_OBJECT(box), "label_format");
	gtk_label_set_text(GTK_LABEL(label), loc->type);
}

static void loc_disabled_clicked(GtkWidget *check, GtkWidget *box)
{
	GtkCList *clist = gtk_object_get_data(GTK_OBJECT(box), "clist");
	GList *tables = gtk_object_get_data(GTK_OBJECT(box), "tables");
	GList *tmp;
	gchar *str;
	struct location *loc = NULL;
	int row;

	if ( clist->selection == NULL ) return;

	row = GPOINTER_TO_INT(clist->selection->data);
	gtk_clist_get_text(clist, row, 0, &str);
	
	while ( tables != NULL ) {
		tmp = ((struct _table*)(tables->data))->locations;
		tables = g_list_next(tables);
		while ( tmp != NULL ) {
			loc = tmp->data;
			if ( strcmp(loc->filename, str) == 0 ) break;
			tmp = g_list_next(tmp);
		}
		if ( tmp != NULL ) break;
	}
	if ( loc == NULL ) {
		/* uh oh */
#ifdef DEBUG_GABY
		debug_print("[select_location] the location doesn't exist !\n");
#endif
		return;
	}
	loc->disabled = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
}
#endif

static GtkWidget* files_properties_box(GList *tables)
{
	GtkWidget *vbox;
/*	GtkWidget *label, *clist, *table;*/
	GtkWidget *check, *frame, *vbox2;
	struct location *l;
	GList *tmp, *tmp2;
	char str[200];
	char plugin_name[30];
	FILE *f;
	
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);
	gtk_widget_show(vbox);

	tmp = g_list_first(tables);
	f = fopen(PLUGINS_DIR "/formats/infos", "r");
	
#if 1
	frame = gtk_frame_new(_("Disabled files"));
	gtk_widget_show(frame);
	gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);

	vbox2 = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox2);
	gtk_container_add(GTK_CONTAINER(frame), vbox2);
	
	while ( tmp != NULL ) {
		/*
		 * it failed when I put 'table*' so I put 'struct _table*'
		 * and it worked.
		 *  $ gcc --version
		 * egcs-2.91.66
		 */
		tmp2 = ((struct _table*)(tmp->data))->locations;
		tmp = g_list_next(tmp);
		while ( tmp2 != NULL ) {
			l = tmp2->data;
			tmp2 = g_list_next(tmp2);
			strcpy(plugin_name, l->type);
			sprintf(str, "%s (%s)", l->filename, 
					get_plugin_real_name(0,f,plugin_name)) ;
			check = gtk_check_button_new_with_label (str);
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 
							l->disabled);
			gtk_signal_connect_object(GTK_OBJECT(check), "clicked",
				GTK_SIGNAL_FUNC(gaby_property_box_changed), 
				GTK_OBJECT(vbox) );
			gtk_widget_show(check);
			gtk_object_set_data(GTK_OBJECT(vbox), l->filename, 
					check);
			gtk_box_pack_start(GTK_BOX(vbox2), check, 
							FALSE, FALSE, 0);
		}
	}
	
#else
	clist = gtk_clist_new(1);
	gtk_signal_connect(GTK_OBJECT(clist), "select_row", 
			GTK_SIGNAL_FUNC(select_location), vbox );

	while ( tmp != NULL ) {
		/*
		 * it failed when I put 'table*' so I put 'struct _table*'
		 * and it worked.
		 *  $ gcc --version
		 * egcs-2.91.66
		 */
		tmp2 = ((struct _table*)(tmp->data))->locations;
		tmp = g_list_next(tmp);
		while ( tmp2 != NULL ) {
			l = tmp2->data;
			tmp2 = g_list_next(tmp2);
			gtk_clist_append(GTK_CLIST(clist), &l->filename);
		}
	}
	gtk_widget_show(clist);
	gtk_object_set_data(GTK_OBJECT(vbox), "clist", clist);
	gtk_box_pack_start(GTK_BOX(vbox), clist, FALSE, FALSE, 5);

	table = gtk_table_new(3, 2, FALSE);
	gtk_widget_show(table);
	gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 5);

	label = gtk_label_new(_("Format")); gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 0, 1);

	label = gtk_label_new(""); gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 1, 2, 0, 1);
	gtk_object_set_data(GTK_OBJECT(vbox), "label_format", label);

	label = gtk_label_new(_("Disabled")); gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 1, 2);

	check = gtk_check_button_new (); gtk_widget_show(check);
	gtk_table_attach_defaults(GTK_TABLE(table), check, 1, 2, 1, 2);
	gtk_signal_connect(GTK_OBJECT(check), "clicked", 
			GTK_SIGNAL_FUNC(loc_disabled_clicked), vbox);
	gtk_object_set_data(GTK_OBJECT(vbox), "check_disabled", check);

	gtk_clist_select_row(GTK_CLIST(clist), 0, 0);
#endif
	if ( f != NULL ) fclose(f);

	gtk_object_set_data(GTK_OBJECT(vbox), "tables", tables );
	gtk_object_set_data(GTK_OBJECT(vbox), "name", _("Files Properties"));
	gtk_object_set_data(GTK_OBJECT(vbox), "cfg_save",files_properties_save);
	return vbox;
}

static GList* get_common_configure_boxes(GList *boxes)
{
	/* some general configure boxes */

	GtkWidget *vbox;
	GtkWidget *check;
	
	vbox = gtk_vbox_new(FALSE, 0);
	gtk_widget_show(vbox);

	check = gtk_check_button_new_with_label(_("Use \"fast\" printing"));
	gtk_widget_set_sensitive(check, FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 
			get_config_bool("common", "print", "fast", FALSE ) );
	gtk_signal_connect_object(GTK_OBJECT(check), "clicked", 
			GTK_SIGNAL_FUNC(gaby_property_box_changed), 
			GTK_OBJECT(vbox) );
	gtk_widget_show(check);
	gtk_object_set_data(GTK_OBJECT(vbox), "fastprint", check);
	gtk_box_pack_start(GTK_BOX(vbox), check, TRUE, TRUE, 0 );
	
	check = gtk_check_button_new_with_label(_("Show tips on startup"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), 
			get_config_bool("common", "misc", "tips", TRUE ) );
	gtk_signal_connect_object(GTK_OBJECT(check), "clicked", 
			GTK_SIGNAL_FUNC(gaby_property_box_changed), 
			GTK_OBJECT(vbox) );
	gtk_widget_show(check);
	gtk_object_set_data(GTK_OBJECT(vbox), "tips", check);
	gtk_box_pack_start(GTK_BOX(vbox), check, TRUE, TRUE, 0 );

	
	gtk_object_set_data(GTK_OBJECT(vbox), "cfg_save", 
							configure_common_save);
	gtk_object_set_data(GTK_OBJECT(vbox), "name", _("Misc"));
	boxes = g_list_append(boxes, vbox);

	vbox = files_properties_box(list_tables);
	boxes = g_list_append(boxes, vbox);

	return boxes;
}

static GList* get_print_configure_boxes(GList *boxes)
{
#if 0
	PrintPluginData *ppd;
	GtkWidget* (*configure) ();
	GtkWidget *box;
	char filename[PATH_MAX];
	GList *print_plugins = list_print_plugins;
	
	if ( print_plugins == NULL ) {
		return boxes;
	}

	while ( print_plugins != NULL ) {
		ppd = print_plugins->data;
		print_plugins = g_list_next(print_plugins);
#ifdef FOLLOW_MIGUEL
		sprintf(filename, "%s_configure", ppd->name);
		g_module_symbol(ppd->handle, filename,(gpointer)&configure );
#else /* ! FOLLOW_MIGUEL */
		sprintf(filename, "%s/print/lib%s.so", PLUGINS_DIR, ppd->name);
#ifdef DEBUG_GABY
		debug_print("Loading %s\n", filename);
#endif
		if ( ppd->loaded == 0 ) {
			ppd->handle = g_module_open(filename, 0);
			if ( ppd->handle == NULL )
				continue;
		}
#ifdef DEBUG_GABY
		else {
			debug_print("[gpcb] ppd->loaded : %d, ppd->name : %s\n",
					ppd->loaded, ppd->name);
		}
#endif
		
#ifdef DEBUG_GABY
		debug_print("Loading %s configure box\n", ppd->i18n_name);
#endif
		g_module_symbol(ppd->handle, "configure",(gpointer)&configure );
#ifdef DEBUG_GABY
		debug_print("configure : %p\n", configure);
#endif
#endif /* ! FOLLOW_MIGUEL */
		if ( configure == NULL ) {
			if ( ppd->loaded == 0 ) {
				g_module_close(ppd->handle);
				ppd->handle = NULL;
			}
			continue;
		}
			
		box = configure();
		if ( box == NULL ) {
			if ( ppd->loaded == 0 ) {
				g_module_close(ppd->handle);
				ppd->handle = NULL;
			}
		} else {
			ppd->loaded++;
#if 1
			gtk_object_set_data(GTK_OBJECT(box), "ppd", ppd);
			if ( ! gtk_object_get_data(GTK_OBJECT(box), "name") ) {
				gtk_object_set_data(GTK_OBJECT(box), "name", 
							ppd->i18n_name);
			}
#endif /* 0 */
			boxes = g_list_append(boxes, box);
		}
	}
#endif
	
	return boxes;
}

#ifndef USE_GNOME
static void apply_one(GtkWidget *page, gpointer data)
{
	void (*configure_apply) ();
	
	configure_apply = gtk_object_get_data(GTK_OBJECT(page), "cfg_apply");
	if ( configure_apply == NULL ) return;
	
	configure_apply();

}
#endif

static void save_one(GtkWidget *page, gpointer data)
{
#if 0
	gchar** (*configure_save) ();
	gchar **result;
	gchar *types[] = {
		"view",
		"actions"
	};
	gchar *name;
	int type;
	int i;
#else
	void (*configure_save) ();
#endif

	configure_save = gtk_object_get_data(GTK_OBJECT(page), "cfg_save");
	if ( configure_save == NULL ) return;
#if 0
	type = GPOINTER_TO_INT(gtk_object_get_data(GTK_OBJECT(page), "type"));
	name = gtk_object_get_data(GTK_OBJECT(page), "gaby_name");
	result = configure_save();
	i=0;

	write_config(types[type], name, result);
	while ( result[i] != NULL ) {
		g_free(result[i]);
		i++;
	}
	g_free(result);
#else
	configure_save();
#endif
}

#ifdef USE_GNOME

void gaby_property_box_changed (gpointer data)
{
#ifdef DEBUG_GABY
	debug_print("[gpbc] let's warn gnome\n");
#endif
	gnome_property_box_changed(GNOME_PROPERTY_BOX(configure_window));
}

#else

void gaby_property_box_changed (gpointer data)
{
	/* with this function, plug-ins no longer need to worry about gnome
	 * available or not -> no more #ifdef USE_GNOME */

	return;
}

#endif /* ! USE_GNOME */

#ifdef USE_GNOME

static void gnome_apply_clicked(GtkWidget *widget, int page)
{
	GList *cfg_boxes = gtk_object_get_data(GTK_OBJECT(widget), "pages");
#ifdef DEBUG_GABY
	debug_print("[gac] page : %d\n", page);
#endif
	if ( page == -1 ) {
#ifdef DEBUG_GABY
		debug_print("[gac] cfg_boxes = %p\n", cfg_boxes);
#endif
		g_list_foreach(cfg_boxes, (GFunc)save_one, NULL);
	}
}

#else /* ! USE_GNOME */

static void ok_clicked(GtkButton *button, GtkWidget *win)
{
	apply_clicked(button, win);
	close_clicked(button, win);
}

static void apply_clicked(GtkButton *button, GtkWidget *win)
{
	GList *cfg_boxes = gtk_object_get_data(GTK_OBJECT(win), "pages");
	g_list_foreach(cfg_boxes, (GFunc)apply_one, NULL);
}

static void save_clicked(GtkWidget *widget, GtkWidget *win)
{
	GList *cfg_boxes = gtk_object_get_data(GTK_OBJECT(win), "pages");
	g_list_foreach(cfg_boxes, (GFunc)save_one, NULL);
}

#endif /* ! USE_GNOME */


static void free_one(GtkWidget *page, gpointer data)
{
#ifndef FOLLOW_MIGUEL
	ActionPluginData *apd;
	PrintPluginData *ppd;

	apd = gtk_object_get_data(GTK_OBJECT(page), "apd");
	if ( apd != NULL ) {
		apd->loaded--;
		if ( apd->loaded == 0 ) {
#ifdef DEBUG_GABY
			debug_print("[free_one] freeing %s\n", apd->name);
#endif
			g_module_close(apd->handle);
			apd->handle = NULL;
		}
	}

#if 0
	ppd = gtk_object_get_data(GTK_OBJECT(page), "ppd");
	if ( ppd != NULL ) {
		ppd->loaded--;
		if ( ppd->loaded == 0 ) {
#ifdef DEBUG_GABY
			debug_print("[free_one] freeing %s\n", ppd->name);
#endif
			g_module_close(ppd->handle);
			ppd->handle = NULL;
		}
	}
#endif
#endif /* ! FOLLOW_MIGUEL */
}

static void close_clicked(GtkButton *button, GtkWidget *win)
{
	GList *cfg_boxes = gtk_object_get_data(GTK_OBJECT(win), "pages");
	g_list_foreach(cfg_boxes, (GFunc)free_one, NULL);
	gtk_widget_destroy(win);
	configure_window = NULL;
}

static gboolean delete_event(GtkWidget *win, gpointer data)
{
	configure_window = NULL;
	return FALSE;
}

void destroy_configure_event(GtkWidget **box, gpointer data)
{
	*box = NULL;
}

