/*  Gaby
 *  Copyright (C) 2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include "gaby.h"
#else
 /* when not compiled inside gaby */
#  include <glib.h>
#  define debug_print printf
#  define _(String) (String)
#  define HAVE_LIBXML
#  define TESTXML

#endif

#ifdef HAVE_LIBXML

#include <libxml/parser.h>
#include "print_plg.h"

static GList *pp_names = NULL;

static GabyPageFields* page_fields_parse(xmlNode *node)
{
	GabyPageFields *pf = g_new0(GabyPageFields, 1);

	pf->optional = FALSE;
	while ( node ) {
		xmlNode *cur = node;
		node = node->next;

		if ( strcmp(cur->name, "optional") == 0 ) {
			pf->optional = TRUE;
			continue;
		}

		if ( strcmp(cur->name, "name") == 0 ) {
			struct ref *r = g_new0(struct ref, 1);
			r->type = PAGE_FIELDS;
			r->key = g_strdup(cur->children->content);
			r->pointer = pf;
			pp_names = g_list_append(pp_names, r);
			continue;
		}

		if ( strcmp(cur->name, "selection") == 0 ) {
			pf->selection = 0;
			if ( strcmp(cur->children->content, "all") == 0 ) {
				pf->selection = G_MAXINT;
				continue;
			}
			pf->selection = atoi(cur->children->content);
		
			if ( strcmp(cur->properties->name, "type") != 0 ) {
				/* unknown attributes */
				continue;
			}
			if ( strcmp(cur->properties->children->content, "last")==0)
				pf->selection = - pf->selection;

			continue;
		}

	}

	return pf;
}

static GabyPage* page_records_parse(xmlNode *node)
{
	GabyPage *pf = g_new0(GabyPage, 1);

	while ( node ) {
		xmlNode *cur = node;
		node = node->next;
		
		if ( strcmp(cur->name, "name") == 0 ) {
			struct ref *r = g_new0(struct ref, 1);
			r->type = PAGE_RECORDS;
			r->key = g_strdup(cur->children->content);
			r->pointer = pf;
			pp_names = g_list_append(pp_names, r);
		}
	}

	return pf;
}

static GabyPage* page_order_parse(xmlNode *node)
{
	GabyPage *pf = g_new0(GabyPage, 1);

	while ( node ) {
		xmlNode *cur = node;
		node = node->next;
		
		if ( strcmp(cur->name, "name") == 0 ) {
			struct ref *r = g_new0(struct ref, 1);
			r->type = PAGE_ORDER;
			r->key = g_strdup(cur->children->content);
			r->pointer = pf;
			pp_names = g_list_append(pp_names, r);
		}
	}

	return pf;
}

static GabyPageMisc* page_misc_parse(xmlNode *node)
{
	GabyPageMisc *pm = g_new0(GabyPageMisc, 1);
	
	pm->widgets = NULL;
	while ( node ) {
		xmlNode *cur = node;
		xmlAttr *attrs;
		GabyWidget *wid;
		node = node->next;
		
		if ( strcmp(cur->name, "widget") != 0 ) continue;
		wid = g_new0(GabyWidget, 1);
		
		attrs = cur->properties;
		while ( attrs ) {
			if ( strcmp(attrs->name, "label") == 0 ) {
				wid->label = g_strdup(_(attrs->children->content));
			} else if ( strcmp(attrs->name, "type") == 0 ) {
				if (strcmp(attrs->children->content, "filesel") == 0)
					wid->type = FILE_SELECTION;
				if (strcmp(attrs->children->content, "label") == 0)
					wid->type = LABEL;
				if (strcmp(attrs->children->content, "checkbox") ==0)
					wid->type = CHECKBOX;
				if (strcmp(attrs->children->content, "entry") ==0)
					wid->type = ENTRY;
			}
			attrs = attrs->next;
		}
		
		if ( cur->children && strcmp(cur->children->name, "name") == 0 ) {
			struct ref *r = g_new0(struct ref, 1);
			switch ( wid->type ) {
				case FILE_SELECTION:
					r->type = WIDGET_FILESEL; break;
				case CHECKBOX:
					r->type = WIDGET_CHECKBOX; break;
				case ENTRY:
					r->type = WIDGET_ENTRY; break;
				default:
					break;
			}
			r->key = g_strdup(cur->children->children->content);
			r->pointer = wid;
			pp_names = g_list_append(pp_names, r);
		}

		pm->widgets = g_list_append(pm->widgets, wid);
	}

	if ( pm->widgets ) pm->widgets = g_list_first(pm->widgets);
	
	return pm;
}

int print_load_xml(char *filename, GList **pages, GList **names)
{
	xmlDoc *doc;
	xmlNode *cur;

	pp_names = NULL;
	
	doc = xmlParseFile(filename);
	if (doc == NULL) {
		return 1;
	}
	
	cur = xmlDocGetRootElement(doc);

	if ( strcmp(cur->name, "pages") != 0 ) {
		debug_print("top level name != pages; aborting.\n");
		xmlFreeDoc(doc);
		return 1;
	}

	cur = cur->children;
	while ( cur != NULL ) {
		xmlNode *currentPage = cur;
		xmlAttr *attrs;
		GabyPage *tpg, *pg;
		
		cur = cur->next;

		if ( strcmp(currentPage->name, "page") != 0 ) continue;

		tpg = g_new0(GabyPage, 1);

		/* attributes: should be label and type */
		attrs = currentPage->properties;
		while ( attrs ) {
			if ( strcmp(attrs->name, "label") == 0 ) {
				tpg->label = g_strdup(_(attrs->children->content));
			} else if ( strcmp(attrs->name, "type") == 0 ) {
				if (strcmp(attrs->children->content, "fields") == 0)
					tpg->type = FIELDS;
				if (strcmp(attrs->children->content, "misc") == 0)
					tpg->type = MISC;
				if (strcmp(attrs->children->content, "records") == 0)
					tpg->type = RECORDS;
				if (strcmp(attrs->children->content, "order") == 0)
					tpg->type = ORDER;
			}
			attrs = attrs->next;
		}
		switch ( tpg->type ) {
			case FIELDS:
			{
				GabyPageFields *pp;
				pp = page_fields_parse(currentPage->children);
				pg = (GabyPage*)pp;
			} break;
			case MISC:
			{
				GabyPageMisc *pp;
				pp = page_misc_parse(currentPage->children);
				pg = (GabyPage*)pp;
			} break;
			case RECORDS:
			{
				GabyPage *pp;
				pp = page_records_parse(currentPage->children);
				pg = (GabyPage*)pp;
			} break;
			case ORDER:
			{
				GabyPage *pp;
				pp = page_order_parse(currentPage->children);
				pg = (GabyPage*)pp;
			} break;
			default:
			{
				pg = g_new0(GabyPage, 1);
			} break;
		}
		memcpy((void*)pg, (void*)tpg, sizeof(GabyPage));
		free(tpg);
		*pages = g_list_append(*pages, pg);
	}

	*names = g_new0(GList, 1);
	memcpy(*names, pp_names, sizeof(GList));
	
	xmlFreeDoc(doc);

	return 0;
}

#ifdef TESTXML
int main(int argc, char *argv[])
{
	char filename[50];
	GList *pages = NULL;
	GList *names;

	if ( argc != 2 ) strcpy(filename, "html2.xml");
	else		 strcpy(filename, argv[1]);
	
	print_load_xml(filename, &pages, &names);
	
	pages = g_list_first(pages);
	while ( pages != NULL ) {
		GabyPage *p = pages->data;
		pages = g_list_next(pages);
		g_print("Page: label: %s, type: %d\n", p->label, p->type);
		switch ( p->type ) {
			case FIELDS:
			{
				GabyPageFields *pp = (GabyPageFields*)p;
				g_print("\toptional: %d\n", pp->optional);
				g_print("\tselection: %d\n", pp->selection);
			} break;
			case MISC:
			{
				GabyPageMisc *pp = (GabyPageMisc*)p;
				GList *tmp = pp->widgets;
				while ( tmp ) {
					GabyWidget *w = tmp->data;
					tmp = g_list_next(tmp);
					g_print("\twidget: %d %s\n",
							w->type, w->label);
				}
			} break;
			default:
			{
				;
			} break;
		}
	}

	if ( names ) names = g_list_first(names);
	while ( names != NULL ) {
		struct ref *r = names->data;
		g_print("key: %s, type: %d, val: %p\n", r->key, r->type, r->pointer);
		names = g_list_next(names);
	}

}
#endif /* TESTXML */


#endif /* HAVE_LIBXML */

