/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

gboolean get_config_bool(gchar *type, gchar *name, gchar *item, gboolean def);
gboolean write_config_bool(gchar *type, gchar *name, gchar *item, gboolean val);

gchar* get_config_str(gchar *type, gchar *name, gchar *item, gchar *def);
gboolean write_config_str( gchar *type, gchar *name, gchar *item, gchar *str);

gint get_config_int(gchar *type, gchar *name, gchar *item, gint def);
gboolean write_config_int(gchar *type, gchar *name, gchar *item, gint val);

