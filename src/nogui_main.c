/*  Gaby
 *  Copyright (C) 1998-2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/* dumb functions to compile without GUI support */

#include <gaby.h>

gint reread_cb(struct location *loc) {return 0; }

gabywindow* new_view_create(struct window_info *wi) { return NULL; }

gboolean* select_subtable_dialog (char *title, void (*callback) (subtable *st),
				  subtable **st) { return NULL; }
void wait_dialog_to_finish(gboolean *val) { return; }

gboolean gaby_quit() { exit(0); return TRUE; }

void page_select_fields_get_info(subtable *st, void *page,
				 gboolean *active, int *selected) {}
void page_select_records_get_info(subtable *st, void *page,
				  gboolean *all, GList **recs) {}
void page_select_order_get_info(subtable *st, void *page, int *order) {}

int debug_mode;

