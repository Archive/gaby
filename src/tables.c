/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gaby.h"
#include "f_desc.h"	/* for field_get_property */
#include "tables.h"
#include "records.h"

#ifdef USE_SQL
#  include <postgres.h>
#  include <libpq-fe.h>
PGconn *sqlconn;        /* connection to the PostgreSQL database */
#endif

/* how much records do we want to reserve memory for ? */
#define OPTIMUM_NUMBER	256

/* this allows to popup a dialog asking to save the files when leaving gaby
 * I should add a field to struct table with this information so I won't have
 * to save every thing (ie not my 2845 records long zipcode file)
 * ODOT: put the updated flag in struct table [DONE:991227]
 */

#ifdef INDEXES_ARE_ARRAYS

static int* index_create (table *t, int no);
static int index_find ( table *t, int num_field, record *r, gboolean exact );

#else /* ! INDEXES_ARE_ARRAYS */

static GList* index_create(table *t, int no);

/*
 * it's so sad : I need a global variable to sort the records :( 
 * glib/gtk+ couldn't be perfect :)
 */
static int gl_no_field;

static int index_sort_id(record *a, record *b);
static int index_sort_str(record *a, record *b);
static int index_sort_int(record *a, record *b);
static int index_sort_real(record *a, record *b);

#endif /* ! INDEXES_ARE_ARRAYS */

static void indexes_create (table *t);

gboolean record_check(table *t, record *r);

/**
 * record_add
 * @t: the table you want to add a record in
 * @r: the record you want to add to the table
 * @check: whether you want to check the new record against integrity rules
 * @loading: whether you are a loading function
 *
 * Description:
 * This function append the record @r to the table @t, if @check is TRUE, the
 * record is checked against the rules specified in the description file.
 *
 * Returns: id of the new record, -1 if failed
 **/
int record_add(table *t, record *r, gboolean check, gboolean loading)
{
	/* return index of the new record, -1 if failed
	 *  if failed, int gaby_errno is set to the error number,
	 *  you may call 'char* gaby_perror()' to get a more
	 *  readable information.
	 *  you can disable the check with the 3rd param
	 */
#ifdef USE_SQL
	char buf[1000];
	PGresult *res;
	int i;
	int new_id;

	debug_print("[record_add (SQL)] --\n");

	g_snprintf(buf, 1000, "SELECT MAX(id)+1 FROM %s", t->name);
	debug_print("sql statement is %s\n", buf);
	res = PQexec(sqlconn, buf);
	if (res == NULL || PQresultStatus(res) != PGRES_TUPLES_OK) {
		debug_print("[record_add (sql)] unable to get new_id :(\n");
		if (res)
			PQclear(res);
		return -1;
	}
	new_id = atoi(PQgetvalue(res, 0, 0));
	PQclear(res);

	if ( new_id == 0 ) new_id++;
	
	debug_print("[record_add (sql)] new_id will be %d\n", new_id);

	g_snprintf(buf, 1000, "INSERT INTO %s VALUES ( %d, ", t->name, new_id );
	
	for ( i=0; i<t->nb_fields; i++) {
		GString *str;
		str = get_table_stringed_field(t, r, i);
		if ( strlen(str->str) == 0 ) {
			strcat(buf, "NULL, ");
		} else {
			strcat(buf, "'");
			strcat(buf, str->str);
			strcat(buf, "', ");
		}
		g_string_free(str, 1);
	}
	strcpy(buf+strlen(buf)-2, " )");

	debug_print("[record_add] sql statement is:\n%s\n", buf);
	res = PQexec(sqlconn, buf);
	if (res == NULL || PQresultStatus(res) != PGRES_COMMAND_OK) {
		debug_print("[record_add (sql)] add failed :(\n");
		if (res) {
			debug_print("[record_add (sql)] reason: %s\n",
					PQresultErrorMessage(res) );
			PQclear(res);
		}
		return -1;
	}
	PQclear(res);
	
	r->id = new_id;
	
	return 0;
#else /* ! USE_SQL */
	int i;
	struct location *loc;
	static int last_found_empty = 0;

	if ( last_found_empty > t->max_records ) last_found_empty = 0;

	if ( check == TRUE && record_check(t, r) == FALSE ) {
#ifdef DEBUG_GABY
		debug_print("[ra] the record failed the check\n");
#endif
		return -1;
	}

	if ( t->max_records % OPTIMUM_NUMBER == 0 ) {
		t->records = g_realloc(t->records, sizeof(record*)* 
			((t->max_records/OPTIMUM_NUMBER)+1)*OPTIMUM_NUMBER);
#if 1
	/* I don't know which is better so I kept the old one that I know is
	 * working. The later _looks_ faster but it didn't show a real
	 * improvement (gain less than 5ms :) ) after a few tests */
		for ( i=0; i < OPTIMUM_NUMBER; i++) {
			t->records[ ((t->max_records/OPTIMUM_NUMBER))* 
					OPTIMUM_NUMBER+i ] = NULL;
		}
#else
		memset( t->records + OPTIMUM_NUMBER * 
					( t->max_records / OPTIMUM_NUMBER ) , 
				0, sizeof(record*) * OPTIMUM_NUMBER);
#endif
	}

	if ( r->id == 0 ) {
		loc = (g_list_first(t->locations))->data;
		r->id = (loc->max_index++) + loc->offset;
	}
	
	if ( r->file_loc == NULL) {
		/* I think that if r->id == 0 then r->file_loc == NULL and that
		 * the reverse is also true - if confirmed I'll be able to wipe
		 * an 'if' */
		loc = (g_list_first(t->locations))->data;
		r->file_loc = loc;
	}

	/* loading a file will lead to lot of records without holes so
	 * we don't try to fill them when loading */
	i = ( loading ) ? last_found_empty : 0;
	while ( t->records[i] != NULL && t->records[i]->id != 0 ) i++;
	
	t->records[i] = r;
	last_found_empty = i;
	
	if ( i==t->max_records )
		t->max_records++;

	t->nb_records++;
	
	if ( ! loading ) {
		/* half the time spent loading a file was consumed
		 * in those two lines */
#  ifndef INDEXES_ARE_ARRAYS
		g_list_insert_sorted(t->indexes[0], r, 
					(GCompareFunc)index_sort_id);
		t->indexes[0] = g_list_first(t->indexes[0]);

		for ( i=1; i<t->nb_fields+1; i++ ) {
			if ( t->indexes[i] == NULL ) continue;

			gl_no_field = i-1;

			switch ( t->fields[gl_no_field].type ) {
				case T_STRING:
				case T_STRINGS:
				case T_MULTIMEDIA:
				case T_FILE:
				{
				g_list_insert_sorted(t->indexes[i], r, 
					(GCompareFunc)index_sort_str);
				} break;
				case T_DATE:
				{
				g_list_insert_sorted(t->indexes[i], r, 
						(GCompareFunc)g_date_compare);
				} break;
				case T_INTEGER:
				case T_DECIMAL:
				{
				g_list_insert_sorted(t->indexes[i], r, 
						(GCompareFunc)index_sort_int);
				} break;
				case T_REAL:
				{
				g_list_insert_sorted(t->indexes[i], r, 
						(GCompareFunc)index_sort_real);
				} break;
				default:
				{
				g_list_append(t->indexes[i], r);
				} break;
			}
		}
#  else /* INDEXES_ARE_ARRAYS */
		/* let's add this new record's id to indexes */;
	
		
		for ( i=0; i<t->nb_fields; i++ ) {
			if ( t->indexes[i] != NULL || i== 0) {
				t->indexes[i] = g_realloc(t->indexes[i], 
						t->nb_records * sizeof(int*));
			}
		}
		t->indexes[0][t->nb_records-1] = last_found_empty;
				
		for ( i=1; i<t->nb_fields+1; i++ ) {
			int p;
			
			if ( t->indexes[i] == NULL ) continue;
			p = index_find(t, i-1, r, FALSE);
			memmove(t->indexes[i] + p + 1, t->indexes[i] + p, 
					sizeof(int) * p );
			t->indexes[i][p] = last_found_empty;
		}

#  endif /* INDEXES_ARE_ARRAYS */
		t->updated = TRUE;
		debug_print("[record_add] table %s marked as updated\n",
				t->name );
	}

	return r->id;
#endif /* ! USE_SQL */
}

/**
 * record_free
 * @t: table
 * @r: record to free
 *
 * Description:
 * This function frees memory allocated for the record @r's fields.
 **/
void record_free(table *t, record *r)
{
	int j;

	for ( j=0; j<t->nb_fields; j++) {
		switch ( t->fields[j].type ) {
			case T_STRING:
			case T_STRINGS:
			case T_MULTIMEDIA:
			case T_FILE:
			{
				if ( r->cont[j].str ) {
					g_string_free(r->cont[j].str,1);
					r->cont[j].str = NULL;
				}
			} break;
			case T_DATE:
			{
				if ( r->cont[j].date ) {
					g_date_free(r->cont[j].date);
					r->cont[j].date = NULL;
				}
			} break;
			case T_INTEGER:
			case T_DECIMAL:
			case T_REAL:
			case T_BOOLEAN:
			case T_RECORD:
			case T_RECORDS:
				{ ; } break;
		}
	}
	g_free(r->cont);
	g_free(r);
}


/**
 * record_remove_id
 * @t: table in which you want to remove a record
 * @id: id of the record you want to remove
 *
 * Description:
 * This function delete the record with the id @id from the table @t.
 *
 * Returns: TRUE if it worked, FALSE if it didn't.
 **/
gboolean record_remove_id(table *t, int id)
{
#ifdef USE_SQL
	char sqlcmd[100];
	PGresult *res;

	g_snprintf(sqlcmd, 100, "DELETE FROM %s WHERE id = %d", t->name, id);
	res = PQexec(sqlconn, sqlcmd);
	if (res == NULL || PQresultStatus(res) != PGRES_TUPLES_OK) {
		debug_print("[record_remove_id (sql)] sql error (?)\n");
		if (res)
			PQclear(res);
		return FALSE;
	}
	PQclear(res);

	return TRUE;
#else /* ! USE_SQL */
	/* TODO (post-2.0, not really important): speed up record_remove_id
	 * it could use sth similar to Python fast_record_ids
	 * actually this code should/could be moved to record_remove 
	 * and locating the record would be done by get_record_no() */
	int i, j;
	record *r;

	for (i=0; i<t->max_records; i++) {
		if ( id == t->records[i]->id ) {
			r = t->records[i];
#ifdef INDEXES_ARE_ARRAYS
			/* remove record's id from indexes */
			for ( i=0; i<t->nb_fields+1; i++ ) {
				int p;
			
				if ( t->indexes[i] == NULL ) continue;
				p = index_find(t, i-1, r, FALSE);
#ifdef DEBUG_GABY
				debug_print("memmove to %p from %p of %d bytes\n",
						t->indexes[i] + p,
						t->indexes[i] + p+1,
						sizeof(int) * (t->nb_records-p-1));
#endif
				memmove( t->indexes[i] + p , 
					t->indexes[i] + p+1, 
					sizeof(int) * (t->nb_records-p-1) );
			}
			for ( i=0; i<t->nb_fields; i++ ) {
				if ( t->indexes[i] == NULL ) continue;
				t->indexes[i] = g_realloc(t->indexes[i], 
						t->nb_records * sizeof(int*));
			}
#endif

			r->id = 0;
#if 1

			debug_print("record: %p\n", r);
			for ( j=0; j < t->nb_fields; j++ ) {
				debug_print("r->cont[%d].data: %p\n", j, 
						r->cont[j].anything );
				switch ( t->fields[j].type ) {
					case T_STRING:
					case T_STRINGS:
					case T_MULTIMEDIA:
					case T_FILE:
					{
						debug_print("%s\n", 
							r->cont[j].str->str );
						if ( r->cont[j].str ) {
							g_string_free(r->cont[j].str,1);
							r->cont[j].str = NULL;
						}
					} break;
					case T_DATE:
					{
						if ( r->cont[j].date ) {
							g_date_free(r->cont[j].date);
							r->cont[j].date = NULL;
						}
					} break;
					case T_INTEGER:
					case T_DECIMAL:
					case T_REAL:
					case T_BOOLEAN:
					case T_RECORD:
					case T_RECORDS:
					{ ; } break;
				}
			}
			g_free(r->cont);
			r->cont = NULL;
#else /* ! 1 */
			record_free(t, r);
#endif /* ! 1 */
			/* TODO (post-2.0): scan for T_RECORDS referring this
			 * record
			 * the value could be replaced by 0 (understood as
			 * 'none') or deleted. Customizable ?
			 * Delayed since referential integrity is not a goal
			 * for now...
			 */

			t->nb_records--;
			t->updated = TRUE;

			return TRUE;
		}
	}
	return FALSE;
#endif /* ! USE_SQL */
}

/**
 * record_remove
 * @t: table in which you want to remove a record
 * @r: record you want to remove
 *
 * Description:
 * This function delete the record @r from the table @t.
 *
 * Returns: TRUE if this works, FALSE if it doesn't.
 **/
gboolean record_remove(table *t, record *r)
{
	/* is this really efficient ? (answer: no :) ) */
	return (record_remove_id(t, r->id));
}

/**
 * record_modify
 * @t: table in which you want to modify a record
 * @r: record with the new values
 *
 * Description:
 * This functions modifies a record in the table @t according to the record @r
 * (its id and its contents)
 *
 * Returns: TRUE if ok
 */
gboolean record_modify(table *t, record *r)
{
#ifdef USE_SQL
	PGresult *res;
	char sqlstr[2000]; /* TODO (post-2.0): no fixed length strings !!!
			    * delayed since sql won't be advertized */
	char tmp[200];
	int i;

	debug_print("[record_modify] --\n");
	g_snprintf(sqlstr, 2000, "SELECT * from %s WHERE id = %d", t->name, r->id);
	res = PQexec(sqlconn, sqlstr);

	if (res == NULL || PQresultStatus(res) != PGRES_TUPLES_OK) {
		/* should I deal differently in case of error and in case of
		 * no result ? */
		debug_print("SELECT command didn't return tuples properly\n");
		if (res)
			PQclear(res);
		return FALSE;
	}

	if ( PQntuples(res) == 0 ) {
		PQclear(res);
		return FALSE;
	}

	g_snprintf(sqlstr, 2000, "UPDATE %s SET ", t->name);
	for ( i=0; i<t->nb_fields; i++) {
		GString *str = get_table_stringed_field(t, r, i);
		g_snprintf(tmp, 200, "%s = \'%s\', ", PQfname(res, i+1), str->str);
		g_string_free(str, 1);
		strcat(sqlstr, tmp);
	}
	sqlstr[strlen(sqlstr)-2] = ' '; /* removing ',' */
	g_snprintf(tmp, 200, "WHERE id = %d", r->id);
	strcat(sqlstr, tmp);

	PQclear(res);

	res = PQexec(sqlconn, sqlstr);
	if (res == NULL || PQresultStatus(res) != PGRES_COMMAND_OK) {
		/* should I deal differently in case of error and in case of
		 * no result ? */
		debug_print("UPDATE command failed\n");
		if (res)
			PQclear(res);
		return FALSE;
	}
	PQclear(res);

	return TRUE;
#else /* ! USE_SQL */
	int i, j;
	gboolean one=FALSE;
	record *oldr;

	for (i=0; i<t->max_records; i++) {
		if ( r->id == t->records[i]->id ) {
			one = TRUE;
			break;
		}
	}
	if ( one == FALSE ) {
		debug_print("[record_modify] id (%d) doesn't exist !\n", r->id);
		return FALSE;
	}

	debug_print("[record_modify] -- 1\n");
	
	/* let's free the old 'cont' */
	oldr = t->records[i];
	for ( j=0; j < t->nb_fields; j++ ) {
		switch ( t->fields[j].type ) {
			case T_STRING:
			case T_STRINGS:
			case T_MULTIMEDIA:
			case T_FILE:
			{
				if ( oldr->cont[j].str ) {
					g_string_free(oldr->cont[j].str,1);
					oldr->cont[j].str = NULL;
				}
			} break;
			case T_DATE:
			{
				if ( oldr->cont[j].date ) {
					g_date_free(oldr->cont[j].date);
					oldr->cont[j].date = NULL;
				}
			} break;
			case T_INTEGER:
			case T_REAL:
			case T_DECIMAL:
			case T_BOOLEAN:
			case T_RECORD:
			case T_RECORDS:
			{ ; } break;
		}
	}
	g_free(oldr->cont);
	oldr->cont = NULL;
	
	debug_print("[record_modify] -- 2\n");
	t->records[i]->cont = r->cont;
	t->updated = TRUE;
	
	/* TODO (post-2.0):optimize index resorting when a record is modified */
	indexes_create (t);

	return TRUE;
#endif /* ! USE_SQL */
}

/**
 * record_defaults
 * @t: table
 *
 * Description
 * This function returns a record filled with the default value for the table
 * @t.
 *
 * Returns: the record (record*)
 **/
record* record_defaults(table *t)
{
	/* it's the caller's task to g_free what is returned */
	int i;
	property *p;
	record *r = g_new0(record, 1);
	union data *d;
	
	r->cont = g_new0(union data, t->nb_fields);
	r->id = 0;
	
	for ( i=0; i<t->nb_fields; i++ ) {
		p = field_get_property(&t->fields[i], "default");
		if ( p != NULL ) {
			d = p->val;
			memcpy(&r->cont[i], d , sizeof(union data));
		} else {
			/* there are no way to just tell 'leave this field
			 * empty' - we need to force something here and this
			 * will work for strings but will put '0' for numbers
			 * (note that this isn't a problem if we use spin
			 * buttons)
			 *
			 * [what is missing is SQL 'Null' value]
			 */
			switch ( t->fields[i].type ) {
				case T_STRING:
				case T_STRINGS:
				case T_MULTIMEDIA:
				{
					r->cont[i].str = g_string_new("");
				} break;
				default:
				{
					r->cont[i].anything = NULL;
				} break;
			}
		}
	}

	return r;
}

/**
 * record_duplicate
 * @t: table
 * @r: record to duplicate
 *
 * Description
 * This function returns a new record filled with the values previously in @r
 *
 * Returns: the new record (record*)
 **/
record* record_duplicate(table *t, record *r)
{
	/* it's the caller's task to g_free what is returned */
	int i;
	record *rnew = g_new0(record, 1);
	
	rnew->cont = g_new0(union data, t->nb_fields);
	rnew->id = 0;
	
	for ( i=0; i<t->nb_fields; i++ ) {
		switch ( t->fields[i].type ) {
			case T_STRING:
			case T_STRINGS:
			case T_MULTIMEDIA:
			case T_FILE:
			{
				rnew->cont[i].str = g_string_new ( 
							r->cont[i].str->str );
			} break;
			case T_DATE:
			{
				if ( r->cont[i].date == NULL ) {
					rnew->cont[i].date = NULL;
				} else {
					rnew->cont[i].date = g_date_new();
					memcpy(rnew->cont[i].date, 
						r->cont[i].date, sizeof(GDate));
				}
			} break;
			default:
			{
				memcpy(&(rnew->cont[i].anything), 
					&(r->cont[i].anything),
					sizeof(gpointer));
			} break;
		}
	}

	return rnew;
}

/**
 * table_search_record
 * @t: concerned table
 * @nf: field number
 * @what: string to search for
 * @from: record id the search should start from
 *
 * Description:
 * This functions searchs for a record whose field @nf matches (actually starts
 * with) @what in table @t
 *
 * Returns: record position in table->records, -1 if not found
 **/
int table_search_record(table *t, int nf, char* what, int from)
{
	int i = from;
	/*union data val;*/
	field_type type = t->fields[nf].type;
	

	/*get_value_for_that_string(&val, t->fields[nf].type, what);*/
	
	for ( i=from; i<t->max_records; i++ ) {
		record *r = t->records[i];
		if ( r == NULL ) continue;
		if ( r->id == 0 ) continue;
		if ( cnd_is( r->cont[nf], what, type) )
			return i;
		if ( cnd_start_with( r->cont[nf], what, type) )
			return i;
	}
	
	return -1;
}

/**
 * subtable_search_record
 * @st: concerned subtable
 * @nf: field number
 * @what: string to search for
 * @from: record id the search should start from
 *
 * Description:
 * This functions searchs for a record whose field @nf matches (actually starts
 * with) @what in subtable @st. (this is actually a stupid wrapper for
 * table_search_record)
 *
 * Returns: record position in subtable->table->records, -1 if not found
 **/
int subtable_search_record(subtable *st, int nf, char* what, int from)
{
	return table_search_record(st->table, st->fields[nf].no, what, from);
}

#ifndef INDEXES_ARE_ARRAYS
static int index_sort_id(record *a, record *b)
{
	if ( a == NULL || b == NULL ) return 0;
	return ( a->id > b->id ) ? 1 : -1;
}

static int index_sort_str(record *a, record *b)
{
	if ( a == NULL || b == NULL ) return 0;

	return ( strcmp(	a->cont[gl_no_field].str->str, 
				b->cont[gl_no_field].str->str)
			 );	
}

static int index_sort_int(record *a, record *b)
{
	if ( a->cont[gl_no_field].i > b->cont[gl_no_field].i ) return -1;
	if ( a->cont[gl_no_field].i < b->cont[gl_no_field].i ) return  1;
	return 0;
}

static int index_sort_real(record *a, record *b)
{
	if ( a->cont[gl_no_field].d > b->cont[gl_no_field].d ) return -1;
	if ( a->cont[gl_no_field].d < b->cont[gl_no_field].d ) return  1;
	return 0;
}
#endif

#if 1
static int string_compare(void *a, void *b)
{
	struct val {
		int i; union data *d;
	};
	struct val *a1 = a;
	struct val *b1 = b;
	
	if ( ! b1->d->str ) return -(a1->d->str != NULL);
	if ( ! a1->d->str ) return 1;
	
	return ( strcmp(a1->d->str->str, b1->d->str->str));
}

static int int_compare(void *a, void *b)
{
	struct val {
		int i; union data *d;
	};
	struct val *a1 = a;
	struct val *b1 = b;
	
	if ( a1->d->i == b1->d->i ) return 0;
	if ( a1->d->i < b1->d->i ) return -1;
	return 1;
}

static int real_compare(void *a, void *b)
{
	struct val {
		int i; union data *d;
	};
	struct val *a1 = a;
	struct val *b1 = b;
	
	if ( a1->d->d == b1->d->d ) return 0;
	if ( a1->d->d < b1->d->d ) return -1;
	return 1;
}

static int date_compare(void *a, void *b)
{
	struct val {
		int i; union data *d;
	};
	struct val *a1 = a;
	struct val *b1 = b;
	
	if ( ! b1->d->date ) return -1;
	if ( ! a1->d->date ) return 1;
	
	return (g_date_compare(a1->d->date, b1->d->date));
}
#endif

#if 0 /* very old thing */
static GList* index_create(table *t, int no)
{
	int i;
	record *r;

#ifdef DEBUG_GABY
	debug_print("[index_create] start : %d\n", (int)time(NULL));
#endif

	if ( t->indexes[no+1] != NULL ) return t->indexes[no+1];
	t->indexes[no+1] = g_list_alloc();
	t->indexes[no+1]->data = NULL;
	
	gl_no_field = no;
	
	for ( i=0; i<t->max_records; i++) {
		r = t->records[i];
		if ( r==NULL || r->id == 0 )
			continue;
		
		switch ( t->fields[gl_no_field].type ) {
			case T_STRING:
			case T_STRINGS:
			{
				g_list_insert_sorted(t->indexes[no+1], r, 
					(GCompareFunc)index_sort_str);
			} break;
			case T_DATE:
			{
				g_list_insert_sorted(t->indexes[no+1], r, 
					(GCompareFunc)g_date_compare);
			} break;
			case T_INTEGER:
			{
				g_list_insert_sorted(t->indexes[no+1], r, 
					(GCompareFunc)index_sort_int);
			} break;
			case T_REAL:
			{
				g_list_insert_sorted(t->indexes[no+1], r, 
					(GCompareFunc)index_sort_real);
			} break;
			default:
			{
				g_list_append(t->indexes[no+1], r);
			}
		}
/*		g_list_insert_sorted(t->indexes[no+1], r, 
				(GCompareFunc)index_sort_str);	*/
		t->indexes[no+1] = g_list_first(t->indexes[no+1]);
	}
}
#endif

#ifdef INDEXES_ARE_ARRAYS
static int* index_create (table *t, int no)
#else
static GList* index_create(table *t, int no)
#endif
{
	/* note: no ranges from 0 (id) to nb_fields+1 */
	struct {
		int i;
		union data *d;
	} ids[t->max_records];
	int i, j=0;
	record *r;
	field_type type;

#ifdef DEBUG_GABY
	debug_print("[index_create] start : %d\n", (int)time(NULL));
#endif

	/* since load_file no longer updates the indexes, this line has to be
	 * removed ... */
#if 0
	if ( t->indexes[no+1] != NULL ) return t->indexes[no+1];
#endif
	
	for ( i=0; i<t->max_records; i++) {
		r = t->records[i];
		if ( r==NULL || r->id == 0 )
			continue;
		ids[j].i = i;
		if ( no == 0 ) {
			ids[j].d = (union data*)(&r->id);
		} else {
			ids[j].d = &r->cont[no-1];
		}
		j++;
	}

#ifdef DEBUG_GABY
	debug_print("[index_create] t->nb_records:%d, j:%d\n", 
				t->nb_records, j );
#endif

	if ( no == 0 ) {
		type = T_INTEGER;
	} else {
		type = t->fields[no-1].type;
	}

	switch ( type ) {
		case T_STRING:
		case T_STRINGS:
		case T_MULTIMEDIA:
		case T_FILE:
		{
			qsort(ids, (size_t)j, sizeof(ids[0]), 
						(int (*)())string_compare);
		} break;
		case T_INTEGER:
		case T_RECORD:
		case T_BOOLEAN:
		case T_DECIMAL:
		{
			qsort(ids, (size_t)j, sizeof(ids[0]), 
						(int (*)())int_compare);
		} break;
		case T_REAL:
		{
			qsort(ids, (size_t)j, sizeof(ids[0]), 
						(int (*)())real_compare);
		} break;
		case T_DATE:
		{
			qsort(ids, (size_t)j, sizeof(ids[0]), 
						(int (*)())date_compare);
		} break;
		case T_RECORDS: {/* not possible in table */} break;
	}

#ifndef INDEXES_ARE_ARRAYS
	for (i=j-1; i>=0; i--) {
		t->indexes[no] = g_list_prepend(t->indexes[no], 
							t->records[ids[i].i]);
	}
	t->indexes[no] = g_list_first(t->indexes[no]);
#else
	if ( t->indexes[no] != NULL ) g_free(t->indexes[no]);
	t->indexes[no] = g_new0(int, t->nb_records);
	for (i=0; i<t->nb_records; i++) {
		t->indexes[no][i] = ids[i].i;
	}
#endif
	
#ifdef DEBUG_GABY
	debug_print("[index_create] end : %d\n", (int)time(NULL));
#endif
	return t->indexes[no];
}

static void indexes_create (table *t)
{
	int i;

	for ( i=0; i<t->nb_fields+1; i++ ) {
		if ( i != 0 && t->indexes[i] == NULL ) continue;
		index_create(t, i);
	}
}

#ifdef INDEXES_ARE_ARRAYS
/*
 * index_find
 * @t: table
 * @num_field: field number (-1 for id)
 * @r: record to find
 * @exact: whether a record with the same value will be accepted (or not)
 */
static int index_find ( table *t, int num_field, record *r, gboolean exact )
{
	int (*compar) (void *, void *) = NULL;
	int *index = t->indexes[num_field+1];
	union data *a, b, b2;
	int bottom, top, tested;
	record *r2;
	field_type type;
	struct { int i; union data *d; } val1, val2;
	int retval;

/*	if ( t->nb_records == 1 ) return index[0];*/

	if ( index == NULL ) {
		index = index_create(t, num_field+1);
	}

	val1.i = 0;
	val2.i = 0;
		
	if ( num_field == -1 ) {
		b.i = r->id;
		a = &b;
	} else {
		a = &(r->cont[num_field]);
	}
	val1.d = a;
	
	if ( num_field == -1 ) {
		type = T_INTEGER;
	} else {
		type = t->fields[num_field].type;
	}

	switch ( type ) {
		case T_STRING:
		case T_STRINGS:
		case T_MULTIMEDIA:
		case T_FILE:
		{
			compar = string_compare;
		} break;
		case T_INTEGER:
		case T_RECORD:
		case T_BOOLEAN:
		case T_DECIMAL:
		{
			compar = int_compare;
		} break;
		case T_REAL:
		{
			compar = real_compare;
		} break;
		case T_DATE:
		{
			compar = date_compare;
		} break;
		case T_RECORDS:
		{ /* this never happens but this removes compiler complaints
		   * about T_RECORDS not handled in switch */
			compar = NULL;
		} break;
	}
		
	bottom = 0; top = t->nb_records;
	tested = (top-bottom)/2;

#ifdef DEBUG_GABY
	debug_print("[index_find] go!\n");
#endif
	while ( (top-bottom) > 1 ) {
#ifdef DEBUG_GABY
/*		debug_print("bottom : %d, top : %d\n", bottom, top ); */
#endif
		r2 = t->records[index[tested]];
		if ( num_field == -1 ) {
			b2.i = r2->id;
			val2.d = &b2;
		} else {
			val2.d = &(r2->cont[num_field]);
		}
		if ( compar(&val1, &val2) > 0 ) { /* a > a2 */
			bottom = tested;
		} else {
			top = tested;
		}
		tested = bottom+(top-bottom)/2;
	}

/*	return ( (compar(a, a2) < 0) ? tested+1 : tested ); */

	r2 = t->records[index[tested]];
	if ( num_field == -1 ) {
		b2.i = r2->id;
		val2.d = &b2;
	} else {
		val2.d = &(r2->cont[num_field]);
	}
	if ( compar(&val1, &val2) > 0 ) { /* a > a2 */
		retval = top;
	} else {
		retval = bottom;
	}

	if ( exact == TRUE ) {
		while ( retval < t->nb_records &&
					t->records[ index[retval] ] != r ) {
			retval++;
		}
	}
	return retval;
}
#endif

/**
 * table_next
 * @t: table you are in
 * @r: record you want to leave
 * @no_field: field you're sorted on
 *
 * Description:
 * This function returns the &record directly following the &record you gave as
 * second parameter. @no_field allows you to specify a field on which the table
 * is sorted (or -1 if you don't want the table to be sorted).
 * Note that the first time you specify a field number Gaby will have to create
 * an index for this field and this may be long but this will only happens once
 * (by session).
 * Note that if you were on the last record you will stay there.
 *
 * Returns: the &record you want to go to.
 **/
record* table_next(table *t, record *r, int no_field)
{
	/* nofield : -1 for id else field number */ 
#ifdef USE_SQL
	PGresult *res;
	char sqlstr[1024];
	char *id_str;

	g_snprintf(sqlstr, 1024, "SELECT * from %s WHERE id > %d ORDER BY id "
			"LIMIT 1", t->name, r->id);
	res = PQexec(sqlconn, sqlstr);

	if (res == NULL || PQresultStatus(res) != PGRES_TUPLES_OK) {
		/* should I deal differently in case of error and in case of
		 * no result ? */
		debug_print("SELECT command didn't return tuples properly\n");
		if (res)
			PQclear(res);
		return NULL;
	}

	if ( PQntuples(res) == 0 ) {
		PQclear(res);
		return table_last(t, no_field);
	}

	id_str = (char *) PQgetvalue(res, 0, 0);
	t->records[0]->id = atoi(id_str);
	get_record_no(t, atoi(id_str));

	return t->records[0];
#else /* ! USE_SQL */
#  ifndef INDEXES_ARE_ARRAYS
	GList *index;
	GList *new_index;
	
	if ( r == NULL ) {
		return table_last(t, no_field);
	}
	
	index = t->indexes[no_field+1];
	if ( index == NULL ) {
		index = index_create(t, no_field+1);
	}
	index = g_list_first(index);
	index = g_list_find(index, r);
	do {
		new_index = g_list_next(index);
		if ( new_index == NULL ) {
			new_index = index;
			break;
		}
		index = new_index;
	} while ( index->data != NULL && ((record*)index->data)->id == 0 );

#    ifdef DEBUG_GABY
	if ( index != NULL && index->data != NULL ) {
		debug_print("new id : %d\n", ((record*)index->data)->id);
	} else {
		debug_print("new id : -- (index->data : %p)\n", index->data);
		debug_print("index->prev : %p\n", index->prev);
	}
#     endif /* DEBUG_GABY */
	
	if ( index->data == NULL && index->prev != NULL ) {
		debug_print("I am in the if\n");
		do {
			index = g_list_previous(index);
			debug_print("I am in the loop\n");
		} while (((record*)index->data)->id == 0);
		debug_print("and the new id is ... %d !\n", ((record*)index->data)->id);
	}
	return index->data;
	
#  else /* INDEXES_ARE_ARRAYS  */
	int cur_pos, new_pos;
	int *index;

	if ( r == NULL || t->nb_records == 0 ) {
		return table_last(t, no_field);
	}
	
	cur_pos = index_find(t, no_field, r, TRUE);
	if ( cur_pos >= t->nb_records-1 ) {
		new_pos = cur_pos; /* already on the last record */
	} else {
		new_pos = cur_pos + 1;
	}
	
	index = t->indexes[no_field+1];
	
	return ( t->records[ index[new_pos] ] );
#  endif /* INDEXES_ARE_ARRAYS */
#endif /* ! USE_SQL */
}

/**
 * table_prev
 * @t: table you are in
 * @r: record you want to leave
 * @no_field: field you're sorted on
 *
 * Description:
 * This function returns the &record directly preceding the &record you gave as
 * second parameter. @no_field allows you to specify a field on which the table
 * is sorted (or -1 if you don't want the table to be sorted).
 * Note that the first time you specify a field number Gaby will have to create
 * an index for this field and this may be long but this will only happens once
 * (by session).
 * Note that if you were on the first record you will stay there.
 *
 * Returns: the &record you want to go to.
 **/
record* table_prev(table *t, record *r, int no_field)
{
	/* nofield : -1 for id else field number */ 
#ifdef USE_SQL
	PGresult *res;
	char sqlstr[1024];
	char *id_str;

	g_snprintf(sqlstr, 1024, "SELECT * from %s WHERE id < %d ORDER BY id DESC "
			"LIMIT 1", t->name, r->id);
	res = PQexec(sqlconn, sqlstr);

	if (res == NULL || PQresultStatus(res) != PGRES_TUPLES_OK) {
		/* should I deal differently in case of error and in case of
		 * no result ? */
		debug_print("SELECT command didn't return tuples properly\n");
		if (res)
			PQclear(res);
		return NULL;
	}

	if ( PQntuples(res) == 0 ) {
		PQclear(res);
		return table_first(t, no_field);
	}

	id_str = (char *) PQgetvalue(res, 0, 0);
	t->records[0]->id = atoi(id_str);
	get_record_no(t, atoi(id_str));

	return t->records[0];
#else /* ! USE_SQL */
#  ifndef INDEXES_ARE_ARRAYS
	GList *index;
	GList *new_index;
	
	if ( r == NULL || t->nb_records == 0 ) {
		return table_first(t, no_field);
	}

	index = t->indexes[no_field+1];
	if ( index == NULL )
		index = index_create(t, no_field+1);
	
	index = g_list_first(index);
	index = g_list_find(index, r);
	
	do {
		new_index = g_list_previous(index);
		if ( new_index == NULL ) {
			new_index = index;
			break;
		}
		index = new_index;
	} while ( index->data != NULL && ((record*)index->data)->id == 0 );

	if ( index->data == NULL && index->next != NULL) {
		index = g_list_next(index);
	} else {
		while (((record*)index->data)->id == 0) {
			index = g_list_next(index);
			debug_print("I am in a loop\n");
		}
	}
		
	return index->data;
#  else /* INDEXES_ARE_ARRAYS */
	int cur_pos, new_pos;
	int *index;

	if ( r == NULL ) {
		return table_first(t, no_field);
	}
	
	cur_pos = index_find(t, no_field, r, TRUE);
	if ( cur_pos == 0 ) {
		new_pos = cur_pos; /* already on the first record */
	} else {
		new_pos = cur_pos - 1;
	}
	
	index = t->indexes[no_field+1];
	
	return ( t->records[ index[new_pos] ] );

#  endif /* INDEXES_ARE_ARRAYS */
#endif /* ! USE_SQL */
}

/**
 * table_first
 * @t: table you are in
 * @no_field: field you're sorted on
 *
 * Description:
 * This function returns the first &record of the table @t. @no_field allows
 * you to specify a field on which the table is sorted (or -1 if you don't
 * want the table to be sorted).
 * Note that the first time you specify a field number Gaby will have to create
 * an index for this field and this may be long but this will only happens once
 * (by session).
 * Note that if you were on the first record you will stay there.
 *
 * Returns: the &record you want to go to.
 **/
record* table_first(table *t, int no_field)
{
#ifdef USE_SQL
	PGresult *res;
	char sqlstr[1024];
	char *id_str;

	g_snprintf(sqlstr, 1024, "SELECT * from %s ORDER BY id LIMIT 1 ", t->name);
	res = PQexec(sqlconn, sqlstr);

	if (res == NULL || PQresultStatus(res) != PGRES_TUPLES_OK) {
		fprintf(stderr, "SELECT command didn't return tuples properly\n");
		if (res)
			PQclear(res);
		return NULL;
	}

	if ( PQntuples(res) == 0 ) {
		PQclear(res);
		return NULL;
	}

	id_str = (char *) PQgetvalue(res, 0, 0);
	t->records[0]->id = atoi(id_str);
	get_record_no(t, atoi(id_str));

	return t->records[0];

#else /* ! USE_SQL */
#  ifndef INDEXES_ARE_ARRAYS
	GList *index;
	GList *new_index;

	index = t->indexes[no_field+1];
	if ( index == NULL )
		index = index_create(t, no_field+1);
	
	index = g_list_first(index);
	while ( index->data != NULL && ((record*)index->data)->id == 0 ) {
		new_index = g_list_next(index);
		if ( new_index == NULL ) {
			new_index = index;
			break;
		}
		index = new_index;
	}
	
	if ( index->data == NULL ) return NULL;

	return (record *)(index->data);
#  else /* INDEXES_ARE_ARRAY */
	int *index;

	if ( t->nb_records == 0 ) return NULL;

	index = t->indexes[no_field+1];
	if ( index == NULL )
		index = index_create(t, no_field+1);
	
	return ( t->records[ index[0] ] );
#  endif /* INDEXES_ARE_ARRAY */
#endif /* ! USE_SQL */
}

/**
 * table_last
 * @t: table you are in
 * @no_field: field you're sorted on
 *
 * Description:
 * This function returns the last &record of the table @t. @no_field allows
 * you to specify a field on which the table is sorted (or -1 if you don't
 * want the table to be sorted).
 * Note that the first time you specify a field number Gaby will have to create
 * an index for this field and this may be long but this will only happens once
 * (by session).
 * Note that if you were on the last record you will stay there.
 *
 * Returns: the &record you want to go to.
 **/
record* table_last(table *t, int no_field)
{
#ifdef USE_SQL
	PGresult *res;
	char sqlstr[1024];
	char *id_str;

	g_snprintf(sqlstr, 1024, "SELECT * from %s ORDER BY id DESC LIMIT 1 ", t->name);
	res = PQexec(sqlconn, sqlstr);

	if (res == NULL || PQresultStatus(res) != PGRES_TUPLES_OK) {
		fprintf(stderr, "SELECT command didn't return tuples properly\n");
		if (res)
			PQclear(res);
		return NULL;
	}

	if ( PQntuples(res) == 0 ) {
		PQclear(res);
		return NULL;
	}

	id_str = (char *) PQgetvalue(res, 0, 0);
	t->records[0]->id = atoi(id_str);
	get_record_no(t, atoi(id_str));

	return t->records[0];
#else /* ! USE_SQL */
#  ifndef INDEXES_ARE_ARRAYS
	GList *index;
	GList *new_index;

	index = t->indexes[no_field+1];
	if ( index == NULL ) {
		index = index_create(t, no_field+1);
	}
	
	index = g_list_last(index);
	while ( index->data == NULL || ((record*)index->data)->id == 0 ) {
		new_index = g_list_previous(index);
		if ( new_index == NULL ) {
			new_index = index;
			break;
		}
		index = new_index;
	}
	
	if ( index->data == NULL ) return NULL;

	return (record *)(index->data);

#  else /* INDEXES_ARE_ARRAYS */
	int *index;

	if ( t->nb_records == 0 ) return NULL;

	index = t->indexes[no_field+1];
	if ( index == NULL )
		index = index_create(t, no_field+1);
	
	return ( t->records[ index[t->nb_records-1] ] );
#  endif /* INDEXES_ARE_ARRAYS */
#endif /* ! USE_SQL */
}

/**
 * table_next_with_conditions
 * @t: table you are in
 * @r: record you want to leave
 * @no_field: field you're sorted on
 * @c: condition(s) the record has to fulfill
 *
 * Description:
 * same as table_next but only stops on records fulfilling the given
 * conditions
 *
 * Returns: the &record you want to go to.
 **/
record* table_next_with_conditions( table *t, record *r, int no_field,
				    condition *c)
{
	record *r_new, *r_old;
	if ( c == NULL ) return table_next(t, r, no_field);
	
	r_new = r;

	do {
		r_old = r_new;
		r_new = table_next(t, r_new, no_field);
		if ( record_meets_condition(t, r_new, c) )
			return r_new;
	} while ( r_old != r_new );

	
	return table_last_with_conditions(t, no_field, c);
	
}
	
/**
 * table_prev_with_conditions
 * @t: table you are in
 * @r: record you want to leave
 * @no_field: field you're sorted on
 * @c: condition(s) the record has to fulfill
 *
 * Description:
 * same as table_prev but only stops on records fulfilling the given
 * conditions
 *
 * Returns: the &record you want to go to.
 **/
record* table_prev_with_conditions( table *t, record *r, int no_field,
				    condition *c)
{
	record *r_new, *r_old;
	if ( c == NULL ) return table_next(t, r, no_field);
	
	r_new = r;

	do {
		r_old = r_new;
		r_new = table_prev(t, r_new, no_field);
		if ( record_meets_condition(t, r_new, c) )
			return r_new;
	} while ( r_old != r_new );

	
	return table_first_with_conditions(t, no_field, c);
}

/**
 * table_first_with_conditions
 * @t: table you are in
 * @no_field: field you're sorted on
 * @c: condition(s) the record has to fulfill
 *
 * Description:
 * same as table_first but goes to the first record fulfilling the given
 * conditions
 *
 * Returns: the &record you want to go to.
 **/
record* table_first_with_conditions( table *t, int no_field, condition *c)
{
	record *r_new, *r_old;
	if ( c == NULL ) return table_first(t, no_field);
	
	r_new = table_first(t, no_field);
	if ( r_new == NULL ) return NULL;
	
	do {
		if ( record_meets_condition(t, r_new, c) )
			return r_new;
		r_old = r_new;
		r_new = table_next(t, r_new, no_field);
	} while ( r_old != r_new );

	
	return NULL;
	
}

/**
 * table_last_with_conditions
 * @t: table you are in
 * @no_field: field you're sorted on
 * @c: condition(s) the record has to fulfill
 *
 * Description:
 * same as table_last but goes to the last record fulfilling the given
 * conditions
 *
 * Returns: the &record you want to go to.
 **/
record* table_last_with_conditions( table *t, int no_field, condition *c)
{
	record *r_new, *r_old;
	if ( c == NULL ) return table_last(t, no_field);
	
	r_new = table_last(t, no_field);
	if ( r_new == NULL ) return NULL;
	
	do {
		if ( record_meets_condition(t, r_new, c) )
			return r_new;
		r_old = r_new;
		r_new = table_prev(t, r_new, no_field);
	} while ( r_old != r_new );

	return NULL;
	
}

/**
 * table_get_records_count
 * @t: table
 *
 * Description:
 * This function counts the number of records in a given table.
 **/
int table_get_records_count(table *t)
{
#ifdef USE_SQL
	PGresult *res;
	char sqlstr[1024];
	char *id_str;

	g_snprintf(sqlstr, 1024, "SELECT count(*) from %s", t->name);
	res = PQexec(sqlconn, sqlstr);

	if (res == NULL || PQresultStatus(res) != PGRES_TUPLES_OK) {
		fprintf(stderr, "SELECT command didn't return tuples properly\n");
		if (res)
			PQclear(res);
		return 0;
	}

	id_str = (char *) PQgetvalue(res, 0, 0);
	return atoi(id_str);
#else
	return t->nb_records;
#endif
}

gabycursor* cursor_declare(table *t)
{
#ifdef USE_SQL
	gabycursor *cs;
	PGresult *res;
	char sqlcmd[200];
	
	res = PQexec(sqlconn, "BEGIN");
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		PQclear(res);
		return NULL;
	}
	PQclear(res);

	g_snprintf(sqlcmd, 200, "DECLARE mycursor CURSOR FOR select * from %s",t->name);
	res = PQexec(sqlconn, sqlcmd);
	if (res == NULL || PQresultStatus(res) != PGRES_COMMAND_OK) {
		if (res)
			PQclear(res);
		return NULL;
	}
	PQclear(res);
	
	cs = g_new0(gabycursor, 1);
	cs->table = t;
	cs->position = -1;
	
	return cs;
#else /* ! USE_SQL */
	gabycursor *cs = g_new0(gabycursor, 1);
	cs->table = t;
	cs->position = -1;
	return cs;
#endif /* ! USE_SQL */
}

#ifdef USE_SQL
void create_record_from_sql_res(table *t, PGresult *res, int n)
{
	int i;
	
	for ( i=0; i<t->nb_fields; i++) {
		char *str = (char*) PQgetvalue(res, n, i+1);
		switch ( t->fields[i].type ) {
			case T_DATE:
			{
				debug_print("date field received: %s\n", str);
			} break;
			default:
			{
				;
			}
		}
		set_table_stringed_field(t, t->records[0], i, str);
	}

}
#endif /* USE_SQL */

record* cursor_get_first(gabycursor *cs)
{
#ifdef USE_SQL
	PGresult *res;

	res = PQexec(sqlconn, "FETCH ALL in mycursor");
	if (res == NULL || PQresultStatus(res) != PGRES_TUPLES_OK) {
		if ( res ) PQclear(res);
		return NULL;
	}
	
	cs->sqlres = res;
	cs->position = 0;
	cs->number = PQntuples(res);

	create_record_from_sql_res(cs->table, cs->sqlres, 0);

	return cs->table->records[0];

#else /* ! USE_SQL */
	table *t = cs->table;
	int i = 0;

	while ( i < t->max_records && (t->records[i] == NULL ||
					t->records[i]->id == 0) ) i++;
	cs->position = i;
	if ( i >= t->max_records ) return NULL;
	
	return t->records[i];
#endif /* ! USE_SQL */
}

record* cursor_get_next(gabycursor *cs)
{
#ifdef USE_SQL
	cs->position++;
	if ( cs->position == cs->number ) return NULL;

	create_record_from_sql_res(cs->table, cs->sqlres, cs->position);

	return cs->table->records[0];
#else /* ! USE_SQL */
	int i;
	table *t = cs->table;
	
	i = cs->position + 1;
	
	while ( i < t->max_records && (t->records[i] == NULL ||
					t->records[i]->id == 0) ) i++;
	cs->position = i;
	if ( i >= t->max_records ) return NULL;
	
	return t->records[i];
#endif /* ! USE_SQL */
}

void cursor_free(gabycursor *cs)
{
#ifdef USE_SQL
	PGresult *res;

	res = PQexec(sqlconn, "CLOSE mycursor");
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		PQclear(res);
		return;
	}
	PQclear(res);
	
	res = PQexec(sqlconn, "COMMIT");
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		PQclear(res);
		return;
	}
	PQclear(res);
	
#else /* ! USE_SQL */
	g_free(cs);
#endif /* ! USE_SQL */
}

/* loading and saving functions (previously known as f_tables.c) */

void table_maxindex(table *t)
{
	int i;
	record *r;
	struct location *l;
		
	for (i=0; i<t->max_records; i++ ) {
		r = t->records[i];
		l = r->file_loc;
		if ( r != NULL && r->id > (l->max_index%(l->offset+(1<<16)))) {
			r->file_loc->max_index = r->id;
		}
	}
}

gboolean table_load_file(table *t)
{
	GList *locations = g_list_first(t->locations);
	struct location *loc;
#ifndef FOLLOW_MIGUEL
	GModule *handle;
	gboolean (*mload_file) (struct location *loc);
	char plugin_name[PATH_MAX];
	char message[1000];
	char fct[100];
#endif
	
	while ( locations != NULL ) {
		loc = locations->data;
		locations = g_list_next(locations);
		if ( loc->disabled == TRUE ) continue;
#ifdef FOLLOW_MIGUEL
#include "py_lf.c"
#else
		g_snprintf(plugin_name, PATH_MAX, PLUGINS_DIR "/formats/lib%s" SO_EXTENSION,
				(loc->type == NULL) ? "gaby" : loc->type);
		handle = g_module_open(plugin_name, 0);
		if ( handle == NULL ) {
			gaby_errno = CUSTOM_ERROR;
			g_snprintf(message, 1000, _("Couldn't load %s format plug-in. (%s)"), loc->type, g_module_error() );
			gaby_message = g_strdup(message);
			gaby_perror_in_a_box();
			continue;
		}
		mload_file = NULL;
		g_module_symbol(handle, "load_file", (gpointer)&mload_file);
		if ( mload_file == NULL ) {
			g_snprintf(fct, 100, "%s_load_file", 
				(loc->type == NULL) ? "gaby" : loc->type);
			g_module_symbol(handle, fct, (gpointer)&mload_file);
		}
			
		if ( mload_file == NULL ) {
			g_module_close(handle);
			gaby_errno = CUSTOM_ERROR;
			g_snprintf(message, 1000, _("Couldn't load %s format plug-in. (loading function not found inside)"), loc->type );
			gaby_perror_in_a_box();
			continue;
		}
		
		mload_file(loc);
		
		g_module_close(handle);
#endif /* ! FOLLOW_MIGUEL */

		/* create a timeout so the file is reloaded every x minutes */

		/* we could save the tag in loc->reread but it would
		 * be uglier */

#if 0 /* TODO (post-2.0): re-enable loc->reread
       * delayed since nobody use it
       */
		/* disabled since it relies on a widget-dependant function */
		if ( loc->reread != 0 ) {
			loc->timeout_tag = gtk_timeout_add(
					1000*60*loc->reread,
					(GtkFunction)reread_cb, loc);
		}
#endif
	}

	table_maxindex(t);
	indexes_create(t);
	t->updated = FALSE;

	return TRUE;
}

void tables_load()
{
	GList *l = list_tables;
#ifndef USE_SQL
	g_list_foreach(l, (GFunc)table_load_file, NULL);
#else /* USE_SQL */

	sqlconn = PQsetdb(NULL, NULL, NULL, NULL, appname);
	if (PQstatus(sqlconn) == CONNECTION_BAD)
	{
		fprintf(stderr, "Connection to database failed.\n");
		fprintf(stderr, "%s", PQerrorMessage(sqlconn));
		PQfinish(sqlconn);
	}

	while ( l ) {
		table *t = l->data;
		l = g_list_next(l);
		t->max_records = -1; /* indicates you have to use 
					table_get_records_count to get
					the information */
		t->records = g_new0(record*, 1);
		t->records[0] = g_new0(record, 1);
		t->records[0]->cont = g_new0(union data, t->nb_fields);
	}

#endif /* USE_SQL */
}

gboolean table_save_file(table *t)
{
	GList *locations;
	struct location *loc;
	GModule *handle;
	gboolean (*msave_file) (struct location *loc);
	char plugin_name[PATH_MAX];
	char message[1000];
	char fct[100];
	
#ifdef DEBUG_GABY
	debug_print("[table_save_file] Saving %s\n", t->name);
#endif
	if ( t->updated == FALSE ) {
#ifdef DEBUG_GABY
		debug_print("[table_save_file] considered up-to-date; skipped.\n");
#endif
		return TRUE;
	}

	locations = g_list_first(t->locations);
	
	while ( locations != NULL ) {
		loc = locations->data;
		locations = g_list_next(locations);

		if ( loc->disabled == TRUE ) continue;

		if ( loc->readonly == TRUE ) {
#ifdef DEBUG_GABY
			debug_print("%s is marked as read-only - skipping\n", 
					loc->filename );
#endif
			continue;
		}

#ifdef FOLLOW_MIGUEL
#include "py_sf.c"
#endif
		
		g_snprintf(plugin_name, PATH_MAX, PLUGINS_DIR "/formats/lib%s" SO_EXTENSION,
				(loc->type == NULL) ? "gaby" : loc->type);
		handle = g_module_open(plugin_name, 0);
		if ( handle == NULL ) {
			gaby_errno = CUSTOM_ERROR;
			g_snprintf(message, 1000, _("Couldn't load %s format plug-in. (%s)"), loc->type, g_module_error() );
			gaby_message = g_strdup(message);
			gaby_perror_in_a_box();
			continue;
		}
		msave_file = NULL;
		g_module_symbol(handle, "save_file", (gpointer)&msave_file);
		if ( msave_file == NULL ) {
			g_snprintf(fct, 100, "%s_save_file", 
				(loc->type == NULL) ? "gaby" : loc->type);
			g_module_symbol(handle, fct, (gpointer)&msave_file);
		}
		if ( msave_file == NULL ) {
			g_module_close(handle);
			g_snprintf(message, 1000, _("Couldn't load %s format plug-in. (saving function not found inside)"), loc->type );
			gaby_errno = CUSTOM_ERROR;
			gaby_message = g_strdup(message);
			gaby_perror_in_a_box();
			continue;
		}

		msave_file(loc);
		
		g_module_close(handle);
	}
	
	t->updated = FALSE;

	return TRUE;
}

void tables_save()
{
	GList *list = g_list_first(list_tables);
#ifdef DEBUG_GABY
	debug_print("[table_save] l : %p\n", list);
#endif
	g_list_foreach(list, (GFunc)table_save_file, NULL);

}

gboolean table_reread_file(table *t, struct location *loc)
{
#ifndef FOLLOW_MIGUEL
	GModule *handle;
	gboolean (*mload_file) (struct location *loc);
	char plugin_name[PATH_MAX];
	char message[1000];
	char fct[100];
#endif
	int i;
	
	if ( loc->disabled == TRUE ) return TRUE;
	
	/* removing the records ... */
	for ( i=0; i<t->max_records; i++ ) {
		if ( t->records[i]->file_loc == loc ) {
			t->records[i]->id = 0;
			g_free(t->records[i]->cont);
		}
	}
	/* ... and re-reading them */

#ifdef FOLLOW_MIGUEL
#include "py_lf2.c"
#else
	
	/* this is from table_load_file, should I merge them ? */
	g_snprintf(plugin_name, PATH_MAX, PLUGINS_DIR "/formats/lib%s" SO_EXTENSION, 
				(loc->type == NULL) ? "gaby" : loc->type);
	handle = g_module_open(plugin_name, 0);
	if ( handle == NULL ) {
		/* note that this shouldn't happen. */
		gaby_errno = CUSTOM_ERROR;
		g_snprintf(message, 1000, _("Couldn't load %s format plug-in. (%s)"), loc->type, g_module_error() );
		gaby_message = g_strdup(message);
		gaby_perror_in_a_box();
		return FALSE;
	}

	mload_file = NULL;
	g_module_symbol(handle, "load_file", (gpointer)&mload_file);
	if ( mload_file == NULL ) {
		g_snprintf(fct, 100, "%s_load_file", 
				(loc->type == NULL) ? "gaby" : loc->type);
		g_module_symbol(handle, fct, (gpointer)&mload_file);
	}
	if ( mload_file == NULL ) {
		g_module_close(handle);
		gaby_errno = CUSTOM_ERROR;
		g_snprintf(message, 1000, _("Couldn't load %s format plug-in. (loading function not found inside)"), loc->type );
		gaby_perror_in_a_box();
		return FALSE;
	}

	mload_file(loc);

	g_module_close(handle);
#endif /* ! FOLLOW_MIGUEL */
	
	indexes_create(t);
	table_maxindex(t);
	t->updated = FALSE;

	return TRUE;
}

gboolean tables_modified()
{
	table *t;
	GList *list = g_list_first(list_tables);
	while ( list != NULL ) {
		t = list->data;
#ifdef DEBUG_GABY
		debug_print("[tables_modified] status for table %s is %d\n",
				t->name, t->updated);
#endif
		if ( t->updated == TRUE ) return TRUE;
		list = g_list_next(list);
	}
	return FALSE;
}

void table_free_records(table *t)
{
	int i;
	record *r;

	for ( i=0; i<t->max_records; i++) {
		r = t->records[i];
		if ( r == NULL ) continue;
		record_free(t, r);
		t->records[i] = NULL;
	}
	g_free(t->records);
	t->records = NULL;
	t->nb_records = 0;
	t->max_records = 0;
}

