/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

record* get_record_no(table *t, int id);

GString* get_subtable_record_field(subtable *st, record *riot, int field_no);
GString* get_subtable_stringed_field(subtable *st, record *r, int field_no);
GString* get_subtable_stringed_field_id(subtable *st, int id, int field_no);

GString* get_table_stringed_field(table *t, record *r, int field_no);
GString* get_table_stringed_field_id(table *t, int id, int field_no);

GList* get_related_records(st_field *sf, int id);
GList* get_conditional_records_list(subtable *st, condition *other);

gboolean record_meets_condition(table *t, record *r, condition *c);

void get_value_for_that_string(union data *val, field_type type, gchar *str);
void set_table_stringed_field(table *t, record *r, int field_no, gchar *str);
void set_subtable_stringed_field(subtable *st, record *r, int field_no,
				 gchar *str);

int id_record_field(gchar *str, subtable *st, int field_no);

/* comparison functions */
gboolean records_are_different(table *t, record *r1, record *r2);
gboolean cnd_is		(union data a, char *b, field_type type);
gboolean cnd_is_not	(union data a, char *b, field_type type);
gboolean cnd_is_greater	(union data a, char *b, field_type type);
gboolean cnd_is_less	(union data a, char *b, field_type type);
gboolean cnd_start_with	(union data a, char *b, field_type type);
gboolean cnd_has	(union data a, char *b, field_type type);
gboolean cnd_regex	(union data a, char *b, field_type type);

