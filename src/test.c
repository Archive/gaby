/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * this file could be outdated. You shouldn't use it as a reference. Thanks. *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#define APP_DECLARED
#define APPNAME_DECLARED
#define LANGUAGE_DECLARED

#include "errors.h"
#include "gaby.h"
#include "struct.h"

void *app = NULL;
char *appname;
char language[3];

GList *list_tables = NULL;
GList *list_subtables = NULL;
GList *list_views = NULL;
GList *list_actions = NULL;

#ifdef HAVE_NCURSES
#include <ncurses.h>
#include <signal.h>
#endif

#include "files.h"
#include "tables.h"
#include "records.h"

/* fake things so we can build without gtk */
GList *list_windows = NULL;

void gaby_perror_in_a_box()
{
	/*gaby_errno = NO_ERROR;*/
	if ( gaby_message ) g_free(gaby_message);
}

#ifdef FOLLOW_MIGUEL
#include "py_fake.c"
/*
GtkWidget* form_create ( struct CommonInfos *ci, view *form,
			gboolean first, ViewPluginData *vpd,
			GtkWidget *parent ) { return NULL; }
void form_fill(GtkWidget *wid) {}
GtkWidget* list_create ( struct CommonInfos *ci, view *form,
			gboolean first, ViewPluginData *vpd,
			GtkWidget *parent ) { return NULL; }
void list_fill(GtkWidget *wid) {}*/
#endif

void show_table(table *data, gpointer s)
{
	int i;
	g_print("%s\n", data->name);
	for (i=0; i < data->nb_fields; i++) {
		g_print("\t%s (%s) ", data->fields[i].name, data->fields[i].i18n_name);
		switch ( data->fields[i].type ) {
			case T_STRING: { g_print("(a string)");     } break;
			case T_INTEGER:{ g_print("(an integer)");   } break;
			case T_REAL:   { g_print("(a real)");       } break;
			case T_DECIMAL:{ g_print("(a decimal)");    } break;
			case T_DATE:   { g_print("(a date)");       } break;
			case T_RECORD: { g_print("(a record)");     } break;
			case T_RECORDS:{ g_print("(some records)"); } break;
			case T_MULTIMEDIA:{ g_print("(multimedia)");} break;
			case T_FILE:   { g_print("(a filename)");   } break;
			default:
			{
				g_print("(?)");
			} break;
		}
/*		if ( data->fields[i].type == T_RECORD || 
				data->fields[i].type == T_RECORDS ) {
			g_print(" (table %d) ", data->fields[i].more->i );
		}
*/		g_print("\n");
	}
	
}

void show_subtable(subtable *data, gpointer s)
{
	int i;
	g_print("%s (from %s)\n", data->name, data->table->name);
	for (i=0; i < data->nb_fields; i++) {
		g_print("\t%s (field %d)\n", 
				data->fields[i].name,
				data->fields[i].no );
	}
}

void show_tables()
{
	GList *t = g_list_first(list_tables);
	g_print("List of tables\n");
	g_list_foreach(t, (GFunc)show_table, NULL);
}

void show_subtables(GList *ff)
{
	GList *f=ff;
	g_print("List of subtables\n");
	g_list_foreach(f, (GFunc)show_subtable, NULL);
}

void show_view(view *data, gpointer s)
{
	g_print("\t%s\n", data->name );
}

void show_views(GList *ff)
{
	GList *f=ff;
	g_print("List of views\n");
	g_list_foreach(f, (GFunc)show_view, NULL);
}

void show_records(table *t)
{
	record *r;
	int i;
	gabycursor *cs;
	
	cs = cursor_declare(t);
	r = cursor_get_first(cs);
	while ( r ) {
		g_print("%d: ", r->id);
		for (i=0; i < 2; i++) {
			GString *str = get_table_stringed_field(t, r, i);
			g_print("%s ", str->str);
			g_string_free(str, 1);
		}
		g_print("\n");
		r = cursor_get_next(cs);
	}
	cursor_free(cs);

}

void record_add_test(table *t)
{
	record *r;
	int i;
	r = g_malloc(sizeof(record));
	r->id = 0;
	r->cont = g_malloc(sizeof(union data)*t->nb_fields);
	r->cont[0].str = g_string_new("Test");
	for(i=1;i<t->nb_fields;i++) {
		r->cont[i].str = g_string_new("");
	}
	record_add(t, r, TRUE, TRUE);
}

void record_modify_test(table *t)
{
	record *r;
	int i;
	r = g_malloc(sizeof(record));
	r->id = 5;
	r->cont = g_malloc(sizeof(union data)*t->nb_fields);
	r->cont[0].str = g_string_new("Modified");
	for(i=1;i<t->nb_fields;i++) {
		r->cont[i].str = g_string_new("");
	}
	record_modify(t, r);
	g_free(r);

}

void record_list_test()
{
	table *t = g_list_first(list_tables)->data;
	int i;
	for (i=0; i<t->max_records; i++ ) {
		if ( t->records[i] != NULL && t->records[i]->id != 0 ) {
			g_print("%d %s %s\n", 
				t->records[i]->id, 
				t->records[i]->cont[0].str->str,
				(t->nb_fields > 1 ) ? 
					t->records[i]->cont[1].str->str :
					"" );
		}
	}
}

void list_list_test(GList *t, subtable *l)
{
	record *r;
	int i, j;
	table *p;

	p = l->table;
	
	for ( i=0; i < p->max_records; i++ ) {
		for ( j=0; j<l->nb_fields; j++ ) {
			r = p->records[i];
			if ( r == NULL || r->id == 0 )
				continue;
			g_print("%s\t", r->cont[l->fields[j].no].str->str);
		}
		g_print("\n");
	}
					
}

void show_one_subtable_record(subtable *st)
{
	int i;
	int fn;
	int which;
	GString *str;
	record *r;
	GList *a;
	
	g_print("Which one ?"); scanf("%d", &which);
	
	r = st->table->records[which-1];
	g_print("From subtable : %s\n", st->name);

	for ( i=0 ; i < st->nb_fields; i++ ) {
		g_print("%s : ", st->fields[i].name);
		fn = st->fields[i].no;
		str = get_subtable_stringed_field_id(st, which, fn);
		g_print(str->str);
		g_string_free(str, 1);
		if ( st->fields[i].type == T_RECORDS ) {
			a = get_related_records(&st->fields[i], which);
			while ( a != NULL ) {
				g_print("%d ", GPOINTER_TO_INT(a->data));
				a = g_list_next(a);
			}
		}
		g_print("\n");
	}
}

void test_condition(table *t)
{
	record *r;
	int i;
	condition cnd1, cnd2, cnd3;
	
	cnd1.type = C_AND;
	cnd1.c.conditions = NULL;
	cnd1.c.conditions = g_list_append(cnd1.c.conditions, &cnd2);
	cnd1.c.conditions = g_list_append(cnd1.c.conditions, &cnd3);
	
	cnd2.type = C_START_WITH;
	cnd2.field_no = 0;
	cnd2.c.val = g_string_new("E");
	cnd3.type = C_START_WITH;
	cnd3.field_no = 9;
	cnd3.c.val = g_string_new("24");

	for ( i=0; i<t->max_records; i++ ) {
		r = t->records[i];
		if ( record_meets_condition(t, r, &cnd1) ) {
			g_print("\t%d (%s) : true\n", i, r->cont[0].str->str);
		} else  {
			g_print("\t%d (%s) : false\n", i, r->cont[0].str->str);
		}
	}
	
}

void record_list_condition ()
{
	record *r;
	int i;
	int sorted_by;
	table *t = g_list_first(list_tables)->data;
	
	condition cnd1, cnd2, cnd3;
	condition *applied;
	
	cnd1.type = C_AND;
	cnd1.c.conditions = NULL;
	cnd1.c.conditions = g_list_append(cnd1.c.conditions, &cnd2);
	cnd1.c.conditions = g_list_append(cnd1.c.conditions, &cnd3);
	
	cnd2.type = C_START_WITH;
	cnd2.field_no = 0;
	cnd2.c.val = g_string_new("Edf");
	cnd3.type = C_START_WITH;
	cnd3.field_no = 9;
	cnd3.c.val = g_string_new("26");

#if 0
	applied = &cnd2;
#else
	applied = NULL;
#endif
	printf("Sorted by (-1 -> ...) : ");
	scanf("%d", &sorted_by);
	
	r = table_first_with_conditions(t, sorted_by, applied);
	g_print("\t%s (first)\n", r != NULL ? r->cont[0].str->str : "-none-");
	for ( i=0; i<7; i++ ) {
		r = table_next_with_conditions(t, r, sorted_by, applied);
		if ( r == NULL ) g_print("\t-none-\n");
		else {
			g_print("\t%s %s\n", r->cont[0].str->str, 
					r->cont[1].str->str );
		}
	}

}

void test_get_plugin_options()
{
	GList *a, *a_orig;
	gchar *s;
	
	a = get_plugin_options("Canvas:Address Book");
	a_orig = a;
	while ( a != NULL ) {
		s = a->data;
		puts(s);
		g_free(s);
		a = g_list_next(a);
	}
	g_list_free(a_orig);
}

void test_free_everything()
{
	free_everything();
}

static void finish(int sig)
{
#ifdef HAVE_NCURSES
	endwin();
#endif
	exit(0);
}

int main(int argc, char *argv[])
{
	int c;
	char name[30];
	char buf[20];

	strcpy(language, "en");
	if ( getenv("LANGUAGE") || getenv("LANG") ) {
		char *t = g_strdup_printf("%s", getenv("LANGUAGE") ?
				getenv("LANGUAGE")
				: getenv("LANG")   );
		t[2] = 0;
		strcpy(language, t);
		g_free(t);
	}

	if ( argc == 1 ) {
		g_print("Desc name (gaby for desc.gaby) : ");
		fgets(name, 29, stdin);
		strchr(name, '\n')[0] = 0;
	} else {
		strcpy(name, argv[1]);
	}
#ifndef USE_SQL
	if ( name[0] == 0 ) strcpy(name, "gaby");
#else
	if ( name[0] == 0 ) strcpy(name, "gabysql");
#endif
	appname = strdup(name);

	g_print("tables_load_struct ...\n");
	if ( ! tables_load_struct(name ) )
		return 1;
	
	g_print("tables_load ...\n");
	tables_load();
	
	g_print("viewplugins_load ...\n");
#if 0
	if ( ! viewplugins_load(view_plugins, name ) )
		return 2;
	/*#else*/
	vpd = g_new0(ViewPluginData, 1);
	vpd->name = "list";
	view_plugins->data = vpd;
	vpd = g_new0(ViewPluginData, 1);
	vpd->name = "form";
	view_plugins = g_list_append(view_plugins, vpd);

#endif

	g_print("st_load_struct ...\n");
	if ( ! st_load_struct(NULL, name ) )
		return 2;
/*	
	if ( ! actions_load(tables, actions, strrchr(argv[0], '/')+1) )
		return 4;
*/	

#ifdef HAVE_NCURSES
	signal(SIGINT, finish);
	initscr();
	start_color();
#endif
	
	do {
		g_print("[ 1] : show_tables\n");
		g_print("[ 2] : record_list_test\n");
		g_print("[ 3] : show_subtables\n");
		g_print("[ 4] : show_views\n");
		g_print("[ 5] : list_list_test\n");
		g_print("[ 6] : show_one_subtable_record\n");
		g_print("[ 7] : test_condition\n");
		g_print("[ 8] : record_list_condition\n");
		g_print("[ 9] : get_plugins_option\n");
		g_print("[10] : free_everything\n");
		g_print("[11] : show_records\n");
		g_print("\n[00]  : quit\n");
		if ( argc == 3 ) {
			c = atoi(argv[2]);
		} else {
			fgets(buf, 10, stdin);
			c = atoi(buf);
		}
		switch ( c ) {
			case 1: show_tables(); break;
			case 2: record_list_test(); break;
			case 3: show_subtables(g_list_first(list_subtables)); break;
			case 4: show_views(g_list_first(list_views)); break;
			case 5: list_list_test(list_views, g_list_first(list_subtables)->data); break;
			case 6:	show_one_subtable_record(g_list_nth(list_subtables,2)->data); break;
			case 7: test_condition((table*)g_list_nth(list_tables,0)->data); break;
			case 8: record_list_condition(); break;
			case 9: test_get_plugin_options(); break;
			case 10: test_free_everything(); break;
			case 11: show_records((table*)g_list_nth(list_tables,0)->data); break;
		}
				
	} while ( c != 0 && argc != 3 );
	
/*
	tables_save();
*/	
	finish(0);
	return 0;
}


