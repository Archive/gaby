/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>

#ifdef COMPATIBILITY

#include "gaby.h"

gboolean convert_file(gchar *name, gchar *file);

void try_to_read_old_config(gchar *name)
{
	char filename[PATH_MAX], new_name[PATH_MAX];
	FILE *f;
	char str[80];

	if ( strcmp(name, "gaby") != 0 && strcmp(name, "gbc") != 0 ) {
		/* 
		 * We can only convert old files when we have something which
		 * corresponds.
		 */
		return;
	}

	sprintf(filename, "%s/.gaby/%src", g_get_home_dir(), name);
	
	f = fopen(filename, "r");
	if ( f == NULL ) {
		sprintf(filename, "%s/gaby/%src", SYSCONF_DIR, name);
		f = fopen(filename, "r");
		if ( f == NULL )
			return;
	}
	
	g_print(_("An old %s has been found.\n"), name);
	
	do {
		fgets(str, 80, f);
	} while ( strcmp(str, "begin bases\n") != 0 && ! feof(f) );

	if ( feof(f) ) {
		fclose(f);
		return;
	}
	
	fgets(str, 80, f);
	while ( strcmp(str, "end\n") != 0 ) {
		strchr(str, ' ')[0] = 0;
		g_print(_("Conversion of %s ..."), str+1);
		if ( convert_file(name, str+1) ) {
			g_print(_(" ok.\n"));
		} else {
			g_print(_(" error.\n"));
		}
		fgets(str, 80, f);
	}
	
	fclose(f);

	/* we don't want to convert again and again */
	sprintf(new_name, "%s.1.0", filename);
	rename(filename, new_name);
}

gboolean convert_file(gchar *name, gchar *file)
{
	FILE *f_orig;
	FILE *f_dest;
	char filename[PATH_MAX];
	char line[512];     /* hum hum      */
	char new_line[512]; /* same comment */
	static int number=1;
	long eof;
	
	f_orig = fopen(file, "r");
	if ( f_orig == NULL )
		return FALSE;
	
	fseek(f_orig, 0, SEEK_END);
	eof = ftell(f_orig);
	rewind(f_orig);
	
	if      ( strcmp(name, "gaby") == 0 )
		sprintf(filename,"%s/.gaby/table.AddressBook",g_get_home_dir());
	else if ( strcmp(name, "gbc") == 0 )
		sprintf(filename,"%s/.gaby/table.BookCase", g_get_home_dir());
	
	f_dest = fopen(filename, "a");
	if ( f_dest == NULL )
		return FALSE;

	while ( ftell(f_orig) < eof ) {
		fgets(line, 512, f_orig);
		if ( line[0] == '#' || strlen(line) < 3 )
			continue;
		sprintf(new_line, "%d;%s", number++, line);
		fputs(new_line, f_dest);
	}
	
	fclose(f_orig);
	fclose(f_dest);
	
	return TRUE;
}

#endif /* COMPATIBILITY */
