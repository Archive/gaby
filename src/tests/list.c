


void list_records (void)
{
	table *t = list_tables->data;
	int i, j;

	for ( i=0; i < t->max_records; i++ ) {
		record *r;

		r = t->records[i];
		if ( r == NULL || r->id == 0 ) continue;

		g_print("id : %d\n", r->id );
		for ( j=0; j<NB_FIELDS; j++ ) {
			GString *str;
			str = get_table_stringed_field(t, r, j);
			g_print("\t");
			g_print(str->str);
			g_print("\n");
			g_string_free(str, 1);
		}
	}
}


void index_list_records (int sort_by)
{
	table *t = list_tables->data;
	record *r, *old_r;
	int j;

	r = table_first(t, sort_by);
	while ( r != NULL ) {
		g_print("id : %d\n", r->id );
		for ( j=0; j<NB_FIELDS; j++ ) {
			GString *str;
			str = get_table_stringed_field(t, r, j);
			g_print("\t");
			g_print(str->str);
			g_print("\n");
			g_string_free(str, 1);
		}
		old_r = r;
		r = table_next(t, r, sort_by);
		if ( old_r == r ) break;
	}
}

void index_reversed_list_records (int sort_by)
{
	table *t = list_tables->data;
	record *r, *old_r;
	int j;

	r = table_last(t, sort_by);
	while ( r != NULL ) {
		g_print("id : %d\n", r->id );
		for ( j=0; j<NB_FIELDS; j++ ) {
			GString *str;
			str = get_table_stringed_field(t, r, j);
			g_print("\t");
			g_print(str->str);
			g_print("\n");
			g_string_free(str, 1);
		}
		old_r = r;
		r = table_prev(t, r, sort_by);
		if ( old_r == r ) break;
	}
}

void index_list_records_with_cond (int sort_by, condition *c)
{
	table *t = list_tables->data;
	record *r, *old_r;
	int j;

	r = table_first_with_conditions(t, sort_by, c);
	while ( r != NULL ) {
		g_print("id : %d\n", r->id );
		for ( j=0; j<NB_FIELDS; j++ ) {
			GString *str;
			str = get_table_stringed_field(t, r, j);
			g_print("\t");
			g_print(str->str);
			g_print("\n");
			g_string_free(str, 1);
		}
		old_r = r;
		r = table_next_with_conditions(t, r, sort_by, c);
		if ( old_r == r ) break;
	}
}

