
/* number of fields should be 6! */


/* each array should have at least 10 items */

char *t_string[] = { /* thanks to the Jargon file */
	"foo", "bar", "baz", "quux", "quuux", "quuuux", /* MIT */
	"bazola", "ztesch",  /* Stanford */
	"foo", "bar", "thud", "grunt", /* CMU */
	"foo", "bar", "fum", /* XEROX PARC */
	"fred", "barney", /* Brittish */
	"corge", "grault", "flarp", /* Rutgers */
	"zxc", "spqr", "wombat", /* Cambridge */ 
	"shme" /* Berkeley */
};

char *t_strings[] = { /* thanks to WordNet dictionary */
	"the blood group whose red cells carry the A antigen",
	"a nuclear weapon in which enormous energy is released",
	"the top layer of a soil profile",
	"in perfect condition or order",
	"of the highest quality",
	"English author who created Sherlock Holmes",
	"as known or named at another time or place",
	"the time interval from midnight to midday",
	"battery used to heat the filaments of a vacuum tube",
	"to a small degree; somewhat",
	"being excessive or unreasonable"
};

int t_integer[] = {
	17, 23, 42, 69, 105, 666, /* from the Jargon file */
	226416, 2092, 222376, 294876, 225376, 226252, 221800, 230948
			/* size of some Gaby .o files */
};

float t_real[] = {
	2.16076, 22.3968, 239.856, 265.984, 2197.76, 22749.2, 255324.0,
	2.36232, 22.9192, 218.328 /* same source, dots added */
};

char *t_date[] = { /* have to be cleared with g_date_clear !! */
	" 8/ 5/1999", /* I know this should be May 8th but this little 'error'
		       * is useful to test something else than an already
		       * sorted list :) */
	" 5/11/1999",
	" 5/13/1999",
	" 5/14/1999",
	" 5/18/1999",
	" 5/21/1999",
	" 5/28/1999",
	" 6/12/1999",
	" 8/15/1999",
	" 9/ 8/1999",
	" 9/20/1999"
		/* Gaby release dates */
};

gboolean t_boolean[] = { /* no explanation */
	FALSE, TRUE, TRUE, FALSE, TRUE, FALSE, TRUE, TRUE, FALSE, TRUE
};

void add_records (int start, int end)
{
	static int id = 0;
	int i, j;
	table *t = list_tables->data;
	
	for ( i=start; i<end; i++ ) {
		record *r;
		id++;

		r = g_new0(record, 1);
		r->id = id;
		r->cont = g_new0(union data, NB_FIELDS);
		for ( j=0; j<NB_FIELDS; j++ ) {
			switch ( t->fields[j].type ) {
				case T_STRING:
					r->cont[j].str = g_string_new(t_string[i]);
					break;
				case T_STRINGS:
					r->cont[j].str = g_string_new(t_strings[i]);
					break;
				case T_INTEGER:
					r->cont[j].i = t_integer[i];
					break;
				case T_REAL:
					r->cont[j].d = t_real[i];
					break;
				case T_DATE:
				{
					r->cont[j].date = g_date_new();
					g_date_set_parse(r->cont[j].date, \
								t_date[i]);
				} break;
				default: {;} break;
			}
		}
		record_add(t, r, FALSE, FALSE);
	}

}

