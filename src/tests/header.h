
#define NO_GUI
#define LISTS_DECLARED
#define GABY_TEST

#include "../gaby.h"
#include "../errors.h"
#include "../records.h"
#include "../tables.h"

GList *list_tables;
GList *list_subtables;
GList *list_views;
GList *list_actions;

int debug_mode;

property* field_get_property ( field *f, gchar *ident ) { return NULL; }
void gaby_perror_in_a_box (void) { puts(errors_list[gaby_errno]); }


/* Miguel forced me to do this :( */
gboolean gaby_load_file     ( struct location *loc ) { return TRUE; }
gboolean gaby1_load_file    ( struct location *loc ) { return TRUE; }
gboolean nosql_load_file    ( struct location *loc ) { return TRUE; }
gboolean vcard_load_file    ( struct location *loc ) { return TRUE; }
gboolean dbase_load_file    ( struct location *loc ) { return TRUE; }
gboolean appindex_load_file ( struct location *loc ) { return TRUE; }
gboolean dpkg_load_file     ( struct location *loc ) { return TRUE; }

gboolean gaby_save_file     ( struct location *loc ) { return TRUE; }
gboolean gaby1_save_file    ( struct location *loc ) { return TRUE; }
gboolean nosql_save_file    ( struct location *loc ) { return TRUE; }
gboolean vcard_save_file    ( struct location *loc ) { return TRUE; }
gboolean dbase_save_file    ( struct location *loc ) { return TRUE; }
gboolean appindex_save_file ( struct location *loc ) { return TRUE; }
gboolean dpkg_save_file     ( struct location *loc ) { return TRUE; }


#define NB_FIELDS 5

#include "create.c"
#include "add.c"
#include "remove.c"
#include "list.c"

