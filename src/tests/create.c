
void create_tables (void)
{
	table *t;
	int i;

	t = g_new0(table, 1);
	t->name = strdup("test");
	t->short_name[0] = 0;

	t->nb_fields = NB_FIELDS;
	t->fields = g_new0(field, t->nb_fields);
	for ( i=0; i<t->nb_fields; i++ ) {
		char st[50];
		field *f = &(t->fields[i]);

		sprintf(st, "field %d", i+1);
		f->name = strdup(st);
		f->i18n_name = f->name;
		f->type = ( i != 5 && i < 9 ) ? i : T_STRING;
		f->properties = NULL;
		f->ok_if = NULL;
	}

	t->records = NULL;
	t->indexes = NULL;
	t->nb_records = 0;
	t->max_records = 0;
	t->locations = g_list_append(NULL, NULL);
	t->indexes = g_malloc0(sizeof(int*) * (NB_FIELDS+1) );
	
	list_tables = g_list_append(list_tables, t);
}

void create_subtables (void)
{
	subtable *st;
	table *t = list_tables->data;
	
	st = g_new0(subtable, 1);
	st->name = t->name;
	st->i18n_name = t->name;
	st->table = t;



	
	st->cond = NULL;
	
	list_subtables = g_list_append(list_subtables, st);
}

void create_db (void)
{
	list_tables = NULL;
	list_subtables = NULL;
	
	create_tables();
	create_subtables();
}

