

void remove_some ()
{
	table *t = list_tables->data;
	
	record_remove_id(t, 2);
	record_remove_id(t, 4);

}

void remove_all_1 ()
{	/* from id 1 to the end */
	table *t = list_tables->data;
	int i;

	for ( i=1; i<t->max_records+1; i++ ) {
		record_remove_id(t, i);
	}
}	

void remove_all_2 ()
{	/* from the end to id 1 */
	table *t = list_tables->data;
	int i;

	for ( i=t->max_records+1; i>0; i-- ) {
		record_remove_id(t, i);
	}
}	

void remove_all_3 ()
{	/* in really pseudo random order */
	table *t = list_tables->data;
	int i;

	for ( i=1; i<t->max_records+1; i+=3 ) {
		record_remove_id(t, i);
	}
	
	for ( i=t->max_records+1; i>0; i-=3 ) {
		record_remove_id(t, i);
	}
	
	for ( i=1; i<t->max_records+1; i+=2 ) {
		record_remove_id(t, i);
	}
	
	for ( i=t->max_records+1; i>0; i-=2 ) {
		record_remove_id(t, i);
	}
	
	remove_all_1();
}	

