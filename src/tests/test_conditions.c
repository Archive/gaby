
#include "header.h"

int main (int argc, char *argv[])
{
	condition c;

	create_db();
	add_records(0,8);

	switch ( atoi(argv[1]) ) {
		case 1:
		{
			c.type = C_IS;
			c.val_true = TRUE;
			c.field_no = 0;
			c.c.val = g_string_new("baz");
		} break;
		case 2:
		{
			c.type = C_IS_NOT;
			c.val_true = TRUE;
			c.field_no = 0;
			c.c.val = g_string_new("baz");
		} break;
		case 3:
		{
			c.type = C_IS_MORE;
			c.val_true = TRUE;
			c.field_no = 1;
			c.c.val = g_string_new("dhs");
		} break;
		case 4:
		{
			c.type = C_IS_MORE;
			c.val_true = TRUE;
			c.field_no = 2;
			c.c.val = g_string_new("25");
		} break;
		case 5:
		{
			c.type = C_IS_MORE;
			c.val_true = TRUE;
			c.field_no = 4;
			c.c.val = g_string_new("05/18/99");
		} break;
		case 6:
		{
			c.type = C_IS_LESS;
			c.val_true = TRUE;
			c.field_no = 3;
			c.c.val = g_string_new("500.5423");
		} break;
		case 7:
		{
			c.type = C_STARTS_WITH;
			c.val_true = TRUE;
			c.field_no = 1;
			c.c.val = g_string_new("a");
		} break;
		case 8:
		{
			c.type = C_HAS;
			c.val_true = TRUE;
			c.field_no = 1;
			c.c.val = g_string_new("in");
		} break;
		case 9:
		{
			c.type = C_REGEX;
			c.val_true = TRUE;
			c.field_no = 1;
			c.c.val = g_string_new("^[aot].*[ey]$");
		} break;
	}
	
	index_list_records_with_cond ( -1, &c);

	return 0;
}

