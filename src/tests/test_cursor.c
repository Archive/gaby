
#include "header.h"

int main(int argc, char *argv[])
{
	create_db();

	add_records(0, 5);
	
	{
		table *t = list_tables->data;
		record *r;
		gabycursor *cs;
		int i;
		
		cs = cursor_declare(t);
		r = cursor_get_first(cs);
		while ( r ) {
			g_print("%d: ", r->id);
			for (i=0; i < 2; i++) {
				GString *str = get_table_stringed_field(t,r,i);
				g_print("%s ", str->str);
				g_string_free(str, 1);
			}
			g_print("\n");
			r = cursor_get_next(cs);
		}
		cursor_free(cs);
	}
	
	return 0;
}

