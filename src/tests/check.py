#! /usr/bin/python

print '''
Gaby testing program (v0.3)
	(copyright (c) 1999 Frederic Peters, released under the GPL)
'''

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


import os
import string

class gaby_test:
	def __init__(self, comment='', program='', args='', result='PASS'):
		self.comment = comment
		self.program = program
		self.args = args
		self.result = result

tests = [ gaby_test('adding records', 'add'),
          gaby_test(),
	  gaby_test('removing some records', 'remove_some'),
	  gaby_test('removing every records (1)', 'remove_all__1' ),
	  gaby_test('removing every records (2)', 'remove_all__2' ),
	  gaby_test('removing every records (3)', 'remove_all__3' ),
	  gaby_test(),
	  gaby_test('indexing records (id)', 'index', '0' ),
	  gaby_test('indexing records (string)', 'index', '1' ),
	  gaby_test('indexing records (strings)', 'index', '2' ),
	  gaby_test('indexing records (integer)', 'index', '3' ),
	  gaby_test('indexing records (real)', 'index', '4' ),
	  gaby_test('indexing records (date)', 'index', '5' ),
	  gaby_test(),
	  gaby_test('reversed-indexing records (id)', 'index', '0' ),
	  gaby_test('reversed-indexing records (string)', 'index_reversed', '1' ),
	  gaby_test('reversed-indexing records (strings)', 'index_reversed', '2' ),
	  gaby_test('reversed-indexing records (integer)', 'index_reversed', '3' ),
	  gaby_test('reversed-indexing records (real)', 'index_reversed', '4' ),
	  gaby_test('reversed-indexing records (date)', 'index_reversed', '5' ),
	  gaby_test(),
	  gaby_test('mixed operations (1)', 'mixed', '1' ),
	  gaby_test('mixed operations (2)', 'mixed', '2' ),
	  gaby_test(),
	  gaby_test('conditions (C_IS on String)', 'conditions', '1' ),
	  gaby_test('conditions (C_IS_NOT on String)', 'conditions', '2' ),
	  gaby_test('conditions (C_IS_MORE on String)', 'conditions', '3' ),
	  gaby_test('conditions (C_IS_MORE on Integer)', 'conditions', '4' ),
	  gaby_test('conditions (C_IS_MORE on Date)', 'conditions', '5' ),
	  gaby_test('conditions (C_IS_LESS on Real)', 'conditions', '6' ),
	  gaby_test('conditions (C_STARTS_WITH on Strings)', 'conditions', '7' ),
	  gaby_test('conditions (C_HAS on Strings)', 'conditions', '8' ),
	  gaby_test('conditions (C_REGEX on Strings)', 'conditions', '9' ),
	  gaby_test(),
	  gaby_test('basic cursor manipulation', 'cursor'),

	]


passed = 0
failed = 0
xpassed = 0
xfailed = 0
blank = 0

for test in tests:
	if ( len(test.comment) == 0 ):
		blank = blank + 1
		print
		continue
	print string.ljust(test.comment + '...', 40),
	program = './test_' + test.program + ' ' + test.args + \
					' > test_' + test.program + '.result'
	result = os.system(program + ' 2> /dev/null' )
	if ( result != 0 ):
		# program failed
		if ( test.result == 'FAIL' ):
			xfailed = xfailed + 1
			print 'XFAIL',
		else:
			failed = failed + 1
			print 'FAIL',
		print '(test failed)'
		continue
	
	expected = 'test_' + string.split(test.program, '__')[0] + \
						test.args + '.expected'
	result2 = os.system( 'diff -Bq ' + expected + 
			' test_' + test.program + '.result ' +
			' > /dev/null 2> /dev/null' )
	if ( result2 != 0 ):
		if ( test.result == 'FAIL' ):
			xfailed = xfailed + 1
			print 'XFAIL',
		else:
			failed = failed + 1
			print 'FAIL',
		print '(incorrect output)'
		continue
	
	if ( test.result == 'FAIL' ):
		xpassed = xpassed + 1
		print 'XPASS'
	else:
		passed = passed + 1
		print 'PASS'

print
print
print 'Results:'
print
print 'On a total of ' + `len(tests) - blank` + ' tests:'
print '  ' + string.rjust( `passed`,2) + ' passed and were expected to pass'
print '  ' + string.rjust(`xpassed`,2) + ' passed but were NOT expected to pass'
print '  ' + string.rjust( `failed`,2) + ' failed but were NOT expected to fail'
print '  ' + string.rjust(`xfailed`,2) + ' failed and were expected to fail'
print

if ( passed == len(tests)-blank ):
	print '(this is nearly incredible)\n'

