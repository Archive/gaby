/*  Gaby
 *  Copyright (C) 1998-2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef GABY_STRUCT_H
#define GABY_STRUCT_H

typedef struct _record		record;
typedef struct _table		table;
typedef struct _field		field;
typedef struct _st_field	st_field;
typedef struct _subtable	subtable;
typedef struct _condition	condition;
typedef struct _view		view;
typedef struct _action		action;
typedef struct _property	property;
typedef struct _gabywindow	gabywindow;
typedef struct _gabycursor	gabycursor;

typedef enum   _field_type	field_type;

typedef struct _ViewPluginData	ViewPluginData;
typedef struct _PrintPluginData	PrintPluginData;
typedef struct _ActionPluginData ActionPluginData;

struct location {
	gchar *filename;
	gchar *type;
	int max_index;
	int offset;
	int reread;	/* reread the file every $reread minutes */
	int timeout_tag;
	gboolean readonly;
	gboolean disabled;
	table *table;
};

union data {
	GString *str;
	int i;
	float d;
	GDate *date;
	gboolean b;
	gpointer anything;
};

struct _record {
	int id;
	union data *cont;
	struct location *file_loc;
};

enum _field_type {
	T_STRING  	= 0,
	T_STRINGS 	= 1,
	T_INTEGER 	= 2,
	T_REAL    	= 3,
	T_DATE    	= 4,
	T_BOOLEAN 	= 5,
	T_RECORD  	= 6,
	T_RECORDS	= 7,	/* this one should only happen in subtables */
	T_MULTIMEDIA	= 8,
	T_DECIMAL	= 9,
	T_FILE		= 10
};

struct _field {
	gchar *name;
	gchar *i18n_name;

	field_type type;
	
	/* see field_property (f_desc.c:~166) for details about this one */
	property **properties; /* NULL terminated array */
	
	GList *ok_if;

};

struct _property {
	gchar *name;
	gpointer val;
};

struct _table {
	gchar *name;
	char short_name[5];
	field *fields;
	int nb_fields;
	record **records;
#ifdef INDEXES_ARE_ARRAYS
	int **indexes;
#else
	GList **indexes;
#endif
	int nb_records;
	int max_records;
	GList *locations;
	gboolean updated;
};

struct _st_field {
	gchar *name;
	gchar *i18n_name;
	int no;
	field_type type;
	
	/* this GList is for things like 
	 * 	Father_{First Name, Last Name}:Father
	 */
	GList *link_format;	/* a list of field numbers */
	
	/* for T_RECORDS */
	view *v;

};

struct _subtable {
	gchar *name;
	gchar *i18n_name;
	table *table;
	st_field *fields;
	int nb_fields;
	condition *cond;
};

struct _condition {
	enum {
		C_OR		= 0,
		C_AND		= 1,
		C_IS 		= 2,
		C_IS_NOT	= 3,
		C_IS_GREATER	= 4,	/* to disappear */
		C_IS_MORE	= 4,
		C_IS_LESS	= 5,
		C_START_WITH	= 6,	/* to disappear */
		C_STARTS_WITH	= 6,
		C_HAS		= 7,
		C_REGEX		= 8
	} type;
	gboolean val_true;
	
	int field_no;

	union {
		GList *conditions;
		GString *val;
	} c;
	
};

struct _view {
	gchar *name;
	subtable *subtable;
	ViewPluginData *type;
};

enum action_param_type {
	P_FIELD = 0,
	P_FIELD_NO = 1,
	P_TABLE = 2,
	P_DIRECT = 3
};

struct action_param {
	enum action_param_type type;
	table *table;
	int field_no;
	union data val;
};

struct _action {
	gchar *name;
	gchar *i18n_name;
	enum {
		PLUG_IN   = 1,
		SCRIPT    = 2,
		SCRIPT_FU = 3
	} type;
	enum {
		EVENT_MENU	= 1 << 0,
		EVENT_STARTUP	= 1 << 1
	} event;
	union {
		ActionPluginData *plugin;
		gchar *script;
	} what;
	int nb_params;
	struct action_param *params;
	void (*function) (struct action_param *params, int *dec );
};

struct window_info {
	view *view;
	int x, y;
	int width, height;
	gboolean visible;
};

struct w_bindings {
	gchar *window_name;
	GList *bound_windows;
};

struct _gabywindow {
	view *view;
	int id;
	gchar *name;
	GList *what;
	gboolean updated;
	gboolean focused;
	GList *bound_windows;
#ifdef NO_GUI
	void *widget;
	void *parent;
#else
	GtkWidget *widget;
	GtkWidget *parent;
#endif
};
	
struct _ActionPluginData {
	gint loaded;
	gchar *name;
	GModule	*handle;
	void (*get_function_by_name) (gchar *name, action *act);
#ifdef NO_GUI
	void (*configure) ();
#else
	GtkWidget* (*configure) ();
#endif
};

#if 0 /* old print plug-in arch */
struct _PrintPluginData {
	gint loaded;
	gchar *name;		/* visible name */
	gchar *i18n_name;	/* real name */
	GModule	*handle;
	gboolean	(*init_print_plugin)	(PrintPluginData *ppd);
#if 0
	void		(*cleanup)		(PrintPluginData *ppd );
#endif
	void		(*print)		(subtable *s,
						 char *filename,
						 FILE *f,
						 int *dec);
	void		(*print_fast)		(subtable *s,
						 char *filename,
						 FILE *f,
						 int *dec);
};
#else

struct _PrintPluginData {
	gchar *name;
	gchar *i18n_name;
};

#endif

struct _ViewPluginData {
	GModule *handle;
	int	(*init_plugin)	(ViewPluginData *vpd);
	
	void	(*view_create)	(gabywindow *win, gboolean first);
	
	void	(*view_fill)	(gabywindow *win);
	void	(*view_save)	(gabywindow *win);
	void	(*view_records)	(gabywindow *win, GList **records);
	
#ifndef NO_GUI
	GtkWidget*	(*configure)		(ViewPluginData *vpd);
	GtkWidget*	(*view_get_widget)	(gabywindow *win, gchar *s);
#else
	void*		(*configure)		(ViewPluginData *vpd);
	void*		(*view_get_widget)	(gabywindow *win, gchar *s);
#endif
	
	gchar *name;		/* this is for config files */
	gchar *i18n_name;	/* this is for menus */
	enum {
		ALL_RECORDS = 1 << 0,
		ONE_RECORD = 1 << 1,
		FILTER = 1 << 2
	} type;
	enum {
		NONE = 0,
		EDITABLE = 1 << 0,
		FILTERABLE = 1 << 1,
			/* my english dict says this word is deprecated
			 * anything more appropriate ? */
	} capabilities;
		/* type and capabilities may be merged later */
};

struct _gabycursor {
	table		*table;
	long		position;
#ifdef USE_SQL
	void		*sqlres;	/* actually PGresult* */
	int		number;
#else
	void		*unused1;
	int		unused2;
#endif
};

#endif /* ! GABY_STRUCT_H */

