/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gaby.h"
#include "tables.h"
#include "f_desc.h"
#include "windows.h"
#include "gtk_dialogs.h"

static void close_dialog(GtkWidget *but, GtkWidget *dialog);

static void row_selected(GtkCList *clist, gint row, gint column,
			 GdkEventButton *event, GtkWidget *win )
{
	;
}

static void management_fill_list(GtkWidget *dialog)
{
	GtkWidget *list = gtk_object_get_data(GTK_OBJECT(dialog), "list");
	GList *tables = gtk_object_get_data(GTK_OBJECT(dialog), "tables");
	GList *locs;
	struct location *l;

	gtk_clist_freeze(GTK_CLIST(list));
	gtk_clist_clear(GTK_CLIST(list));
	
	while ( tables != NULL ) {
		locs = ((table*)(tables->data))->locations;
		tables = g_list_next(tables);
		locs = g_list_first(locs);
		while ( locs != NULL ) {
			l = locs->data;
			gtk_clist_append(GTK_CLIST(list), &l->filename);
			locs = g_list_next(locs);
		}
	}

	gtk_clist_thaw(GTK_CLIST(list));
}

GtkWidget* management_dialog(GList *tables)
{
	/*
	 * this is a minimal file management dialog whose main purpose is to
	 * help convert from one format to another, for a more complete
	 * handling of files the user must use the builder (locations section)
	 */
	static GtkWidget *dialog = NULL;
	table *t;
	GtkWidget *vbox, *hbox, *list, *frame, *table;
	GtkWidget *label, *entry, *combo, *li;
	GList *tmp;
	char *format_plugins[] = { /* hard-coded here :( */
		"gaby",
		"gaby1",
		"dpkg",
		"vcard",
		NULL
	};
	int i;

#ifndef USE_GNOME
	gaby_errno = ONLY_IN_GNOME;	/* useless, skip it */
	gaby_perror_in_a_box();
	return NULL;
#endif
	if ( dialog && dialog->window && GTK_IS_WINDOW(dialog))
		return dialog;

#ifdef USE_GNOME
	dialog = gnome_dialog_new(_("Files management"), 
			GNOME_STOCK_BUTTON_CLOSE, NULL );
	gnome_dialog_set_close(GNOME_DIALOG(dialog), TRUE );
	vbox = GNOME_DIALOG(dialog)->vbox;
#else /* ! USE_GNOME */

#endif

	gtk_object_set_data(GTK_OBJECT(dialog), "tables", tables);
	
	hbox = gtk_hbox_new(FALSE, 0);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0 );
	
	list = gtk_clist_new(1);
	gtk_signal_connect(GTK_OBJECT(list), "select_row", 
			GTK_SIGNAL_FUNC(row_selected), dialog );
	gtk_widget_set_usize(list, 200, -1);
	gtk_object_set_data(GTK_OBJECT(dialog), "list", list);
	gtk_widget_show(list);
	gtk_box_pack_start(GTK_BOX(hbox), list, TRUE, TRUE, 3 );

	frame = gtk_frame_new(_("Properties"));
	gtk_widget_show(frame);
	gtk_box_pack_start(GTK_BOX(hbox), frame, TRUE, TRUE, 3 );
	
	table = gtk_table_new( 3, 2, FALSE);
	gtk_container_set_border_width(GTK_CONTAINER(table), 5);
	gtk_table_set_row_spacings(GTK_TABLE(table), 5 );
	gtk_table_set_col_spacings(GTK_TABLE(table), 5 );
	gtk_widget_show(table);
	gtk_container_add(GTK_CONTAINER(frame), table );

	label = gtk_label_new(_("File name"));
	gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 0, 1);

#ifdef USE_GNOME
	entry = gnome_file_entry_new(NULL, _("Filename"));
#else /* ! USE_GNOME */
	entry = gtk_entry_new();
#endif
	gtk_widget_show(entry);
	gtk_object_set_data(GTK_OBJECT(dialog), "filename", entry);
	gtk_table_attach_defaults(GTK_TABLE(table), entry, 1, 2, 0, 1);

	label = gtk_label_new(_("Table"));
	gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 1, 2);
	
	combo = gtk_combo_new();
	gtk_widget_set_sensitive(combo, FALSE);
	gtk_entry_set_editable(GTK_ENTRY(GTK_COMBO(combo)->entry), FALSE);
	/* filling the combo with tables names */
	tmp = g_list_first(tables);
	while ( tmp != NULL ) {
		t = tmp->data;
		li = gtk_list_item_new_with_label( t->name );
		gtk_widget_show(li);
		gtk_container_add (GTK_CONTAINER(GTK_COMBO(combo)->list), li);
		tmp = g_list_next(tmp);
	}
	gtk_widget_show(combo);
	gtk_object_set_data(GTK_OBJECT(dialog), "table", combo);
	gtk_table_attach_defaults(GTK_TABLE(table), combo, 1, 2, 1, 2);

	label = gtk_label_new(_("Format"));
	gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 2, 3);

	combo = gtk_combo_new();
	/*
	 * currently the plug-ins name are hard-coded
	 *
	 * we could have a 'plug-in repository' (sp?) file which would keep
	 * track of plug-in in a way like :
	 * 	file:gaby	Standard file format
	 * 	file:dpkg	File format used for Debian packaging
	 * 	view:form	Versatile form view
	 * 	view:list	Simple list view
	 * 	print:HTML	Printing to HTML
	 * 	actions:stat	Statistical functions (count, sum, average, ...)
	 * 	...
	 *
	 * and we should have a way to tell that the dpkg format can only be
	 * used for debian packages files, the vcard format for personal datas,
	 * ...
	 */
	i=0;
	while ( format_plugins[i] != NULL ) {
		li = gtk_list_item_new_with_label( format_plugins[i] );
		gtk_widget_show(li);
		gtk_container_add (GTK_CONTAINER(GTK_COMBO(combo)->list), li);
		i++;
	}
	gtk_widget_show(combo);
	gtk_object_set_data(GTK_OBJECT(dialog), "format", combo);
	gtk_table_attach_defaults(GTK_TABLE(table), combo, 1, 2, 2, 3);

#if 0	/* I think a better place for them is in the builder */
	label = gtk_label_new(_("Access permissions"));
	gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 3, 4);

	label = gtk_label_new(_("Reread every "));
	gtk_widget_show(label);
	gtk_table_attach_defaults(GTK_TABLE(table), label, 0, 1, 4, 5);
#endif /* 0 */
	
	management_fill_list(dialog);
	gtk_clist_select_row(GTK_CLIST(list), 0, 0);

	return dialog;
}

#if 0
static void row_changed( GtkCList *clist, gint row, gint column,
			 GdkEventButton *event, GtkWidget *wid)
{
#ifdef USE_GNOME
	gnome_dialog_set_sensitive(GNOME_DIALOG(wid), 0, 
					( clist->selection != NULL ) );
#else
	gtk_widget_set_sensitive(wid, ( clist->selection != NULL ) );
#endif
}

void select_subtable_dialog(GList *subtables, GtkSignalFunc ok_cb, gchar *text )
{
	GtkWidget *dialog;
	GtkWidget *vbox, *clist;
	GtkWidget *label;
#ifndef USE_GNOME
	GtkWidget *button, *buttonok;
#endif

#ifdef USE_GNOME
	dialog = gnome_dialog_new(_("Select a subtable"), 
				GNOME_STOCK_BUTTON_OK, 
				GNOME_STOCK_BUTTON_CANCEL, 
				NULL );
	gnome_dialog_set_close(GNOME_DIALOG(dialog), TRUE );
	vbox = GNOME_DIALOG(dialog)->vbox;

	gnome_dialog_button_connect(GNOME_DIALOG(dialog), 0, ok_cb, dialog);
#else
	dialog = gtk_dialog_new();
	vbox = GTK_DIALOG(dialog)->vbox;
	
	button = gtk_button_new_with_label(_("OK"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), button, 
				FALSE, FALSE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", ok_cb, dialog );
	buttonok = button;

	button = gtk_button_new_with_label(_("Cancel"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), button, 
				FALSE, FALSE, 0 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", close_dialog, dialog);
#endif

	label = gtk_label_new(text);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0 );
	
	clist = gtk_clist_new(1);
	gtk_box_pack_start(GTK_BOX(vbox), clist, TRUE, TRUE, 0 );
	
	subtables = g_list_first(subtables);
	while ( subtables != NULL ) {
		gtk_clist_append(GTK_CLIST(clist), 
				&((subtable*)subtables->data)->i18n_name );
		subtables = g_list_next(subtables);
	}

#ifdef USE_GNOME
	gtk_signal_connect(GTK_OBJECT(clist), "select_row", 
						row_changed, dialog );
	gtk_signal_connect(GTK_OBJECT(clist), "unselect_row", 
						row_changed, dialog );
	row_changed(GTK_CLIST(clist), 0, 0, NULL, dialog);
#else
	gtk_signal_connect(GTK_OBJECT(clist), "select_row", 
						row_changed, buttonok );
	gtk_signal_connect(GTK_OBJECT(clist), "unselect_row", 
						row_changed, buttonok );
	row_changed(GTK_CLIST(clist), 0, 0, NULL, buttonok);
#endif
	
	gtk_widget_show(clist);
	gtk_object_set_data(GTK_OBJECT(dialog), "clist", clist );
	
	gtk_widget_show(dialog);
}
#endif /* 0 */

static void close_dialog(GtkWidget *but, GtkWidget *dialog)
{
	GList *plugin_list = gtk_object_get_data(GTK_OBJECT(dialog), "plugin_list");
	GList *orig = plugin_list;
	
	while ( plugin_list != NULL ) {
		g_free(plugin_list->data);
		plugin_list = g_list_next(plugin_list);
	}

	g_list_free(orig);
	
	gtk_widget_destroy(dialog);

	update_windows(NULL);
}

static void import_really_real_callback(GtkWidget *but, GtkWidget *dialog)
{
	GList *plugin_list = gtk_object_get_data(GTK_OBJECT(dialog), "plugin_list");
	GList *orig = plugin_list;
	subtable *st = gtk_object_get_data(GTK_OBJECT(dialog), "subtable");
	struct location locat;
	struct location *new_loc;
	char plugin_file[PATH_MAX];
#ifndef FOLLOW_MIGUEL
	GModule *handle;
	gboolean (*load_file) (struct location *loc);
	char fctname[100];
#else
	struct location *loc;
#endif
	gboolean success = FALSE;
	int i;
	record *r;
	char *plugin;
	
	/* 
	 * struct location and loading as usual but then replacing every
	 * occurence of location by the first location of subtable->table
	 */

	plugin = (char*)gtk_object_get_data(GTK_OBJECT(dialog), "plugin");
	
	sprintf(plugin_file, PLUGINS_DIR "/formats/lib%s" SO_EXTENSION, plugin);
#ifdef DEBUG_GABY
	debug_print("[irrc] importing with %s\n", plugin_file );
#endif

	new_loc = g_list_first(st->table->locations)->data;
	
	locat.filename = gtk_file_selection_get_filename ( 
						GTK_FILE_SELECTION(dialog));
	locat.type = g_strdup(plugin);
	locat.offset = new_loc->max_index + 1;
	locat.table = new_loc->table;

#ifndef FOLLOW_MIGUEL
	handle = g_module_open(plugin_file, 0 );
	if ( handle != NULL ) {
		sprintf(fctname, "%s_load_file", plugin);
		g_module_symbol(handle, fctname, (gpointer)&load_file);
		if ( load_file != NULL ) {
			success = load_file(&locat);
		}
		g_module_close(handle);
	}
#else
	loc = &locat;
#include "py_lf.c"
	success = TRUE;
#endif
	
	if ( success == TRUE ) {
		table_maxindex(st->table);
		for (i=0; i < st->table->max_records; i++ ) {
			r = st->table->records[i];
			if ( r == NULL || r->id == 0 )
				continue;
			if ( r->file_loc == &locat ) {
				r->file_loc = new_loc;
				new_loc->max_index = locat.offset+1;
			}
		}
	}
	/* we need to free them */
	while ( plugin_list != NULL ) {
		g_free(plugin_list->data);
		plugin_list = g_list_next(plugin_list);
	}

	g_list_free(orig);
	g_free(locat.type);

	gtk_widget_destroy(dialog);

	if ( success ) {
		gaby_errno = CUSTOM_MESSAGE;
		gaby_message = g_strdup(_("File imported succesfully."));
	} else {
		gaby_errno = CUSTOM_ERROR;
		gaby_message = g_strdup(_("Gaby failed to import this file."));
	}
	gaby_perror_in_a_box();

	update_windows(NULL);
}

static void export_really_real_callback(GtkWidget *but, GtkWidget *dialog)
{
	GList *plugin_list = gtk_object_get_data(GTK_OBJECT(dialog), "plugin_list");
	GList *orig = plugin_list;
	subtable *st = gtk_object_get_data(GTK_OBJECT(dialog), "subtable");
	char plugin_file[PATH_MAX];
	struct location locat;
#ifndef FOLLOW_MIGUEL
	GModule *handle;
	gboolean (*save_file) (struct location *loc);
	char fctname[100];
#else
	struct location *loc;
#endif
	char *plugin;
	gboolean success = FALSE;

	/* 
	 *  struct location filled with type == NULL; the plug-in will have to
	 *  look this to know if they save only the records from a given
	 *  location (!= NULL) or every record (==NULL)
	 */
	plugin = (char*)gtk_object_get_data(GTK_OBJECT(dialog), "plugin");
	sprintf(plugin_file, PLUGINS_DIR "/formats/lib%s" SO_EXTENSION, plugin);
#ifdef DEBUG_GABY
	debug_print("[irrc] exporting with %s\n", plugin_file );
#endif

	locat.filename = gtk_file_selection_get_filename ( 
						GTK_FILE_SELECTION(dialog));
	locat.type = NULL;
	locat.offset = 0;
	locat.table = st->table;
		
#ifndef FOLLOW_MIGUEL
	handle = g_module_open(plugin_file, 0 );
	if ( handle != NULL ) {
		sprintf(fctname, "%s_save_file", plugin);
		g_module_symbol(handle, fctname, (gpointer)&save_file);
		if ( save_file != NULL ) {
			success = save_file(&locat);
		}
		g_module_close(handle);
	}
#else
	loc = &locat;
#include "py_lf.c"
	success = TRUE;
#endif

	while ( plugin_list != NULL ) {
		g_free(plugin_list->data);
		plugin_list = g_list_next(plugin_list);
	}

	g_list_free(orig);
	
	if ( success ) {
		gaby_errno = CUSTOM_MESSAGE;
		gaby_message = g_strdup(_("Records exported succesfully."));
	} else {
		gaby_errno = CUSTOM_ERROR;
		gaby_message = g_strdup(_("Gaby failed to export these records."));
	}
	gaby_perror_in_a_box();

	gtk_widget_destroy(dialog);
}

static void format_selected(GtkWidget *item, GtkWidget *dialog)
{
	gchar *plugin = gtk_object_get_data(GTK_OBJECT(item), "plugin");

	gtk_object_set_data(GTK_OBJECT(dialog), "plugin", plugin );
}

void import_export_dialog(subtable *st, gint type)
{
	GtkWidget *dialog;
	GtkWidget *hbox, *frame;
	GtkWidget *label, *om, *om_menu, *om_item;
#ifndef FOLLOW_MIGUEL
	DIR *dir;
	struct dirent *ent;
	char dirname[PATH_MAX];
	char message[1000];
#endif
	gboolean is_first = TRUE;
	int flg = 0;
	char *plugin;
	GList *plugin_list = NULL;
	FILE *f;
	int lensoext = strlen(SO_EXTENSION);

	if ( type == 0 ) {
		dialog = gtk_file_selection_new(_("Import from"));
	} else {
		dialog = gtk_file_selection_new(_("Export to"));
	}

	gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(dialog)->ok_button), 
			"clicked", 
			( type == 0 ) ? import_really_real_callback : 
					export_really_real_callback,
			dialog );
	gtk_signal_connect( 
			GTK_OBJECT(GTK_FILE_SELECTION(dialog)->cancel_button),
			"clicked", close_dialog, dialog );
			
	
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(GTK_FILE_SELECTION(dialog)->main_vbox), 
				hbox, TRUE, TRUE, 3 );
	
	label = gtk_label_new(_("Format :"));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0 );
	
	om = gtk_option_menu_new();
	om_menu = gtk_menu_new();

#ifndef FOLLOW_MIGUEL
	sprintf(dirname, "%s/formats/", PLUGINS_DIR);
	dir = opendir(PLUGINS_DIR "/formats/");

	if ( dir == NULL ) { /* oops */
		sprintf(message, _("You don't have %s"), dirname);
		gaby_message = g_strdup(message);
		gaby_errno = CUSTOM_WARNING;
		gaby_perror_in_a_box();
		return;
	}
	
	f = fopen(PLUGINS_DIR "/formats/infos", "r");
	
	while ( 1 ) {
		ent = readdir(dir);
		if ( ent == NULL ) break;
		if ( strncmp(ent->d_name, "lib", 3) == 0 && 
			strncmp( ent->d_name + strlen(ent->d_name) - lensoext,
				SO_EXTENSION, lensoext) == 0 ) {
			strcpy(dirname, ent->d_name+3);
			strchr(dirname, '.')[0] = 0;
			if (! format_plugin_can_do(f, dirname, st->table, type))
				continue;
			plugin = g_strdup(dirname);
			om_item = gtk_menu_item_new_with_label( 
					get_plugin_real_name(0, f, dirname) );
			plugin_list = g_list_append(plugin_list, plugin);
			
			if ( is_first ) {
				gtk_object_set_data(GTK_OBJECT(dialog),
						"plugin", plugin );
				is_first = FALSE;
			}
			if ( flg == 0 ) {
				gtk_object_set_data(GTK_OBJECT(dialog), 
						"plugin", plugin );
			}
			gtk_object_set_data(GTK_OBJECT(om_item), "plugin", 
					plugin );
			gtk_signal_connect(GTK_OBJECT(om_item), "activate", 
					format_selected, dialog );
			gtk_widget_show(om_item);
			gtk_menu_append(GTK_MENU(om_menu), om_item);
		}
	}
	
	closedir(dir);
	
#else /* FOLLOW_MIGUEL */
	{
		/* this shouldn't be written here ! */
		char *format_plugins[] =
			{ "addressbook", "appindex", "dbase", "dpkg",
			  "gaby", "gaby1", "nosql", "vcard", "videobase" };
		char name[80];
		int i=0;
		
		f = fopen(PLUGINS_DIR "/formats/infos", "r");
		for ( i=0; i<9; i++ ) {
			strcpy(name, format_plugins[i]);
			if (! format_plugin_can_do(f, name, st->table, type))
				continue;
			plugin = g_strdup(name);
			om_item = gtk_menu_item_new_with_label( 
					get_plugin_real_name(0, f, name) );
			plugin_list = g_list_append(plugin_list, plugin);
			
			if ( is_first ) {
				gtk_object_set_data(GTK_OBJECT(dialog),
						"plugin", plugin );
				is_first = FALSE;
			}
			if ( flg == 0 ) {
				gtk_object_set_data(GTK_OBJECT(dialog), 
						"plugin", plugin );
			}
			gtk_object_set_data(GTK_OBJECT(om_item), "plugin", 
					plugin );
			gtk_signal_connect(GTK_OBJECT(om_item), "activate", 
					format_selected, dialog );
			gtk_widget_show(om_item);
			gtk_menu_append(GTK_MENU(om_menu), om_item);
		}
	}
#endif /* FOLLOW_MIGUEL */
	if ( f != NULL ) fclose(f);

	gtk_widget_show(om_menu);
	gtk_option_menu_set_menu(GTK_OPTION_MENU(om), om_menu);
	gtk_option_menu_set_history(GTK_OPTION_MENU(om), 0 );
	gtk_widget_show(om);
	gtk_box_pack_start(GTK_BOX(hbox), om, TRUE, TRUE, 0 );

	if ( type == 0 ) { /* import */
		frame = gtk_frame_new("");
		gtk_widget_show(frame);
		label = gtk_label_new(
			_("Please note that the fields of the imported file\n"
			  "have to be the same as the ones of the table."));
		gtk_widget_show(label);
		gtk_container_add(GTK_CONTAINER(frame), label);
		gtk_box_pack_start(GTK_BOX(GTK_FILE_SELECTION
						(dialog)->main_vbox),
						frame, TRUE, TRUE, 0 );
	}

	gtk_object_set_data(GTK_OBJECT(dialog), "subtable", st);
	gtk_object_set_data(GTK_OBJECT(dialog), "plugin_list", plugin_list );
	gtk_widget_show(dialog);
}

void import_real_callback(subtable *st)
{
#if 0
	subtable *st;
	GtkWidget *clist;
	gchar *text;
	GList *subtables = list_subtables;
	
#ifdef DEBUG_GABY
	debug_print("[import_real_callback] --\n");
#endif
	clist = gtk_object_get_data(GTK_OBJECT(dialog), "clist");
	
	if ( GTK_CLIST(clist)->selection == NULL ) {
		gaby_message = g_strdup(_("You have to select a subtable."));
		gaby_errno = CUSTOM_ERROR;
		gaby_perror_in_a_box();
		return;
	}
	
	gtk_clist_get_text(GTK_CLIST(clist), 
			GPOINTER_TO_INT(GTK_CLIST(clist)->selection->data), 
			0, &text );
	
	st = get_subtable_pointer(subtables, text);
#endif /* 0 */
	
	if ( st == NULL ) {
#ifdef DEBUG_GABY
		debug_print("oops, something really bad happened to your tables\n");
#endif
	} else {
		import_export_dialog(st, 0);
	}
	
}

void export_real_callback(subtable *st)
{
#if 0
	subtable *st;
	GtkWidget *clist;
	gchar *text;
	GList *subtables = list_subtables;
	
	clist = gtk_object_get_data(GTK_OBJECT(dialog), "clist");
	
	if ( GTK_CLIST(clist)->selection == NULL ) {
		gaby_message = g_strdup(_("You have to select a subtable."));
		gaby_errno = CUSTOM_ERROR;
		gaby_perror_in_a_box();
		return;
	}

	gtk_clist_get_text(GTK_CLIST(clist), 
			GPOINTER_TO_INT(GTK_CLIST(clist)->selection->data), 
			0, &text );
	
	st = get_subtable_pointer(subtables, text);
#endif /* 0 */
	if ( st == NULL ) {
#ifdef DEBUG_GABY
		debug_print("oops, something really bad happened to your tables\n");
#endif
	} else {
		import_export_dialog(st, 1);
	}
}


