/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gaby.h"
#include "f_config.h"

#define MAX_LEN		300

static GList* load_config_file()
{
	GList *file = NULL;
	FILE *f;
	char filename[PATH_MAX];
	char st[MAX_LEN+1];
	
	sprintf(filename, "%s/.gaby/Gabyrc", g_get_home_dir());
	
	f = fopen(filename, "r");
	if ( f == NULL ) {
		return NULL;
	}
	
	while (1) {
		fgets(st, MAX_LEN, f);
		if ( feof(f) ) break;
		file = g_list_append(file, g_strdup(st));
	}
	
	return file;
}

static void free_config_file(GList *file)
{
	GList *a = file;

	while ( a != NULL ) {
		g_free(a->data);
		a = g_list_next(a);
	}
	
	g_list_free(file);
}

static FILE* config_file_goto(gchar *type, gchar *name, gchar *item)
{
	char filename[PATH_MAX];
	FILE *f;
	char begin_str[MAX_LEN+1];
	char st[MAX_LEN+1];
	
	sprintf(filename, "%s/.gaby/Gabyrc", g_get_home_dir());
	
	f = fopen(filename, "r");
	if ( f == NULL ) {
		return NULL;
	}
	
	sprintf(begin_str, "Begin %s:%s\n", type, name);

	do {
		fgets(st, MAX_LEN, f);
	} while ( strcmp(st, begin_str) != 0 && ! feof(f) );

	if ( feof(f) ) {
		fclose(f);
		return NULL;
	}

	while (1) {
		fgets(st, MAX_LEN, f);
		if ( st[0] == '#' ) continue;
		if ( strncmp(st+1, item, strlen(item)) == 0 ) {
			fseek(f, -(strlen(st)), SEEK_CUR);
			return f;
		}
		if ( strcmp(st, "End\n" )==0 || feof(f))
			break;
	}
	
	fclose(f);
	return NULL;
}

static gboolean write_config_file(GList *file)
{
	char filename[PATH_MAX];
	FILE *f;
	
	sprintf(filename, "%s/.gaby/Gabyrc", g_get_home_dir());
	
	f = fopen(filename, "w");
	if ( f == NULL ) {
		return FALSE;
	}

	while ( file != NULL ) {
		fputs((char*)(file->data), f);
		file = g_list_next(file);
	}
	
	fclose(f);

	return TRUE;
}

/**
 * write_config_str
 * @type: type of the plug-in (view, actions, ...)
 * @name: name of the plug-in
 * @item: name of the configuration option
 * @str: value for this option
 *
 * Description:
 * This function saves in the config file (usually Gabyrc) the option @item
 * with the value @str under a section which will be named @type:@name.
 *
 * Returns: TRUE if ok
 **/
gboolean write_config_str( gchar *type, gchar *name, gchar *item, gchar *str)
{
/*
 * ex: write_config_str("action", "net", "email_command", "xterm -e mutt");
 */
	GList *file;
	GList *a;
	gboolean section_ok = FALSE;
	gboolean section_ok_once = FALSE;
	char begin_str[MAX_LEN+1];
	char *st;
	char *new_str;
	int pos=1;
	
	file = load_config_file();
	
	sprintf(begin_str, "Begin %s:%s\n", type, name);

	new_str = g_malloc(strlen(item)+strlen(str)+30);
	new_str[0] = '\t';
	strcpy(new_str+1, item);
	strcat(new_str, " ");
	strcat(new_str, str);
	strcat(new_str, "\n");
	
	a = file;
	while ( a != NULL ) {
		st = a->data;
		if ( strcmp(st, begin_str) == 0 ) {
			section_ok = TRUE;
			section_ok_once = TRUE;
			a = g_list_next(a);
			continue;
		}
		if ( strcmp(st, "End\n") == 0 && section_ok == TRUE ) {
			section_ok = FALSE;
			a = g_list_next(a);
			continue;
		}
		if ( strncmp(st+1, item, strlen(item)) == 0 ) {
			g_free(st);
			a->data = new_str;
			break;
		}
		a = g_list_next(a);
	}
	
	if ( a == NULL ) { /* the entry didn't exist */
		a = file;
		if ( section_ok_once) {
			while ( strcmp((char*)(a->data), begin_str) != 0 ) {
				pos++;
				a = g_list_next(a);
			}
			while ( strcmp((char*)(a->data), "End\n") != 0 ) {
				pos++;
				a = g_list_next(a);
			}
			pos--;
			file = g_list_insert(file, new_str, pos);
		} else {
			file = g_list_append(file, g_strdup("\n"));
			file = g_list_append(file, g_strdup(begin_str));
			file = g_list_append(file, new_str);
			file = g_list_append(file, g_strdup("End\n"));
		}
	}

	if ( ! write_config_file(file) ) {
		free_config_file(file);
		
		gaby_errno = CUSTOM_ERROR;
		gaby_message = g_strdup(_("Error writing config file\n"));
		gaby_perror_in_a_box();
		
		return FALSE;
	}
	
	free_config_file(file);
	return TRUE;
}

/**
 * write_config_bool
 * @type: type of the plug-in (view, actions, ...)
 * @name: name of the plug-in
 * @item: name of the configuration option
 * @val: value for this option
 *
 * Description:
 * This function saves in the config file (usually Gabyrc) the option @item
 * with the boolean value @val under a section which will be named @type:@name.
 * The option will be written as 'TRUE' or 'FALSE' in the config file.
 *
 * Returns: TRUE if ok
 **/
gboolean write_config_bool( gchar *type, gchar *name, gchar *item, gboolean val)
{
/*
 * ex: write_config_bool("view", "form", "use_textbox_widget", TRUE);
 */
	return write_config_str(type, name, item, 
				( val == TRUE ) ? "TRUE" : "FALSE");
}

/**
 * write_config_int
 * @type: type of the plug-in (view, actions, ...)
 * @name: name of the plug-in
 * @item: name of the configuration option
 * @val: value for this option
 *
 * Description:
 * This function saves in the config file (usually Gabyrc) the option @item
 * with the integer value @val under a section which will be named @type:@name.
 *
 * Returns: TRUE if ok
 **/
gboolean write_config_int( gchar *type, gchar *name, gchar *item, gint val)
{
	char nb[30];
	sprintf(nb, "%d", val);
	return write_config_str(type, name, item, nb );
}

/**
 * get_config_str
 * @type: type of the plug-in (view, actions, ...)
 * @name: name of the plug-in
 * @item: name of the configuration option
 * @def: default value for this option
 *
 * Description:
 * This function gets from the config file (usually Gabyrc) the option @item
 * under a section which will be named @type:@name.
 *
 * Returns: the value of the option, @def if it was not present (the caller has
 * to g_free the allocated string)
 **/
gchar* get_config_str(gchar *type, gchar *name, gchar *item, gchar *def)
{
	FILE *f;
	char st[MAX_LEN+1];
	char *s;

	f = config_file_goto(type, name, item);
	if ( f == NULL ) return g_strdup(def);
	
	fgets(st, MAX_LEN, f);
	if ( st[0] == '#' || strncmp(st+1, item, strlen(item)) != 0 ) {
		/* looks like config_file_goto is not perfect
		 * (that shouldn't happen)
		 */
		fclose(f);
		return def;
	}
	fclose(f);
	
	s = st+1+strlen(item);
	strchr(s, '\n')[0] = 0;
#ifdef DEBUG_GABY
	debug_print("[get_config_str] analysing %s\n", s);
#endif
	while ( ( s[0] == ' ' || s[0] == '\t' ) && s[0] != 0 ) s++;
#ifdef DEBUG_GABY
	debug_print("[get_config_str] result : %s\n", s);
#endif
	
	return g_strdup(s);
}

/**
 * get_config_bool
 * @type: type of the plug-in (view, actions, ...)
 * @name: name of the plug-in
 * @item: name of the configuration option
 * @def: default value for this option
 *
 * Description:
 * This function gets from the config file (usually Gabyrc) the option @item
 * under a section which will be named @type:@name.
 *
 * Returns: the value of the option, @def if it was not present
 **/
gboolean get_config_bool( gchar *type, gchar *name, gchar *item, gboolean def)
{
	char *s;
	
	s = get_config_str(type, name, item, 
			( def == TRUE ) ? "TRUE" : "FALSE");
	
	if ( strncmp(s, "TRUE", 4) ==0 ) {
		g_free(s);
		return TRUE;
	} else {
		g_free(s);
		return FALSE;
	}
	
}

/**
 * get_config_int
 * @type: type of the plug-in (view, actions, ...)
 * @name: name of the plug-in
 * @item: name of the configuration option
 * @def: default value for this option
 *
 * Description:
 * This function gets from the config file (usually Gabyrc) the option @item
 * under a section which will be named @type:@name.
 *
 * Returns: the value of the option, @def if it was not present
 **/
gint get_config_int(gchar *type, gchar *name, gchar *item, gint def)
{
	char *s;
	char nb[30];
	gint res;

	sprintf(nb, "%d", def);
	s = get_config_str(type, name, item, nb);
	res = atoi(s);
	g_free(s);
	return res;
}

