/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "errors.h"
#include "gaby.h"

#ifndef USE_GNOME
	/* if gnome is present, we use gnome dialog boxes */

static void close_dialog(GtkWidget *w, GtkWidget *dlg)
{
	gtk_widget_destroy(dlg);
}

void gaby_dlg_message(gchar *title, gchar *message)
{
	GtkWidget *dialog;
	GtkWidget *vbox;
	GtkWidget *label;
	GtkWidget *button;

	dialog = gtk_dialog_new();
	gtk_window_set_title(GTK_WINDOW(dialog), title);
	vbox = GTK_DIALOG(dialog)->vbox;
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 5 );
	label = gtk_label_new(message);
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0 );
	gtk_widget_show(label);
	
	button = gtk_button_new_with_label(_("OK"));
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), 
					button, TRUE, TRUE, 5 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", close_dialog, dialog);
	gtk_widget_show(button);
	
	gtk_widget_show(dialog);
}

#endif

static void information_box()
{
	if ( gaby_message == NULL || strlen(gaby_message) == 0 )
		return;

	if ( app == NULL || ! GTK_WIDGET_REALIZED(app) ) {
		fputs(gaby_message, stderr);
		putc('\n', stderr);
		return;
	}

#ifdef USE_GNOME
	gnome_app_message(GNOME_APP(app), gaby_message);
#else
	gaby_dlg_message(_("Information"), gaby_message);
#endif
}

static void tip_box()
{
	if ( gaby_message == NULL || strlen(gaby_message) == 0 )
		return;

	if ( app == NULL ) {
		g_print(gaby_message);
		g_print("\n");
		return;
	}

#ifdef USE_GNOME
	gtk_window_set_title( GTK_WINDOW( 
		gnome_ok_dialog_parented(gaby_message, GTK_WINDOW(app))),
		_("Tip of the day") );
#else
	gaby_dlg_message(_("Tip of the day"), gaby_message);
#endif
}

static void warning_box()
{
	if ( gaby_message == NULL || strlen(gaby_message) == 0 )
		return;

	if ( app == NULL || ! GTK_WIDGET_REALIZED(app) ) {
		fputs(gaby_message, stderr);
		putc('\n', stderr);
		return;
	}

#ifdef USE_GNOME
	gnome_app_warning(GNOME_APP(app), gaby_message);
#else
	gaby_dlg_message(_("Warning"), gaby_message);
#endif
}

static void error_box()
{
	char str[1000];
	char *s;


	if ( gaby_errno == 3 ) {
		s = gaby_message;
	} else {
		s = gaby_perror();
		if ( gaby_message != NULL ) {
			switch ( gaby_errno ) {
				case FILE_READ_ERROR:
				case FILE_WRITE_ERROR:
				case LOAD_PLUGIN_ERROR:
				{
					sprintf(str, "%s (%s)", s,gaby_message);
				} break;
			}
			s = str;
		}
	}
	
	if ( app == NULL || ! GTK_WIDGET_REALIZED(app) ) {
		fputs(s, stderr);
		putc('\n', stderr);
		return;
	}

#ifdef USE_GNOME
	gnome_app_error(GNOME_APP(app), s);
#else
	gaby_dlg_message(_("Error"), s);
#endif
}
	

/**
 * gaby_perror_in_a_box
 * @win: the window in which occured the error
 * 
 * Description:
 * If gaby_errno is set (to something else than 0), this function shows a
 * box with a message explaining the error @gaby_errno (if the GUI isn't
 * currently available the message is simply printed on console).
 * Note that with some value of @gaby_errno (CUSTOM_*) it will use the text
 * which is in @gaby_message.
 **/
void gaby_perror_in_a_box()
{
	switch ( gaby_errno ) {
		case NO_ERROR: 		break;
		case CUSTOM_MESSAGE:	information_box(); break;
		case CUSTOM_WARNING:	warning_box(); break;
		case TIP_OF_THE_DAY:	tip_box(); break;
		default:		error_box(); break;
	}
	
	/*gaby_errno = NO_ERROR;*/
	
	if ( gaby_message != NULL ) {
		g_free(gaby_message);
		gaby_message = NULL;
	}

}

