/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

int record_add(table *t, record *r, gboolean check, gboolean loading);
void record_free(table *t, record *r);
gboolean record_modify(table *t, record *r);
gboolean record_remove(table *t, record *r);
gboolean record_remove_id(table *t, int id);
record* record_defaults(table *t);
record* record_duplicate(table *t, record *r);

record* table_next(table *t, record *r, int no_field);
record* table_prev(table *t, record *r, int no_field);
record* table_first(table *t, int no_field);
record* table_last(table *t, int no_field);

record* table_next_with_conditions( table *t, record *r, int no_field,
				    condition *c);
record* table_prev_with_conditions( table *t, record *r, int no_field,
				    condition *c);
record* table_first_with_conditions( table *t, int no_field, condition *c);
record* table_last_with_conditions( table *t, int no_field, condition *c);

int table_get_records_count(table *t);

gabycursor* cursor_declare(table *t);
record* cursor_get_first(gabycursor *cs);
record* cursor_get_next(gabycursor *cs);
void cursor_free(gabycursor *cs);

int table_search_record(table *t, int nf, char* what, int from);
int subtable_search_record(subtable *st, int nf, char* what, int from);

void tables_load();
gboolean table_load_file(table *t);
gboolean table_save_file(table *t);
void tables_save();
void table_maxindex(table *t);
gboolean tables_modified();

gboolean table_reread_file(table *t, struct location *loc);

void table_free_records(table *t);

