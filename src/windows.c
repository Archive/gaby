/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gaby.h"
#include "windows.h"
#include "tables.h"
#include "records.h"

/**
 * update_windows
 * @window: the window in which a change occured
 *
 * Description:
 * This function update every windows after a change (usually a new, modified
 * or deleted record) which happens in @window.
 */
void update_windows ( gabywindow *window )
{
	GList *a = list_windows;
	gabywindow *win;
	ViewPluginData *vpd;
	int *id_orig, *id;
	view *v_orig, *v;
	record *r;
	
	if ( window != NULL ) {
		v_orig = window->view;
		id_orig = &(window->id);
	} else {
		v_orig = NULL;
		id_orig = NULL;
	}
	
	while ( a != NULL ) {
		win = a->data;
		a = g_list_next(a);
		if ( win == window )
			continue;
		vpd = win->view->type;
		v = win->view;
		if ( v_orig != NULL && 
			    v_orig->subtable->table != v->subtable->table ) {
			continue;
		}
		if ( vpd->type == ALL_RECORDS ) {
			vpd->view_fill(win);
		} else {
			id = &(win->id);
			if ( get_record_no(v->subtable->table, *id) == NULL ) {
				/* 
				 * this is not the best way but this works and
				 * it is enough for now.
				 */
				r = table_first(v->subtable->table, -1);
				*id = ( r == NULL ) ? 0 : r->id;
				vpd->view_fill(win);
			} else {
				if ( id_orig == NULL || *id_orig == *id ) {
					vpd->view_fill(win);
				}
			}
		}
	}
}

/*
 * set_window_binding 
 * @bindings: the GList that holds the window binding structs  
 *
 * Description:
 * This function sets the window bindings loaded from the desc file. 
 * Note: every struct from bindings are freed
 **/
void set_window_binding(GList *bindings)
{
	gabywindow *win_main;
	gabywindow *bound;
	GList *main_loop;
	GList *loop;
	GList *bound_windows;
        gchar *name;
	struct w_bindings *wb;

	main_loop = g_list_first(bindings);
	while (main_loop != NULL) {
		wb = main_loop->data;
		win_main = get_window_by_name(wb->window_name);
		if (win_main == NULL) {
#ifdef DEBUG_GABY
			debug_print("Can't find window %s\n",wb->window_name);
#endif
			main_loop = main_loop->next;
			continue;
		
		} else {
#ifdef DEBUG_GABY
			debug_print("Parent : %s\n",wb->window_name);
#endif
		}
	
		bound_windows = win_main->bound_windows;

		/* Really not needed because we are the first thing called
	           but just to be sure... */
		if ( bound_windows != NULL) {
			GList *t = bound_windows;
			while ( t ) {
				g_free(t->data);
				t = g_list_next(t);
			}
			g_list_free(bound_windows);
			win_main->bound_windows = NULL;
			bound_windows = NULL;
		}

		loop = g_list_first(wb->bound_windows);
	 	while (loop != NULL ) {
			name = loop->data;
			bound = get_window_by_name(name);
			if ( bound != NULL ) {
#ifdef DEBUG_GABY
				debug_print("Binding : %s\n",name);
#endif
				bound_windows = g_list_append(bound_windows, 
								bound);	
			} else {
#ifdef DEBUG_GABY
				debug_print("Cannot find window %s\n",name);
#endif
			}
			loop = g_list_next(loop);
		}
		win_main->bound_windows = bound_windows;

		/* let's free the struct */
		loop = g_list_first(wb->bound_windows);
		while (loop != NULL) {
			g_free(loop->data);
			loop = loop->next;
		}
		g_list_free(wb->bound_windows);
		g_free(wb->window_name);
		g_free(wb);
		main_loop = main_loop->next;
	}
	g_list_free(main_loop);
	
	loop = list_windows;
	while ( loop != NULL ) {
		win_main = loop->data;
		update_bound_windows ( win_main );
		loop = g_list_next(loop);
	}
}

/**
 * get_window_by_name
 * @st: name of the window
 *
 * Description:
 * This functions searchs for a window named @st ( "%s [%s]", subtable name,
 * view name).
 *
 * Returns: the window
 **/
gabywindow* get_window_by_name(gchar *st)
{
	GList *a = g_list_first(list_windows);
	gabywindow *win;
	gchar *name;
		
	while ( a != NULL ) {
		win = a->data;
		name = win->name;
		
#ifdef DEBUG_GABY
		debug_print("[get_window_by_name] found %s\n", 
					( name == NULL ) ? "(--)" : name );
#endif
		if ( name != NULL && strcmp(name, st) == 0 ) {
			break;
		}
		a = g_list_next(a);
	}
	
	if ( a == NULL ) {
		return NULL;
	}
	
	return win;
}

/**
 * update_bound_windows
 * @window: the window in which a change occured
 *
 * Description:
 * This function sets the record id of every windows bound to @win to the
 * value it has in @win.
 **/
void update_bound_windows ( gabywindow *window )
{
	GList *bound_windows;
	gabywindow *a_win;
	ViewPluginData *vpd;
	int *new_id = &(window->id);
	int *id;
	static gboolean semaphore = FALSE;	/* not a real semaphore since
						 * it is only used to avoid
						 * recursion... */

	if ( semaphore == TRUE ) return;
	semaphore = TRUE;
	
#ifdef DEBUG_GABY
	debug_print("[update_bound_windows] window : %p\n", window);
#endif
	if ( window == NULL ) {
#ifdef DEBUG_GABY
		debug_print("[update_bound_windows] called with window==NULL !!!\n");
#endif
		semaphore = FALSE;
		return;
	}

	bound_windows = window->bound_windows;
	if ( new_id == NULL || *new_id == 0 ) {
#ifdef DEBUG_GABY
		debug_print("[update_bound_windows] called with bad window->id\n");
#endif
		semaphore = FALSE;
		return;	/* sorry, we can't admit that :) */
	}

	while ( bound_windows != NULL ) {
#ifdef DEBUG_GABY
		debug_print("[update_bound_windows] not yet NULL\n");
#endif
		a_win = bound_windows->data;
#ifdef DEBUG_GABY
		debug_print("[update_bound_windows] getting next...\n");
#endif
		bound_windows = g_list_next(bound_windows);
		if ( a_win == NULL || a_win->widget == NULL || 
						a_win->parent == NULL ) {
#ifdef DEBUG_GABY
			debug_print("[update_bound_windows] data is NULL\n");
#endif
			continue;
		}
#ifdef DEBUG_GABY
		debug_print("[update_bound_windows] grabbing daddy\n");
#endif

#if 0
		if ( a_win->id == 0 ) {
#ifdef DEBUG_GABY
			debug_print("[update_bound_windows] id is 0\n");
#endif
			continue;
		}
#endif
		
#ifdef DEBUG_GABY
		debug_print("[update_bound_windows] who is a_win?\n");
#endif
		id = &(a_win->id);
		if ( id == NULL ) {
#ifdef DEBUG_GABY
			debug_print("[update_bound_windows] id is NULL\n");
#endif
			continue;
		}
		/* this prevents an infinite loop */
		if ( *id != *new_id ) {
#ifdef DEBUG_GABY
			debug_print("[update_bound_windows] found a new window\n");
#endif
#if 1
			if ( (a_win->view->type->capabilities & EDITABLE) &&
					a_win->updated &&
					a_win->view->type->view_save &&
					! (window->view->type->capabilities
						& EDITABLE)
					)
			{
				a_win->view->type->view_save(a_win);
			}
#endif
			*id = *new_id;
			vpd = a_win->view->type;
			vpd->view_fill(a_win);
			update_bound_windows(a_win);
		}
	}
	semaphore = FALSE;
}

gboolean window_already_there(gchar *s)
{
	GList *a = g_list_first(list_windows);
	gboolean found = FALSE;
	gabywindow *win;

	while ( a != NULL ) {
		win = a->data;
		a = g_list_next(a);
		if ( strcmp(win->name, s) == 0 ) {
			found = TRUE;
			break;
		}
	}

	return found;
}


