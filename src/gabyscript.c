/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#define APP_DECLARED
#define APPNAME_DECLARED
#define LISTS_DECLARED
#define ALL_WINS_DECLARED
#define LANGUAGE_DECLARED

#include "errors.h"
#include "gaby.h"
#include "files.h"
#include "tables.h"
#include "records.h"
#include "actions.h"
#include "nogui_main.h"

GList *list_windows = NULL;
#ifdef NO_GUI
void *app = NULL;
#else
GtkWidget *app = NULL;
#endif
gchar *appname;
char language[3];

GList *list_tables = NULL;
GList *list_subtables = NULL;
GList *list_views = NULL;
GList *list_actions;

/* fake functions so that dynamic linking works even without everyfiles */
#ifdef FOLLOW_MIGUEL
#include "py_fake.c"
#endif
 
void gaby_property_box_changed(gpointer data) {	return; }

void gaby_perror_in_a_box()
{
	if ( gaby_errno == CUSTOM_MESSAGE || gaby_errno == CUSTOM_ERROR ) {
		fputs(gaby_message, stdout);
		fputs("\n", stdout);
	} else {
		fputs(gaby_perror(), stderr);
		fputs("\n", stderr);
	}
	if ( gaby_message ) g_free(gaby_message);
}

static void parse_options(int argc, char *argv[], char *name, char *interpreter)
{
	int c;
	
#ifdef GETOPTLONG_AVAILABLE
	static struct option long_options[] = {
		{ "as"         , 1, 0, 'a' },
		{ "interpreter", 1, 0, 'i' },
		{ "version"    , 0, 0, 'V' },
		{ "help"       , 0, 0, 'h' },
		{ 0            , 0, 0,  0  }
	};
#endif

	opterr = 0;

	while (1) {
#ifdef GETOPTLONG_AVAILABLE
		c = getopt_long(argc, argv, "a:i:Vh", long_options, NULL);
#else
		c = getopt(argc, argv, "a:i:Vh");
#endif
		if ( c == -1 ) break;
		switch ( c ) {
			case 'a':
			{
				strcpy(name, optarg);
			} break;
			case 'i':
			{
				strcpy(interpreter, optarg);
			} break;
			case 'V':
			{
				g_print(_("gabyscript %s\n"), VERSION);
				g_print(_("Copyright (c) 1999 Frederic Peters\n"));
				exit(0);
			} break;
			case 'h':
			{
				g_print(_("Usage: gabyscript --as .. --interpreter .. SCRIPT_FILE\n"));
				exit(0);
			} break;
		}
	}
	
}


#define COMMAND_LINE_SIZE	1000

int main(int argc, char *argv[])
{
	char name[30], interpreter[30], file[PATH_MAX];
	char scriptline[200];
	char cmdline[COMMAND_LINE_SIZE+20];
	int cmdline_len;
	char *fake_argv[20]; /*, **real_argv;*/
	int fake_argc;
	int i;
	action act;

	setlocale(LC_ALL, "");
	bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
	textdomain(GETTEXT_PACKAGE);

	strcpy(language, "en");
	if ( getenv("LANGUAGE") || getenv("LANG") ) {
		char *t = g_strdup_printf("%s", getenv("LANGUAGE") ?
				getenv("LANGUAGE")
				: getenv("LANG")   );
		t[2] = 0;
		strcpy(language, t);
		g_free(t);
	}
#if 0
#ifndef NO_GUI
	gtk_set_locale();
#endif
#endif

	name[0] = 0;
	interpreter[0] = 0;
	
#ifdef DEBUG_GABY
	debug_print("Paramaters : \n");
	for ( i=0; i<argc; i++ ) {
		debug_print("\t%d: %s\n", i, argv[i]);
	}
#endif

	/* the parameters are passed differently whether we're called from the
	 * command line or from a '#!' script first line.
	 * Therefore I remake a whole command line from them that I immediatly
	 * rebreak in smaller parts. This works but ...
	 *
	 * there is one 'little' problem: you can't put ' (quote) in args.
	 * I'll work on that when I'll receive a bug report describing me this
	 * bug from someone who didn't read this comment. (too late for you)
	 */
	cmdline[0] = 0;
	cmdline_len = 0;
	for ( i=1; i < argc; i++ ) {
		if ( cmdline_len > COMMAND_LINE_SIZE ) {
			/* I don't want Gaby to be featured on
			 * security@debian.org */
			g_print("you look malicious.\n");
			exit(-666);
		}
		if ( i != 1 && strchr(argv[i], ' ') ) {
			strcat(cmdline, "'");
			strcat(cmdline, argv[i]);
			strcat(cmdline, "'");
			cmdline_len+=3;
		} else {
			strcat(cmdline, argv[i]);
			cmdline_len++;
		}
		strcat(cmdline, " ");
		cmdline_len += strlen(argv[i]);
	}

#ifdef DEBUG_GABY
	debug_print( "cmdline : %s\n", cmdline );
#endif
	
	fake_argv[0] = argv[0];

	cmdline_len = 0;
	fake_argc=1;
	while ( strchr(cmdline+cmdline_len, ' ') ) {
		if ( (cmdline+cmdline_len)[0] == '\'' ) {
			strchr(cmdline+cmdline_len+1, '\'')[1] = 0;
			strchr(cmdline+cmdline_len+1, '\'')[0] = 0;
			fake_argv[fake_argc] = cmdline+cmdline_len+1;
			cmdline_len+=2;
				/* why 2 ? 'cause we remove the trailing ' */
		} else {
			strchr(cmdline+cmdline_len, ' ')[0] = 0;
			fake_argv[fake_argc] = cmdline+cmdline_len;
		}
		cmdline_len += strlen(fake_argv[fake_argc]);
		cmdline_len++;
		fake_argc++;
	}
	fake_argv[fake_argc] = NULL;

#if 0
	if ( argc != 6 && argc != 3 ) {
		g_print(_("Parameters missing (try --help)\n"));
		exit(1);
	}
		
	if ( argc == 6 ) { /* command-line */
		real_argv = argv;
	}
	if ( argc == 3 ) { /* first line from a script */
		fake_argv[0] = argv[0];
		fake_argv[1] = argv[1];
		i = 1;
		do {
			strchr(fake_argv[i], ' ')[0] = 0;
			fake_argv[i+1] = fake_argv[i]+strlen(fake_argv[i])+1;
			i++;
		} while ( strchr(fake_argv[i], ' ') );
		fake_argv[4] = fake_argv[3]+strlen(fake_argv[3])+1;
		fake_argv[5] = argv[2];
		real_argv = fake_argv;
		argc = 6;
	}
#endif

#ifdef DEBUG_GABY
	debug_print("argc : %d\n", fake_argc);
	for (i=0; i<fake_argc; i++) {
		debug_print("argv[%d] : %s\n", i, fake_argv[i]);
	}
#endif

	parse_options(fake_argc, fake_argv, name, interpreter);
	
	if ( strlen(name) == 0 || strlen(interpreter) == 0 || fake_argc < 6 ) {
		g_print(_("Parameters missing (try --help)\n"));
		exit(-1);
	}

	strcpy(file, fake_argv[5]);
	
#ifdef DEBUG_GABY
	debug_print("app : %s, interpreter : %s, script : %s\n", 
			name, interpreter, file );
#endif
	if ( strcmp(name, "select") == 0 ) {
		/* descfile selected on run-time */
		g_print(_("Enter the descfile you want to use: "));
		fgets(name, 29, stdin);
		name[ strlen(name)-1 ] = 0;
	}

        if ( ! tables_load_struct(name ) ) {
		g_print(_("Error loading the descfile (section: table)\n"));
		return 1;
	}

	if ( ! st_load_struct(NULL, name ) ) {
		g_print(_("Error loading the descfile (section: subtable)\n"));
		return 2;
	}
	
	tables_load();
	
	sprintf(scriptline, "%s:%s", interpreter, file);
	act.what.script = scriptline;
	act.nb_params = 0;
	
	if ( get_interpreter ( interpreter, fake_argv+6 ) == NULL ) {
		g_print(_("Error loading the interpreter\n"));
		return 3;
	}

	do_action_script_fu(&act);
	
	tables_save();

	return 0;
}

