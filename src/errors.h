/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#define gettext_noop(String) (String)

static char *errors_list[] = {
		     "No error",	/* this has to be the first one */
		     "No error but a message to show",
		     "No error but a warning message to show",
		     "Error and a custom message to show",
	gettext_noop("Sorry but this option is currently only available\n"\
		     "in the Gnome version of Gaby."),
	gettext_noop("Error reading from a file."),
	gettext_noop("Error writing to a file."),
	gettext_noop("Error using temporary files."),
	gettext_noop("Unable to load plug-in."),
	gettext_noop("Failed to save record."),
	gettext_noop("Unknown error.")	/* this has to be the last one */
};

#define N_ERRORS	sizeof(errors_list)/sizeof(char*)

int gaby_errno;
char *gaby_message;

#define gaby_perror()	(errors_list[ ( gaby_errno > N_ERRORS ) ? \
					  N_ERRORS-1 : gaby_errno ] )
#define GABY_ERRNO_DECLARED

#undef gettext_noop

