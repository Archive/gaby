/*  Gaby
 *  Copyright (C) 1998-2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

gboolean tables_load_struct(gchar *name);
gboolean table_load_file(table *t);
/*void tables_load(GList *list); */

gboolean st_load_struct (GList *plugins, gchar *name );

struct window_info* misc_load_main (GList *view_plugins);
GList* misc_load_other_windows (GList *view_plugins);

GList* viewplugins_load(gchar *name);
gboolean actions_load(gchar *name);
GList* printplugins_load(void);

property* field_get_property(field *f, gchar *ident);

table* get_table_by_name(char *st);
subtable* get_subtable_by_name( gchar *st);

int subtable_get_field_no(subtable *st, char *str);

char* get_plugin_real_name(int type, FILE *f, char *name);
gboolean format_plugin_can_do (FILE *f, char *name, table *t, gint way);

GList *load_window_bindings(gchar *name, GList *plugins);

GList* get_plugin_options(char *section);

void free_everything(void);

