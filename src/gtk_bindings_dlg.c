/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "gaby.h"
#include "windows.h"
/*
#include "files.h"
#include "v_main.h"
#include "menu.h"
#include "tables.h"
#include "configure.h"
*/
#include "gtk_dialogs.h"

static void update_combo(GtkCombo *combo);

#ifndef USE_GNOME
static void close_dialog(GtkWidget *w, GtkWidget *dlg)
{
	gtk_widget_hide(dlg);
}
#endif

static void wbinding_win_changed(GtkWidget *entry, GtkWidget *clist)
{
	gchar *st = gtk_entry_get_text(GTK_ENTRY(entry));
	gabywindow *win;
	GtkWidget *view_widget;
	gchar *name;
	view *view_one, *view_two;
	gabywindow *tmp_view;
	GtkWidget *tmp_parent;
	GList *a;
	int row;
	GList *bound_windows, *tmpbnd;

	win = get_window_by_name(st);
	if ( win == NULL || win->widget == NULL ) {
		/*
		 *  uh oh, the user has killed the selected window 
		 *  we have to clear the clist and update the combo
		 */
		tmp_view = gtk_object_get_data(GTK_OBJECT(entry), "combo");
		update_combo(GTK_COMBO(tmp_view));
		return;		
	}
	view_widget = win->widget;

	view_one = win->view;
	bound_windows = win->bound_windows;
	bound_windows = ( bound_windows != NULL ) ? 
				g_list_first(bound_windows) : NULL;
#ifdef DEBUG_GABY
	debug_print("[wwc] bound_windows : %p\n", bound_windows);
#endif

	gtk_clist_clear(GTK_CLIST(clist));
	
	a = g_list_first(list_windows);
	
	while ( a != NULL ) {
		tmp_view = a->data;
		a = g_list_next(a);
		if ( tmp_view == win ) continue;
		view_two = tmp_view->view;
		if ( view_two == NULL ) continue;
		if ( view_two->type->type == FILTER ) continue;
		if ( view_one->subtable->table == view_two->subtable->table ) {
			tmp_parent = tmp_view->parent;
			if ( ! GTK_IS_WINDOW(tmp_parent) )
				continue;
			name = tmp_view->name;
			row = gtk_clist_append(GTK_CLIST(clist), &name);

			tmpbnd = g_list_first(bound_windows);
#ifdef DEBUG_GABY
			debug_print("[wwc] looking for %p in bound_windows\n", 
					tmp_view );
			debug_print("[wwc] currently in bound_windows :");
#endif
			/* could certainly be done by g_list_find but I removed
			 * it while tracking for another bug :)
			 */
			while ( tmpbnd != NULL ) {
#ifdef DEBUG_GABY
				debug_print("  %p", tmpbnd->data);
#endif
				if ( tmpbnd->data == tmp_view ) {
					/*  select_row if this windows is also
					 *  in the 'bound_windows' of
					 *  view_widget
					 */
					gtk_clist_select_row(GTK_CLIST(clist),
							row, 1);
				}
					
				tmpbnd = g_list_next(tmpbnd);
			}
#ifdef DEBUG_GABY
			debug_print("\n");
#endif

		}
	}
}

static void update_combo(GtkCombo *combo)
{
	GList *a = g_list_first(list_windows);
	gabywindow *window;
	GtkWidget *li;
	GtkWidget *view_widget;
	GtkWidget *its_parent;
	gchar *name;
	ViewPluginData *vpd;
	
	gtk_list_clear_items(GTK_LIST(combo->list), 0, -1);

	while ( a != NULL ) {
		window = a->data;
		a = g_list_next(a);
		its_parent = window->parent;
		if ( GTK_IS_WINDOW(its_parent) ) {
			vpd = window->view->type;
			view_widget = window->widget;
			if ( vpd->type == FILTER ) continue;
			name = window->name;
#ifdef DEBUG_GABY
			debug_print("[update_combo] adding : %s\n", name);
#endif
			li = gtk_list_item_new_with_label(name);
			gtk_widget_show(li);
			gtk_container_add(GTK_CONTAINER(combo->list), li);
		}
	}
	
}

void selection_changed_callback( GtkCList *clist, gint row, gint column,
				 GdkEventButton *event, GtkCombo *combo )
{
	GtkWidget *entry = combo->entry;
	gchar *current_win_name;
	gabywindow *current_win;
	GList *selection;
	gchar *name;
	gabywindow *a_win;
	GList *bound_windows;
	
	if ( column == 1 ) {
		/* 
		 * this prevents wbinding_win_changed() to go further in this
		 * function.
		 */
		return;
	}
	
	current_win_name = gtk_entry_get_text(GTK_ENTRY(entry));
#ifdef DEBUG_GABY
	debug_print("[selection_changed_callback] this apply to %s\n", current_win_name);
#endif
	current_win = get_window_by_name(current_win_name);
	if ( current_win == NULL ) {
#ifdef DEBUG_GABY
		debug_print("[ssc] aborting, no window found\n");
#endif
	}

	bound_windows = current_win->bound_windows;
	
	if ( bound_windows != NULL ) {
		g_list_free(bound_windows);
		bound_windows = NULL;
	}
	
	selection = clist->selection;
	
#ifdef DEBUG_GABY
	debug_print("[ssc] selection :");
#endif
	while ( selection != NULL ) {
#ifdef DEBUG_GABY
		debug_print("  %d", GPOINTER_TO_INT(selection->data));
#endif
		gtk_clist_get_text(clist, GPOINTER_TO_INT(selection->data), 
				0, &name);
		
		selection = g_list_next(selection);
		
		a_win = get_window_by_name(name);
		if ( a_win == NULL )	/* this shouldn't happens */
			continue;
		
		bound_windows = g_list_append(bound_windows, a_win);
	}

#ifdef DEBUG_GABY
	debug_print(".\n");
#endif

	current_win->bound_windows = g_list_first(bound_windows);

	update_bound_windows(current_win);
}

GtkWidget* windows_binding_dialog()
{
	static GtkWidget *dialog = NULL;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *combo;
	GtkWidget *clist;
#ifndef USE_GNOME
	GtkWidget *button;
#endif

	if ( dialog && dialog->window && GTK_IS_WINDOW(dialog))
		return dialog;

#ifdef USE_GNOME
	dialog = gnome_dialog_new(_("Windows binding"), 
			GNOME_STOCK_BUTTON_CLOSE, NULL );
	gnome_dialog_set_close(GNOME_DIALOG(dialog), TRUE);
	vbox = GNOME_DIALOG(dialog)->vbox;
#else
	
	dialog = gtk_dialog_new();
	gtk_window_set_title(GTK_WINDOW(dialog), _("Windows binding") );
	vbox = GTK_DIALOG(dialog)->vbox;
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 5 );

	button = gtk_button_new_with_label(_("Close"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->action_area), 
			button, TRUE, TRUE, 5 );
	gtk_signal_connect(GTK_OBJECT(button), "clicked", close_dialog, dialog);	
#endif
	hbox = gtk_hbox_new(FALSE, 5);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0 );
	gtk_widget_show(hbox);
	label = gtk_label_new(_("Window :"));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0 );
	combo = gtk_combo_new();
	gtk_object_set_data(GTK_OBJECT(GTK_COMBO(combo)->entry),"combo",combo);
	gtk_entry_set_editable(GTK_ENTRY(GTK_COMBO(combo)->entry), FALSE);
	
	gtk_widget_show(combo);
	gtk_widget_set_usize(GTK_COMBO(combo)->list, 350, 100 );
	gtk_box_pack_start(GTK_BOX(hbox), combo, TRUE, TRUE, 0 );

	clist = gtk_clist_new(2);
	gtk_clist_set_column_visibility(GTK_CLIST(clist), 1, FALSE);
	gtk_signal_connect(GTK_OBJECT(clist), "select_row", 
			GTK_SIGNAL_FUNC(selection_changed_callback), combo);
	gtk_signal_connect(GTK_OBJECT(clist), "unselect_row", 
			GTK_SIGNAL_FUNC(selection_changed_callback), combo);
	gtk_clist_set_selection_mode(GTK_CLIST(clist), GTK_SELECTION_MULTIPLE);
	gtk_widget_set_usize(clist, 350, 100);	/* :( */
	gtk_widget_show(clist);
	gtk_box_pack_start(GTK_BOX(vbox), clist, TRUE, TRUE, 0 );
	
	gtk_signal_connect(GTK_OBJECT(GTK_COMBO(combo)->entry),"changed", 
			GTK_SIGNAL_FUNC(wbinding_win_changed), clist);
	
	update_combo(GTK_COMBO(combo));

	/* we could select the first one */
	
	return dialog;
}

