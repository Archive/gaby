/*  Gaby
 *  Copyright (C) 1998-1999 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#define NO_ERROR                0
#define CUSTOM_MESSAGE          1
#define CUSTOM_WARNING          2
#define CUSTOM_ERROR            3
#define ONLY_IN_GNOME           4
#define FILE_READ_ERROR         5
#define FILE_WRITE_ERROR        6
#define TEMPFILE_ERROR		7
#define LOAD_PLUGIN_ERROR	8
#define SAVE_RECORD_ERROR	9

#define TIP_OF_THE_DAY		998
#define UNKNOWN_ERROR           999

void gaby_perror_in_a_box();

