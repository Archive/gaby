/*  Gaby
 *  Copyright (C) 2000 Frederic Peters
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef _PRINT_PLG_H
#define _PRINT_PLG_H

typedef struct _GabyPage	GabyPage;
typedef struct _GabyPageFields	GabyPageFields;
typedef struct _GabyPageMisc	GabyPageMisc;

typedef struct _GabyWidget	GabyWidget;

enum pagetype { FIELDS, MISC, RECORDS, ORDER };

struct _GabyPage {
	gchar *label;
	enum pagetype type;
	gpointer real_widget;
};

struct _GabyPageFields {
	GabyPage p;
	gboolean optional;
	gint selection;
};

struct _GabyPageMisc {
	GabyPage p;
	GList *widgets;
};

enum widgettype { FILE_SELECTION, LABEL, CHECKBOX, ENTRY };

struct _GabyWidget {
	enum widgettype type;
	gchar *label;
	gpointer real_widget;
};

enum structype {
	PAGE, PAGE_FIELDS, PAGE_RECORDS, PAGE_ORDER,
	WIDGET_FILESEL, WIDGET_CHECKBOX, WIDGET_ENTRY
};

struct ref {
	enum structype type;
	char *key;
	gpointer pointer;
};

int print_load_xml(char *filename, GList **pages, GList **names);

#endif /* ! _PRINT_PLG_H */

